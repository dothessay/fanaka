<?php

use App\DB\Sale\MonthlyInstallmentPlan as InstallmentPlan;
use Codex\Classes\Handlers\SendInnoxNotification;
use Codex\Classes\Repository\MonthlyInstallmentPlan;
use Illuminate\Support\Facades\Storage;
use Phpml\Classification\KNearestNeighbors;
use Phpml\Classification\MLPClassifier;
use Phpml\NeuralNetwork\ActivationFunction\PReLU;
use Phpml\NeuralNetwork\ActivationFunction\Sigmoid;
use Phpml\NeuralNetwork\Layer;
use Phpml\NeuralNetwork\Node\Neuron;

//Home Routes

Route::get('/', 'HomeController@index')->name('dashboard');
Route::get('/un-paid-plots', 'HomeController@unpaidPlots')->name('un-paid-plots');
Route::get('/search', 'HomeController@search')->name('home.search');




Route::group([
    'prefix' => 'notification'
], function () {

    Route::get('/details/{id}','NotificationController@index')->name('notification');
    Route::get('/add-issue','NotificationController@addIssues')->name('notification_add-issue');
    Route::post('/store-issue','NotificationController@storeIssues')->name('notification_store-issue');
    Route::get('/issue-details/{id}','NotificationController@issueDetails')->name('notification_issue-details');
    Route::get('/issue-details-delete','NotificationController@issueDetailDelete')->name('notification_issue-details-delete');
});

Route::get('loadExpenses/{project_id}', 'Product\StoreProjectExpensesController@loadExpenses');



Route::get('profile/edit/{userId}' , function (){
    return view('profile');
})->name('profile_edit')->middleware('auth');




Route::group([
    'prefix' => 'offer',
    'namespace' => 'Offer',
    'middleware' => ['web','auth','access.module']
] , function () {

    Route::group([
        'prefix' => 'generate',
        'middleware' => ['web','auth','access.module']
    ] , function () {

        Route::get('/','GenerateOfferController@index')->name('offer_generate');
        Route::post('/store-customers','EditReservationController@storeCustomers')->name('offer_generate_store-customers');
        Route::get('/customer-remove/{customer}/{offer}','EditReservationController@removeCustomers')->name('offer_generate_customer_remove');
        Route::post('store','StoreOfferController@store')->name('offer_generate_store');

        /*********UPDATE RESERVATION PAYMENT********/


        Route::post('update-reservation-payment','UpdateReservationPaymentController@index')->name('offer_generate_update-reservation-payment');


        Route::get('convert-to-sale','GenerateOfferController@convertToSale')->name('offer_generate_convert-to-sale');
        Route::post('convert-to-sale','GenerateOfferController@convertingToSale')->name('offer_generate_convert-to-sale');
        Route::get('receipt','GenerateOfferController@offerReceipts')->name('offer_generate_receipt');
        Route::get('list-sale','GenerateOfferController@listSale')->name('offer_generate_list-sale');
        Route::get('download/{offer_id}','GenerateOfferController@generateOfferLetter')->name('offer_generate_download');
        Route::get('print/{offer_id}','GenerateOfferController@generateOfferLetter')->name('offer_generate_print');


        Route::get('add-note', 'AddOfferNotesController@index')->name('offer_generate_add-note');
        Route::post('store-add-note', 'AddOfferNotesController@store')->name('offer_generate_store-add-note');

        Route::get('approve/{offer}',"GenerateOfferController@approve")->name('offer_generate_approve');
        Route::post('approving',"GenerateOfferController@approving")->name('offer_generate_approving');
        Route::get('download-receipt/{uploadId}',"GenerateOfferController@downloadReceipt")->name('offer_generate_download-receipt');

        //editing


        Route::get('edit','EditReservationController@index')->name('offer_generate_edit');
        Route::get('edit-details','EditReservationController@editDetails')->name('offer_generate_edit-details');
        Route::post('edit-details','EditReservationController@updateDetails')->name('offer_generate_edit-details');
        Route::post('post-kin-details','EditReservationController@storeKinDetails')->name('offer_generate_post-kin-details');
        Route::get('delete-kin-details','EditReservationController@removeNextOfKin')->name('offer_generate_delete-kin-details');

        Route::post('print-reservation','GenerateOfferController@printOfferLetter')->name('offer_generate_print-reservation');



    });

});

/**********DASHBOARD ROUTES**********/


Route::get('customer-uploads','CustomerFileUploadsController@index')->name('customer-uploads');
Route::get('customer-uploads-mark-as-seen/{file}','CustomerFileUploadsController@markAsSeen')->name('customer-uploads-mark-as-seen');
Route::get('customer-referrals','CustomerReferralsController@index')->name('customer-referrals');
Route::get('customer-referrals/contact/{referral}','CustomerReferralsController@contact')->name('customer-referrals');




/***
 *
 * This where test routes will be
*/

Route::any('test', function (\Illuminate\Http\Request $request) {

    dd(\Codex\Classes\Helper::numberToWord(70000));

    /*$data = Codex\Classes\Handlers\Table::build()
        ->setOffset(0)
        ->setLimit(10)
        ->setBuilder(\App\Sale\Sale::query())
        ->dateBetween(\Carbon\Carbon::now()->subRealMonths(3)->startOfMonth() , now()->endOfMonth())
        ->setFilterResult(['agent' => 12])
        ->outPut();

    dd($data);*/



    $allBalanceSales = \App\Sale\Sale::where('balance' ,'>',  0)->get();

    foreach ($allBalanceSales as $sale){

       if (isset($sale->installment->completion_date))
       {
           $months = \Carbon\Carbon::parse($sale->installment->completion_date)
               ->diffInMonths(\Carbon\Carbon::parse($sale->created_at));

           for ($i = 0 ; $i < $months ; $i++)
           {
               InstallmentPlan::updateOrCreate([
                   'sale_id'   => $sale->id,
                   'month'     =>  \Carbon\Carbon::parse($sale->created_at)->addRealMonths($i)->format('Y-m'),
                   'day'       =>  \Carbon\Carbon::parse($sale->created_at)->format('d'),
               ],[
                   'sale_id'   => $sale->id,
                   'month'     =>  \Carbon\Carbon::parse($sale->created_at)->addRealMonths($i)->format('Y-m'),
                   'day'       =>  \Carbon\Carbon::parse($sale->created_at)->format('d'),
                   'is_paid'   =>  false,
                   'amount'    => $sale->installment->amount,
                   'penalty'   => 0
               ]);
           }
       }



       foreach ($sale->salePayments as $payment){

           $month = \Carbon\Carbon::parse($payment->created_at)->format('Y-m');

           $monthlyPlan = App\DB\Sale\MonthlyInstallmentPlan::where([
               'sale_id'  => $sale->id,
               'month'    =>$month
           ])->first();

           if ($monthlyPlan){

               if ($payment->amount > $monthlyPlan->amount ){

                   // CREATE A PREPAID

                   $request = request();

                   $request->request->add(['month'  => $payment->created_at]);

                   (new \Codex\Classes\Repository\MonthlyInstallmentPrepaid())
                           ->setSale($sale)
                           ->store($request);
               }

               $monthlyPlan->is_paid =  true;

               $monthlyPlan->save();
           }
       }
    }
    dd('closer');


    dd(\Illuminate\Support\Facades\Artisan::call('notification:installmentReminder'));


    return "";
    // all sunrise

    $month = $request->has('month') ? $request['month'] : 7;
    $deposit = $request->has('deposit') ? $request['deposit'] : 100000 ;
    $rates = \Codex\Classes\Handlers\Calculator\CalculatorHandlerFacade::getRate($month)->calculate(399000 , $deposit);
    $sum = 0;
    $interest = 0;
    foreach ($rates as $rate) {

        $sum += $rate['monthlyPayment'];
        $interest += $rate['depositInterest'];
    }

    dd($rates , $sum , $interest);


    $project = \App\DB\Product\Product::where('slug','fanaka-plam-gardens')->orderBy('created_at','ASC')->first();

    $startParsel = 83246;
    $titlePrefix = "DONYO SABUK/KOMAROCK BLOCK 1/";
        foreach ($project->plots as $plot) {
            $plot->title_no = $titlePrefix.$startParsel;
            $plot->save();
            $startParsel++;
            if ($startParsel <= 83288)
            {
                continue;
            }
            else{
                break;
            }
        }

    return $titlePrefix;
    $lates = [];

    foreach (\App\Sale\Sale::all() as $sale) {

        if ($sale->getBalance() > 0)
        {
            $installment = $sale->installment;
           $now = now();

           $endDate = Carbon\Carbon::parse(isset($installment->completion_date )? $installment->completion_date  : \Carbon\Carbon::parse($sale->created_at)->addMonths(6));

           $diff = $endDate->diffInMonths($now);

           if ($now->greaterThan($endDate))
           {
               for( $i = 0 ; $i < $diff ; $i++)
               {
                   \App\DB\Sale\InterestSale::create([
                       'sale_id' => $sale->id,
                       'amount'    => 0.01 * floatval($sale->getBalance()),
                       'interest_date'   => \Carbon\Carbon::parse($endDate)->addMonths($i),
                       'percentage'    => 0.01 * 100

                   ]);
                   $lates[] = [
                       'sale_id' => $sale->id,
                       'fine'    => fanakaConfig('fine', 0.12) * floatval($sale->getBalance()),
                       'month'   => \Carbon\Carbon::parse($endDate)->addMonths($i)->format('Y-F-d'),
                       'diff'    => $diff
                   ];
               }
           }

        }

    }

    dd($lates);

})->name('test');
Route::get('test-b', function (\Illuminate\Http\Request $request) {
    $layer1 = new Layer(2, Neuron::class, new PReLU);
    $layer2 = new Layer(2, Neuron::class, new Sigmoid);
    $mlp = new MLPClassifier(4, [$layer1, $layer2], ['Dun', 'Ken', 'Mercy']);
    $mlp->setLearningRate(0.1);
    $mlp->partialTrain(
        $samples = [[1, 3, 2, 0], [3, 4, 2, 2]],  // SALES PER WEEK OF September and October
        $targets = ['Dun', 'Dun'] // Marketer
    );
    $mlp->partialTrain(
        $samples = [[1, 1, 1, 1], [2, 0, 2, 1]],
        $targets = ['Ken', 'Ken']
    );
    $mlp->partialTrain(
        $samples = [[1, 1, 1, 1], [0, 2, 1, 1]],
        $targets = ['Mercy', 'Mercy']
    );
   $result  =   $mlp->predict([[1, 1, 1 , 1], [1, 0, 3, 1]]);

   dd('done', $result);

    $samples = [
        [3,5,2,4,3,5,2,4,7,11],
        [4,3,3,4,0,2,4,5,6,5],
        [0,0,0,0,0,0,2,4,8,5],
        [0,0,0,0,0,0,0,0,0,1],
        [5,4,7,5,6,8,5,9,7,5],
        [5,4,5,3,2,4,2,3,5,3],

    ]; // sales october
    $labels = ['Duncan', 'Ken', 'Mercy', 'Carol', 'Liz', 'Pesh'];  // Marketers

    $classifier = new KNearestNeighbors();
    $classifier->train($samples, $labels);

    echo $classifier->predict([5,4,5,4,2,4,2,3,5,3  ]);

    dd('finished');

    $arra = [];

    foreach (\App\Sale\SalePayment::oldest()->get() as $payment) {

        $todayAmount = $payment->amount;

        $previousAmount = floatval($payment->sale->salePayments->sum('amount')) - floatval($todayAmount);

        $previousPercentage = (floatval($previousAmount) * 100 ) / floatval($payment->sale->getTotalSaleAmount());


        $sale = $payment->sale;
        $sale->half_payment_at = null;


        $sale->save();


            if ($payment->sale->getPercentage() >= 50)
            {

                $arra['sales-above-now'][] = $sale->getPercentage();

                $sale->half_payment_at = \Carbon\Carbon::parse($payment->created_at);

            }

            if ($previousPercentage >=  50  && is_null($sale->half_payment_at)) {


                $arra['sales-above-previous'][] = $sale->getPercentage();
                $sale->half_payment_at = \Carbon\Carbon::parse($payment->created_at);


            }



       $sale->save();
    }



})->name('test-b');

Route::get('test-a', function (\Illuminate\Http\Request $request) {

    \Illuminate\Support\Facades\Schema::table('sales', function (\Illuminate\Database\Schema\Blueprint $table) {


        $table->string('is_paying')->default(true);;



    });


    


   /* $details = [];

    foreach (\App\DB\Sale\CustomerSale::all() as $customerSale) {


        $details[] = [
            'customer' => $customerSale->customer->fullName(),
            'sale_id'  => $customerSale->sale->id,
            'plot_no'  => $customerSale->saleItems()->pluck('id')
        ];



    }

    dd($details);*/



    /*\Illuminate\Support\Facades\Schema::table('sale_payments', function (\Illuminate\Database\Schema\Blueprint $table) {


        $table->timestamp('deposit_date')->after('payment_type')->nullable();


    });


    \Illuminate\Support\Facades\Schema::table('offer_payments', function (\Illuminate\Database\Schema\Blueprint $table) {

        $table->string('deposit_date')->nullable();



    });/*
    \Illuminate\Support\Facades\Schema::table('accounts', function (\Illuminate\Database\Schema\Blueprint $table) {




        $table->boolean('is_active')->after('accountable_id')->default(false);
        $table->unsignedInteger('payable_id')->after('accountable_id')->nullable();
        $table->string('payable_type')->after('accountable_id')->nullable();



    });*/


})->name('test-a');

//Route::get('/{any}', "HomeController@index")->where('any', '.*')->name('dashboard');
