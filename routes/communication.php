<?php

Route::get('/settings','CommunicationController@index')->name('communication_settings');
Route::post('/settings/store','CommunicationController@store')->name('communication_settings_store');


Route::group([
    'prefix'  => 'manage'
] , function () {

    Route::get('/','SendCommunicationController@index')->name('communication_manage');
    Route::post('/store','SendCommunicationController@store')->name('communication_manage_store');
    Route::get('/download-contacts','SendCommunicationController@downloadContacts')->name('communication_manage_download-contacts');
    Route::post('/bulk-sms','SendCommunicationController@bulkSms')->name('communication_manage_bulk-sms');
    Route::group(['prefix' => 'contacts'], function () {

        Route::get('/','SendCommunicationController@contacts')->name('communication_manage_contacts');
        Route::post('/store','SendCommunicationController@storeContacts')->name('communication_manage_contacts_store');
        Route::get('/{contact}/edit','SendCommunicationController@editContacts')->name('communication_manage_contacts_edit');
        Route::patch('/{contact}/update','SendCommunicationController@updateContacts')->name('communication_manage_contacts_update');
        Route::post('/import','SendCommunicationController@importContacts')->name('communication_manage_contacts_import');
        Route::get('/list','SendCommunicationController@listContacts')->name('communication_manage_contacts_list');
        Route::get('import-samples','SendCommunicationController@ImportsContactsSamples')->name('communication_manage_contacts_import-samples');
        Route::get('export','SendCommunicationController@exportsContacts')->name('communication_manage_contacts_export');

    });
    Route::get('/list','ManageCommunicationController@listEmails')->name('communication_manage_list');
    Route::get('/{uid}/read','ManageCommunicationController@readEmails')->name('communication_manage_read');
});


Route::get('test' , function () {



    echo  phpinfo();

   // $inbox = (new \Codex\Classes\Handlers\ImapHandler())->inbox(10);

    //dd($inbox);



});
