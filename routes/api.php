<?php

use App\DB\Contract;
use App\DB\Notification\Notification;
use App\Jobs\SendSmsNotifications;
use App\Sale\Invoice;
use Carbon\Carbon;
use Codex\Classes\Handlers\SendAdvantaAfricaInnoxNotification;
use Codex\Classes\Handlers\SendInnoxNotification;
use Codex\Classes\Handlers\SystemSms;
use Codex\Classes\Repository\MonthlyInstallmentPlan;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Phpml\Classification\KNearestNeighbors;
use Phpml\Classification\MLPClassifier;
use Phpml\NeuralNetwork\ActivationFunction\PReLU;
use Phpml\NeuralNetwork\ActivationFunction\Sigmoid;
use Phpml\NeuralNetwork\Layer;
use Phpml\NeuralNetwork\Node\Neuron;

Route::get('notification', function (Request $request) {

    try
    {
        Mail::raw($request->message , function ($mail) {
            $mail->to("no-reply@fanaka.co.ke")
                ->subject("New Lead:")
                ->cc("sales@fanaka.co.ke")
                ->from('no-reply@fanaka.co.ke');
        });

        \App\DB\Communication\Contact::create([
            'phone' => $request['tel'],
            'name' => $request['name'],
            'hasVisited' =>   false,
            'user_id' =>  1 ,
        ]);

        SendInnoxNotification::build()
            ->sendSms( new SendAdvantaAfricaInnoxNotification() , ['to' => $request['to'] ,'message'  => $request['message']]);

    } catch (Exception $e){

        Mail::raw($e->getMessage() , function ($mail) {
            $mail->to("philipnjuguna66@gmail.com")
                ->subject("Erro on Pipe:")
                ->from('no-reply@fanaka.co.ke');
        });
    }

});



Route::group([
    'prefix' => 'offer',
    'namespace' => 'Offer',
] , function () {
    Route::get('sale','GenerateOfferController@listSale')->name('offer_sale');
});

Route::group([
    'namespace' => 'API',
] , function () {

    Route::get('projects', 'ProjectApiController@index')->name('projects');
    Route::get('appointment', 'AppointmentApiController@index')->name('appointment');
    Route::get('reservations', 'ReservationApiController@index')->name('reservations');
});


Route::post('login', function (Request $request){



    $user = \App\User::where('email', $request['email'])->first();

    if (! $user)
    {
        return response()->json([
            'status' => 'ERROR',
            'message'   => "These Credentials Does not match our records"
        ] , 401);
    }

    if (! \Illuminate\Support\Facades\Hash::check($request['password'] , $user->password))
    {
        return response()->json([
            'status' => 'ERROR',
            'message'   => "Password Does not match our record"
        ] , 401);
    }

    return response()->json([
        'status' => 'OK',
        'data'   => $user
    ] , 200);

})->name('login');



Route::group([
    'prefix' => 'payment',
    'namespace'   => 'API\\Payment'
] , function () {
    Route::get('/c2b/register','RegisterMpesaC2BController@index')->name('api_payment_c2b_register');


    Route::get('c2b/confirmation',"MpesaC2BConfirmationController@index")->name("api_payment_c2b_confirmation");

    Route::get('c2b/validation',"MpesaC2BValidationController@index")->name('api_payment_c2b_validation');
});

Route::any('callback',  function () {

    return view('test')->with([
        'callback' => true
    ]);
});


Route::any('test', function (\Illuminate\Http\Request $request) {

    $sales = \App\DB\Sale\CustomerSale::all();
    foreach ($sales as $sale)
    {
        $customer = $sale->customer;


            $customer->user_id = $sale->sale->agent;

            $customer->save();

    }

    $directorSales =  \App\DB\Sale\SaleDirector::all();

    foreach ($directorSales as $directorSale) {

       // $customer = $directorSale->customer;

       // $customer->user_id = $directorSale->sale->agent;

        //$customer->save();

    }





    $startParsenal = 91373;


    $lastNo = 91428;
})->name('api_test');



Route::get('blank-agreement', function () {


    $contract  = Contract::where('name', 'agreement')->first();

    $agreement = $contract->value;



    $agreement = str_replace("#date",Carbon::now()->format('Y-m-d') , $agreement);
    $agreement = str_replace("#purchaser","........................." , $agreement);
    $agreement = str_replace("#plotDetails","........................." , $agreement);
    $agreement = str_replace("#saleAmountInWords","........................." , $agreement);
    $agreement = str_replace("#saleAmountInWords","........................." , $agreement);
    $agreement = str_replace("#saleAmountInFigures","........................." , $agreement);
    $agreement = str_replace("#paidAmount","........................." , $agreement);
    $agreement = str_replace("#installmentStatement","........................." , $agreement);
    $agreement = str_replace("of 12%","<b>of 12%</b>" , $agreement);
    $firstPage = "<style> .body{ 
                  font-family: serif; 
                  font-size: 12px;
                   
                  }</style>
                   <div class='body'> <p style=\"font-weight: bold; font-size: 12pt; align-content: center; text-align: center\">
                  
                     <img src='" . asset('assets/img/fanaka_logo.png') . "' style=\"width: 100%;height: auto; max-height: 70px\"alt=\"Fanaka Real Estate\"/>
     
                    <p style='text-align: center; font-family: serif;  font-weight: bolder; font-size: 16px; '>DATED….." . Carbon::now()->format('Y-F-d') . "	
                                                 <br/>
                                                 <br/>
                                                 <br/>
                                                 <br/>
                                                 <br/>
                                                 <br/>
                                                 <br/>AGREEMENT FOR SALE
                                                  <br/>
                                                 <br/>
                                                 <br/>
                                                 <br/>
                                                 <br/>
                                                 <br/>
                                                 <br/>FANAKA REAL ESTATE LTD
                                                 <br/>
                                                 <br/>
                                                 <br/>
                                                 <br/>
                                                 <br/>
                                                 (Vendor)
                                                    <br/>
                                                 <br/>
                                                 <br/>
                                                 <br/>
                                                 <br/>
                                                 <br/>
                                                   <br/>.....................................
                                                   
                                                    <br/>
                                                 <br/>
                                                 <br/>
                                                 
                                                   <br/>(Purchaser)
                                                 <br/>
                                                 <br/>
                                                 <br/>
                                                 <br/>
                                                 <br/>
                                                 <br/>
                                                   <br/>.................................................                                          
                                                   
                         </p>";

    $firstPage .= "<p style='text-align: center ; letter-spacing: 3px'>
      <h1 style='align-content: center ; text-align: center'>
      <b>REPUBLIC OF KENYA</b></h1>
       <br/>
       <br/>
      
       </p> 
       
       <p  style='align-content: center ; text-align: center'>
        <b><u style='line-height: 1cm;'>SALE AGREEMENT</u></b>
        </p>";

    $html = $firstPage .$agreement;

    $winteness = "<p style='text-align: justify ;  margin-top: 50%'></p><br/>
                    <br/>                 
                    VENDOR<br/>
                    <br/>
                    <br/>
                    <b>Fanaka Real Estate Limited,<br/>
                    Dune Plaza, Suite D1 , Ruai Town, <br/>
                    P.o Box 252-00222 , Nairobi, <br/>
                    Tel: +254 799 000 111, <br/>
                    Email: info@fanaka.co.ke.</b>
                    <br/>
                    <br/>DIRECTOR<br/><br/>
                    <b>NAME: MOSES MURIITHI KIHUNII  <br/>
                    ID NO: 25876155 <br/><br/>
                    Signature: ........................................<br/>
                     <br/><br/>
                    SIGNED BY THE PURCHASER<br/>
                    Name: ........................................<br/>
                    ID: ..........................................<br/>
                    Phone Number: ................................<br/>
                    Signature: ........................................<br/>
                     <br/><br/>
                    <br/><br/>
                    SIGNED BY THE WITNESS<br/>
                    Name: ........................................<br/>
                    ID: ..........................................<br/>
                    Phone Number: ................................<br/>
                    Signature: ........................................<br/>
                     <br/><br/>
                    <br/><br/>
                    Next Of Kins<br/>
                    Name: ........................................<br/>
                    Phone Number: ................................<br/>
                    <br/>";

    //$html .= $winteness;




    $pdf = new \Mpdf\Mpdf();

    $pdf->watermark(asset("assets/img/fanaka_logo.png"));

    $pdf->WriteHTML($html);

    $pdf->AddPage();


    $pdf->WriteHTML($winteness);

    $pdf->Output();


});


Route::get('leadwebhook', function (Request $request) {

    $request->setUserResolver(function () {
        return \App\User::first();
    });

    $challage = $request['hub_challenge'];

    echo  $challage;

    Mail::raw($challage, function ($mail) {
       $mail->to('philipnjuguna66@gmail.com')->subject('facebook chat challenge ');
    });

});

