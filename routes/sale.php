<?php
Route::group([
    'prefix' => 'manage'
], function () {
    Route::get('/', 'ManageSaleController@index')->name('sale_manage');
    Route::get('approve-sale/{saleID}', 'ManageSaleController@getSale')->name('sale_manage_approve');
   //    Route::get('edit-agreement/{saleID}', 'ManageSaleController@editAgreement')->name('sale_manage_approve-agreement');
    Route::get('approve-agreement/{saleID}', 'ManageSaleController@approveAgreement')->name('sale_manage_approve-agreemt');
    Route::get('approve-agreement/{saleID}', 'ManageSaleController@approveAgreement')->name('sale_manage_approve-agreement');

    Route::get('edit-agreement/{sale}', 'EditSaleAgreementController@index')->name('sale_manage_edit_agreement');
    Route::post('edit-agreement/{sale}', 'EditSaleAgreementController@update')->name('sale_manage_edit_agreement');

    Route::get('edit-installment/{sale}', 'EditSaleAgreementController@editInstallment')->name('sale_manage_edit_installment');
    Route::post('edit-installment/{sale}', 'EditSaleAgreementController@updateInstallment')->name('sale_manage_edit_installment');

    // edit saele details change plot no

    Route::post('change-plot/{sale}','EditSaleAgreementController@changePlot')->name('sale_manage_change-plot');

    Route::get('reject-agreement/{saleID}', 'ManageSaleController@rejectAgreement')->name('sale_manage_approve-agreemt');
    Route::get('add', 'ManageSaleController@add')->name('sale_manage_add');
    Route::get('create-sale/{id}', 'ManageSaleController@createSale')->name('sale_manage_create-sale');
    Route::post('store', 'StoreSaleController@index')->name('sale_manage_store');

    Route::post('store-witness', 'StoreSaleController@witness')->name('sale_manage_store-witness');
    Route::get('remove-witness/{id}', 'StoreSaleController@removeWitness')->name('sale_manage_remove-witness');
    Route::get('remove-kins/{id}', 'StoreSaleController@removeKins')->name('sale_manage_remove-kins');
    Route::post('update', 'ManageSaleController@index')->name('sale_manage_update');
    Route::get('agreement/{id}', 'ManageSaleController@agreement')->name('sale_manage_agreement');
    Route::get('agreement/{id}', 'ManageSaleController@agreement')->name('sale_manage_agreement');
    Route::get('agreement/{upload}/delete', 'ManageSaleController@destroy')->name('sale_manage_agreement_delete');
    Route::get('uploaded-agreement/{id}', 'ManageSaleController@uploadedAgreement')->name('sale_manage_uploaded-agreement');
    Route::get('get-uploaded-agreement/{id}', 'ManageSaleController@viewUploadedAgreement')->name('sale_manage_get-uploaded-agreement');
    Route::post('upload-scanned-agreement', 'ManageSaleController@uploadScannedAgreement')->name('sale_manage_upload-scanned-agreement');
    Route::get('receipt/{id}', 'ManageSaleController@receipt')->name('sale_manage_receipt');
    Route::get('approve/{id}', 'ManageSaleController@approve')->name('sale_manage_approve');
    Route::get('reject/{id}', 'ManageSaleController@reject')->name('sale_manage_reject');
    Route::get('view', 'ViewSaleController@index')->name('sale_manage_view');

    Route::get('pay-title/{saleId}','PayTitleController@index')->name('sale_manage_pay-title');
    Route::post('store-pay-title','PayTitleController@pay')->name('sale_manage_store-pay-title');


    // calulator

    Route::group(['prefix' => 'calculator'], function () {
        Route::get('/','CalculatorController@index')->name('sale_manage_calculator');
    });


    //Acknowgement

    Route::group(['prefix' => 'acknowledgement'], function (){
        Route::get('{sale}/view','AcknowledgementController@index')->name('sale_manage_acknowledgement_view');
        Route::post('{sale}/print','AcknowledgementController@print')->name('sale_manage_acknowledgement_print');
    });


    Route::group([
        'prefix' => 'customer'
    ], function () {

        Route::post('store','MoreSaleCustomerController@index')->name("sale_manage_customer_store");
        Route::post('directors','MoreSaleCustomerController@directors')->name("sale_manage_customer_directors");
        Route::get('directors-remove/{id}','MoreSaleCustomerController@removeDirectors')->name("sale_manage_customer_directors-remove");
        Route::get('remove/{id}','MoreSaleCustomerController@remove')->name("sale_manage_customer_remove");
        Route::post('separate-customer','MoreSaleCustomerController@separate')->name("sale_manage_customer_separate");

    });




    Route::group([
        'prefix' => 'payment',
        'namespace' => 'Payment',
    ], function (){
        Route::post('update','ConfirmPaymentController@update')->name("sale_manage_payment_update");
        Route::get('confirm','ConfirmPaymentController@index')->name("sale_manage_payment_confirm");
        Route::get('reject','ConfirmPaymentController@reject')->name("sale_manage_payment_reject");
    });



    Route::group([
        'prefix'  => 'instalment',
        'namespace' => 'Instalment'
    ], function () {
        Route::get('plan','InstalmentController@plan')->name('sale_manage_instalment_plan');
        Route::get('plan/{sale}/details','InstalmentController@show')->name('sale_manage_instalment_plan.details');
    });


});
Route::group([
    'prefix' => 'invoice'
], function () {
    Route::get('/', 'ManageSaleInvoiceController@index')->name('sale_invoice');
    Route::get('details/{id}', 'ManageSaleInvoiceController@details')->name('sale_invoice_details');
    Route::get('print-details/{id}', 'ManageSaleInvoiceController@outPut')->name('sale_invoice_print-details');
    Route::post('update', 'ManageSaleInvoiceController@update')->name('sale_invoice_update');
});


Route::group([
    'prefix' => 'reports'
], function () {
    Route::get('/','SaleReportController@index')->name('sale_reports');
    Route::get('above-50','SaleReportController@above50')->name('sale_reports_above-50');
    Route::get('late-installment','SaleReportController@lateInstallment')->name('sale_reports_late-installment');
    Route::get('week-installment','SaleReportController@weekInstallment')->name('sale_reports_week-installment');
    Route::get('below-50','SaleReportController@below50')->name('sale_reports_below-50');
    Route::get('completed','SaleReportController@completed')->name('sale_reports_completed');
    Route::get('by-date','SaleReportController@byDate')->name('sale_reports_by-date');
    Route::get('availability','SaleReportController@availability')->name('sale_reports_availability');
    Route::get('by-project','SaleReportController@byProject')->name('sale_reports_by-project');
    Route::get('download-agreement','SaleReportController@scannedAgreement')->name('sale_reports_scanned-agreement');
    Route::get('unpaid','SaleReportController@unpaid')->name('sale_reports_unpaid');
    Route::get('reservation','SaleReportController@reservation')->name('sale_reports_reservation');
    Route::get('reservation','SaleReportController@byCustomer')->name('sale_reports_customer');
    Route::get('agent','SaleReportController@byAgent')->name('sale_reports_agent');
    Route::get('agent/commission','SaleReportController@byAgentCommissions')->name('sale_reports_agent.commissions');
    Route::get('profit-loss','SaleReportController@ProfitLoss')->name('sale_reports_profit-loss');

    // performance


    Route::get('performance','SaleReportController@performance')->name('sale_reports_performance');
    Route::get('performance/{user}','SaleReportController@agentPerformance')->name('sale_reports_agent.performance');
    Route::get('performance/{user}/{month}','SaleReportController@agentPerformanceDetails')->name('sale_reports_agent.performance.details');


});


//Appointments


Route::group([
    'prefix' => 'appointment',
    'namespace'  => 'Appointment'
] , function () {

    Route::get('/','AppointmentController@index')->name('sale_appointment');
    Route::post('store','AppointmentController@store')->name('sale_appointment_store');
    Route::post('update','TodaysAppointmentReportsController@updateAppointment')->name('sale_appointment_update');

    /**
     * Reports comes here
     */

    Route::get('today-appointment','TodaysAppointmentReportsController@index')->name('sale_appointment_today-appointment');
    Route::get('absent-appointment','TodaysAppointmentReportsController@absentAppointment')->name('sale_appointment_absent-appointment');
});




Route::group([
    'prefix' => 'commissions',
    'namespace'  => 'Commission'
] , function () {

    Route::get('/','CommissionController@index')->name('sale_commissions_index');
    Route::post('/update','CommissionController@update')->name('sale_commissions_update');


});


Route::group(['prefix' => 'payments','namespace' => 'Payment'], function () {

    Route::get('/','PaymentController@index')->name('sale_payments');
    Route::get('{payment}/edit/{customer}','PaymentController@edit')->name('sale_payments_edit');
    Route::patch('{payment}/update/{customer}','PaymentController@update')->name('sale_payments_update');
    Route::get('{payment}/delete','PaymentController@delete')->name('sale_payments_delete');

});

Route::group(['prefix' => 'interest', 'namespace'  => 'Interest'] , function (){
    Route::get('/','InterestController@index')->name('sale_interest');
    Route::get('/details/{sale}','InterestController@saleDetailsInterest')->name('sale_interest_details');
});


Route::group(['prefix' => 'transfer', 'namespace'  => 'Transfer'] , function (){
    Route::get('/','TransferController@index')->name('sale_transfer');
    Route::get('/list','TransferController@allTransfers')->name('sale_transfer_list');
    Route::get('start-processing/{titleProcess}','TransferController@startProcessing')->name('sale_transfer_start_processing');
    Route::get('upload-documents/{titleProcess}','TransferController@uploadDocuments')->name('sale_transfer_upload-documents');
    Route::post('upload-documents/{titleProcess}','TransferController@storeUploadDocuments')->name('sale_transfer_upload-documents');
});

Route::group([
    'prefix' => 'manage/v2'
], function ( ) {
    Route::post('/','SaleController@store')->name('sale_manage_v2_store');
});




