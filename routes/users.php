<?php
Route::group([
    'prefix' => 'manage'
], function () {

    Route::get('/', 'ManageUserController@index')->name('users_manage');
    Route::get('/list', 'ManageUserController@agents')->name('users_manage_list');
    Route::get('/{user}/details', 'ManageUserController@details')->name('users_manage_details');
    Route::post('store', 'StoreUserController@index')->name('users_manage_store');
    Route::get('{user}/activate/', 'StoreUserController@activate')->name('users_manage_activate');
    Route::post('update', 'StoreUserController@update')->name('users_manage_update');
    Route::post('delete', 'StoreUserController@delete')->name('users_manage_delete');
    Route::get('delete', 'StoreUserController@delete')->name('users_manage_delete');
    Route::get('/{user}/impersonate', 'ManageUserController@impersonate')->name('users_manage_impersonate');
    Route::get('leave-impersonate', 'ManageUserController@leaveImpersonate')->name('users_manage_leave-impersonate');
});


Route::group([
    'prefix'  => 'department-head',
    'namespace' => 'Department'
] , function () {
    Route::get('/','DepartmentController@index')->name('users_department-head');
    Route::get('/create','DepartmentController@create')->name('users_department-head_create');
    Route::get('/{id}/show','DepartmentController@show')->name('users_department-head_show');
    Route::post('/store','DepartmentController@store')->name('users_department-head_store');
    Route::post('/{id}/update','DepartmentController@update')->name('users_department-head_update');
    Route::get('/','DepartmentController@index')->name('users_department-head');
});


Route::group([
    'prefix' => 'role'
], function () {

    Route::get('/', 'ManageRoleController@index')->name('users_role');
    Route::get('/{role}/details', 'ManageRoleController@details')->name('users_role_details');
    Route::post('store', 'StoreRoleController@index')->name('users_role_store');
    Route::post('update', 'UpdateRoleController@index')->name('users_role_update');
    Route::post('delete', 'DeleteRoleController@index')->name('users_role_delete');
});

Route::group([
    'prefix' => 'permissions'
], function () {

    Route::get('/', 'ManagePermissionController@index')->name('users_permissions');
    Route::get('configure-role/{id}', 'ManagePermissionController@configureRole')->name('users_permissions_configure-role');
    Route::get('configure-role-module/{id}', 'ManagePermissionController@configureRoleModule')->name('users_permissions_configure-role-module');
    Route::get('configure-role-components/{id}', 'ManagePermissionController@configureRoleComponent')->name('users_permissions_configure-role-components');
    Route::get('store-configure-role-components', 'ManagePermissionController@storeRoleComponent')->name('users_permissions_store-configure-role-components');
    Route::get('store-configure-role-functionality', 'ManagePermissionController@storeRoleFunctionality')->name('users_permissions_store-configure-role-functionality');
    Route::get('configure-role-functionality/{id}', 'ManagePermissionController@configureRoleFunctionality')->name('users_permissions_configure-role-functionality');

    //User permissions


    Route::get('configure-user/{id}', 'ManagePermissionController@configureUser')->name('users_permissions_configure-user');
    Route::get('configure-user-module/{id}', 'ManagePermissionController@configureUserModule')->name('users_permissions_configure-user-module');
    Route::get('configure-user-components/{id}', 'ManagePermissionController@configureUserComponent')->name('users_permissions_configure-user-components');
    Route::get('store-configure-user-components', 'ManagePermissionController@storeUserComponent')->name('users_permissions_store-configure-user-components');
    Route::get('store-configure-user-functionality', 'ManagePermissionController@storeUserFunctionality')->name('users_permissions_store-configure-user-functionality');
    Route::get('configure-user-functionality/{id}', 'ManagePermissionController@configureUserFunctionality')->name('users_permissions_configure-user-functionality');


});




Route::group([
    'prefix' => 'leave',
    'namespace'  => 'Leave'
], function () {

    Route::get('/', 'ManageLeaveController@index')->name('users_leave');

    Route::group([
        'prefix' => 'type',
    ], function () {

        Route::get('/', 'ManageLeaveTypeController@index')->name('users_leave_type');
        Route::post('/store', 'ManageLeaveTypeController@store')->name('users_leave_type_store');
        Route::get('/{type}/find', 'ManageLeaveTypeController@find')->name('users_leave_type_find');
        Route::get('/{type}/delete', 'ManageLeaveTypeController@delete')->name('users_leave_type_delete');

    });

    Route::group([
        'prefix' => 'request',
    ], function () {

        Route::get('/', 'ManageLeaveRequestController@index')->name('users_leave_requests');
        Route::post('/store', 'ManageLeaveRequestController@store')->name('users_leave_request_store');
        Route::get('/{leave}/approve', 'ManageLeaveRequestController@approve')->name('users_leave_request_approve');
        Route::get('/{leave}/find', 'ManageLeaveRequestController@find')->name('users_leave_request_find');
        Route::get('/{leave}/delete', 'ManageLeaveRequestController@delete')->name('users_leave_request_delete');


        Route::get('/{leave}/accept', 'ManageLeaveRequestController@accept')->name('users_leave_request_accept');
        Route::get('/{leave}/reject', 'ManageLeaveRequestController@reject')->name('users_leave_request_reject');
        Route::patch('/{leave}/reject', 'ManageLeaveRequestController@updateReject')->name('users_leave_request_reject');



        Route::group([
            'prefix' => 'report',
        ], function () {

            Route::get('/', 'ManageLeaveReportController@index')->name('users_leave_requests_report');
            Route::get('/accepted', 'ManageLeaveReportController@index')->name('users_leave_requests_report_accepted');
            Route::get('/rejected', 'ManageLeaveReportController@index')->name('users_leave_requests_report_rejected');
            Route::get('/pending', 'ManageLeaveReportController@index')->name('users_leave_requests_report_pending');
            Route::get('/remaining', 'ManageLeaveReportController@index')->name('users_leave_requests_report_remaining');

        });




    });

});
