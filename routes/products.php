<?php
Route::group([
    'prefix' => 'manage'
] , function () {

    Route::get('/','ManageProductController@index')->name('projects_manage');
    Route::post('store','StoreProductController@index')->name('projects_manage_store');
    Route::post('delete','DeleteProductController@index')->name('projects_manage_delete');
    Route::post('update', 'StoreProductController@update')->name('projects_manage_update');
    Route::get('details/{slug}', 'ManageProductController@details')->name('projects_manage_details');



    Route::get('available/{slug}','AvailablePlotsController@index')->name('projects_manage_available');
    Route::get('sold/{slug}','AvailablePlotsController@sold')->name('projects_manage_sold');
    Route::get('hold/{product}','AvailablePlotsController@hold')->name('projects_manage_hold');
    Route::get('available-for-sale/{product}','AvailablePlotsController@hold')->name('projects_manage_available-for-sale');


    Route::group(['prefix' => 'pricing/{product}'], function () {


        Route::get('/', 'ManageProductController@pricing')->name('projects_manage_pricing');
        Route::get('/create', 'ManageProductController@pricingCreate')->name('projects_manage_pricing_create');
        Route::post('/store', 'ManageProductController@pricingStore')->name('projects_manage_pricing_store');
        Route::get('/{pricing}/edit', 'ManageProductController@pricingEdit')->name('projects_manage_pricing_edit');
        Route::get('/{pricing}/delete', 'ManageProductController@pricingDelete')->name('projects_manage_pricing_delete');

    });
    Route::group(['prefix' => '{product}/map/'], function () {
        Route::get('/', 'MapProductController@index')->name('projects_manage_map');
        Route::post('/store', 'MapProductController@store')->name('projects_manage_map_store');
        Route::get('/download', 'MapProductController@download')->name('projects_manage_map_download');
        Route::get('/delete', 'MapProductConoller@destroy')->name('projects_manage_map_delete');

    });
});

Route::group([
    'prefix' => 'expenses'
], function (){

    Route::get('expenses', function (){ return "closer" ;})->name('projects_expenses')->middleware('access.module');
    Route::get('get-expenses/{id}','ListProjectExpensesController@getExpenses')->name('projects_expenses_get-expenses');
    Route::post('expenses/add', 'StoreProjectExpensesController@index')->name('projects_expenses_add');
    Route::post('deleteexpense','DeleteProjeExpensesController@index')->name('projects_delete_expense');
});

Route::group([
    'prefix' =>'reports',
    'namespace' => 'Report'
], function () {
    Route::get('/','ProjectReportController@index')->name('projects_reports');
    Route::get('/below30','ProjectReportController@below30K')->name('projects_reports_below_30k');
    Route::get('/unpaid','ProjectReportController@unPaid')->name('projects_reports_unpaid');
    Route::get('/plot-with-clients','ProjectReportController@allPlotsWithClients')->name('projects_reports_plot-with-clients');
    Route::get('above-50','Above50ProjectReportController@index')->name('projects_reports_above-50');
    Route::get('below-50','Below50ProjectReportController@index')->name('projects_reports_below-50');
    Route::get('completed','CompletedPlotsController@index')->name('projects_reports_completed');
    Route::get('available','AvailableProjectReportController@index')->name('projects_reports_availability');
    Route::get('map/{projectId}','AvailableProjectReportController@printMap')->name('projects_reports_map');
    Route::get('taken','ProjectReportController@index')->name('projects_reports_taken');
    Route::get('collections','ProjectReportController@collection')->name('projects_reports_collections');
});



//Route::get('expenses','ListProjectExpensesController@index')->name('projects_expenses');
