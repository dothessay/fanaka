<?php

Route::get('company','CompanySettingController@index')->name('settings_company');
Route::post('store','CompanySettingController@store')->name('settings_company_store');
Route::post('store-message-template','CompanySettingController@storeMessageTemplates')->name('settings_company_store-message-template');
Route::any('store-config','CompanySettingController@storeConfig')->name('settings_company_store-config');
Route::any('store-agreement-config','CompanySettingController@storeAgreementConfig')->name('settings_company_store-agreement-config');


Route::get('system','SystemSettingController@index')->name('settings_system');
Route::get('system/{id}/find','SystemSettingController@find')->name('settings_system_find');
Route::post('system/update','SystemSettingController@update')->name('settings_system_update');
