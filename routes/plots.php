<?php

Route::group([
    'prefix' => 'manage',

], function () {

    Route::get('/','ManagePlotController@index')->name('plots_manage');
    Route::get('/titles','ManagePlotController@getTitles')->name('plots_manage_titles');

    Route::get('details/{plot}','ManagePlotController@details')->name('plots_manage_details');

    Route::post('store','StorePlotController@index')->name('plots_manage_store');
    Route::post('update','UpdatePlotController@index')->name('plots_manage_update');
    Route::post('delete','DeletePlotController@index')->name('plots_manage_delete');

    Route::post('mark-available','DeletePlotController@markAvailable')->name('plots_manage_mark-available');
    Route::post('release','DeletePlotController@release')->name('plots_manage_release');


    Route::get('reports','PlotReportsController@index')->name('plots_manage_reports');
    Route::post('searchdate','PlotReportsController@searchdate')->name('plots_manage_searchdate');
    Route::get('issue-title/{plotId}','ManagePlotController@IssueTitle')->name('plots_manage_issue-title');

    Route::get('uploads/{plot}', "UploadPlotDocumentController@index")->name('plots_manage_uploads');
    Route::post('store-uploads/', "UploadPlotDocumentController@store")->name('plots_manage_store-uploads');
    Route::get('download-uploads/{id}', "UploadPlotDocumentController@downloadUpload")->name('plots_manage_download-uploads');


    Route::get('mark-can-sell','MarkCanSellPlotController@index')->name('plots_manage_mark-can-sell');

    Route::post('mark-can-not-sell','MarkCanSellPlotController@hold')->name('plots_manage_store-mark-can-not-sell');
    Route::post('mark-can-sell','MarkCanSellPlotController@unHold')->name('plots_manage_store-mark-can-sell');


    // Plot holding request form


    Route::group([
        'prefix'  => 'request',
    ], function () {
        Route::get('/','PlotHoldRequestController@index')->name('plots_manage_request_index');
        Route::post('store','PlotHoldRequestController@store')->name('plots_manage_request_store');
        Route::get('{holdingRequest}','PlotHoldRequestController@show')->name('plots_manage_request_show');
        Route::patch('{holdingRequest}','PlotHoldRequestController@update')->name('plots_manage_request_update');
    });


});


Route::group([
    'prefix' => 'reports',

], function () {

    Route::get('/','PlotReportsController@index')->name('plots_reports');
    Route::get('plots_reports_available','PlotReportsController@avaialble')->name('plots_reports_available');

});




