<?php



Route::any('client-login', "ClientLoginController@index");
Route::post('update-profile', "UpdateClientProfileController@index");

Route::group(['prefix' => 'sales'], function () {

    Route::get('/',"StatementController@index");
    Route::get('/statement',"StatementController@agreement");
    Route::get('/receipt',"StatementController@receipt");

});

Route::get('agents', "AgentsController@index");

Route::group(['prefix' => 'referral'], function () {


    Route::get('/', "ReferralController@index");

    Route::post('/store', "ReferralController@store");

});
Route::group(['prefix' => 'customer-uploads'], function () {


    Route::post('/', "CustomerUploadsController@index");
    Route::get('list', "CustomerUploadsController@getUploads");
    Route::get('download', "CustomerUploadsController@download");
    Route::get('delete/{upload}', "CustomerUploadsController@delete");
    Route::get('download/{id}', "CustomerUploadsController@downloadFile");

});


Route::group(['prefix' => 'notifications'], function () {

    Route::get('/','NotificationController@index');
    Route::post('/read','NotificationController@read');

});

