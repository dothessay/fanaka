<?php

use App\DB\Communication\Contact;
use App\DB\Contract;
use App\DB\Plot\PlotHoldingRequest;
use App\DB\PrintContract;
use App\User;
use App\Plot\Plot;
use App\Sale\Sale;
use App\Customer\Customer;
use App\DB\Product\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;

Route::group([
    'namespace' => 'Auth'
], function () {
    Route::post('login', 'LoginController@login');
    Route::middleware('auth:api')->get('/profile', 'LoginController@profile');
});

Route::group([
    'prefix' => 'sales',
    'namespace' => 'Sales',
    'middleware' => 'auth:api'
], function () {
    Route::get('/', 'SalesController@index');
});

Route::group([
    'prefix' => 'clients',
    'namespace' => 'Client',
    'middleware' => 'auth:api'
], function () {
    Route::get('/', 'ClientController@index');
    Route::post('/store', 'ClientController@store');

    //the last route
    Route::get('/{id}', 'ClientController@show');
});

Route::group([
    'prefix' => 'user',
    'namespace' => 'User',
    'middleware' => 'auth:api'
], function () {
    Route::get('/', 'UserController@index');
    Route::get('sales', 'UserController@sales');

    //the last route
    Route::get('/{id}', 'UserController@show');
});


Route::group(['prefix' => 'export'], function () {

    Route::get('users', function () {
        return User::all()->map(function ($user) {
            return [
                'id' => $user->id,
                'name' => $user->fullName(),
                'password' => $user->password,
                'phone_number' => $user->phone_number,
                'email' => $user->email,
                'is_active' => $user->is_active,
                'created_at' => $user->created_at,
                'role' => $user->role->name
            ];
        });
    });

    Route::get('projects', function () {
        return Product::all()->map(function ($product) {
            return [
                'id' => $product->id,
                'name' => $product->name(),
                'title_no' => $product->title_no,
                'size' => $product->size,
                'cost' => $product->cost,
                'created_at' => $product->created_at,
            ];
        });
    });


    Route::get('plots', function () {
        return Plot::all()->map(function ($plot) {
            return [
                'id' => $plot->id,
                'project_id' => $plot->product->id,
                'title_no' => $plot->title_no,
                'size' => $plot->size->label,
                'plot_no' => $plot->plot_no,
                'created_at' => $plot->created_at,
                'can_sell' => $plot->can_sell,
                'price' => $plot->price,
                'is_sold' => $plot->is_sold
            ];
        });
    });


    Route::get('clients', function () {
        return Customer::all()->map(function ($client) {
            return [
                'id' => $client->id,
                'name' => $client->fullName(),
                'phone_number' => $client->phone_number,
                'user_id' => $client->user_id,
                'email' => $client->email,
                'password' => $client->password,
                'has_downloaded_app' => $client->has_downloaded_app,
                'id_no' => $client->id_no,
                'created_at' => $client->created_at,
                'count' => Customer::count()
            ];
        });
    });

    Route::get('sales', function () {
        return Sale::all()->map(function ($sale) {// dd($sale);
            if ($sale->getBalance() > 0) {
                $installment = isset($sale->installment) ? $sale->installment->installmentToArray()['months'] : [];
            } else {
                $installment = [];
            }

            return [
                'sale_ref' => $sale->id,
                'date' => Carbon::parse($sale->created_at)->format('Y-m-d H:i:s'),
                'sale_date' => $sale->created_at,
                'customers' => $sale->customers->map(function ($customer) {
                    return ['id' => $customer->id];
                }),
                'directors' => $sale->directors->map(function ($customer) {
                    return ['id' => $customer->id];
                }),
                'saleItems' => $sale->saleItems->map(function ($saleItem) {
                    return [
                        'id' => $saleItem->id,
                        'plot_id' => $saleItem->plot->id,
                        'project_id' => $saleItem->plot->product->id,
                        'price' => $saleItem->price,
                        'payment_option' => $saleItem->sale_type,
                        'created_at' => $saleItem->created_at,
                    ];
                }),
                'balance' => $sale->getBalance(),
                'is_agreement_approved' => $sale->is_agreement_downloaded,
                'percentage' => $sale->getPercentage(),
                'amountPaid' => $sale->getPaidAmount(),
                'saleAmount' => $sale->getTotalSaleAmount(),
                'commission_at' => $sale->commission_at,
                'installment' => $installment,
                'agent_id' => ucwords(strtolower($sale->soldBy->id)),
                'user_id' => ucwords(strtolower($sale->user->id)),
                '50-percent-at' => $sale->half_payment_at,
                'payments' => $sale->salePayments->map(function ($payment) {
                    return [
                        'id' => $payment->id,
                        'amount' => $payment->amount,
                        'user_id' => $payment->user_id,
                        'payment_method' => $payment->payment_method,
                        'reference_code' => $payment->reference_code,
                        'deposit_at' => $payment->deposit_date,
                        'created_at' => $payment->created_at,
                    ];
                }),
                'uploads' => $sale->saleUploads->map->uploadsToArray(),

            ];

        });
    });

    Route::get('setting', function () {
        return [
            'agreement_body' => Contract::find(2)->value,
            'agreement_first_page' => fanakaConfig('front_page'),
            'vendor' => fanakaConfig('vendor'),
            'director' => fanakaConfig('director'),
            'agreement_footer' => fanakaConfig('agreement_footer'),
        ];
    });

    Route::get('contacts', function () {

        $data = [];

        foreach (Contact::all() as $contact) {

            $data[] = [
                'name' => $contact->name,
                'phone' => $contact->phone,
                'hasvisited' => $contact->hasVisited,
                'user_id' => $contact->user_id,
                'date' => $contact->date,
                'status' => $contact->getMeta("status"),
                'date_collected' => $contact->getMeta("date_collected"),
                'comment' => $contact->getMeta("comment"),
                'created_at' => $contact->created_at,
            ];


        }

        return response()->json($data);
    });
    Route::get('agreements', function () {

        $data = [];
        foreach (PrintContract::all() as $agreement) {

            $data[] = [
                'sale_id' => $agreement->sale_id,
                'user_id' => $agreement->user_id,
                'value' => $agreement->value,
                'created_at' => $agreement->created_at,
            ];
        }
        return response()->json($data);
    });

    Route::get('hold-request', function () {

        $data = [];

        foreach (PlotHoldingRequest::all() as $holdingRequest) {
            $data[] = [
                'agent_id' => $holdingRequest->user_id,
                'plot_id' => $holdingRequest->id,
                'comment' => $holdingRequest->customer_name,
                'phone_number' => $holdingRequest->phone_number,
                'approved_by' => $holdingRequest->approved_by,
                'approved_at' => $holdingRequest->approved_at,
                'release_at' => $holdingRequest->release_at,
            ];

        }

        foreach (Plot::where(['can_sell' => false, 'is_sold' => false])->get() as $plot) {
            if (!in_array($plot->id, $data)) {
                $data[] = [
                    'agent_id' => User::first()->id,
                    'plot_id' => $plot->id,
                    'comment' => "System",
                    'phone_number' => null,
                    'approved_by' => null,
                    'approved_at' => null,
                    'release_at' => now()->addRealMonths(5),
                ];
            }

        }

        return response()->json($data);
    });

});
