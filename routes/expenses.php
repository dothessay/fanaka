<?php
Route::group([
    'prefix' => 'add',
], function () {

    Route::get('/','AddExpensesController@index')->name('expenses_add');

    Route::post('store-petty','AddExpensesController@storePettyCash')->name('expenses_add_store-petty');
});
Route::group([
    'prefix' => 'manage',
], function () {

    Route::get('/','ManageExpensesController@index')->name('expenses_manage');
    Route::post('store','AddExpensesController@store')->name('expenses_manage_add_store');
});