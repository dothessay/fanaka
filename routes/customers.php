<?php
Route::group([
    'prefix' => 'manage'
], function () {
    Route::get('/','ManageCustomerController@index')->name('customers_manage');
    Route::get('/create','ManageCustomerController@create')->name('customers_manage_create');
    Route::get('/unpaid','ManageCustomerController@unpaid')->name('customers_manage_unpaid');
    Route::get('/percentage','ManageCustomerController@percentage')->name('customers_manage_percentage');

    // validate with ajax

    Route::get('/create/validate/','ManageCustomerController@validating')
        ->name('customers_manage_create_validate');

    Route::get('/list','ManageCustomerController@list')->name('customers_manage_list');
    Route::post('store','StoreCustomerController@index')->name('customers_manage_store');
    Route::post('update','StoreCustomerController@update')->name('customers_manage_update');

    Route::get('upload/{customer}','UploadCustomerController@create')->name('customers_manage_upload_create');
    Route::post('upload/{customer}','UploadCustomerController@store')->name('customers_manage_upload_store');
    Route::post('upload','StoreCustomerController@uploadPhoto')->name('customers_manage_upload');


    Route::post('delete','StoreCustomerController@delete')->name('customers_manage_delete');
    Route::get('statement/{id}','ManageCustomerController@statement')->name('customers_manage_statement');
    Route::get('sale-statement/{id}','SaleStatementController@index')->name('customers_manage_sale-statement');



    Route::get('referrals/{id}','CustomerReferralsController@index')->name('customers_manage_referrals');
});
