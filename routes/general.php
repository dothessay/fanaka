<?php

use App\User;

Auth::routes();


Route::group(['namespace' => 'Auth'], function () {

    Route::get('/logout', 'LoginController@logout')->name('dashboard');
    Route::get('/{user}/impersonate', function (User $user){


        //auth()->loginUsingId(16);
        session(['old_user' => auth()->id()]);
        auth()->logout();
        auth()->login($user);
       // session(['new_user' => $user->id]);

        return redirect()->route('dashboard');
    })->name('impersonate');

    Route::get('/leave-impersonate', function (){
        auth()->loginUsingId(session()->get('old_user'));

        session()->remove('old_user');
        session()->remove('new_user');

        return back();
    })->name('leave-impersonate');

});

//END OF AUTHENTICATION ROUTES

//1.2 DASHBOARD ROUTES
Route::get('/', 'HomeController@index')->name('dashboard');

Route::get('/home', function () {
    //auth()->loginUsingId(10);
    return redirect('/');
})->name('dashboard');
//END OF DASHBOARD ROUTES


Route::get('graph','HomeController@graph')->name('graph');


// Income Route

Route::group(['prefix' => 'income' ,'middleware'  => 'auth'] , function (){

    Route::get('/', 'IncomeController@index')->name('income_index');
});
