<?php
Route::group([
    'prefix' => 'charts',
    'namespace' => 'Chart'
], function () {
    Route::get('/','ChartOfAccountController@index')->name('finance_charts_index');
    Route::get('/{id}/show','ChartOfAccountController@show')->name('finance_charts_show');
    Route::get('/{id}/edit','ChartOfAccountController@edit')->name('finance_charts_find');
    Route::patch('/{id}/update','ChartOfAccountController@update')->name('finance_charts_update');

    Route::post('/store-type','ChartOfAccountController@storeType')->name('finance_charts_store-type');
    Route::get('/create-chart','ChartOfAccountController@createChart')->name('finance_charts_create-chart');
    Route::post('/store-chart','ChartOfAccountController@storeChart')->name('finance_charts_store-chart');
    Route::post('/update-type','ChartOfAccountController@updateType')->name('finance_charts_update-type');
    Route::post('/destroy-type','ChartOfAccountController@destroyType')->name('finance_charts_destroy-type');
    Route::get('/list-type','ChartOfAccountController@getType')->name('finance_charts_list-type');
    Route::get('/list-charts','ChartOfAccountController@getCharts')->name('finance_charts_list-charts');
});

Route::group([
    'prefix' => 'expenses',
    'namespace' => 'Expenses'
], function () {
    Route::get('/','ExpensesController@index')->name('finance_expenses_index');
    Route::get('/{id}/show','ExpensesController@showExpenses')->name('finance_expenses_show-expense');
    Route::get('/{id}/show-petty','ExpensesController@showPetty')->name('finance_expenses_show-petty');
    Route::group([
        'prefix' => 'project'
    ], function (){
        Route::post('/store','ExpensesController@store')->name('finance_expenses_project_store');
        Route::get('/download-evidence/{expense}','ExpensesController@downloadEvidence')->name('finance_expenses_project_download-evidence');
    });
});

Route::group([
    'prefix' => 'reports',
    'namespace' => 'Report'
], function () {
    Route::get('/','FinanceReportController@index')->name('finance_reports_index');
    Route::get('analytics','FinanceReportController@analytics')->name('finance_reports_analytics');
    Route::get('collections','FinanceReportController@collections')->name('finance_reports_collection');
});


Route::group([
    'prefix' => 'journal-entry',
    'namespace' => 'Journal'
], function () {
    Route::get('/','JournalEntryController@index')->name('finance_journal-entry_index');
    Route::get('/create','JournalEntryController@create')->name('finance_journal-entry_create');
    Route::post('/store','JournalEntryController@store')->name('finance_journal-entry_store');
});

Route::group([
    'prefix' => 'requisition',
    'namespace' => 'Requisition'
], function () {
    Route::get('/','RequisitionController@index')->name('finance_requisition_index');
    Route::get('/create','RequisitionController@create')->name('finance_requisition_create');
    Route::post('/','RequisitionController@store')->name('finance_requisition_store');
    Route::get('/{requisition}/','RequisitionController@show')->name('finance_requisition_show');
    Route::patch('/{requisition}/approve','RequisitionController@approve')->name('finance_requisition_approve');
    Route::patch('/{requisition}/reject','RequisitionController@reject')->name('finance_requisition_reject');
    Route::patch('/{requisition}/disburse','RequisitionController@disburse')->name('finance_requisition_disburse');
    Route::get('/{requisition}/edit','RequisitionController@edit')->name('finance_requisition_edit');
    Route::patch('/{requisition}/update','RequisitionController@update')->name('finance_requisition_update');
});
