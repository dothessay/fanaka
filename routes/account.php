<?php
Route::group(['prefix' => 'list'], function () {
    Route::get('/','ListAccountController@index')->name('account_list');
    Route::get('/{account}/delete','ListAccountController@delete')->name('account_list_delete');
});
