<?php

return [
    'equity_bank' => [
        'name'    => "EQUITY BANK",
        'number'  => "1440269339566",
        'branch'  => 'EQUITY BANK'
    ],
    'currency' => 'KES. ',
    'currency_string' => 'Kenya Shillings ',
    'sms' => [
        'mis.fanaka.co.ke'
    ],
    /*'contact_sms' => [
        'Dennis' => '254724985367',
        'Moses'  =>  '254721701118',
    ],*/
   'contact_sms' => [
       'Dennis' => '254724985367',
   ],
     'error_email' => [
        'philip'  => 'philip.njuguna@fanaka.co.ke'
    ],
    'admin_emails' => [
        //'Dennis'            => 'dennis@fanaka.co.ke',
        'Philip Njuguna'    => 'philip.njuguna@fanaka.co.ke',
        //'Moses Muriithi'    =>  'kmuriithi@fanaka.co.ke'
    ],
    'upload_notification' => [
        'Tabby'  => 254790662004
    ]
];

