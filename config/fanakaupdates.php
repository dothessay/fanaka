<?php

$start = \Illuminate\Support\Carbon::create(2018,11,30 ,11,01,00,"AFRICA/NAIROBI");
$end =    \Illuminate\Support\Carbon::create(2018,11,30 ,11,01,00,"AFRICA/NAIROBI")->addRealHours(3);
return [

    'updates' => [
        'start' => $start,
        'end' => $end,
        'has_updated' => false,
        'text' =>  "<h6>Note The following</h6>
                        <ul>
                        <li>Before you go the field download the map from the system <br/> Follow the following procedure</li>
                        <ol>
                        <li> >> Click DashBoard</li>
                        <li> >> Available Projects</li>
                        <li> >> Select a Projects</li>
                        <li> >> Click Pull</li>
                        <li> >> Click Map</li>
                        <li> >> print it</li>
</ol>
                        </ul>"
    ]
];
