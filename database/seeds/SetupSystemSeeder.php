<?php

use App\DB\Role\Business;
use Codex\Classes\System;
use Illuminate\Database\Seeder;

class SetupSystemSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $business = factory(Business::class)->make([
            'name' => "Fanaka Real Estate",
            'code' => 'fanaka',
            'slug' => "...A Cut Above The Rest"
        ]);

        $business = Business::add($business);


        System::setupModulesAndComponents();
        System::setupDefaultBusinessPermissions($business);
        System::setupDefaultBusinessRoles($business->code);
        System::setupHQUsers($business->code);
    }
}
