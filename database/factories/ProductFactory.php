<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use Faker\Generator as Faker;

$factory->define(\App\DB\Product\Product::class, function (Faker $faker) {
    return [
        'land_location'  => $faker->name,
        'business_code'   => factory(\App\DB\Role\Business::class)->create()->code,
        'code' => $faker->companySuffix,
        'size'=> $faker->numberBetween(5,10),
        'cost' => $faker->numberBetween(10000000,15000000),
        'title_no'=> $faker->slug('9'),
        'phase' => $faker->numberBetween(1,10),
        'slug' => $faker->slug,
        'user_id'  => factory(\App\User::class)->create()->id

    ];
});
