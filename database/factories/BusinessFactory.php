<?php

use App\DB\Role\Business;
use Faker\Generator as Faker;


$factory->define(Business::class, function (Faker $faker) {

    $company = $faker->unique()->company . $faker->randomLetter . rand(1, 99) . $faker->randomLetter;
    $code = strtolower($faker->word . rand(1, 99) . $faker->randomLetter);

    return [
        'name' => $company,
        'code' => $code,
        'address' => $faker->address,
        'county' => 'Nairobi',
        'logo' => "http://lorempixel.com/200/200/",
        'slug' => $faker->slug,
    ];
});
