<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOfferCustomersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('offer_customers', function (Blueprint $table) {

                $table->increments('id');
                $table->unsignedInteger('offer_letter_id');
                $table->unsignedInteger('customer_id');
                $table->timestamps();
                $table->softDeletes();

                $table->foreign('offer_letter_id')
                    ->references('id')->on('offer_letters')
                    ->onUpdate('cascade')->onDelete('cascade');

                $table->foreign('customer_id')
                    ->references('id')->on('customers')
                    ->onUpdate('cascade')->onDelete('cascade');

            });


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('offer_customers');
    }
}
