<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRolesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('roles', function (Blueprint $table) {
            $table->increments('id');
            $table->string('business_code');
            $table->string('label');
            $table->string('name');
            $table->text('description');
            $table->timestamps();
        });



        Schema::create('role_permissions', function (Blueprint $table) {

            $table->increments('id');
            $table->integer('role_id')->unsigned();
            $table->integer('role_permission_id')->unsigned();
            $table->string('role_permission_type');
            $table->timestamps();

            $table->foreign('role_id')
                ->references('id')->on('roles')
                ->onDelete('cascade')->onUpdate('cascade');

            //$table->unique(['role_id','role_permission_id','role_permission_type'],'role_permission');

        });


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('role_permissions');
        Schema::dropIfExists('roles');
    }
}
