<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePlotSizeSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('plot_size_settings', function (Blueprint $table) {
            $table->increments('id');
            $table->string('business_code');
            $table->string('label');



            $table->timestamps();



            $table->foreign('business_code')
                ->references('code')->on('businesses')
                ->onDelete('cascade')->onUpdate('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('plot_size_settings');
    }
}
