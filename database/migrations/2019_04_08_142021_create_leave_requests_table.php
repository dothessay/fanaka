<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLeaveRequestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('leave_requests', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('leave_type_id');
            $table->unsignedInteger('user_id');
            $table->unsignedInteger('handling_user_id');
            $table->unsignedInteger('handle_clients_user_id')->nullable();
            $table->boolean('handling_user_accepted')->default(false);
            $table->timestamp('last_day')->nullable();
            $table->timestamp('return_at')->nullable();
            $table->integer('days_off');
            $table->text('comments');
            $table->boolean('is_approved')->default(false);
            $table->text('refusal_reason')->nullable();
            $table->text('duties')->nullable();
            $table->string('status')->default('pending');
            $table->unsignedInteger('approved_by')->nullable();
            $table->timestamp('approved_at')->nullable();
            $table->timestamps();



            $table->foreign('leave_type_id')
                ->references('id')->on('leave_types')
                ->onDelete('cascade')->onUpdate('cascade');

            $table->foreign('user_id')
                ->references('id')->on('users')
                ->onDelete('cascade')->onUpdate('cascade');

            $table->foreign('handling_user_id')
                ->references('id')->on('users')
                ->onDelete('cascade')->onUpdate('cascade');

            $table->foreign('handle_clients_user_id')
                ->references('id')->on('users')
                ->onDelete('cascade')->onUpdate('cascade');

            $table->foreign('approved_by')
                ->references('id')->on('users')
                ->onDelete('cascade')->onUpdate('cascade');


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('leave_requests');
    }
}
