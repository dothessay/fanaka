<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSalesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sales', function (Blueprint $table) {
            $table->increments('id');
            $table->string('business_code');
            $table->unsignedInteger('user_id');
            $table->unsignedInteger('agent');
            $table->unsignedInteger('customer_id');
            $table->string('total_amount');
            $table->string('balance');
            $table->boolean('is_approved')->default(false);
            $table->boolean('is_agreement_approved')->default(false);
            $table->boolean('agreement_request')->default(false);
            $table->boolean('title_fee')->default(false);
            $table->unsignedInteger('approved_by')->nullable();
            $table->string('color')->nullable();
            $table->boolean('is_paying')->default(true);

            $table->timestamps();

            $table->foreign('business_code')
                ->references('code')->on('businesses')
                ->onDelete('cascade')->onUpdate('cascade');


            $table->foreign('user_id')
                ->references('id')->on('users')
                ->onDelete('cascade')->onUpdate('cascade');

            $table->foreign('agent')
                ->references('id')->on('users')
                ->onDelete('cascade')->onUpdate('cascade');

            $table->foreign('approved_by')
                ->references('id')->on('users')
                ->onDelete('cascade')->onUpdate('cascade');



            $table->foreign('customer_id')
                ->references('id')->on('customers')
                ->onDelete('cascade')->onUpdate('cascade');


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sales');
    }
}
