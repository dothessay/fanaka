<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInterestSalesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('interest_sales', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('sale_id')->references('id')->on('sales')->onDelete('cascade')->onUpate('cascade');
            $table->timestamp('interest_date')->nullable();
            $table->string('percentage')->default('0.01');
            $table->string('amount');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('interest_sales');
    }
}
