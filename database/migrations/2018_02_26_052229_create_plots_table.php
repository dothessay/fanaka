<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePlotsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('plots', function (Blueprint $table) {
            $table->increments('id');
            $table->string('business_code');
            $table->string('product_code');
            $table->string('plot_no');
            $table->string('title_no')->nullable();
            $table->unsignedInteger('size_id');
            $table->double('price',12,2);
            $table->unsignedInteger('user_id');
            $table->boolean('is_sold')->default(0);
            $table->boolean('is_reserved')->default(0);
            $table->boolean('is_completely_paid')->default(0);
            $table->boolean('can_sell')->default(0);
            $table->timestamps();



            $table->foreign('business_code')
                ->references('code')->on('businesses')
                ->onDelete('cascade')->onUpdate('cascade');

            $table->foreign('product_code')
                ->references('code')->on('products')
                ->onDelete('cascade')->onUpdate('cascade');

            $table->foreign('size_id')
                ->references('id')->on('plot_size_settings')
                ->onDelete('cascade')->onUpdate('cascade');

            $table->foreign('user_id')
                ->references('id')->on('users')
                ->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('plots');
    }
}
