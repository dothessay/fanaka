<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class
CreateBusinessesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('businesses', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('code')->index();
            $table->string('address');
            $table->string('county');
            $table->string('logo');
            $table->string('slug');
            $table->timestamps();
        });



        Schema::create('business_permissions', function (Blueprint $table) {

            $table->increments('id');
            $table->integer('business_id')->unsigned();
            $table->integer('business_permission_id')->unsigned();
            $table->string('business_permission_type');
            $table->timestamps();

            $table->foreign('business_id')
                ->references('id')->on('businesses')
                ->onDelete('cascade')->onUpdate('cascade');

            //$table->unique(['business_id','business_permission_id','business_permission_type'],'business_permission_unique');

        });


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('business_permissions');
        Schema::dropIfExists('businesses');
    }
}
