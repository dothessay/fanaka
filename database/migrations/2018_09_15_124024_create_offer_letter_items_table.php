<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOfferLetterItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('offer_letter_items', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('offer_letter_id');
            $table->unsignedInteger('plot_id');
            $table->double('selling_price' , 12);
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('offer_letter_id')
                ->references('id')->on('offer_letters')
                ->onUpdate('cascade')->onDelete('cascade');



        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('offer_letter_items');
    }
}
