<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateExpensesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('expenses', function (Blueprint $table) {
            $table->increments('id');
            $table->string('business_code');
            $table->double('amount',12,2);
            $table->text('purpose');
            $table->unsignedInteger('user_id');
            $table->timestamps();

            $table->foreign('business_code')
                ->references('code')->on('businesses')
                ->onUpdate('Cascade')->onUpdate('cascade');


            $table->foreign('user_id')
                ->references('id')->on('users')
                ->onUpdate('Cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('expenses');
    }
}
