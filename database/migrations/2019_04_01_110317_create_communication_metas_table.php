<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCommunicationMetasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('communication_metas', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('communication_id');
            $table->unsignedInteger('customer_id');
            $table->string('key')->nullable();
            $table->string('value')->nullable();
            $table->timestamps();




            $table->foreign('customer_id')
                ->references('id')->on('customers')
                ->onDelete('cascade')->oUpdate('cascade');

            $table->foreign('communication_id')
                ->references('id')->on('communications')
                ->onDelete('cascade')->oUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('communication_metas');
    }
}
