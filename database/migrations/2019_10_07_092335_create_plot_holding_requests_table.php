<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePlotHoldingRequestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('plot_holding_requests', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('plot_id')
                ->foreign('plot_id')
                ->references('id')->on('plots')
                ->onDelete('cascade')->onUpdate('cascade');

            $table->unsignedBigInteger('user_id')
                ->foreign('user_id')
                ->references('id')->on('user')
                ->onDelete('cascade')->onUpdate('cascade');

            $table->string('customer_name');
            $table->string('phone_number');
            $table->boolean('is_approved')->default(false);
            $table->unsignedBigInteger('approved_by')
                ->nullable()
                ->foreign('approved_by')
                ->references('id')->on('users')
                ->onDelete('cascade')->onUpdate('cascade');

            $table->timestamp('approved_at')->nullable();
            $table->timestamp('release_at')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('plot_holding_requests');
    }
}
