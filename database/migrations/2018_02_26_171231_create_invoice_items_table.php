<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInvoiceItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('invoice_items', function (Blueprint $table) {
            $table->increments('id');
            $table->string('business_code');
            $table->unsignedInteger('invoice_id');
            $table->unsignedInteger('plot_id');
            $table->double('price', 12, 2);
            $table->string('plot_no');




            $table->timestamps();


            $table->foreign('business_code')
                ->references('code')->on('businesses')
                ->onDelete('cascade')->onUpdate('cascade');

            $table->foreign('invoice_id')
                ->references('id')->on('invoices')
                ->onDelete('cascade')->onUpdate('cascade');

            $table->foreign('plot_id')
                ->references('id')->on('plots')
                ->onDelete('cascade')->onUpdate('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('invoice_items');
    }
}
