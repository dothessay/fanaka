<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterProjectExpenses extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('project_expenses', function (Blueprint $table) {
            $table->unsignedBigInteger('user_id')
                ->nullable()->after('project_id')
                ->foreign('user_id')->references('id')
                ->on('users')->onDelete('cascade')->onUpdate('cascade');
            $table->string('purpose')->after('user_id')->nullable();
            $table->string('amount')->after('purpose')->nullable();
            $table->string('evidence')->after('amount')->nullable();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('project_expenses', function (Blueprint $table) {
            $table->dropColumn('evidence')->nullable();
            $table->dropColumn('amount');
            $table->dropColumn('user_id');
            $table->dropColumn('purpose');
            $table->dropForeign('user_id')->references('id')->on('users')->onDelete('cascade')->onUpdate('cascade');
        });
    }
}
