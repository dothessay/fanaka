<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOfferLettersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('offer_letters', function (Blueprint $table) {
            $table->increments('id');
            $table->string('business_code');
            $table->string('total_amount');
            $table->unsignedInteger('user_id');
            $table->unsignedInteger('agent_id');
            $table->string('payment_option');
            $table->string('cash_option' );
            $table->string('installment_option');
            $table->boolean('is_approved')->default(false);
            $table->boolean('is_converted')->default(false);
            $table->unsignedInteger('approved_by');
            $table->text('note')->nullable();
            $table->string('color')->default('#46bef1');

            $table->unsignedInteger('next_of_kin_id')->nullable();


            $table->timestamps();

            $table->softDeletes();


            $table->foreign('business_code')
                ->references('code')->on('businesses')
                ->onUpdate('cascade')->onDelete('cascade');



            $table->foreign('user_id')
                ->references('id')->on('users')
                ->onUpdate('cascade')->onDelete('cascade');


            $table->foreign('agent_id')
                ->references('id')->on('users')
                ->onUpdate('cascade')->onDelete('cascade');

            $table->foreign('next_of_kin_id')
                ->references('id')->on('next_of_kins')
                ->onUpdate('cascade')->onDelete('cascade');



        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('offer_letters');
    }
}
