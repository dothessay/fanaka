<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMonthlyInstallmentPlansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('monthly_installment_plans', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('sale_id')->foreign('sale_id')
                ->references('id')->on('sales')
                ->onDelete('cascade')->onUpdate('cascade');
            $table->string('month');
            $table->string('day');
            $table->boolean('is_paid')->default(false);
            $table->double('amount', 12,2);
            $table->double('penalty', 12,2);
            $table->timestamps();


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('monthly_installment_plans');
    }
}
