<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTablePlot extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('plots', function (Blueprint $table) {
            $table->string('is_title_issued')->nullable();
            $table->unsignedInteger('issued_by')->nullable();
            $table->timestamp('date_issued')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

        Schema::table('plots', function (Blueprint $table) {
            $table->dropColumn('is_title_issued');
            $table->dropColumn('issued_by');
            $table->dropColumn('date_issued');
        });
    }
}
