<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePlotLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('plot_logs', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('plot_id');
            $table->string('customer_id');
            $table->unsignedInteger('agent_id')->comment('user');
            $table->string('amount_paid');
            $table->string('reservation_type');
            $table->unsignedInteger('user_id');

            $table->timestamps();


            $table->foreign('user_id')
                ->references('id')->on('users')
                ->onDelete('cascade')->onUpdate('cascade');

            $table->foreign('plot_id')
                ->references('id')->on('plots')
                ->onDelete('cascade')->onUpdate('cascade');


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('plot_logs');
    }
}
