<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOfferUploadsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('offer_uploads', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('offer_letter_id');
            $table->string('file_url');
            $table->string('file_name');
            $table->timestamps();

            $table->foreign('offer_letter_id')
                ->references('id')->on('offer_letters')
                ->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('offer_uploads');
    }
}
