<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePrintReservationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('print_reservations', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('offer_letter_id');
            $table->unsignedInteger('user_id');
            $table->text("value");
            $table->timestamps();


            $table->foreign('offer_letter_id')
                ->references('id')->on('offer_letters')
                ->onDelete('cascade')->onUpdate('cascade');

            $table->foreign('user_id')
                ->references('id')->on('users')
                ->onDelete('cascade')->onUpdate('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('print_reservations');
    }
}
