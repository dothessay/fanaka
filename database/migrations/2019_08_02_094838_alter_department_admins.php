<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterDepartmentAdmins extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('department_admins', function (Blueprint $table) {
            $table->unsignedInteger('role_id')->after('user_id')->nullable();
            $table->foreign('role_id')->references('id')
                ->on('roles')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('department_admins', function (Blueprint $table) {
           $table->dropColumn('role_id');
        });
    }
}
