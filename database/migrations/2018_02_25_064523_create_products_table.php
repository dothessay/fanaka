<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');
            $table->string('business_code');
            $table->string('land_location');
            $table->string('code')->index();
            $table->string('size')->nullable();
            $table->string('cost')->nullable();
            $table->string('title_no');
            $table->string('phase');
            $table->boolean('is_inclusive_title')->default(true);
            $table->boolean('display_phase')->default(false);
            $table->string('slug')->nullable();
            $table->unsignedInteger('user_id');

            $table->timestamps();

            $table->foreign('business_code')
                ->references('code')->on('businesses')
                ->onDelete('cascade')->onUpdate('cascade');

            $table->foreign('user_id')
                ->references('id')->on('users')
                ->onDelete('cascade')->onUpdate('cascade');


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
