<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableSaleAddColumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('sales', function (Blueprint $table) {
            $table->boolean('is_agreement_downloaded')->default(0);
            $table->boolean('is_agreement_uploaded')->default(0);
            $table->string('agreement_url')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('sales', function (Blueprint $table) {
            $table->dropColumn('is_agreement_downloaded');
            $table->dropColumn('is_agreement_uploaded');
            $table->dropColumn('agreement_url');

        });
    }
}
