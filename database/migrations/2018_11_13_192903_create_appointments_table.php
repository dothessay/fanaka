<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAppointmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('appointments', function (Blueprint $table) {
            $table->increments('id');
            $table->string('full_name');
            $table->string('phone_number');
            $table->timestamp('date_time')->nullable();
            $table->text('project_ids')->nullable();
            $table->text('note')->nullable();
            $table->boolean('has_visited')->default(false);
            $table->boolean('is_interested')->default(false);
            $table->boolean('send_notification')->default(false);
            $table->unsignedInteger('agent_id');
            $table->unsignedInteger('user_id');
            $table->timestamps();

            $table->foreign('agent_id')
                ->references('id')->on('users')
                ->onDelete('cascade')->onUpdate('cascade');

            $table->foreign('user_id')
                ->references('id')->on('users')
                ->onDelete('cascade')->onUpdate('cascade');


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('appointments');
    }
}
