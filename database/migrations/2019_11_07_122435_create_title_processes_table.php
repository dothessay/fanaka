<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTitleProcessesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('title_processes', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('sale_id')->foreign('sale_id')
                ->references('id')->on('sales')
                ->onUpdate('cascade')->onDelete('cascade');
            $table->timestamp('start_processing_at')->nullable();
            $table->timestamp('received_at')->nullable();
            $table->timestamp('has_issue_at')->nullable();
            $table->timestamp('client_collect_at')->nullable();
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('title_processes');
    }
}
