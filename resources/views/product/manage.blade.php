@extends('layouts.master')
@section('title','Manage Projects')
@section('content')

    @can('access-module-component-functionality', 'projects_manage_add')
        <ul class="nav nav-pills">
            <li class="active">
                <a  data-toggle="modal" data-target="#addProduct">Add Project</a>
            </li>
            <li class="active">
                <a onclick="window.history.back()" style="color:white;">Back</a>
            </li>
        </ul>
    @endcan



    @component('layouts.partials.panel')

        @slot('heading',' ')

        <div class="table-responsive">
            <table class="table-bordered table-striped table" id="data-table" width="100%">
                <thead>
                <tr>
                    <th>Date</th>
                    <th>Land Location</th>
                    <th>Cost</th>
                    <th>Size</th>
                    <th>Land Title No</th>
                    <th>Map</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody>
                @foreach($products as $product)
                    <tr>
                        <td>{{ $product->created_at }}</td>
                        <td><a href="{{url('projects/manage/details/'.$product->id)}}"> {{$product->name()}}</a></td>
                        <td>{{ number_format((double)$product->cost , 2)}}</td>
                        <td>{{ $product->size }}</td>
                        <td>{{ $product->title_no }}</td>
                        <td> @if($product->getMeta('map', false))
                                 <a href="{{ route('projects_manage_map_download', ['product' => $product->id]) }}">Download</a>
                            @else No Map @endif</td>
                        <td>
                            @component('layouts.button')
                                @can('access-module-component-functionality','projects_manage_update')
                                    <li><a data-toggle="modal" data-target="#update-product" id="edit"
                                           data-name="{{$product->land_location}}"
                                           data-cost="{{number_format((double)$product->cost , 2)}}"
                                           data-size="{{explode(' ', $product->size)[0]}}"
                                           data-titleno="{{$product->title_no}}"
                                           data-phase="{{$product->phase}}"
                                           data-id="{{$product->id}}">Update</a>
                                    </li>

                                    <li>
                                        <a href="{{route('projects_manage_pricing', ['product'  => $product->id])}}">Add Pricing</a>
                                    </li>

                                    <li>
                                        @if($product->getMeta('map', false))

                                            <a href="{{route('projects_manage_map_delete', ['product'  => $product->id])}}">Delete Map</a>
                                        @else

                                            <a href="{{route('projects_manage_map', ['product'  => $product->id])}}">Upload Map</a>
                                        @endif
                                    </li>

                                @endcan

                                @can('access-module-component-functionality','projects_manage_delete')
                                    <li><a data-toggle="modal" data-target="#delete-product" id="code"
                                           data-code="{{$product->code}}">Delete</a></li>
                                @endcan
                            @endcomponent
                        </td>
                    </tr>
                @endforeach

                </tbody>


            </table>
        </div>

    @endcomponent

    @component('layouts.partials.modal')
        @slot("id","addProduct")
        @slot('dialogClass','modal-lg')
        @slot('title')<h6>
            Fill the form below to add a mother plot
        </h6>@endslot


        <form class="form-horizontal form-bordered productForm" method="post" action="{{route('projects_manage_store')}}">
            {{csrf_field()}}
            <div class="form-group">
                <label class="control-label col-md-2">Land Location <span class="required-uzapoint">*</span> </label>
                <div class="col-md-4">
                    <input type="text" class="form-control" name="land_location" placeholder="Kamulu Phase 5">
                </div>

                @can('access-module-component-functionality','projects_manage_add-buying-price')

                <label class="control-label col-md-2">Buying Price <span class="required-uzapoint">*</span> </label>
                <div class="col-md-4">
                    <input type="text" class="form-control" name="cost" placeholder="500000">
                </div>

                    @endcan
            </div>


            <div class="form-group">

                <label class="control-label col-md-2">Land Title No <span class="required-uzapoint">*</span> </label>
                <div class="col-md-4">
                    <input type="text" class="form-control" name="title_no" placeholder="Mavoko/Oldonyosambuku 201/209">
                </div>


                <label class="control-label col-md-2"> Phase <span class="required-uzapoint">*</span> </label>
                <div class="col-md-4">
                    <input type="text" class="form-control" name="phase" placeholder="1">
                </div>






            </div>


            <div class="form-group">


                <label class="control-label col-md-2"> Display Phase <span class="required-uzapoint">*</span> </label>
                <div class="col-md-4">
                    <input type="checkbox" class="checkbox" name="display_phase" placeholder="1">
                </div>


            </div>


            <div class="form-group">



                @can('access-module-component-functionality','projects_manage_add-buying-price')

                <label class="control-label col-md-2">Size</label>
                <div class="col-md-4">
                    <input type="text" class="form-control" name="size" placeholder="4 acres">
                </div>

                <label class="control-label col-md-1">&nbsp;</label>
                <div class="col-md-4">
                    <select name="size_type" class="form-control" >
                        <option>Acres</option>
                        <option>Hectares</option>
                    </select>
                </div>


                @endcan


                <label class="control-label col-md-2">No Of Plots</label>
                <div class="col-md-2">
                    <input type="number" class="form-control" id="no_of_plot" name="no_of_plots">
                </div>

                <label class="control-label col-md-2">Price</label>
                <div class="col-md-2">
                    <input type="number" class="form-control" id="child_price" name="price">
                </div>




            </div>



            <div class="form-group">
                <label class="control-label col-md-2">&nbsp;</label>
                <div class="col-md-2">
                <button type="button" class="btn btn-sm btn-primary" id="add_plots">Add Plots</button>
            </div>
            </div>

            <div class="form-group">
                <div class="add_plots">

                </div>
            </div>


            <div class="modal-footer">
                <button type="button" class="btn btn-success addProject">Submit</button>
            </div>
        </form>

    @endcomponent

    <!-- edit product -->
    @component('layouts.partials.modal')
        @slot("id","update-product")
        @slot('dialogClass','modal-lg')
        @slot('title')<h6>
            Update
        </h6>@endslot


        <form class="form-horizontal updateProductForm form-bordered" method="post" id="productEdit"
              action="{{route('projects_manage_update')}}">
            {{csrf_field()}}


            <div class="form-group">
                <label class="control-label col-md-2">Land Location <span class="required-uzapoint">*</span> </label>
                <div class="col-md-4">
                    <input type="text" class="form-control" name="land_location" id="name" placeholder="Kamulu Phase 5">
                </div>

                @can('access-module-component-functionality','projects_manage_add-buying-price')
                <label class="control-label col-md-2">Buying Price <span class="required-uzapoint">*</span> </label>
                <div class="col-md-4">
                    <input type="text" class="form-control" name="cost" id="cost" placeholder="500000">
                </div>
                @endcan
            </div>


            <div class="form-group">

                <label class="control-label col-md-2">Land Title No <span class="required-uzapoint">*</span> </label>
                <div class="col-md-4">
                    <input type="text" class="form-control" id="title_no" name="title_no" placeholder="title #3892-K">
                </div>


                <label class="control-label col-md-2"> Phase <span class="required-uzapoint">*</span> </label>
                <div class="col-md-4">
                    <input type="text" class="form-control" id="phase" name="phase" placeholder="1">
                </div>



            </div>


            <div class="form-group">


                @can('access-module-component-functionality','projects_manage_add-buying-price')



                <label class="control-label col-md-2">Size</label>
                <div class="col-md-4">
                    <input type="text" class="form-control" id="size" name="size" placeholder="4 acres">
                </div>

                <label class="control-label col-md-1">&nbsp;</label>
                <div class="col-md-4">
                    <select name="size_type"  id="type" class="form-control selectpicker" data-live-search="true" data-size="5">
                        <option>Acres</option>
                        <option>Hectares</option>
                    </select>
                </div>

                @endcan



            <input type="hidden" name="id" id="id">
            <div class="modal-footer">
                <button type="button" id="editProduct" class="btn updateProductButton btn-success">Edit</button>
            </div>
        </form>

    @endcomponent
    <!-- end edit product -->

    @component('layouts.partials.modal')
        @slot('id','delete-product')
        @slot('title')
            <h6></h6>
        @endslot

        <p>
        <h5>ARE YOU YOU WANT TO DELETE THIS MOTHER PRODUCT / PLOT </h5>
        <h6>The following will be affected</h6>
        <ul>
            <li>All child plots will lost, Proceed if you are very sure</li>
        </ul>

        <form method="post" action="{{route('projects_manage_delete')}}">
            {{csrf_field()}}
            <input type="hidden" id="plot_code" name="code">


            <div class="modal-footer">
                <button type="submit" class="btn btn-danger">Delete</button>
            </div>
        </form>

        </p>
    @endcomponent


@endsection

@section('extra_js_footer')
    @include('layouts.dataTables.datatable')
    <script>
        $("#data-table").DataTable({
            sort:false
        });

        $(document).on('click', 'a#code', function () {

            var code = $(this).attr('data-code')

            $("#plot_code").val(code)
        });

        $(document).ready(function () {
            $('#data-table').on('click', '#edit', function () {
                $("#productEdit #name").val($(this).data('name'));
                $("#productEdit #cost").val($(this).data('cost'));
                $("#productEdit #title_no").val($(this).data('titleno'));
                $("#productEdit #phase").val($(this).data('phase'));
                $("#productEdit #size").val($(this).data('size'));
                $("#productEdit #id").val($(this).data('id'));
            });


        });

        $(document).on('click','button#add_plots', function () {

            $(".add_plots").html('');

            var noOfPlots = $("#no_of_plot").val();

            var tableBody = '<tbody>';

            var plotSizes = '@foreach($sizes as $size)<option value="{{$size->id}}">{{$size->label}}</option>@endforeach';


            for (var i = 1 ; noOfPlots >=  i ; i++){

                tableBody += '<tr>' +
                    '<td><input type="hidden" name="child_plot_no[]" value="'+i+'">'+i+'</td>' +
                    '<td><input type="text" name="child_price[]" value="'+parseFloat($('#child_price').val()).toFixed(2)+'" class="col-md-12 form-control"></td>' +
                    '<td><select name="size_id[]" class="form-control">'+plotSizes+'</select></td>' +
                    '<td><input type="text" name="child_title[]" class="col-md-3 form-control"></td>' +
                    '</tr>'
            }

            tableBody += '</tbody>'

            var tableDetails = '<table class="table table-bordered table-stripped">' +
                '<thead>' +
                '<tr>' +
                '<th>PLot NO</th>' +
                '<th>Selling Price</th>' +
                '<th>Size</th>' +
                '<th>Parcel No</th>' +
                '</tr>' +
                '</thead>' +
                    tableBody +
                '</table>'

            $(".add_plots").append(tableDetails)


        })

        $(document).on('click', 'button.addProject', function () {

            $(this).attr('disabled', true).text('submitting .....')

            let formData = new FormData($("form.productForm")[0])
            axios.post('/projects/manage/store', formData)
                .then((res) => {

                    $(this).attr('disabled', false).text('Submit') ;
                    $.growl.notice({
                        message: res.data.message
                    })

                    $('.modal').modal('hide');

                }).catch((err) => {

                $(this).attr('disabled', false).text('Submit')
                    if(err.response.status === 422) {

                        $.each(err.response.data.message, (values , errors) => {

                            $.growl.warning({
                                message: errors
                            })
                        })
                        return
                    }

                $.growl.warning({
                    message: err.response.data.message
                })



            })

        });
        $(document).on('click', 'button.updateProductButton', function () {

            $(this).attr('disabled', true).text('updating .....')

            let formData = new FormData($("form.updateProductForm")[0])
            axios.post('/projects/manage/update', formData)
                .then((res) => {

                    $(this).attr('disabled', false).text('Update') ;
                    $.growl.notice({
                        message: res.data.message
                    })

                    $('.modal').modal('hide');

                }).catch((err) => {

                $(this).attr('disabled', false).text('Update')
                    if(err.response.status === 422) {

                        $.each(err.response.data.message, ( values, errors) => {

                            $.growl.warning({
                                message: errors
                            })
                        })
                        return
                    }

                $.growl.warning({
                    message: err.response.data.message
                })



            })

        });

    </script>
@endsection
