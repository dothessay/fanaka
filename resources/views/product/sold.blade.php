@extends('layouts.master')
@section('title','Sold plots for project '. $product->name)
@section('content')

    @include('layouts.back')

    @component('layouts.partials.panel')



        <table class="table-bordered table-striped table" id="data-table" width="100%" cellspacing="2" cellpadding="2">
            <thead>
            <tr>
                <th>Date Sold</th>
                <th>Price</th>
                <th>Paid Amount</th>
                <th>Balance</th>
                <th>Size</th>
                <th>Plot No</th>
                <th>Parcel No</th>
                <th>Customer</th>
                <th>Agent By</th>
            </tr>
            </thead>
            <tbody> <?php $paid = 0; $balance = 0 ; $sellingPrice = 0;?>
            @foreach($plots as $plot)
                @if(isset($plot->saleItem))
                    <tr>
                        <td>{{ $plot->saleItem->created_at }}</td>
                        <td>{{ number_format($plot->saleItem->sale->total_amount , 2) }}</td>
                        <td>{{ number_format($plot->saleItem->sale->getPaidAmount(), 2) }}</td>
                        <td>{{ number_format($plot->saleItem->sale->balance, 2) }}</td>
                        <td>{{ $plot->size->label }}</td>
                        <td><a href="{{ url('plots/manage/details/'. $plot->id) }}">{{ $plot->plot_no }} </a> </td>
                        <td>{{ $plot->title_no }}</td>
                        <td>{!! $plot->getCustomers() !!}</td>
                        <td>{{ $plot->saleItem->sale->soldBy->fullName() }}</td>
                    </tr>
                @endif
                @endforeach

            </tbody>


        </table>

    @endcomponent

@endsection

@section('extra_js_footer')
    @include('layouts.dataTables.tablebuttons')
    @include('layouts.dataTables.buttons')

@endsection
