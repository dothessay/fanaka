@extends('layouts.master')
@section('title','Report By Availability')

@section('content')

    @include('layouts.back')
    @component('layouts.partials.panel')

        @slot('class','')
        @slot('heading','')

        <form class="form-horizontal">
            <div class="form-group">
                <label class="col-md-2">Project </label>


                <div class="col-md-4">

                    <select class="form-control selectpicker" name="product_code" data-live-search="true">

                       @if(isset($_GET['product_code']))
                            <option data-display="none" value="{{@$_GET['product_code']}}" selected>{{@$projects->where('code', @$_GET['product_code'])->first()->name()}}</option>

                        @endif

                        @foreach($projects as $project)

                            <option value="{{$project->code}}">{{$project->name()}}</option>

                        @endforeach

                    </select>

                </div>
            </div>


            <div class="modal-footer">
                <button type="submit" class="btn btn-success">Pull</button>
                <input type="submit" name="pdf" value="Print" class="btn btn-primary">
            </div>

        </form>



        @include('product.report.available._available_data')
    @endcomponent


@endsection



@section('extra_js_footer')
    @include('layouts.dataTables.datatable')
    @include('layouts.dataTables.buttons')
    @include('layouts.dataTables.tablebuttons')
    @include('layouts.datePicker.datepicker')

    <script>
        $(function () {

            $('#available-table').DataTable({
                sort:false,
                dom: 'Bfrtip',
                buttons: [
                    {
                    extend: "csv",
                    className: "btn-sm",
                    exportOptions: {
                        columns: 'th:not(:last-child)'
                    }
                }, {
                    extend: "excel",
                    className: "btn-sm",
                    exportOptions: {
                        columns: 'th:not(:last-child)'
                    }
                }, {
                    extend: "pdf",
                    className: "btn-sm",
                    exportOptions: {
                        columns: 'th:not(:last-child)'
                    }
                },],
                responsive: !0
            })


            $(".date").datepicker();
        })
    </script>


@endsection
