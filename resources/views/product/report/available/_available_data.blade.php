<div class="table-responsive-lg">

    @if($plots->count())
        <ul class="nav nav-pills">
            <li class="active">

                <a href="{{route('projects_reports_map' ,['projectId' => $plots->first()->product->id])}}">Map</a>
            </li>
        </ul>
    @endif
    <table class="table-bordered table-striped table" id="available-table" width="100%" cellspacing="2" cellpadding="2">
        <thead>
        <tr>

            <th>Land Location</th>
            <th>Plot No</th>
            <th>Size</th>
            <th>Status</th>
            <th>Price</th>

        </tr>
        </thead>
        <tbody>
        @foreach($plots as $plot)
            <tr>


                <?php

                if (!$plot->is_reserved && !$plot->is_sold && $plot->can_sell) {

                ?>
                <td><a href="{{url('sale/manage/create-sale/'.$plot->id)}}">{{$plot->product->name()}}</a></td><?php
                } else {
                ?>
                <td>{{$plot->product->name()}}</td>
                <?php


                } ?>


                <td>{{$plot->plot_no}}</td>
                    <td>{{$plot->size->label}}</td>
                <td>{!! $plot->getStatus() !!}</td>

                <td>{{number_format($plot->price , 2)}}</td>


            </tr>
        @endforeach

        </tbody>


    </table>

</div>
