
@if(isset($project))
    <h2 style="text-align: center; text-transform: uppercase">{{$project->name()}} Below 50%</h2>
    @endif

    <table class="table table-bordered table-striped nowrap" width="100%" cellspacing="0" cellpadding="0" id="data-table">
        <thead>
        <tr>
            <th>Land Location</th>
            <th>Price</th>
            <th>Size</th>
            <th>Plot No</th>
            <th>Parcel No</th>
            <th>Customer</th>
            <th>% Paid</th>
            <th>Amount</th>
            <th>Balance %</th>
            <th>Balance Amount</th>
        </tr>
        </thead>
        <tbody>
        @foreach($plots as $plot)
            <tr>
                <td>{{$plot->name()}}</td>
                <td>{{number_format($plot->saleItem->price , 2)}}</td>
                <td>{{$plot->size->label}}</td>
                <td>{{$plot->plot_no}}</td>
                <td>{{$plot->title_no}}</td>
                <td>{!! $plot->getCustomers() !!}</td>
                <td>{!! number_format($plot->getPercentage() , 2)  !!} %</td>
                <td>{!! number_format(floatval($plot->getAmountPaid()) , 2) !!} </td>
                <td>{!! number_format(100 - $plot->getPercentage() , 2)  !!} %</td>
                <td>{!! number_format(floatval($plot->getSaleBalance()) , 2)!!} </td>
            </tr>
        @endforeach
        </tbody>
    </table>
