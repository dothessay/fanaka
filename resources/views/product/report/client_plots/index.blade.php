@extends('layouts.master')

@section('title')
    All Plots with Clients
@endsection

@section('content')

    <div class="container-fluid">

        <form class="form-horizontal">
            <div class="form-group">
                <label class="col-md-2 control-label">Project</label>
                <div class="col-md-4">
                    <select name="project" class="form-control selectpicker" data-live-search="true">
                        @foreach($projects as $product)
                            <option value="{{$product->slug}}" @if(isset($project)) @if($product->name() === $project->name()) selected @endif @endif>{{$product->name()}}</option>
                        @endforeach
                    </select>
                </div>
                <label class="col-md-1 control-label">&nbsp;</label>
                <div class="col-md-2">
                    <input type="submit" name="pull" value="Get Report" class="btn btn-primary">

                </div>
                <label class="col-md-1 control-label">&nbsp;</label>
                <div class="col-md-4">
                    <input type="submit" name="pdf" value="Get Pdf" class="btn btn-default">
                    <input type="submit" name="excel" value="Download Excel" class="btn btn-warning">
                </div>
            </div>

        </form>


        @component('layouts.partials.panel')
            <div class="table-responsive">

                @include('product.report.client_plots._data')
            </div>

        @endcomponent
    </div>

@endsection


@section('extra_js_footer')
    @include('layouts.dataTables.datatable')
    <script>
        $("table#data-table").DataTable({
            sort: false
        })
    </script>
@endsection


