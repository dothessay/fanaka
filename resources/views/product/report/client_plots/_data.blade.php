
@if(isset($project))

    <h4  style="text-align: center"><b>All Plots with our clients  for project {{$project->name()}}</b></h4>

    @endif


<table class="table-responsive table-striped table-bordered table table-condensed" id="data-table">
    <thead>
    <tr>
        <th>Date Reserved/First Installment</th>
        <th>Customer</th>
        <th>Phone Number</th>
        <th>PlotNo</th>
        <th>Amount Paid</th>
        <th>Balance</th>
        <th>Agent</th>
        <th>Type</th>

    </tr>
    </thead>
    <tbody>
    @foreach($sales as  $sale)
        <tr>
            <td width="10%">{!! $sale['date'] !!}</td>
            <td width="10%">{!! $sale['customer'] !!}</td>
            <td width="10%">{!! $sale['phone_number']!!}</td>
            <td width="10%">{!! $sale['plotNo'] !!}</td>
            <td width="10%">{!! number_format($sale['amount'], 2) !!}</td>
            <td width="10%">{!! number_format($sale['balance'], 2) !!}</td>
            <td width="10%">{!! $sale['agent'] !!}</td>
            <td width="10%">{!! $sale['type'] !!}</td>

        </tr>
    @endforeach
    </tbody>
</table>
