<table class="table-responsive table-striped table-bordered table table-condensed" id="data-table">
    <thead>
    <tr>
        <th>Reference</th>
        <th>Sale Type</th>
        <th>Customer</th>
        <th>Amount Paid</th>
        <th>Plot</th>
        <th>Date Registered</th>
    </tr>
    </thead>
    <tbody>
    @foreach($sales as  $sale)
        <tr>
            <td width="5%">{{ ($sale['ref'])  }}</td>
            <td width="5%">{{ ($sale['type'])  }}</td>
            <td width="20%">{!! $sale['customer'] !!}</td>
            <td width="10%">{!! $sale['amount'] !!}</td>
            <td width="40%">{!! $sale['plot'] !!}</td>
            <td width="20%">{!! $sale['date'] !!}</td>
        </tr>
    @endforeach
    </tbody>
</table>