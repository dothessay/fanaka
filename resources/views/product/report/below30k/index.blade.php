@extends('layouts.master')

@section('title')
    Below 30k paid projects
    @endsection

@section('content')

    <div class="container-fluid">
        <form>
            <input type="submit" class="btn btn-primary" name="print" value="print pdf">
        </form>

        <hr>


        @component('layouts.partials.panel')
            <div class="table-responsive">

                @include('product.report.below30k._data')
            </div>

        @endcomponent
    </div>

@endsection


@section('extra_js_footer')
    @include('layouts.dataTables.datatable')
    <script>
        $("table#data-table").DataTable({
            sort: false
        })
    </script>
@endsection


