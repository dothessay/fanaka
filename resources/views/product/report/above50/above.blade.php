@extends('layouts.master')

@if($project)

    @section('title','Shows Report all project paid that have been  above 50 %  for '. $project->name())
@endif

@section('content')

    <ul class="nav nav-pills">
        <li class="active">
            <a onclick="window.history.back()" style="color:white;">Back</a>
        </li>
    </ul>

    @component('layouts.partials.panel')




        <div class="table-responsive">

            <form class="form-horizontal">
                <div class="form-group">
                    <label class="col-md-2 control-label">Project</label>
                    <div class="col-md-4">
                        <select name="project" class="form-control selectpicker" data-live-search="true">
                            @foreach($projects as $product)
                                <option value="{{$product->slug}}" @if(isset($project)) @if($product->name() === $project->name()) selected @endif @endif>{{$product->name()}}</option>
                            @endforeach
                        </select>
                    </div>
                    <label class="col-md-1 control-label">&nbsp;</label>
                    <div class="col-md-2">
                        <input type="submit" name="pull" value="Get Report" class="btn btn-primary">

                    </div>
                    <label class="col-md-1 control-label">&nbsp;</label>
                    <div class="col-md-2">
                        <input type="submit" name="pdf" value="Get Pdf" class="btn btn-default">
                    </div>
                </div>

            </form>

            @include('product.report.above50.data')
        </div>
    @endcomponent
@endsection


@section('extra_js_footer')

    @include('layouts.form')
    @include('layouts.dataTables.tablebuttons')
    <script>
        $("#data-table").DataTable({
            sort: false,
            paginate:false
        })
    </script>
@endsection