<table class="table-responsive table-striped table-bordered table table-condensed" id="data-table">
    <thead>
    <tr>
        <th>Agent</th>
        <th>Customer</th>
        <th>Phone</th>
        <th>Amount Paid</th>
        <th>Balance</th>
        <th>% Paid</th>
        <th>Plot</th>
        <th>Date Registered</th>
        <th>Last Installment Amount</th>
        <th>Last Installment Date</th>
        <th>Due Date</th>
    </tr>
    </thead>
    <tbody>
    <?php $totalBalance =  0 ;?>
    @foreach($sales as  $sale)
        <?php $totalBalance +=  $sale['balance'] ;?>
        <tr>
            <td style="background: {{ $sale['color'] }}; ">{!! $sale['agent'] !!}</td>
            <td style="background: {{ $sale['color'] }}; ">{!! $sale['customer'] !!}</td>
            <td style="background: {{ $sale['color'] }}; ">{!! $sale['phone_number'] !!}</td>
            <td style="background: {{ $sale['color'] }}">{!! number_format($sale['amount'], 2) !!}</td>
            <td style="background: {{ $sale['color'] }}">{!! number_format($sale['balance']  , 2) !!}</td>
            <td style="background: {{ $sale['color'] }}">{!! (floatval(number_format($sale['percentage'] , 2))) !!} % </td>
            <td style="background: {{ $sale['color'] }}">{!! $sale['plot'] !!}</td>
            <td style="background: {{ $sale['color'] }}">{!! $sale['date'] !!}</td>
            <td style="background: {{ $sale['color'] }}">{!!  number_format($sale['last_amount']  , 2)  !!}</td>
            <td style="background: {{ $sale['color'] }}">{!! $sale['last_date'] !!}</td>
            <td style="background: {{ $sale['color'] }}">{!! $sale['completion_date'] !!}</td>
        </tr>
    @endforeach
    </tbody>
    <tfoot>
    <tr>
        <td>Total:</td>
        <td></td>
        <td></td>
        <td></td>
        <td>{{ number_format(floatval($totalBalance) , 2) }}</td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
    </tr>
    </tfoot>
</table>
