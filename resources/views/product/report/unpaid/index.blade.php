@extends('layouts.master')

@section('title')
    All Un Paid Plots
@endsection

@section('content')

    <div class="container-fluid">
        <div class="row">
            key: <span style=" background: orange"> : Represent </span><b>Defaulters</b>
        </div>

        <form class="form-horizontal">
            <div class="form-group">
                <label class="col-md-2 control-label">Project</label>
                <div class="col-md-4">
                    <select name="project" class="form-control selectpicker" data-live-search="true">
                        <option>All</option>
                        @foreach($projects as $product)
                            <option value="{{$product->id}}" @if(isset($project)) @if($product->name() === $project->name()) selected @endif @endif>{{$product->name()}}</option>
                        @endforeach
                    </select>
                </div>
            </div>

            <div class="form-group">
                <label class="col-md-2 control-label">Above %</label>
                <div class="col-md-4">
                    <select name="rate" class="form-control selectpicker" data-live-search="true">
                        <option data-hidden="true">All</option>
                        @foreach([0,10,20,30,40,50,60,70,80,90,100] as $rate)
                            <option value="{{ $rate }}" @if(request()->has('rate') && request('rate')  == $rate) selected @endif >{{ $rate }} %</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-2">Past Due date</label>
                <div class="col-md-4">
                    <input
                            type="checkbox"
                            name="past_due_date"
                           @if(isset($_GET['past_due_date']) ?  $_GET['past_due_date'] : false)  checked @endif
                    >
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-1 control-label">&nbsp;</label>
                <div class="col-md-6">
                    <input type="submit" name="pull" value="Get Report" class="btn btn-primary">

                    <input type="submit" name="pdf" value="Get Pdf" class="btn btn-danger">
                    <input type="submit" name="excel" value="Excel" class="btn btn-warning">
                </div>
            </div>

        </form>


        @component('layouts.partials.panel')
            <div class="table-responsive">

                @include('product.report.unpaid._data')
            </div>

        @endcomponent
    </div>

@endsection


@section('extra_js_footer')
    @include('layouts.dataTables.datatable')
    <script>
        $("table#data-table").DataTable({
            sort: false
        })
    </script>
@endsection


