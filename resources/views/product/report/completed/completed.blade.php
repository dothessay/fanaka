@extends('layouts.master')
@section('title','Completed')

@section('content')

    @include('layouts.back')

    @component('layouts.partials.panel')



        <form class="form-horizontal">
            <div class="form-group">
                <label class="col-md-2">Project </label>


                <div class="col-md-4">

                    <select class="form-control selectpicker" name="product_code" data-live-search="true">

                        @if(isset($_GET['product_code']))
                            <option data-display="none" value="{{@$_GET['product_code']}}" selected>{{@$projects->where('code', @$_GET['product_code'])->first()->name()}}</option>

                        @endif

                        @foreach($projects as $product)

                            <option value="{{$product->code}}"
                            @if(isset($project) && $project->id === $product->id) selected @endif
                            >{{$product->name()}}</option>

                        @endforeach

                    </select>

                </div>
            </div>


            <div class="modal-footer">
                <button type="submit" class="btn btn-success">Pull</button>
                <input type="submit" name="pdf" value="Print" class="btn btn-primary">
            </div>

        </form>



        @include('product.report.completed.data')
    @endcomponent


@endsection



@section('extra_js_footer')
    @include('layouts.dataTables.datatable')
    @include('layouts.datePicker.datepicker')

    <script>
        $(function () {

            $('#data-table').DataTable({
                sort:false
            })


            $(".date").datepicker();
        })
    </script>


@endsection