@extends('layouts.master')
@section('title','Upload map for '. $product->name())

@section('content')


@component('layouts.partials.panel')
    <form
            class="form-horizontal form-bordered"
            method="post"
            enctype="multipart/form-data"
            action="{{ route('projects_manage_map_store', ['product'  => $product->id]) }}"
    >
        {{csrf_field()}}


        <div class="form-group">
            <label class="control-label col-md-1">Select Map</label>
            <div class="col-md-4">
                <input
                        type="file"
                        name="map"
                        class="form-control"
                >
            </div>
        </div>
        <div class="email-footer">
            <button
                    class="btn btn-success">
                <i class="fa fa-cloud-upload"></i>
                Upload
            </button>
        </div>

    </form>
  @endcomponent


@endsection
