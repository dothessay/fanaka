@extends('layouts.master')
@section('title','Showing plots for project '. $product->name)
@section('content')


    <div class="row">
        {!! statisticWidget('No of Available Plots', $product->availablePlots() , url('projects/manage/available/'.$product->slug)) !!}
        {!! statisticWidget('No Of Held Plots', $product->getHeldPlots()->count() , route('projects_manage_hold', ['product' => $product->id])) !!}
        {!! statisticWidget('No of Plots Sold', $product->soldPlots() - $product->getHeldPlots()->count() , url('projects/manage/sold/'.$product->slug)) !!}
    </div>

    @include('layouts.back')

    @component('layouts.partials.panel')



        <table class="table-bordered table-striped table" id="data-table" width="100%" cellspacing="2" cellpadding="2">
            <thead>
            <tr>
                <th></th>
                <th>Date</th>
                <th>Land Location</th>
                <th>Price</th>
                <th>Size</th>
                <th>Plot No</th>
                <th>Parcel No</th>
                <th>Status</th>
                <th>Added By</th>
            </tr>
            </thead>
            <tbody>
            @foreach($product->plots as $plot)
                <tr>

                    <td><input type="checkbox" class="switchery mark_canSell" name="can_sell" value="{{$plot->id}}" @if($plot->can_sell) checked @endif></td>
                    <td>{{$plot->created_at}}</td>
                    <?php

                    if ($plot->available()) {

                        ?>
                    <td><a href="{{url('sale/manage/create-sale/'.$plot->id)}}">{{$plot->product->name()}}</a></td><?php
                    } else {
                        ?>
                    <td>{{$plot->product->name()}}</td>
                    <?php


                    } ?>

                    <td>{{number_format($plot->price , 2)}}</td>
                    <td>{{$plot->size->label}}</td>

                    <td>{{$plot->plot_no}}</td>
                    <td>{{$plot->title_no}}</td>
                    <td>
                        <?php
                        $class = "primary";
                        $isSold = "Available";

                        if ($plot->is_reserved)
                            {
                                $isSold = "Reserved";
                                $class = "danger";
                            }



                        if ($plot->is_sold) {

                            $class = "warning";
                            $isSold = "Sold ( Payment in Progress)";
                        }


                        if ($plot->is_completely_paid) {

                            $class = "success";
                            $isSold = "Sold Out";
                        }

                        ?>

                        {!! $plot->getStatus() !!}
                    </td>

                    <td>{{$plot->user->fullName()}}</td>



                </tr>
            @endforeach

            </tbody>


        </table>

    @endcomponent

@endsection

@section('extra_js_footer')
    @include('layouts.dataTables.tablebuttons')
    @include('layouts.dataTables.buttons')

@endsection
