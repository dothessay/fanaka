@extends('layouts.master')
@section('title','Project Expenses')
@section('content')

    <ul class="nav nav-pills">
        <li class="active">
            <a class="btn btn-info" data-toggle="modal" data-target="#addProjectExpense">Add Expenses</a>
        </li>
    </ul>
   

    @component('layouts.partials.panel')

        @slot('heading',' ')

        <div class="table-responsive-sm">
            <table class="table-bordered table-striped table" id="data-table" width="100%">
                <thead>
                    <tr>
                        <th> Project </th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                @foreach($projectexpenses as $projectexpense)
                    <tr>
                        <td> {{ $projectexpense->projects->land_location }} </td>
                        <td>
                            @component('layouts.button')
                <li><a id="expenses" data-toggle="modal" data-id ="{{ $projectexpense->id }}"  data-project="{{ $projectexpense->projects->land_location }}" data-target="#projectExpenses" data-project-id="{{ $projectexpense->project_id }}">View Expenses</a></li>
                            @endcomponent
                        </td>
                    </tr>
                @endforeach

                </tbody>
            </table>
        </div>

    @endcomponent

    @component('layouts.partials.modal')
        @slot("id","addProjectExpense")
        @slot('dialogClass','modal-lg')
        @slot('title')<h6>
            Fill the form below to add an expense
        </h6>@endslot
        <form class="form-horizontal form-bordered" method="post" action="{{route('projects_expenses_add')}}">
            {{csrf_field()}}
            <div class="form-group">
                <label> Project <span class="required-uzapoint">*</span> </label>
                <div>
                    <select name="project_id" id="" class="form-control selectpicker" data-live-search="true" data-size="5">
                        @foreach($projects as $project)
                            <option value="{{$project->id}}">{{$project->name()}}</option>
                            @endforeach
                    </select>
  {{--                  {!! Form::select('project_id', App\DB\Product\Product::pluck('land_location', 'id'), null, ['class' => 'form-control', 'placeholder' => 'Choose project', 'required' => 'required']) !!}--}}
                </div>
            </div>
            <div class="form-group">
                <label> Cost <span class="required-uzapoint">*</span> </label>
                <div>
                    <input type="text" class="form-control" name="cost[]" id="cost" required>
                </div>
            </div>
            <div class="form-group">
                <label> Purpose <span class="required-uzapoint">*</span> </label>
                <div>
                    <textarea type="text" class="form-control" name="purpose[]" id="purpose" required></textarea>
                </div>
            </div>
            
            <div class="form-group">
                <label>&nbsp;</label>
                <div>
                    <button type="button" class="btn btn-sm btn-primary" id="add_expenses">Add Expenses</button>
                     <label class="col-md-2">No Of Expenses</label>
                    <div class="col-md-2">
                        <input type="number" class="form-control" id="no_of_expenses" name="no_of_expenses" min="0">
                    </div>
                </div>
            </div>
            
            <div id="more_expenses">
            </div>
            
            <div class="modal-footer">
                <button type="submit" class="btn btn-success">Submit</button>
            </div>
        </form>
    @endcomponent    
    
    @component('layouts.partials.modal')
        @slot("id","projectExpenses")
        @slot('dialogClass','modal-lg')
        @slot('title')
            <h3>  <span id="projectName">  </span></h3>
        @endslot 
        <div class="table-responsive-sm">
            <table class="table-bordered table-striped table" id="expenses-table" width="100%">
                <thead>
                <tr>
                    <th> Cost </th>
                    <th> Purpose </th>                    
                </tr>
                </thead>
               
            </table>
        </div>
    @endcomponent     
    
    @endsection

@section('extra_js_footer')
    @include('layouts.dataTables.datatable')
    <script>
        $("#data-table").DataTable({
            sort:false
        });


        var url = "{{url('/')}}";


        $(document).ready(function(){
            $('#projectExpenses').on('shown.bs.modal', function (e) {
                var projectName = $(e.relatedTarget).data('project');
                var projectId = $(e.relatedTarget).data('project-id');
                
                $('#projectName').text(projectName+ ' Expenses');
                
                $('#expenses-table').DataTable().clear().destroy();
                
                pullCostandPurpose(projectId);
                
                
            });
            
            
            $('#add_expenses').on('click', function(){
                
                $('#more_expenses').empty();
                
                var html = '';
                
                var noOfExpenses = $('#no_of_expenses').val();
                
                for (var i = 1; noOfExpenses >=  i; i++)
                {
                    html += '<div class="form-group">'+
                                '<label> Cost <span class="required-uzapoint">*</span> </label>'+
                            '<div>'+
                               '<input type="text" class="form-control" name="cost[]" id="cost" required>'+
                            '</div> </div>';
                    html += '<div class="form-group">'+
                                '<label> Purpose <span class="required-uzapoint">*</span> </label>'+
                            '<div>'+
                                '<textarea type="text" class="form-control" name="purpose[]" id="purpose" required></textarea>'+
                            '</div> </div>';
                }    
                $('#more_expenses').append(html);
            });
        });

        function pullCostandPurpose(projectId)
        {
            $('#expenses-table').DataTable({
                "ajax":{
                    url : url+ '/loadExpenses' +
                    '/'+projectId,
                    type : 'GET',
                    dataSrc : '',
                    "bDestroy": true
                },
                "aoColumns": [
                    { "data": "cost" },
                    { "data": "purpose" }
                ]
            });
        }
    </script>
@endsection
