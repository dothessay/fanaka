@extends('layouts.master')
@section('title',' Reports')
@section('content')
    <div class="form-group col-md-12">
        {!! \Codex\Classes\Helper::reportWidget('Above 50%','projects_reports_above-50','Shows all plots which have been paid above 50%') !!}
        {!! \Codex\Classes\Helper::reportWidget('Below 50%','projects_reports_below-50','Shows all plots which have paid been below 50%') !!}
        {!! \Codex\Classes\Helper::reportWidget('Completed','projects_reports_completed','Shows all plots which have <br> full paid ' ) !!}
        {!! \Codex\Classes\Helper::reportWidget('Availability','projects_reports_availability','Availability availability <br/> per project') !!}
        {!! \Codex\Classes\Helper::reportWidget('Below 30 k','projects_reports_below_30k','Paid below 30 k') !!}
        {!! \Codex\Classes\Helper::reportWidget('Un Paid Plots','projects_reports_unpaid','Un Paid Plots') !!}
        {!! \Codex\Classes\Helper::reportWidget('All Plots with Clients','projects_reports_plot-with-clients','All Plots With Clients') !!}
        {!! \Codex\Classes\Helper::reportWidget('Total Collections','projects_reports_collections','Project collections per project per duration of time') !!}
    </div>
@endsection
