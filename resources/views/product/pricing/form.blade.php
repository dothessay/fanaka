@extends('layouts.master')
@section('title','New '. $product->name() .' Project pricing')
@section('content')
    @component('layouts.partials.panel')
        <form
                class="form-bordered form-horizontal"
                method="post"
                action="{{route('projects_manage_pricing_store', ['product' => $product->id])}}"
                id="pricingForm"
                autocomplete="off"
        >
            {{csrf_field()}}

            <input type="hidden" name="price_id" value="{{isset($pricing) ? $pricing->id : ''}}">
            <div class="form-group">
                <label class="control-label col-md-2">Size</label>
               <div class="col-md-4">
                   <select
                           name="size_id"
                           class="form-control selectpicker"
                           data-live-search="true"
                   >

                       @foreach($sizes as $size)
                           <option
                                   value="{{$size->id}}"
                                   @if (isset($pricing) && $pricing->size_id == $size->id) selected @endif

                           >{{$size->label}}</option>

                       @endforeach

                   </select>
               </div>

                <label class="control-label col-md-2">Payment Option</label>
                <div class="col-md-4">
                    <select
                            name="payment_option"
                            class="form-control selectpicker"
                            data-live-search="true"
                    >

                        <option value="cash"
                                @if (isset($pricing) && $pricing->payment_option == 'cash') selected @endif
                        >Cash</option>
                        <option value="offer" selected @if (isset($pricing) && $pricing->payment_option == 'offer') selected @endif>Offer</option>
                        <option value="installment"  @if (isset($pricing) && $pricing->payment_option == 'installment') selected @endif>Installment</option>

                    </select>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-2">Payable Amount</label>
                <div class="col-md-4">
                    <input type="text"
                           name="amount"
                           class="form-control"
                           @if (isset($pricing->amount)) value="{{$pricing->amount}}" @endif>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-2">Start Date</label>
                <div class="col-md-4">
                    <input
                            type="text"
                            name="start_at"
                            class="form-control date"
                            @if (isset($pricing->start_at)) value="{{$pricing->start_at}}" @endif>
                </div>


                <label class="control-label col-md-2">End Date</label>
                <div class="col-md-4">
                    <input
                            type="text"
                            name="end_at"
                            class="form-control date"
                            @if (isset($pricing->end_at)) value="{{$pricing->end_at}}" @endif>
                </div>
            </div>

            <div class="modal-footer">
                <button class="btn btn-success" id="button" type="button">{{ isset($pricing->id) ? "Update" : "Save" }} </button>
            </div>

        </form>
    @endcomponent
@endsection

@section('extra_js_footer')
    @include('layouts.form')
    @include('layouts.datePicker.datepicker')


    <script>

        class form {
             validate(){

                 $('button#button').on('click', function () {
                     $(this).attr('disabled',  true).text('saving . . . ');

                     $("form#pricingForm").submit();

                 })


             }

             datePickers(){
                 $('input.date').datepicker();
             }

        }

       let pricingForm =  new form();

        pricingForm.validate();
        pricingForm.datePickers();


    </script>

@stop
