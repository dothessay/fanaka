@extends('layouts.master')
@section('title','Project pricing')
@section('content')
    @component('layouts.partials.panel')

       <div class="mb-5" style=" margin-bottom: 12px">
           <a href="{{route('projects_manage_pricing_create',['product'  => $product->id])}}" class="btn btn-info mb-5">New Pricing</a>

       </div>
        <div class="table-responsive">
            <table class="table table-striped table-bordered mt-4">
                <thead>
                <tr>
                    <th>Project Size</th>
                    <th>Payment Option</th>
                    <th>Amount</th>
                    <th>Start</th>
                    <th>End</th>
                    <th>Actions</th>
                </tr>
                </thead>
                <tbody>
                @foreach($product->pricing as $pricing)
                    <tr>
                        <td>{{$pricing->size->label}}</td>
                        <td>{{$pricing->payment_option}}</td>
                        <td>{{$pricing->amount}}</td>
                        <td>{{ $pricing->payment_option == 'offer' ? $pricing->start_at : 'Start Of Project'}}</td>
                        <td>{{$pricing->payment_option == 'offer' ?  $pricing->end_at : 'Till End Of Project'}}</td>

                        <td>
                            @component('layouts.button')
                                <li>
                                    <a href="{{route('projects_manage_pricing_edit', ['product' => $product->id ,'pricing'  => $pricing->id])}}">Edit</a>
                                </li>
                                <li>
                                    <a href="{{route('projects_manage_pricing_delete', ['product' => $product->id ,'pricing'  => $pricing->id])}}">delete</a>
                                </li>
                            @endcomponent

                        </td>
                    </tr>


                @endforeach
                </tbody>
            </table>
        </div>

    @endcomponent
@endsection
@section('extra_js_footer')

    @include('layouts.dataTables.datatable')
    <script>
        $("table").DataTable({})
    </script>
@endsection
