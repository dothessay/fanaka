@extends('layouts.master')
@section('title','Avalailable plots for project '. $product->name)
@section('content')

    @include('layouts.back')

    @component('layouts.partials.panel')



        <table class="table-bordered table-striped table" id="data-table" width="100%" cellspacing="2" cellpadding="2">
            <thead>
            <tr>
                <th>Price</th>
                <th>Size</th>
                <th>Plot No</th>

            </tr>
            </thead>
            <tbody>
            @foreach($plots as $plot)
                <tr>
                    <td>{{number_format($plot->price , 2)}}</td>
                    <td>{{$plot->size->label}}</td>
                    <td>{{$plot->plot_no}}</td>
                </tr>
            @endforeach

            </tbody>


        </table>

    @endcomponent

@endsection

@section('extra_js_footer')
    @include('layouts.dataTables.tablebuttons')
    @include('layouts.dataTables.buttons')

@endsection
