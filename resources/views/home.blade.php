@extends('layouts.master')

@section('title','Dashboard')

@section('extra_js_header')

    {{-- {!! Charts::assets(['global','highcharts']) !!}--}}

@endsection


@section('content')

    <div class="">
        @can('access-module-component','dashboard_project-statistics')
            @foreach($availabilities as $availability)
                    {!!

                      \Codex\Classes\Handlers\Widget::build()
                      ->title($availability['product'])
                      ->value('('.$availability['available'].' /'.$availability['total'].')'.$availability['percentage'])
                      ->uri(route('projects_manage_details', ['slug' => $availability['id']]))
                      ->output();

                       !!}


            @endforeach
        @endcan

        @include('_home_buttons')

    </div>
@endsection
