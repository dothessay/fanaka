@extends('layouts.master')
@section('title','company setting')
@section('content')

   @component('layouts.partials.panel')
       <form class="form-horizontal" method="post" action="{{route('settings_company_store')}}" enctype="multipart/form-data">

           {{csrf_field()}}

           <div class="form-group">

               <label class="col-md-2" title="e.g logo">Company Logo</label>
               <div class="col-md-4">
                   <input type="file" class="form-control" name="logo" value="{{$business->logo}}">

               </div>
               <div class="col-md-4">
                   <img src="{{url(\Illuminate\Support\Facades\Storage::url($business->logo))}}" class="img-rounded" width="70px">
               </div>


           </div>


           <div class="form-group">

               <label class="col-md-2" title="e.g Fanaka Real Estate">Company Name</label>
               <div class="col-md-4">
                   <input type="text" class="form-control" name="name" value="{{$business->name}}">
               </div>


               <label class="col-md-2" title="e.g .. a cut above the rest">Slogan </label>
               <div class="col-md-4">
                   <input type="tel" class="form-control"  name="slug" value="{{$business->slug}}">
               </div>



           </div>

           <div class="form-group">
               <label class="col-md-2" title="e.g Ruai town abc building 1st flr rm 1">Address </label>
               <div class="col-md-4">
                   <input type="text" class="form-control"  name="address" value="{{$business->address}}">
               </div>


           <label class="col-md-2" title="info@fanaka.co.ke">Company Emails</label>
               <div class="col-md-4">
                   <input type="text" class="form-control tagit" name="email" value="{{$business->setting('email') ? : "info@company.com"}}">
               </div>




           </div>

           <div class="form-group">

               <label class="col-md-2" title="separated by comma">Tell(s) </label>
               <div class="col-md-4">
                   <input type="tel" class="form-control tagit"  name="tell" value="{{$business->setting('phone')? : "0700 000 000"}}">
               </div>


               <label class="col-md-2" title="bank name e.g equity">Bank Name </label>
               <div class="col-md-4">
                   <input type="text" class="form-control"  name="bank_name" value="{{$business->setting('bank_name') ? : config('config.equity_bank.name')}}">
               </div>



           </div>

           <div class="form-group">

               <label class="col-md-2" title="Account Name e.g fanaka real estate"> Account Name </label>
               <div class="col-md-4">
                   <input type="text" class="form-control "  name="account_name" value="{{$business->setting('account_name') ? : config('config.equity_bank.branch')}}">
               </div>


               <label class="col-md-2" title="bank name e.g 0123456789">Account No </label>
               <div class="col-md-4">
                   <input type="text" class="form-control"  name="account_no" value="{{$business->setting('account_no') ? : config('config.equity_bank.number')}}">
               </div>

           </div>


           <div class="form-group">

               <label class="col-md-2" title="Account Name e.g fanaka real estate"> Plot Sizes </label>
               <div class="col-md-4">
                   <input type="text" class="form-control tagit "  name="plot_sizes" value="{{implode($business->plotSizeSettings->pluck('label')->toArray(),' , ')}}">
               </div>




           </div>

           <div class="modal-footer">
               <button type="submit" class="btn btn-sm btn-info">Update</button>
           </div>

       </form>
   @endcomponent

@endsection


@section('extra_js_footer')

    @include('layouts.form')

    <script>
        $(function () {

            $('.tagit').tagit();

        })
    </script>

@endsection