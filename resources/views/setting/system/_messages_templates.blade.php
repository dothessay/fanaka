<style>
    input.sms {
        height: 100px;
        width: 600px;
    }
    input.sms {
        height: 100px;
        width: 600px;
    }

    span.keyword ul li {
        display: block;
        padding: 0.3em;
    }

    span.keyword ul li strong{
        color: #ee3b2b;
    }
    span.keyword h6 strong{
        color: #ee3b2b;

    }
</style>

<span class="keyword">
    <h2 class="text-capitalize">Message should be on a single line</h2>
    <h6 class="text-justify">Use the keywords below with square brackets e.g. <strong>[purchaser]</strong></h6>
    <ul class="text-justify">
        <li>Customer Name: <strong>[purchaser]</strong></li>
        <li>Plots <strong>[plots]</strong></li>
        <li>Projects <strong>[projects]</strong></li>
        <li>Installment Balance Amount <strong> [balance_statement] </strong>: will read either full or installment </li>
        <li>Next Payment Amount <strong>[next_amount]</strong></li>
        <li>Offer Payment Amount <strong>[offer_amount]</strong></li>
        <li>Payment Amount <strong>[payment_amount]</strong></li>
        <li>Payment Date <strong> [payment_date]</strong></li>
        <li>Next Payment Date <strong>[next_date]</strong></li>
        <li>Visit Date <strong>[visit_date]</strong></li>
        <li>Company Phone Number <strong>[company_tell]</strong></li>
        <li>Installment Remaining Months <strong>[remaining_months]</strong></li>
    </ul>
</span>


<form
        method="post"
        action="{{route('settings_company_store-message-template')}}"
        class="form-horizontal form-bordered"

>
    @csrf

    <div class="form-group">

        <label class="control-label col-md-2">{{__('Appointment Reminder Message')}}</label>
        <div class="col-md-10">

            <input
                    type="text"
                    aria-rowspan="300px"
                    name="appointment_message"
                    class="form-control sms"
                    value="{{fanakaConfig('appointment_message') ? : "Hi [purchaser], Kindly note that the site visit will be on [visit_date] . Kindly be punctual"}}"
            >
        </div>


    </div>

    <div class="form-group">

        <label class="control-label col-md-2">{{__('Reservation Approval Request (admin)')}}</label>
        <div class="col-md-10">
            <input
                    type="text"
                    name="offer_message"
                    class="form-control sms"
                    value="{{fanakaConfig('offer_message') ? : "Approve an reservation for: [purchaser] [plots] of [offer_amount]"}}"
            >
        </div>


    </div>

    <div class="form-group">

        <label class="control-label col-md-2">{{__('Reservation Message to Client')}}</label>
        <div class="col-md-10">
            <input
                    type="text"
                    name="offer_message_client"
                    class="form-control sms"
                    value="{{fanakaConfig('offer_message_client') ? : "Dear [purchaser], We have received your Reservation payment of [offer_amount] for [plots]. We would like to thank you and appreciate your confidence in investing with us. Feel free to get in touch with us on [company_tell] for any enquiries.Have a great day!"}}"
            >
        </div>


    </div>

    <div class="form-group">


        <label class="control-label col-md-2">{{__('Admin New Sale')}}</label>
        <div class="col-md-10">
            <input
                    type="text"
                    name="new_sale"
                    class="form-control sms"
                    value="{{fanakaConfig('new_sale') ? : "Approve new Sale request for: [purchaser] [plots] [payment_amount]"}}"
            >
        </div>

    </div>

    <div class="form-group">

        <label class="control-label col-md-2">{{__('Payment Request')}}</label>
        <div class="col-md-10">
            <input
                    type="text"
                    name="payment_request"
                    class="form-control sms"
                    value="{{fanakaConfig('payment_request') ? : "Kindly review and approve an installment payment request for: [purchaser] [plots] [payment_amount]"}}"
            >
        </div>

    </div>

    <div class="form-group">

        <label class="control-label col-md-2">{{__('Payment Received (client)')}}</label>
        <div class="col-md-10">
            <input
                    type="text"
                    name="payment_received"
                    class="form-control sms"
                    value="{{fanakaConfig('payment_received') ? : "We have received your [balance_statement] payment of [payment_amount] and would like to thank you. We appreciate your diligence in paying for the plot. Feel free to get in touch with us on [company_tell] for any inquiries. Have a great day!"}}"
            >
        </div>
    </div>


    <div class="form-group">

        <label class="control-label col-md-2">{{__('Activation Message')}}</label>
        <div class="col-md-10">
            <input
                    type="text"
                    name="activation_message"
                    class="form-control sms"
                    value="{{fanakaConfig('activation_message') ? : "We have received your [balance_statement] payment of [payment_amount] and would like to thank you. We appreciate your diligence in paying for the plot. Feel free to get in touch with us on [company_tell] for any inquiries. Have a great day!"}}"
            >
        </div>
    </div>


    <div class="form-group">

        <label class="control-label col-md-2">{{__('Installment Reminder')}}</label>
        <div class="col-md-10">
            <input
                    type="text"
                    name="installment_reminder"
                    class="form-control sms"
                    value="{{fanakaConfig('installment_reminder') ? : "Hi [purchaser]. You are only [remaining_months] instalments away from getting your title deed. Pay today and reduce your waiting period"}}"
            >
        </div>
    </div>
    <div class="form-group">

        <label class="control-label col-md-2">{{__('Plot Holding Notification')}}</label>
        <div class="col-md-10">
            <input
                    type="text"
                    name="plot_holding"
                    class="form-control sms"
                    value="{{fanakaConfig('plot_holding') ? : "Hi {client} you have booked a {project}. No that it will be automatically go back to market after {release_date}. Thank You"}}"
            >
            <span class="help-block keyword">
                <ul class="text-justify nav nav-pills">
                    <li>Use these Tags for plot holding</li>
                    <li>1. <strong>{client}</strong></li>
                    <li>2. <strong>{project}</strong></li>
                    <li>2. <strong>{release_date}</strong></li>
                </ul>
            </span>
        </div>
    </div>
    <div class="form-group">

        <label class="control-label col-md-2">{{__('21 Days Notice ')}}</label>
        <div class="col-md-10">
            <input
                    type="text"
                    name="expired_notice"
                    class="form-control sms"
                    value="{{fanakaConfig('expired_notice') ? : "Hi {client} you have booked a {project}. No that it will be automatically go back to market after {release_date}. Thank You"}}"
            >
            <span class="help-block keyword">
                <ul class="text-justify nav nav-pills">
                    <li>Use these Tags for plot holding</li>
                    <li>1. <strong>{client}</strong></li>
                    <li>2. <strong>{expiry_date}</strong></li>
                    <li>2. <strong>{project_plot}</strong></li>
                    <li>2. <strong>{balance}</strong></li>
                </ul>
            </span>
        </div>
    </div>

    <div class="modal-footer">
        <button class="btn btn-success" type="submit">Submit</button>
    </div>

</form>

