<table class="table table-bordered table-striped">
    <thead>
    <tr>
        <th>Contract Type</th>
        <th>Edit</th>
    </tr>
    </thead>
    <tbody>
    @foreach($contracts as $contract)
        <tr>
            <td>{{$contract->name}}</td>
            <td>
                @component('layouts.button')
                    <li>
                        <a href="{{url('settings/system/'.$contract->id.'/find')}}">Edit</a>
                    </li>
                @endcomponent
            </td>
        </tr>

    @endforeach
    </tbody>

</table>