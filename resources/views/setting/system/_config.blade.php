<form
        name="configure_form"
        class="form-horizontal form-bordered"
        action="{{route('settings_company_store-config')}}"
>

    @csrf

    <div class="form-group">
        <label class="col-md-2 control-label">{{__('Send Sms')}}</label>
        <div class="col-md-4">
            <input
                    type="checkbox"
                    name="send_sms"
                    @if(fanakaConfig('send_sms') ) checked @endif
            >
        </div>

        <label class="col-md-2 control-label">Reservation Days</label>
        <div class="col-md-4">
            <input
                    type="number"
                    class="form-control"
                    name="reservation_days"
                    value="{{fanakaConfig('reservation_days') ? : "7"}}"
            >
        </div>
    </div>

    <div class="form-group">


        <label class="col-md-2 control-label">Select SMS Gateway</label>
        <div class="col-md-4">

            <input
                    type="radio"
                    name="sms_gateway"
                    value="SendAfricanTalkingInnoxNotification"
                    @if(fanakaConfig('sms_gateway')  == 'SendAfricanTalkingInnoxNotification') checked @endif
            >
            Afrikas Talking

            <input
                    type="radio"
                    name="sms_gateway"
                    value="SendAdvantaAfricaInnoxNotification"
                    @if(fanakaConfig('sms_gateway')== "SendAdvantaAfricaInnoxNotification" ) checked @endif
            >
            Advanta Africa

            <input
                    type="radio"
                    name="sms_gateway"
                    value="OneSignalHandler"
                    @if(fanakaConfig('sms_gateway')== "OneSignalHandler" ) checked @endif
            >
            OneSignal <small style="color: lightblue"> (Push Notifications)</small>

        </div>
    </div>


    <div class="form-group">
        <label class="col-md-2 control-label">Afrika Talking UserName</label>
        <div class="col-md-4">
            <input
                    type="text"
                    name="afrika_talking_username"
                    class="form-control"
                    value="{{fanakaConfig('afrika_talking_username') ? : "none"}}"

            >
        </div>

        <label class="col-md-2 control-label">Afrikas Talking Api Key</label>
        <div class="col-md-4">
            <input
                    type="text"
                    name="afrika_takling_api_key"
                    class="form-control"
                    value="{{fanakaConfig('afrika_takling_api_key') ? : "none"}}"

            >
        </div>
    </div>


    <div class="form-group">
        <label class="col-md-2 control-label">Advanta UserName</label>
        <div class="col-md-4">
            <input
                    type="text"
                    name="advanta_username"
                    class="form-control"
                    value="{{fanakaConfig('advanta_username') ? : "none"}}"

            >
        </div>

        <label class="col-md-2 control-label">Advanta Password</label>
        <div class="col-md-4">
            <input
                    type="text"
                    name="advanta_password"
                    class="form-control"
                    value="{{fanakaConfig('advanta_password')  ? : "none"}}"

            >
        </div>

        <label class="col-md-2 control-label">Advanta Sender Id</label>
        <div class="col-md-4">
            <input
                    type="text"
                    name="advanta_sender_id"
                    class="form-control"
                    value="{{fanakaConfig('advanta_sender_id') ? : "none"}}"

            >
        </div>
    </div>

    <div class="form-group">

        <label class="control-label col-md-2">{{__('Leave Days / Year')}}</label>
        <div class="col-md-4">
            <input
                type="text"
                name="leave_days"
                class="form-control"
                value="{{fanakaConfig('leave_days') ? : "21"}}"
        >
        </div>

        <label class="col-md-2 control-label">Commission %</label>
        <div class="col-md-4">
            <input
                    type="text"
                    name="commission_percentage"
                    class="form-control"
                    value="{{fanakaConfig('commission_percentage') ?  fanakaConfig('commission_percentage'):  50}}"

            >
        </div>
    </div>
    <div class="form-group">
        <label class="col-md-2 control-label">Installment Months </label>
        <div class="col-md-4">
            <input
                    type="text"
                    name="installment_months"
                    class="form-control"
                    value="{{fanakaConfig('installment_months') ?  fanakaConfig('installment_months'):  6}}"

            >
        </div>

    </div>



    <div class="form-group">
        <div class="modal-footer">
            <button
                    class="btn btn-success"
                    type="submit"
            >Submit
            </button>
        </div>
    </div>


</form>
