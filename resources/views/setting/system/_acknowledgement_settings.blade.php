
<form
        name="configure_form"
        class="form-horizontal form-bordered"
        action="{{route('settings_company_store-config')}}"
>

    @csrf


    <!-- The toolbar will be rendered in this container. -->
        <div id="toolbar-container"></div>
    <div class="form-group">
        <label class="col-md-2 control-label"> Acknowledgement</label>
        <div class="col-md-10">
            <textarea

                    name="acknowledgement"
                    class="form-control acknowledgement"


            >{!!fanakaConfig('acknowledgement') ? fanakaConfig('acknowledgement'): ""!!}
            </textarea>
        </div>
    </div>




    <div class="form-group">
        <div class="modal-footer">
            <button
                    class="btn btn-success"
                    type="submit"
            >Submit
            </button>
        </div>
    </div>


</form>

<script src="https://cdn.ckeditor.com/ckeditor5/12.3.1/decoupled-document/ckeditor.js"></script>

<script>
    DecoupledEditor
        .create( document.querySelector( 'textarea' ) )
        .then( editor => {
            const toolbarContainer = document.querySelector( '#toolbar-container' );

            toolbarContainer.appendChild( editor.ui.view.toolbar.element );
        } )
        .catch( error => {
            console.error( error );
        } );
</script>
