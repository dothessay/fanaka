<form
        name="configure_form"
        class="form-horizontal form-bordered"
        action="{{route('settings_company_store-agreement-config')}}"
>

@csrf



    <div class="form-group">
        <label class="col-md-2 control-label"> Front  Page</label>
        <div class="col-md-10">
            <textarea

                    name="front_page"
                    class="form-control"


            >
                {!!
                fanakaConfig('front_page') ? fanakaConfig('front_page'): "

                    <p style='text-align: center; font-family: serif;  font-weight: bolder; font-size: 16px; '>DATED….. {date}
                                                 <br/>
                                                 <br/>
                                                 <br/>
                                                 <br/>
                                                 <br/>
                                                 <br/>
                                                 <br/>AGREEMENT FOR SALE
                                                  <br/>
                                                 <br/>
                                                 <br/>
                                                 <br/>
                                                 <br/>
                                                 <br/>
                                                 <br/>FANAKA REAL ESTATE LTD
                                                 <br/>
                                                 <br/>
                                                 <br/>
                                                 <br/>
                                                 <br/>
                                                 (Vendor)
                                                    <br/>
                                                 <br/>
                                                 <br/>
                                                 <br/>
                                                 <br/>
                                                 <br/>
                                                   <br/>{customerDetails}

                                                    <br/>
                                                 <br/>
                                                 <br/>

                                                   <br/>(Purchaser)
                                                 <br/>
                                                 <br/>
                                                 <br/>
                                                 <br/>
                                                 <br/>
                                                 <br/>
                                                   <br/>{plots}

                         </p>"
                 !!}
            </textarea>
        </div>
    </div>

    <div class="form-group">
        <label class="col-md-2 control-label">Vendor</label>
        <div class="col-md-10">
            <textarea

                    name="vendor"
                    class="form-control agreementVendor"


            >
            {!!  fanakaConfig('vendor') ?  fanakaConfig('vendor'):  "<p>Fanaka Real Estate Limited,<br/>Dune Plaza, Suite D1 , Ruai Town, <br/>P.o Box 252-00222 , Nairobi, <br/>Tel: +254 799 000 111, <br/>Email: info@fanaka.co.ke.</b>" !!}
            </textarea>
        </div>
    </div>

    <hr/>
    <div class="form-group">
        <label class="col-md-2 control-label"> Director</label>
        <div class="col-md-6">
            <textarea

                    name="director"
                    class="form-control agreementDirectors"


            >
            {{fanakaConfig('director') ?  fanakaConfig('director'):  "<b>NAME: </b>MOSES MURIITHI KIHUNII	…………………………… <br/><b>ID NO:</b> 25876155 <br/><br/><br/><br/><br/><br/>"}}
            </textarea>
        </div>
    </div>

    <hr/>
    <div class="form-group">
        <label class="col-md-2 control-label"> Agreement Footer</label>
        <div class="col-md-10">
            <textarea

                    name="agreement_footer"
                    class="form-control agreementFooter"


            >
            {{fanakaConfig('agreement_footer') ?  fanakaConfig('agreement_footer'):  "IN WITNESS whereof this Agreement has been duly executed by the Parties hereto the day and year first herein before written. Sealed with the official seal of the VENDOR {vendor}                                                                                                            DIRECTOR                                                                             

{director} 

{purchaserSignature}"}}
            </textarea>
        </div>
    </div>




    <div class="form-group">
        <div class="modal-footer">
            <button
                    class="btn btn-success"
                    type="submit"
            >Submit
            </button>
        </div>
    </div>


</form>
