@extends('layouts.master')

@section('title','System Setting')
@section('extra_css_header')
    <style>
        .tagit{
            height: 100px;
        }

    </style>

   @endsection

@section('content')
    <ul class="nav nav-tabs tabs-uzapoint">


        <li class="active ">
            <a href="#default-tab-1" data-toggle="tab">
                <span class="">Contracts</span>
            </a>
        </li>




        <li class="">
            <a href="#default-tab-2" data-toggle="tab">
                <span class="">System Messages</span>
            </a>
        </li>


        <li class="">
            <a href="#default-tab-3" data-toggle="tab">
                <span class="">Configurations</span>
            </a>
        </li>


        <li class="">
            <a href="#default-tab-4" data-toggle="tab">
                <span class="">Agreement Settings</span>
            </a>
        </li>


        <li class="">
            <a href="#default-tab-5" data-toggle="tab">
                <span class="">Acknowledgement Note Settings</span>
            </a>
        </li>




    </ul>


    <div class="tab-content">

        <div class="tab-pane fade active  in" id="default-tab-1">
            @include('setting.system._contracts')
        </div>


        <div class="tab-pane fade   in" id="default-tab-2">

            @include('setting.system._messages_templates')
        </div>


        <div class="tab-pane fade   in" id="default-tab-3">

            @include('setting.system._config')

        </div>

        <div class="tab-pane fade   in" id="default-tab-4">

            @include('setting.system._agreement_settings')

        </div>
        <div class="tab-pane fade   in" id="default-tab-5">

            @include('setting.system._acknowledgement_settings')

        </div>



    </div>






@endsection


@section('extra_js_footer')

    @include('layouts.form')

    <script>
        $(function () {

            $('.tagit').tagit();

            ClassicEditor
                .create( document.querySelector( '.agreementDirectors' ) )
                .then( editor => {
                    console.log( editor );
                } )
                .catch( error => {
                    console.error( error );
                } );
            ClassicEditor
                .create( document.querySelector( '.agreementVendor' ) )
                .then( editor => {
                    console.log( editor );
                } )
                .catch( error => {
                    console.error( error );
                } );
            DecoupledEditor
                .create( document.querySelector( '.acknowledgement' ) )
                .then( editor => {
                    const toolbarContainer = document.querySelector( '#toolbar-container' );

                    toolbarContainer.appendChild( editor.ui.view.toolbar.element );
                } )
                .catch( error => {
                    console.error( error );
                } );
            ClassicEditor
                .create( document.querySelector( '.acknowledgement' ) )
                .then( editor => {
                    console.log( editor );
                } )
                .catch( error => {
                    console.error( error );
                } );
            ClassicEditor
                .create( document.querySelector( '.agreementFooter' ) )
                .then( editor => {
                    console.log( editor );
                } )
                .catch( error => {
                    console.error( error );
                } );
        })
    </script>

    @endsection
