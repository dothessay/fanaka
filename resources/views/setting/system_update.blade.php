@extends('layouts.master')

@section('title','System Setting')


@section('content')


    @component('layouts.partials.panel')


        <form class="form-horizontal form-bordered" method="post" action="{{route('settings_system_update')}}">
            @csrf


            <input type="hidden" name="id" class="" value="{{$contract->id}}">
            <div class="form-group">
                <h4 class="col-md-12">Update {{ucfirst($contract->name)}}
                    <small>
                        NB: <strong>Do not change words starting with # e.g
                            <span style="color: red">#projectName</span></strong>

                    </small>
                </h4>
                <div class="col-md-12">
                    <textarea name="name" id="" cols="30" rows="10" class="form-control">
                        {!! $contract->value !!}
                    </textarea>
                </div>
            </div>


            <div class="form-group">

                <button type="submit" class="btn btn-success">SUBMIT</button>
            </div>
        </form>

    @endcomponent

@endsection