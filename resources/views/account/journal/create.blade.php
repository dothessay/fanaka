@extends('layouts.master')
@section('title','Add Manual Journal Entry')
@section('extra_css_header')
<style>
    .journal-column{
        display: flex;
    }
</style>
@endsection
@section('content')
@component('layouts.partials.panel')
    <form
            class="form-horizontal"
            method="post"
            action="{{ route('finance_journal-entry_store') }}"
            enctype="multipart/form-data"

    >
        {{ csrf_field() }}
       <table class="table table-borderless table-condensed" id="manual-entry-table" >
           <thead>
           <tr>
               <th>Debit Account</th>
               <th>Debit Amount</th>
               <th>Credit Account</th>
               <th>Credit Amount</th>
               <th>Action</th>
           </tr>
           </thead>
           <tbody>
           <tr>
               <td>
                   <select
                           name="debit_account[]"
                           class="form-control selectpicker"
                           data-live-search="true"
                           data-size="6"
                   >
                       @foreach($charts as $chart)
                           <option value="{{ $chart->id }}">{{ $chart->name }}</option>
                       @endforeach
                   </select>
               </td>
               <td>
                   <input
                           name="debit_amount[]"
                           type="number"
                           value="0"
                           class="form-control debit_amount" required
                   >
               </td>
               <td>

                   <select
                           name="credit_account[]"
                           class="form-control selectpicker"
                           data-live-search="true"
                           data-size="6"
                   >
                       @foreach($charts as $chart)
                           <option value="{{ $chart->id }}">{{ $chart->name }}</option>
                       @endforeach
                   </select>
               </td>
               <td><input
                           type="number"
                           name="credit_amount[]"
                           value="0"
                           class="form-control col-md-2 credit_amount" required
                   >
               </td>
               <td>
                   <button type="button" class="btn btn-sm btn-primary clickedToAdd">+</button>
               </td>
           </tr>
           </tbody>

       </table>
        <div class="email-footer">
            <button class="btn btn-sm btn-primary">Submit</button>
        </div>
    </form>
@endcomponent
@endsection
@section('extra_js_footer')
    @include('layouts.form')
<script>

    var htmlTable = '<tr>\n' +
        '               <td>\n' +
        '                   <select\n' +
        '                           name="debit_account[]"\n' +
        '                           class="form-control selectpicker"\n' +
        '                           data-live-search="true"\n' +
        '                           data-size="6"\n' +
        '                   >\n' +
        '                       @foreach($charts as $chart)\n' +
        '                           <option value="{{ $chart->id }}">{{ $chart->name }}</option>\n' +
        '                       @endforeach\n' +
        '                   </select>\n' +
        '               </td>\n' +
        '               <td>\n' +
        '                   <input\n' +
        '                           name="debit_amount[]"\n' +
        '                           type="number"\n' +
        '                           value="0"\n' +
        '                           class="form-control" required\n' +
        '                   >\n' +
        '               </td>\n' +
        '               <td>\n' +
        '\n' +
        '                   <select\n' +
        '                           name="credit_account[]"\n' +
        '                           class="form-control selectpicker"\n' +
        '                           data-live-search="true"\n' +
        '                           data-size="6"\n' +
        '                   >\n' +
        '                       @foreach($charts as $chart)\n' +
        '                           <option value="{{ $chart->id }}">{{ $chart->name }}</option>\n' +
        '                       @endforeach\n' +
        '                   </select>\n' +
        '               </td>\n' +
        '               <td><input\n' +
        '                           type="number"\n' +
        '                           name="credit_amount[]"\n' +
        '                           value="0"\n' +
        '                           class="form-control col-md-2" required\n' +
        '                   >\n' +
        '               </td>\n' +
        '               <td>\n' +
        '                   <button type="button" class="btn btn-sm btn-primary clickedToAdd">+</button>\n' +
        '                   <button type="button" class="btn btn-sm btn-danger clickedToRemove">-</button>\n' +
        '               </td>\n' +
        '           </tr>';


        $(document).on('click', 'button.clickedToAdd', function () {

            $('table#manual-entry-table>tbody').append(htmlTable);

            $(".selectpicker").selectpicker('refresh')
        })
        $(document).on('click', 'button.clickedToRemove', function () {

            $(this).closest('tr').remove()

            $(".selectpicker").selectpicker('refresh')
        })

</script>
@endsection
