@extends('layouts.master')
@section('title','Journal Entry')
@section('content')
    <ul class="nav nav-pills">
        <li class="active">
            <a href="{{ route('finance_journal-entry_create') }}">Manual Entry</a>
        </li>
    </ul>
@component('layouts.partials.panel')



    <form class="form-horizontal form-bordered">
        <div class="form-group">
            <label class="control-label  col-md-2">Start Date</label>
            <div class="col-md-4">
                <input type="text" class="form-control date" name="startDate">
            </div>

            <label class="control-label col-md-2">End Date</label>
            <div class="col-md-4">
                <input type="text" class="form-control date" name="endDate">
            </div>
        </div>
        <div class="email-footer form-group modal-footer">
            <button class="btn btn-primary pull-right"> pull</button>
        </div>
    </form>
<div class="table-responsive modal-body">
    <table class="table table-bordered table-striped" id="journal-table">
        <thead>
        <tr>
            <th>#</th>
            <th>Account</th>
            <th>Debit</th>
            <th>Credit</th>
            <th>Action</th>
            <th>User Responsible</th>
            <th>Time</th>
        </tr>
        </thead>
        <tbody>
        @foreach($journals as $journal)
            <tr>
                <td> {{ $journal->id }} </td>
                <td> <a href="{{ $journal->chart->path() }}">{{ $journal->chart->name .'  ( '. $journal->chart->code .' ) ' }}</a> </td>
                <td>{{ number_format(floatval($journal->debit) , 2) }} </td>
                <td>{{ number_format(floatval($journal->credit ) , 2)}} </td>
                <td><a href="{{ ! is_null($journal->journalable) ?  url($journal->journalable->path() ): '#' }}">{{ ! is_null($journal->journalable) ? $journal->journalable_type : '#' }}</a> </td>
                <td>{{ $journal->user->fullName() }} </td>
                <td>{{ $journal->created_at }}</td>
            </tr>
        @endforeach
        </tbody>
        <tfoot>
        <tr>
            <td>Totals</td><td></td>
            <td>{{ number_format(floatval($journals->sum('debit')) ,2) }}</td>
            <td>{{ number_format(floatval($journals->sum('credit')), 2) }}</td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
        </tfoot>
    </table>
</div>
@endcomponent



@endsection
@section('extra_js_footer')
@include('layouts.dataTables.datatable')
@include('layouts.datePicker.datepicker')
    <script>
        $("table#journal-table").DataTable({
            "order": [[0, "desc"]],
            "pageLength": 15,
            "columnDefs": [
                {"orderable": false, "targets": []}
            ]
        })

        $("input").datepicker();
    </script>

@endsection
