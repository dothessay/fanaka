@extends('layouts.master')
@section('title','Manage Requisitions')
@section('content')

    <ul class="nav nav-pills">
        <li class="active">
            <a href="{{ route('finance_requisition_index') }}"> Back Requisitions</a>
        </li>
    </ul>

    <table class="table table-hover">
        <tbody>
        <tr>
            <th>Requested By</th>
            <td>{{  $requisition->user->fullName() }}</td>
        </tr>
        <tr>
            <th>Date</th>
            <td>{{  $requisition->created_at }}</td>
        </tr>
        <tr>
            <th>Amount</th>
            <td>{{ $requisition->amount }}</td>
        </tr>
        <tr>
            <th>Reason</th>
            <td>{!! $requisition->reason  !!} </td>
        </tr>
        @if(! is_null($requisition->rejected_at))

            <tr>
                <th>Rejected By</th>
                <td>{!! $requisition->rejectedBy->fullName()  !!} </td>
            </tr>
            <tr>
                <th>Rejected At</th>
                <td>{!! $requisition->rejected_at  !!} </td>
            </tr>
            <tr>
                <th>Reason</th>
                <td>{!! $requisition->reject_reason  !!} </td>
            </tr>
        @endif
        @if(! is_null($requisition->approved_at))

            <tr>
                <th>Approved By</th>
                <td>{!! $requisition->approvedBy->fullName()  !!} </td>
            </tr>
            <tr>
                <th>Approved At</th>
                <td>{!! $requisition->approved_at  !!} </td>
            </tr>
        @endif
        @if(! is_null($requisition->disbursed_at))

            <tr>
                <th>Disbursed By</th>
                <td>{!! $requisition->disbursedBy->fullName()  !!} </td>
            </tr>
            <tr>
                <th>Disbursed At</th>
                <td>{!! $requisition->disbursed_at  !!} </td>
            </tr>
        @endif
        <tr>
            <th>Status</th>
            <td>{!! $requisition->status() !!}</td>
        </tr>
        <tr>
            <td>
                @if( is_null($requisition->approved_at) && is_null($requisition->rejected_at))

                    <a class="btn btn-success" href="{{ route('finance_requisition_edit', ['requisition' => $requisition->id]) }}">
                        Update
                    </a>

                    @can('access-module-component-functionality','finance_requisition_approve-and-reject')
                        <button class="btn btn-success" type="button" data-toggle="collapse" data-target="#approve"
                                aria-expanded="false" aria-controls="approve">
                            Approve
                        </button>

                        <button class="btn btn-danger" type="button" data-toggle="collapse"
                                data-target="#collapseExample" aria-expanded="false" aria-controls="collapseExample">
                            Reject
                        </button>
                    @endcan


                @endif
                @can('access-module-component-functionality','finance_requisition_disburse')
                    @if( ! is_null($requisition->approved_at) && is_null($requisition->disbursed_at))
                        <button class="btn btn-success" type="button" data-toggle="collapse" data-target="#disburse"
                                aria-expanded="false" aria-controls="disburse">
                            Disburse
                        </button>
                    @endif
                @endcan
            </td>
            <td>&nbsp;</td>

        </tr>

        </tbody>
    </table>

    <div class="row">
        <div class="collapse" id="collapseExample">
            <form
                    method="post"
                    id="reject-form"
                    class="form-horizontal form-bordered"
                    action="{{ route('finance_requisition_reject', ['requisition' => $requisition->id]) }}">
                @csrf
                {{ method_field('PATCH') }}
                <div class="form-group">
                    <label class="control-label col-md-2"> Reason </label>
                    <textarea rows="30"
                              name="reject_reason">{{ __('Reason why you have rejected the requisition') }}</textarea>
                </div>

                <div class="email-footer">
                    <button type="button"
                            onclick='swal({ title: "Are you sure?", text: "Once rejected requisition can not be restored", icon: "warning", buttons: true, dangerMode: true, }) .then((reject) => { if (reject) { $("form#reject-form").submit() } else { swal("You have canceled!"); } });'
                            class="btn btn-primary">Confirm
                    </button>

                </div>
            </form>
        </div>
    </div>
    <div class="row">
        <div class="collapse" id="approve">
            <form
                    method="post"
                    id="approve-form"
                    class="form-horizontal form-bordered"
                    action="{{ route('finance_requisition_approve', ['requisition' => $requisition->id]) }}">
                @csrf
                {{ method_field('PATCH') }}

                <div class="email-footer">
                    <button type="button"
                            onclick='swal({ title: "Are you sure?", text: "Once approved requisition can not be undone", icon: "warning", buttons: true, dangerMode: true, }) .then((reject) => { if (reject) { $("form#approve-form").submit() } else { swal("You have canceled!"); } });'
                            class="btn btn-primary">Confirm
                    </button>

                </div>
            </form>
        </div>
    </div>
    <div class="row">
        <div class="collapse" id="disburse">
            <form
                    method="post"
                    id="disburse-form"
                    class="form-horizontal form-bordered"
                    action="{{ route('finance_requisition_disburse', ['requisition' => $requisition->id]) }}">
                @csrf
                {{ method_field('PATCH') }}
                <div class="email-footer">
                    <button type="button"
                            onclick='swal({ title: "Are you sure?", text: "Once disbursed requisition can not be undone", icon: "warning", buttons: true, dangerMode: true, }) .then((reject) => { if (reject) { $("form#disburse-form").submit() } else { swal("You have canceled!"); } });'
                            class="btn btn-primary">Confirm
                    </button>

                </div>
            </form>
        </div>
    </div>
@endsection
