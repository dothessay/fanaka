@extends('layouts.master')
@section('title','Add new requisition')
@section('content')

    <div class="row">
        <form
                class="form-horizontal"
                method="post"
                action="@if(isset($requisition->id)) {{ route('finance_requisition_update', ['requisition' => $requisition->id]) }} @else {{ route('finance_requisition_store') }} @endif"
        >
            {{ csrf_field() }}
            @if(isset($requisition->id)) {{ method_field('PATCH') }} @endif

            <div class="form-group">
                <label class="control-label col-md-2">Amount</label>
                <div class="col-md-4">
                    <input
                            type="number"
                            class="form-control @error('amount') is-invalid @enderror"
                            name="amount"
                            value="{{ old('amount' , $requisition->amount) }}"

                    >
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-2">Reason</label>
                <div class="col-md-8">
                    <textarea
                            class="form-control @error('reason') is-invalid @enderror"
                            name="reason"
                    >{{ old('reason' , $requisition->reason) }}</textarea>
                </div>
            </div>

            <div class="modal-footer">
                <button class="btn btn-success">Submit</button>
            </div>

        </form>
    </div>

@endsection
