@extends('layouts.master')
@section('title','Manage Requisitions')
@section('content')

    <ul class="nav nav-pills">
        <li class="active">
            <a href="{{ route('finance_requisition_create') }}"> New Requisitions</a>
        </li>
    </ul>

    <div class="table-responsive">
        <table class="table table-striped table-bordered" width="100%" cellpadding="0" cellspacing="0">
            <thead>
            <tr>
                <th>Date</th>
                <th>Amount</th>
                <th>Reason</th>
                <th>Status</th>
                <th>Actions</th>
            </tr>
            </thead>
            <tbody>
            @foreach($requisitions as $requisition)
                <tr>
                    <td>{{ $requisition->created_at }}</td>
                    <td>{{ config('app.currency') .' '. number_format(floatval($requisition->amount ) , 2)}} /= </td>
                    <td>{!! $requisition->reason  !!}</td>
                    <td>{!! $requisition->status() !!}</td>
                    <td><a class="label label-success" href="{{ route('finance_requisition_show', ['requisition' => $requisition->id]) }}"> View </a></td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>

@endsection

@section('extra_js_footer')
    @include('layouts.dataTables.datatable')
    <script>
        $("table").DataTable()
    </script>
@endsection
