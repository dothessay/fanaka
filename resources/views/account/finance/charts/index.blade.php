@extends('layouts.master')
@section('title','Charts of accounts')
@section('content')
    <div id="app">


        <ul class="nav nav-tabs tabs-uzapoint">


            <li class="active ">
                <a href="#default-tab-1" data-toggle="tab">
                    <span class="">Charts of accounts</span>
                </a>
            </li>
        </ul>


        <div class="tab-content">

            <div class="tab-pane fade active  in" id="default-tab-1">

                @include('account.finance.charts.inc._charts')

            </div>



        </div>

    </div>

    @include('account.finance.charts.inc._modals')


@endsection
@section('extra_js_footer')
    <!-- import CSS -->
    <link rel="stylesheet" href="https://unpkg.com/element-ui/lib/theme-chalk/index.css">
    <!-- import JavaScript -->
    <script src="https://unpkg.com/element-ui/lib/index.js"></script>
    @include('layouts.dataTables.datatable')
    <script>
        new Vue({
            el:"#app",
            data(){
                return {
                    isLoading: false,
                    isSubmittingType: false,
                    expense_name: '',
                    typeIsLoading: false,
                    isUpdatingType: false,
                    expense_type_id: '',
                    types:[],
                    chart_name:'',
                    uploadCSV: false,

                }
            },
            created(){
                this.getIncomeType()
                this.getCharts();

            },
            methods: {
                /***Income types******/
                getIncomeType(){
                    this.typeIsLoading =true;
                    axios.get("{{route('finance_charts_list-type')}}").then(res => {
                        var tableData = '<div id="type-table-remove"><table class="table table-bordered table-striped" id="income-table"> ' +
                            '<thead> ' +
                            '<tr>' +
                            ' <th>Name</th>' +
                            ' <th>Actions</th>' +
                            ' </tr>' +
                            ' </thead><tbody>';
                        this.types = res.data.data;
                        var options = '';
                        $.each(res.data.data ,(index , value) => {
                            options +='<option value="'+value.id+'">'+value.name+'</option>'
                            tableData+='<tr>' +
                               '<td>'+value.name+'</td>'+
                               '<td><div class="btn-group">' +
                                ' <button type="button" class="btn btn-primary btn-sm dropdown-toggle" ' +
                                'data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> Action <span class="caret"></span> ' +
                                '</button> ' +
                                '<ul class="dropdown-menu"> ' +
                                '<li><a class="edit-type" @click="updateType()" data-toggle="modal" data-target="#updateType" data-id='+value.id+' data-name='+value.name+'>Edit</a></li>' +
                                '<li><a class="delete-type" data-toggle="modal" data-target="#deleteType" data-id='+value.id+' data-name='+value.name+'>Delete</a></li>' +
                                '</ul> </div></td>' +
                               '</tr>'
                       });
                        $("select#types").empty();
                        $("select#types").append(options).selectpicker('refresh');
                        tableData +="</div></tbody></table>";
                        $("div#type-table-remove").remove();
                        $("div#append-type-table").html(tableData);
                        $("table#income-table").DataTable({sort: false});
                    });
                    this.typeIsLoading = false;

                },
                getCharts(){
                    this.typeIsLoading =true;
                    axios.get("{{route('finance_charts_list-charts')}}").then(res => {
                        var tableData = '<div id="chart-table-remove"><table class="table table-bordered table-striped" id="chart-table"> ' +
                            '<thead> ' +
                            '<tr>' +
                            ' <th>Name</th>' +
                            ' <th>Code</th>' +
                            ' <th>Actions</th>' +
                            ' </tr>' +
                            ' </thead><tbody>';
                        $.each(res.data.data ,(index , value) => {
                            console.log(value)
                            tableData+='<tr>' +
                               '<td>'+value.name+'</td>'+
                               '<td>'+value.code+'</td>'+
                               '<td><div class="btn-group">' +
                                ' <button type="button" class="btn btn-primary btn-sm dropdown-toggle" ' +
                                'data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> Action <span class="caret"></span> ' +
                                '</button> ' +
                                '<ul class="dropdown-menu"> ' +
                                '<li><a class="edit-type" @click="updateType()" data-toggle="modal" data-target="#updateType" data-id='+value.id+' data-name='+value.name+'>Edit</a></li>' +
                                '<li><a class="delete-type" data-toggle="modal" data-target="#deleteType" data-id='+value.id+' data-name='+value.name+'>Delete</a></li>' +
                                '</ul> </div></td>' +
                               '</tr>'
                       });

                        tableData +="</div></tbody></table>";
                        $("div#chart-table-remove").remove();
                        $("div#append-chart-table").html(tableData);
                        $("table#chart-table").DataTable({sort: false});
                    });


                    this.typeIsLoading = false;

                },
                updateNewType() {
                    this.isUpdatingType = true;

                    axios.post("{{route('finance_charts_update-type')}}",{
                        name: this.expense_name,
                        id: $("input.type_id").val(),
                    })
                        .then(res => {
                            $.growl.notice({
                                message: res.data.message,
                            });
                            this.isUpdatingType = false;
                            this.expense_name = '';
                            this.getIncomeType();
                            $("#updateType").modal('hide')
                        }).catch( err => {

                            this.isUpdatingType = false;
                            if(err.response.status === 422)
                            {
                               var errors = err.response.data.message;

                                $.each(errors, (value, messages) => {

                                    $.growl.warning({
                                        message: messages,
                                    })
                                });

                            }
                    })
                },
                deleteNewType(){
                    this.isUpdatingType = true;

                    axios.post("{{route('finance_charts_destroy-type')}}",{
                        id: $("input.delete_type_id").val(),
                    })
                        .then(res => {
                            $.growl.notice({
                                message: res.data.message,
                            });
                            this.isUpdatingType = false;
                            this.expense_name = '';
                            this.getIncomeType();
                            $("#deleteType").modal('hide')
                        }).catch( err => {

                        this.isUpdatingType = false;
                        if(err.response.status === 422)
                        {
                            var errors = err.response.data.message;

                            $.each(errors, (value, messages) => {

                                $.growl.warning({
                                    message: messages,
                                })
                            });

                        }
                    })
                },
                /*****CHART METHODS*****/
                createNewType() {
                    this.isSubmittingType = true;
                    let formData = new FormData($("form.account-new-form")[0])
                    axios.post("{{route('finance_charts_store-chart')}}",formData )
                        .then(res => {
                            $.growl.notice({
                                message: res.data.message,
                            });
                            this.isSubmittingType = false;
                            this.expense_name = '';
                            this.getCharts();

                        }).catch( err => {

                        this.isSubmittingType = false;
                        if(err.response.status === 422)
                        {
                            var errors = err.response.data.message;

                            $.each(errors, (value, messages) => {

                                $.growl.warning({
                                    message: messages,
                                })
                            });

                        }
                    })
                },
            }
        });

        $(document).on('click',"a.edit-type", function () {
            $("input.type_id").val($(this).data('id'))
            $("input.expense_name").val($(this).data('name'))
        })
        $(document).on('click',"a.delete-type", function () {
            $("input.delete_type_id").val($(this).data('id'))

            $("span.expense_name").html($(this).data('name'))
        })


        $('#chart-table').DataTable({})
    </script>
@endsection
