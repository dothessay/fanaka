@extends('layouts.master')
@section('title', $chart->name.' Acoount Journal')

@section('content')
    @component('layouts.partials.panel')

    <div class="table-responsive-md">
        <table class="table table-striped table-bordered tab-content" id="chart_table">
            <thead>
            <tr>
                <th>Debit</th>
                <th>Credit</th>
                <th>Action</th>
                <th>Time</th>
            </tr>
            </thead>
            <tbody>
            @foreach($chart->journals as $journal)
            <tr>
                <td>{{ $journal->debit }}</td>
                <td>{{ $journal->credit }}</td>
                <td><a href="{{ ! is_null($journal->journalable) ?  url($journal->journalable->path() ): '#' }}">{{ ! is_null($journal->journalable) ? $journal->journalable_type : '#' }}</a> </td>
                <td>{{ $journal->created_at }}</td>
            </tr>
            @endforeach
            </tbody>
        </table>
    </div>

    @endcomponent
@endsection

@section('extra_js_footer')

    @include('layouts.dataTables.datatable')
    <script>
        $('table#chart_table').DataTable()
    </script>

@endsection
