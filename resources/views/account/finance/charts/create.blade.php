@extends('layouts.master')

@section('title','New Chart of account')
<?php if (! isset($chart)) { $chart = new \App\DB\Finance\ChartOfAccount() ; }?>

@section('content')
    <ul class="nav-pills nav">
        <li class="active">
            <a href="{{ route('finance_charts_index') }}">Back To Charts</a>
        </li>

    </ul>
   @component('layouts.partials.panel')

       <form
               class="form-horizontal account-new-form"
               action="@if(isset($chart->id)) {{ route('finance_charts_update',['chart' => $chart->id]) }}
               @else
               {{  route('finance_charts_store-chart') }}@endif"
               method="post" @submit.prevent="createNewType()"
               v-loading="isSubmittingType"
               enctype="multipart/form-data">
           {{csrf_field()}}
           @if(isset($chart->id)) @method('patch') @else @method('post') @endif


           @if( ! isset($chart->id))<div class="form-group">
               <fieldset>
                   <div class="well-lg">
                       <legend>Upload Charts</legend>
                       <label class="col-md-2 control-label">Excel File</label>
                       <div class="col-md-4">
                           <input
                                   type="file"
                                   name="charts"
                                   class="attached-document"
                           >
                       </div>
                   </div>
               </fieldset>
           </div>
       @endif

           <fieldset>
               <legend>Manual Inputs</legend>
               <div class="form-group">
                   <label class="col-md-2 control-label">Name</label>
                   <div class="col-md-4">
                       <input
                               type="text"
                               name="name"
                               class="form-control"
                               value="{{ old('name', $chart->name)  }}"
                       >
                   </div>

               </div>

               @if(! isset($chart->id))
                   <div class="form-group">

                       <label class="col-md-2 control-label">Account Code</label>
                       <div class="col-md-4">
                           <input
                                   type="text"
                                   name="code"
                                   class="form-control"
                                   value="{{ old('code',  $chart->code) }}"
                           >
                       </div>
                       <div class="help-block">
                           <span class="required-uzapoint">Used in the source code</span>
                       </div>
                   </div>
               @endif
               <div class="form-group">

                   <label class="col-md-2 control-label">Account Type</label>
                   <div class="col-md-4">
                       <select
                               name="type"
                               class="form-control selectpicker"
                               data-live-search="true"
                       >
                           @foreach(\Codex\Classes\Helper::system()->chartTypes as $type)
                               <option
                                       @if($chart->type == $type) selected @endif
                               >{{ $type }}</option>
                           @endforeach
                       </select>
                   </div>
               </div>

           </fieldset>

           <div class="modal-footer d-flex justify-content-center">
               <button class="btn btn-primary">submit</button>
           </div>
       </form>

   @endcomponent
@endsection

@section('extra_js_footer')
    @include('layouts.form')
@endsection
