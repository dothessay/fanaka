
<!----- NEW TYPE---------->
@component('layouts.partials.modal')
    @slot('id','newType')
    @slot('title','Fill this form to create a new account type')

    <form class="form-horizontal" method="post" @submit.prevent="createNewType()" v-loading="isSubmittingType">
        {{csrf_field()}}
        <div class="form-group">
            <label class="col-md-2 control-label">Type</label>
            <div class="col-md-4">
                <input
                        type="text"
                        name="name"
                        v-model="expense_name"
                        class="form-control"
                >
            </div>
        </div>


        <div class="modal-footer d-flex justify-content-center">
            <button class="btn btn-default">submit</button>
        </div>
    </form>
@endcomponent

@component('layouts.partials.modal')
    @slot('id','updateType')
    @slot('title','Fill this form to create a new account type')

    <form class="form-horizontal" method="post" @submit.prevent="updateNewType()" v-loading="isUpdatingType">
        {{csrf_field()}}
        <div class="form-group">
            <label class="col-md-2 control-label">Type</label>
            <div class="col-md-4">
                <input
                        type="text"
                        name="name"
                        v-model="expense_name"
                        class="form-control expense_name"
                >
                <input

                        type="hidden"
                        name="id"
                        class="form-control type_id"
                >
            </div>
        </div>


        <div class="modal-footer d-flex justify-content-center">
            <button class="btn btn-default">submit</button>
        </div>
    </form>
@endcomponent

@component('layouts.partials.modal')
    @slot('id','deleteType')
    @slot('title')
        Are you sure you want to delete account type <span class="expense_name"></span> @endslot

    <form class="form-horizontal" method="post" @submit.prevent="deleteNewType()" v-loading="isUpdatingType">
        {{csrf_field()}}
        {{method_field('DELETE')}}


        <input

                type="hidden"
                name="id"
                class="form-control delete_type_id"
        >
        <div class="modal-footer d-flex justify-content-center">
            <button class="btn btn-danger">submit</button>
        </div>
    </form>

@endcomponent
@component('layouts.partials.modal')
    @slot('id','newChart')
    @slot('dialogClass','modal-lg')
    @slot('title','Fill this form to create a new chart of account')

    <form class="form-horizontal account-new-form" method="post" @submit.prevent="createNewType()" v-loading="isSubmittingType">
        {{csrf_field()}}
        <div class="form-group">
            <label class="col-md-2 control-label">Name</label>
            <div class="col-md-4">
                <input
                        type="text"
                        name="name"
                        class="form-control"
                >
            </div>

            <label class="col-md-2 control-label">Account Type</label>
            <div class="col-md-4">
                <select class="form-control" name="type_id" data-live-search="true" id="types">
                    <option data-hidden="true">Select Account Type</option>
                </select>
            </div>
        </div>

        <div class="form-group">

            <label class="col-md-2 control-label">Account Code</label>
            <div class="col-md-4">
                <input
                        type="text"
                        name="code"
                        class="form-control"
                >
            </div>
        </div>


        <div class="modal-footer d-flex justify-content-center">
            <button class="btn btn-default">submit</button>
        </div>
    </form>
@endcomponent

@component('layouts.partials.modal')
    @slot('id','updateType')
    @slot('title','Fill this form to create a new account type')

    <form class="form-horizontal" method="post" @submit.prevent="updateNewType()" v-loading="isUpdatingType">
        {{csrf_field()}}
        <div class="form-group">
            <label class="col-md-2 control-label">Type</label>
            <div class="col-md-4">
                <input
                        type="text"
                        name="name"
                        v-model="expense_name"
                        class="form-control expense_name"
                >
                <input

                        type="hidden"
                        name="id"
                        class="form-control type_id"
                >
            </div>
        </div>


        <div class="modal-footer d-flex justify-content-center">
            <button class="btn btn-default">submit</button>
        </div>
    </form>
@endcomponent

@component('layouts.partials.modal')
    @slot('id','deleteType')
    @slot('title')
        Are you sure you want to delete account type <span class="expense_name"></span> @endslot

    <form class="form-horizontal" method="post" @submit.prevent="deleteNewType()" v-loading="isUpdatingType">
        {{csrf_field()}}
        {{method_field('DELETE')}}


        <input

                type="hidden"
                name="id"
                class="form-control delete_type_id"
        >
        <div class="modal-footer d-flex justify-content-center">
            <button class="btn btn-danger">submit</button>
        </div>
    </form>

@endcomponent