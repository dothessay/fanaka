<ul class="nav nav-pills">
    <li>
        <a class="active btn-primary" href="{{ route('finance_charts_create-chart') }}">New Chart Of Account</a>
    </li>
</ul>

<div id="table-loading" v-loading="typeIsLoading">

        <table class="table table-striped table-bordered" id="chart-table">
            <thead>
            <tr>
                <th>Code</th>
                <th>Name</th>
                <th>Type</th>
                <th>Action</th>
            </tr>
            </thead>
            <tbody>
            @foreach($charts as $chart)
                <tr>
                    <td> {{ $chart->code }}</td>
                    <td> {{ $chart->name }}</td>
                    <td> {{ $chart->type }}</td>
                    <th>
                        @component('layouts.button')
                            <li>
                                <a href="{{ route('finance_charts_find', ['chart'  => $chart->id]) }}">Edit</a>
                            </li>
                        @endcomponent
                    </th>
                </tr>
            @endforeach
            </tbody>
        </table>


</div>
