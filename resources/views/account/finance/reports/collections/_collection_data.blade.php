<div class="table-responsive">
    @if (isset($_GET) && sizeof($_GET) > 0)
        <h3 class="text-center">Collection From: {{\Carbon\Carbon::parse($_GET['start_date'])->format('Y-m-d')}}
            To: {{\Carbon\Carbon::parse($_GET['end_date'])->format('Y-m-d')}}</h3>
    @endif
    <?php $cummurative = [];
    ?>
    @foreach($totals as $index => $total)
           <h1 class="text-center">{{$index}}</h1>
        <table class="table table-striped table-bordered">


            <thead>
            <tr>
                <th>Month</th>
                <th>Amount</th>
            </tr>
            </thead>
            <tbody>

            <?php $totalAmount = [] ?>
            @foreach($total as $month => $sum)
               <?php $totalAmount[] = $sum['payments']->sum('amount'); ?>
                <tr>
                    <td>{{$sum['months']}}</td>
                    <td>{{number_format(floatval($sum['payments']->sum('amount')) , 2)}}</td>
                </tr>
            @endforeach
            <?php $cummurative[] = array_sum($totalAmount);?>
            </tbody>
            <tfoot>
            <tr>
                <td>Total:</td>
                <td style="border-top: solid #eee 1px; border-bottom: solid #ebfbe ">{{config('config.currency')}}:
                    <b>{{ number_format(array_sum($totalAmount) , 2) }}</b></td>
            </tr>
            </tfoot>

        </table>
    @endforeach
    <table class="table">
        <thead>
        <tr>
            <th>Total</th>
            <th>{!! number_format(array_sum($cummurative) , 2) !!}</th>
        </tr>
        </thead>
    </table>
</div>
