
@extends('layouts.master')
@section('title','Total Project Collections')
@section('content')

@component('layouts.partials.panel')
    <div class="">
        <form
                class="form-horizontal form-bordered"

        >
            <div class="form-group">
                <label for="" class="control-label col-md-2">Select Projects</label>
                <div class="col-md-4">
                    <select
                            multiple
                            name="project_id[]"
                            class="form-control selectpicker"
                            data-live-search="true"
                            data-size="5"

                    >
                        @foreach($projects as $project)
                            <option
                                    value="{{$project->id}}"
                                    @if (isset($_GET['project_id']) && in_array($project->id ,$_GET['project_id']))
                                    selected
                                    @endif
                            >
                                {!! $project->name() !!}
                            </option>
                        @endforeach
                    </select>
                </div>
            </div>

            <div class="form-group">
                <label class="control-label col-md-2">Start Date</label>
                <div class="col-md-4">
                    <input
                    type="text"
                    name="start_date"
                    @if (isset($_GET['start_date']))
                    value="{{$_GET['start_date']}}"
                    @endif
                    class="date form-control"
                    >
                </div>
                <label class="control-label col-md-2">End Date</label>
                <div class="col-md-4">
                    <input
                    type="text"
                    name="end_date"
                    @if (isset($_GET['end_date']))
                        value="{{$_GET['end_date']}}"
                    @endif
                    class="date form-control"
                    >
                </div>
            </div>

            <div class="form-group">
                <div class="modal-footer">
                    <button type="submit" class="btn btn-success">Pull</button>
                    <input type="submit" class="btn btn-warning" name="pdf" value="Print PDF">
                </div>
            </div>
        </form>


        @include('account.finance.reports.collections._collection_data')
    </div>
@endcomponent

@stop
@section('extra_js_footer')
@include('layouts.form')
@include('layouts.datePicker.datepicker')
    <script>
        $('.date').datetimepicker()
    </script>
@endsection
