@extends('layouts.master')
@section('title','Finance Report')
@section('content')
    {!! \Codex\Classes\Helper::reportWidget('Analytics & Statistics','finance_reports_analytics','Get Finance Analytivs and statics based on time') !!}
    {!! \Codex\Classes\Helper::reportWidget('Collection Analytics','finance_reports_collection','Get Finance Collection based on time') !!}
@endsection