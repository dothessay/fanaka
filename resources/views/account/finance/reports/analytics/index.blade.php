@extends('layouts.master')
@section('title','Collection Report')
@section('content')
        {!!
          \Codex\Classes\Handlers\DashBoardAnalytics::build()->year()->output();

         !!}
        {!!
          \Codex\Classes\Handlers\DashBoardAnalytics::build()->month()->output();

         !!}
        {!!
          \Codex\Classes\Handlers\DashBoardAnalytics::build()->today()->output();

         !!}

        <div class="col-md-6">
            <canvas id="pie" width="400px" height="400px"></canvas>
        </div>
        <div class="col-md-6">
            <canvas id="bar" width="400px" height="400px"></canvas>
        </div>
@endsection
@section('extra_js_footer')
    <script>

        (function () {
            $("div#fanaka-updates").modal('show')
        })();

        $.ajax({
            url: "{{route('graph')}}",
            "method": "get",
        }).success(function (data) {

            console.log(data.data['labels']);
            var bar = document.getElementById("bar");
            var myChart = new Chart(bar, {

                type: 'bar',
                data: {

                    labels: data.data['labels'],

                    datasets: [{
                        label: 'Income(KES)',
                        data: data.data['values'],
                        backgroundColor: [
                            'rgba(255, 99, 132, 0.2)',
                            'rgba(54, 162, 235, 0.2)',
                            'rgba(255, 206, 86, 0.2)',
                            'rgba(75, 192, 192, 0.2)',
                            'rgba(153, 102, 255, 0.2)',
                            'rgba(255, 159, 64, 0.2)',
                            'rgba(255, 99, 132, 0.2)',
                            'rgba(54, 162, 235, 0.2)',
                            'rgba(255, 206, 86, 0.2)',
                            'rgba(75, 192, 192, 0.2)',
                            'rgba(153, 102, 255, 0.2)',
                            'rgba(255, 159, 64, 0.2)'
                        ],
                        borderColor: [
                            'rgba(255,99,132,1)',
                            'rgba(54, 162, 235, 1)',
                            'rgba(255, 206, 86, 1)',
                            'rgba(75, 192, 192, 1)',
                            'rgba(153, 102, 255, 1)',
                            'rgba(255, 159, 64, 1)',
                            'rgba(255,99,132,1)',
                            'rgba(54, 162, 235, 1)',
                            'rgba(255, 206, 86, 1)',
                            'rgba(75, 192, 192, 1)',
                            'rgba(153, 102, 255, 1)',
                            'rgba(255, 159, 64, 1)'
                        ],
                        borderWidth: 1
                    }]
                },
                options: {
                    scales: {
                        yAxes: [{
                            ticks: {
                                beginAtZero: true
                            }
                        }]
                    }
                }
            });
            var pie = document.getElementById("pie");
            var myChart = new Chart(pie, {

                type: 'pie',
                data: {

                    labels: data.data['labels'],

                    datasets: [{
                        label: 'Income(KES)',
                        data: data.data['values'],
                        backgroundColor: [
                            'rgba(255, 99, 132, 0.2)',
                            'rgba(54, 162, 235, 0.2)',
                            'rgba(255, 206, 86, 0.2)',
                            'rgba(75, 192, 192, 0.2)',
                            'rgba(153, 102, 255, 0.2)',
                            'rgba(255, 159, 64, 0.2)',
                            'rgba(255, 99, 132, 0.2)',
                            'rgba(54, 162, 235, 0.2)',
                            'rgba(255, 206, 86, 0.2)',
                            'rgba(75, 192, 192, 0.2)',
                            'rgba(153, 102, 255, 0.2)',
                            'rgba(255, 159, 64, 0.2)'
                        ],
                        borderColor: [
                            'rgba(255,99,132,1)',
                            'rgba(54, 162, 235, 1)',
                            'rgba(255, 206, 86, 1)',
                            'rgba(75, 192, 192, 1)',
                            'rgba(153, 102, 255, 1)',
                            'rgba(255, 159, 64, 1)',
                            'rgba(255,99,132,1)',
                            'rgba(54, 162, 235, 1)',
                            'rgba(255, 206, 86, 1)',
                            'rgba(75, 192, 192, 1)',
                            'rgba(153, 102, 255, 1)',
                            'rgba(255, 159, 64, 1)'
                        ],
                        borderWidth: 1
                    }]
                },
                options: {
                    scales: {
                        yAxes: [{
                            ticks: {
                                beginAtZero: true
                            }
                        }]
                    }
                }
            });

        })

    </script>
@endsection