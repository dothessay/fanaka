@extends('layouts.master')
@section('title','Finance Expenses')
@section('extra_css_header')

@endsection
@section('content')



    <ul class="nav nav-tabs tabs-uzapoint">


        <?php $active = true ?>

        @can('access-module-component-functionality','finance_expenses_can-add-project')
            <li class="{{$active ? "active" : ""}}  ">
                <a href="#default-tab-1" data-toggle="tab">
                    <span class="">Project Expenses</span>
                </a>
            </li>

            <?php $active = false; ?>
        @endcan
    <!-- end #content -->

        @can('access-module-component-functionality','finance_expenses_can-add-petty')
            <li class="{{$active ? "active" : ""}} ">
                <a href="#default-tab-2" data-toggle="tab">
                    <span class="">Add Petty Cash</span>
                </a>
            </li>

            <?php $active = false; ?>
        @endcan
    <!-- end #content -->

        @can('access-module-component-functionality','finance_expenses_can-expense')
            <li class="{{$active ? "active" : ""}} ">
                <a href="#default-tab-3" data-toggle="tab">
                    <span class="">Office Expenses</span>
                </a>
            </li>

        <?php $active = false; ?>
    @endcan
    <!-- end #content -->


    </ul>


    <div class="tab-content">
        <?php $active = true ?>

        @can('access-module-component-functionality','finance_expenses_can-add-project')
            <div class="tab-pane fade {{$active ? "active" : ""}} in" id="default-tab-1">

                @include('account.finance.expenses._project')
            </div>
            <?php $active = false; ?>
        @endcan
    <!-- end #content -->

        @can('access-module-component-functionality','finance_expenses_can-add-petty')

            <div class="tab-pane fade {{$active ? "active" : ""}}  in" id="default-tab-2">
                @include('expenses.add_petty')
            </div>
            <?php $active = false; ?>
        @endcan
        @can('access-module-component-functionality','finance_expenses_can-expense')
            <div class="tab-pane fade {{$active ? "active" : ""}}  in" id="default-tab-3">
                @include('expenses.add')
            </div>
            <?php $active = false; ?>
        @endcan

    </div>

@endsection

@section('extra_js_footer')
    @include('layouts.form')
    @include('layouts.datePicker.datepicker')
    @include('layouts.dataTables.tablebuttons')
    <script>
        $("table").DataTable({sort: 0})
        $("input.datepicker").datepicker()
    </script>
@endsection