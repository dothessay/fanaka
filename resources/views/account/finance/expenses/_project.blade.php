
<div class="p-20">
    <!-- begin row -->
    <div class="row">
        <!-- begin col-2 -->
        <div class="col-md-3">
            <div class="">
                <h5 class="m-t-20">Projects</h5>
                <ul class="nav nav-pills nav-stacked nav-inbox">
                    @foreach($projects as $project)
                        <li @if(isset($_REQUEST['project']) && $_GET['project'] == $project->id ) class="active" @endif>
                            <a href="{{url()->current().'?project='.$project->id}}"><i
                                        class="fa fa-inbox fa-fw m-r-5"></i> {{$project->name()}}</a></li>
                    @endforeach
                </ul>
            </div>
        </div>
        <!-- end col-2 -->
        <!-- begin col-10 -->
        <div class="col-md-9">
            @if(isset($_GET['project']))
                <div class="email-btn-row hidden-xs">
                    <a href="{{url()->current().'?action=create'. (isset($_GET['project']) ? '&project='. $_GET['project'] : '')}}"
                       class="btn btn-sm btn-inverse"><i class="fa fa-plus m-r-5"></i> New</a>
                </div>
            @endif
            <div class="email-content">
                @if( ! isset($_GET['action']))
                    <div class="table-responsive">
                        <table class="table table-email" id="my-inbox-email">
                            <thead>
                            <tr>
                                <th>Purpose</th>
                                <th>Amount</th>
                                <th>Date</th>
                                <th>Download</th>
                            </tr>
                            </thead>
                            <tbody id="append-emails-here">
                            <?php $total = 0?>
                            @foreach($expenses as $expense)
                                <?php $total += floatval($expense['amount']) ?>
                                <tr>
                                    <td class="email-select">{{($expense['purpose'])}}</td>
                                    <td class="email-select">{{(config('config.currency') .' ' . number_format(floatval($expense['amount']) , 2))}}</td>
                                    <td class="email-select">{{$expense['date']}}</td>
                                    <td class="email-select">
                                        <a href="{{route('finance_expenses_project_download-evidence',['expense' => $expense['id']])}}"><i class="fa fa-download"></i> </a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                            <tfoot>
                            <tr>
                                <td>Total:</td>
                                <td colspan="2"><strong>{{ config('config.currency') .' ' .number_format(floatval($total) , 2) }}</strong></td>
                            </tr>
                            </tfoot>
                        </table>
                    </div>
                    <div class="email-footer clearfix">


                    </div>
                @else
                    <form
                            action="{{route('finance_expenses_project_store')}}"
                            class="form-horizontal form-bordered"
                            enctype="multipart/form-data"
                            method="post"
                    >
                        {{csrf_field()}}
                        <div class="form-group">
                            <label class="control-label col-md-2">Project</label>
                            <div class="col-md-4">
                                <input
                                        type="hidden"
                                        name="project_id"
                                        value="{{$product->id}}"
                                        class="form-control"
                                >{!! $product->name() !!}
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-2">Purpose</label>
                            <div class="col-md-4">
                                <select
                                        name="purpose"
                                        class="form-control selectpicker"
                                        data-live-search="true"
                                >
                                    @foreach(\Codex\Classes\Helper::system()->projectExpenseTypes as $types)
                                        <option>
                                            {!! $types !!}
                                        </option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-2">Amount</label>
                            <div class="col-md-4">
                                <input
                                        type="text"
                                        name="amount"
                                        class="form-control"
                                >
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-2">Receipt / Evidence</label>
                            <div class="col-md-4">
                                <input
                                        type="file"
                                        name="evidence"
                                        class="form-control"
                                >
                            </div>
                        </div>
                        <div class="email-footer clearfix">
                            <button class="btn btn-success">Submit</button>
                        </div>

                    </form>
                @endif
            </div>
        </div>
        <!-- end col-10 -->
    </div>
    <!-- end row -->
</div>