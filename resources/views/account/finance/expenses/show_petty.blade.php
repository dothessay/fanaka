@extends('layouts.master')
@section('title','Petty cash log')
@section('content')
    <table class="table table-striped data-table table-bordered" id="cashPetty">
        <thead>
        <tr>
            <th>Date</th>
            <th>Authorised By</th>
            <th>Credit</th>
            <th>Account</th>
        </tr>
        </thead>
        <tbody>
        <?php $debit = 0; $credit = 0; $balance = 0?>

        <?php $debit += floatval($petty['debit']); $credit += floatval($petty['credit']) ?>
        @foreach($petty->loggable as $log)
            <?php $credit += $log->credit ;?>
            <tr>
                <td>{{ $petty->created_at}}</td>
                <td>{{ $petty->user->fullName() }}</td>
                <td>{{ config('config.currency') .' '.  $log->credit }}</td>
                <td width="0.06">{{$petty->chart->name}}</td>
            </tr>

        @endforeach
        </tbody>
        <tfoot>
        <tr>
            <td>Total:</td>
            <td></td>
            <td><strong>{!! config('config.currency') .' '.  number_format(floatval($credit) , 2) !!}</strong></td>
            <td></td>
        </tr>
        </tfoot>
    </table>

@endsection
