@extends('layouts.master')
@section('title','Account Records')
@section('content')


    @component('layouts.partials.panel')

        <form class="form-horizontal form-bordered">

            <div class="form-group">
                <label class="col-md-2 control-label">Start Date</label>
                <div class="col-md-4">
                    <input type="text" name="start_date" value="{{   @$_GET['start_date'] }}" class="form-control" id="start-date">
                </div>

                <label class="col-md-2 control-label">End Date</label>
                <div class="col-md-4">
                    <input type="text" name="end_date" value="{{ @$_GET['end_date'] }}" class="form-control" id="end-date">
                </div>

            </div>
            <div class="form-group">
                <label class="col-md-2 control-label">Reference Source
                <span class="small" style="color: red"><br/>e.g. sale , offer</span> </label>
                <div class="col-md-4">

                    <select name="accountable_type" class="form-control selectpicker">
                        <option value="">ALL</option>
                        <option value="sale" @if (isset($_GET['accountable_type']) && $_GET['accountable_type'] ==="sale") selected  @endif>Sale</option>
                        <option value="offer"  @if (isset($_GET['accountable_type']) && $_GET['accountable_type'] ==="offer") selected  @endif >Offer</option>
                    </select>

                </div>
                <label class="col-md-2 control-label"> Source
                <span class="small" style="color: red"><br/>e.g. Installment , New Sale , New Offer Deposit</span> </label>
                <div class="col-md-4">

                    <select name="source" class="form-control selectpicker" data-live-search="true">
                        <option value="">ALL</option>
                        <option value="New Offer Deposit" @if (isset($_GET['New Offer Deposit']) && $_GET['New Offer Deposit'] ==="New Offer Deposit") selected  @endif>New Offer Deposit</option>
                        <option value="Installment" @if (isset($_GET['source']) && $_GET['source'] ==="Installment") selected  @endif>Installment</option>
                        <option value="New Sale"  @if (isset($_GET['source']) && $_GET['source'] ==="New Sale") selected  @endif >New Sale</option>
                    </select>

                </div>

            </div>

            <div class="modal-footer">


                <input class="btn btn-success" type="submit" name="get-account-pdf" value="GET PDF">
                <button class="btn btn-success">Pull</button>
            </div>
        </form>




        @include('account._data')

    @endcomponent
@endsection


@section('extra_js_footer')
    @include('layouts.dataTables.buttons')
    @include('layouts.dataTables.tablebuttons')
    @include('layouts.datePicker.datepicker')
    <script>


       $("input#start-date").datetimepicker({})


        $("input#end-date").datetimepicker({})

    </script>
@endsection
