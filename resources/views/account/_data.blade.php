<?php

$defaultStartDate = '';

if (isset($_GET['start_date'])){

    $defaultStartDate =  \Illuminate\Support\Carbon::parse(request('start_date'))->format('Y-m-d');


}

$defaultEndDate = '';

if (isset($_GET['end_date'])){

    $defaultEndDate =  \Illuminate\Support\Carbon::parse(request('end_date'))->format('Y-m-d');


}



?>
<h2> From: {{ $defaultStartDate }}  to {{ $defaultEndDate}}</h2>
    <table class="table table-bordered table-email table-striped"width="100%" id="data-table">
        <thead>
        <tr>
            <th>Date Deposited</th>
            <th>Date Received</th>
            <th>Reference</th>
            <th>Reference Source</th>
            <th>customer Details</th>
            <th>Project</th>
            <th>Plots No</th>
            <th>User Responsible</th>
            <th>Amount</th>
            <th>Source</th>
            <th>Payment Method</th>
            <th>Reference Code</th>
            @can('access-module-component-functionality','account_list_delete')
                <th>Actions</th>
            @endcan
        </tr>
        </thead>
        <tbody>
        @php $total = 0 @endphp
        @foreach($accounts as $account)




            @php  $total += $account->amount @endphp
            <tr>
                <td>{{$account->deposit_date}}</td>
                <td>{{$account->created_at}}</td>
                <td><a href="{!!  $account->getHref() !!}">{{$account->getAccountType()}}{{$account->accountable_id}}</a> </td>

                <td>{{$account->accountable_type}}</td>
                <td>{{@$account->accountable->customer_details ? : null}}</td>
                <td><?php echo isset($account->accountable) ? $account->accountable->getProjects() : null?></td>
                <td><?php echo isset($account->accountable) ? $account->accountable->getCustomerPlotNos() : null?></td>

                <td>{{$account->user ? $account->user->fullname() : ""}}</td>
                <td>{{number_format($account->amount , 2)}}</td>
                <td>{{$account->source}}</td>
                <td>{{$account->getPaymentMethod()}}</td>
                <td>{{$account->getPaymentReferenceNumbers()}}</td>
                @can('access-module-component-functionality','account_list_delete')
                    <td>
                        @component('layouts.button')
                            <li><a href="{{route('account_list_delete' ,['account' => $account->id])}}">Delete</a></li>
                        @endcomponent
                    </td>
                @endcan
            </tr>
        @endforeach
        </tbody>
        <tfoot>
        <tr>
            <td colspan="6"><strong>Total</strong></td>
            <td colspan="2"><strong>{{number_format($total , 2)}}</strong></td>

        </tr>
        </tfoot>
    </table>
