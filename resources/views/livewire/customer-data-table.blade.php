<div class="table-responsive">
    <div class="col-md-12">
        <form wire:submit.prevent="">
            <div class="form-group">
                <div class="col-md-6">{{ $this->search }}</div>
                <div class="col-md-4">
                    <input class="form-control" wire:model="search" value="{{ $this->search }}">
                </div>
                <input class="btn btn-success" name="search"type="submit" wire:click="searching" value="Search">
                <input class="btn btn-danger" name="reset" type="submit" wire:click="reset" value="Reset">
            </div>
        </form>
    </div>

    @php var_dump($this->customers) ; echo $this->isSearching ;@endphp
@if($this->customers)

        @include('customer::customers._list'  ,  ['customers' => $this->customers])
        @endif
</div>
