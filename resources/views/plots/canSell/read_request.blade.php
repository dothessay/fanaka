@extends('layouts.master')
@section('title','Manage Plot request form')
@section('content')

    <ul class="nav nav-pills">
        <li class="active">
            <a href="{{ route('plots_manage_request_index' , ['user_id' => request()->user()->id]) }}">Back</a>
        </li>
    </ul>

    <table class="table table-hover">
        <tr>
            <th>Project & Plot No:</th> <td> {{ $request->plot->getPropertyDetails() }}</td>
        </tr>
        <tr>
            <th>Agent: </th><td>{{ $request->user->fullName() }}</td>
        </tr>

        <tr>
            <th>Not Approved: </th><td>{!!  $request->status() !!}</td>
        </tr>
           <tr>
               <th>Customer Name :</th><td> {{ $request->customer_name }}</td>
           </tr>
        <tr>
            <th>Phone Number: </th><td>{{ $request->phone_number }}</td>
        </tr>
        <tr>
            <th>Release Date: </th><td>{{ Carbon\Carbon::parse($request->release_at)->englishDayOfWeek . ' '. $request->release_at  }}</td>
        </tr>
        <tr>
            @can('access-module-component-functionality','plots_manage_mark-can-sell')

                @if( is_null($request->approved_at))
                   <td>
                       <form method="post" action="{{ route('plots_manage_request_update' , ['holdingRequest' => $request->id]) }}">
                           {{ method_field('PATCH') }}
                           {{ csrf_field() }}
                           <button class="btn btn-sm btn-success" name="approve"> Approve </button>
                           <button class="btn btn-sm btn-danger" name="reject"> Reject </button>
                       </form>
                   </td>
                    <td>&nbsp;</td>
                @endif

            @endcan

        </tr>
    </table>

@endsection

@section('extra_js_footer')

@endsection
