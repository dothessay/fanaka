@extends('layouts.master')
@section('title','Manage Plot request form')
@section('content')

    <ul class="nav nav-pills">
        <li class="active">
            <a href="{{ route('plots_manage' , ['tab' => 'default-tab-2']) }}">Back</a>

        </li>
        <li> <a class="btn btn-danger" href="{{ route('plots_manage_request_index',['is_approved' => false]) }}">Not approved</a></li>
        <li><a class="btn btn-warning" href="{{ route('plots_manage_request_index', ['is_approved' => true]) }}">Approved</a></li>
    </ul>

    <table class="table table-bordered table-striped">
        <thead>
        <tr>
            <th>Project & Plot No</th>
            <th>Agent</th>
            <th>Customer Name</th>
            <th>Phone Number</th>
            <th>Release Date</th>
            <th>Status</th>
            <th>Is Released</th>
            <th>Actions</th>
        </tr>
        </thead>
        <tbody>
        @foreach($requests as $request)
            <tr>
                <td>{{ $request->plot->getPropertyDetails() }}</td>
                <td>{{ $request->user->fullName() }}</td>
                <td>{{ $request->customer_name }}</td>
                <td>{{ $request->phone_number }}</td>
                <td>Release Date: {{ Carbon\Carbon::parse($request->release_at)->englishDayOfWeek . ' '. $request->release_at  }}</td>
                <td>{!! $request->status() !!}</td>
                <td>{!! $request->isReleased() !!}</td>
                <td>
                    @component('layouts.button')
                     <li><a href="{{ route('plots_manage_request_show', ['holdingRequest' => $request]) }}">view</a></li>
                    @endcomponent
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>

@endsection

@section('extra_js_footer')
    @include('layouts.dataTables.datatable')
    <script>
        $('table.table').DataTable()
    </script>
@endsection
