<form class="form-horizontal form-bordered canSellForm" method="post" action="{{route('plots_manage_store-mark-can-not-sell')}}">

    @csrf

    <div class="form-group">
        <label class="control-label col-md-2">Select The Project</label>

        <div class="col-md-4">
            <select class="form-control selectpicker" data-live-search="true" id="selected-project">

                <option data-hidden="true">Select A project</option>

                @foreach($projects as $project)
                    <option
                            data-plots="{{$project->plots->where('can_sell', true)}}"
                            value="{{$project->code}}">{{$project->name()}}</option>
                @endforeach
            </select>
        </div>

        <label class="control-label col-md-2">&nbsp;&nbsp;&nbsp;&nbsp;</label>

        <div class="col-md-4">

            <button type="submit" class="btn btn-success canSellButton">Hold Plot</button>
        </div>


    </div>

    <div class="form-group">

        <table class="table table-bordered table-striped" id="plot-table">
            <thead>
            <tr>
                <th><input type="checkbox" class="select_all" /></th>
                <th>Plot No</th>
                <th>Price</th>
                <th>Reason</th>
                <th>Release date</th>
            </tr>
            </thead>
            <tbody></tbody>
        </table>




    </div>

    <div class="modal-footer">

        <button type="submit" class="btn btn-success canSellButtoncojhvh">Hold Plot</button>
    </div>
</form>
