
<script>


    $(document).on('change','select#selected-project', function() {
        $("table#plot-table>tbody").empty();
        $("select#selected-plot").empty();

        let plots = $("select#selected-project option:selected").data('plots');

        let selectOptions = '<option data-hidden="true">Select A plot</option>';
        let options = ' ';
        $.each(plots , (index , plot) => {

            selectOptions +='<option value="'+plot.id+'">'+plot.plot_no+' ('+ moneyFormat(plot.price.toFixed(2)) +')</option>';
            options += '<tr>' +
                '<td><input type="checkbox"  name="plot_id['+plot.id+']" class="plot-selected" value="'+plot.id+'"> </td>' +
                '<td>'+plot.plot_no+'  ' +plot.title_no+'</td>' +
                '<td>'+plot.price.toFixed(2)+'</td>' +
                '<td><textarea name="reason['+plot.id+']" class="form-control" cols="20"></textarea></td>' +
                '<td><input type="date" name="date['+plot.id+']" class="form-control date" ></td>' +
                '</tr>'
           // console.log(index , plot)

        });

        console.log(options);
        $("table#plot-table>tbody").append(options);
        $("select#selected-plot").append(selectOptions).selectpicker('refresh')


    })

    $(document).on('change','select#selected-project-on-hold', function() {


        $("table#plot-on-hold-table>tbody").empty();

        let plots = $("select#selected-project-on-hold option:selected").data('plots')



        let options = '';


        $.each(plots , (index , plot) => {
            options += '<tr>' +
                '<td><input type="checkbox"  name="plot_id[]" class="plot-selected" value="'+plot.id+'"> </td>' +
                '<td>'+plot.plot_no+'</td>' +
                '<td>'+plot.price.toFixed(2)+'</td>' +
                '<td>'+plot.title_no+'</td>' +
                '<td>'+plot.hold_reason+'</td>' +
                '<td>'+plot.hold_release_date+'</td>' +
                '</tr>'
           // console.log(index , plot)

        })


        for (let i = 0 ; i < plots.length ; i++){


        }

        $("table#plot-on-hold-table>tbody").append(options);


    })

    $('.plot-selected').each(function () {
        var total = $('.plot-selected').length;
        $(this).on('click', function () {
            var i = 0;
            $('.plot-selected').each(function () {
                if ($(this).is(':checked')) {
                    i++;
                }
            });
            if (i === total) {
                $('.select_all').prop('checked', true);
            } else {
                $('.select_all').prop('checked', false);
            }
        });
    });
    $('.select_all').on('click', function (e) {
        if ($(this).is(':checked')) {
            $('.plot-selected').each(function () {
                $(this).prop('checked', true);
            });
        } else {
            $('.plot-selected').each(function () {
                $(this).prop('checked', false);
            });
        }
    });


    $(document).on('click','button.canSellButton', function () {

        let buttonText = $(this).text();



        $(this).attr('disabled',true).text("submitting ...");



        let formData = new FormData($("form.canSellForm")[0]);



        axios.post('/plots/manage/mark-can-not-sell', formData)
            .then((res) => {


                $.growl.notice({
                    message: res.data.message
                })

                $(this).attr('disabled',true).text(buttonText);


                location.reload(true)

            }).catch((err)  => {

            $(this).attr('disabled',false).text(buttonText);


            $.growl.warning({
                message: err.response.data.message
            })
        })


    })

    $(document).on('click','button.unHoldButton', function () {

        let buttonText = $(this).text();




        $(this).attr('disabled',true).text("submitting ...");

        let formData = new FormData($("form.unHoldPlot")[0]);


        axios.post('/plots/manage/mark-can-sell', formData)
            .then(res=> {


                $.growl.notice({
                    message: res.data.message
                })

                $(this).attr('disabled',true).text(buttonText);


                location.reload(true)

            }).catch(err => {

            $(this).attr('disabled',false).text(buttonText);


            $.growl.warning({
                message: err.response.data.message
            })
        })


    });

   function  moneyFormat(amount) {
        const formatter = new Intl.NumberFormat('en-US', {
            style: 'currency',
            currency: 'KES',
        });
        return formatter.format(amount);
    }


    $('.date').datepicker();
    $('.release_date').datepicker({
        maxDate: 3,
        minDate: 0,
    });
</script>
