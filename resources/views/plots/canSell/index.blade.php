@extends('layouts.master')


@section('title','Mark Plots available for sale')
@section('content')

    @include('layouts.back')
    <ul class="nav nav-tabs tabs-uzapoint">

        <li class="active ">
            <a href="#default-tab-1" data-toggle="tab">
                <span class="">Hold Plots</span>
            </a>
        </li>


        <li class=" ">
            <a href="#default-tab-2" data-toggle="tab">
                <span class="">Un Hold</span>
            </a>
        </li>





    </ul>


    <div class="tab-content">
        <div class="tab-pane fade active  in" id="default-tab-1">

        @include('plots.canSell._form')

        </div>


        <div class="tab-pane fade  in" id="default-tab-2">

        @include('plots.canSell._unhold')

        </div>
    </div>

@endsection

@section('extra_js_footer')

    @include('layouts.form')
@include('layouts.datePicker.datepicker')
    @include('plots.canSell._scripts')
@endsection
