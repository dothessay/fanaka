<form class="form-horizontal form-bordered unHoldPlot" method="post" action="{{route('plots_manage_store-mark-can-sell')}}">

    @csrf
    <div class="form-group">
        <label class="control-label col-md-2">Select Project</label>

        <div class="col-md-4">
            <select class="form-control selectpicker" data-live-search="true" id="selected-project-on-hold">

                <option data-hidden="true">Select A project</option>

                @foreach($holds as $project)


                    <option
                            data-plots="{{$project->getHeldPlots()}}"
                            value="{{$project->code}}">{{$project->name()}}</option>
                @endforeach
            </select>
        </div>

        <label class="control-label col-md-2">&nbsp;&nbsp;&nbsp;&nbsp;</label>

        <div class="col-md-4">

            <button type="submit" class="btn btn-success unHoldBufddfttonjv">Un Hold</button>
        </div>


    </div>

    <div class="form-group">

        <table class="table table-bordered table-striped" id="plot-on-hold-table">
            <thead>
            <tr>
                <th><input type="checkbox" class="select_all" /></th>
                <th>Plot No</th>
                <th>Price</th>
                <th>Title</th>
                <th>Reason </th>
                <th>Release Date</th>
            </tr>
            </thead>
            <tbody></tbody>
        </table>




    </div>

    <div class="modal-footer">

        <button type="submit" class="btn btn-success">Un Hold</button>
    </div>
</form>
