<form
        class="form-horizontal form-bordered canSellForm"
        method="post"
        autocomplete="off"
        action="{{ route('plots_manage_request_store') }}" >

    @csrf

    <div class="form-group">
        <label class="control-label col-md-2">Select The Project</label>

        <div class="col-md-4">
            <select class="form-control selectpicker" data-live-search="true" id="selected-project">

                <option data-hidden="true">Select A project</option>

                @foreach($projects as $project)
                    <option
                            data-plots="{{$project->plots->where('can_sell', true)}}"
                            value="{{$project->code}}">{{$project->name()}}</option>
                @endforeach
            </select>
        </div>
        <label class="control-label col-md-2">Select The Plot</label>

        <div class="col-md-4">
            <select class="form-control selectpicker" name="plot_id" data-live-search="true" id="selected-plot">

                <option data-hidden="true">Select A plot</option>

            </select>
        </div>



    </div>

    <div class="form-group">
        <label class="control-label col-md-2">Customer Name</label>
        <div class="col-md-4">
            <input
                    name="customer_name"
                    class="form-control"
                    type="text"
                    value="{{ old('customer_name') }}"
            >
        </div>
        <label class="control-label col-md-2"> Phone Number </label>
        <div class="col-md-4">
            <input
                    name="phone_number"
                    class="form-control phone"
                    id="phone"
                    type="tel"
                    value="{{ old('phone_number') }}"
            >
        </div>
    </div>

    <div class="form-group">
        <label class="control-label col-md-2"> Release Date </label>
        <div class="col-md-4">
            <input
                    name="release_at"
                    class="form-control release_date"
                    type="text"
                    value="{{ old('release_at') }}"
            >
        </div>
    </div>



    <div class="modal-footer">
        <button type="submit" class="btn btn-success canSellButtoncojhvh">Hold Plot</button>
    </div>
</form>
