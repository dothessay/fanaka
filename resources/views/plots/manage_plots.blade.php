@extends('layouts.master')
@section('title','All Plots')
@section('extra_css_header')
    <style>
        td span a{ text-decoration: none}
    </style>
@endsection
@section('content')

    @can('access-module-component-functionality', 'plots_manage_add')
        <ul class="nav nav-pills">
            <li class="active">
                <a class="btn btn-info" data-toggle="modal" data-target="#addProduct">Add Plot</a>
            </li>
        </ul>
    @endcan




    <ul class="nav nav-tabs tabs-uzapoint">

        <?php $active = true ? "active" : "" ?>
        @can('access-module-component-functionality','plots_manage_add')
            <li class="{{$active}} ">
                <a href="#default-tab-1" data-toggle="tab">
                    <span class="">List Plots</span>
                </a>
            </li>
            <?php $active = "" ?>
        @endcan



            <li class="{{$active}} ">
                <a href="#default-tab-2" data-toggle="tab">
                    <span class="">Hold Plots</span>
                </a>
            </li>
        <li>
            <a href="{{ route('plots_manage_request_index') }}">Hold Requests</a>
        </li>


    </ul>


    <div class="tab-content">
        <?php $isActive = true ? "active" : "";

        ?>
        @can('access-module-component-functionality','plots_manage_add')
            <div class="tab-pane fade {{$isActive}}  in" id="default-tab-1">

                @include('plots._manage-plots')
            </div>


                <?php $isActive = false ?>

        @endcan

            <div class="tab-pane fade {{$isActive}}  in" id="default-tab-2">

                @include('plots.canSell.agent_hold_request')
            </div>

            <div class="tab-pane fade in" id="default-tab-3">


            </div>
    </div>







@endsection

@section('extra_js_footer')
    @include('layouts.dataTables.datatable')
    @include('plots.canSell._scripts')
    @include('layouts.form')
    <script>
        $("#data-table").DataTable({
            sort: false
        });

        $(document).on('click', 'a#code', function () {

            var code = $(this).attr('data-code')

            $("#plot_code").val(code)
        });

        $(document).on('click', 'a#markAsAvailable', function () {

            var code = $(this).attr('data-id')

            $("#plot_id_available").val(code)
        });
        ;


        $(document).on('click', 'a#update', function () {

            var productId = $(this).attr('data-product-id');
            var productName = $(this).attr('data-product-name');
            var price = $(this).attr('data-price');
            var sizeId = $(this).attr('data-size');
            var sizeLabel = $(this).attr('data-size-label');


            $("#product").append('<option value="' + productId + '">' + productName + '</option>').selectpicker("render");

            $("#product-update").html('<option value="' + productId + '" selected>' + productName + '</option>')
            $("#size").append('<option value="' + sizeId + '" selected>' + sizeLabel + '</option>').selectpicker("render");


            $("#price").val(price)
            $("#id").val($(this).attr('data-id'))
            $("#plot_no").val($(this).attr('data-plot_no'))
            $("#title_no").val($(this).data('title_no'))
        })

        $(document).on('click', 'button.addPlot', function () {

            $(this).attr('disabled', true).text('submitting .....')

            $("button.savePlot").trigger('submit')



        });
    </script>
@endsection
