<div class="table table-responsive">

    <table class="table-bordered table-striped table data-table">

        <thead>
        <tr>
            <th>Plot No</th>
            <th>Project</th>
            <th>Unit Price</th>
            <th>Selling Price</th>
            <th>Discount</th>
            <th>Balance</th>
            <th>Customer ('s)</th>
            <th>Date</th>
            <th>Sale Ref</th>
            <th>Served By</th>
        </tr>
        </thead>
        <tbody>
        @foreach($plots as $plot)
            <tr>
                <td>{{$plot['plot_no']}}</td>
                <td>{{$plot['project_name']}}</td>
                <td>{{$plot['unit_price']}}</td>
                <td>{{$plot['selling_price']}}</td>
                <td>{{$plot['discount']}}</td>
                <td>{{$plot['balance']}}</td>
                <td>{!! $plot['customers']!!}</td>
                <td>{{$plot['sale_date']}}</td>
                <td>FSL_{{$plot['sale_ref']}}</td>
                <td>{{$plot['served_by']}}</td>

            </tr>
        @endforeach
        </tbody>


    </table>

</div>