@extends('layouts.master')
@section('title','Availability reports')

@section('content')

   <div class="panel panel-success">

       <div class="panel-heading">

       </div>
       <div class="panel panel-body">
           <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 panel-content">


               <form class="form-horizontal form-bordered">

                   <div class="form-group">

                       <label class="control-label col-md-2">Projects</label>
                       <div class="col-md-2">



                           <select class="selectpicker form-control projectSelected" data-live-search="true" data-size="7" name="project_code">
                               <option data-hidded="true">Select A Project</option>
                               @foreach($projects as $project)


                                   <option value="{{$project['project_code']}}" data-plots="{{json_encode($project['plots'])}}">{{$project['name']}}</option>
                               @endforeach
                           </select>
                       </div>
                       <label class="control-label col-md-2">Plots</label>


                       <div class="col-md-2">



                           <select class="form-control" id="plotSelected" data-live-search="true" data-size="7"  name="plot_id">

                           </select>
                       </div>


                   </div>

                   <div class="modal-footer">
                       <button class="btn btn-success" type="submit">Pull Report</button>
                   </div>




               </form>
               @include('plots.reports._available_data')

           </div>
       </div>

   </div>


@endsection

@section('extra_js_footer')

    @include('layouts.form')
    @include('layouts.dataTables.buttons')
    @include('layouts.dataTables.tablebuttons')


    <script>
        $(function () {

            $("select.selectpicker").selectpicker('refresh')


            $('select.projectSelected').on('change', function () {

                let plots = JSON.parse($("select.projectSelected option:selected").attr('data-plots'));



                let options = '';

                for(let i =0 ; i < plots.length ; i++){

                    options +='<option value="'+plots[i].id+'">Plot No '+plots[i].plotNo+'</option>'
                }

                $("select#plotSelected").append(options).selectpicker('refresh')


            })


        })
    </script>

    @endsection



