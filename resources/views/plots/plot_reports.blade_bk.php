@extends('layouts.master')
@section('title','Plots Reports')
@section('content')

    @component('layouts.partials.panel')
        <form class="form-horizontal" method="post" action="{{route('plots_manage_searchdate')}}">
            {{csrf_field()}}
            <div class="form-group">
                <label class="col-md-2">Start Date</label>
                <div class="col-md-4">
                    <input type="text" class="form-control datepicker" name="start_date">
                </div>

                <label class="col-md-2">End Date</label>
                <div class="col-md-4">
                    <input type="text" class="form-control datepicker" name="end_date">
                </div>
            </div>


            <div class="modal-footer">
                <button class="btn btn-sm btn-info" type="pull" name="search" id="search">Pull Report</button>
            </div>
        </form>
    
        <table class="table table-bordered table-responsive table-striped" id="data-table" width="100%">
            <thead>
                <tr>
                    <th>Date</th>
                    <th>Land Location</th>
                    <th>Price</th>
                    <th>Size</th>
                    <th>Plot No</th>
                    <th>Parcel No</th>
                    <th>Status</th>
                    <th>Added By</th>
                    
                </tr>
                </thead>
                <tbody>
                    @foreach($plots as $plot)
                    <tr>
                        <td>{{$plot->created_at}}</td>
                        <td>{{$plot->product->name()}}</td>
                        <td>{{number_format($plot->price , 2)}}</td>
                        <td>{{$plot->size->label}}</td>

                        <td>{{$plot->plot_no}}</td>
                        <td>{{$plot->title_no}}</td>
                        <td>
                            <?php
                            $class = "primary";
                            $isSold = "Available";

                            if ($plot->is_reserved)
                            {
                                $isSold = "Reserved";
                                $class = "warning";
                            }


                            if ($plot->is_sold) {

                                $class = "success";
                                $isSold = "Sold Out";
                            }

                            ?>

                            <span class="label label-{{$class}}">{{$isSold}}</span>
                        </td>
                        <td>{{$plot->user->fullName()}}</td>
                        </tr>
                @endforeach

                
                </tbody>
            <tbody>

            </tbody>
        </table>

    @endcomponent
@endsection

@section('extra_js_footer')

    @include('layouts.dataTables.tablebuttons')
    @include('layouts.dataTables.buttons')
    @include('layouts.datePicker.datepicker')
    <script>
        $('.datepicker').datepicker({
            minDate : new Date()
        });
        
        $(document).ready(function(){
            $('#search').click(function(e){
                e.preventDefault();
                $('#data-table').DataTable().destroy();
                 
           }); 
        });
        
    </script>

@endsection
