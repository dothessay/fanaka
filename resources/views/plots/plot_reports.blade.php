@extends('layouts.master')
@section('title','Plots Reports')
@section('content')

    @component('layouts.partials.panel')
        <form class="form-horizontal">
            {{csrf_field()}}
            <div class="input-daterange">
                <div class="form-group">
                    <label class="col-md-2">Start Date</label>
                    <div class="col-md-4">
                        <input type="text" class="form-control datepicker" name="start_date" id="start_date">
                    </div>

                    <label class="col-md-2">End Date</label>
                    <div class="col-md-4">
                        <input type="text" class="form-control datepicker" name="end_date" id="end_date">
                    </div>
                </div>
            </div>


            <div class="modal-footer">
                <button class="btn btn-sm btn-info" type="pull" name="search" id="search">Pull Report</button>
            </div>
        </form>
    
        <table class="table table-bordered table-responsive table-striped" id="data-table" width="100%">
            <thead>
                <tr>
                    <th>Date</th>
                    <th>Land Location</th>
                    <th>Price</th>
                    <th>Size</th>
                    <th>Plot No</th>
                    <th>Parcel No</th>
                    <th>Status</th>
                    <th>Added By</th>
                    
                </tr>
                </thead>
                
            <tbody>

            </tbody>
        </table>

    @endcomponent
@endsection

@section('extra_js_footer')

    @include('layouts.dataTables.tablebuttons')
    
    @include('layouts.datePicker.datepicker')
    <script>
        $('.input-daterange').datepicker({
            todayBtn:'linked',
            format: "yyyy-mm-dd",
            autoclose: true
        });
        
                
        $(document).ready(function(){
            
            fetch_data('no');
            
            function fetch_data(is_date_search, start_date='', end_date='')
            {
                $('#data-table').DataTable({
                    "processing" : true,
                    "serverSide" : true,
                    "ajax" : {
                        url:"/plots/manage/searchdate",
                        type:"POST",
                        data:{
                            is_date_search:is_date_search, start_date:start_date, end_date:end_date, _token: '{{csrf_token()}}'
                        }
                    }
                });
            }
            
            $('#search').click(function(){
                var start_date = $('#start_date').val();
                var end_date = $('#end_date').val();
                
                if(start_date != '' && end_date !='')
                {
                    $('#data-table').DataTable().destroy();
                    fetch_data('yes', start_date, end_date);
                }
                else
                {
                    alert("Both dates are required");
                }
                
                return false;
           }); 
        });
        
    </script>

@endsection
