@extends('layouts.master')
@section('title','Manage Plots Uploads for '. $plot->getPropertyDetails())

@section('content')

    <ul class="nav nav-tabs tabs-uzapoint">


        <li class="active ">
            <a href="#default-tab-1" data-toggle="tab">
                <span class="hidden-xs">New Upload</span>
            </a>
        </li>
        <li class="">
            <a href="#default-tab-2" data-toggle="tab">
                <span class="hidden-xs">Upload List</span>
            </a>
        </li>

    </ul>


    <div class="tab-content">

        <div class="tab-pane fade active  in" id="default-tab-1">

           @include('plots._upload_form')

        </div>
        <div class="tab-pane fade   in" id="default-tab-2">

            @include('plots._upload_data')
        </div>
    </div>


@endsection

@section('extra_js_footer')

    <script>

        $(document).on('click', 'button.uploadButton', function () {

            $(this).attr('disabled', true).text('Upload ........')

            let formData = new FormData($("form.plotUploadForm")[0])

            axios.post('/plots/manage/store-uploads', formData)
                .then((res) => {
                    $(this).attr('disabled', false).text('Upload')

                    $("form.plotUploadForm").trigger('reset')

                    $.growl.notice({
                        message: res.data.data
                    })

                })
                .catch((err) => {

                    $(this).attr('disabled', false).text('Upload')

                    $.growl.warning({
                        message: err.response.data.message
                    })

                })
        } )

    </script>

@endsection