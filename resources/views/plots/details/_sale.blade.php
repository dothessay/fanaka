


<div class="table-responsive">
    <table class="table-bordered table-striped table" id="data-table" width="100%">
        <thead>
        <tr>
            <th>Sale Reference</th>
            <th>Entered By</th>
            <th>Agent</th>
            <th>Approved By</th>
            <th>Customer</th>
            <th>Total Amount</th>
            <th>Balance</th>
            <th>Action</th>
        </tr>
        </thead>
        <tbody>
        @if (! is_null($sale->id))

        <?php

        $color = isset($sale->color) ? $sale->color : "";
        $class = "style=background:{$color}";

        if(! $sale->is_approved)
        {
            $class = "style=background:lightblue";
        }

        $approvedBy = isset($sale->approvedBy) ? $sale->approvedBy->fullName() : "Not Approved";
        ?>
        <tr>
            <td {{$class}}> FSL_{{$sale->id}}</td>
            <td {{$class}}> {{$sale->user->fullName()}}</td>
            <td {{$class}}> {{$sale->soldBy->fullName()}}</td>
            <td {{$class}}> {{$approvedBy}}</td>
            <td {{$class}} width="15%"> {!! $sale->getCustomerHref() !!}</td>
            <td {{$class}}> {{number_format((double)$sale->total_amount , 2)}}</td>
            <td {{$class}}> {{number_format((double)$sale->getBalance() , 2)}} </td>
            <td {{$class}}>
                @component('layouts.button')

                    @can('access-module-component-functionality','sale_manage_update')
                        @if(! $sale->is_approved)
                            <li><a href="{{url('sale/manage/approve/'.$sale->id)}}">Approve</a></li>
                            <li><a href="{{url('sale/manage/reject/'.$sale->id)}}">Cancel</a></li>
                        @endif
                    @endcan



                    <li>
                        <a href="{{url('sale/manage/receipt/'.$sale->id)}}" target="_blank">Receipt</a>
                    </li>

                    @if($sale->is_agreement_uploaded)
                        <li>
                            <a href="{{url('sale/manage/uploaded-agreement/'.$sale->id)}}" target="_blank">Scanned Agreement</a>
                        </li>
                    @endif

                    @if( (!$sale->is_agreement_uploaded) && $sale->is_agreement_downloaded)
                        <li>
                            <a data-toggle="modal"
                               data-title="Upload Scanned Agreement"
                               data-id="{{$sale->id}}"
                               id="uploadAgreementNow"
                               data-target="#UploadScannedAgreement">
                                Upload Agreement
                            </a>
                        </li>

                        <li>
                            <a href="{{url('sale/manage/agreement/'.$sale->id)}}"
                               target="_blank">Agreement</a>
                        </li>

                    @endif

                    @if( (!$sale->is_agreement_uploaded) && (! $sale->is_agreement_downloaded))

                        <li>
                            <a href="{{url('sale/manage/agreement/'.$sale->id)}}"
                               target="_blank">Agreement</a>
                        </li>

                    @endif

                    <li>
                        <a data-toggle="modal" data-target="#witness"
                           data-witness="{{($sale->witnesses)}}"
                           data-sale_id="{{($sale->id)}}"
                           data-customer_id="{{($sale->customer_id)}}"
                           id="addWitness">Witnesses</a>
                    </li>
                @endcomponent
            </td>
        </tr>
        @endif
        </tbody>
    </table>






    <h4 style="text-align: center; text-transform: capitalize">Plot Details</h4>

    <table class="table table-bordered table-striped" id="data-table" width="100%" cellspacing="0"
           cellpadding="0">
        <thead>
        <tr>
            <th>Land Location</th>
            <th>Price</th>
            <th>Size</th>
            <th>Plot No</th>
            <th>Parcel No</th>
            <th>Parcel No Issued</th>
            <th>Issued By</th>
            <th>Date Issued</th>

        </tr>
        </thead>
        <tbody>

        @foreach($sale->saleItems as $saleItem)
            <tr>
                <?php
                $plot = $saleItem->plot;


                ?>

                <td>{{$plot->product->name()}}</td>


                <td>{{number_format($saleItem->price , 2)}}</td>
                <td>{{$plot->size->label}}</td>

                <td>{{$plot->plot_no}}</td>
                <td>{{$plot->title_no}}</td>

                @php

                    if ($plot->is_title_issued )
                    { $titleIssued= 'primary';
                    }
                    else{

                    $titleIssued = "warning";

                    }


                @endphp
                <td>
                    <span class="label label-{{$plot->is_title_issued ? "primary" : "danger" }}">{{$plot->is_title_issued ? "Issued" : "Not Issued" }}</span>
                </td>
                <td>{{$plot->issuedBy? $plot->issuedBy->fullName() : ""}}</td>
                <td>{{$plot->date_issued}}</td>
            </tr>
        @endforeach
        </tbody>
    </table>

    <h4 style="text-align: center; text-transform: uppercase">Payment Details</h4>

    <table class="table table-bordered table-striped data-table" width="100%">
        <thead>

        <tr>
            <th>Date Deposited</th>
            <th>Date Received</th>
            <th>Amount Paid</th>
            <th>Payment Method</th>
            <th>Reference No</th>
            <th>Received By</th>
        </tr>

        </thead>

        <tbody>

        @foreach($sale->salePayments as $payment)


            <tr>
                <td>{{$payment->deposit_date}}</td>
                <td>{{$payment->created_at}}</td>
                <td>{{number_format($payment->amount , 2)}}</td>
                <td>{{$payment->payment_method}}</td>
                <td>{{$payment->reference_code}}</td>
                <th>{{$payment->user->fullName()}}</th>
            </tr>

        @endforeach
        </tbody>
        <tfoot>
        <tr>
            <td colspan="2"><strong>Totals</strong></td>
            <td colspan="9"><strong>{{number_format($sale->salePayments->sum('amount') , 2)}}</strong></td>
        </tr>
        </tfoot>


    </table>

    <h4 style="text-align: center; text-transform: uppercase">Scanned Documents</h4>

    <table class="table table-bordered table-striped data-table" width="100%">
        <thead>

        <tr>
            <th>Date Scanned</th>
            <th>Document Type</th>
            <th>View Document</th>

        </tr>

        </thead>

        <tbody>

        <?php $sumPaid = [];?>

        @foreach($sale->saleUploads->map(function ($q) { return $q->uploadsToArray() ;}) as $key => $upload)


            <tr>
                <td>{{$upload['date']}}</td>
                <td>{{ $upload['type']}}</td>
                <td><a data-toggle="modal" data-target="#viewDocument" class="view-document" data-file-name="{{$upload['name']}}" data-url="{{$upload['url']}}" target="_blank">View</a>  </td>


            </tr>

        @endforeach
        </tbody>


    </table>


</div>

