@extends('layouts.master')
@section('title',' Reports')
@section('content')
    <div class="form-group col-md-12">
        {!! \Codex\Classes\Helper::reportWidget('Availability Report','plots_reports_available','Show plots available report',true) !!}

    </div>
@endsection