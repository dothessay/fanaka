<table class="table">
    <thead>
    <tr>
        <th>Plot No</th>
        <th>Upload Type</th>
        <th>Uploaded By</th>
        <th>Document Name</th>
        <th>Download</th>
    </tr>
    </thead>

    <tbody>
    @foreach($uploads as $upload)

        <tr>
            <td>{{$upload['plot']}}</td>
            <td>{{$upload['type']}}</td>
            <td>{{$upload['uploaded_by']}} </td>
            <td>{{$upload['file_name']}} </td>
            <td>
                <a href="{{url('plots/manage/download-uploads/' . $upload['id'])}}" class="badge badge-secondary">Download</a>
            </td>
        </tr>

        @endforeach
    </tbody>

</table>