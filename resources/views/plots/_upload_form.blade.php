<form class="form-bordered form-horizontal plotUploadForm" enctype="multipart/form-data" method="post" action="{{route('plots_manage_store-uploads')}}">

    @csrf
    <div class="form-group">
        <label class="col-md-2 control-label">File</label>
        <div class="col-md-4">
            <input type="file" name="doc_file">
            <input type="hidden" name="id" value="{{$plot->id}}">
        </div>

        <label class="col-md-1 control-label">Type</label>
        <div class="col-md-4">
            <select class="form-control" name="type">

                <option>Title Deed</option>
                <option>Title collection Acknowledgement note</option>
            </select>
        </div>

    </div>

    <div class="modal-footer">
        <button type="submit" class="btn btn-success">Upload</button>
    </div>
</form>