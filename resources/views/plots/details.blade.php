@extends('layouts.master')
@section('title','plot details for '. $plot->getPropertyDetails() )

@section('content')

    <ul class="nav nav-tabs tabs-uzapoint">

        <?php $active = true ? "active" : "" ?>
        @can('access-module-component-functionality','plots_manage_add')
            <li class="{{$active}} ">
                <a href="#default-tab-1" data-toggle="tab">
                    <span class="">Sale</span>
                </a>
            </li>
            <?php $active = "" ?>
        @endcan


        @can('access-module-component-functionality','plots_manage_mark-can-sell')
            <li class="{{$active}} ">
                <a href="#default-tab-2" data-toggle="tab">
                    <span class="">Offer</span>
                </a>
            </li>


            <li class="">
                <a href="#default-tab-3" data-toggle="tab">
                    <span class="">Sale Payment Request <span class="badge badge-danger">{{$paymentRequests->count()}}</span></span>
                </a>
            </li>

        @endcan


    </ul>


    <div class="tab-content">
        <?php $isActive = true ? "active" : ""; ?>

        @can('access-module-component-functionality','plots_manage_add')
            <div class="tab-pane fade {{$isActive}}  in" id="default-tab-1">



                @include('plots.details._sale', ['sale' => $sale])
            </div>
            <?php $isActive = false?>
        @endcan



        @can('access-module-component-functionality','plots_manage_add')
            <div class="tab-pane fade {{$isActive}}  in" id="default-tab-2">



                @include('plots.details._offer', ['offer' => $offer])
            </div>
                <?php $isActive = false?>
        @endcan


        @can('access-module-component-functionality','plots_manage_add')
            <div class="tab-pane fade {{$isActive}}  in" id="default-tab-3">



                @include('sales._payment_request', ['paymentRequests' => $paymentRequests])
            </div>
                <?php $isActive = false?>
        @endcan
    </div>

    @component('layouts.partials.modal')

        @slot('dialogClass','modal-lg')
        @slot('id','viewDocument')
        @slot('title') <h6>Scanned Document</h6>@endslot


        <div class="document-details"></div>

    @endcomponent
@stop

@section('extra_js_footer')

    <script>
        $(document).on('click', 'a.view-document', function () {

            let source = $(this).data("url")


            $("div.document-details").empty();

            let filename = $(this).data('file-name');

            let pieces = filename.split(".");

            if (pieces[1] == 'pdf'){

                $("div.document-details").append('<iframe src="'+source+'" style="width:718px; height:700px;" frameborder="0"></iframe>');

                return;
            }


            $("div.document-details").append("<img src='"+source+"' class='img-responsive thumbnail'>");


        });



        /**
         *
         * APPROVE PAYMENT
         */

        $(document).on('click','a.approve-payment-alert', function () {

            let id = $(this).data('id');

            swal({
                title: "Are you sure?",
                text: "Once you have approved this payment it cannot be undone!",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
                .then((willDelete) => {
                    if (willDelete) {

                        axios.get('/sale/manage/payment/confirm?id='+id).then((res) => {

                            swal(res.data.message, {
                                icon: "success",
                            });


                            window.location.reload(true)

                        }).catch((err) => {})

                    }
                });

        })


        /**
         *
         * Reject PAYMENT
         */

        $(document).on('click','a.reject-payment-alert', function () {

            let id = $(this).data('id');

            swal({
                title: "Are you sure?",
                text: "Once you have rejected this payment cannot be undone!",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
                .then((willDelete) => {
                    if (willDelete) {

                        axios.get('/sale/manage/payment/reject?id='+id).then((res) => {

                            swal(res.data.message, {
                                icon: "success",
                            });

                            window.location.reload(true)

                        }).catch((err) => {})

                    }
                });

        })



        $(document).on('click','a.update-payment-alert', function () {


            let myId = $(this).data('id')

            let amount = $("input#amount_"+myId).val();
            let reference = $("input#reference_"+myId).val();

            let formData = {
                id: myId,
                amount: amount,
                reference_code: reference,
                _token: "{{csrf_token()}}"
            }

            swal({
                title: "Are you sure?",
                text: "Note updating amount will alter the balance and calculation , confirm the figures before updating",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
                .then((willDelete) => {
                    if (willDelete) {

                        axios.post('/sale/manage/payment/update', formData).then((res) => {

                            swal(res.data.message, {
                                icon: "success",
                            });

                            window.location.reload(true)

                        }).catch((err) => {

                        })

                    }
                });




        })

    </script>
@stop

