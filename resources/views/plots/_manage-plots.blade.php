<div id="id">

    <form class="form-horizontal form-bordered">

        <div class="form-group">

            <label class="control-label col-md-2">Projects</label>
            <div class="col-md-2">



                <select class="selectpicker form-control projectSelected" data-live-search="true" data-size="7" name="product_code">
                    <option data-hidded="true">Select A Project</option>
                    @foreach($projects as $project)


                        <option value="{{$project->code}}" >{{$project->name()}}</option>
                    @endforeach
                </select>
            </div>
            <div class="modal-footer">

                <button class="btn btn-success">Pull</button>
            </div>
        </div>
    </form>
    <div class="table-responsive">
        <table class="table-bordered table-striped table" id="data-table" width="100%">
            <thead>
            <tr>
                <th>Date</th>
                <th>Land Location</th>
                <th>Price</th>
                <th>Size</th>
                <th>Plot No</th>
                <th>Parcel No</th>
                <th>Parcel No Issued</th>
                <th>Issued By</th>
                <th>Date Issued</th>
                <th>Status</th>
                <th>Added By</th>
                <th>Action</th>
            </tr>
            </thead>
            <tbody>
            @foreach($plots as $plot)
                <tr>
                    <td>{{$plot->created_at}}</td>
                    <?php

                    if ( $plot->available()) {

                    ?>
                    <td><a href="{{url('sale/manage/create-sale/'.$plot->id)}}">{{$plot->product->name()}}</a>
                    </td><?php
                    } else {
                    ?>
                    <td>{{$plot->product->name()}}</td>
                    <?php


                    } ?>

                    <td>{{number_format($plot->price , 2)}}</td>
                    <td>{{$plot->size->label}}</td>

                    <td>{{$plot->plot_no}}</td>
                    <td>{{$plot->title_no}}</td>

                    @php

                        if ($plot->is_title_issued )
                        { $titleIssued= 'primary';
                        }
                        else{

                        $titleIssued = "warning";

                        }


                    @endphp
                    <td>
                        <span class="label label-{{$plot->is_title_issued ? "primary" : "danger" }}">{{$plot->is_title_issued ? "Issued" : "Not Issued" }}</span>
                    </td>
                    <td>{{$plot->issuedBy? $plot->issuedBy->fullName() : ""}}</td>
                    <td>{{$plot->date_issued}}</td>
                    <td>
                        <?php
                        $class = "primary";
                        $isSold = "Available";

                        if ($plot->is_reserved) {
                            $isSold = "Reserved";
                            $class = "danger";
                        }


                        if ($plot->is_sold) {

                            $class = "warning";
                            $isSold = "Sold (Payment in Progress)";
                        }


                        if ($plot->is_completely_paid) {

                            $class = "success";
                            $isSold = "Sold Out";
                        }

                        ?>


                        <span class="label label-{{$class}}">
                        <a href="{{url('plots/manage/details/'.$plot->id)}}">{{$isSold}}</a>
                    </span>
                    </td>
                    <td>{{$plot->user->fullName()}}</td>
                    <td>
                        @component('layouts.button')
                            @can('access-module-component-functionality','plots_manage_update')
                                <li><a data-toggle="modal" data-target="#updateProduct"
                                       id="update"
                                       data-id="{{$plot->id}}"
                                       data-product-id="{{$plot->product->code}}"
                                       data-product-name="{{$plot->product->name}}"
                                       data-price="{{$plot->price}}"
                                       data-size="{{$plot->size->id}}"
                                       data-size-label="{{$plot->size->label}}"
                                       data-plot_no="{{$plot->plot_no}}"
                                       data-title_no="{{$plot->title_no}}"
                                    >Update</a></li>

                                <li>
                                    <a href="{{url('plots/manage/uploads/'.$plot->id)}}">Uploads</a>
                                </li>

                            @endcan





                            @if( ! $plot->is_title_issued  )
                                @can('access-module-component-functionality','plots_manage_issue-title-deed')
                                    <li><a href="{{url('plots/manage/issue-title/'.$plot->id)}}">Issue Title</a>
                                    </li>
                                @endcan

                            @endif

                            @can('access-module-component-functionality','plots_manage_delete')
                                <li><a data-toggle="modal" data-target="#delete-product" id="code"
                                       data-code="{{$plot->id}}">Delete</a></li>
                            @endcan

                            @can('access-module-component-functionality','plots_manage_release')
                                <li>
                                    <a data-toggle="modal" data-target="#available" id="markAsAvailable"
                                       data-id="{{$plot->id}}">Un Reserved</a>
                                </li>
                            @endcan
                        @endcomponent
                    </td>
                </tr>
            @endforeach

            </tbody>


        </table>
    </div>



    @component('layouts.partials.modal')
        @slot("id","addProduct")
        @slot('dialogClass','modal-lg')
        @slot('title')<h6>
            Fill the form below to add a plot
        </h6>@endslot


        <form class="form-horizontal form-bordered" method="post" action="{{route('plots_manage_store')}}">
            {{csrf_field()}}
            <div class="form-group">
                <label class="control-label col-md-2">Plot Name <span class="required-uzapoint">*</span> </label>
                <div class="col-md-4">
                    <select name="product_code" class="form-control selectpicker" data-live-search="true" data-size="5">

                        @foreach($products as $product)

                            <option value="{{$product->code}}">{{$product->name}}</option>

                        @endforeach

                    </select>
                </div>

                <label class="control-label col-md-2">Price <span class="required-uzapoint">*</span> </label>
                <div class="col-md-4">
                    <input type="text" class="form-control" name="price" placeholder="500000">
                </div>
            </div>


            <div class="form-group">


                <label class="control-label col-md-2">Size <span class="required-uzapoint">*</span> </label>
                <div class="col-md-4">

                    <select name="size_id" class="form-control selectpicker" data-live-search="true" data-size="5">

                        @foreach($sizes as $size)

                            <option value="{{$size->id}}">{{$size->label}}</option>

                        @endforeach

                    </select>

                </div>


                <label class="control-label col-md-2">Plot No <span class="required-uzapoint">*</span> </label>
                <div class="col-md-4">
                    <input type="text" class="form-control" name="plot_no" placeholder=" 9">
                </div>
            </div>

            <div class="form-group">

                <label class="control-label col-md-2">Title Serial No </label>
                <div class="col-md-4">
                    <input type="text" class="form-control" name="title_no" placeholder=" 9">
                </div>


            </div>


            <div class="modal-footer">
                <button type="button" class="btn btn-success addPlot">Submit</button>
                <button type="submit" class="btn btn-success savePlot hidden">Submit</button>
            </div>
        </form>

    @endcomponent
    @component('layouts.partials.modal')
        @slot("id","updateProduct")
        @slot('dialogClass','modal-lg')
        @slot('title')<h6>
            Fill the form below to update a
        </h6>@endslot


        <form class="form-horizontal form-bordered" method="post" action="{{route('plots_manage_update')}}">
            {{csrf_field()}}
            <div class="form-group">
                <label class="control-label col-md-2">Plot Name <span class="required-uzapoint">*</span> </label>
                <div class="col-md-4">
                    <select name="product_code" class="form-control" id="product-update" data-live-search="true"
                            data-size="5">

                        <span id="product_option"></span>
                        @foreach($products as $product)

                            <option value="{{$product->code}} ">{{$product->name()}}</option>

                        @endforeach

                    </select>
                </div>

                <label class="control-label col-md-2">Price <span class="required-uzapoint">*</span> </label>
                <div class="col-md-4">
                    <input type="text" class="form-control" id="price" name="price" placeholder="500000">
                    <input type="hidden" class="form-control" id="id" name="id" placeholder="500000">
                </div>
            </div>


            <div class="form-group">


                <label class="control-label col-md-2">Size <span class="required-uzapoint">*</span> </label>
                <div class="col-md-4">

                    <select name="size_id" id="size" class="form-control" data-live-search="true" data-size="5">

                        @foreach($sizes as $size)

                            <option value="{{$size->id}}">{{$size->label}}</option>

                        @endforeach

                    </select>

                </div>


                <label class="control-label col-md-2">Plot No <span class="required-uzapoint">*</span> </label>
                <div class="col-md-4">
                    <input type="text" class="form-control" id="plot_no" name="plot_no" placeholder=" 9">
                </div>

            </div>

            <div class="form-group">


                <label class="control-label col-md-2">Parcel No </label>
                <div class="col-md-4">
                    <input type="text" class="form-control" id="title_no" name="title_no" placeholder=" 9">
                </div>


            </div>


            <div class="modal-footer">
                <button type="submit" class="btn btn-success">Submit</button>
            </div>
        </form>

    @endcomponent



    @component('layouts.partials.modal')
        @slot('id','delete-product')
        @slot('title')
            <h6></h6>
        @endslot

        <p>
        <h5>ARE YOU YOU WANT TO DELETE THIS PLOT </h5>
        <h6>The following will be affected</h6>
        <ul>
            <li>All sales belonging to this plot will be lost forever</li>
        </ul>

        <form method="post" action="{{route('plots_manage_delete')}}">
            {{csrf_field()}}
            <input type="hidden" id="plot_code" name="code">


            <div class="modal-footer">
                <button type="submit" class="btn btn-danger">Delete</button>
            </div>
        </form>

        </p>
    @endcomponent

    @component('layouts.partials.modal')
        @slot('id','available')
        @slot('title')
            <h6></h6>
        @endslot

        <p>
        <h5>ARE YOU YOU WANT TO MARK THIS PLOT AS AVAILABLE </h5>
        <h6>The following will be affected</h6>
        <ul>
            <li>All sales belonging to this plot will be lost forever</li>
        </ul>

        <form method="post" action="{{route('plots_manage_mark-available')}}">
            {{csrf_field()}}
            <input type="hidden" id="plot_id_available" name="id">


            <div class="modal-footer">
                <button type="submit" class="btn btn-danger">Mark Available</button>
            </div>
        </form>

        </p>
    @endcomponent

</div>


