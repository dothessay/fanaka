@extends('layouts.master')
@section('title','Customer Referrals')
@section('content')

    @component('layouts.partials.panel')

        @include('customers.referrals._data', ['referrals' => $referrals])

    @endcomponent
@stop

