<div class="table-responsive">
    <table class="table table-bordered table-striped">
        <thead>
        <tr>
            <th>Date</th>
            <th>Customer</th>
            <th>Phone Number</th>
            <th>Agent</th>
            <th>Contacted</th>
        </tr>
        </thead>
        <tbody>
        @foreach($referrals as $referral)
            <tr>
                <td>{{$referral['date']}}</td>
                <td>{{$referral['fullName']}}</td>
                <td>{{$referral['phone_number']}}</td>
                <td><a href="{{$referral['agentUrl']}}">{{$referral['agent']}}</a></td>
                <td>{!! $referral['is_contacted'] ? "<span class='badge badge-success'>Yes</span>" :
                         "<span class='badge badge-warning'><a href='".url('customer-referrals/contact/'.$referral['id'])."'>Mark as contacted</a> </span>" !!}</td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>

<style>
    span a {
        color: #FFF;
    }

    span a:hover {
        color: #FFF;
        text-decoration: none;
    }
</style>