<div class="table-responsive">
    <table class="table-bordered table-striped customer-table table" id="data-table" width="100%">
        <thead>
        <tr>
            <th>Date</th>
            <th>Ref</th>
            <th>Photo</th>
            <th>Full Name</th>
            <th>Email</th>
            <th>Phone Number</th>
            <th>ID No</th>
            <th>Registered By</th>
            <th>Offers</th>
            <th>Sales</th>

            <th>Referrals</th>
            <th>Action</th>
        </tr>
        </thead>
        <tbody>
        @foreach($customers as $customer)
            <tr>


                <td>{{$customer->created_at}}</td>
                <td>FCT/{{$customer->id}}</td>
                <td><img width='50px' height='50px' src='{{ $customer->getImage() }}'
                         class="img-responsive img-circle img-rounded"></td>
                <td>
                    <a href="{!! url('customers/manage/statement/'.$customer->id) !!}"> {{$customer->fullName()}}</a>
                </td>
                <td>{{$customer->email}}</td>
                <td>{{$customer->phone_number}}</td>
                <td>{{$customer->id_no}}</td>
                <td>{{$customer->user->fullName()}}</td>
                <td><a href="{{url('customers/manage/statement/'.$customer->id.'?tab=offers')}}" class="badge badge-success"><strong>{{$customer->offers->count()}}</strong></a></td>
                <td><a href="{{route('customers_manage_statement',['id' => $customer->id,'tab=sales'])}}" class="badge badge-warning"><strong>{{$customer->sales->count()}}</strong></a></td>
                <td><a href="{{route('customers_manage_referrals',['id' => $customer->id])}}" class="badge badge-danger"><strong>{{$customer->referrals->count()}}</strong></a></td>

                <td>
                    @component('layouts.button')
                        <li>
                            <a data-name="{{$customer->fullName()}}"
                               data-id="{{$customer->id}}" data-toggle="modal" class="customer-image"
                               data-target="#upload">Upload
                                Image</a>
                        </li>
                        @can('access-module-component-functionality','customers_manage_update')
                            <li><a data-toggle="modal" id="edit" data-target="#update-customer"
                                   data-firstname="{{$customer->first_name}}"
                                   data-lastname="{{$customer->last_name}}"
                                   data-middlename="{{$customer->middle_name}}"
                                   data-phone="{{$customer->phone_number}}" data-email="{{$customer->email}}"
                                   data-id-no="{{$customer->id_no}}" data-id="{{$customer->id}}">Update</a></li>
                        @endcan

                        @can('access-module-component-functionality','customers_manage_delete')
                            <li><a data-toggle="modal" data-target="#delete-product" id="id"
                                   data-id="{{$customer->id}}">Delete</a></li>
                        @endcan
                    @endcomponent
                </td>
            </tr>
        @endforeach

        </tbody>


    </table>
</div>
