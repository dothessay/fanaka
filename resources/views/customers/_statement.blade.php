<div class="table-responsive">

    <style>
        .img-circle {
            border-radius: 50%;
            margin: auto !important;
        }

        table.offer:nth-child(odd){
            background: lightblue;
        }
        table.sales:nth-child(odd){
            background: lightblue;
        }
    </style>
        <h4 style="text-align: center; text-transform: capitalize">{{__('Sales')}}</h4>

        <table class="table table-bordered table-striped sales" id="data-table" width="100%" cellspacing="0"
               cellpadding="0">
            <thead>
            <tr>
                <th>REF NO</th>
                <th>Agent</th>
                <th>Sale Amount</th>
                <th>Amount Paid</th>
                <th>Amount PrePaid</th>
                <th>Balance</th>
                <th>Project</th>
                <th>view</th>

            </tr>
            </thead>
            <tbody>

            <?php $total = 0  ; $balance = 0 ; $saleAmount = 0;?>

            @foreach($customer->sales as $sale)

                <?php $total += $sale->getPaidAmount() ; $balance += $sale->getBalance(); $saleAmount += $sale->getTotalSaleAmount(); ?>
                <tr>
                    <td>{{$sale->id}}</td>
                    <td><a href="{{route('users_manage_details',['user' => $sale->soldBy->id])}}">{{$sale->soldBy->fullName()}}</a> </td>
                    <td>{{number_format($sale->getTotalSaleAmount() , 2)}}</td>
                    <td>{{number_format($sale->getPaidAmount() , 2)}}</td>
                    <td>{{ number_format($sale->getPrepaidAmount() , 2)}}</td>
                    <td>{{ number_format($sale->getBalance() , 2)}}</td>
                    <td>{{ $sale->getCustomerPlotNos() }}</td>
                    <td><a href="{{url('sale/manage/approve-sale/'.$sale->id)}}" class="badge badge-success">View</a></td>
                </tr>



            @endforeach
            </tbody>
            <tfoot>
            <tr>
                <td colspan="2"><strong>Totals</strong></td>
                <td><strong>{{number_format(floatval($saleAmount) , 2)}}</strong></td>
                <td><strong>{{number_format(floatval($total) , 2)}}</strong></td>
                <td><strong>{{number_format(floatval($balance) , 2)}}</strong></td>
            </tr>
            </tfoot>
        </table>



    <!-----END OF SALES---->


    @component('layouts.partials.modal')

        @slot('dialogClass','modal-lg')
        @slot('id','viewDocument')
        @slot('title') <h6>Scanned Document</h6>@endslot


        <div class="document-details"></div>

    @endcomponent



</div>
