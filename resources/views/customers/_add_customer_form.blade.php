{{csrf_field()}}

<div class="customer-add-form">
    <div class="show-client-form">
        <div class="form-group">
            <label class="control-label col-md-2">{{__('Customer Type')}}</label>
            <div class="col-md-4">
                <select
                        name="type"
                        class="form-control selectpicker"
                        data-live-search="true"
                        id="customer_type"
                >
                    @foreach(\Codex\Classes\Helper::system()->customerTypes as $type)

                        <option>{{$type}}</option>
                    @endforeach
                </select>
            </div>
        </div>


        <div class="form-group">
            <label class="control-label col-md-2">First Name <span class="required-uzapoint">*</span> </label>
            <div class="col-md-4">
                <input type="text" class="form-control" id="first_name" name="first_name[]">
            </div>

            <label class="control-label col-md-2">Last Name <span class="required-uzapoint">*</span> </label>
            <div class="col-md-4">
                <input type="text" class="form-control" id="last_name" name="last_name[]" placeholder="Doe">
            </div>
        </div>


        <div class="form-group">
            <label class="control-label col-md-2">Middle Name <span class="required-uzapoint">*</span> </label>
            <div class="col-md-4">
                <input type="text" class="form-control" id="last_name" name="middle_name[]" value=".">
            </div>


            <label class="control-label col-md-2" for="id_no">Id No <span class="required-uzapoint">*</span> </label>
            <div class="col-md-4">
                <input type="text" id="id_no" class="form-control" name="id_no[]" oninput="customer.checkIfIdExist()"
                       placeholder="12345678">
                <div class="help-block">
                    <span id="invalid_id"></span>
                </div>

            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-md-2" for="email">Email <span class="required-uzapoint">*</span> </label>
            <div class="col-md-4">
                <input type="email" id="email" class="form-control" name="email[]"
                       oninput="customer.checkIfEmailExist()">
                <div class="help-block">
                    <span id="invalid_email"></span>
                </div>
            </div>

            <label class="control-label col-md-2">Phone number <span class="required-uzapoint">*</span> </label>
            <div class="col-md-4">
                <input type="text" class="form-control" name="phone_number[]">


            </div>


        </div>


    </div>


    <div class="form-group">
        <label for="" class="col-md-2">
        </label>
        <button class="btn-success show-form-button" type="button" onclick="customer.showForm()"><i
                    class="fa fa-plus-circle show-form">
                <span onload="customer.showingForm">Show Customer Form</span>
            </i>
        </button>
        <button class="btn-info" type="button" onclick="customer.moreCustomers()">
            More Customers
        </button>

        <div class="duplicate-here"></div>

    </div>

    <hr/>

</div>

<div class="form-group">
    <label class="control-label col-md-2">Create a sale</label>
    <div class="col-md-4">
        <input
                type="checkbox"
                name="new_sale"
                class="create_sale"
        >
    </div>
</div>

<div class="new-sale-form hidden">
    @include('customers.profile._new-sale')
</div>


<div class="modal-footer">
    <button type="submit" class="btn btn-success">Submit</button>
</div>
