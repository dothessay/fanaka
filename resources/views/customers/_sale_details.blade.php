

<h4 style="text-align: center; text-transform: uppercase">{{$sale->customer->fullName()}}</h4>
<h4 style="text-align: center;">{!! $sale->customer->contactPoint() !!}</h4>



<h4 style="text-align: center; text-transform: capitalize">Sale Details</h4>

<table class="table table-bordered table-striped" id="data-table" width="100%" cellspacing="0"
       cellpadding="0">
    <thead>
    <tr>
        <th>REF NO</th>
        <th>Amount Paid</th>
        <th>Balance</th>

    </tr>
    </thead>
    <tbody>
    <tr>
        <td>FSL_{{$sale->sale->id}}</td>
        <td>{{$total = number_format($sale->sale->salePayments->sum('amount') , 2)}}</td>
        <td>{{$balance = number_format($sale->sale->getBalance() , 2)}}</td>
    </tr>
    </tbody>
    <tfoot>
    <tr>
        <td colspan="0"><strong>Totals</strong></td>
        <td><strong>{{$total}}</strong></td>
        <td><strong>{{$balance}}</strong></td>
    </tr>
    </tfoot>
</table>

<h4 style="text-align: center; text-transform: capitalize">Plot Details</h4>

<table class="table table-bordered table-striped" id="data-table" width="100%" cellspacing="0"
       cellpadding="0">
    <thead>
    <tr>
        <th>Land Location</th>
        <th>Price</th>
        <th>Size</th>
        <th>Plot No</th>
        <th>Parcel No</th>
        <th>Parcel No Issued</th>
        <th>Issued By</th>
        <th>Date Issued</th>

    </tr>
    </thead>
    <tbody>

    @foreach($sale->sale->saleItems as $saleItem)
        <tr>
            <?php
            $plot = $saleItem->plot;


            ?>

            <td>{{$plot->product->name()}}</td>


            <td>{{number_format($saleItem->price , 2)}}</td>
            <td>{{$plot->size->label}}</td>

            <td>{{$plot->plot_no}}</td>
            <td>{{$plot->title_no}}</td>

            @php

                if ($plot->is_title_issued )
                { $titleIssued= 'primary';
                }
                else{

                $titleIssued = "warning";

                }


            @endphp
            <td>
                <span class="label label-{{$plot->is_title_issued ? "primary" : "danger" }}">{{$plot->is_title_issued ? "Issued" : "Not Issued" }}</span>
            </td>
            <td>{{$plot->issuedBy? $plot->issuedBy->fullName() : ""}}</td>
            <td>{{$plot->date_issued}}</td>
        </tr>
    @endforeach
    </tbody>
</table>

<h4 style="text-align: center; text-transform: uppercase">Payment Details</h4>

<table class="table table-bordered table-striped data-table" width="100%">
    <thead>

    <tr>
        <th>Date Deposited</th>
        <th>Amount Paid</th>
        <th>Payment Method</th>
        <th>Reference No</th>

        <th>Received By</th>
    </tr>

    </thead>

    <tbody>

    @foreach($sale->sale->salePayments as $payment)


        <tr>
            <td>{{$payment->created_at}}</td>
            <td>{{number_format($payment->amount , 2)}}</td>
            <td>{{$payment->payment_method}}</td>
            <td>{{$payment->reference_code}}</td>
            <th>{{$payment->user->fullName()}}</th>
        </tr>

    @endforeach
    </tbody>
    <tfoot>
    <tr>
        <td><strong>Totals</strong></td>
        <td colspan="9"><strong>{{number_format($sale->sale->salePayments->sum('amount') , 2)}}</strong></td>
    </tr>
    </tfoot>


</table>

@if($groups->count())
    <h4 style="text-transform: uppercase; text-align: center">Partners</h4>

    <table class="table table-bordered table-striped">
        <thead>
        <tr>
            <th>Name</th>
            <th>Id</th>
            <th>Phone Number</th>
        </tr>
        </thead>
        <tbody>

        @foreach($groups as $group)
            <tr>
                <td>{{$group['name']}}</td>
                <td>{{$group['id']}}</td>
                <td>{{$group['phone_number']}}</td>
            </tr>
        @endforeach

        </tbody>
    </table>

@endif