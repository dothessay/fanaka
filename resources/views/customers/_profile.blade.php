@extends('layouts.adminLte.layouts.master')
@section('title',' '.$customer->fullName())

@section('extra_css_header')

    <style>
        .img-circle {
            border-radius: 50%;
            margin: auto !important;
        }

        table.offer:nth-child(odd) {
            background: lightblue;
        }

        table.sales:nth-child(odd) {
            background: lightblue;
        }
    </style>

@endsection
@section('content')


    <a href="{{ route('customers_manage') }}" class="btn btn-primary btn-dark mb-2"> Back </a>


    <div class="row">
        <div class="col-md-3">



            <!-- Profile Image -->
            <div class="box box-primary">
                <div class="box-body box-profile">
                    <img class="profile-user-img img-responsive img-circle" src="{{ $customer->getImage() }}"
                         alt="User profile picture">

                    <h3 class="profile-username text-center">{{ $customer->fullName() }}</h3>

                    <ul class="list-group list-group-unbordered">
                        <li class="list-group-item">
                            <b>ID: </b> <a class="pull-right">{{ $customer->id_no }}</a>
                        </li>
                        <li class="list-group-item">
                            <b>Tel</b> <a class="pull-right">{{ $customer->phone_number }}</a>
                        </li>
                        <li class="list-group-item">
                            <b>Email</b> <a class="pull-right">{{ $customer->email }}</a>
                        </li>
                        <li class="list-group-item">
                            <b>Agent</b> <a class="pull-right">{{ $customer->user->fullName() }}</a>
                        </li>
                        <li class="list-group-item">
                            <b>Sales</b> <a class="pull-right">{{ $customer->sales->count() }}</a>
                        </li>
                    </ul>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
        <!-- /.col -->
        <div class="col-md-9">

            <div class="nav-tabs-custom">
                <ul class="nav nav-tabs">
                    <li class="active"><a href="#sales" data-toggle="tab">Sales</a></li>
                    <li><a href="#documents" data-toggle="tab">Documents</a></li>
                    <li><a href="#newSale" onclick="newSale.getProjects()" data-toggle="tab">New Sale</a></li>
                    <li><a href="#edit"  data-toggle="tab">Edit Details</a></li>
                    <li><a href="#referrals" data-toggle="tab">Referrals</a></li>
                    <li><a href="#unpaid" data-toggle="tab">UnPaid Sales</a></li>
                </ul>
                <div class="tab-content">
                    <div class="active tab-pane" id="sales">
                        <!-- sales -->
                    @include('sales._list_table',['sales' => $customer->sales] )
                    <!-- /.sales -->
                    </div>
                    <!-- /.tab-pane -->
                    <div class="tab-pane" id="documents">
                        <a href="{{ route('customers_manage_upload_create', ['customer' => $customer->id]) }}"> New
                            Uploads </a>
                        <table class="table table-bordered table-striped">
                            <thead>
                            <tr>
                                <th>Customer</th>
                                <th>Document Type</th>
                                <th>Status</th>
                                <th>View</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($customer->uploads->map->uploadsToArray() as $file)
                                <tr>
                                    <td>{{$file['customer']}}</td>
                                    <td>{{$file['type']}} {{ $file['meme_type'] }}</td>
                                    <td>
                                        @if (! $file['isSeen'])
                                            <span class="badge badge-warning">Not Seen</span>
                                        @else


                                            <span class="badge badge-success"> Seen</span>
                                        @endif

                                    </td>
                                    <td>
                                        @component('layouts.button')
                                            <li>
                                                <a
                                                        class="view-document"
                                                        data-url="{{$file['url']}}"
                                                        data-file_name="{{ $file['meme_type'] }}"
                                                        data-toggle="modal"
                                                        data-target="#viewFile"

                                                >
                                                    View
                                                </a>
                                            </li>
                                            @if( ! $file['isSeen'])
                                                <li>
                                                    <a
                                                            href="{{route('customer-uploads-mark-as-seen' ,['file' => $file['id']])}}"
                                                    >
                                                        Mark As Seen
                                                    </a>
                                                </li>
                                            @endif
                                        @endcomponent
                                    </td>

                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                    <!-- /.tab-pane -->

                    <div class="tab-pane" id="edit">
                        @include('customers.profile.edit_customer',  ['customer' => $customer])
                    </div>
                    <!-- /.tab-pane -->

                    <div class="tab-pane" id="referrals">
                        @include('customers.profile.edit_customer',  ['customer' => $customer])
                    </div>
                    <div class="tab-pane" id="newSale">

                        <form
                                class="form-horizontal form-bordered"
                                method="post"
                                id="newSaleForm"
                                action="{{ route('sale_manage_v2_store') }}"
                        >
                            @csrf
                            <input type="hidden" name="customer_id[]" value="{{ $customer->id }}">
                            @include('customers.profile._new-sale' , ['customer' => $customer])
                            <div class="email-footer">
                                <button
                                        type="button"
                                        onclick="newSale.createSale()"
                                        class="btn btn-success form-control"
                                > Save
                                </button>
                            </div>
                        </form>

                    </div>
                    <!-- /.tab-pane -->

                    <div class="tab-pane" id="unpaid">
                        <div class="table-responsiv">
                            <table class="table-bordered table-striped table" id="data-table" width="100%">
                                <thead>
                                <tr>
                                    <th>Total Sale Amount</th>
                                    <th>Total Amount Paid</th>
                                    <th>Total Balance</th>
                                    <th>Due Date</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($customer->invoices as $invoice)

                                    @if($invoice->balance() > 0)
                                        <?php
                                        $sale = $invoice->sale;

                                        $color = isset($sale->color) ? $sale->color : "";
                                        $class = "style=background:{$color}";

                                        if (!$sale->is_approved) {
                                            $class = "style=background:lightblue";
                                        }

                                        $approvedBy = isset($sale->approvedBy) ? $sale->approvedBy->fullName() : "Not Approved";
                                        ?>
                                        <tr>
                                            <td {!! $class !!}>{{number_format(floatval($invoice->total_amount) , 2)}}</td>
                                            <td {!! $class !!}>{{number_format(floatval($invoice->totalPaid()) , 2)}}</td>
                                            <td {!! $class !!}>{{$balance = number_format((floatval($invoice->sale->getBalance())) , 2)}}</td>
                                            <td {!! $class !!}>{{$invoice->due_date}}</td>
                                            <td {!! $class !!}>
                                                @component('layouts.button')
                                                    <li><a href="{{url('sale/invoice/details/'.$invoice->id)}}"
                                                           id="detail">Details</a></li>
                                                    @can('access-module-component-functionality','sale_manage_pay-title')
                                                        <li>
                                                            <a href="{{ url('sale/manage/pay-title/'.$invoice->sale->id)  }}"
                                                               target="_blank">Pay Title</a>
                                                        </li>
                                                    @endcan
                                                @endcomponent
                                            </td>
                                        </tr>
                                    @endif
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <!-- /.tab-pane -->


                </div>
                <!-- /.tab-content -->
            </div>
            <!-- /.nav-tabs-custom -->
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->



    {{--   <div class="p-20">
           <!-- begin row -->
           <div class="row">
               <!-- begin col-2 -->
               <div class="col-md-3">
                   <div class="hidden-sm hidden-xs">
                       <h5 class="m-t-20">Profile</h5>
                       <ul class="nav nav-pills nav-stacked nav-inbox">
                           <li class="">
                               <a href="#">Name: <strong>{{ $customer->fullName() }}</strong> </a>
                           </li>
                           <li class="">
                               <a href="#">Email: <strong>{{ $customer->email }}</strong></a>
                           </li>
                           <li class="">
                               <a href="#">Phone: <strong> {{ $customer->phone_number }}</strong></a>
                           </li>
                           <li class="">
                               <a href="#">Id No: <strong> {{ $customer->id_no }}</strong></a>
                               <a href="#"> Agent: <strong> {{ ucwords( $customer->user->fullName() ) }}</strong></a>
                           </li>

                       </ul>
                   </div>
               </div>
               <!-- end col-2 -->
               <!-- begin col-10 -->
               <div class="col-md-9">
                   <div class="email-btn-row hidden-xs">
                       <a href="#sales" data-toggle="tab" class="btn btn-sm btn-default"> Sales </a>
                       @can('access-module-component-functionality','customers_manage_update')
                           <a href="#edit" data-toggle="tab" class="btn btn-sm btn-default"> Edit Details </a>
                       @endcan
                       <a href="#newSale" onclick="newSale.getProjects()" data-toggle="tab" class="btn btn-sm btn-default"><i
                                   class="fa fa-plus m-r-5"></i> New Sale</a>
                       <a href="#documents" data-toggle="tab" class="btn btn-sm btn-default ">Documents</a>
                       <a href="#referrals" data-toggle="tab" class="btn btn-sm btn-default ">Referrals</a>
                       <a href="#unpaid" data-toggle="tab" class="btn btn-sm btn-default ">UnPaid Sales</a>
                   </div>
                   <div class="email-content">

                       <div class="tab-content">

                           @can('access-module-component-functionality','customers_manage_update')
                               <div class="tab-pane fade  in" id="edit">
                                   @include('customers.profile.edit_customer',  ['customer' => $customer])
                               </div>
                           @endcan

                           <div class="tab-pane fade active in" id="sales">
                               @include('sales._list_table',['sales' => $customer->sales] )
                           </div>
                           <div class="tab-pane fade in" id="newSale">
                               <form
                                       class="form-horizontal form-bordered"
                                       method="post"
                                       id="newSaleForm"
                                       action="{{ route('sale_manage_v2_store') }}"
                               >
                                   @csrf
                                   <input type="hidden" name="customer_id" value="{{ $customer->id }}">
                                   @include('customers.profile._new-sale' , ['customer' => $customer])
                                   <div class="email-footer">
                                       <button
                                               type="button"
                                               onclick="newSale.createSale()"
                                               class="btn btn-success form-control"
                                       > Save
                                       </button>
                                   </div>
                               </form>


                           </div>
                           <div class="tab-pane fade in" id="documents">
                               <a href="{{ route('customers_manage_upload_create', ['customer' => $customer->id]) }}"> New
                                   Uploads </a>
                               <table class="table table-bordered table-striped">
                                   <thead>
                                   <tr>
                                       <th>Customer</th>
                                       <th>Document Type</th>
                                       <th>Status</th>
                                       <th>View</th>
                                   </tr>
                                   </thead>
                                   <tbody>
                                   @foreach($customer->uploads->map->uploadsToArray() as $file)
                                       <tr>
                                           <td>{{$file['customer']}}</td>
                                           <td>{{$file['type']}} {{ $file['meme_type'] }}</td>
                                           <td>
                                               @if (! $file['isSeen'])
                                                   <span class="badge badge-warning">Not Seen</span>
                                               @else


                                                   <span class="badge badge-success"> Seen</span>
                                               @endif

                                           </td>
                                           <td>
                                               @component('layouts.button')
                                                   <li>
                                                       <a
                                                               class="view-document"
                                                               data-url="{{$file['url']}}"
                                                               data-file_name="{{ $file['meme_type'] }}"
                                                               data-toggle="modal"
                                                               data-target="#viewFile"

                                                       >
                                                           View
                                                       </a>
                                                   </li>
                                                   @if( ! $file['isSeen'])
                                                       <li>
                                                           <a
                                                                   href="{{route('customer-uploads-mark-as-seen' ,['file' => $file['id']])}}"
                                                           >
                                                               Mark As Seen
                                                           </a>
                                                       </li>
                                                   @endif
                                               @endcomponent
                                           </td>

                                       </tr>
                                   @endforeach
                                   </tbody>
                               </table>
                           </div>

                           <div class="tab-pane fade in" id="referrals">
                               <h2>referrals</h2>
                           </div>

                           <div class="tab-pane fade in" id="unpaid">
                               <h2>unpaid</h2>
                           </div>
                       </div>
                   </div>
               </div>
               <!-- end col-10 -->
           </div>
           <!-- end row -->
       </div>
       <!-- end #content -->--}}



    @component('layouts.partials.modal')

        @slot('dialogClass','modal-lg')
        @slot('id','viewFile')
        @slot('title') <h6>View Document</h6>@endslot


        <div class="document-details"></div>

    @endcomponent

@endsection

@section('extra_js_footer')
    <script src="{{asset('assets/plugins/jquery-1.9.1.min.js')}}"></script>
    @include('layouts.dataTables.datatable')
    @include('layouts.datePicker.datepicker')
    @include('layouts.form')
    @include('customers.profile.customer_script')

    @include('layouts.partials.alerts')
    <script>
        $('table.sales-table').dataTable();
        $('input.date').datepicker({
            endDate: new Date()
        });

        $(document).on('click', 'a.view-document', function () {
            let source = $(this).data("url");

            let filename = $(this).data('file_name');

            $("div.document-details").empty();

            if (filename == 'pdf' || filename === "txt" || filename === "docx" || filename === "doc") {

                $("div.document-details").append('<iframe src="' + source + '" style="width:718px; height:700px;" frameborder="0"></iframe>');

                return;
            }

            $("div.document-details").append("<img src='" + source + "' class='img-responsive thumbnail'>");

        });


        const newSale = new Sale();
        newSale.getAgents();
        newSale.getProjects();


        (() => {
            $('div#page-container').addClass('page-sidebar-minified')
        })();
    </script>
@endsection

