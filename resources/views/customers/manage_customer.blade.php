@extends('layouts.master')
@section('title','Manage Customers')



@section('extra_css_header')
    <style>
        .btn-file {
            position: relative;
            overflow: hidden;
        }

        .btn-file input[type=file] {
            position: absolute;
            top: 0;
            right: 0;
            min-width: 100%;
            min-height: 100%;
            font-size: 100px;
            text-align: right;
            filter: alpha(opacity=0);
            opacity: 0;
            outline: none;
            background: white;
            cursor: inherit;
            display: block;
        }

        #img-upload {
            width: 100%;
        }
    </style>

@endsection

@section('content')

    @can('access-module-component-functionality', 'customers_manage_add')
        <ul class="nav nav-pills">
            <li class="active">
                <a class="btn btn-info" data-toggle="modal" data-target="#addProduct">Add Customer</a>
            </li>
        </ul>
    @endcan



    @component('layouts.partials.panel')

        @slot('heading',' ')



        @include('customers._table', ['customers' => $customers])

    @endcomponent


    @component('layouts.partials.modal')
        @slot('modalClass','modal-lg')
        @slot('id','upload')
        @slot('title','Upload customer image')



        <form action="{{route('customers_manage_upload')}}" method="post" enctype="multipart/form-data">

            {{csrf_field()}}
            <div class="container">

                <input text="hidden" class="upload-customer-id" name="customer_id">
                <div class="col-md-6">
                    <div class="form-group">
                        <label>Upload Image</label>
                        <div class="input-group">
            <span class="input-group-btn">
                <span class="btn btn-default btn-file">
                    Browse… <input type="file" name="photo" id="imgInp">
                </span>
            </span>
                            <input type="text" class="form-control" readonly>
                        </div>
                        <img id='img-upload' class="thumbnail img-responsive img-circle">
                    </div>
                </div>


            </div>

            <div class="modal-footer">
                <button type="submit" class="btn btn-primary">Upload</button>
            </div>

        </form>

    @endcomponent


    @component('layouts.partials.modal')
        @slot("id","addProduct")
        @slot('dialogClass','modal-lg')
        @slot('title')<h6>
            Fill the form below to add a CUSTOMER
        </h6>@endslot

        <form class="form-horizontal form-bordered" method="post" action="{{route('customers_manage_store')}}">


            @include('customers._add_customer_form')
        </form>

    @endcomponent



    <!-- end customer edit -->

    @component('layouts.partials.modal')
        @slot('id','delete-product')
        @slot('title')
            <h6></h6>
        @endslot

        <p>
        <h5>ARE YOU YOU WANT TO DELETE THIS customer </h5>

        <form method="post" action="{{route('customers_manage_delete')}}">
            {{csrf_field()}}
            <input type="hidden" id="id-cus" name="id">


            <div class="modal-footer">
                <button type="submit" class="btn btn-danger">Delete</button>
            </div>
        </form>

        </p>
    @endcomponent


@endsection

@section('extra_js_footer')
    @include('layouts.dataTables.datatable')
    @include('layouts.form')
    <script>
        $("#data-table").DataTable({
            sort: false
        });

        $(document).on('click', 'a#id', function () {

            var id = $(this).data('id');

            $("#id-cus").val(id);
        });

        $(document).ready(function () {
            $('#data-table').on('click', '#edit', function () {
                $("#customerEdit #first_name").val($(this).data('firstname'));
                $("#customerEdit #last_name").val($(this).data('lastname'));
                $("#customerEdit #middle_name").val($(this).data('middlename'));
                $("#customerEdit #phone_number").val($(this).data('phone'));
                $("#customerEdit #email").val($(this).data('email'));
                $("#customerEdit #id_no").val($(this).data('id-no'));
                $("#customerEdit #id").val($(this).data('id'));
            });


        });


        /*********UPLOAD IMAGE AND VIEW THE IMAGE*****/



        $(document).on('click', 'a.customer-image', function () {

            let id = $(this).data('id');

            $("input.upload-customer-id").val(id)

        });

        $(document).ready(function () {
            $(document).on('change', '.btn-file :file', function () {
                var input = $(this),
                    label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
                input.trigger('fileselect', [label]);
            });

            $('.btn-file :file').on('fileselect', function (event, label) {

                var input = $(this).parents('.input-group').find(':text'),
                    log = label;

                if (input.length) {
                    input.val(log);
                } else {
                    if (log) alert(log);
                }

            });

            function readURL(input) {
                if (input.files && input.files[0]) {
                    var reader = new FileReader();

                    reader.onload = function (e) {
                        $('#img-upload').attr('src', e.target.result);
                    }

                    reader.readAsDataURL(input.files[0]);
                }
            }

            $("#imgInp").change(function () {
                readURL(this);
            });
        });

    </script>
@endsection



