<style>
    .img-circle {
        border-radius: 50%;
        margin: auto !important;
    }

    table.offer:nth-child(odd){
        background: lightblue;
    }

</style>

<div class="table-responsive">



        <h4 style="text-align: center; text-transform: capitalize">{{__('Offer')}}</h4>

        <table class="table table-bordered table-striped offer" id="data-table" width="100%" cellspacing="0"
               cellpadding="0">
            <thead>
            <tr>
                <th>REF NO</th>
                <th>Agent</th>
                <th>Payment Option</th>
                <th>Converted To Sale</th>
                <th>{{__('Project')}}</th>
                <th>View</th>

            </tr>
            </thead>
            <tbody>
            <?php $total = 0  ; $balance = 0 ?>


            @foreach($customer->offers as $offer)

            <?php $total += $offer->getPaidAmount() ; $balance += $offer->getBalance() ?>
            <tr>
                <td>{{$offer->id}}</td>
                <td><a href="{{route('users_manage_details',['user' => $offer->agent->id])}}">{!! $offer->agent->fullName() !!}</a></td>
                <td>{{$offer->payment_option}}</td>
                <td>{{$offer->is_converted ? "Yes" : "not yet"}}</td>
                <td>{{ $offer->getCustomerPlotNos()}}</td>
                <td><a href="{{url('offer/generate/approve/'.$offer->id)}}" class="badge badge-success">View</a></td>
            </tr>
            @endforeach

            </tbody>

        </table>


<!-----END OF SALES---->


    @component('layouts.partials.modal')

        @slot('dialogClass','modal-lg')
        @slot('id','viewDocument')
        @slot('title') <h6>Scanned Document</h6>@endslot


        <div class="document-details"></div>

    @endcomponent



</div>
