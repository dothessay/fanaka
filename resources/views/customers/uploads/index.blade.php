@extends('layouts.master')
@section('title','Customer File Uploads')
@section('content')

    @component('layouts.partials.panel')

        <div class="table-responsive">
            <table class="table table-bordered table-striped">
                <thead>
                <tr>
                    <th>Customer</th>
                    <th>Document Type</th>
                    <th>Status</th>
                    <th>View</th>
                </tr>
                </thead>
                <tbody>
                @foreach($files as $file)
                    <tr>
                        <td>{{$file['customer']}}</td>
                        <td>{{$file['type']}}</td>
                        <td>
                            @if (! $file['isSeen'])
                                <span class="badge badge-warning">Not Seen</span>
                                @else


                                <span class="badge badge-success"> Seen</span>
                            @endif

                        </td>
                        <td>
                            @component('layouts.button')
                                <li>
                                    <a
                                            class="view-document"
                                            data-url="{{$file['url']}}"
                                            data-toggle="modal"
                                            data-target="#viewFile"

                                    >
                                        View
                                    </a>
                                </li>
                               @if( ! $file['isSeen'])
                                    <li>
                                        <a
                                                href="{{route('customer-uploads-mark-as-seen' ,['file' => $file['id']])}}"
                                        >
                                            Mark As Seen
                                        </a>
                                    </li>
                                @endif
                            @endcomponent
                        </td>

                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>

    @endcomponent


    @component('layouts.partials.modal')

        @slot('dialogClass','modal-lg')
        @slot('id','viewFile')
            @slot('title') <h6>View Document</h6>@endslot


        <div class="document-details"></div>

    @endcomponent
@stop



@section('extra_js_footer')
    <script>
        $(document).on('click', 'a.view-document', function () {

            let source = $(this).data("url")


            $("div.document-details").empty();

            $("div.document-details").append("<img src='"+source+"' class='img-responsive thumbnail'>");


        });
    </script>

@endsection





