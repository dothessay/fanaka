@extends('layouts.master')
@section('title','Sale details for '.$sale->customer->fullName())
@section('content')

    @include('layouts.back')
    @component('layouts.partials.panel')

        <form target="_blank">
            <input type="hidden" name="sale" value="{{$sale->sale->id}}">
            <input type="submit"  name="download" class="btn btn-default" value="Download PDF">
        </form>


        <div class="table-responsive">

            @include('customers._sale_details')

        </div>

    @endcomponent
@endsection

@section('extra_js_footer')
    @include('layouts.dataTables.datatable')

@endsection