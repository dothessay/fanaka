@extends('layouts.master')
@section('title','Manage Customers')



@section('extra_css_header') @endsection

@section('content')





    @can('access-module-component-functionality', 'customers_manage_add')
        <ul class="nav nav-pills">
            <li class="active">
                <a href="{{ route('customers_manage_create') }}" >Add Customer</a>
            </li>
        </ul>
    @endcan

    <div class="row">
        {!!

                  \Codex\Classes\Handlers\Widget::build()
                  ->title("All Unpaid")
                  ->value($invoices->count())
                 ->uri(route('customers_manage_unpaid'))
                 ->output();

                   !!}   {!!

                  \Codex\Classes\Handlers\Widget::build()
                  ->title("Paid 100 %")
                  ->value($sales->count())
                 ->uri(route('customers_manage_percentage'))
                 ->output();

                   !!}
    </div>

    <div class="table-responsive">
        <table class="table-bordered table-striped customer-table table" id="data-table" width="100%">
            <thead>
            <tr>
                <th>Ref</th>
                <th>Full Name</th>
                <th>Email</th>
                <th>Phone Number</th>
                <th>ID No</th>
            </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
    </div>

    <!-- customer Add -->
    @component('layouts.partials.modal')
        @slot("id","addCustomer")
        @slot('dialogClass','modal-lg')
        @slot('title')<h6>
            Fill the form below to add a CUSTOMER
        </h6>@endslot

        <form class="form-horizontal form-bordered" method="post" action="{{route('customers_manage_store')}}">


            @include('customers._add_customer_form')
        </form>

    @endcomponent

    <!-- end customer add -->
@endsection
@section('extra_js_footer')
    @include('layouts.dataTables.datatable')
    @include('layouts.dataTables.tablebuttons')
    <script>

        $("table#data-table").DataTable({
            "processing": true,
            "serverSide": true,
            ajax: "{{ route('customers_manage_list') }}",
            columns: [
                { "data" : "id"},
                { "data" : "name"},
                { "data" : "email"},
                { "data" : "phone"},
                { "data" : "id_no"},
            ],
            fnCreatedRow: function( nRow, aData, iDataIndex ) {
                $(nRow).attr('clicked_row',aData['id']);
            }
        });

        $(document).on('click','table#data-table tr',function(e){
            var uri = "{{ url('customers/manage/statement/') }}";
            var id = $(this).attr('clicked_row');

            uri  += '/'+ id;


           window.location.href = uri;
        });




    </script>
@endsection
