@extends('layouts.master')
@section('title','Sale details for '.$customer->fullName())

@section('extra_css_header')

    <style>
        .img-circle {
            border-radius: 50%;
            margin: auto !important;
        }

        table.offer:nth-child(odd){
            background: lightblue;
        }
        table.sales:nth-child(odd){
            background: lightblue;
        }
    </style>

@endsection
@section('content')

    @include('layouts.back')

    <div style="text-align: center; justify-content: center"><img width='150px' height='150px' src='{{ $customer->image }}' class="img-responsive img-circle img-rounded"></div>

    <h4 style="text-align: center; text-transform: uppercase">{{$customer->fullName()}}</h4>
    <h4 style="text-align: center;">{!! $customer->contactPoint() !!}</h4>


    <?php

    $isActiveTab =  isset($_GET['tab']) ? $_GET['tab']: [];



    $isActive = "";


    if (! isset($isActiveTab)){

        $isActive = "active";

    }


    ?>

    <ul class="nav nav-tabs tabs-uzapoint">

        <?php


        if ($isActiveTab == 'sales')
        {

            $isActive = "active";
        }
        else{
            $isActive = "";
        }

        ?>

        <li class="{{$isActive}} ">
            <a href="#default-tab-1" data-toggle="tab">
                <span class="">List Sales</span>
                <span class="badge badge-danger"><sup>{{$customer->sales->count()}}</sup></span>

            </a>
        </li>


        <?php


        if ($isActiveTab == 'offers')
        {

            $isActive = "active";

        }

        else{
            $isActive = "";
        }

        ?>


        <li class="{{$isActive}} ">
            <a href="#default-tab-2" data-toggle="tab">
                <span class="">Reservations</span>
                <span class="badge badge-danger"><sup>{{$customer->offers->count()}}</sup></span>

            </a>
        </li>


        <?php


        if ($isActiveTab == 'referrals')
        {

            $isActive = "active";

        }

        else{
            $isActive = "";
        }

        ?>

        <li class="{{$isActive}} ">
            <a href="#default-tab-3" data-toggle="tab">
                <span class="">Referrals</span>
                <span class="badge badge-danger"><sup>{{$customer->referrals->count()}}</sup></span>

            </a>
        </li>




    </ul>


    <div class="tab-content">
        <?php


        if ($isActiveTab == 'sales')
        {

            $isActive = "active";

        }

        else{
            $isActive = "";
        }

        ?>

        <div class="tab-pane fade {{$isActive}}  in" id="default-tab-1">

            @include('customers._statement')
        </div>
        <?php


        if ($isActiveTab == 'offers')
        {

            $isActive = "active";

        }

        else{
            $isActive = "";
        }

        ?>
        <div class="tab-pane fade {{$isActive}}  in" id="default-tab-2">


            @include('customers._offer_statement')

        </div>
        <?php


        if ($isActiveTab == 'referrals')
        {

            $isActive = "active";

        }

        else{
            $isActive = "";
        }

        ?>
        <div class="tab-pane fade {{$isActive}}  in" id="default-tab-3">


            @include('customers.referrals._data', ['referrals' => $customer->referrals->map(function ($referral) { return $referral->referralToArray() ; })])


        </div>
    </div>



@endsection

@section('extra_js_footer')
    @include('layouts.dataTables.datatable')
    <script>
        $(document).on('click', 'a.view-document', function () {

            let source = $(this).data("url")


            $("div.document-details").empty();

            let filename = $(this).data('file-name');

            let pieces = filename.split(".");

            if (pieces[1] == 'pdf'){


                $("div.document-details").append('<iframe src="'+source+'" style="width:718px; height:700px;" frameborder="0"></iframe>');

                return;
            }



            $("div.document-details").append("<img src='"+source+"' class='img-responsive thumbnail'>");


        });
    </script>

@endsection



