<form class="form-horizontal form-bordered" id="customerEdit" method="post"
      action="{{route('customers_manage_update')}}">
    {{csrf_field()}}
    <div class="form-group">
        <label class="control-label col-md-2">First Name <span class="required-uzapoint">*</span> </label>
        <div class="col-md-4">
            <input type="text" class="form-control" name="first_name" value="{{ old('first_name', $customer->first_name) }}" id="first_name">
        </div>

        <label class="control-label col-md-2">Last Name <span class="required-uzapoint">*</span> </label>
        <div class="col-md-4">
            <input type="text" class="form-control" name="last_name" value="{{ old('last_name', $customer->last_name) }}" placeholder="500000" id="last_name">
        </div>
    </div>


    <div class="form-group">
        <label class="control-label col-md-2">Middle Name <span class="required-uzapoint">*</span> </label>
        <div class="col-md-4">
            <input type="text" class="form-control" name="middle_name"  value="{{ old('middle_name', $customer->middle_name) }}" id="middle_name">
        </div>


        <label class="control-label col-md-2">Id No <span class="required-uzapoint">*</span> </label>
        <div class="col-md-4">
            <input type="text" class="form-control" name="id_no" placeholder="12345678"  value="{{ old('id_no', $customer->id_no) }}"id="id_no">

        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-md-2">Email <span class="required-uzapoint">*</span> </label>
        <div class="col-md-4">
            <input type="email" class="form-control" name="email" id="email" value="{{ old('email', $customer->email) }}">
        </div>

        <label class="control-label col-md-2">Phone number <span class="required-uzapoint">*</span> </label>
        <div class="col-md-4">
            <input type="text" class="form-control phone" id="phone_number" value="{{ old('phone_number', $customer->phone_number) }}">
        </div>


    </div>

    <input type="hidden" id="id" name="id" value="{{ old('id', $customer->id) }}">

    <div class="modal-footer">
        <button type="submit" class="btn btn-success">Edit</button>
    </div>
</form>
