<script>
    class Sale{
        products = "";

        getProjects() {
            let url = "{{ route('projects')  }}";

            window.axios.get(url)
                .then(res => {
                    let options = "<option value='' data-hidden=true>-Select Project </option>";
                    for (let i = 0; i < res.data.data.length; i++) {
                        options += "<option data-plots='" + JSON.stringify(res.data.data[i].available_plots) + "'>" + res.data.data[i].project_name + "</option>"
                    }
                    $("input.saleTotal").val(0);

                    $("select#sale-products").append(options);
                    $("select#sale-products").selectpicker("refresh")

                })
                .catch()

        }
        getAgents() {
            let url = "{{ route('users_manage_list')  }}"

            window.axios.get(url)
                .then(res => {
                    let options = "<option value='' data-hidden=true>-Select Agent </option>";
                    for (let i = 0; i < res.data.data.length; i++) {
                        options += "<option value='" + res.data.data[i].id + "'>" +  res.data.data[i].name + "</option>"
                    }


                    $("select#sale-agents").append(options);
                    $("select#sale-agents").selectpicker("refresh")

                })
                .catch()

        }

        plots(event) {
            $("input.saleTotal").empty();
            $("select#sale-plots").empty();
            let plots = JSON.parse(event.target.options[event.target.selectedIndex].dataset.plots)
            let options = "<option value='' data-hidden=true>-Select Plot </option>";
            for (let i = 0; i < plots.length; i++) {
                options += "<option data-price='" + plots[i].sellingPrice + "' value='" + plots[i].id + "'>" + plots[i].plotNo + "</option>"
            }

            $("select#sale-plots").append(options);
            $("select#sale-plots").selectpicker("refresh")

        }
        getMaxMonths()
        {
            $("input.saleTotal").empty();
            var input = document.getElementById("maxMonths");
            if($("input#maxMonths").val() > 12)
            {
                $("input#maxMonths").val(12);
                return;
            }
            input.setAttribute("max",12); // set a new value;

            let months = $("input#maxMonths").val();
            let price = $('select#sale-plots option:selected').data('price');

            this.calculateTotal(price , months);

            this.getBalance()

            this.getInstalment();

        }
        price(event) {
            $("input.saleTotal").empty();
            let price = JSON.parse(event.target.options[event.target.selectedIndex].dataset.price)
            $('input.saleTotal').val(price);
            let months = $("input#maxMonths").val();
            this.calculateTotal(price , months);

            this.getBalance()
        }
        getBalance(){
            let total = $("input.saleTotal").val();
            let deposit = $("input.deposit").val();
            $("input.balance").val(parseFloat(total ) - parseFloat(deposit))

        }
        calculateTotal(price , months){
            if(parseInt(months) > 1)
            {
                return  $("input.saleTotal").val((parseFloat(months) * 10000 )  + parseFloat(price))
            }
            return   $("input.saleTotal").val(price)

        }

        getInstalment(){

            let price = $("input.saleTotal").val();
            let depositRequired = 0.3 * parseFloat(price)
            let period = $("input#maxMonths").val();
            let balance = price - depositRequired;
            let monthlyAmount = balance - (period - 1);
            $("input.instalment_amount").val(monthlyAmount)
            $("div>span.deposit").html(`30% is ${depositRequired}`)
        }


        createSale(){
            swal("Conform all records are entered correctly", {
                buttons: {
                    Submit: {
                        text: "Submit",
                        value: "submit",

                    },
                    Cancel: {
                        text: "Cancel",
                        value: "cancel",

                    },

                },
            })
                .then((value) => {
                    switch (value) {

                        case "submit":
                            $("form#newSaleForm").submit();
                            $('form#newSaleForm button').attr('disabled', true);
                            break;

                        case "cancel":
                            swal("Cancel");
                            break;

                        default:
                    }
                });
        }
    }

    class Customer {

        showingForm = true;

       constructor() {
           $("i.show-form").html('Hide Form')
           $("button.show-form-button").addClass('btn-warning')
       }
        checkIfIdExist(){

            let id = $("input#id_no").val();
            let url = "{{ url('customers/manage/create/validate')}}";
            document.getElementById("invalid_id").innerHTML = ""
            if( !parseInt(id))
            {
                document.getElementById("invalid_id").innerHTML = "id can not contains alphabets";
                document.getElementById("invalid_id").classList.add( "required");

                return;

            }
            if(id.length >= 7 )
            {
                window.axios.get(`${url}/?id=${id}`)
                    .then( success => {
                        document.getElementById("invalid_id").classList.remove( "required");
                        document.getElementById("invalid_id").innerHTML = success.data.message
                        document.getElementById("invalid_id").classList.add( "available" );

                        return;
                    })
                    .catch(err => {

                        if(err.response.status == 500)
                        {
                            document.getElementById("invalid_id").classList.remove( "available" );
                            document.getElementById("invalid_id").innerHTML = err.response.data.message;
                            document.getElementById("invalid_id").classList.add( "required");

                        }

                        return ;
                    })
            }
            document.getElementById("invalid_id").innerHTML = "";

        }
        checkIfEmailExist(){

            let email= $("input#email").val();
            let url = "{{ url('customers/manage/create/validate')}}";
            document.getElementById("invalid_email").innerHTML = "";
            window.axios.get(`${url}/?email=${email}`)
                .then( success => {
                    document.getElementById("invalid_email").classList.remove( "required");
                    document.getElementById("invalid_email").innerHTML = success.data.message
                    document.getElementById("invalid_email").classList.add( "available" );

                    return;
                })
                .catch(err => {

                    if(err.response.status == 500)
                    {
                        document.getElementById("invalid_email").classList.remove( "available" );
                        document.getElementById("invalid_email").innerHTML = err.response.data.message;
                        document.getElementById("invalid_email").classList.add( "required");

                    }

                    return ;
                })

        }

        showForm(){

           if(this.showingForm)
           {
               $(".show-client-form").addClass('hidden');
               $("button.show-form-button").removeClass('btn-warning');
               $("button.show-form-button").addClass('btn-success');
               $("i.show-form").html('Show Form');
               this.showingForm = false;

           }
           else{
               this.showingForm = true;
               $(".show-client-form").removeClass('hidden');
               $("button.show-form-button").removeClass('btn-success');
               $("button.show-form-button").addClass('btn-warning');
               $("i.show-form").html('Hide Form');
           }

        }

        hideForm(){
        }


        moreCustomers(){

           let htmlForm =  '<div class="show-client-form Customer-List"> <div class="form-group">\n' +
               '            <label class="control-label col-md-2">First Name <span class="required-uzapoint">*</span> </label>\n' +
               '            <div class="col-md-4">\n' +
               '                <input type="text" class="form-control" id="first_name" name="first_name[]">\n' +
               '            </div>\n' +
               '\n' +
               '            <label class="control-label col-md-2">Last Name <span class="required-uzapoint">*</span> </label>\n' +
               '            <div class="col-md-4">\n' +
               '                <input type="text" class="form-control" id="last_name" name="last_name[]" placeholder="Doe">\n' +
               '            </div>\n' +
               '        </div>\n' +
               '\n' +
               '\n' +
               '        <div class="form-group">\n' +
               '            <label class="control-label col-md-2">Middle Name <span class="required-uzapoint">*</span> </label>\n' +
               '            <div class="col-md-4">\n' +
               '                <input type="text" class="form-control" id="last_name" name="middle_name[]" value=".">\n' +
               '            </div>\n' +
               '\n' +
               '\n' +
               '            <label class="control-label col-md-2" for="id_no">Id No <span class="required-uzapoint">*</span> </label>\n' +
               '            <div class="col-md-4">\n' +
               '                <input type="text" id="id_no" class="form-control" name="id_no[]" oninput="customer.checkIfIdExist()"\n' +
               '                       placeholder="12345678">\n' +
               '                <div class="help-block">\n' +
               '                    <span id="invalid_id"></span>\n' +
               '                </div>\n' +
               '\n' +
               '            </div>\n' +
               '        </div>\n' +
               '        <div class="form-group">\n' +
               '            <label class="control-label col-md-2" for="email">Email <span class="required-uzapoint">*</span> </label>\n' +
               '            <div class="col-md-4">\n' +
               '                <input type="email" id="email" class="form-control" name="email[]"\n' +
               '                       oninput="customer.checkIfEmailExist()">\n' +
               '                <div class="help-block">\n' +
               '                    <span id="invalid_email"></span>\n' +
               '                </div>\n' +
               '            </div>\n' +
               '\n' +
               '            <label class="control-label col-md-2">Phone number <span class="required-uzapoint">*</span> </label>\n' +
               '            <div class="col-md-4">\n' +
               '                <input type="text" class="form-control" name="phone_number[]">\n' +
               '\n' +
               '\n' +
               '            </div>\n' +
               '\n' +
               '\n' +
               '        </div>' +
               '    <div class="form-group">\n' +
               '        <label for="" class="col-md-2">\n' +
               '        </label>\n' +
               '        <button class="btn-success show-form-button" type="button" onclick="customer.showForm()"><i\n' +
               '                    class="fa fa-plus-circle show-form">\n' +
               '                <span onload="customer.showingForm">Hide</span>\n' +
               '            </i>\n' +
               '        </button>\n' +
               '        <button class="btn-info" type="button" onclick="customer.moreCustomers()">\n' +
               '            More Customers\n' +
               '        </button>\n' +
               '        <button class="btn-danger" type="button" onclick="customer.removeCustomers(event)">\n' +
               '            Remove Customer\n' +
               '        </button>\n' +
               '\n' +
               '        \n' +
               '\n' +
               '    </div>' +
               '</div>';

            $( ".duplicate-here" ).append(htmlForm);
        }


        removeCustomers(event){
            event.target.closest('div.Customer-List').remove();
        }
    }

</script>

