
    <div class="form-group">
        <label for="" class="control-label col-md-2">  Projects </label>
        <div class="col-md-4">
            <select id="sale-products" class="form-control" onchange="newSale.plots(event)" data-live-search="true" >

            </select>
        </div>
        <label for="" class="control-label col-md-2">  Plots </label>
        <div class="col-md-4">
            <select
                    id="sale-plots"
                    class="form-control"
                    name="plot_id"
                    onchange="newSale.price(event)"
                    data-live-search="true" >

            </select>
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-md-2">Sale Amount</label>
        <div class="col-md-4">
            <input
                    name="total"
                    class="form-control saleTotal"
                    value="0"
            >
        </div>

        <label class="control-label col-md-2">Discount</label>
        <div class="col-md-4">
            <input
                    name="discount"
                    class="form-control discount"
                    value="0"
            >
        </div>
    </div>
    <div class="form-group">

        <label class="control-label col-md-2">Payment Period</label>
        <div class="col-md-4">
            <input
                    type="number"
                    max="newValue"
                    min="1"
                    class="form-control"
                    id="maxMonths"
                    value="1"
                    oninput="newSale.getMaxMonths()"
                    name="months">
        </div>

    </div>
    
    <div class="form-group">
        <label class="control-label col-md-2">Deposit</label>
        <div class="col-md-4">
            <input
                    name="amount"
                    class="form-control deposit"
                    value="0"
                    oninput="newSale.getBalance()"
            >
            <div class="help-block">
                <span class="deposit text-danger"></span>
            </div>
        </div>

        <label class="control-label col-md-2">Deposit Date</label>
        <div class="col-md-4">
            <input
                    name="deposit_date"
                    class="form-control date"
                    >
        </div>
    </div>
    <div class="form-group">

        <label class="control-label col-md-2">Balance</label>
        <div class="col-md-4">
            <input
                    name="balance"
                    class="form-control balance"
                    value="0">
        </div>

        <label class="control-label col-md-2">Instalment Amount</label>
        <div class="col-md-4">
            <input
                    name="monthly_amount"
                    class="form-control instalment_amount"
                    value="0">
        </div>


    </div>
    <div class="form-group">
        <label class="control-label col-md-2">Payment Method</label>
        <div class="col-md-4">
            <select
                    name="payment_method"
                    class="selectpicker form-control"
                    data-live-search="true">
                @foreach(\Codex\Classes\Helper::system()->paymentMethods() as $method)
                    <option>{{ $method }}</option>
                @endforeach
            </select>
        </div>
        <label class="control-label col-md-2">Reference No</label>
        <div class="col-md-4">
            <input
                    name="reference_no"
                    class="form-control"
                    value="0">
        </div>

    </div>
    <div class="form-group">
        <label class="control-label col-md-2">Upload Receipt</label>
        <div class="col-md-4">
            <input
                    type="file"
                    name="receipt"
                    class="el-upload__input form-control"
            >
        </div>
        <label class="control-label col-md-2">Marketer</label>
        <div class="col-md-4">
           <select
                   id="sale-agents"
                   name="agent"
                   class="form-control"
                   data-live-search="true"
           >

           </select>
        </div>

    </div>

