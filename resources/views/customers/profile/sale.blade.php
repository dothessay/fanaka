@extends('layouts.master')
@section('title','Sales By Percentage')
@section('content')

@include('layouts.back')
    <div class="table-responsive">
        @include('sales._list_table')
    </div>
@endsection

@section('extra_js_footer')
    @include('layouts.dataTables.datatable')
    <script>
        $("table.table").DataTable({
            sort: false
        });
    </script>
@endsection
