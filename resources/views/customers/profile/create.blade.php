@extends('layouts.master')
@section('title','New Customer')
@section('content')

    <form class="form-horizontal form-bordered" method="post" action="{{route('customers_manage_store')}}">
        @include('customers._add_customer_form')
    </form>
@endsection
@section('extra_js_footer')
    @include('layouts.form')
    @include('layouts.datePicker.datepicker')
    @include('customers.profile.customer_script')
    <script>

        class NewSale extends  Sale {

            activateNewSale(){

            }
        }
        const newSale = new NewSale();
        newSale.getProjects();
        newSale.getAgents();
        $(document).on('change', 'input.create_sale', function () {
            if($(this).is(':checked'))
            {
                $("div.new-sale-form").removeClass('hidden')
            }
            else{
                $("div.new-sale-form").addClass('hidden')

            }

        });

        // customers object
        const customer = new Customer();


        // add another customer




        ( () =>  {
            $('div#page-container').addClass('page-sidebar-minified')
        })();

        $(".date").datepicker();
    </script>
@endsection
