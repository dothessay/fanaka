@extends('layouts.master')
@section('title','All Unpaid Clients')
@section('content')


    @include('layouts.back')

    @component('layouts.partials.panel')

        <form class="form-horizontal form-bordered">

            <div class="form-group">

                <label class="col-md-2 control-label">Select Customer</label>
                <div class="col-md-4">
                    <select name="customer_id" class="form-control selectpicker" data-live-search="true" data-size="10">

                        @foreach($customers as $customer)
                            <option value="{{$customer->id}}">{{ $customer->fullName() }}</option>
                        @endforeach
                    </select>


                </div>

                <div class="col-md-2">
                    <button class="btn btn-primary">Get Invoices</button>
                </div>



            </div>

        </form>

        <div class="table-responsive">
            <table class="table-bordered table-striped table" id="data-table" width="100%">
                <thead>
                <tr>
                    <th>Sale Reference</th>
                    <th>Agent</th>
                    <th>Customer</th>
                    <th>Project</th>
                    <th>Sale Amount </th>
                    <th> Amount Paid</th>
                    <th> Balance</th>
                    <th>To Pay</th>
                    <th>Due Date</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody>
                @foreach($invoices as $invoice)
                    <?php
                    $sale = $invoice->sale;

                    $color = "{$invoice->color()}";

                    $class = "style=background:{$color}";



                    $approvedBy = isset($sale->approvedBy) ? $sale->approvedBy->fullName() : "Not Approved";
                    ?>
                    <tr>
                        <td {!! $class !!}><a href="{{url('sale/manage/approve-sale/'.$invoice->sale->id)}}" data-title="Click to see sale details"> FSL_{{$invoice->sale->id}}</a> </td>

                        <td {!! $class !!}>{{$invoice->sale->soldBy->fullName()}}</td>
                        <td {{$class}}> {!! $invoice->sale->getCustomerHref() .' Tell: '. $invoice->sale->getCustomerPhoneNumber()!!}</td>
                        <td {{$class}}> {!! $invoice->sale->getPropertyDetails() !!}</td>
                        <td {!! $class !!}>{{number_format(floatval($invoice->total_amount) , 2)}}</td>
                        <td {!! $class !!}>{{number_format(floatval($invoice->totalPaid()) , 2)}}</td>
                        <td style="background-color: lightgreen">{{$balance = number_format((floatval($invoice->sale->getBalance())) , 2)}}</td>
                        <td {!! $class !!}>
                            @if(\Carbon\Carbon::parse($invoice->sale->getSaleCompletionDate())->greaterThan(now()))

                                {!! number_format(  $invoice->sale->getMonthlyAmount() ,2 ) !!}

                            @endif
                            @if(\Carbon\Carbon::parse($invoice->sale->getSaleCompletionDate())->lessThan(now()))
                                    {!! number_format($invoice->sale->getBalance()  , 2) !!}
                            @endif

                        </td>
                        <td {!! $class !!}>{{$invoice->sale->getSaleCompletionDate()}}</td>
                        <td {!! $class !!}>
                            @component('layouts.button')
                                <li><a href="{{url('sale/invoice/details/'.$invoice->id)}}" id="detail">Details</a></li>
                                @can('access-module-component-functionality','sale_manage_pay-title')
                                    <li>
                                        <a href="{{ url('sale/manage/pay-title/'.$invoice->sale->id)  }}"
                                           target="_blank">Pay Title</a>
                                    </li>
                                @endcan
                            @endcomponent
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>


    @endcomponent



@endsection


@section('extra_js_footer')
    @include('layouts.dataTables.datatable')
    <script>
        $('table.table').DataTable()
    </script>

@endsection
