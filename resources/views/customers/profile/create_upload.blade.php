@extends('layouts.master')
@section('title','Upload '. $customer->fullName().' Uploads')
@section('content')
    <form
            action="{{ route('customers_manage_upload_store', ['customer' => $customer->id]) }}"
            class="form-horizontal form-bordered uploadDocuments"
            method="post"
            enctype="multipart/form-data">
        @csrf
        @include('transfer._upload_form',['customer' => $customer])

        <div class="email-footer">
            <button type="button" class="btn btn-success start-upload" onclick="upload.startUpload(event)"> {{ __('Start Upload') }}</button>
        </div>
    </form>

@endsection
@section('extra_js_footer')
    <script>
        class Uploads{

            startUpload(event){
                swal("Are you sure you want start uploads", {
                    buttons: {
                        cancel: "Cancel!",
                        okay: "Start!",
                    },
                })
                    .then((value) => {
                        switch (value) {

                            case "okay":
                                $('form.uploadDocuments').trigger('submit');
                                break;

                            case "cancel":
                                swal("You have canceled uploads");
                                break;

                            default:

                        }
                    });
            }
        }
        const upload = new Uploads()
    </script>
@endsection
