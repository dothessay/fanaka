<div class="">
    {!!
        (new \Codex\Classes\Handlers\Widget())
                    ->title("Customer Uploads")
                    ->value($uploads)
                    ->uri(url('customer-uploads'))
                    ->output();
    !!}

    {!!
            (new \Codex\Classes\Handlers\Widget())
                    ->title("Customer Referrals")
                    ->value($referrals)
                    ->uri(url('customer-referrals'))
                    ->output();
    !!}

    {!!
            (new \Codex\Classes\Handlers\Widget())
                    ->title("Last Month Defaulters")
                    ->value($lastMonthDefaulters->count())
                    ->uri(route('sale_reports_week-installment', [
                    'start_date' => now()->subRealMonth()->startOfMonth()->format('Y-m-d'),
                    'end_date'   =>  now()->subRealMonth()->endOfMonth()->format('Y-m-d'),
                    ]))
                    ->output();
    !!}

    {!!
            (new \Codex\Classes\Handlers\Widget())
                    ->title("Last Week Defaulters")
                    ->value($lastWeekDefaulters->count())
                    ->uri(route('sale_reports_week-installment', [
                    'start_date' => now()->subRealWeek()->startOfWeek()->format('Y-m-d'),
                    'end_date' => now()->subRealWeek()->endOfWeek()->format('Y-m-d'),
                    ]))
                    ->output();
    !!}

    {!!
            (new \Codex\Classes\Handlers\Widget())
                    ->title("This week Defaulters")
                    ->value($thisWeekDefaulters->count())
                    ->uri(route('sale_reports_week-installment', [
                    'start_date' => now()->startOfWeek()->format('Y-m-d'),
                    'end_date' => now()->subRealDay()->endOfDay()->format('Y-m-d'),
                    ]))
                    ->output();
    !!}

    {!!
            (new \Codex\Classes\Handlers\Widget())
                    ->title("Today expected payment")
                    ->value($todaysPayment->count())
                    ->uri(route('sale_reports_week-installment', [
                    'start_date' => now()->startOfDay()->format('Y-m-d'),
                    'end_date' => now()->endOfDay()->format('Y-m-d'),
                    ]))
                    ->output();
    !!}
</div>


<div class="form-group col-md-12">




        <a href="{{route('projects_reports_availability')}}" class="btn btn-menu btn-lg btn-outline-primary"

           style="background: lightskyblue">Available Project</a>


        @can('access-module-component','sale_appointment')

        <a href="{{route('sale_appointment')}}" class="btn btn-menu btn-lg btn-outline-primary"
           style="background: lightskyblue">Appointments</a>

    @endcan
    @can('access-module-component','projects_manage')

        <a href="{{route('projects_manage')}}" class="btn btn-menu btn-lg btn-outline-primary"
           style="background: orangered">Manage Projects</a>

    @endcan
    @can('access-module-component-functionality','plots_manage_mark-can-sell')

        <a href="{{route('plots_manage_mark-can-sell')}}" class="btn btn-menu btn-lg btn-outline-primary"
           style="background: orangered">Hold and Unhold Plot</a>

    @endcan

    @can('access-module-component','projects_reports')

        <a href="{{route('projects_reports')}}" class="btn btn-menu btn-lg btn-outline-primary"
           style="background: lightgreen"> Projects Reports</a>

    @endcan



    @can('access-module-component','plots_reports')

        <a href="{{route('plots_reports')}}" class="btn btn-menu btn-lg btn-outline-primary col-md-6"
           style="background: lightgreen">Available Plot Report</a>

    @endcan
    @can('access-module-component','sale_manage')

        <a href="{{route('sale_manage')}}" class="btn btn-menu btn-lg btn-outline-primary col-md-6"
           style="background: lightseagreen">List Sales</a>

    @endcan
    @can('access-module-component','offer_generate')

        <a href="{{route('offer_generate')}}" class="btn btn-menu btn-lg btn-outline-primary col-md-6"
           style="background: lightblue">Write Offer</a>

    @endcan
    @can('access-module-component','sale_invoice')

        <a href="{{route('sale_invoice')}}" class="btn btn-menu btn-lg btn-outline-primary col-md-6"
           style="background: lightseagreen">List Invoices</a>

    @endcan
    @can('access-module-component-functionality','sale_reports_late-installment')

        <a href="{{route('sale_reports_late-installment')}}" class="btn btn-menu btn-lg btn-outline-primary col-md-6"
           style="background: lightseagreen">Late Installment</a>

    @endcan
    @can('access-module-component-functionality','sale_reports_week-installment')

        <a href="{{route('sale_reports_week-installment')}}" class="btn btn-menu btn-lg btn-outline-primary col-md-6"
           style="background: lightseagreen">This Week Installment</a>

    @endcan
    @can('access-module-component','customers_manage')

        <a href="{{route('customers_manage')}}" class="btn btn-menu btn-lg btn-outline-primary col-md-6"
           style="background: lightgreen">Manage Customers</a>

    @endcan

    @can('access-module-component','expenses_add')

        <a href="{{route('expenses_add')}}" class="btn btn-menu btn-lg btn-outline-primary col-md-6"
           style="background: limegreen">Add Petty Cash</a>

    @endcan
    @can('access-module-component','expenses_manage')

        <a href="{{route('expenses_manage')}}" class="btn btn-menu btn-lg btn-outline-primary col-md-6"
           style="background: green">Add Expenses</a>

    @endcan
    @can('access-module-component','account_list')

        <a href="{{route('account_list')}}" class="btn btn-menu btn-lg btn-outline-primary col-md-6"
           style="background: lightskyblue">Accounts</a>

    @endcan
</div>


@if(config('fanakaupdates.updates.has_updated')   )

<!-- Modal -->
<div class="modal fade" id="fanaka-updates" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Recent updates</h4>
            </div>
            <div class="modal-body">

                    {!! config('fanakaupdates.updates.text') !!}

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>

            </div>
        </div>
    </div>
</div>

@endif
@section('extra_css_header')
    <style>
        .btn-menu {
            height: 95px;
            width: 30%;
            margin: 12px;
            padding: 40px 12px 12px 12px;
            font-size: larger;
            color: #000000;
            font-weight: bold;
        }
        @media only screen and (max-width: 600px){
            .btn-menu {
                height: 95px;
                width: 100%;
                margin: 12px;
                padding: 40px 12px 12px 12px;
                font-size: larger;
                color: #000000;
                font-weight: bold;
            }
        }
    </style>
@endsection
