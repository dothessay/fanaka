@extends('layouts.master')
@section('title',auth()->user()->fullName() .' Profile')

@section('content')
    @php $user = auth()->user() ; @endphp
    @component('layouts.partials.panel')

        <form class="form-horizontal form-bordered" id="editUser" method="post" action="{{route('users_manage_update')}}">
            {{csrf_field()}}
            <div class="form-group">
                <label class="control-label col-md-2">First Name <span class="required-uzapoint">*</span> </label>
                <div class="col-md-4">
                    <input type="text" class="form-control" name="first_name" id="first_name" value="{{$user->first_name}}" placeholder="John">
                </div>

                <label class="control-label col-md-2">Last Name <span class="required-uzapoint">*</span> </label>
                <div class="col-md-4">
                    <input type="text" class="form-control" name="last_name" id="last_name" value="{{$user->last_name}}" placeholder="Doe">
                </div>
            </div>


            <div class="form-group">
                <label class="control-label col-md-2">Phone Number <span class="required-uzapoint">*</span> </label>
                <div class="col-md-4">
                    <input type="text" class="form-control phone"  id="phone_number"value="{{$user->phone_number}}" placeholder="254 712 345 678">
                </div>


                <label class="control-label col-md-2">E-mail <span class="required-uzapoint">*</span> </label>
                <div class="col-md-4">
                    <input type="text" class="form-control" name="email" id="email" value="{{$user->email}}" placeholder="johndoe@fanaka.co.ke">
                </div>
            </div>


            <div class="form-group">


                <label class="control-label col-md-2"> Password <span class="required-uzapoint">*</span> </label>
                <div class="col-md-4">
                    <input type="password" class="form-control" name="password" id="password" placeholder="xxxxxx">
                </div>

            </div>

            <input type="hidden" name="id" id="id" value="{{$user->id}}">
            <input type="hidden" name="role_id" value="{{$user->role->id}}">

            <div class="modal-footer">
                <button type="submit" class="btn btn-success">Edit</button>
            </div>
        </form>

    @endcomponent

@endsection

@section('extra_js_footer')

    @include('layouts.form')
    @endsection