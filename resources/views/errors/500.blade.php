@extends('layouts.master')
@section('title','Action Could Not be Completed')

@section('content')


    <div class="container text-center" id="error">
        <svg height="100" width="100">
            <polygon points="50,25 17,80 82,80" stroke-linejoin="round" style="fill:none;stroke:#ff8a00;stroke-width:8" />
            <text x="42" y="74" fill="#ff8a00" font-family="sans-serif" font-weight="900" font-size="42px">!</text>
        </svg>
        <div class="row">
            <div class="col-md-12">
                <div class="main-icon text-warning"><span class="uxicon uxicon-alert"></span></div>
                <h1>Whoops!! , Action could not be completed </h1>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6 col-md-push-3">
                <p class="lead">If we have notice the error and we are working on it, please contact the 254714686511 for immediate response </p>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6 col-md-push-3">
                <p class="lead"> @include('layouts.back') </p>
            </div>
        </div>
    </div>
@endsection
