@extends('layouts.master')
@section('title','Home')

@section('content')

    @component('layouts.partials.panel')
        @slot('class','info')
        Welcome Back  <strong>{{auth()->user()->fullName()}}</strong>
        <br/>
        <br/>
        <br/>

        <strong style="width: 20px">
            {{\Illuminate\Foundation\Inspiring::quote()}}
        </strong>

        <br/>
        <br/>
        <hr/>


        @include('_home_buttons')

        <br/>


    @endcomponent
@endsection

@section('extra_js_footer')

    <script>
        (function () {
           $("div#fanaka-updates").modal('show')
        })()
    </script>
@endsection
