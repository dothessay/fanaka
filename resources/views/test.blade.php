<?php
class JadaMpesa{

    public $consumerKey;
    public $consumerSecret;
    public $passKey;
    public $environment;


    public function __construct()
    {
        date_default_timezone_set('AFRICA/NAIROBI');
        $this->consumerKey = "HTCNWhCyClXfdGAll7WZc3fGjJ0UxWBM";
        $this->consumerSecret = "hSFv1Wp4fKGSpmGF";
        $this->passKey = 'bfb279f9aa9bdbcf158e97dd71a467cd2e0c893059b10f78e6b72ada1ed2c919';
        $this->environment = 0;  // 0 for sandbox 1 for live

    }

    private  function generateAccessToken()
    {
        $url = 'https://api.safaricom.co.ke/oauth/v1/generate?grant_type=client_credentials';

        if (! $this->environment)
        {
            $url = 'https://sandbox.safaricom.co.ke/oauth/v1/generate?grant_type=client_credentials';
        }

         $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        $credentials = base64_encode($this->consumerKey.':'.$this->consumerSecret);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array('Authorization: Basic '.$credentials)); //setting a custom header
        curl_setopt($curl, CURLOPT_HEADER, false);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);

        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);

        $curl_response = curl_exec($curl);

        return json_decode($curl_response)->access_token;
    }
    public function initiateStk()
    {
        try{
            $PhoneNumber = $_REQUEST['phone_number'];
            $amount = $_REQUEST['amount'];
            $url = 'https://api.safaricom.co.ke/mpesa/stkpush/v1/processrequest';
            $token = $this->generateAccessToken();
            if( ! $this->environment)
            {
                $url = 'https://sandbox.safaricom.co.ke/mpesa/stkpush/v1/processrequest';
            }

            $BusinessShortCode = '174379';

            $curl = curl_init();
            curl_setopt($curl, CURLOPT_URL, $url);
            curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-Type:application/json','Authorization:Bearer '.$token));
            $timestamp='20'.date(    "ymdhis");
            $password=base64_encode($BusinessShortCode.$this->passKey.$timestamp);
            $curl_post_data = array(
                //Fill in the request parameters with valid values
                'BusinessShortCode' => '174379',
                'Password' => $password,
                'Timestamp' => $timestamp,
                'TransactionType' => 'CustomerPayBillOnline',
                'Amount' => $amount,
                'PartyA' => $PhoneNumber,
                'PartyB' => '174379',
                'PhoneNumber' => $PhoneNumber,
                'CallBackURL' =>  "https://mis.fanaka.co.ke/api/callback",
                'AccountReference' => 'Ref',
                'TransactionDesc' => 'Working'
            );
            $data_string = json_encode($curl_post_data);

            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($curl, CURLOPT_POST, true);
            curl_setopt($curl, CURLOPT_POSTFIELDS, $data_string);
            curl_setopt($curl, CURLOPT_HEADER, false);
            $curl_response=curl_exec($curl);

            print_r($curl_response);
            return $curl_response;

        } catch (Exception $exception)
        {

            //Get input stream data and log it in a file
            $payload = $exception->getMessage();
            $file = 'newfile.txt'; //Please make sure that this file exists and is writable
            $fh = fopen($file, 'a');
            fwrite($fh, "\n====".date("d-m-Y H:i:s")."====\n");
            fwrite($fh, "\n====ERROR");
            fwrite($fh, $payload."\n");
            fclose($fh);
        }
    }

    public function receiveCallBack()
    {

        try{
            \Illuminate\Support\Facades\Mail::raw(json_encode(request()->all()) , function ($mail) {
                $mail->to("philipnjuguna66@gmail.com")
                    ->from('info@fanaka.co.ke')
                    ->subject("Davis Test");
            });

            //Get input stream data and log it in a file
            $payload = file_get_contents('php://input');
            $file = 'newfile.txt'; //Please make sure that this file exists and is writable
            $fh = fopen($file, 'a');
            fwrite($fh, "\n====".date("d-m-Y H:i:s")."====\n");
            fwrite($fh, $payload."\n");
            fclose($fh);

        }
        catch (\Exception $exception)
        {
            //Get input stream data and log it in a file
            $payload = $exception->getMessage();
            $file = 'newfile.txt'; //Please make sure that this file exists and is writable
            $fh = fopen($file, 'a');
            fwrite($fh, "\n====".date("d-m-Y H:i:s")."====\n");
            fwrite($fh, "\n====ERROR");
            fwrite($fh, $payload."\n");
            fclose($fh);
        }
    }
}

$mpesa = (new JadaMpesa());

if (isset($init))
    {
        $mpesa->initiateStk();
    }

if ( isset($callback))
{
    $mpesa->receiveCallBack();
}
