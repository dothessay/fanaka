@extends('layouts.master')
@section('title')
    @if(collect($notification)->count())
        {{$notification->title}}
    @endif
@endsection

@section('content')
    <!-- begin vertical-box -->
    <div class="vertical-box">
        <!-- begin vertical-box-column -->
        <div class="vertical-box-column width-250">

            <!-- end wrapper -->
            <!-- begin wrapper -->
            <div class="wrapper">
                <p><b>Notifications</b></p>
                <ul class="nav nav-pills nav-stacked nav-sm">
                    <li><a href=""><i class="fa fa-inbox fa-fw m-r-5"></i> Unread
                            <span class="badge badge-danger pull-right">{{collect($notifications)->where('read',false)->count()}}</span></a>
                    </li>
                    <li><a href=""><i class="fa fa-flag fa-fw m-r-5"></i> Read
                            <span class="badge badge-primary pull-right">{{collect($notifications)->where('read',true)->count()}}</span></a>
                    </li>

                </ul>

            </div>
            <!-- end wrapper -->
        </div>
        <!-- end vertical-box-column -->
        <!-- begin vertical-box-column -->
        <div class="vertical-box-column">
            <!-- begin list-email -->
            <ul class="list-group list-group-lg no-radius list-email">
                @if(($notification->count()))
                    <li class="list-group-item inverse">
                        <div class="email-checkbox">
                            <label>
                                <i class="fa fa-square-o"></i>
                                <input type="checkbox" data-checked="email-checkbox"/>
                            </label>
                        </div>
                        <a href="" class="email-user">
                            <span class="fa fa-envelope-open"></span>
                        </a>

                        <div class="email-info">
                            <span class="email-time">{{$notification->created_at->diffForHumans()}}</span>
                            <h5 class="email-title">
                                <a>{{$notification->title}}</a>

                            </h5>
                            <p class="email-desc">

                                <a href="{{$notification->url}}">{{$notification->message}}</a>
                            </p>
                        </div>

                    </li>
                @endif
                <li class="list-group-item">

                </li>
                <?php
                $id=null;
                if(collect($notification)->count()){
                    $id = $notification->id;
                }
                ?>
                @foreach($notifications->except($id) as $notif)
                    <a href="{{url('notification/details/'.$notif->id)}}">
                        <li class="list-group-item inverse">


                            <div class="email-checkbox">
                                <label>
                                    <i class="fa fa-square-o"></i>
                                    <input type="checkbox" data-checked="email-checkbox"/>
                                </label>
                            </div>

                            <a href="{{url('notification/details/'.$notif->id)}}" class="email-user">
                                @if($notif->read == false)
                                    <span class="fa fa-envelope-square"></span>
                                @else
                                    <span class="fa fa-envelope-open"></span>
                                @endif

                            </a>
                            <div class="email-info">
                                <span class="email-time">{{$notif->created_at->diffForHumans()}}</span>
                                <h5 class="email-title">
                                    <a  href="{{url('notification/details/'.$notif->id)}}">{{$notif->title}}</a>

                                </h5>
                                <p class="email-desc">

                                    {{ str_limit($notif->message , 50,'...')}}

                                </p>
                            </div>

                        </li>
                    </a>
                @endforeach
            </ul>
            <!-- end list-email -->
        </div>
        <!-- end vertical-box-column -->
    </div>
    <!-- end vertical-box -->
@endsection
