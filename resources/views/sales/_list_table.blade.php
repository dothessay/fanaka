<div class="table-responsive">
    <table class="table-bordered table-striped table sales-table" id="data-table" width="100%">
        <thead>
        <tr>
            <th>Date Recorded</th>
            <th>Agent</th>
            <th>Customer</th>
            <th>Total Amount</th>
            <th>Balance</th>

            <th>Project</th>
            @if(! request()->has('show_actions')  ||  request()->has('show_actions') && request('show_actions'))
                <th>Action</th> @endif
        </tr>
        </thead>
        <tbody>
        @foreach($sales as $sale)
            <?php
            $color = isset($sale->color) ? $sale->color : "";
            $class = "style=background:{$color}";

            $printedClass = 'warning';

            $printedLabel = "Not Printed";
            $uploadedLabel = "Not Uploaded";

            $uploadedClass = 'warning';
            $agreementApprovedClass = 'warning';
            $agreementApprovedLabel = 'No Approved';

            if ($sale->is_agreement_downloaded) {
                $printedClass = 'primary';

                $printedLabel = "printed";

            }

            if ($sale->is_agreement_uploaded) {

                $uploadedClass = 'primary';


                $uploadedLabel = 'Uploaded';
            }
            if ($sale->is_agreement_approved) {

                $agreementApprovedClass = 'primary';


                $agreementApprovedLabel = isset($sale_agreement_request) ? "Agreement Request Sent" : $sale->is_agreement_approved ? "Approved" : "Not Approved";
            }


            if (!$sale->is_approved) {
                $class = "style=background:";
            }

            $approvedBy = isset($sale->approvedBy) ? $sale->approvedBy->fullName() : "Not Approved";
            ?>
            <tr>
                <td {{$class}}> {{ $sale->created_at->format($date_format)}}</td>
                <td {{$class}}> {{$sale->soldBy->fullName()}}</td>

                <td {{$class}} width="15%"> {!! $sale->getCustomerHref() !!}</td>
                <td {{$class}}> {{number_format((double)$sale->total_amount , 2)}}</td>
                <td {{$class}}> {{number_format((double)$sale->getBalance() , 2)}} </td>
                <td {{$class}}> {{$sale->getCustomerPlotNos()}} </td>
                @if(! request()->has('show_actions')  ||  request()->has('show_actions') && request('show_actions'))
                    <td {{$class}}>
                        @component('layouts.button')
                            <li><a href="{{ route('sale_manage_instalment_plan.details', ['sale' => $sale->id]) }}">Instalment Plan</a> </li>
                            <li>
                                <a href="{{url('sale/manage/receipt/'.$sale->id)}}" target="_blank">Receipt</a>

                            </li>
                            <li>
                                <a href="{{url('sale/manage/approve-sale/'.$sale->id)}}" target="_blank">Statement</a>

                            </li>



                                <li><a href="{{url('sale/manage/edit-agreement/' .$sale->id )}}">Print
                                        Agreement</a>
                                </li>



                            @can('access-module-component-functionality','sale_manage_edit-sale-details')

                                <li><a href="{{url('sale/manage/edit-installment/'. $sale->id)}}">Edit Sale Details
                                    </a>
                                </li>
                            @endcan

                            @if(request()->has('customer_id'))
                                @can('access-module-component-functionality','sale_manage_print-title-acknowledgement-note')
                                    <li>
                                        <a href="{{url('sale/manage/acknowledgement/'.$sale->id.'/view?customerId='.@$_GET['customer_id'] )}}">Acknowledgement
                                            Note
                                        </a>
                                    </li>
                                @endcan
                            @endif


                            @if($sale->is_agreement_uploaded)
                                <li>
                                    <a href="{{url('sale/manage/get-uploaded-agreement/'.$sale->id)}}"
                                       target="_blank">Scanned Agreement</a>
                                </li>
                            @endif

                            @if(  $sale->is_agreement_downloaded)

                                <li>
                                    <a href="{{ url('sale/manage/get-uploaded-agreement/' . $sale->id) }}">
                                        Upload Agreement
                                    </a>
                                </li>


                            @endif



                        @endcomponent
                    </td>
                @endif
            </tr>
        @endforeach
        </tbody>
    </table>

    {{--<div class="pagination">
        {{$sales->links()}}
    </div>--}}

</div>
