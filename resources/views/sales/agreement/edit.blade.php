@extends('layouts.master')

@section('title','Editing Agreement Letter')


@section('content')

    <?php
     $canEdit = false;
     ?>

    @cannot('access-module-component-functionality','sale_manage_agreement-request')
        <?php
        $canEdit = true;
        ?>
         @endcan


    <form class="form-horizontal" method="POST" action="{{ url('sale/manage/edit-agreement/'. $sale->id )}}">

        @csrf

        <div class="row form-group">
            <div class="col-md-12">
                <input name="id" type="hidden" value="{{$sale->id}}">

                <textarea id="reservation"  name="agreement" cols="130" rows="60" {{ isset($canEdit) ? "" : "readonly"}}>{!! $agreement !!}</textarea>

            </div>
        </div>

        <br>

        <div class="form-group row">

            <label class="col-md-5"></label>
            <div class="col-md-5">

                @can('access-module-component-functionality','sale_manage_approve-agreement')

                    @if(!  $sale->is_agreement_approved)
                        <?php $text = "Approve" ; $class = "success"; $type = "submit"; ?>
                            <input name="approved" type="hidden" value="1">

                            <a href="{{url('sale/manage/reject-agreement/'.$sale->id)}}" class="btn btn-danger">Cancel</a>

                    @else
                        <?php $text = "Print" ; $class = "success"; $type = "submit"; ?>
                    @endif
                    <button  type="{{ $type }}" class="btn btn-{{ $class }}">{{ $text }}</button>
                @endcan

                    @cannot('access-module-component-functionality','sale_manage_approve-agreement')

                    @if(! $sale->agreement_request)
                        <?php $text = "Send Print Agreement Request" ; $class = "warning"; $type = "submit"; ?>
                    @elseif ( ! $sale->is_agreement_approved)
                        <?php $text = "waiting to be Approved" ; $class = "danger"; $type = "button"; ?>
                    @else
                            <?php $text = "Print" ; $class = "success"; $type = "submit"; ?>
                    @endif
                        <button  type="{{ $type }}" class="btn btn-{{ $class }}">{{ $text }}</button>
                @endcan

            </div>
        </div>
    </form>

@endsection


@section('extra_js_footer')





@endsection
