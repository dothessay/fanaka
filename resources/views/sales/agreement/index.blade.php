@extends('layouts.master')


@section('title','Edit sale agreement for customer '. $sale->getCustomerDetails())

@section('extra_css_header')

    <style>
        #customer-sales {
            display: grid;
            grid-template-columns: 100%;
            grid-gap: 1em;
        }

        @media only screen and (max-width: 600px) {

            #customer-sales {
                display: grid;
                grid-template-columns: 100%;
                grid-gap: 1em;
            }


        }
    </style>

@endsection

@section('content')




    <ul class="nav nav-tabs tabs-uzapoint">


        @can('access-module-component-functionality','sale_manage_edit-sale-details')
            <li class="active ">
                <a href="#default-tab-1" data-toggle="tab">
                    <span class=""> Sales Payments</span>
                </a>
            </li>
        @endcan



        @can('access-module-component-functionality','sale_manage_change-customers')

            <li class="">
                <a href="#default-tab-2" data-toggle="tab">
                    <span class="">Customer Details  </span>
                </a>
            </li>

        @endcan


        <li class="">
            <a href="#default-tab-3" data-toggle="tab">
                <span class="">Next of Kin </span>
            </a>
        </li>


        @can('access-module-component-functionality','sale_manage_change-project')

            <li class="">
                <a href="#default-tab-4" onclick="newSale.getProjects()" data-toggle="tab">
                    <span class=""> Change Project </span>
                </a>
            </li>
        @endcan





        @can('access-module-component-functionality','sale_manage_change-director')
            <li class="">
                <a href="#default-tab-5" data-toggle="tab">
                    <span class=""> Add Directors </span>
                </a>
            </li>
        @endcan

        <li class="">
            <a href="#instalmentPlan" data-toggle="tab">
                <span class=""> Instalment Plans </span>
            </a>
        </li>


    </ul>


    <div class="tab-content">


        @can('access-module-component-functionality','sale_manage_edit-sale-details')
            <div class="tab-pane fade active  in" id="default-tab-1">

                @include('sales.agreement.editPartials._sale_details')
            </div>
        @endcan


        @can('access-module-component-functionality','sale_manage_change-customers')

            <div class="tab-pane fade in" id="default-tab-2">

                @include('sales.agreement.editPartials._sale_customer_details')
            </div>
        @endcan


        <div class="tab-pane fade in" id="default-tab-3">

            @include('sales.agreement.editPartials._next_of_kin')

        </div>


        @can('access-module-component-functionality','sale_manage_change-project')
            <div class="tab-pane fade in" id="default-tab-4">

                @include('sales.agreement.editPartials.separate_sale')
            </div>
        @endcan



    </div>

@endsection


@section('extra_js_footer')
    @include('layouts.form')
    @include('customers.profile.customer_script')

    <script>

        const newSale = new Sale;

        $(document).on('click', 'button.updateButton', () => {

            $("button.updateButton").attr('disabled', true);
            $("button.submitButton").trigger('click')
        });


        $(document).on('input', 'input.calculateInstallmentAmount', function () {


            let balance = $(this).data('balance');

            let months = $(this).val();

            let result = parseFloat(balance) / parseFloat(months);

            $("input.amount-per-month").val(Math.ceil(result))


        });


        FormSliderSwitcher.init();

        //new customer
        var new_customer = function () {
            return $('#new_customer').is(":checked");
        };
        //
        if (!new_customer()) {
            $(".display_new_customer").slideUp();
        }
        $("#new_customer").on('change', function () {

            if (!new_customer()) {
                $(".display_new_customer").slideUp();
                $("div.hide-if-new").removeClass('hidden')
            } else {
                $(".display_new_customer").slideDown();
                $("div.hide-if-new").addClass('hidden')
            }
        });


        $(document).on('change', 'select#selectedCustomer', function () {

            $("span.append-here").empty();

            let customerDetails = "Name:   " + $("select#selectedCustomer option:selected").data('fullname');
            customerDetails += "<br/>I.D:   " + $("select#selectedCustomer option:selected").data('id_no');
            customerDetails += "<br/>Phone Number:   " + $("select#selectedCustomer option:selected").data('phone_number');
            customerDetails += "<br/>E-MAIL:  " + $("select#selectedCustomer option:selected").data('email');
            $("span.append-here").append(customerDetails)
        });


        $(document).on('input', 'input.amount', function () {

            let paidAmount = parseFloat($("input#amountPaid").val());

            let sum = 0;

            $('input.amount').each(function (value, key) {

                sum += parseFloat(key.value);


            });

            var text = "";


            if (sum > paidAmount) {

                text = "sum can not be greater than " + paidAmount;

                $("button#submitSeparation").attr('disabled', true)


            }
            if (sum < paidAmount) {

                text = "sum can not be less than " + paidAmount;

                $("button#submitSeparation").attr('disabled', true)


            }

            if (sum == paidAmount) {

                text = "sum = " + paidAmount;


                $("button#submitSeparation").attr('disabled', false)

            }


            $("span#myCalculation").html(sum + " " + text)


            //calcate total cont.
            //compare with amount#
            //less //button#submitSepar class disabled
            //show amount remianing.


        });


        $(document).on('click', 'button#submitSeparation', function (e) {

            if ($(this).hasClass('disabled')) {
                e.preventDefault();
            }


        })


    </script>

@endsection
