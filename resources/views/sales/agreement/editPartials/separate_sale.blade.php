{{--
<form
        class="form-bordered form-horizontal"
        method="post"
        action="{{route('sale_manage_customer_separate')}}"

>

    @csrf

    <input type="hidden" name="sale_id" value="{{$sale->id}}">

    <div class="form-group">

        <label class="col-md-1 control-label">Paid Amount</label>
        <div class="col-md-4">
            <input
                    readonly

                    value="{{$sale->getPaidAmount()}}"
                    class="form-control "
                    id="amountPaid"
                    name="paid_amount"
            >
        </div>

        <label class="col-md-1 control-label">Calculator</label>
        <div class="col-md-4">

            <span style="color: red" id="myCalculation"></span>
        </div>


    </div>

    @foreach($sale->customers as $customer)
        <label class="col-md-1 control-label">Plots Nos</label>
        <div class="col-md-2">
            <select
                    name="plot_id[{{$customer->id}}]"
                    class="form-control selectpicker"
                    data-live-search="true"
            >
                @foreach($sale->saleItems as $saleItem)
                    <option
                            value="{{$saleItem->plot->id}}"
                    >{{$saleItem->plot->plot_no}}</option>
                @endforeach
            </select>
        </div>


        <label class="col-md-1 control-label">Customers</label>
        <div class="col-md-3">
            <select
                    name="customer_id[]"
                    class="form-control selectpicker"
                    data-live-search="true"
            >
                <option
                        value="{{$customer->id}}"
                >{{$customer->fullName()}}</option>

            </select>
        </div>

    <div class="form-group">


        <label class="col-md-1 control-label"> Amount</label>
        <div class="col-md-3">
            <input
                    name="amount[{{$customer->id}}]"
                    value="{{ round(doubleval($sale->getPaidAmount() / $sale->customers->count()) )}}"
                    class="form-control amount"
                    id="amount_{{$customer->id}}"
            >
        </div>
    </div>
    @endforeach


    <div class="modal-footer">


        <button
            type="submit"
            id="submitSeparation"
            class="btn btn-success"
            >
            Submit
        </button>

    </div>


</form>--}}



<form class="form-horizontal" method="post" action="{{ route('sale_manage_change-plot', ['sale' => $sale->id]) }}">
    @csrf
    <div class="form-group">
        <label class="control-label col-md-2" for="sale_products"> Select Project</label>
        <div class="col-md-4">
            <select id="sale-products" class="selectpicker form-control"  onchange="newSale.plots(event)" data-live-search="true">

            </select>
        </div>
        <label class="control-label col-md-2" for="sale-plots"> Select Plot</label>
        <div class="col-md-4">
            <select id="sale-plots" name="plot_id" class="form-control" data-live-search="true" onchange="newSale.price(event)">

            </select>
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-md-2" for="price"> New Price </label>
        <div class="col-md-4">
            <input type="number" id="price" class="saleTotal form-control" name="price">
        </div>
        <label class="control-label col-md-2" for="sale_id">  Plot Before</label>
        <div class="col-md-4">
            <select id="sale_id" class="form-control selectpicker" name="sale_id" data-live-search="true" onchange="newSale.price(event)">
                @foreach($sale->saleItems as $saleItem)
                    <option value="{{ $saleItem->id }}"> {{ $saleItem->plot->plot_no }}</option>
                @endforeach

            </select>
        </div>
    </div>

    <div class="email-footer">
        <button type="submit" class="btn btn-success">Submit</button>
    </div>
</form>
