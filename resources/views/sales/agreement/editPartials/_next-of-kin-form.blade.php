




<div class="form-group">

    <label class="col-md-2 control-label">Full Name</label>
    <div class="col-md-4">
        <input type="text" name="next_of_kin_name" value="{{$kin->fullName()}}" class="form-control">
    </div>


    <label class="col-md-2 control-label">Phone Number</label>
    <div class="col-md-4">
        <input type="text" name="next_of_kin_phone_number" value="{{$kin->phone_number}}" class="form-control">
    </div>
</div>

<div class="form-group">


    <label class="col-md-2 control-label">Relation</label>
    <div class="col-md-4">
        <input type="text" name="next_of_kin_relation" value="{{$kin->relation}}" class="form-control">
    </div>
</div>


<div class="modal-footer">
    <button class="btn btn-success">Submit</button>
</div>
