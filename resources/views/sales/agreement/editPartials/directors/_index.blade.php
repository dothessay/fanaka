{{--Add more customers to sale--}}

<div id="customer-sales">

    <div class="customer-item">
        <table class="table table-bordered">
            <thead>
            <tr>
                <th>Full Name</th>
                <th>Id No</th>
                <th>Email</th>
                <th>Tel</th>
                <th>Action</th>
            </tr>
            </thead>
            <tbody>
            @foreach($sale->directors as $director)
                <tr>
                    <td>{{$director->customer->fullName()}}</td>
                    <td>{{$director->customer->id_no}}</td>
                    <td>{{$director->customer->email}}</td>
                    <td>{{$director->customer->phone_number}}</td>
                    <td>
                        @component('layouts.button')
                            @if($sale->directors->count() > 1)
                                <li><a href="{{route('sale_manage_customer_directors-remove', ['id' => $director->id ,'sale-id' => $sale->id])}}">Remove</a></li>
                            @endif
                        @endcomponent
                    </td>

                </tr>
            @endforeach
            </tbody>

        </table>
    </div>

    <div class="customer-item">

        <form
                class="form-horizontal form-bordered"
                method="post"
                action="{{route('sale_manage_customer_directors')}}"
        >

            <input
                    type="hidden"
                    class=""
                    name="sale_id"
                    value="{{$sale->id}}"
            >

            <div class="form-group hide-if-new">

                <label class="col-md-2 control-label">Existing Customer</label>
                <div class="col-md-4">

                    <select class="form-control selectpicker" data-live-search="true" data-size="10"
                              id="selectedCustomer" name="customer_id">
                        @foreach($customers as $customer)

                            <option
                                    value="{{$customer->id}}"
                                    data-fullname="{{$customer->fullName()}}"
                                    data-email="{{$customer->email}}"
                                    data-phone_number="{{$customer->phone_number}}"
                                    data-id_no="{{$customer->id_no}}"
                                    id="">{{$customer->fullName()}}</option>
                        @endforeach
                    </select>
                </div>

                <div class="customer-details col-md-6">
                    <div  style="
            background:#fff;
            box-shadow:0 20px 50px rgba(0,0,0,.1);
            border-radius:10px;
            font-size: small;
            font-style: italic;
            transition:0.5s;">
                        <div class="box">
                            <span class="append-here"></span>
                        </div>
                    </div>



                </div>


                <div class="email-footer">
                    <div class="form-group row">

                        <button type="submit" class="btn btn-success">Submit</button>
                    </div>

                </div>
            </div>

        </form>



    </div>




</div>
