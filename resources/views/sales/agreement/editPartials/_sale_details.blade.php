
<form class="form-horizontal form-bordered" action="{{route('sale_manage_edit_installment' ,['sale' => $sale->id])}}" method="post">
    @csrf


    <div class="form-group">

        <label class="col-md-2 control-label">Agreed Amount</label>
        <div class="col-md-4">
            <input type="text" name="agreed_amount" class="form-control" value="{{$sale->total_amount}}">
        </div>
    </div>



    <div class="form-group">

        @foreach($sale->saleItems as $saleItem)


            <label class="col-md-6 control-label">{{$saleItem->plot->product->name() .' Plot No://' .$saleItem->plot->plot_no}}
                <span style="font-size: 10px ; font-style: italic; color: lightgreen">Price Per Plot</span> </label>

            <div class="col-md-6">
                <input type="text" name="selling_price[{{$saleItem->plot->id}}]" class="form-control" value="{{$saleItem->price}}">
            </div>


        @endforeach

    </div>



    <div class="form-group">


        <label class="col-md-2 control-label">Installment Per month</label>
        <div class="col-md-4">
            <input type="number" readonly name="installment_amount" class="form-control amount-per-month" value="{{$sale->installment ? $sale->installment->amount: null}}">
        </div>

        <label class="col-md-2 control-label">No Of Month</label>
        <div class="col-md-4">
            <input type="number" name="installment_months" class="form-control calculateInstallmentAmount" data-initialAmount="{{$sale->getInitialAmount()}}" data-balance="{{floatval($sale->total_amount) - floatval($sale->getInitialAmount()) }}"  data-months="{{$sale->installment ? $sale->installment->months: 6}}" value="{{$sale->installment ? $sale->installment->months: null}}">
        </div>

        {{--<label class="col-md-2 control-label">Additional Info</label>
        <div class="col-md-8">
            <textarea name="additional_info"></textarea>
        </div>--}}
    </div>

    <div class="form-group">
        <label for="Agent" class="control-label col-md-2">Agent</label>
        <div class="col-md-4">

            <select name="agent_id" id="" class="selectpicker form-control" data-live-search="true" data-size="5">
                @foreach($agents as $agent)
                    <option value="{{$agent->id}}"
                            @if($sale->soldBy->id === $agent->id) selected @endif
                    >{{$agent->fullName()}}</option>
                @endforeach
            </select>

        </div>

    </div>

    <div class="modal-footer">
        <button type="button" class="btn updateButton btn-success">Update</button>
        <button type="submit" class="btn btn-success submitButton hidden">Update</button>
    </div>

</form>



