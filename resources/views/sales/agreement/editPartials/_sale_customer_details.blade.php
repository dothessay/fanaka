{{--Add more customers to sale--}}

<div id="customer-sales">

    <div class="customer-item">
        <table class="table table-bordered">
            <thead>
            <tr>
                <th>Full Name</th>
                <th>Id No</th>
                <th>Email</th>
                <th>Tel</th>
                <th>Action</th>
            </tr>
            </thead>
            <tbody>
            @foreach($sale->customers as $customer)
                <tr>
                    <td>{{$customer->fullName()}}</td>
                    <td>{{$customer->id_no}}</td>
                    <td>{{$customer->email}}</td>
                    <td>{{$customer->phone_number}}</td>
                    <td>
                        @component('layouts.button')
                            @if($sale->customers->count() > 1)
                                <li><a href="{{route('sale_manage_customer_remove', ['id' => $customer->id ,'sale-id' => $sale->id])}}">Remove</a></li>
                            @endif
                        @endcomponent
                    </td>

                </tr>
            @endforeach
            </tbody>

        </table>
    </div>

    <div class="customer-item">

        <form
                class="form-horizontal form-bordered"
                method="post"
                action="{{route('sale_manage_customer_store')}}"
        >

            <input
            type="hidden"
            class=""
            name="sale_id"
            value="{{$sale->id}}"
            >

            <div class="form-group hide-if-new">

                <label class="col-md-2 control-label">Existing Customer</label>
                <div class="col-md-4">

                    <select class="form-control selectpicker" data-live-search="true" data-size="10" id="selected-customer" name="customer_id" >
                       <option value="">Select customer</option>

                        @foreach($customers as $customer)

                            <option
                                    value="{{$customer->id}}"
                                    data-fullname="{{$customer->fullName()}}"
                                    data-email="{{$customer->email}}"
                                    data-phone_number="{{$customer->phone_number}}"
                                    data-id_no="{{$customer->id_no}}"
                                    id="">{{$customer->fullName()}}</option>
                        @endforeach
                    </select>
                </div>

                <div class="customer-details col-md-6">
                    <div  style="
            background:#fff;
            box-shadow:0 20px 50px rgba(0,0,0,.1);
            border-radius:10px;
            font-size: small;
            font-style: italic;
            transition:0.5s;">
                        <div class="box">
                            <span class="append-here"></span>
                        </div>
                    </div>



                </div>


                <div class="form-group row">

                    <button type="submit" class="btn btn-success">Submit</button>
                </div>
            </div>

            <div class="form-group">
                <label class="control-label col-md-2">New Customer</label>
                <div class="col-md-4">
                    <input type="checkbox" data-render="switchery" id="new_customer" name="new_customer"
                           data-theme="default">
                </div>
            </div>


           <div id="new-customer" class="display_new_customer">
               @include('customers._add_customer_form')
           </div>
        </form>



    </div>




</div>