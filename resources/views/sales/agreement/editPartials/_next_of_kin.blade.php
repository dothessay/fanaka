<form
        method="post"
        class="form-horizontal form-bordered"
        action="{{route('sale_manage_store-witness')}}"
>
    {{csrf_field()}}

    <input type="hidden" name="sale_id" value="{{$sale->id}}">

    <div class="add-next-of-kin">

@include('sales.agreement.editPartials._next-of-kin-form' ,['kin' => new \App\DB\Customer\NextOfKin()])


        <table class="table table-bordered table-responsive" width="100%" id="kins-table">
            <thead>
            <tr>
                <td>Name</td>
                <td>Phone Number</td>
                <td>Relation</td>
                <td>Action</td>
            </tr>
            </thead>
            <tbody>
            @foreach($sale->nextOfKins() as $nexOfKin)
                <tr>
                    <td>{{$nexOfKin->fullName()}}</td>
                    <td>{{$nexOfKin->phone_number}}</td>
                    <td>{{$nexOfKin->relation}}</td>
                    <td>
                        @component('layouts.button')
                            <li>
                                <a
                                        href="{{route('sale_manage_remove-kins' , ['id' => $nexOfKin->id])}}"
                                >Remove</a>
                            </li>
                        @endcomponent
                    </td>
                </tr>

            @endforeach
            </tbody>
        </table>


    </div>

</form>