<div class="form-horizontal">

    @foreach($sale->installmentPlans  as $index => $instalmentPlan)

        <div class="form-group">
            <label  class="control-label col-md-1">{{ $index +1  }}</label>
            <label  class="control-label col-md-2">Payment Date</label>
            <div class="col-md-3">
                <input class="form-control" value="{{ $instalmentPlan->payment_date }}">
            </div>
            <label class="control-label col-md-1"> Amount </label>
            <div class="col-md-3">
                <input type="text" class="form-control" readonly value="{{ $instalmentPlan->amount }}">
            </div>
            <label  class="control-label col-md-1">{{ $instalmentPlan->is_paid ? " yes " : "No" }}</label>
        </div>
    @endforeach

</div>
