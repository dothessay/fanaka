@extends('layouts.master')
@section('title','update invoice')
@section('content')

    <ul class="nav nav-pills">
    @if(! $invoice->is_paid)
        @can('access-module-component-functionality','sale_invoice_update')

                <li class="active">
                    <a type="button" data-toggle="modal"  data-target="#update">Add Payment</a>
                </li>

        @endcan
    @endif
        <li class="active">
            <a href="{{url('sale/manage/receipt/'. $invoice->sale->id)}}">Print receipt</a>
        </li>
        <li class="active">
            <a href="{{url('sale/invoice/print-details/'. $invoice->id)}}" target="_blank">Print Statement</a>
        </li>
    </ul>

    @component('layouts.partials.panel')
        @slot('heading', "The Remaining balance is ". number_format($invoice->balance(), 2))
        <div class="table-responsive">

            @include('sales._invoice_upload')

        </div>


    @endcomponent


    @component('layouts.partials.modal')
        @slot('id', 'update')
        @slot('dialogClass','modal-lg')
        @slot('title')
            <h3>Fill the form below to update payment</h3>
        @endslot

        <form class="form-bordered form-horizontal" method="post" action="{{route('sale_invoice_update')}}" enctype="multipart/form-data">
            {{csrf_field()}}

            <input type="hidden" name="id" value="{{$invoice->id}}">

            <div class="form-group">

                <label class="control-label col-md-2">Payment Type</label>
                <div class="col-md-4">
                    <select name="payment_type" class="form-control selectpicker" id="payment_typed"
                            data-live-search="true"
                            data-size="5">
                        <option value="full">Full Payment</option>
                        <option value="installment" selected >Installment</option>
                    </select>
                </div>


                <label class="control-label col-md-2">Payment Method</label>
                <div class="col-md-4">
                    {{--  <select name="payment_method"  id="method" class="form-control selectpicker" data-live-search="true"
                              data-size="5">--}}
                    @foreach(\Codex\Classes\Helper::system()->paymentMethods() as $paymentMethod)

                        <input type="radio" name="payment_method" class="method" value="{{$paymentMethod}}">{{$paymentMethod}}
                        {{--  <option value="{{$paymentMethod}}">{{$paymentMethod}}</option>--}}

                        @endforeach
                        </select>
                </div>

            </div>

            <div class="form-group">

                <label class="control-label col-md-2">Amount Paid</label>
                <div class="col-md-4">
                    <input type="text" name="amount" class="form-control gtotal">
                </div>

                <label class="control-label col-md-2">Deposit Date</label>
                <div class="col-md-4">
                    <input type="text" name="deposit_date" class="form-control datepicker" id="">
                </div>


            </div>

            <div class="form-group">

                <div style="display: none" class="next_date">


                    <label class="control-label col-md-2">Months</label>
                    <div class="col-md-4">
                        <input type="text" name="months" class="form-control" value="6">
                    </div>


                    <label class="control-label col-md-2">Next Payment Date</label>
                    <div class="col-md-4">
                        <input type="text" name="due_date" value="{{\Carbon\Carbon::parse($invoice->due_date)->addMonth(1)}}" class="datepicker form-control">
                    </div>



                </div>


            </div>





            <div class="mpesa"  style="display: none">
                <div class="form-group">

                    <label class="control-label col-md-2">Reference code  <span class="required-uzapoint">*</span></label>
                    <div class="col-md-4">
                        <input type="text" name="mpesa_reference_code" class="form-control">
                    </div>
                </div>
            </div>



            <div class="bank" style="display: none">

                <div class="form-group">

                    <label class="control-label col-md-2">Bank Name <span class="required-uzapoint">*</span> </label>
                    <div class="col-md-4">
                        <input type="text" name="bank_name"  value="{{config('config.equity_bank.name')}}" class="form-control">
                    </div>

                    <label class="control-label col-md-2">Account No  <span class="required-uzapoint">*</span></label>
                    <div class="col-md-4">
                        <input type="text" name="account_no"  value="{{config('config.equity_bank.number')}}"  class="form-control">
                    </div>
                </div>
                <div class="form-group">


                    <label class="control-label col-md-2">Slip NO  <span class="required-uzapoint">*</span></label>
                    <div class="col-md-4">
                        <input type="text" name="slip_no" class="form-control">
                    </div>

                </div>
            </div>

            <div class="cheque"  style="display: none">

                <div class="form-group">
                    <label class="control-label col-md-2">Bank Name  <span class="required-uzapoint">*</span></label>
                    <div class="col-md-4">
                        <input type="text" name="check_bank_name" class="form-control">
                    </div>

                    <label class="control-label col-md-2">Cheque No  <span class="required-uzapoint">*</span></label>
                    <div class="col-md-4">
                        <input type="text" name="cheque_no" class="form-control">
                    </div>


                    <label class="control-label col-md-2">Drawer  <span class="required-uzapoint">*</span></label>
                    <div class="col-md-4">
                        <input type="text" name="drawer" class="form-control">
                    </div>

                </div>


            </div>


            <div class="receipts" style="display: none">
                <div class="form-group">
                    <label class="control-label col-md-2">Receipt <span class="required-uzapoint">*</span></label>
                    <div class="col-md-4">
                        <input type="file" name="receipts" class="form-control">
                    </div>
                </div>
            </div>



            <div class="modal-footer">
                <button type="button" class="btn btn-success updateInvoice">Update Invoice</button>

                <button type="submit" class="hidden submitInvoice"></button>
            </div>


        </form>
    @endcomponent
@endsection
@section('extra_js_footer')


    @include('layouts.datePicker.datepicker')
    @include('layouts.dataTables.datatable')

    <script>
        $(function () {

            $('.datepicker').datepicker({
                maxDate: new Date()
            });

            $(document).on('change', '#payment_type', function () {


                if ($(this).val('full')) {
                    $(".next_date").slideUp();
                }
                if ($(this).val('installment')) {

                    $(".next_date").slideDown();
                }


            })
            //$("#data-table").DataTable();
        })


        $(document).on('change','.method',function () {



            if ($(this).val() === 'Mpesa')
            {
                $(".mpesa").show();
                $(".bank").hide();
                $(".cheque").hide();
                $(".receipts").hide();
            }
            if ($(this).val() === 'Bank')
            {
                $(".mpesa").hide();
                $(".bank").show();
                $(".cheque").hide();
                $(".receipts").show();
            }
            if ($(this).val() === 'Cheque')
            {
                $(".mpesa").hide();
                $(".bank").hide();
                $(".cheque").show();
                $(".receipts").show();
            }
            if ($(this).val() === 'Cash')
            {
                $(".mpesa").hide();
                $(".bank").hide();
                $(".receipts").hide();
                $(".cheque").hide();
            }
        })

        $(document).on('click', 'button.updateInvoice', function () {

            $("button.submitInvoice").trigger('click')
            $(this).attr('disabled', true).text('submitting .....')

        });
    </script>
@endsection
