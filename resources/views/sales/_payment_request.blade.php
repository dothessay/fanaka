<div class="table-responsive">

    <form>
    <table class="table table-bordered table-striped " id="payment-requests" width="100%">
        <thead>

        <tr>
            <th>Customer</th>
            <th>Date Received</th>
            <th>Date Deposited</th>
            <th>Amount Paid</th>
            <th>Payment Method</th>
            <th>Reference No</th>
            <th>Received By</th>
            <th>Actions</th>
        </tr>

        </thead>

        <tbody>

        @foreach($paymentRequests as $payment)


            <tr>

                <td>{!! $payment->sale->getCustomerHref() !!}</td>
                <td>{{$payment->deposit_date}}</td>
                <td>{{$payment->created_at}}</td>

                <td><input name="amount[{{$payment->id}}]" class="control-label " id="amount_{{$payment->id}}" value="{{number_format($payment->amount , 2)}}"></td>
                <td>{{$payment->payment_method}}</td>
                <td><input name="reference_code[{{$payment->id}}]" id="reference_{{$payment->id}}" class="control-label" value="{{$payment->reference_code}}"></td>

                <td>{{$payment->user->fullName()}}</td>
                <td>
                    @component('layouts.button')

                        @can('access-module-component-functionality','sale_manage_payment-update') <li>
                            <a
                                    data-id="{{$payment->id}}"
                                    class="update-payment-alert">Update</a>
                        </li>
                        @endcan

                            @can('access-module-component-functionality','sale_manage_payment-approve')<li>
                            <a
                                    data-id="{{$payment->id}}"
                                    class="approve-payment-alert">Approve</a>
                        </li>
                        <li>
                            <a
                                    data-id="{{$payment->id}}"
                                    class="reject-payment-alert">Reject</a>
                        </li>
                        @endcan

                    @endcomponent
                </td>
            </tr>

        @endforeach
        </tbody>



    </table>
    </form>
</div>
