
<div class="table-responsive">
    <table class="table-bordered table-striped table" id="agreement-requests" width="100%">
        <thead>
        <tr>
            <th>Sale Date</th>
            <th>Sale Reference</th>
            <th>Entered By</th>
            <th>Agent</th>
            <th>Customer</th>
            <th>Total Amount</th>
            <th>Balance</th>
            <th>Action</th>
        </tr>
        </thead>
        <tbody>
        @foreach($agreementRequests as $sale)

            <?php

            $color = isset($sale->color) ? $sale->color : "";
            $class = "style=background:{$color}";

            $printedClass = 'warning';

            $printedLabel = "Not Printed";
            $uploadedLabel = "Not Uploaded";

            $uploadedClass = 'warning';

            if ($sale->is_agreement_downloaded) {
                $printedClass = 'primary';

                $printedLabel = "printed";

            }

            if ($sale->is_agreement_uploaded) {

                $uploadedClass = 'primary';


                $uploadedLabel = 'Uploaded';
            }


            if (!$sale->is_approved) {
                $class = "style=background:lightblue";
            }

            $approvedBy = isset($sale->approvedBy) ? $sale->approvedBy->fullName() : "Not Approved";
            ?>
            <tr>
                <td {{$class}}> {{$sale->created_at}}</td>
                <td {{$class}}><a
                            href="{{url('sale/manage/approve-sale/'.$sale->id)}}">FSL_{{$sale->id}}</a></td>
                <td {{$class}}> {{$sale->user->fullName()}}</td>
                <td {{$class}}> {{$sale->soldBy->fullName()}}</td>
                <td {{$class}} width="15%"> {!! $sale->getCustomerHref() !!}</td>
                <td {{$class}}> {{number_format((double)$sale->total_amount , 2)}}</td>
                <td {{$class}}> {{number_format((double)$sale->getBalance() , 2)}} </td>
                <td {{$class}}>
                    @component('layouts.button')


                        <li><a href="{{ url('sale/manage/edit-agreement/'.  $sale->id )}}">View
                                Agreement</a>
                        </li>

                    @endcomponent
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>

    {{--<div class="pagination">
        {{$sales->links()}}
    </div>--}}

</div>
