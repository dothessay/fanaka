@foreach($sales as $key => $sale)

    <p class="text-center"><b>{{strtoupper($key)}}</b></p>
    <table class="table table-bordered table-striped" id="data-table">

        <thead>
        <tr>

            <th>Sale Date</th>
            <th>Amount Paid</th>
            <th>Balances</th>
            <th>Plots</th>

        </tr>
        </thead>

        <tbody>

        <?php $total = 0 ; $balance = 0  ?>
        @foreach($sale as $value)
            <tr>
                <td>{{$value['sale_date']}}</td>
                <td>{{  $value['sale_amount']}}</td>
                <td>{{ $value['sale_balance']}}</td>
                <td>{{implode(',' , $value['plots'])}}</td>
            </tr>

            @php $total +=  $value['sale_amount'] ;  $balance += $value['sale_balance'] @endphp

        @endforeach
        </tbody>
        <tfoot>
        <tr>
            <td>Totals</td>
            <td><b>{{number_format($total , 2)}}</b></td>
            <td><b>{{number_format($balance , 2)}}</b></td>
            <td></td>
        </tr>
        </tfoot>



    </table>
@endforeach