@extends('layouts.master')
@section('sub_title')
    @php  $project  = isset($_GET['project_code']) ? \App\DB\Product\Product::where('code', $_GET['project_code'])->first()->name : "";

    @endphp
    @endsection

@section('title', "Unpaid {$project} ")
@section('content')

    <ul class="nav nav-pills">
        <li class="active">
            <a href="{{route('sale_reports')}}">Back</a>
        </li>
    </ul>
    @component('layouts.partials.panel')

        <form class="form-horizontal">
            <div class="form-group">
                <label class="col-md-2"> Project </label>


                <div class="col-md-4">

                    <select class="form-control selectpicker" name="project_code" data-live-search="true">
                        @foreach($projects as $project)
                            <option value="{{$project->code}}">{{$project->name()}}</option>

                        @endforeach

                    </select>


                    {{-- <input type="text" name="start_date" class="form-control date" value="{{request('start_date') ? : old('start_date')}}">--}}



                </div>
                {{--
                                <label class="col-md-2"> End  Date  </label>
                                <div class="col-md-4">
                                    <input type="text" name="end_date" class="form-control date" value="{{request('end_date') ? : old('old_date')}}">
                                </div>--}}

            </div>


            <div class="modal-footer">
                <button type="submit" class="btn btn-success">Pull Report</button>
            </div>

        </form>


        @include('sales.report.unpaid.data')
    @endcomponent

@endsection


@section('extra_js_footer')
    @include('layouts.dataTables.datatable')
    @include('layouts.dataTables.buttons')
    @include('layouts.dataTables.tablebuttons')
    @include('layouts.datePicker.datepicker')

    <script>
        $(function () {




            $(".date").datepicker();
        })
    </script>


@endsection
