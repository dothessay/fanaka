<div class="table-responsive">
    <table class="table-bordered table-striped table" id="data-table" width="100%">
        <thead>
        <tr>
            <th>Sale Date</th>
            <th>Sale Reference</th>
            <th>Entered By</th>
            <th>Agent</th>
            <th>Customer</th>
            <th>Project</th>
            <th>Plot</th>
            <th>Total Amount</th>
            <th>Balance</th>
            <th>paid %</th>
        </tr>
        </thead>

        @php $total = 0 ; $balance = 0 ;@endphp
        @foreach($sales as $key => $sale)


            <?php



            $total += $sale->total_amount;
            $balance += $sale->getBalance();


            $color = isset($sale->color) ? $sale->color : "";
            $class = "style=background:{$color}";

            if(! $sale->is_approved)
            {
                $class = "style=background:lightblue";
            }


            $approvedBy = isset($sale->approvedBy) ? $sale->approvedBy->fullName() : "Not Approved";
            ?>
            <tr>
                <td> {{$sale->created_at}}</td>
                <td> FSL_{{$sale->id}}</td>
                <td> {{$sale->user->fullName()}}</td>
                <td> {{$sale->soldBy->fullName()}}</td>
                <td> {{$sale->getCustomerDetails()}}  :  {!! $sale->customer() !!}</td>
                <td> {{$sale->getProjects()}}</td>
                <td> {{$sale->getCustomerPlotNos()}}</td>
                <td> {{number_format((double)$sale->total_amount , 2)}}</td>
                <td>
                    @can('access-module-component-functionality','sale_reports_show-figures')
                        {{number_format((double)$sale->getBalance() , 2)}}

                        @else


                        @endcan
                        @cannot('access-module-component-functionality','sale_reports_show-figures')
                            {{number_format(100 - $sale->getPercentage() , 2) }} %
                        @endcan
                </td>
                <td> {{number_format( $sale->getPercentage() , 2) }} % </td>

            </tr>
            @endforeach
            </tbody>
            <tfoot>
            <tr>

                <td colspan="7">Totals</td>

                <td> <b>{{number_format((double)$total , 2)}}</b></td>
                <td> <b>{{number_format((double)$balance , 2)}}</b> </td>
                <td>  </td>



            </tr>
            </tfoot>
    </table>

</div>