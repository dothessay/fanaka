@extends('layouts.master')
@section('sub_title')
    @php  $project  = isset($_GET['project_code']) ? \App\DB\Product\Product::where('code', $_GET['project_code'])->first()->name : "";

    @endphp
@endsection

@section('title', "Paid Above 50 % in {$project} ")
@section('content')

    <ul class="nav nav-pills">
        <li class="active">
            <a href="{{route('sale_reports')}}">Back</a>
        </li>
    </ul>
    @component('layouts.partials.panel')

        <form class="form-horizontal">



            <div class="form-group">

                <label class="control-label col-md-2">Start Date</label>

                <div class="col-md-4">

                    <input
                            type="text"
                            name="start_date"
                            class="form-control datepicker"
                            value="{{isset($_GET['start_date']) ? $_GET['start_date'] :  null}}"
                    >
                </div>



                <label class="control-label col-md-1">End Date</label>

                <div class="col-md-4">

                    <input
                            type="text"
                            name="end_date"
                            class="form-control datepicker"
                            value="{{isset($_GET['end_date']) ? $_GET['end_date'] :  null}}"
                    >
                </div>


            </div>

            <div class="form-group">
                <label class="col-md-2"> Project </label>


                <div class="col-md-4">

                    <select
                            class="form-control selectpicker"
                            name="project_code"
                            data-live-search="true"
                    >
                        @foreach($projects as $project)
                            <option
                                    value="{{$project->code}}"
                                    @if(isset($_GET['project_code']) && $_GET['project_code'] == $project->code)
                                    selected
                                    @endif

                            >
                                {{$project->name()}}
                            </option>

                        @endforeach

                    </select>


                    {{-- <input type="text" name="start_date" class="form-control date" value="{{request('start_date') ? : old('start_date')}}">--}}



                </div>


                <label class="agent col-md-1 control-label">Agent</label>
                <div class="col-md-4">
                    <select
                            name="agent"
                            class="selectpicker form-control"
                            data-live-search="true"
                            data-size="6"

                    >
                        <option value="">  All</option>


                        @foreach($agents as $agent)

                            <option
                                    value="{{$agent->id}}"
                                    @if(isset($_GET['agent']) && $_GET['agent'] == $agent->id)
                                    selected
                                    @endif
                            >
                                {{$agent->fullName()}}

                            </option>

                        @endforeach

                    </select>
                </div>

            </div>


            <div class="modal-footer">
                <button type="submit" class="btn btn-success">Pull</button>
                <input type="submit" name="pdf" value="Print" class="btn btn-primary">
            </div>

        </form>


        @include('sales.report.above50.data')
    @endcomponent

@endsection



@section('extra_js_footer')
    @include('layouts.dataTables.datatable')
    @include('layouts.datePicker.datepicker')

    <script>
        $(function () {
            $("table#data-table").DataTable();
            $(".datepicker").datepicker();
        })
    </script>


@endsection