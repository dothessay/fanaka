@extends('layouts.master')
@section('title','Agents Commissions')

@section('extra_css_header')

    <style>
        #commissions {
            display: grid;
            grid-template-columns: auto;
        }

        #commissions .commission-item #commissions-form {
            display: grid;
            grid-template-columns: 80% auto;
        }


    </style>

@endsection
@section('content')


    <div id="commissions">


        <div class="commission-item">
            <form class="form-horizontal">
                <div id="commissions-form">


                    <div class="commissions-form">

                        <div class="form-group">
                            <label class="control-label col-md-1">Agent</label>
                            <div class="col-md-4">
                                <select
                                        name="agent_id"
                                        class="form-control selectpicker"
                                        data-live-search="true"
                                        data-size="5">

                                    <option value="">All</option>

                                    @foreach($agents as $agent)
                                        <option
                                                value="{{$agent->id}}"
                                                @if(isset($_GET['agent_id']) && $_GET['agent_id'] == $agent->id) selected @endif
                                        >{{$agent->fullName()}}</option>
                                    @endforeach

                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-1">Start Date</label>
                            <div class="col-md-4">
                                <input
                                        autocomplete="off"
                                        name="commission_start_date"
                                        class="form-control datepicker"
                                        value="{{isset($_GET['commission_start_date']) ? $_GET['commission_start_date'] : null}}"
                                >
                            </div>

                            <label class="control-label col-md-1">End Date</label>
                            <div class="col-md-4">
                                <input
                                        autocomplete="off"
                                        name="commission_end_date"
                                        class="form-control datepicker"
                                        value="{{isset($_GET['commission_end_date']) ? $_GET['commission_end_date'] : null}}"

                                >
                            </div>
                        </div>


                    </div>


                    <div class="commissions-form">

                        <div class="form-group">
                            <button class="btn btn-success" name="pdf">Pull Pdf</button>
                        </div>
                        <div class="form-group">
                            <button class="btn btn-success">&nbsp;&nbsp;Pull &nbsp;</button>
                        </div>

                    </div>
                </div>
            </form>
        </div>
        <div class="commission-item">
            <div class="table-responsive">
                @include('sales.report.commissions._data')
            </div>
        </div>
    </div>

@endsection

@section('extra_js_footer')

    @include('layouts.dataTables.datatable')
    @include('layouts.datePicker.datepicker')


    <script>
        // $("table#data-table").DataTable()

        $("input.datepicker").datepicker()


        $('.sale-selected').each(function () {
            var total = $('.sale-selected').length;
            $(this).on('click', function () {
                var i = 0;
                $('.sale-selected').each(function () {
                    if ($(this).is(':checked')) {
                        i++;
                    }
                });
                if (i === total) {
                    $('.select_all').prop('checked', true);
                } else {
                    $('.select_all').prop('checked', false);
                }
            });
        });
        $('.select_all').on('click', function (e) {
            if ($(this).is(':checked')) {
                $('.sale-selected').each(function () {
                    $(this).prop('checked', true);
                });
            } else {
                $('.sale-selected').each(function () {
                    $(this).prop('checked', false);
                });
            }
        });



        $(document).on('click','button#submit-commission', function () {


            let link =  "";


            $('.sale-selected').each(function (value , key) {
                if ($(this).is(':checked')) {


                    console.log(key ,'input log')

                    link += '<input type="hidden" name="sale_id[]" value="'+key.value+'">'

                }
            });

            console.log(link ,'output log')

            $("div.append-values-here").html(link);

            $('button#submit-commission').attr('disabled', true);
            $('button#submitting-result').trigger('click');

        })
        $(document).on('click','button#submit-exclude', function () {


            let link =  "";


            $('.sale-selected').each(function (value , key) {
                if ($(this).is(':checked')) {


                    console.log(key ,'input log')

                    link += '<input type="hidden" name="sale_id[]" value="'+key.value+'">'
                    link += '<input type="hidden" name="exclude" value="on">'

                }
            });

            console.log(link ,'output log')

            $("div.append-values-here").html(link);

            $('button#submit-commission').attr('disabled', true);
            $('button#submitting-result').trigger('click');

        })

    </script>

@endsection
