

<p class="text-center text-capitalize text-uzapoint-bold">Commission report From
    {{
    isset($_GET['commission_start_date']) ? $_GET['commission_start_date'] :
    Carbon\Carbon::parse(date('Y-m-25'))->subRealMonth()->startOfDay() }} To

    {{
    isset($_GET['commission_end_date']) ? $_GET['commission_end_date'] :
    Carbon\Carbon::parse(date('Y-m-25'))->endOfDay()
    }}

</p>
<p>Total: {{ sizeof($sales) }}</p>
<table
        class="table table-bordered table-striped"
        id="data-table"
>
    <thead>
    <tr>

        <th>{{fanakaConfig('commission_percentage')}} % at</th>
        <th>Agent</th>
        <th>Customer</th>
        <th>Project</th>
        <th>Plot</th>
        <th>Amount Paid</th>
        <th>Balance</th>
        <th>percentage Paid</th>
    </tr>
    </thead>
    <tbody>
    @foreach($sales as $sale)

        <tr>

            <td>{{$sale['50-percent-at']}}</td>
            <td>{{$sale['agent']}}</td>
            <td>{!! implode(',', $sale['customers']->map(function ($q) { return $q['name'] . ' Tel: '. $q['phone_number']. ' ' .' <br/>';})->toArray()) !!}</td>
            <td>{!! implode(',', $sale['plots']->map(function ($q) { return $q['landLocation'] .' <br/>' ;})->toArray()) !!}</td>
            <td>{!! implode(',', $sale['plots']->map(function ($q) { return $q['plotNo']  .' <br/>';})->toArray()) !!}</td>
            <td>{!! number_format(floatval($sale['amountPaid']) , 2) !!}</td>
            <td>{!! number_format(floatval($sale['balance'] ) , 2) !!}</td>
            <td>{!! number_format(floatval( round($sale['percentage'] , PHP_ROUND_HALF_UP)) , 1) !!}</td>


        </tr>

    @endforeach
    </tbody>
</table>
