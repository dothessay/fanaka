<div class="table-responsive-lg">
    <table class="table-bordered table-striped table" id="data-table" width="100%" cellspacing="2" cellpadding="2">
        <thead>
        <tr>
            <th>Date</th>
            <th>Land Location</th>
            <th>Price</th>
            <th>Size</th>
            <th>Plot No</th>
            <th>Parcel No</th>
            <th>Status</th>
            <th>Added By</th>
        </tr>
        </thead>
        <tbody>
        @foreach($plots as $plot)
            <tr>

                <td>{{$plot->created_at}}</td>
                <?php

                if (!$plot->is_reserved  && !$plot->is_sold) {

                ?>
                <td><a href="{{url('sale/manage/create-sale/'.$plot->id)}}">{{$plot->product->name()}}</a></td><?php
                } else {
                ?>
                <td>{{$plot->product->name()}}</td>
                <?php


                } ?>

                <td>{{number_format($plot->price , 2)}}</td>
                <td>{{$plot->size->label}}</td>

                <td>{{$plot->plot_no}}</td>
                <td>{{$plot->title_no}}</td>
                <td>
                    <?php
                    $class = "primary";
                    $isSold = "Available";

                    if ($plot->is_reserved)
                    {
                        $isSold = "Reserved";
                        $class = "danger";
                    }



                    if ($plot->is_sold) {

                        $class = "warning";
                        $isSold = "Sold ( Payment in Progress)";
                    }


                    if ($plot->is_completely_paid) {

                        $class = "success";
                        $isSold = "Sold Out";
                    }

                    ?>

                    <span class="label label-{{$class}}">{{$isSold}}</span>
                </td>
                <td>{{$plot->user->fullName()}}</td>


            </tr>
        @endforeach

        </tbody>


    </table>

</div>