<div style="text-align: center">From : @php $request = request()->all();
     echo isset($request['start_date']) ? $request['start_date'] : \Carbon\Carbon::now()->subDays(1) .' To: ';
    echo isset($request['end_date']) ? $request['end_date'] : \Carbon\Carbon::now() @endphp</div>

<div class="table-responsive">

    <table class="table-bordered table-striped table" id="data-table" width="100%">
        <thead>
        <tr>
            <th>Date</th>
            <th>Sale Reference</th>
            <th>Entered By</th>
            <th>Agent</th>
            <th>Months Given</th>
            <th>Completion Date</th>
            <th>Customer</th>
            <th>Project</th>
            <th>Plot No</th>
            <th>Total Amount</th>
            <th>Balance</th>
            <th>Percentage</th>
        </tr>
        </thead>
        <tbody>
        @php $total = 0 ; $balance = 0 ;@endphp
        @foreach($sales as $key => $sale)

            <?php

                    $total += $sale->total_amount;
                    $balance += $sale->getBalance();


            $color = isset($sale->color) ? $sale->color : "";
            $class = "style=background:";

            if(! $sale->is_approved)
            {
                $class = "style=background:lightblue";
            }

            $approvedBy = isset($sale->approvedBy) ? $sale->approvedBy->fullName() : "Not Approved";
            ?>
            <tr>
                <td> {{$sale->created_at}}</td>
                <td> FSL_{{$sale->id }}</td>
                <td> {{$sale->user->fullName()}}</td>
                <td> {{$sale->soldBy->fullName()}}</td>
                <td> {{ isset($sale->installment->months) ? $sale->installment->months : 1 }}</td>
                <td> {{ isset($sale->installment->completion_date) ? $sale->installment->completion_date : \Carbon\Carbon::parse($sale->created_at)->addRealDays(1) }}</td>
                <td> {{$sale->getCustomerDetails()}}</td>
                <td> {{$sale->getProjects()}}</td>
                <td> {{$sale->getCustomerPlotNos()}}</td>
                <td> {{number_format((double)$sale->total_amount , 2)}}</td>
                <td> {{number_format((double)$sale->getBalance() , 2)}} </td>
                <td> {{ number_format(round($sale->getPercentage() , PHP_ROUND_HALF_EVEN) , 2) }} % </td>

            </tr>
        @endforeach
        </tbody>
        <tfoot>
        <tr>

            <td colspan="9">Totals</td>

            <td> <b>{{number_format((double)$total , 2)}}</b></td>
            <td> <b>{{number_format((double)$balance , 2)}}</b> </td>
            <td> <b>{{number_format((double)$total - $balance , 2)}}</b> </td>
            <td>  </td>

        </tr>
        </tfoot>
    </table>

</div>
