@extends('layouts.master')
@section('title','Sale By Date')

@section('content')

    <ul class="nav nav-pills">
        <li class="active">
            <a href="{{route('sale_reports')}}">Back</a>
        </li>
    </ul>
    @component('layouts.partials.panel')

        <form class="form-horizontal">
            <div class="form-group">
                <label class="col-md-2"> Start Date  </label>


                <div class="col-md-4">
                    <input type="text" name="start_date" class="form-control date" value="{{request('start_date') ? : old('start_date')}}">
                </div>

                <label class="col-md-2"> End  Date  </label>
                <div class="col-md-4">
                    <input type="text" name="end_date" class="form-control date" value="{{request('end_date') ? : old('old_date')}}">
                </div>

            </div>

            <div class="form-group">
                <label class="col-md-2"> Sale Type</label>


                <div class="col-md-4">
                    <select class="form-control"
                            name="sale_type"
                    >

                    <option value="all">All</option>
                    <option value="cash">Cash Sale</option>
                    <option value="instalment">Instalment </option>
                    </select>
                </div>



            </div>


            <div class="modal-footer">
                <button type="submit" class="btn btn-success">Pull</button>
                <input type="submit" name="pdf" value="Print" class="btn btn-primary">
            </div>

        </form>


        @include('sales.report.date.data')
    @endcomponent

@endsection



@section('extra_js_footer')
    @include('layouts.dataTables.datatable')
    @include('layouts.datePicker.datepicker')

    <script>
        $(function () {

            $(".date").datepicker();
        })
    </script>


@endsection
