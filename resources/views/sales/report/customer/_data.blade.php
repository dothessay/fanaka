<div class="table-responsive">

<table class="table-bordered table-striped table no-padding" cellpadding="0" cellspacing="0" id="data-table" width="100%">
    <thead>
    <tr>
        <th>Sale Date</th>
        <th>Agent</th>
        <th>Customer</th>
        <th>Project</th>
        <th>Plots</th>
        <th>Total Amount</th>
        <th>Balance</th>
        <th>Paid %</th>

    </tr>
    </thead>
    <tbody>

    @php $total = 0 ; $balance = 0 ;@endphp

    @foreach($sales as $key => $sale)
    <?php

     $total += $sale->total_amount;
     $balance += $sale->getBalance();
    ?>

        <tr>
            <td>{{\Carbon\Carbon::parse($sale->created_at)->format('Y-m-d')}}</td>
            <td>{{$sale->soldBy->fullName()}}</td>
            <td>{!! $sale->getCustomerDetails() !!}</td>
            <td> {{$sale->getProjects()}}</td>
            <td> {{$sale->getCustomerPlotNos()}}</td>
            <td> {{number_format((double)$sale->getTotalSaleAmount() , 2)}}</td>
            <td>
                @can('access-module-component-functionality','sale_reports_show-figures')
                    {{number_format((double)$sale->getBalance() , 2)}}

                @else


                @endcan
                @cannot('access-module-component-functionality','sale_reports_show-figures')
                    {{number_format(100 - $sale->getPercentage() , 2) }} %
                @endcan
            </td>
            <td> {{number_format( $sale->getPercentage() , 2) }} % </td>

        </tr>
    @endforeach
    </tbody>

</table>



</div>
