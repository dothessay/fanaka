@extends('layouts.master')
@section('title','By Customer Report')
@section('content')


    @include('layouts.back')

    <form class="form-horizontal form-bordered" autocomplete="off">

        <div class="form-group">

            <label class="control-label col-md-2">Start Date</label>

            <div class="col-md-4">

                <input
                        type="text"
                        name="start_date"
                        autocomplete="off"
                        class="form-control datepicker"
                        value="{{isset($_GET['start_date']) ? $_GET['start_date'] :  null}}"
                >
            </div>



            <label class="control-label col-md-1">End Date</label>

            <div class="col-md-4">

                <input
                        type="text"
                        name="end_date"
                        autocomplete="off"
                        class="form-control datepicker"
                        value="{{isset($_GET['end_date']) ? $_GET['end_date'] :  null}}"
                >
            </div>


        </div>


        <div class="form-group">
            <label class="control-label col-md-2">Customers</label>
            <div class="col-md-4">
                <select
                        class="form-control selectpicker"
                        multiple name="customer_id[]"
                        data-size="10"
                        class="customers"
                        data-live-search="true"
                >
                    <option
                            @if(isset($_GET['customer_id']) && in_array("all", @$_GET['customer_id']))
                            selected
                            @endif
                            value="all" >All</option>
                    @foreach($customers as $customer)
                        <option value="{{$customer->id}}"
                                @if(isset($_GET['customer_id']) && in_array($customer->id, @$_GET['customer_id']))
                                selected
                                @endif
                                class="hide-if-percentage"
                        >{{$customer->fullName()}}
                        </option>
                    @endforeach
                </select>
            </div>


            <label class="control-label col-md-1">
                % paid
            </label>

            <div class="col-md-4">
                    <select
                            class="form-control selectpicker"
                            multiple name="percentage"
                            data-size="10"
                            data-live-search="true"
                    >
                        <option
                                value=""
                                @if(isset($_GET['percentage']) && $_GET['percentage'] == '')
                                selected
                                @endif>
                            All</option>
                        @foreach(['above-30','below-30'] as $percentage)
                            <option value="{{ $percentage }}"
                                    @if(isset($_GET['percentage']) && $_GET['percentage'] == $percentage)
                                    selected
                                    @endif
                                    class="hide-if-percentage"
                            >{{ $percentage  }}
                            </option>
                        @endforeach
                    </select>
                </div>
        </div>


        <div class="form-group">
            <label class="control-label col-md-2">Agent</label>
            <div class="col-md-4">
                <select
                        name="agent"
                        class="form-control selectpicker"
                        data-live-search="true"
                        data-size="5"
                        
                >
                    <option value="" >All</option>

                    @foreach($agents as $agent)

                        <option
                                value="{{$agent->id}}"
                                @if(isset($_GET['agent']) && $_GET['agent'] == $agent->id)
                                    selected
                                @endif

                        >
                            {!! $agent->fullName() !!}
                        </option>
                    @endforeach
                </select>
            </div>

            <label class="control-label col-md-1">Having Sale?</label>

            <div class="col-md-4">

                <input
                        type="number"
                        name="count"
                        autocomplete="off"
                        min="1"
                        class="form-control "
                        value="{{isset($_GET['count']) ? $_GET['count'] :  1}}"
                >
            </div>

        </div>

        <div class="form-group">
            <label class="control-label col-md-2"> Project</label>
            <div class="col-md-4">
                <select
                        name="project_id"
                        class="form-control selectpicker"
                        data-live-search="true"
                        data-size="5"

                >
                    <option value="" selected>All</option>

                    @foreach($projects as $project)

                        <option
                                value="{{$project->id}}"
                                @if(isset($_GET['project_id']) && $_GET['project_id'] == $project->id)
                                selected
                                @endif

                        >
                            {!! $project->name() !!}
                        </option>
                    @endforeach
                </select>
            </div>

            <label class="col-md-1 control-group"></label>
            <div class="col-md-3">
                <button type="submit" class="btn btn-success">Pull</button>
                <input type="submit" name="pdf" value="Pdf" class="btn btn-warning">
            </div>
        </div>


    </form>

    @include('sales.report.customer._data')

@endsection

@section('extra_js_footer')
    @include('layouts.form')
    @include('layouts.datePicker.datepicker')
    @include('layouts.dataTables.datatable')

    <script>
        $("table#data-table").DataTable();
        $(".datepicker").datepicker();

    </script>



@endsection
