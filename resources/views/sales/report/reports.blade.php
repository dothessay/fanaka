 @extends('layouts.master')
@section('title','Sale Reports')
@section('content')


    {!! \Codex\Classes\Helper::reportWidget('Late Payments','sale_reports_late-installment','Late payment Installments') !!}
    {!! \Codex\Classes\Helper::reportWidget('Week Payments','sale_reports_week-installment','This week Installments') !!}
    {!! \Codex\Classes\Helper::reportWidget('Above 50%','sale_reports_above-50','Paid above 50%') !!}
    {!! \Codex\Classes\Helper::reportWidget('Un Paid ','sale_reports_unpaid','Unpaid Report') !!}
    {!! \Codex\Classes\Helper::reportWidget('Below 50%','sale_reports_below-50','Paid below 50%') !!}
    {!! \Codex\Classes\Helper::reportWidget('Completed','sale_reports_completed','Report of fully paid Sales') !!}
    {!! \Codex\Classes\Helper::reportWidget('By Date','sale_reports_by-date','Shows all sales by date <br/>') !!}
  {{--  {!! \Codex\Classes\Helper::reportWidget('Availability','sale_reports_availability','Availability') !!}--}}
    {!! \Codex\Classes\Helper::reportWidget('By project','sale_reports_by-project','Shows sale type (cash/instalment) all by project') !!}
    {!! \Codex\Classes\Helper::reportWidget('Scanned Agreements','sale_reports_scanned-agreement','Shows all scanned agreement') !!}
    {{--{!! \Codex\Classes\Helper::reportWidget('Reservation ','sale_reports_customer','Reservations Reports by dates') !!}--}}
    {!! \Codex\Classes\Helper::reportWidget('By Customer ','sale_reports_customer','Customer Reports') !!}
    {!! \Codex\Classes\Helper::reportWidget('By Agent','sale_reports_agent','Agent Reports') !!}
    {!! \Codex\Classes\Helper::reportWidget('Commissions','sale_reports_agent.commissions','All Commissions Paid Report', true) !!}
    @endsection
