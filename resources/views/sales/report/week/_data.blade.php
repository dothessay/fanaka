
<div class="table-responsive">
    <table class="table-bordered table-striped table" width="100%">
        <thead>
        <tr>
            <th>Agent</th>
            <th>Customer</th>
            <th>Total Amount</th>
            <th>Total Amount Paid</th>
            <th>Total Balance</th>
            <th>Installment to be Paid</th>
            <th>Penalty</th>
            <th>Due Date</th>
            <th>Last Payment Date</th>
            <th>Last Installment Paid</th>

        </tr>
        </thead>
        <tbody>
        @foreach($plans as $plan)

            @if(isset($plan->sale->id) && $plan->sale->getBalance() > 0)
                <?php
                $sale = $plan->sale;

                $color = isset($sale->color) ? $sale->color : "";
                $class = "style=background:{$color}";

                if (!$sale->is_approved) {
                    $class = "style=background:lightblue";
                }

                $approvedBy = isset($sale->approvedBy) ? $sale->approvedBy->fullName() : "Not Approved";
                ?>
                <tr>
                    @if( $plan->sale->lastPaymentDate() instanceof \Carbon\Carbon  && ! $plan->sale->lastPaymentDate()->isCurrentMonth())

                        <td {!! $class !!}>{{$plan->sale->soldBy->fullName()}}</td>

                        <td {{$class}} width="15%"> {!! $plan->sale->getCustomerHref() !!}</td>
                        <td {!! $class !!}>{{number_format((double)$plan->sale->total_amount , 2)}}</td>
                        <td {!! $class !!}>{{number_format((double)$plan->sale->salepayments->sum('amount') , 2)}}</td>
                        <td {!! $class !!}>{{$balance = number_format(((double)$plan->sale->getBalance()) , 2)}}</td>
                        <td {!! $class !!}>{{$balance = number_format(((double)$plan->amount) , 2)}}</td>
                        <td {!! $class !!}>{{$balance = number_format(((double) $plan->penalty) , 2)}}</td>
                        <td {!! $class !!}>{{$plan->payment_date}}</td>
                        <td {!! $class !!}>{{$plan->sale->lastPaymentDate()}}</td>
                        <td {!! $class !!}>{{$plan->sale->lastPaymentAmount()}}</td>
                    @endif
                </tr>
            @endif

        @endforeach
        </tbody>
    </table>
</div>
