@extends('layouts.master')
@section('title','Late installments')
@section('content')


    @include('layouts.back')


    <section>
        <div class="row">
            <div class="col-md-12">

                <p>Expected Payment This Week</p>
                <div class="box box-white">
                    <div class="box box-body">
                        <form class="form-horizontal">
                            <div class="form-group">
                                <label class="mr-2 col-md-2 control-label">Start date</label>
                                <div class="col-md-3">
                                    <input
                                            type="text"
                                            class="form-control col-md-3 date"
                                            name="start_date"
                                            value="{{ old('start_date', request()->has('start_date') ? request('start_date') : '') }}"
                                    >
                                </div>
                                <label class="mr-2 col-md-1 control-label">End date</label>

                                <div class="col-md-3">
                                    <input
                                            type="text"
                                            class="form-control date col-md-3"
                                            name="end_date"
                                            value="{{ old('end_date',request()->has('end_date') ? request('end_date') : '' ) }}"

                                    >
                                </div>
                                <button class="btn btn-success">Get</button>
                                <button class="btn btn-warning" name="pdf">Print</button>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="box box-white">
                    <div class="box box-body">

                        @include('sales.report.week._data')

                    </div>
                </div>
            </div>
        </div>
    </section>


@endsection

@section('extra_js_footer')

    @include('layouts.dataTables.datatable')
    @include('layouts.datePicker.datepicker')

    <script>

        $("input.date").datepicker({
            endDate: new Date(),
        });
    </script>

@endsection
