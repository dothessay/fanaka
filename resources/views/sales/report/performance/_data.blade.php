<div class="table-responsive">
    <table class="table-bordered table-striped table-condensed table" width="50%">
        <thead>
        <tr>
            <th>Agent</th>
            <th>Amount</th>
            <th></th>
        </tr>
        </thead>
        <tbody> <?php $total = 0?>

        @foreach($agents as $index => $agent) <?php $total +=  floatval($agent['amount'])?>
            <tr>
                <td>{{ $agent['name'] }}</td>
                <td>{{ config('config.currency') .' '.  number_format(floatval($agent['amount']) , 2) }}</td>
                <td><a href="{{ route('sale_reports_agent.performance', ['user' => $agent['id'] , 'start_date' => request('start_date') , 'end_date' => request('end_date')]) }}">View</a></td>
            </tr>
        @endforeach
        </tbody>
        <tfoot>
        <tr>
            <td><strong>Total</strong></td>
            <td style="border-bottom: double; border-top: double; font-weight: bold">{{ config('config.currency') . ' ' . number_format( $total , 2) }}</td>
            <td></td>
        </tr>
        </tfoot>
    </table>
</div>