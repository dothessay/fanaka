@extends('layouts.master')
@section('title','Performance Report')
@section('content')

    @component('layouts.partials.panel')

        <form class="form-horizontal">
            <div class="form-group">
                <label class="control-label col-md-2">Start Date</label>
                <div class="col-md-4">
                    <input
                            type="text"
                            name="start_date"
                            class="form-control date"
                            value="{{ request('start_date','') }}"
                    >
                </div>
                <label class="control-label col-md-1">End Date</label>
                <div class="col-md-4">
                    <input
                            type="text"
                            name="end_date"
                            class="form-control date"
                            value="{{ request('end_date','') }}"
                    >


                </div>
                <div class="col-md-1">
                    <button
                            type="submit"
                            class="btn btn-primary"
                    >Pull</button>
                </div>
            </div>

            <div class="form-group">

                <div class="col-md-1 pr-2">
                    <button
                            name="pdf"
                            type="submit"
                            class="btn btn-warning"
                    >Print PDF</button>
                </div>


                <div class="col-md-2">
                    <button
                            type="submit"
                            name="excel"
                            class="btn btn-danger"
                    >Excel</button>
                </div>

            </div>

        </form>
    <div class="col-md-12 offset-4">
        {!! $chart->container() !!}
    </div>
    @endcomponent
    @include('sales.report.performance.show._data')

@endsection

@section('extra_js_footer')
    @include('layouts.datePicker.datepicker')
    <script src=https://cdnjs.cloudflare.com/ajax/libs/echarts/4.0.2/echarts-en.min.js charset=utf-8></script>

    {!! $chart->script() !!}

    <script>
        $('input.date').datepicker({
            format: 'yyyy/mm/dd',
        })
    </script>
@endsection