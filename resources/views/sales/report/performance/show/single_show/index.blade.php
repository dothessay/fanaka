@extends('layouts.master')
@section('title','Performance Report')
@section('content')

    <ul class="nav nav-pills">
        <li class="active">
            <a href="{{ url('sale/reports/performance/'. $user->id .'?start_date='. request('start_date','').'&start_date='. request('start_date','')) }}">Back</a>
        </li>
    </ul>

    @component('layouts.partials.panel')
    <div class="col-md-12 offset-4"><h3 class="text-center">{{ $user->fullName() }} Client Payment for the Month of {{ $month }}</h3>
    </div>
    @endcomponent
    @include('sales.report.performance.show.single_show._data')

@endsection

@section('extra_js_footer')
    <script src=https://cdnjs.cloudflare.com/ajax/libs/echarts/4.0.2/echarts-en.min.js charset=utf-8></script>


@endsection