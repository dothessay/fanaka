<div class="table-responsive">
    <table class="table-bordered table-striped table-condensed table" width="50%">
        <thead>
        <tr>
            <th>Date Deposited</th>
            <th>Amount</th>
            <th>Customer</th>
            <th>Project</th>
        </tr>
        </thead>
        <tbody> <?php $total = 0?>


        @foreach($sales as $sale) <?php $total +=  floatval($sale->amount)?>
            <tr>
                <td >{{ \Carbon\Carbon::parse($sale->deposit_date )->format('Y-m-d')}}</td>
                <td width="20%">{{ config('config.currency') .' '.  number_format(floatval($sale->amount) , 2) }}</td>
                <td width="30%">{!! $sale->getCustomerHref() !!}</td>
                <td>{!! $sale->getPropertyDetails() !!}</td>

            </tr>
        @endforeach
        </tbody>
        <tfoot>
        <tr>
            <td><strong>Total</strong></td>
            <td style="border-bottom: double; border-top: double; font-weight: bold">{{ config('config.currency') . ' ' . number_format( $total , 2) }}</td>

        </tr>
        </tfoot>
    </table>
</div>