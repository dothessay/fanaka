@extends('layouts.master')

@section('title','Reservation Reports')

@section('content')
    <div class="app">


        @component('layouts.partials.panel')
            <form class="form-horizontal form-bordered">
                <div class="form-group">
                    <label class="col-md-2 control-label">Start Date</label>
                    <div class="col-md-3">
                        <input type="text" class="form-control" name="start_date" value="{{old('start_date')}}">
                    </div>

                    <label class="col-md-1 control-label">End Date</label>
                    <div class="col-md-3">
                        <input type="text" class="form-control" name="end_date" value="{{old('end_date')}}">
                    </div>

                    <label class="col-md-1 control-label"></label>
                    <div class="col-md-2">
                       <button type="submit"class="btn-success btn">Pull</button>
                    </div>
                </div>
            </form>

            <div class="table-responsive table-condensed">

                @include('sales.report.reservation._data')
            </div>
        @endcomponent
    </div>
@endsection



@section('extra_js_footer')

    @include('layouts.datePicker.datepicker')
    @include('layouts.dataTables.tablebuttons')
    @include('layouts.dataTables.buttons')


    <script>
        $( "input[name='start_date'] , input[name='end_date']" ).datepicker();
    </script>



@endsection
