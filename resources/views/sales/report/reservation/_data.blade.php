<table class="table table-bordered table-striped nowrap" id="data-table">
    <thead>
    <tr>
        <th>Date Reserved</th>
        <th>Offer Reference</th>
        <th>Payment Option</th>
        <th>Customer</th>
        <th>Plots</th>
        <th>Cash Option</th>
        <th>Installment Option</th>

    </tr>
    </thead>

    <tbody>
    @foreach($reservations as $offer)
        <tr>
            <td>{{$offer->created_at->format('Y-m-d H:i')}}</td>
            <td><a href="{{route('offer_generate_approve' ,['offer_id' => $offer->id])}}">FRV/{{$offer->id}}</a> </td>
            <td>{{$offer->payment_option}}</td>
            <td>{!! $offer->getCustomerHref() !!}</td>
            <td>{{$offer->getCustomerPlots()}}</td>
            <td>{{$offer->cash_option}}</td>
            <td>{{$offer->installment_option}}</td>


        </tr>
    @endforeach
    </tbody>

</table>
