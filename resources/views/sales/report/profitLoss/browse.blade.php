@extends('layouts.master')
@section('title','Profit and Loss account')
@section('content')

    @component('layouts.partials.panel')
        <form class="form-horizontal">
            <div class="form-group">
                <label class="mr-2 col-md-2 control-label">Start date</label>
                <div class="col-md-3">
                    <input
                            type="text"
                            class="form-control col-md-3 date"
                            name="start_date"
                            value="{{ old('start_date' , request()->has('start_date') ? request('start_date') : '') }}"
                    >
                </div>
                <label class="mr-2 col-md-1 control-label">End date</label>

                <div class="col-md-3">
                    <input
                            type="text"
                            class="form-control date col-md-3"
                            name="end_date"
                            value="{{ old('end_date' , request()->has('end_date') ? request('end_date') : '') }}"
                    >
                </div>
                <input type="hidden" value="{{ auth()->id() }}" name="agent">
                <button class="btn btn-success">Get</button>
            </div>
        </form>
        <hr>
        @include('sales.report.profitLoss._data')
    @endcomponent

@endsection
@section('extra_js_footer')
    @include('layouts.datePicker.datepicker')
    @include('layouts.form')
    @include('layouts.dataTables.datatable')
    <script>
        $("input.date").datepicker();
    </script>
@endsection
