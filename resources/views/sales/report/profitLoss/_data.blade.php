<div class="col-md-4"><?php $filter = '?start_date='. request('start_date') .'&end_date='.request('end_date') ;?></div>
<div class="col-md-4 align-content-center offset-1">
<table class="table table-info table-borderless">
    <thead>
    <tr>
        <th>Income</th>
        <th> <a href="{{ url('income'.$filter) }}"> {{ number_format(floatval( round($income)) , 2) }}</a> </th>
    </tr>
    <tr>
        <th>Expenses</th>
        <th> <a href="{{ url('expenses'. $filter) }}">{{ number_format(floatval(round($expenses )) , 2)}}</a></th>
    </tr>
    </thead>
    <tbody>
    <tr>
        <td>Total</td>
        <td style="border-top:double #000000; border-bottom: double #000000">
            {{ number_format( floatval($income) - floatval($expenses)  , 2) }}
        </td>
    </tr>
    </tbody>
</table>
</div>
