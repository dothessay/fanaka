@extends('layouts.master')
@section('title', ucwords($report . ' sales') )

@section('content')

    @include('layouts.back')
    @component('layouts.partials.panel')

        <div class="table-responsive">
            <table class="table-bordered table-responsive table-striped table nowrap"  id="data-table" cellpadding="0" cellspacing="0" width="100%">
                <thead>
                <tr>
                    <th>Sale Date</th>
                    <th>Sale Reference</th>
                    <th>Entered By</th>
                    <th>Agent</th>
                    <th>Approved By</th>
                    <th>Customer</th>
                    <th>Total Amount</th>
                    <th>Balance</th>
                    <th>Amount Paid</th>
                    <th>Printed</th>
                    <th>Uploaded</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody>
                <?php

                $totalPaid   = 0;
                $totalAmount = 0;
                $totalBalance = 0;
                ?>
                @foreach($sales as $sale)

                    <?php

                    $color = isset($sale->color) ? $sale->color : "";
                    $class = "style=background:{$color}";

                    $printedClass = 'warning';

                    $printedLabel = "Not Printed";
                    $uploadedLabel = "Not Uploaded";

                    $uploadedClass = 'warning';

                    if ( $sale->is_agreement_downloaded)
                    {
                        $printedClass = 'primary';

                        $printedLabel= "printed";

                    }

                    if ( $sale->is_agreement_uploaded)
                    {

                        $uploadedClass = 'primary';


                        $uploadedLabel = 'Uploaded';
                    }


                    if (!$sale->is_approved) {
                        $class = "style=background:lightblue";
                    }

                    $approvedBy = isset($sale->approvedBy) ? $sale->approvedBy->fullName() : "Not Approved";


                    $totalPaid += $sale->getPaidAmount();
                    $totalAmount += $sale->total_amount;
                    $totalBalance += $sale->getBalance();
                    ?>
                    <tr>
                        <td {{$class}}> {{$sale->created_at}}</td>
                        <td {{$class}}><a href="{{url('sale/manage/approve-sale/'.$sale->id)}}">FSL_{{$sale->id}}</a> </td>
                        <td {{$class}}> {{$sale->user->fullName()}}</td>
                        <td {{$class}}> {{$sale->soldBy->fullName()}}</td>
                        <td {{$class}}> {{$approvedBy}}</td>
                        <td {{$class}} width="15%"> {!! $sale->getCustomerHref() !!}</td>
                        <td {{$class}}> {{number_format((double)$sale->total_amount , 2)}}</td>
                        <td {{$class}}> {{number_format((double)$sale->getBalance() , 2)}} </td>
                        <td {{$class}}> {{number_format((double)$sale->getPaidAmount() , 2)}} </td>
                        <td {{$class}}> <span class="label label-{{$printedClass}}">{{$printedLabel}}</span></td>
                        <td {{$class}}> <span class="label label-{{$uploadedClass}}">{{$uploadedLabel}}</span></td>
                        <td {{$class}}>
                            @component('layouts.button')

                                @can('access-module-component-functionality','sale_manage_update')
                                    @if(! $sale->is_approved)
                                        <li><a href="{{url('sale/manage/approve/'.$sale->id)}}">Approve</a></li>
                                        <li><a href="{{url('sale/manage/reject/'.$sale->id)}}">Cancel</a></li>
                                    @endif
                                @endcan

                                <li>
                                    <a data-toggle="modal" data-target="#details"
                                       data-reference="fanaka_{{$sale->id}}"
                                       data-sold_by="{{$sale->user->fullName()}}"
                                       data-customer="{{$sale->getCustomerDetails()}}"
                                       data-total_amount="{{number_format((double)$sale->total_amount , 2)}}"
                                       data-balance="{{number_format((double)$sale->balance , 2)}}"
                                       data-saleItems="{{json_encode($sale->saleItems)}}"
                                       id="detail">Details</a></li>

                                <li>
                                    <a href="{{url('sale/manage/receipt/'.$sale->id)}}" target="_blank">Receipt</a>
                                </li>

                                @if($sale->is_agreement_uploaded)
                                    <li>
                                        <a href="{{url('sale/manage/uploaded-agreement/'.$sale->id)}}" target="_blank">Scanned Agreement</a>
                                    </li>
                                @endif

                                @if( (!$sale->is_agreement_uploaded) && $sale->is_agreement_downloaded)
                                    <li>
                                        <a data-toggle="modal"
                                           data-title="Upload Scanned Agreement"
                                           data-id="{{$sale->id}}"
                                           id="uploadAgreementNow"
                                           data-target="#UploadScannedAgreement">
                                            Upload Agreement
                                        </a>
                                    </li>

                                    <li>
                                        <a href="{{url('sale/manage/agreement/'.$sale->id)}}"
                                           target="_blank">Agreement</a>
                                    </li>

                                @endif

                                @if( (!$sale->is_agreement_uploaded) && (! $sale->is_agreement_downloaded))

                                    <li>
                                        <a href="{{url('sale/manage/agreement/'.$sale->id)}}"
                                           target="_blank">Agreement</a>
                                    </li>

                                @endif

                                <li>
                                    <a data-toggle="modal" data-target="#witness"
                                       data-witness="{{($sale->witnesses)}}"
                                       data-sale_id="{{($sale->id)}}"
                                       data-customer_id="{{($sale->customer_id)}}"
                                       id="addWitness">Witnesses</a>
                                </li>
                            @endcomponent
                        </td>
                    </tr>
                @endforeach
                </tbody>
                <tfoot>
                <tr>
                    <td colspan="6"><strong>Total</strong></td>
                    <td><strong>{{ number_format($totalAmount , 2) }}</strong></td>
                    <td><strong>{{ number_format($totalBalance , 2) }}</strong></td>
                    <td colspan="4"><strong>{{ number_format($totalPaid , 2) }}</strong></td>
                </tr>
                </tfoot>
            </table>


        </div>
    @endcomponent

@endsection


@section('extra_js_footer')
    @include('layouts.dataTables.tablebuttons')
    <script>
        $("table").DataTable({sort:false})
    </script>
@endsection