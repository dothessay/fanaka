@extends('layouts.master')
@section('title','Late installments')
@section('content')


    @include('layouts.back')

    <form class="form-horizontal form-bordered" id="lateForm">
        <div class="form-group">
            <label for="" class="col-md-2 control-label">Start Date</label>
            <div class="col-md-4">
                <input
                        type="text"
                        name="start_date"
                        id="start_date"
                        value="{{isset($_GET['start_date']) ? $_GET['start_date']: old('start_date')}}"
                        class="form-control date">

            </div>
            <label for="" class="col-md-2 control-label">End Date</label>
            <div class="col-md-4">
                <input
                        type="text"
                        name="end_date"
                        id="end_date"
                        value="{{isset($_GET['end_date']) ? $_GET['end_date']: old('end_date')}}"
                        class="form-control date">

                <input
                        type="hidden"
                        name="pdf"
                        id="pdfValue"
                        class="form-control">

            </div>
        </div>
        <ion-item>
            <ion-button
                    size="6"
                    id="submit"
                    fill="outline"
                    color="primary"
            >Pull</ion-button>
            <ion-button
                    size="6"
                    id="pdfQuery"
                    fill="outline"
                    color="warning"
            >PDF</ion-button>
            <ion-button
                    size="6"
                    type="reset"
                    id="resetQuery"
                    fill="outline"
                    color="danger"
            >Reset</ion-button>
        </ion-item>
    </form>

    @include('sales.report.late._data')


    <ion-alert-controller></ion-alert-controller>


@endsection

@section('extra_js_footer')
    @include('layouts.form')
    @include('layouts.datePicker.datepicker')
    @include('layouts.dataTables.datatable')


    <script>
        (function () {
            $('table#data-table').DataTable()
            $('.date').datepicker()
        })()

        const startDate = document.querySelector('#start_date')
        const endDate = document.querySelector('#end_date')
        const submitQuery = document.querySelector('#submit')
        const resetQuery = document.querySelector('#resetQuery')
        const pdfQuery = document.querySelector('#pdfQuery')
        const pdfValue = document.querySelector('#pdfValue')
        const form = document.querySelector('#lateForm')
        const alertContr = document.querySelector('ion-alert-controller')

        submitQuery.addEventListener('click' , () => {

            if (startDate.value.trim().length < 1 || endDate.value.trim().length < 1){

                alertContr.create({
                    title: "All fields are required",
                    message: "Enter both start and end date",
                    buttons: ['Ok']
                }).then(alertElement => {

                    alertElement.present();
                })

                return '';
            }
            form.submit();
        });



        resetQuery.addEventListener('click' , () => {

            startDate.value = '';
            endDate.value = '';


        });

        pdfQuery.addEventListener('click' , () => {



            if (startDate.value.trim().length < 1 || endDate.value.trim().length < 1){

                alertContr.create({
                    title: "All fields are required",
                    message: "Enter both start and end date",
                    buttons: ['Ok']
                }).then(alertElement => {

                    alertElement.present();
                })

                return '';
            }
            pdfValue.value = "pull"
            form.submit();

        });


    </script>
@endsection
