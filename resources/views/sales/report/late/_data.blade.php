<table class="table-bordered table-striped table" id="data-table" width="100%">
    <thead>
    <tr>
        <th>Sale Reference</th>
        <th>Invoice Reference</th>
        <th>Sold By</th>
        <th>Customer</th>
        <th>Total Amount Due</th>
        <th>Total Amount Paid</th>
        <th>Total Balance</th>
        <th>Due Date</th>

    </tr>
    </thead>
    <tbody>
    @foreach($invoices as $invoice)
        <?php
        $sale = $invoice->sale;

        $color = isset($sale->color) ? $sale->color : "";
        $class = "style=background:{$color}";

        if(! $sale->is_approved)
        {
            $class = "style=background:lightblue";
        }

        $approvedBy = isset($sale->approvedBy) ? $sale->approvedBy->fullName() : "Not Approved";
        ?>
        <tr>
            <td {!! $class !!}><a href="{{url('sale/manage/approve-sale/'.$invoice->sale->id)}}" data-title="Click to see sale details"> FSL_{{$invoice->sale->id}}</a> </td>
            <td {!! $class !!}>FINV_{{$invoice->id}}</td>
            <td {!! $class !!}>{{$invoice->user->fullName()}}</td>
            <td {{$class}} width="15%"> {!! $invoice->sale->getCustomerHref() !!}</td>
            <td {!! $class !!}>{{number_format((double)$invoice->total_amount , 2)}}</td>
            <td {!! $class !!}>{{number_format((double)$invoice->totalPaid() , 2)}}</td>
            <td {!! $class !!}>{{$balance = number_format(((double)$invoice->sale->getBalance()) , 2)}}</td>
            <td {!! $class !!}>{{$invoice->due_date}}</td>

        </tr>
    @endforeach
    </tbody>
</table>
