@extends('layouts.master')


@section('title','Agent Report')


@section('content')


@component('layouts.partials.panel')

    <div class="col-md-12">


        <form class="form-horizontal form-bordered">

            <div class="form-group">
                <label class="control-label col-md-2">Type</label>
                <div class="col-md-4">
                    <select name="type" class="selectpicker" data-live-search="true">



                        <option value="sale" @if(old('type' , request('type')) == "sale" ) selected @endif>Sales</option>
                        <option value="reservation"  @if( old('type', request('type')) ==    "reservation") selected @endif>Reservation</option>

                    </select>
                </div>

                <label class="control-label col-md-1">Agent</label>
                <div class="col-md-4">
                    <select name="agent" class=" selectpicker" data-live-search="true">
                        <option value="">All</option>

                        @foreach($agents as $agent)

                            <option value="{{$agent->id}}"
                                    @if(isset($_GET['agent']) && $agent->id == $_GET['agent']) selected @endif
                            >{{$agent->fullName()}}</option>
                        @endforeach
                    </select>
                </div>
            </div>

            <div class="form-group">
                <label class="control-label col-md-2">Start Date</label>
                <div class="col-md-4">
                    <input type="text" name="start_date" value="{{@$_GET['start_date']}}" class="date form-control">
                </div>


                <label class="control-label col-md-1">End Date</label>
                <div class="col-md-4">
                    <input type="text" name="end_date"value="{{@$_GET['end_date']}}" class="date form-control">
                </div>
            </div>


            <div class="modal-footer">
                <button type="submit" class="btn btn-warning" name="pdf">Pull Pdf</button>
                <button type="submit" class="btn btn-primary">Pull</button>
            </div>

        </form>



        @if($sales->count())

            <div class="table-responsive">
                @include('sales.report.agent._data')

            </div>

        @endif
    </div>
    @endcomponent







@endsection


@section('extra_js_footer')
    @include('layouts.datePicker.datepicker')
    @include('layouts.dataTables.datatable')
    <script>
        $("input.date").datetimepicker()
        $("table#data-table").DataTable()
    </script>

    @endsection
