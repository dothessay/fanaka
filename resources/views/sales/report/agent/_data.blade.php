<div class="table-responsive">
    <table class="table-bordered table-striped table no-padding" cellpadding="0" cellspacing="0" id="data-table" width="100%">
        <thead>
        <tr>
            <th> Date Recorded </th>
            @if(request()->has('type') && request('type') == 'sale')
                <th>{{ fanakaConfig('commission_percentage', 50) }} Date </th>

            @endif

            <th>Agent</th>
            <th>Customer</th>
            <th>Project</th>
            <th>Plots</th>
            <th>Total Amount</th>
            <th>Balance</th>
            <th>Paid</th>
            <th>Paid %</th>

        </tr>
        </thead>
        <tbody>


        @php $total = 0 ; $balance = 0 ;@endphp
        @foreach($sales as $key => $sale)


            <?php

            $total += $sale->total_amount;
            $balance += $sale->getBalance();
            ?>

            <tr>

                <td style="{!! $sale->color !!}"> {{ isset($sale->created_at) ? $sale->created_at : "-- " }}</td>
                @if(request()->has('type') && request('type') == 'sale') <td style="{!! $sale->color !!}">{{$sale->getHalfPaymentDate()}}</td> @endif
                <td style="{!! $sale->color !!}">{{ isset($sale->soldBy->id)? $sale->soldBy->fullName(): ''}}</td>
                <td style="{!! $sale->color !!}">{!! $sale->getCustomerDetails() !!}</td>
                <td style="{!! $sale->color !!}"> {{$sale->getProjects()}}</td>
                <td style="{!! $sale->color !!}"> {{$sale->getCustomerPlotNos()}}</td>
                <td style="{!! $sale->color !!}"> {{number_format((double)$sale->total_amount , 2)}}</td>
                <td style="{!! $sale->color !!}">

                        {{number_format((double)$sale->getBalance() , 2)}}


                </td>
                <td style="{!! $sale->color !!}"> {{number_format( $sale->payments()->sum('amount') , 2) }}  </td>
                <td style="{!! $sale->color !!}"> {{number_format( $sale->getPercentage() , 2) }} % </td>

            </tr>
        @endforeach
        </tbody>

    </table>

    {{--<div class="pagination">
        {{$sales->links()}}
    </div>--}}

</div>
