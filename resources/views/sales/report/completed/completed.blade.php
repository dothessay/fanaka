@extends('layouts.master')
@section('title','Completed')

@section('content')

    <ul class="nav nav-pills">
        <li class="active">
            <a href="{{route('sale_reports')}}">Back</a>
        </li>
    </ul>
    @component('layouts.partials.panel')

        <form class="form-horizontal">
            <div class="form-group">
                <label class="col-md-2"> Projects  </label>

                <div class="col-md-4">

                    <select
                    name="product_id"
                    data-live-search="true"
                    data-size="8"
                    class="form-control selectpicker">
                        @foreach($projects as $project)
                            <option
                                    value="{{ $project->id }}"
                                    @if(request()->has('product_id') && request('product_id') == $project->id) selected @endif
                            >{{ $project->name() }}</option>
                        @endforeach
                    </select>



                </div>


            </div>


            <div class="modal-footer">
                <button type="submit" class="btn btn-success">Pull</button>
                <input type="submit" name="pdf" value="Print" class="btn btn-primary">
                <input type="submit" name="excel" value="Excel" class="btn btn-warning">
            </div>

        </form>


        @include('sales.report.completed.data')
    @endcomponent

@endsection



@section('extra_js_footer')
    @include('layouts.dataTables.datatable')
    @include('layouts.datePicker.datepicker')

    <script>
        $(function () {

            $('#data-table').DataTable({
                sort:false
            })


            $(".date").datepicker();
        })
    </script>


@endsection