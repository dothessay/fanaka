<div class="table-responsive">
    <table class="table-bordered table-striped table" id="data-table" width="100%">
        <thead>
        <tr>
            <th>Sale date</th>
            <th>Agent</th>
            <th>Customer</th>
            <th>Plot</th>
            <th>Total Amount</th>
            <th>Balance</th>
            <th>Percentage</th>
        </tr>
        </thead>
        <tbody>
        @php $total = 0 ; $balance = 0 ;@endphp
        @foreach($completed as $key => $sale)

        <?php


            $total += $sale->total_amount;
            $balance += $sale->getBalance();


            $color = isset($sale->color) ? $sale->color : "";
            $class = "style=background:{$color}";

            if(! $sale->is_approved)
            {
                $class = "style=background:lightblue";
            }

            $approvedBy = isset($sale->approvedBy) ? $sale->approvedBy->fullName() : "Not Approved";
            ?>
            <tr>
                <td> {{$sale->created_at}}</td>
                <td> {{$sale->soldBy->fullName()}}</td>
                <td> {{$sale->getCustomerDetails()}}</td>
                <td> {{$sale->getCustomerPlots()}}</td>
                <td> {{number_format((double)$sale->total_amount , 2)}}</td>
                <td> {{number_format((double)$sale->getBalance() , 2)}} </td>
                <td> {{$sale->getPercentage()}} % </td>

            </tr>
        @endforeach
        </tbody>

            <tfoot>
            <tr>


                <td colspan="4">Totals</td>

                <td> <b>{{number_format((double)$total , 2)}}</b></td>
                <td> <b>{{number_format((double)$balance , 2)}}</b> </td>
                <td> </td>

            </tr>
            </tfoot>
    </table>

</div>