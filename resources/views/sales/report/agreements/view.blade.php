@extends('layouts.master')
@section('title','All sale uploads')

@section('content')


    <ul class="nav nav-pills">
        <li class="active">
            <a data-toggle="modal"
               data-title="Upload Scanned Agreement"
               data-id="{{$sale->id}}"
               id="uploadAgreementNow"
               data-target="#UploadScannedAgreement">
                Upload Agreement
            </a>
        </li>
    </ul>
    @component('layouts.partials.panel')


        <div class="table-responsive">
            <table class="table table-bordered table-striped">
                <thead>
                <tr>
                    <th>File Name</th>
                    <th>Upload Type</th>
                    <th>Download</th>
                </tr>
                </thead>
                <tbody>
                @foreach($saleUploads as $saleUpload)
                    <tr>
                        <td>{{$saleUpload->file_name}}</td>
                        <td>{{$saleUpload->type}}</td>
                        <td>
                            <a href="{{url('sale/manage/uploaded-agreement/'.$saleUpload->id)}}"  class="label label-primary" target="_blank">download
                                Agreement
                            </a>
                        </td>

                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    @endcomponent


    @component('layouts.partials.modal')

        @slot('dialogClass','modal-lg')
        @slot('id','UploadScannedAgreement')
        @slot('title') <h6>Upload Scanned More Files for this Sale <span>SALE REF FSL_{{$sale->id}}</span></h6>@endslot

        <form method="post" action="{{route('sale_manage_upload-scanned-agreement')}}" enctype="multipart/form-data">
            {{csrf_field()}}
            <input type="hidden" name="id"  value="{{$sale->id}}">

            <div class="form-group">
                <label class="control-label col-md-2">Upload Agreement</label>
                <div class="col-md-3">
                <input name="agreement[]" multiple type="file" class="document-file">
                </div>

            <label class="control-label col-md-1">Upload Type</label>
            <div class="col-md-3">

                <select name="upload_type[]" class="form-control selectpicker">

                    <option>Agreement</option>
                    <option>Acknowledgment</option>
                    <option>Receipt</option>
                    <option>Title</option>
                </select>

            </div>

            <div class="modal-footer">
                <button type="submit" class="btn btn-success">Upload</button>
            </div>

        </form>


    @endcomponent

@endsection
