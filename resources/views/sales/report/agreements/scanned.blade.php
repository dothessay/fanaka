@extends('layouts.master')
@section('title','All Uploaded Agreement Uploaded')
@section('content')

        <ul class="nav nav-tabs tabs-uzapoint">

            <li class="active">
                <a href="#default-tab-1" data-toggle="tab">
                    <span class="visible-xs">Without Agreement</span>
                    <span class="hidden-xs">Without Agreement</span>
                </a>
            </li>

            <li>
                <a href="#default-tab-2" data-toggle="tab">
                    <span class="visible-xs">Scanned Documents</span>
                    <span class="hidden-xs">Scanned Documents</span>
                </a>


            </li>


        </ul>

        @php
            $active = true;

        @endphp

        <div class="tab-content">


            <div class="tab-pane fade active in" id="default-tab-1">
                <form class="form-horizontal" onautocomplete="off">
                    <div class="form-group">
                        <label for="start_date" class="control-label col-md-2">Start Date</label>
                        <div class="col-md-4">
                            <input
                                type="text"
                                name="start_date"
                                id="start_date"
                                class="form-control date"
                                value="{{ old('start_date', request()->has('start_date') ? request('start_date') : old('start_date')) }}"
                        >
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="end_date" class="control-label col-md-2">End Date</label>
                       <div class="col-md-4">
                           <input
                                   type="text"
                                   name="end_date"
                                   id="end_date"
                                   class="form-control date"
                                   value="{{ old('end_date', request()->has('end_date') ? request('end_date') : old('end_date')) }}"
                           >
                       </div>
                        <input name="un_printed" type="hidden">
                    </div>
                    <div class="email-footer">
                       <div class="align-content-between">
                           <button class="btn-success btn">Get Clients without Agreement</button>
                           <button name="pdf" class="btn-success btn">Print PDF of clients without Agreement</button>
                       </div>
                    </div>
                </form>
                <div class="table-responsive">
                    @include('sales._list_table', ['sales' => $unprinted])
                </div>
            </div>
            <div class="tab-pane fade  in" id="default-tab-2">
                <div class="table-responsive">
                    <table class="table-bordered table-striped table" cellpadding="10" cellspacing="10" id="scanned-table"
                           width="100%">
                        <thead>
                        <tr>
                            <th>Sale Reference</th>
                            <th>Entered By</th>
                            <th>Agent</th>
                            <th>Customer</th>
                            <th>Total Amount</th>
                            <th>Balance</th>
                            <th>download</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($sales as $sale)

                            <tr>
                                <td> FSL_{{$sale->id}}</td>
                                <td> {{$sale->user->fullName()}}</td>
                                <td> {{$sale->soldBy->fullName()}}</td>
                                <td width="15%"> {!! $sale->getCustomerHref() !!}</td>
                                <td> {{number_format((double)$sale->total_amount , 2)}}</td>
                                <td> {{number_format((double)$sale->getBalance() , 2)}} </td>
                                <td>
                                    @if($sale->is_agreement_uploaded)

                                        <a href="{{url('sale/manage/get-uploaded-agreement/'.$sale->id)}}"
                                           class="label label-primary" target="_blank">View Docs</a>
                                    @endif
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    {{$sales->links()}}


                </div>
            </div>
        </div>


@endsection


@section('extra_js_footer')

    @include('layouts.dataTables.datatable')
    @include('layouts.datePicker.datepicker')

    <script>
        $("#data-table").DataTable({
            sort: true,
        });
        $("#scanned-table").DataTable({
            sort: false,
            paginate: false
        })
        $("input.date").datepicker()
    </script>

@endsection
