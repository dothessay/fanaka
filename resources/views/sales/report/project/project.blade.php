@extends('layouts.master')
@section('title','Report By Project')

@section('content')

    <ul class="nav nav-pills">
        <li class="active">
            <a href="{{route('sale_reports')}}">Back</a>
        </li>
    </ul>

    @component('layouts.partials.panel')

        <form class="form-horizontal form-bordered">
            <div class="form-group">
                <label class="col-md-2">Project </label>

                <div class="col-md-4">

                    <select class="form-control selectpicker" name="product_id" data-live-search="true">
                        <option value="all"
                                @if(request()->has('product_id') && request('product_id') == "all")
                                selected
                                @endif
                        >{{ __('All') }}</option>
                        @foreach($projects as $project)

                            <option value="{{$project->id}}"
                                    @if(request()->has('product_id') && request('product_id') == $project->id)
                                        selected
                                    @endif
                            >{{$project->name()}}</option>

                        @endforeach

                    </select>

                </div>

                <label class="col-md-2"> Sale Type</label>
                <div class="col-md-4">

                    <select class="form-control selectpicker" name="sale_type" data-live-search="true">
                        <option
                                @if(request()->has('sale_type') && request('sale_type') =='Cash')
                                selected
                                @endif
                        > Cash</option>
                        <option
                                @if(request()->has('sale_type') && request('sale_type') =='Instalment')
                                selected
                                @endif
                        > Instalment</option>
                    </select>

                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-2" for="balance">With Balance</label>
                <div class="col-md-4">
                    <input
                            @if(request()->has('with_balance') )
                            checked
                            @endif
                            type="checkbox" id="balance" name="with_balance">

                </div>
                <label class="control-label col-md-2" for="pass_month">Past One Month</label>
                <div class="col-md-4">
                    <input
                            @if(request()->has('pass_month') )
                            checked
                            @endif
                            type="checkbox"
                            id="pass_month"
                            name="pass_month">

                </div>
            </div>


            <div class="modal-footer">
                <div class="align-content-between">
                    <div>
                        <button type="submit" class="btn btn-success">Pull</button>
                        <input type="submit" name="pdf" value="Print" class="btn btn-sm btn-danger">
                        <input type="submit" name="excel" value="Excel" class="btn btn-sm btn-warning">
                    </div>
                </div>


            </div>

        </form>


        <div class="table-responsive">

            @include('sales.report.project.data')
        </div>


    @endcomponent


@endsection



@section('extra_js_footer')


@endsection
