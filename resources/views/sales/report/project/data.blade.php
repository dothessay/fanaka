    <table class="table-bordered table-striped table" id="data-table" width="100%" cellspacing="2" cellpadding="2">
        <thead>
        <tr>
            <th>Date Sold</th>
            <th>Land Location</th>
            <th>Selling Price</th>
            <th>Balance</th>
            <th>Last Amount Paid</th>
            <th>Last Payment Date</th>
            <th>Customer</th>
            <th>Tel</th>
            <th>Agent</th>
        </tr>
        </thead>
        <tbody>
        @foreach($sales as $sale)
            <tr>
                <td> {{ $sale->created_at->format('Y-m-d') }}</td>
                <td> {{ $sale->getProject() .' plot no:  '. $sale->getPlotNo() }}</td>
                <td> {{ number_format(floatval($sale->total_amount)  , 2) }}</td>
                <td> {{  number_format(floatval($sale->balance) , 2) }}</td>
                <td> {{  number_format(floatval($sale->lastPaymentAmount()) , 2) }}</td>
                <td> {{  $sale->lastPaymentDate() }}</td>
                <td> {!! $sale->getCustomerHref() !!}</td>
                <td> {!! $sale->getCustomerPhoneNumber() !!}</td>
                <td> {!! $sale->soldBy->fullName() !!}</td>
            </tr>

        @endforeach

        </tbody>
        <tfoot>
        <tr>
            <td>Totals</td>
            <td>--</td>
            <td> {{
                number_format( $sales->map( function ($sale) {
                                        return $sale->saleItems->map(function ($item) {
                                         return  floatval($item->plot->price);
                                         })->sum();
                                          })->sum(), 2)
            }}
            </td>
            <td>{{ number_format(floatval($sales->sum('total_amount')) , 2) }}</td>
            <td>{{ number_format(floatval($sales->sum('balance'))  , 2) }}</td>
            <td>--</td>
            <td>--</td>
            <td>--</td>
        </tr>
        </tfoot>


    </table>


