<form class="form-horizontal form-bordered">

    <div class="form-group">
        {{--<label class="col-md-1 control-label">Sale Ref</label>
        <div class="col-md-3">
            <input type="number" class="form-control col-md-4 sale-ref-search">
        </div>--}}

        <label class="col-md-2 control-label">Select Customer</label>
        <div class="col-md-4">
            <select name="customer_id" class="form-control selectCustomer" data-live-search="true" data-size="10">

                <option data-hidden="true">Select customer</option>
                @foreach($customers as $customer)
                    <option value="{{$customer->id}}">{{$customer->fullName()}}</option>
                @endforeach
            </select>


        </div>

        <div class="col-md-2">
            <button class="btn btn-primary">Get sales</button>
        </div>


    </div>

</form>
<div class="table-responsive">
    <table class="table-bordered table-striped table" id="new-sale-table" width="100%">
        <thead>
        <tr>
            <th>Sale Date</th>
            <th>Sale Reference</th>
            <th>Entered By</th>
            <th>Agent</th>
            <th>Customer</th>
            <th>Total Amount</th>
            <th>Balance</th>
            @can('access-module-component-functionality','sale_manage_approve')
                <th>Action</th>

            @endcan
        </tr>
        </thead>
        <tbody>
        @foreach($approved as $sale)

            <?php

            $color = isset($sale->color) ? $sale->color : "";
            $class = "style=background:{$color}";

            $printedClass = 'warning';

            $printedLabel = "Not Printed";
            $uploadedLabel = "Not Uploaded";

            $uploadedClass = 'warning';

            if ($sale->is_agreement_downloaded) {
                $printedClass = 'primary';

                $printedLabel = "printed";

            }

            if ($sale->is_agreement_uploaded) {

                $uploadedClass = 'primary';


                $uploadedLabel = 'Uploaded';
            }


            if (!$sale->is_approved) {
                $class = "style=background:lightblue";
            }

            $approvedBy = isset($sale->approvedBy) ? $sale->approvedBy->fullName() : "Not Approved";
            ?>
            <tr>
                <td {{$class}}> {{$sale->created_at}}</td>
                <td {{$class}}><a
                            href="{{url('sale/manage/approve-sale/'.$sale->id)}}">FSL_{{$sale->id}}</a></td>
                <td {{$class}}> {{$sale->user->fullName()}}</td>
                <td {{$class}}> {{$sale->soldBy->fullName()}}</td>
                <td {{$class}} width="15%"> {!! $sale->getCustomerHref() !!}</td>
                <td {{$class}}> {{number_format((double)$sale->total_amount , 2)}}</td>
                <td {{$class}}> {{number_format((double)$sale->getBalance() , 2)}} </td>

                @can('access-module-component-functionality','sale_manage_approve')
                    <td {{$class}}>
                    @component('layouts.button')

                        @can('access-module-component-functionality','sale_manage_update')
                            @if(! $sale->is_approved)
                                <li>
                                    <a href="{{url('sale/manage/approve-sale/'.$sale->id)}}" target="_blank">Details</a>

                                </li>
                            @endif
                        @endcan



                        <li>
                            <a href="{{url('sale/manage/receipt/'.$sale->id)}}" target="_blank">Receipt</a>

                        </li>

                        @can('access-module-component-functionality','sale_manage_pay-title')
                            <li>
                                <a href="{{ url('sale/manage/pay-title'. $sale->id )  }}"
                                   target="_blank">Pay Title</a>
                            </li>
                        @endcan

                        @if($sale->is_agreement_uploaded && $sale->is_approved)
                            <li>
                                <a href="{{url('sale/manage/get-uploaded-agreement/'.$sale->id)}}"
                                   target="_blank">Scanned Agreement</a>
                            </li>
                        @endif

                        @if( (!$sale->is_agreement_uploaded) && $sale->is_agreement_downloaded)
                            <li>
                                <a data-toggle="modal"
                                   data-title="Upload Scanned Agreement"
                                   data-id="{{$sale->id}}"
                                   id="uploadAgreementNow"
                                   data-target="#UploadScannedAgreement">
                                    Upload Agreement
                                </a>
                            </li>

                            <li>
                                <a href="{{url('sale/manage/agreement/'.$sale->id)}}"
                                   target="_blank">Agreement</a>
                            </li>

                        @endif

                        @if( (!$sale->is_agreement_uploaded) && (! $sale->is_agreement_downloaded))

                            <li>
                                <a href="{{url('sale/manage/agreement/'.$sale->id)}}"
                                   target="_blank">Agreement</a>
                            </li>

                        @endif

                        <li>
                            <a data-toggle="modal" data-target="#witness"
                               data-witness="{{($sale->witnesses)}}"
                               data-sale_id="{{($sale->id)}}"
                               data-customer_id="{{($sale->customer_id)}}"
                               id="addWitness">Witnesses</a>
                        </li>
                    @endcomponent
                </td>
                    @endcan
            </tr>
        @endforeach
        </tbody>
    </table>

    {{--<div class="pagination">
        {{$sales->links()}}
    </div>--}}

</div>
