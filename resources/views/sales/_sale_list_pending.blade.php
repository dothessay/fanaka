{{--@can('access-module-component-functionality','sale_manage_sale-statistics')

<div class="row">
    {!! statisticWidget('yearly sale Payment', $yearly, url('sale/manage/view?report=yearly')) !!}
    {!! statisticWidget('monthly sale Payment',$monthly , url('sale/manage/view?report=monthly')) !!}
    {!! statisticWidget('weekly sale Payment',$weekly, url('sale/manage/view?report=weekly')) !!}
    {!! statisticWidget('daily sale Payment',$daily, url('sale/manage/view?report=daily')) !!}
</div>
@endcan--}}


<form class="form-horizontal form-bordered">

    <div class="form-group">
        {{--<label class="col-md-1 control-label">Sale Ref</label>
        <div class="col-md-3">
            <input type="number" class="form-control col-md-4 sale-ref-search">
        </div>--}}

        <label class="col-md-2 control-label">Select Customer</label>
        <div class="col-md-4">
            <select name="customer_id" class="form-control selectCustomer" data-live-search="true" data-size="10">

                <option data-hidden="true">Select customer</option>
                @foreach($customers as $customer)
                    <option value="{{$customer->id}}">{{$customer->fullName()}}</option>
                @endforeach
            </select>


        </div>

        <div class="col-md-2">
            <button class="btn btn-primary">Get sales</button>
        </div>


    </div>

</form>

<div class="table-responsive">
    <table class="table-bordered table-striped table" id="data-table" width="100%">
        <thead>
        <tr>
            <th>Sale Date</th>
            <th>Sale Reference</th>
            <th>Entered By</th>
            <th>Agent</th>
            <th>Approved By</th>
            <th>Customer</th>
            <th>Total Amount</th>
            <th>Balance</th>
            <th>Is Agreement Approved</th>
            <th>Printed</th>
            <th>Uploaded</th>
            <th>Action</th>
        </tr>
        </thead>
        <tbody>
        @foreach($sales->where('is_approved', false) as $sale)

            <?php

            $color = isset($sale->color) ? $sale->color : "";
            $class = "style=background:{$color}";

            $printedClass = 'warning';

            $printedLabel = "Not Printed";
            $uploadedLabel = "Not Uploaded";

            $uploadedClass = 'warning';
            $agreementApprovedClass = 'warning';
            $agreementApprovedLabel = 'No Approved';

            if ($sale->is_agreement_downloaded) {
                $printedClass = 'primary';

                $printedLabel = "printed";

            }

            if ($sale->is_agreement_uploaded) {

                $uploadedClass = 'primary';


                $uploadedLabel = 'Uploaded';
            }
            if ($sale->is_agreement_approvedd) {

                $agreementApprovedClass = 'primary';


                $agreementApprovedLabel = isset($sale_agreement_request) ? "Agreement Request Sent" : $sale->is_agreement_approved ? "Approved" : "Not Approved";
            }


            if (!$sale->is_approved) {
                $class = "style=background:lightblue";
            }

            $approvedBy = isset($sale->approvedBy) ? $sale->approvedBy->fullName() : "Not Approved";
            ?>
            <tr>
                <td {{$class}}> {{$sale->created_at}}</td>
                <td {{$class}}><a
                            href="{{url('sale/manage/approve-sale/'.$sale->id)}}">FSL_{{$sale->id}}</a></td>
                <td {{$class}}> {{$sale->user->fullName()}}</td>
                <td {{$class}}> {{$sale->soldBy->fullName()}}</td>
                <td {{$class}}> {{$approvedBy}}</td>
                <td {{$class}} width="15%"> {!! $sale->getCustomerHref() !!}</td>
                <td {{$class}}> {{number_format((double)$sale->total_amount , 2)}}</td>
                <td {{$class}}> {{number_format((double)$sale->getBalance() , 2)}} </td>
                <td {{$class}}><span class="label label-{{$agreementApprovedClass}}">{{$agreementApprovedLabel}}</span>
                </td>
                <td {{$class}}><span class="label label-{{$printedClass}}">{{$printedLabel}}</span></td>
                <td {{$class}}><span class="label label-{{$uploadedClass}}">{{$uploadedLabel}}</span></td>
                <td {{$class}}>
                    @component('layouts.button')

                        @if( $sale->is_approved)
                            @can('access-module-component-functionality','sale_manage_update')
                                @if(! $sale->is_approved)
                                    <li><a href="{{url('sale/manage/approve/'.$sale->id)}}">Approve</a></li>
                                    <li><a href="{{url('sale/manage/reject/'.$sale->id)}}">Cancel</a></li>
                                @endif
                            @endcan

                            <li>
                                <a data-toggle="modal" data-target="#details"
                                   data-reference="fanaka_{{$sale->id}}"
                                   data-sold_by="{{$sale->user->fullName()}}"
                                   data-customer="{{$sale->getCustomerDetails()}}"
                                   data-total_amount="{{number_format((double)$sale->total_amount , 2)}}"
                                   data-balance="{{number_format((double)$sale->balance , 2)}}"
                                   data-saleItems="{{json_encode($sale->saleItems)}}"
                                   id="detail">Details</a></li>

                            @if($sale->is_approved)
                                <li>
                                    <a href="{{url('sale/manage/receipt/'.$sale->id)}}" target="_blank">Receipt</a>

                                </li>
                            @endif


                            @can('access-module-component-functionality','sale_manage_pay-title')
                                <li>
                                    <a href="{{ route('sale_manage_pay-title', ['saleId' => $sale->id])  }}"
                                       target="_blank">Pay Title</a>
                                </li>
                            @endcan

                            @if($sale->is_agreement_uploaded)
                                <li>
                                    <a href="{{url('sale/manage/get-uploaded-agreement/'.$sale->id)}}"
                                       target="_blank">Scanned Agreement</a>
                                </li>
                            @endif

                            @if( (!$sale->is_agreement_uploaded) && $sale->is_agreement_downloaded)
                                <li>
                                    <a data-toggle="modal"
                                       data-title="Upload Scanned Agreement"
                                       data-id="{{$sale->id}}"
                                       id="uploadAgreementNow"
                                       data-target="#UploadScannedAgreement">
                                        Upload Agreement
                                    </a>
                                </li>


                            @endif
                            @if($sale->is_approved)
                                @if( (!$sale->is_agreement_uploaded) && (! $sale->is_agreement_downloaded))

                                    <li>
                                        <a href="{{url('sale/manage/agreement/'.$sale->id)}}"
                                           target="_blank">Agreement</a>
                                    </li>

                                @endif
                            @endif

                            <li>
                                <a data-toggle="modal" data-target="#witness"
                                   data-witness="{{($sale->witnesses)}}"
                                   data-sale_id="{{($sale->id)}}"
                                   data-customer_id="{{($sale->customer_id)}}"
                                   id="addWitness">Witnesses</a>
                            </li>
                        @endif
                    @endcomponent
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>

    {{--<div class="pagination">
        {{$sales->links()}}
    </div>--}}

</div>
