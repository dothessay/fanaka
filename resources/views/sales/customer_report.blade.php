@extends('layouts.master')
@section('title','Customer report')
@section('content')

@component('layouts.partials.panel')

    <form class="form-horizontal">
        <div class="form-group">
            <label class="col-md-2"> Start Date  </label>


            <div class="col-md-4">
                <input type="text" name="start_date" class="form-control date">
            </div>

            <label class="col-md-2"> End  Date  </label>
            <div class="col-md-4">
                <input type="text" name="end_date" class="form-control date">
            </div>

        </div>


        <div class="modal-footer">
            <button type="submit" class="btn btn-success">Pull</button>
            <input type="submit" name="pdf" value="Print" class="btn btn-primary">
        </div>

    </form>



@include('sales._customer_data')

@endcomponent





@endsection


@section('extra_js_footer')
@include('layouts.dataTables.datatable')
    @include('layouts.datePicker.datepicker')

    <script>
        $(function () {

            $('#data-table').DataTable({
                sort:false
            })


            $(".date").datepicker();
        })
    </script>


@endsection