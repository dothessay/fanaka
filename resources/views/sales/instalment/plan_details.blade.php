@extends('layouts.master')
@section('title','Instalment Plan Details for: '. $sale->getCustomerDetails()  )
@section('content')

    <div class="table-responsive">
        <h4 class="text-danger panel-heading">
            Sale Amount : {{ number_format($sale->getTotalSaleAmount() , 2) }} Paid Amount {{ number_format($sale->getPaidAmount() , 2) }}
             {{ $sale->getProject() . ' Plot No: ' .$sale->getPlotNo()}}
        </h4>
        <table class="table-striped table table-bordered">
            <thead>
            <tr>
                <th>Payment Date</th>
                <th>Amount To Be Paid</th>
                <th>Amount Paid</th>
                <th>Balance</th>
            </tr>
            </thead>
            <tbody>
            <?php $paid = 0?>
            <?php $balance = 0?>
            <?php $prepaid = 0?>
            @foreach($sale->installmentPlans as $plan)

                <tr>
                    <td width="20%">{{ $plan->payment_date }}</td>
                    <td width="20%">{{  number_format($plan->amount) }}</td>
                    <td width="20%">{{ $plan->amountPaid( $plan->payment_date , $paid)  }}</td>
                    <td width="20%">{{ $plan->amount -  $plan->amountPaid( $plan->payment_date, $paid)  }}</td>
                </tr>

                <?php
                $paid += $plan->amountPaid( $plan->payment_date, $paid) ;
                $balance +=  $plan->amount -  $plan->amountPaid( $plan->payment_date);
                ?>
            @endforeach
            </tbody>
            <tfoot>
            <tr>
                <td>Total:</td>
                <td>{{ number_format($amount = $sale->installmentPlans->sum('amount') , 2)  }}</td>
                <td>{{ number_format(floatval($paid) , 2)  }}</td>

                <td> {{ number_format($sale->getTotalSaleAmount() - $paid , 2) }}</td>
            </tr>
            </tfoot>
        </table>
    </div>
@endsection
@section('extra_js_footer')
    @include('layouts.dataTables.datatable')
    <script>
        $("table").DataTable()
    </script>
@endsection