@extends('layouts.master')
@section('title','Instalment Plan Details')
@section('content')

    <div class="table-responsive">
        <table class="table-striped table table-bordered">
            <thead>
            <tr>
                <th>Customer</th>
                <th>Project</th>
                <th>Balance</th>
                <th>Completion Date</th>
                <th></th>
            </tr>
            </thead>
            <tbody>

            @foreach($invoices as $invoice)
                <tr>
                    <td width="20%">{{ $invoice->sale->getCustomerDetails() }}</td>
                    <td width="20%">{{ $invoice->sale->getCustomerPlotNos() }}</td>
                    <td width="20%">{{ (number_format($invoice->balance() , 2))  }}</td>
                    <td width="20%">{{ $invoice->sale->getSaleCompletionDate() }}</td>
                    <td width="20%"><a href="{{ route('sale_manage_instalment_plan.details', ['sale'  => $invoice->sale->id]) }}" role="button"> View</a> </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
@endsection
@section('extra_js_footer')
    @include('layouts.dataTables.datatable')
    <script>
        $("table").DataTable()
    </script>
@endsection