@extends('layouts.master')
@section('title','Make Sales')
@section('content')


    @component('layouts.partials.panel')

        <div id="app">
            <form class="form-horizontal submitNewSale form-bordered" @submit.prevent="" method="post"
                  action="{{route('sale_manage_store')}}" enctype="multipart/form-data">
                {{csrf_field()}}


                <div class="form-group hide-if-new">

                    <label class="col-md-2 control-label">Existing Customer</label>
                    <div class="col-md-4">

                        <select class="form-control selectpicker" data-live-search="true" data-size="10"
                                id="selected-customer" name="customer_id">
                            @foreach($customers as $customer)

                                <option
                                        value="{{$customer->id}}"
                                        data-fullname="{{$customer->fullName()}}"
                                        data-email="{{$customer->email}}"
                                        data-phone_number="{{$customer->phone_number}}"
                                        data-id_no="{{$customer->id_no}}"
                                        id="">{{$customer->fullName()}}</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="customer-details col-md-6">
                        <div style="
            background:#fff;
            box-shadow:0 20px 50px rgba(0,0,0,.1);
            border-radius:10px;
            font-size: small;
            font-style: italic;
            transition:0.5s;">
                            <div class="box">
                                <span class="append-here"></span>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label col-md-2">New Customer</label>
                    <div class="col-md-4">
                        <input type="checkbox" data-render="switchery" id="new_customer" name="new_customer"
                               data-theme="default">
                    </div>
                </div>


                <div class="display_new_customer" style="display: none">

                    <div class="form-group">
                        <label class="control-label col-md-2">{{__('Customer Type')}}</label>
                        <div class="col-md-4">
                            <select
                                    class="form-control selectpicker"
                                    id="type"
                                    data-live-search="true"
                            >
                                @foreach(\Codex\Classes\Helper::system()->customerTypes as $type)

                                    <option value="{{$type}}">{{$type}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>


                    <div class="form-group">
                        <label class="control-label col-md-2">First Name <span class="required-uzapoint">*</span>
                        </label>

                        <div class="col-md-4">
                            <input type="text" class="form-control" name="first_name[]">
                        </div>
                        <label class="control-label col-md-2">Last Name <span class="required-uzapoint">*</span>
                        </label>
                        <div class="col-md-4">
                            <input type="text" class="form-control" name="last_name[]" placeholder="500000">
                        </div>
                    </div>


                    <div class="form-group">
                        <label class="control-label col-md-2">Middle Name <span class="required-uzapoint">*</span>
                        </label>
                        <div class="col-md-4">
                            <input type="text" class="form-control" name="middle_name[]" value=".">
                        </div>
                        <label class="control-label col-md-2"><span class="replaceId">Id No </span> <span
                                    class="required-uzapoint">*</span> </label>
                        <div class="col-md-4">
                            <input type="text" class="form-control" name="id_no[]" placeholder="12345678">
                        </div>
                    </div>


                    <div class="form-group">
                        <la-plbel class="control-label col-md-2">Email <span
                                    class="required-uzapoint">*</span>
                        </la-plbel>
                        <div class="col-md-4">
                            <input type="text" class="form-control" name="email[]">
                        </div>
                        <label class="control-label col-md-2">Phone number <span class="required-uzapoint">*</span>
                        </label>
                        <div class="col-md-4">
                            <input type="text" class="form-control phoneNumber" name="phone_number[]"></div>
                    </div>
                    <span></span>
                    <span class="addCustomerHtml">


                </span>

                    <div class="modal-footer">
                        <button class="btn btn-success moreCustomer" type="button">More Customer</button>
                    </div>

                </div>


                <div class="form-group">
                    <label class="control-label col-md-2">Plot</label>
                    <div class="col-md-4">
                        <select class="form-control selectpicker" @change="PlotChanged" v-model="plot" id="plot"
                                data-live-search="true" data-size="15">
                            <option value="">Select a plot</option>
                            @foreach($plots as $plot)
                                @if($plot->available())
                                    <option value="{{$plot->id}}"
                                            data-unit-price="{{$plot->price}}"
                                            data-size_id="{{$plot->size->id}}"
                                            data-pricing="{{json_encode($plot->getPricing())}}"
                                            data-plot="{{json_encode($plot->plotToArray())}}"
                                            data-plot_no="{{$plot->plot_no}}">
                                        <small>{{$plot->product->name . ' ' . $plot->size->label }}
                                            (No:// {{$plot->plot_no}})
                                        </small>
                                    </option>
                                @endif
                            @endforeach
                        </select>
                    </div>

                    <label class="control-label col-md-2">Plot No</label>
                    <div class="col-md-4">
                        <input type="text" readonly="readonly" class="form-control" id="plot_no">
                    </div>


                </div>

                <div class="form-group">
                    <label class="col-md-2 control-label">Agent</label>
                    <div class="col-md-4">
                        <select name="agent" class="form-control selectpicker" data-live-search="true" data-size="5">
                            @foreach($users as $user)
                                <option value="{{$user->id}}">{{$user->fullName()}}</option>
                            @endforeach
                        </select>
                    </div>

                    <label class="control-label col-md-2">Is Paying</label>
                    <div class="col-md-4">

                        <input type="checkbox" data-render="switchery" id="isPaying" v-model="isPaying"
                               @change="checkIsPaying" name="is_paying"
                               data-theme="default">

                    </div>

                </div>


                <div class="form-group">
                    <label class="col-md-2 control-label">Payment Option</label>

                    <div class="col-ld-6">
                        <input type="radio" name="paymentOptionChecker" value="cash" v-model="paymentOption"> Cash
                        Option
                        <input type="radio" name="paymentOptionChecker" value="installment" v-model="paymentOption">
                        Installment Option
                    </div>

                </div>


                <div class="modal-footer">
                    <button class="btn btn-primary AddMoreProduct " @click.prevent="selectPlot()" type="button">Select Plot</button>
                </div>

                <table class="table-bordered table-striped table" id="addSale" width="100%">
                    <thead>
                    <tr>
                        <th>Plot</th>
                        <th>Plot No</th>
                        <th>Unit Price</th>
                        <th>Sub Total</th>
                        <th><i class="fa fa-trash-o"></i></th>
                    </tr>
                    </thead>
                    <tbody class="product-list"></tbody>
                    <tfoot>
                    <tr>
                        <td colspan="3"><strong>Total</strong></td>
                        <td><span id="total"><b></b></span></td>
                    </tr>
                    </tfoot>
                </table>


                <input type="hidden" name="total" class="gtotal">

                <div class="modal-footer">
                    <button type="button" class="btn btn-primary addPayment" style="display: none" data-toggle="modal"
                            data-target="#payment">Add Payment
                    </button>
                </div>

                @component('layouts.partials.modal')
                    @slot('id','payment')
                    @slot('dialogClass','modal-lg')
                    @slot('title') <h6>Select payment method and complete the sale</h6> @endslot


                    <div>
                        <div class="form-group">


                            <div class="">
                                <label class="control-label col-md-2">Payment Type</label>
                                <div class="col-md-4">
                                    <select name="payment_type" class="form-control selectpicker" id="payment_type"
                                            data-live-search="true"
                                            data-size="5">
                                        <option value="full">Full Payment</option>
                                        <option value="installment">Installment</option>
                                    </select>
                                </div>

                            </div>

                            <label class="control-label col-md-2">Payment Method</label>
                            <div class="col-md-4">
                                {{--  <select name="payment_method"  id="method" class="form-control selectpicker" data-live-search="true"
                                          data-size="5">--}}
                                @foreach(\Codex\Classes\Helper::system()->paymentMethods() as $paymentMethod)

                                    <input
                                            type="radio"
                                           name="payment_method"
                                            class="method"
                                            v-model="paymentMethod"
                                           value="{{$paymentMethod}}">{{$paymentMethod}}
                                    {{--  <option value="{{$paymentMethod}}">{{$paymentMethod}}</option>--}}

                                    @endforeach
                                    </select>
                            </div>

                        </div>

                        <div class="form-group">

                            <label class="control-label col-md-2">Amount Paid</label>
                            <div class="col-md-4">
                                <input type="text" name="amount" v-model="paidAmount" class="form-control gtotal" id="paid">
                            </div>


                            <label class="control-label col-md-2">Deposit Date</label>
                            <div class="col-md-4">
                                <input type="text" name="deposit_date" class="form-control datepicker" id="">
                            </div>


                        </div>

                        <div class="form-group">


                                <div style="display: none" class="next_date">

                                    <div class="hidden">

                                        <label class="control-label col-md-4">Balance</label>
                                        <div class="col-md-4">
                                            <input type="text" name="balance" class="form-control" id="balance">
                                        </div>

                                    </div>

                                <label class="control-label col-md-2">Months</label>
                                <div class="col-md-2">
                                    <input type="text" name="months" class="form-control" value="6" id="month">
                                </div>

                            </div>
                        </div>



                        <div class="mpesa">
                            <div class="form-group">

                                <label class="control-label col-md-2">Reference code <span
                                            class="required-uzapoint">*</span></label>
                                <div class="col-md-4">
                                    <input type="text" v-model="referenceCode" name="mpesa_reference_code" class="form-control">
                                </div>
                            </div>
                        </div>


                        <div class="bank" style="display: none">

                            <div class="form-group">

                                <label class="control-label col-md-2">Bank Name <span class="required-uzapoint">*</span>
                                </label>
                                <div class="col-md-4">
                                    <input type="text" name="bank_name" value="{{config('config.equity_bank.name')}}"
                                           class="form-control">
                                </div>

                                <label class="control-label col-md-2">Account No <span
                                            class="required-uzapoint">*</span></label>
                                <div class="col-md-4">
                                    <input type="text" name="account_no" value="{{config('config.equity_bank.number')}}"
                                           class="form-control">
                                </div>
                            </div>
                            <div class="form-group">


                                <label class="control-label col-md-2">Slip NO <span
                                            class="required-uzapoint">*</span></label>
                                <div class="col-md-4">
                                    <input type="text" name="slip_no" class="form-control">
                                </div>

                            </div>
                        </div>

                        <div class="cheque" style="display: none">

                            <div class="form-group">
                                <label class="control-label col-md-2">Bank Name <span class="required-uzapoint">*</span></label>
                                <div class="col-md-4">
                                    <input type="text" name="check_bank_name" class="form-control">
                                </div>

                                <label class="control-label col-md-2">Cheque No <span class="required-uzapoint">*</span></label>
                                <div class="col-md-4">
                                    <input type="text" name="cheque_no" class="form-control">
                                </div>


                                <label class="control-label col-md-2">Drawer <span
                                            class="required-uzapoint">*</span></label>
                                <div class="col-md-4">
                                    <input type="text" name="drawer" class="form-control">
                                </div>


                            </div>


                        </div>

                        <div class="receipts" style="display: none">
                            <div class="form-group">
                                <label class="control-label col-md-2">Receipt <span
                                            class="required-uzapoint">*</span></label>
                                <div class="col-md-4">
                                    <input type="file" name="receipts" class="form-control">
                                </div>
                            </div>
                        </div>


                        <fieldset>
                            <legend>Next Of kin</legend>

                            <div class="form-group">

                                <label class="col-md-2 control-label">Full Name</label>
                                <div class="col-md-4">
                                    <input type="text" name="next_of_kin_name" class="form-control">
                                </div>


                                <label class="col-md-2 control-label">Phone Number</label>
                                <div class="col-md-4">
                                    <input type="text" name="next_of_kin_phone_number" class="form-control">
                                </div>
                            </div>
                            <div class="form-group">

                                <label class="col-md-2 control-label">Relation</label>
                                <div class="col-md-4">
                                    <input type="text" name="next_of_kin_relation" class="form-control">
                                </div>
                            </div>

                        </fieldset>


                        <div class="form-group">
                            <label class="control-label col-md-2">Has Witnesses</label>
                            <div class="col-md-4">
                                <input type="checkbox" data-render="switchery" id="hasWitness" name="has_witness"
                                       data-theme="default">
                            </div>
                        </div>

                        <div class="add-witness" style="display: none">

                    <span style="text-align: center; ">
                        <b>IF YOU HAVE WITNESS ADD THEM HERE
                        </b>
                    </span>

                            <div class="witnessDiv">

                                <div class="form-group">
                                    <label class="control-label col-md-2">Name</label>
                                    <div class="col-md-3">
                                        <input type="text" name="witness_name[]" class="form-control">
                                    </div>

                                    <label class="control-label col-md-2">Type</label>
                                    <div class="col-md-3">
                                        <select name="type[]" class="form-control selectpicker" data-live-serach="true">
                                            <option value="witness">Witness</option>
                                            <option value="advocate">Advocate</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">

                                    <label class="control-label col-md-2">ID NO:/</label>
                                    <div class="col-md-3">
                                        <input type="text" name="witness_id[]" class="form-control">
                                    </div>

                                    <div class="col-md-1">
                                        <button class="btn btn-primary addWintess" type="button">+</button>

                                    </div>

                                </div>

                            </div>
                        </div>


                        {{--<div class="form-group">
                            <label class="control-label col-md-2">Include Title Fee</label>
                            <div class="col-md-4">
                                <input data-render="switchery" type="checkbox" name="title_fee">
                            </div>
                        </div>--}}


                        <div class="form-group">
                            <label class="control-label col-md-2">Other Details</label>
                            <div class="col-md-4">
                                <input data-render="switchery" type="checkbox" id="otherDetails" class="otherDetails"
                                       name="other_details">
                            </div>
                        </div>

                        <div class="add-notes" style="display: none">

                    <span style="text-align: center; ">
                        <b>Add Customer Note Here</b>
                    </span>

                            <div class="addCustomerNote">

                                <div class="form-group">
                                    <label class="control-label col-md-2">Note</label>
                                    <div class="col-md-8">
                                        <textarea name="customer_payment_note[]" class="form-control" rows="10"
                                                  cols="20"></textarea>
                                    </div>

                                </div>

                            </div>
                        </div>
                    </div>




                    <div class="modal-footer">
                        <button type="button" class="btn submitSale btn-success">Complete Sale</button>
                    </div>
                @endcomponent


            </form>

        </div>

    @endcomponent


    @component('layouts.partials.modal')

        @slot('dialogClass','modal-lg')
        @slot('id','details')
        @slot('title') <h6>Sale Details</h6>@endslot


        <div class="sale_details"></div>

    @endcomponent



@endsection


@section('extra_js_footer')

    <script>
        const vue = new Vue({
            el: '#app',
            data() {
                return {
                    isPaying: true,
                    paymentOption: 'installment',
                    plot: {
                        approximate: '',
                        details: '',
                        id: '',
                        plotNo: '',
                        landLocation: '',
                        price: Number,
                        sellingPrice: Number,
                        project_code: '',
                        size: '',
                    },
                    pricing: {
                        amount: '',
                        end_at: '',
                        payment_option: '',
                        size_at: '',
                        status: '',
                    },
                    subTotal: 0,
                    total: 0,
                    paidAmount: 0,
                    paymentMethod: 'Mpesa',
                    referenceCode: 'Mpesa',
                }
            },
            methods: {
                checkIsPaying() {

                    if (this.isPaying === false)
                    {
                        this.paidAmount = 0;
                    }
                    else {
                        if (this.isPaying === true)
                        {
                            this.paidAmount =  $("span#total").text();
                        }
                    }


                },
                PlotChanged() {

                    console.log(this.plot)


                },

                selectPlot() {


                    this.pricing = $('select#plot option:selected').data('pricing');

                    this.plot = $('select#plot option:selected').data('plot');


                    var customerId = $('#customer').val();

                    var sizeId = this.plot.size_id;
                    var unitPrice = this.plot.sellingPrice;

                    console.log(unitPrice, this.plot)

                    this.pricing.forEach((price, index) => {

                        if (price.payment_option == this.paymentOption && price.size_id == this.plot.size_id) {

                            unitPrice = ( price.amount);

                            console.log('closer to cash');

                        }
                        if (price.size_id == sizeId && this.paymentOption == 'cash' && price.status === true) {

                            unitPrice =  ( price.amount);


                        }

                        this.subTotal = unitPrice

                    });


                    console.log(unitPrice)
                    var htmlObject = "<tr class='myRow[]' id='" + customerId + this.plot.id + "'>" +

                        "<td><input type='hidden' name='plot_id[]' value='" + this.plot.id + "'>" + this.plot.details + "</td>" +
                        "<td><input type='text' class='col-md-4' name='plot_no[]' readonly value='" + this.plot.plotNo + "'></td>" +
                        "<td><input type='text'  id='price" + customerId + this.plot.id + "' class='col-md-12 form-control item_price' name='price[]' value='" + unitPrice + "'></td>" +
                        "<td>" +
                        "<span id='subtotal" + customerId + this.plot.id + "'  class='subtotal'>"+this.subTotal+"</td>" +
                        "<td><button class='btn btn-danger removeThis' @click='removeItemPlot(unitPrice)' type='button'> &times;</td>" +
                        "</tr>";

                    var i = 1;

                    var myTableRow = $("table#addSale>tbody>tr#" + customerId + this.plot.id);

                    if (myTableRow.length) {


                        return false;
                    } else {
                        $("#addSale>tbody").append(htmlObject);

                        $(".addPayment").show();

                    }

                    if (this.isPaying)
                    {
                        this.paidAmount += this.subTotal

                        console.log(this.paidAmount ,  $("input.gtotal").val() ,'total');

                    }



                },
                removeItemPlot(unitPrice){

                    this.total -= unitPrice;
                },
                compute_total_price () {

                    var total_price = 0;
                    $('tr[class="myRow[]"]').each(function () {
                        var id = ($(this)[0]['id']);
                        var q = 1; ///from an input you can edit
                        var p = $("#price" + id).val();
                        console.info('price' + p + 'was');
                        if (!p) {
                            var pr = $("#price" + id).html();
                            p = pr;
                        }


                        $("#subtotal" + id).html(p * q);
                        total_price = total_price + p * q;

                    });


                    var sum_sub_total = $("table#addSale>tbody>tr").find('span.subtotal');
                    var su = 0;

                    console.info('subtotal is ' + sum_sub_total);
                    $.each(sum_sub_total, function () {

                        su = su + parseInt($(this).html());
                        //su +=total_price;
                    });
                    $("#total").html(su);
                    $(".gtotal").val(su);
                    $("#amountToPay").val(su);

                    if (this.isPaying)
                    {
                        this.paidAmount = su
                    }
                    this.total = su

                }
            }
        })
    </script>


    @include('layouts.dataTables.datatable')
    <script src="https://cdn.ckeditor.com/4.9.2/standard/ckeditor.js"></script>
    @include('layouts.datePicker.datepicker')
    @include('layouts.form')
    <script>


        var customerHtml = '<div class="moreCustomerDetails"  style="border-top: solid"> <div class="form-group">' +
            ' <label class="control-label col-md-2">First Name <span class="required-uzapoint">*</span> ' +
            '</label>' +
            ' <div class="col-md-4"> ' +
            '<input type="text" class="form-control" name="first_name[]"> </div> ' +
            '<label class="control-label col-md-2">Last Name <span class="required-uzapoint">*</span>' +
            ' </label> <div class="col-md-4">' +
            ' <input type="text" class="form-control" name="last_name[]" placeholder="500000"> </div> ' +
            '</div> <div class="form-group"> ' +
            '<label class="control-label col-md-2">Middle Name <span class="required-uzapoint">*</span> ' +
            '</label> <div class="col-md-4"> ' +
            '<input type="text" class="form-control" name="middle_name[]" value="."> </div>' +
            ' <label class="control-label col-md-2">Id No <span class="required-uzapoint">*</span>' +
            ' </label> <div class="col-md-4"> ' +
            '<input type="text" class="form-control" name="id_no[]" placeholder="12345678"> </div> ' +
            '</div> <div class="form-group">' +
            ' <label class="control-label col-md-2">Email <span class="required-uzapoint">*</span> ' +
            '</label> <div class="col-md-4"> ' +
            '<input type="text" class="form-control" name="email[]"> ' +
            '</div> <label class="control-label col-md-2">Phone number <span class="required-uzapoint">*</span> ' +
            '</label> <div class="col-md-4"> ' +
            '<input type="text" class="form-control phoneNumber" name="phone_number[]"> </div>' +
            ' </div><div class="modal-footer"> ' +
            '<button class="btn btn-danger removeCustomer" type="button">remove Customer</button> ' +
            '</div></div>';


        $(document).on('click', 'button.moreCustomer', function () {

            $(".addCustomerHtml").append(customerHtml)
        });

        $(document).on('click', 'button.removeCustomer', function () {

            $(this).closest('div.moreCustomerDetails').remove()
        });


        FormSliderSwitcher.init();


        var witnessHtml = '<div class="witnessDiv">' +
            ' <div class="form-group">' +
            ' <label class="control-label col-md-2">Name</label> ' +
            '<div class="col-md-3"> ' +
            '<input type="text" name="witness_name[]" class="form-control">' +
            ' </div> <label class="control-label col-md-2">Type</label>' +
            '<div class="col-md-3">' +
            '<select name="type[]" class="form-control selectpicker" data-live-serach="true">' +
            '<option value="witness">Witness</option>' +
            '<option value="advocate">Advocate</option>' +
            '</select>' +
            '</div></div><div class="form-group"> ' +
            '<label class="control-label col-md-2">ID NO:/</label> ' +
            '<div class="col-md-3">' +
            ' <input type="text" name="witness_id[]" class="form-control"> ' +
            '</div>' +
            ' <div class="col-md-1"> ' +
            '<button class="btn btn-primary addWintess" type="button">+</button> ' +
            '</div> <div class="col-md-1">' +
            ' <button class="btn btn-danger removeWitness" type="button">-</button>' +
            ' </div>' +
            ' </div>' +
            ' </div>';


        $(document).on('click', 'button.addWintess', function () {


            $(".add-witness").append(witnessHtml);


        });
        $(document).on('click', 'button.removeWitness', function () {

            $(this).closest('div.witnessDiv').remove();


        });

        //new customer
        var new_customer = function () {
            return $('#new_customer').is(":checked");
        };
        //
        if (!new_customer()) {
            $(".display_new_customer").slideUp();
        }
        $("#new_customer").on('change', function () {

            if (!new_customer()) {
                $(".display_new_customer").slideUp();
                $("div.hide-if-new").removeClass('hidden')
            } else {
                $(".display_new_customer").slideDown();
                $("div.hide-if-new").addClass('hidden')
            }
        });


        $(document).on('change', 'select#selected-customer', function () {

            $("span.append-here").empty();

            let customerDetails = "Name:   " + $("select#selected-customer option:selected").data('fullname');
            customerDetails += "<br/>I.D:   " + $("select#selected-customer option:selected").data('id_no');
            customerDetails += "<br/>Phone Number:   " + $("select#selected-customer option:selected").data('phone_number');
            customerDetails += "<br/>E-MAIL:  " + $("select#selected-customer option:selected").data('email');
            $("span.append-here").append(customerDetails)
        });

        //new witness

        var witness = function () {
            return $('#hasWitness').is(":checked");
        };
        //
        if (!witness()) {
            $(".add-witness").slideUp();
        }
        $("#hasWitness").on('change', function () {

            if (!witness()) {
                $(".add-witness").slideUp();
            } else {
                $(".add-witness").slideDown();
            }
        });


        var PaymentNoteHmtl = '<div class="addCustomerNote"> ' +
            '<div class="form-group"> ' +
            '<label class="control-label col-md-2">Note</label> ' +
            '<div class="col-md-8">' +
            ' <textarea name="customer_payment_note[]" class="form-control" rows="10" cols="20"></textarea> ' +
            '</div> ' +
            '<div class="col-md-1"> ' +
            '<button class="btn btn-primary addCustomerDetails" type="button">+</button>' +
            ' </div> ' +
            '<div class="col-md-1"> ' +
            '<button class="btn btn-danger removeDetails" type="button">-</button>' +
            ' </div> ' +
            '</div>' +
            ' </div>';


        //new payment Note

        var paymentNote = function () {
            return $('#otherDetails').is(":checked");
        };
        //
        if (!paymentNote()) {
            $(".add-notes").slideUp();
        }
        $("#otherDetails").on('change', function () {

            if (!paymentNote()) {
                $(".add-notes").slideUp();
            } else {
                $(".add-notes").slideDown();
            }
        });


        $(document).on('click', 'button.addCustomerDetails', function () {


            $("div.addCustomerNote").append(PaymentNoteHmtl);


        });
        $(document).on('click', 'button.removeDetails', function () {

            $(this).closest('div.addCustomerNote').remove();


        });


        (function () {

            $('#plot_no').val($("select#plot option:selected").attr('data-plot_no'));


            $("#paid").on('keyup', function () {


                var amount = $(".gtotal").val();

                var paid = $(this).val();

                var balance = Math.ceil((numeral(amount).value()) - (numeral(paid).value()));

                $("#balance").val(balance);


                var installmenets = ((balance) / (numeral($("#month").val()).value()));

                $("#m_installment").val(Math.ceil(installmenets))


            });


            $("#month").on('keyup', function () {


                var balance = numeral($("#balance").val()).value();

                var installmenets = ((balance) / (numeral($(this).val()).value()));

                $("#m_installment").val(Math.ceil(installmenets));


                var today = new Date();

                var completion = today.getMonth() + numeral($(this).val()).value();


            })


        })();


        $(document).on('change', '#plot', function () {

            $('#plot_no').val($("select#plot option:selected").attr('data-plot_no'))

        });

        /*$('.datepicker').datepicker({
            date:new Date()
        });*/


        var dateToday = new Date();
        $(".datepicker").datepicker({
            maxDate: dateToday

        });

        $(document).on('change', '#payment_type', function () {


            if ($(this).val('full')) {
                $(".next_date").slideUp();
            }
            if ($(this).val('installment')) {

                $(".next_date").slideDown();
                $("#balance").val((numeral($("#paid").val()).value()));


                var balance = numeral($("#balance").val()).value();

                var installmenets = ((balance) / (numeral($("#month").val()).value()));

                $("#m_installment").val(Math.ceil(installmenets));


                var today = new Date();


                var completion = today.getMonth() + numeral($("#month").val()).value();


                $("#complection").datepicker({

                    maxDate: "+7m "
                })


            }


        });


        $(document).on('click', 'button.AddMoreProduct', function () {

            //$("#addSale>tbody").html('')


            compute_total_price();


        });


        var compute_total_price = function () {
            var total_price = 0;
            $('tr[class="myRow[]"]').each(function () {
                var id = ($(this)[0]['id']);
                var q = 1; ///from an input you can edit
                var p = $("#price" + id).val();
                console.info('price' + p + 'was');
                if (!p) {
                    var pr = $("#price" + id).html();
                    p = pr;
                }


                $("#subtotal" + id).html(p * q);
                total_price = total_price + p * q;

            });


            var sum_sub_total = $("table#addSale>tbody>tr").find('span.subtotal');
            var su = 0;

            console.info('subtotal is ' + sum_sub_total);
            $.each(sum_sub_total, function () {

                su = su + parseInt($(this).html());
                //su +=total_price;
            });
            $("#total").html(su);
            $(".gtotal").val(su);
            $("#amountToPay").val(su);
//                $("#balance").html(su);
            //calculate_balance();
            console.info('total_price is' + su)

        };

        $("#data-table").DataTable({
            sort: false

        });


        $(document).on('click', '#detail', function () {


            $('.sale_details').html('');
            var saleItems = JSON.parse($('a#detail').attr('data-saleItems'));
            var table = '<table class="table-bordered table-striped table" width="100%"> ' +
                '<thead>' +
                '<tr>' +
                '<th>Plot No</th>' +
                '<th>Plot Name</th>' +
                '<th>Size</th>' +
                '<th>Plot Price</th>' +
                '</tr>' +
                '</thead>' +
                '<tbody>' +
                '<tr>' +
                '<td>' + saleItems.plot_no + '</td>' +
                '<td>' + saleItems.plot.product.name + '</td>' +
                '<td>' + saleItems.plot.size.label + '</td>' +
                '<td>' + saleItems.price + '</td>' +
                '</tr>' +
                '</tbody>' +
                '</table>';
            $('.sale_details').html(table)


        });


        $(document).on('change', '.method', function () {


            if ($(this).val() === 'Mpesa') {
                $(".mpesa").show();
                $(".bank").hide();
                $(".cheque").hide();
                $(".receipts").hide();
            }
            if ($(this).val() === 'Bank') {
                $(".mpesa").hide();
                $(".bank").show();
                $(".cheque").hide();
                $(".receipts").show();
            }
            if ($(this).val() === 'Cheque') {
                $(".mpesa").hide();
                $(".bank").hide();
                $(".cheque").show();
                $(".receipts").show();
            }
            if ($(this).val() === 'Cash') {
                $(".mpesa").hide();
                $(".bank").hide();
                $(".cheque").hide();
                $(".receipts").hide();
            }
        });

        $(document).on('click', 'button.removeThis', function () {

            $(this).closest('tr').remove();
            compute_total_price();

        });


        $(document).on('keyup', '.item_price', function () {
            compute_total_price();
        });


        $(document).on('click', 'button.submitSale', function () {

            $(this).attr('disabled', true).text('submitting .....');


            let formData = new FormData($("form.submitNewSale")[0]);

            axios.post('/sale/manage/store', formData)
                .then((res) => {

                    $(this).attr('disabled', false).text('Submit');

                    $("div#payment").modal('hide');

                   // $("form.submitNewSale").trigger('reset');

                    $.growl.notice({
                        message: res.data.message
                    })
                    // window.location.reload(true)
                }).catch((err) => {

                $(this).attr('disabled', false).text('submit');
                if (err.response.status == 422) {

                    $.each(err.response.data.message, (value, messages) => {

                        $.growl.warning({
                            message: messages,
                        })
                    });

                    return;
                }

                $.growl.warning({
                    message: err.response.data.message,
                })
            })


        });


    </script>


@endsection
