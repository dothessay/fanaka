@extends('layouts.master')

@section('title','edit project')

@section('content')

@component('layouts.partials.panel')
    <h2 class="text-justify">{!! $payment->sale->getCustomerHref() !!}</h2>

    <form
            class="form-horizontal form-bordered"
            method="post"
            action="{{route('sale_payments_update' ,['payment' => $payment->id ,'customer' => $customer->id])}}"
    >
        {{csrf_field()}}
        {{method_field('PATCH')}}
        <div class="form-group">
            <label for="" class="control-label col-md-2">Amount</label>
            <div class="col-md-4">
                <input
                        type="number"
                        class="form-control"
                        name="amount"
                        value="{{$payment->amount}}"
                >
            </div>
            <label for="" class="control-label col-md-2">Payment Method</label>
            <div class="col-md-4">
                <select
                        class="form-control"
                        name="payment_method"
                >
                    @foreach(\Codex\Classes\Helper::system()->paymentMethods() as $paymentMethod)
                        <option
                        @if ($payment->payment_method  === $paymentMethod)
                            selected
                        @endif
                        value="{{$paymentMethod}}">
                            {{$paymentMethod}}
                        </option>
                    @endforeach
                </select>
            </div>
        </div>

        <div class="form-group">
            <label for="" class="control-label col-md-2">Referenced Code</label>
            <div class="col-md-4">
                <input
                        type="text"
                        name="reference_code"
                        class="form-control"
                        value="{{$payment->reference_code}}"
                >
            </div>

            <label for="" class="control-label col-md-2">Deposit Date</label>
            <div class="col-md-4">
                <input
                        type="text"
                        name="deposit_date"
                        class="form-control date"
                        @if (isset($payment->deposit_date) && ! is_null($payment->deposit_date))
                        value="{{$payment->deposit_date}}"
                        @else
                        value="{{now()}}"
                        @endif

                >
            </div>
        </div>

        <div class="form-group">
            <button class="btn btn-success">Update</button>
        </div>
    </form>

@endcomponent


@endsection

@section('extra_js_footer')

    @include('layouts.datePicker.datepicker')
    <script>
        $('.date').datepicker();
    </script>


@stop
