@extends('layouts.master')
@section('title','Manage Payments')
@section('content')

    @component('layouts.partials.panel')


        <form>
            <div class="form-group">
                <label class="col-md-2">Customer</label>
                <div class="col-md-4">
                    <select class="form-control selectpicker" data-live-search="true" name="customer_id">
                        @foreach($customers as $customer)
                            <option
                                    value="{{$customer->id}}"
                                    @if( isset($_GET['customer_id']) && $_GET['customer_id'] == $customer->id ) selected @endif
                            >{{$customer->fullname()}}</option>
                        @endforeach
                    </select>
                </div>
                <button class="btn btn-success btn-outline-light">Get</button>
            </div>
        </form>


        @include('sales.payment._data')
    @endcomponent



@endsection

@section('extra_js_footer')
    @include('layouts.form')
 @endsection
