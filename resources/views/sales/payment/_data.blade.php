@foreach($payments as $key =>  $paymentGroups)
    <h2>Project: {{$key}}</h2>
    <table class="table table-striped table-bordered table-responsive">
        <thead>
        <tr>
            <th>Date</th>
            <th>Amount</th>
            <th>Payment Method</th>
            <th>Reference Code</th>
            <th>Date Deposited</th>
            <th>Customer</th>
           @can('access-module-component-functionality','account_list_update')
                <th>Actions</th>
            @endcan
        </tr>
        </thead>
        <tbody>
        @foreach($paymentGroups as  $payment)
            <tr>
                <td>{{$payment['date']}}</td>
                <td>{{ number_format( floatval($payment['amount']) , 2) }}</td>
                <td>{{$payment['payment_method']}}</td>
                <td>{{$payment['reference_code']}}</td>
                <td>{!! $payment['deposit_date'] !!}</td>
                <td>{!! $payment['customer'] !!}</td>
                @can('access-module-component-functionality','account_list_update')
                    <td>
                        @component('layouts.button')
                            <li><a href="{{route('sale_payments_edit' , ['payment' => $payment['id'] ,'customer' => $_GET['customer_id']])}}">Update</a> </li>
                            <li><a href="{{route('sale_payments_delete', ['payment' => $payment['id']])}}">Delete</a> </li>
                        @endcomponent
                    </td>
                @endcan
            </tr>
        @endforeach
        </tbody>
    </table>
@endforeach
