@extends('layouts.master')
@section('title','List Sales')
@section('content')


   {{-- @can('access-module-component-functionality', 'sale_manage_add')
        <ul class="nav nav-pills">
            <li class="active">
                <a --}}{{--data-toggle="modal" data-target="#addSale"--}}{{-- href="{{route('sale_manage_add')}}">Add Sale</a>
            </li>
        </ul>
    @endcan--}}



    <ul class="nav nav-tabs tabs-uzapoint">

        <?php $active = true ? "active " : ""; ?>

        @can('access-module-component-functionality','sale_manage_list')
            <li class="{{$active}}">
                <a href="#default-tab-1" data-toggle="tab">
                    <span class="">List Sales</span>
                </a>
            </li>

            <?php $active = false; ?>
        @endcan


        @can('access-module-component-functionality','sale_manage_new-sale')

            <li class="{{$active}}">
                <a href="#default-tab-2" data-toggle="tab">
                    <span class="">Pending Approvals <i class="badge badge-danger">{{$approved->count()}}</i> </span>
                </a>
            </li>

            <?php $active = false; ?>

        @endcan
        @can('access-module-component-functionality','sale_manage_agreement-request')

            <li class="{{$active}}">
                <a href="#default-tab-3" data-toggle="tab">
                    <span class="">Request Agreement <i class="badge badge-danger">{{$agreementRequests->count()}}</i></span>
                </a>
            </li>

            <?php $active = false; ?>

        @endcan

        @can('access-module-component-functionality','sale_manage_payment-request')

            <li class="{{$active}}">
                <a href="#default-tab-4" data-toggle="tab">
                    <span class="">Payment Requests <i class="badge badge-danger">{{$paymentRequests->count()}} </i></span>
                </a>
            </li>

            <?php $active = false; ?>
        @endcan

    </ul>


    <div class="tab-content">
        <?php $active = true ? "active " : ""; ?>

        @can('access-module-component-functionality','sale_manage_list')
            <div class="tab-pane fade {{$active}} in" id="default-tab-1">

                @include('sales._sale_list')
            </div>
            <?php $active = false?>
        @endcan

            @can('access-module-component-functionality','sale_manage_new-sale')
            <div class="tab-pane fade {{$active}} in" id="default-tab-2">

                @include('sales._new_sale_list')
            </div>

                <?php $active = false?>
            @endcan
            @can('access-module-component-functionality','sale_manage_agreement-request')
            <div class="tab-pane fade {{$active}} in" id="default-tab-3">

                @include('sales._agreement_request')
            </div>


                <?php $active = false?>
            @endcan

            @can('access-module-component-functionality','sale_manage_payment-request')
            <div class="tab-pane fade {{$active}} in" id="default-tab-4">

                @include('sales._payment_request')
            </div>

                <?php $active = false?>
            @endcan
    </div>


    @component('layouts.partials.modal')

        @slot('dialogClass','modal-lg')
        @slot('id','details')
        @slot('title') <h6>Sale Details</h6>@endslot


        <div class="sale_details"></div>

    @endcomponent






    @component('layouts.partials.modal')

        @slot('dialogClass','modal-lg')
        @slot('id','UploadScannedAgreement')
        @slot('title') <h6>Upload Scanned Agreement for this Sale <span></span></h6>@endslot

        <form method="post" class="form-horizontal" action="{{route('sale_manage_upload-scanned-agreement')}}"
              enctype="multipart/form-data">
            {{csrf_field()}}
            <input type="hidden" name="id" id="idUploadAgreement">

            <div class="form-group">
                <label class="control-label col-md-2">Upload Agreement</label>
                <div class="col-md-3">
                    <input name="agreement[]" type="file" class="document-file">

                </div>


                <label class="control-label col-md-1">Upload Type</label>
                <div class="col-md-3">

                    <select name="upload_type[]" class="form-control selectpicker">

                        <option>Agreement</option>
                        <option>Receipt</option>
                        <option>Title</option>
                    </select>

                </div>
                <div class="col-md-1">
                    <button type="button" class="btn btn-primary add-more-files"><i class="fa fa-plus"></i>
                    </button>

                </div>
            </div>

            <div class="multipleFiles">

            </div>

            <div class="modal-footer">
                <button type="submit" class="btn btn-success">Upload</button>
            </div>

        </form>


    @endcomponent
    @component('layouts.partials.modal')

        @slot('dialogClass','modal-lg')
        @slot('id','witness')
        @slot('title') <h6><span> Add Witness to this sale</span></h6>@endslot

        <form method="post" class="form-horizontal" action="{{route('sale_manage_store-witness')}}"
              enctype="multipart/form-data">
            {{csrf_field()}}

            <div class="witnessDiv">
                <input type="hidden" name="sale_id" class="form-control" id="witness_sale_id">

                <div class="form-group">
                    <label class="control-label col-md-2">Name</label>
                    <div class="col-md-3">
                        <input type="text" name="witness_name[]" class="form-control">
                    </div>

                    <label class="control-label col-md-2">Type</label>
                    <div class="col-md-3">
                        <select name="type[]" class="form-control selectpicker" data-live-serach="true">
                            <option value="witness">Witness</option>
                            <option value="advocate">Advocate</option>
                        </select>
                    </div>
                </div>
                <div class="form-group">

                    <label class="control-label col-md-2">ID NO:/</label>
                    <div class="col-md-3">
                        <input type="text" name="witness_id[]" class="form-control">
                    </div>

                </div>

            </div>

            <div class="modal-footer">
                <button type="submit" class="btn btn-success">Submit</button>
            </div>

        </form>


    @endcomponent


@endsection


@section('extra_js_footer')
    @include('layouts.dataTables.datatable')
@include('layouts.form')
    @include('layouts.datePicker.datepicker')
    <script>


        (function () {

            $('#plot_no').val($("select#plot option:selected").attr('data-plot_no'))

            $("select.selectCustomer").selectpicker('refresh')

        })();


        $(document).on('click', 'a#addWitness', function () {


            var witnesses = ($(this).data('witness'));

            $("#witness_sale_id").val($(this).data('sale_id'))
            $("#witness_customer_id").val($(this).data('customer_id'))

            var tbody = '';

            var url = "{{url('sale/manage/remove-witness/')}}";


            for (var i = 0; i < witnesses.length; i++) {

                tbody += '<tr>' +
                    '<td>' + witnesses[i].name + '</td>' +
                    '<td>' + witnesses[i].type + '</td>' +
                    '<td>' + witnesses[i].id_no + '</td>' +
                    '<td><a href="' + url + '/' + witnesses[i].id + '" class="btn btn-danger">Remove </td>' +
                    '</tr>'

            }

            $("#witness-table>tbody").append(tbody);
           // $("#witness-table").addClass('table-striped nowrap').DataTable({sort: false})




            var kins = ($(this).data('kins'));

            var removeKinUrl = "{{url('sale/manage/remove-kins/')}}";


            let kinsOptions = '';

            for (let k =0 ; k < kins.length ; k++){

                let customerKins = kins[k].customer.next_of_kins;


                console.log(customerKins , 'next of kins')

                for (let j =0 ; j< customerKins.length ; j++) {


                    kinsOptions += '<tr>' +
                        '<td>' + customerKins[j].full_name + '</td>' +
                        '<td>' + customerKins[j].phone_number + '</td>' +
                        '<td>' + customerKins[j].relation + '</td>' +
                        '<td><a href="' + removeKinUrl + '/' + customerKins[j].id + '" class="btn btn-danger">Remove </td>' +
                        '</tr>'

                   // $nextOfKinArray[] ="<br/><br/>Name: ". $nextOfKin->fullName() . "&nbsp;&nbsp;&nbsp;<br/>Phone Number  : &nbsp;".$nextOfKin->phone_number ."&nbsp;&nbsp;&nbsp; <br/>Relation :" .$nextOfKin->relation."</br>";
                }



            }

            console.log(kinsOptions , 'next of kins table details')

            $("#kins-table>tbody").append(kinsOptions);
            //$("#kins-table").addClass('table-striped nowrap').DataTable({sort: false})




        });


        var witnessHtml = '<tr>' +
            ' <td> ' +

            '<input type="text" name="witness_name[]" class="form-control">' +
            '</td> ' +
            '<td>' +

            '<select name="type[]" class="form-control selectpicker" data-live-serach="true"><option value="witness">Witness</option><option value="advocate">Advocate</option></select>' +
            '</td>' +
            '<td> ' +

            ' <input type="number" name="witness_id[]" class="form-control"> ' +
            '</td>' +
            '<td> ' +
            '<button class="btn btn-danger removeWitness" type="button">-</button>' +
            ' </td>' +
            '</tr>';


        $(document).on('click', 'button.add-witness', function () {


            $("#witness-table>tbody").append(witnessHtml);

            $("#witnessBtn").removeAttr('disabled');
        });


        $(document).on('click', 'button.removeWitness', function () {

            $(this).closest('tr').remove();


        });

        $(document).on('click', 'a#uploadAgreementNow', function () {

            $("#idUploadAgreement").val($(this).data('id'));

        });


        $(document).on('change', '#plot', function () {

            $('#plot_no').val($("select#plot option:selected").attr('data-plot_no'))

        })

        $('.datepicker').datepicker();

        $(document).on('change', '#payment_type', function () {


            if ($(this).val('full')) {
                $(".next_date").slideUp();
            }
            if ($(this).val('installment')) {

                $(".next_date").slideDown();
            }


        })


        $(document).on('click', 'button.AddMoreProduct', function () {

            $("#addSale>tbody").html('')


            var customerId = $('#customer').val();

            var customerFullName = $("select#customer option:selected").text();

            var plotId = $('#plot').val();
            var plotLabel = $("select#plot option:selected").text();
            var quantity = 1;
            var unitPrice = $("select#plot option:selected").attr('data-unit-price');

            var PlotNo = $("select#plot option:selected").attr('data-plot_no');


            var htmlObject = "<tr class='myRow[]' id='" + customerId + plotId + "'>" +
                "<td><input type='hidden' name='customer_id' value='" + customerId + "'>" + customerFullName + "</td>" +
                "<td><input type='hidden' name='plot_id' value='" + plotId + "'>" + plotLabel + "</td>" +
                "<td><input type='text' class='col-md-4' name='plot_no' readonly value='" + PlotNo + "'></td>" +
                "<td><input type='hidden' readonly='readonly' id='price" + customerId + plotId + "' class='col-md-12' name='price' value='" + unitPrice + "'>" + unitPrice + "</td>" +
                "<td>" +
                "<span id='subtotal" + customerId + plotId + "'  class='subtotal'>0.0</td>" +
                "</tr>";

            var i = 1;

            var myTableRow = $("table#addSale>tbody>tr#" + customerId + plotId);
            console.log('rows' + myTableRow)

            if (myTableRow.length) {


                return false;
            } else {
                $("#addSale>tbody").append(htmlObject);

                $(".addPayment").show();

            }
            compute_total_price();


        });


        var compute_total_price = function () {
            var total_price = 0;
            $('tr[class="myRow[]"]').each(function () {
                var id = ($(this)[0]['id']);
                var q = 1; ///from an input you can edit
                var p = $("#price" + id).val();
                console.info('price' + p + 'was')
                if (!p) {
                    var pr = $("#price" + id).html();
                    p = pr;
                }

                console.info('price' + p + 'is')


                $("#subtotal" + id).html(p * q);
                total_price = total_price + p * q;
                console.info(total_price)
            });


            var sum_sub_total = $("table#addSale>tbody>tr").find('span.subtotal');
            var su = 0;

            console.info('subtotal is ' + sum_sub_total);
            $.each(sum_sub_total, function () {

                su = su + parseInt($(this).html());
                //su +=total_price;
            });
            $("#total").html(su);
            $(".gtotal").val(su);
            $("#amountToPay").val(su);
//                $("#balance").html(su);
            //calculate_balance();
            console.info('total_price is' + su)

        };


        $(document).on('click', '#detail', function () {


            $('.sale_details').html('')
            var saleItems = JSON.parse($(this).attr('data-saleItems'));

            var tbodyItems = '';


            for (var i = 0; i < saleItems.length; i++) {

                tbodyItems += '<tr>' +
                    '<td>' + saleItems[i].plot_no + '</td>' +
                    '<td>' + saleItems[i].plot.product.name + '</td>' +
                    '<td>' + saleItems[i].plot.size.label + '</td>' +
                    '<td>' + saleItems[i].price + '</td>' +
                    '</tr>';

            }
            var table = '<table class="table-bordered table-striped table" width="100%"> ' +
                '<thead>' +
                '<tr>' +
                '<th>Plot No</th>' +
                '<th>Plot Name</th>' +
                '<th>Size</th>' +
                '<th>Plot Price</th>' +
                '</tr>' +
                '</thead>' +
                '<tbody>' + tbodyItems +
                '</tbody>' +
                '</table>';
            $('.sale_details').html(table)


        })

        $("#data-table").DataTable({
            sort: false,
            pagination: true,
            searching: false,
            paging: true,
            info: true

        });
        $("#payment-requests").DataTable({
            sort: false,
            pagination: true,
            searching: true,
            paging: true,
            info: true

        });
        $("#agreement-requests").DataTable({
            sort: false,
            pagination: true,
            searching: true,
            paging: true,
            info: true

        });
        $("table#new-sale-table").DataTable({
            sort: false,
            pagination: true,
            searching: true,
            paging: true,
            info: true

        });

        var moreFilesHtml = '<div class="append-files">' +
            '<div class="form-group">\n' +
            '                <label class="control-label col-md-2">Upload Agreement</label>\n' +
            '                <div class="col-md-3">\n' +
            '                <input name="agreement[]" type="file" class="document-file">\n' +
            '\n' +
            '                </div>' +
            '<label class="control-label col-md-1">Upload Type</label>' +
            ' <div class="col-md-3"> ' +
            '<select name="upload_type[]" class="form-control selectpicker"> ' +
            '<option>Agreement</option>' +
            ' <option>Receipt</option> ' +
            '<option>Title</option> ' +
            '</select> ' +
            '</div>\n' +
            '                <div class="col-md-1">\n' +
            '                <button type="button" class="btn btn-primary add-more-files"><i class="fa fa-plus"> </i> </button>\n' +
            '\n' +
            '                </div>\n' +
            '                <div class="col-md-1">\n' +
            '                <button type="button" class="btn btn-danger remove-files"><i class="fa fa-minus"></i> </button>\n' +
            '\n' +
            '                </div>\n' +
            '            </div></div>';

        $(document).on('click', 'button.add-more-files', function () {


            $("div.multipleFiles").append(moreFilesHtml)
        })

        $(document).on('click', 'button.remove-files', function () {


            $(this).closest('div.append-files').remove()
        })


        /**
         *
         * APPROVE PAYMENT
         */

        $(document).on('click','a.approve-payment-alert', function () {

            let id = $(this).data('id');

            swal({
                title: "Are you sure?",
                text: "Once you have approved this payment it cannot be undone!",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
                .then((willDelete) => {
                    if (willDelete) {

                        axios.get('/sale/manage/payment/confirm?id='+id).then((res) => {

                            swal(res.data.message, {
                                icon: "success",
                            });


                            window.location.reload(true)

                        }).catch((err) => {})

                    }
                });

        })


        /**
         *
         * Reject PAYMENT
         */

        $(document).on('click','a.reject-payment-alert', function () {

            let id = $(this).data('id');

            swal({
                title: "Are you sure?",
                text: "Once you have rejected this payment cannot be undone!",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
                .then((willDelete) => {
                    if (willDelete) {

                        axios.get('/sale/manage/payment/reject?id='+id).then((res) => {

                            swal(res.data.message, {
                                icon: "success",
                            });

                            window.location.reload(true)

                        }).catch((err) => {})

                    }
                });

        })



        $(document).on('click','a.update-payment-alert', function () {


            let myId = $(this).data('id')

            let amount = $("input#amount_"+myId).val();
            let reference = $("input#reference_"+myId).val();

            let formData = {
                id: myId,
                amount: amount,
                reference_code: reference,
                _token: "{{csrf_token()}}"
            }

            swal({
                title: "Are you sure?",
                text: "Note updating amount will alter the balance and calculation , confirm the figures before updating",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
                .then((willDelete) => {
                    if (willDelete) {

                        axios.post('/sale/manage/payment/update', formData).then((res) => {

                            swal(res.data.message, {
                                icon: "success",
                            });

                            window.location.reload(true)

                        }).catch((err) => {

                        })

                    }
                });




        })


        $("div.add-next-of-kin").hide();
        $("div.add-witness").hide();

        $(document).on('change','input.witnessNextOfKin', function () {

            if ($(this).val() === 'nextOfKin'){

                $("div.add-next-of-kin").show();
                $("div.add-witness").hide();

            }

            if ($(this).val() === 'witness'){

                $("div.add-next-of-kin").hide();
                $("div.add-witness").show();

            }

        })





    </script>
@endsection
