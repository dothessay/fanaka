@extends('layouts.master')
@section('extra_js_header')

@endsection
@section('title','Acknowledgment Note')
@section('content')
<form
        class="form-horizontal"
        method="post"
        action="{{ route('sale_manage_acknowledgement_print' ,['sale' =>  $sale->id ]) }}"
>
{{csrf_field()}}
    <input type="hidden" name="customer_id" value="{{ $customer->id }}">
    <textarea name="content" id="editor">{!! $acknowledgement !!}</textarea>
    <div class="email-footer">
        <button
                class="btn btn-success"
        > Print </button>
    </div>
</form>
@endsection
@section('extra_js_footer')

@endsection
