<section class="interest">
    <div class="table-responsive">
        <div class="col-md-12">
            <div class="row">
                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>Month</th>
                        <th>Amount</th>
                        <th>Percentage</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($interests as $key =>  $interest)
                        <tr>
                            <td>{{ $key +1  }} </td>
                            <td>{{ $interest->interest_date }} </td>
                            <td>{{ number_format(floatval($interest->amount) , 2) }} </td>
                            <td>{{ $interest->percentage }} </td>
                        </tr>
                    @endforeach
                    </tbody>
                    <tfoot>
                    <tr>
                        <td colspan="2">Totals</td>
                        <td> <b> {{ number_format(floatval($interests->sum('amount') ) , 2)}}</b></td>
                        <td> <b> {{ $interests->sum('percentage') }}</b></td>
                    </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>
</section>
