@extends('layouts.master')
@section('title')
    Interest Details  {{ $sale->getCustomerDetails()  }}     For project {{ $sale->getPropertyDetails() }}
    <br/>
@endsection

@section('content')

    <ul class="nav nav-pills">
        <li class="active">
            <a href="{{ url('sale/interest') }}"> Back</a>
        </li>
    </ul>

    @component('layouts.partials.panel')
        @slot('heading' , "Balance: ". config('app.currency'). " ". number_format(floatval( $sale->getBalance()) , 2) )
        @include('sales.interest._interest_details')
    @endcomponent
@endsection

