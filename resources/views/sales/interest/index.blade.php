@extends('layouts.master')
@section('title','Sale Interests')
@section('content')
    <form class="form-horizontal">
        <div class="form-group">
            <label class="control-label col-md-2">Start Date</label>
            <div class="col-md-3">
                <input
                        type="text"
                        name="start_date"
                        class="form-control date"
                        value="{{ old('start_date', request()->has('start_date') ? request('start_date') : '') }}"
                >
            </div>
            <label class="control-label col-md-2">End Date</label>
            <div class="col-md-3">
                <input
                        type="text"
                        name="end_date"
                        class="form-control date"
                        value="{{ old('end_date', request()->has('end_date') ? request('end_date') : '') }}"
                >
            </div>
        </div>
        <div class="email-footer">
            <button class="btn btn-success">GET</button>
        </div>
    </form>
    @component('layouts.partials.panel')
        <div class="table-responsive">
            <table class="table table-bordered table-striped" id="interest-date" width="100%">
                <thead>
                <tr>
                    <th>Sale Ref</th>
                    <th>Customer</th>
                    <th>project</th>
                    <th>Last Payment Date</th>
                    <th>Interest Date</th>
                    <th>Interest Amount</th>
                    <th>View</th>
                </tr>
                </thead>
                <tbody>
                @foreach($interests as $interest)
                    <tr>
                        <td width="5%"> <a href="{{ url('sale/interest/details/'. $interest['sale_id'] ) }}">{!!  $interest['sale_id'] !!}</a> </td>
                        <td width="10%"> {!!  $interest['customer'] !!} </td>
                        <td width="10%">{{ $interest['projects'] }}</td>
                        <td width="10%">{!! $interest['lastPaymentDate'] !!}</td>
                        <td width="10%">{!! $interest['totalMonths'] !!}</td>
                        <td width="10%">{!! $interest['totalAmount'] !!}</td>
                        <td width="1%" >  <a href="{{ url('sale/interest/details/'. $interest['sale_id'] ) }}" class="label label-success"> View </a></td>
                    </tr>
                @endforeach
                </tbody>
            </table>

        </div>
    @endcomponent
@endsection
@section('extra_js_footer')
    @include('layouts.dataTables.datatable')
    @include('layouts.dataTables.buttons')
    @include('layouts.form')
    @include('layouts.datePicker.datepicker')

    <script>
        $('input.date').datepicker();
        $("table#interest-date").DataTable({
            paginate: true
        })
    </script>
@endsection
