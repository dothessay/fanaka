@extends('layouts.master')
@section('title','Income Report')
@section('extra_css_header')
<style>
    .income{
       max-height: 70vh ;
       overflow: scroll ;
       scroll-margin: 8px ;
       scrollbar-shadow-color: #2befbe
       flex-direction: row;
   }
   ::-webkit-scrollbar {
       -webkit-appearance: none;
       width: 7px;
   }
   ::-webkit-scrollbar-thumb {
       border-radius: 4px;
       background-color: rgb(103, 194, 58);
       -webkit-box-shadow: 0 0 1px rgba(255,255,255,.5);
   }

</style>
@endsection
@section('content')
    @component('layouts.partials.panel')
        <div class="col-md-1"></div>
        <div class="col-md-10 offset-1 income" style="">

            <table class="table table-striped table-bordered" width="100%">
                <thead>
                <tr>
                    <th class="rotate">Date</th>
                    <th class="rotate">Customer</th>
                    <th class="rotate">Project</th>
                    <th class="rotate">Amount</th>
                    <th class="rotate">Payment Method</th>
                </tr>
                </thead>
                <tbody class="income">
                <?php $sum = []; ?>
                @foreach($incomes->chunk(10) as $incomeChuck)

                    @foreach($incomeChuck as $income)

                        @if(isset($income->payable->amount))
                            <?php $sum[] = isset($income->payable->amount) ? $income->payable->amount : 0?>
                            <tr>
                                <td>{{ Carbon\Carbon::parse($income->payable->deposit_date )->format('Y-m-d')}}</td>
                                <td>{{ isset($income->accountable->customer_details) ? $income->accountable->customer_details : "" }}</td>
                                <td>{{ isset( $income->accountable) ? $income->accountable->getCustomerPlotNos() : ''  }}</td>
                                <td>{{ number_format(isset($income->payable->amount) ? $income->payable->amount : 0 , 2) }}</td>
                                <td>{{ $income->getPaymentMethod() }}</td>
                            </tr>
                        @endif
                    @endforeach
                @endforeach
                </tbody>
                <tfoot>
                <tr>
                    <td colspan="3">Total</td>
                    <td style="border-top: double; border-bottom: double; width: 20px ; font-weight:bold">{{ number_format(array_sum($sum) ,2 ) }}</td>
                </tr>
                </tfoot>
            </table></div>
    @endcomponent
@endsection

@section('extra_js_footer')
<script>

</script>
@endsection
