<table class="table table-bordered table-striped"  width="100%" id="data-table">
    <thead>
    <tr>
        <th>Sale Reference</th>
        <th>Invoice Reference</th>
        <th>Served By</th>
        <th>Customer</th>
        <th>Total Amount Due</th>
        <th>Total Amount Paid</th>
        <th>Total Balance</th>
        <th>Due Date</th>
    </tr>
    </thead>
    <tbody>

    <tr>
        <td>FSL_{{$invoice->sale->id}}</td>
        <td>FINV_{{$invoice->id}}</td>
        <td>{{$invoice->user->fullName()}}</td>
        <td>{{$invoice->sale->getCustomerDetails()}}</td>
        <td>{{number_format((double)$invoice->total_amount , 2)}}</td>
        <td>{{number_format((double)$invoice->totalPaid() , 2)}}</td>
        <td>{{$balance = number_format(((double)$invoice->sale->getBalance()) , 2)}}</td>
        <td>{{$invoice->due_date}}</td>


    </tr>


    </tbody>
</table>

<p style="text-align: center; font-weight: bold">Project Details</p>
<table class="table table-bordered table-striped" width="100%" id="data-table">
    <thead>
    <tr>

        <th>Plot</th>
        <th>Plot Size</th>
        <th>Plot No</th>
        <th>Unit Price</th>

    </tr>
    </thead>
    <tbody>

    @foreach($invoice->invoiceItems as $invoiceItem)
        <tr>
            <td>{{$invoiceItem->plot->product->name}}</td>
            <td>{{$invoiceItem->plot->size->label}}</td>
            <td>{{$invoiceItem->plot_no}}</td>
            <td>{{number_format($invoiceItem->price , 2)}}</td>
        </tr>
    @endforeach

    </tbody>
</table>
<p style="text-align: center; font-weight: bold">Payment Statement</p>
<table class="table table-bordered table-striped" width="100%" id="data-table">
    <thead>
    <tr>

        <th>Date Received</th>
        <th>Date Deposited</th>
        <th>Amount Paid</th>
        <th>Method</th>
        <th>Reference</th>
        <th>Served By</th>
    </tr>
    </thead>
    <tbody>
    @php $paid = [] @endphp

    @foreach($payments as $payment)

        <?php $paid[] = $payment->amount?>
        <tr>
            <td>{{$payment->created_at}}</td>
            <td>{{$payment->deposit_date}}</td>
            <td class="bade badge-{{$payment->is_approved ? "success" : "danger"}}">{{number_format((double)$payment->amount , 2)}}</td>
            <td>{{$payment->payment_method}}</td>
            <td>{{$payment->reference_code}}</td>
            <td>{{$payment->user->fullName()}}</td>
        </tr>
    @endforeach
    </tbody>
    <tfoot>
    <tr>
        <td>Total Paid</td>
        <td colspan="4">{{number_format(array_sum($paid) , 2)}}  {{\Codex\Classes\Helper::numberToWord(array_sum($paid))}} </td>
    </tr>
    </tfoot>
</table>

<h4 style="text-align: center; text-transform: uppercase">Scanned Documents</h4>

<table class="table table-bordered table-striped data-table" width="100%">
    <thead>

    <tr>
        <th>Date Scanned</th>
        <th>Document Type</th>
        <th>Document Name</th>
        <th>View Document</th>

    </tr>

    </thead>

    <tbody>

    <?php $sumPaid = [];?>

    @foreach($invoice->sale->saleUploads as $key => $uploaded)
        <?php $upload = $uploaded->uploadsToArray() ; ?>
        <tr>
            <td>{{$upload['date']}}</td>
            <td>{{ $upload['type']}}</td>
            <td>{{ $upload['name']}}</td>
            <td><a href="{{$upload['url']}}" target="_blank">View</a> </td>


        </tr>

    @endforeach
    </tbody>


</table>
