{{--@can('access-module-component-functionality','sale_manage_sale-statistics')

    <div class="row">
        {!! statisticWidget('yearly sale Payment', $yearly, url('sale/manage/view?report=yearly')) !!}
        {!! statisticWidget('monthly sale Payment',$monthly , url('sale/manage/view?report=monthly')) !!}
        {!! statisticWidget('weekly sale Payment',$weekly, url('sale/manage/view?report=weekly')) !!}
        {!! statisticWidget('daily sale Payment',$daily, url('sale/manage/view?report=daily')) !!}
    </div>
@endcan--}}


<form class="form-horizontal form-bordered">

    <div class="form-group">
        {{--<label class="col-md-1 control-label">Sale Ref</label>
        <div class="col-md-3">
            <input type="number" class="form-control col-md-4 sale-ref-search">
        </div>--}}

        <label class="col-md-2 control-label">Select Customer</label>
        <div class="col-md-4">
            <select name="customer_id" class="form-control selectCustomer" data-live-search="true" data-size="10">

                <option data-hidden="true">Select customer</option>
                @foreach($customers as $customer)
                    <option value="{{$customer->id}}">{{$customer->fullName()}}</option>
                @endforeach
            </select>


        </div>

        <div class="col-md-2">
            <button class="btn btn-primary">Get sales</button>
        </div>


    </div>

</form>

@include('sales._list_table', ['sales' => $sales])
