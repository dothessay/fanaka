@extends('layouts.master')
@section('title','Paying for title on sale on fsl_'. $sale->id ." " . $sale->getCustomerDetails())

@section('content')

    @component('layouts.partials.panel')

       <div id="app">
           <form  class="form-horizontal addPropertyFormClass form-bordered" >

               @csrf



                   <div class="form-group">

                       <label class="control-label col-md-2">Payment Method</label>
                       <div class="col-md-4">
                           {{--  <select name="payment_method"  id="method" class="form-control selectpicker" data-live-search="true"
                                     data-size="5">--}}
                           @foreach(\Codex\Classes\Helper::system()->paymentMethods() as $paymentMethod)

                               <input type="radio" name="payment_method" class="method"
                                      value="{{$paymentMethod}}">{{$paymentMethod}}
                               {{--  <option value="{{$paymentMethod}}">{{$paymentMethod}}</option>--}}

                               @endforeach
                               </select>
                       </div>


                       <label class="control-label col-md-2">Amount Paid</label>
                       <div class="col-md-4">
                           <input type="text" name="amount" class="form-control gtotal" id="paid">
                           <input type="hidden" name="sale_id" class="form-control " value="{{$sale->id}}">
                       </div>


                   </div>







                   <div class="mpesa" style="display: none">
                       <div class="form-group">

                           <label class="control-label col-md-2">Reference code <span
                                       class="required-uzapoint">*</span></label>
                           <div class="col-md-4">
                               <input type="text" name="mpesa_reference_code" class="form-control">
                           </div>
                       </div>
                   </div>



                   <div class="bank" style="display: none">

                       <div class="form-group">

                           <label class="control-label col-md-2">Bank Name <span class="required-uzapoint">*</span>
                           </label>
                           <div class="col-md-4">
                               <input type="text" name="bank_name"  value="{{config('config.equity_bank.name')}}" class="form-control">
                           </div>

                           <label class="control-label col-md-2">Account No <span
                                       class="required-uzapoint">*</span></label>
                           <div class="col-md-4">
                               <input type="text" name="account_no"  value="{{config('config.equity_bank.number')}}" class="form-control">
                           </div>
                       </div>
                       <div class="form-group">


                           <label class="control-label col-md-2">Slip NO <span class="required-uzapoint">*</span></label>
                           <div class="col-md-4">
                               <input type="text" name="slip_no" class="form-control">
                           </div>

                       </div>
                   </div>

                   <div class="cheque" style="display: none">

                       <div class="form-group">
                           <label class="control-label col-md-2">Bank Name <span class="required-uzapoint">*</span></label>
                           <div class="col-md-4">
                               <input type="text" name="check_bank_name" class="form-control">
                           </div>

                           <label class="control-label col-md-2">Cheque No <span class="required-uzapoint">*</span></label>
                           <div class="col-md-4">
                               <input type="text" name="cheque_no" class="form-control">
                           </div>


                           <label class="control-label col-md-2">Drawer <span class="required-uzapoint">*</span></label>
                           <div class="col-md-4">
                               <input type="text" name="drawer" class="form-control">
                           </div>

                       </div>


                   </div>



               <div class="modal-footer">
                   <button class="btn btn-primary submitButton" type="button"  id="">Submit</button>
               </div>

           </form>
       </div>

    @endcomponent

@endsection

@section('extra_js_footer')

    <script>



        $(document).on('change', '.method', function () {


            if ($(this).val() === 'Mpesa') {
                $(".mpesa").show();
                $(".bank").hide();
                $(".cheque").hide();
            }
            if ($(this).val() === 'Bank') {
                $(".mpesa").hide();
                $(".bank").show();
                $(".cheque").hide();
            }
            if ($(this).val() === 'Cheque') {
                $(".mpesa").hide();
                $(".bank").hide();
                $(".cheque").show();
            }
            if ($(this).val() === 'Cash') {
                $(".mpesa").hide();
                $(".bank").hide();
                $(".cheque").hide();
            }
        })

        $(document).on("click","button.submitButton",function () {


            $(this).attr('disabled', true).text('submitting .....')

            var formData = new FormData($('.addPropertyFormClass')[0]);

            axios.post('/sale/manage/store-pay-title', formData).then((res) => {

                $(this).attr('disabled', false).text('Submit')

                $('.addPropertyFormClass').trigger('reset')
                $.growl.notice({
                    message: res.data.message
                })
            }).catch((err) => {

                $(this).attr('disabled', false).text('Submit')


                if (err.response.status === 422){

                    $.each(err.response.data.message ,(errors, values) => {

                        $.growl.warning({
                            message:values
                        })
                    })

                    return ;
                }



                $.growl.warning({
                    message:err.response.data.message
                })

            })
        });
    </script>
@endsection