@extends('layouts.master')
@section('title','Calculator')
@section('content')

    @component('layouts.partials.panel')

        <form class="form-horizontal">
            <div class="form-group">
                <label class="control-label col-md-2"> Project</label>
                <div class="col-md-4">
                    <select name="project" class="form-control selectpicker" id="project" data-live-search="true">
                        <option data-hidden="true">-SELECT PROJECT</option>
                        @foreach($projects as $project)
                            <option
                                    value="{{ $project->id}}"
                                    data-plots="{{ ($project->plots) }}"
                                    @if(isset($_GET['project']) && $_GET['project'] == $project->id ) selected @endif
                            >{{ $project->name() }} </option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-2"> Plots</label>
                <div class="col-md-4">
                    <select name="principal" class="form-control" id="principal" data-live-search="true">

                    </select>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-2"> principal</label>
                <div class="col-md-4">
                   <input value="{{ old('principal', @$_GET['principal']) }}" readonly class="form-control appendPrincipal">
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-2"> Deposit</label>
                <div class="col-md-4">
                   <input name="deposit" value="{{ old('deposit', @$_GET['deposit']) }}" class="form-control">
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-2"> Months</label>
                <div class="col-md-4">
                    <select name="months" class="form-control selectpicker" data-live-search="true">
                        @foreach(range(1 , 12) as $range)
                            <option
                                    @if(isset($_GET['months']) && $_GET['months'] == $range) selected @endif
                                    value="{{ $range }}"
                            >{{ $range}} Months </option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="modal-footer">
                <div class="email-footer">
                    <button class="btn btn-success">Calculate</button>
                    <button class="btn btn-success" name="pdf">Print PDF</button>
                </div>
            </div>
        </form>

        @include('sales.calculator._data')
    @endcomponent

@endsection

@section('extra_js_footer')

    @include('layouts.form')
    <script>
        $(document).on('change','select#project', function () {

            $("select#principal").empty();
            var options = ' <option data-hidden="true">-SELECT PLOT</option>';

            var plots = $("select#project option:selected").data('plots');

            for (let i = 0 ; i < plots.length ; i++)
            {
                options += '<option value="'+plots[i].price+'">'+plots[i].plot_no+'</option>'
            }
            $("select#principal").append(options).selectpicker('refresh')

        });

        $(document).on('change','select#principal', function () {

            $("input.appendPrincipal").empty();
            $("input.appendPrincipal").val($("select#principal option:selected").val());


        })
    </script>
@endsection
