
<div class="invoice-box">
    <h4 class="header text-center">Monthly Payment Plan</h4>
    <table class="table table-bordered table-striped">
        <thead>
        <tr class="heading">
            <td><strong>Month</strong></td>
            <td><strong>Monthly Payment</strong></td>
            @if(isset($showPaid)) <td><strong>Amount paid</strong></td>@endif
        </tr>
        </thead>
        <tbody>

        <?php  $total = 0 ;?>
        <?php  $interest = 0 ;?>
        @foreach($payments as $payment)
            <?php  $total += floatval($payment['monthlyPayment']) ;?>
            <tr class="item">
                <td> {{ $payment['month'] }}</td>
                <td> {{ number_format(floatval(round( $payment['monthlyPayment'] , 0 , PHP_ROUND_HALF_UP)) , 2) }}</td>
                @if(isset($showPaid))<td> {{ number_format(floatval($payment['amountPaid']) , 2) }}</td>@endif

            </tr>
        @endforeach
        </tbody>
        <tfoot>
        <tr class="total">
            <td colspan="1">Total</td>
            <td> <strong>{{ config('config.currency') }}{{ number_format(floatval( round($total,0, PHP_ROUND_HALF_UP)) , 2) }}</strong> </td>
        </tr>
        </tfoot>
    </table>
</div>

