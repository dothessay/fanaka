@extends('layouts.master')

@section('title','Appointment Booking')

@section('content')


    <ul class="nav nav-tabs tabs-uzapoint">

        <?php $active = true ? "active " : ""; ?>

        @can('access-module-component-functionality','sale_appointment_add')
            <li class="{{$active}}">
                <a href="#default-tab-1" data-toggle="tab">
                    <span class="hidden-xs">Add Appointment</span>
                </a>
            </li>

                <?php $active = false; ?>
        @endcan
        @can('access-module-component-functionality','sale_appointment_follow-up')

            <li class="{{$active}}">
                <a href="#default-tab-2" data-toggle="tab">
                    <span class="hidden-xs">List Of Appointments</span>
                </a>
            </li>

                <?php $active = false; ?>
        @endcan

            @can('access-module-component-functionality','sale_appointment_reports')
            <li class="{{$active}}">
                <a href="#default-tab-3" data-toggle="tab">
                    <span class="hidden-xs">Appointment Reports</span>
                </a>
            </li>
                <?php $active = false; ?>
        @endcan

    </ul>


    <div class="tab-content">
        <?php $active = true ? "active " : ""; ?>

        @can('access-module-component-functionality','sale_appointment_add')
            <div class="tab-pane fade {{$active}} in" id="default-tab-1">

                @include('sales.appointment._book')

            </div>

                <?php $active = false ?>
        @endcan


        @can('access-module-component-functionality','sale_appointment_follow-up')
            <div class="tab-pane fade {{$active}} in" id="default-tab-2">

                @include('sales.appointment._appointment')
            </div>
                <?php $active = false; ?>
        @endcan
            @can('access-module-component-functionality','sale_appointment_reports')
            <div class="tab-pane fade {{$active}} in" id="default-tab-3">

                @include('sales.appointment._reports')
            </div>
                <?php $active = false; ?>
        @endcan
    </div>
@endsection


@section('extra_js_footer')
@include('layouts.form')
@include('layouts.datePicker.datepicker')

@include('layouts.dataTables.buttons')
@include('layouts.dataTables.tablebuttons')


    <script>


        /*$("table#appointment-table").DataTable({
            sort:false
        })*/

        /*$('table#reservation').DataTable({
            dom: 'Bfrtip',
            buttons: [{
                extend: "copy",
                className: "btn-sm",
                exportOptions: {
                    columns: 'th:not(:last-child)'
                }
            }, {
                extend: "csv",
                className: "btn-sm",
                exportOptions: {
                    columns: 'th:not(:last-child)'
                }
            }, {
                extend: "excel",
                className: "btn-sm",
                exportOptions: {
                    columns: 'th:not(:last-child)'
                }
            }, {
                extend: "pdf",
                className: "btn-sm",
                exportOptions: {
                    columns: 'th:not(:last-child)'
                }
            },],
            responsive: !0

        });*/


        var date = new Date();
        date.setDate(date.getDate());


        $("input.phone").on('change', function () {


           let phone =  $(this).val()

            let newPhone =$("div.selected-dial-code").text()+''+ phone;

            $("input.phone").val(newPhone)



        });


        $("input.date").datetimepicker({
            minDate: 0
        });

        $('input.time').timepicker({
            timeFormat: 'h:mm p',
            interval: 60,
            minTime: '10',
            maxTime: '4:00pm',
            defaultTime: '11',
            startTime: '10:00',
            dynamic: false,
            dropdown: true,
            scrollbar: true
        });

        $(document).on("click","button.buttonSubmitAppointment",function () {

            $(this).attr('disabled', true).text('Booking . . . ')

            let formData = new FormData($("form.appointmentForm")[0]);

            axios.post('/sale/appointment/store', formData)
                .then((res) => {
                    $(this).attr('disabled', false).text('Book ')


                    $.growl.notice({
                        message: res.data.message
                    })

                    $("form.appointmentForm").trigger('reset')

                }).catch((err) => {
                $(this).attr('disabled', false).text('Book')

                if ((err.response.status === 422)){

                    $.each(err.response.data.message , (value, error) => {
                        $.growl.warning({
                            message: error
                        })

                    });

                    return;
                }
                $.growl.warning({
                    message: err.response.data.message
                })

            })

        })

        $(function () {

            FormSliderSwitcher.init();

            $('table#appointment-table').DataTable({
                sort:false,
                dom: 'Bfrtip',
                buttons: [{
                    extend: "copy",
                    className: "btn-sm",
                    exportOptions: {
                        columns: 'th:not(:last-child)'
                    }
                }, {
                    extend: "csv",
                    className: "btn-sm",
                    exportOptions: {
                        columns: 'th:not(:last-child)'
                    }
                }, {
                    extend: "excel",
                    className: "btn-sm",
                    exportOptions: {
                        columns: 'th:not(:last-child)'
                    }
                }, {
                    extend: "pdf",
                    className: "btn-sm",
                    exportOptions: {
                        columns: 'th:not(:last-child)'
                    }
                },],
                responsive: !0

            });



            $(document).on('click', 'a.updateStatus', function () {

                $("input#appointment_id").val($(this).data('id'));
                $("span#clientName").text($(this).data('fullname'));

            })

            $(document).on('change', '#hasVisited', function () {

                if($('#hasVisited').is(":checked"))
                {
                    $("div.comment").removeClass('hidden')
                }
                else{
                    $("div.comment").addClass('hidden')
                }

            })

            $(document).on('click','button.submitComment', function () {

                let formData = new FormData($("form.UpdateAppointmentForm")[0]);



                $(this).attr('disabled', true).text('Updating .... ');


                axios.post('/sale/appointment/update',formData)
                    .then((res) => {


                        $(this).attr('disabled', false).text('Update');
                        $("form.UpdateAppointmentForm").trigger('reset')
                        $("div.updateReservationStatus").modal('hide')

                        $.growl.notice({message: res.data.message})


                    })
                    .catch((err) => {

                        $.growl.warning({message: err.response.data.message})

                        $(this).attr('disabled', false).text('Update');
                    })

            })
        })
    </script>
@endsection