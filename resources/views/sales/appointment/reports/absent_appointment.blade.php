@extends('layouts.master')
@section('title',"Absentee's Appointments ". \Carbon\Carbon::today()->format('Y-M-d'))

@section('content')

    @include('layouts.back')
    @component('layouts.partials.panel')

        @include('sales.appointment.reports._today_table')

    @endcomponent
@endsection

@section('extra_js_footer')
    @include('layouts.form')
    @include('layouts.dataTables.tablebuttons')
    @include('layouts.dataTables.buttons')

    <script>
        $(function () {

            FormSliderSwitcher.init();

            $('table#reservation').DataTable({
                sort:false,
                dom: 'Bfrtip',
                buttons: [{
                    extend: "copy",
                    className: "btn-sm",
                    exportOptions: {
                        columns: 'th:not(:last-child)'
                    }
                }, {
                    extend: "csv",
                    className: "btn-sm",
                    exportOptions: {
                        columns: 'th:not(:last-child)'
                    }
                }, {
                    extend: "excel",
                    className: "btn-sm",
                    exportOptions: {
                        columns: 'th:not(:last-child)'
                    }
                }, {
                    extend: "pdf",
                    className: "btn-sm",
                    exportOptions: {
                        columns: 'th:not(:last-child)'
                    }
                },],
                responsive: !0

            });



            $(document).on('click', 'a.updateStatus', function () {

                $("input#appointment_id").val($(this).data('id'));
                $("span#clientName").text($(this).data('fullname'));

            })

            $(document).on('change', '#hasVisited', function () {

                if($('#hasVisited').is(":checked"))
                {
                    $("div.comment").removeClass('hidden')
                }
                else{
                    $("div.comment").addClass('hidden')
                }

            })

            $(document).on('click','button.submitComment', function () {

                let formData = new FormData($("form.UpdateAppointmentForm")[0]);



                $(this).attr('disabled', true).text('Updating .... ');


                axios.post('/sale/appointment/update',formData)
                    .then((res) => {


                        $(this).attr('disabled', false).text('Update');
                        $("form.UpdateAppointmentForm").trigger('reset')
                        $("div.updateReservationStatus").modal('hide')

                        $.growl.notice({message: res.data.message})


                    })
                    .catch((err) => {

                        $.growl.warning({message: err.response.data.message})

                        $(this).attr('disabled', false).text('Update');
                    })

            })
        })
    </script>
@endsection