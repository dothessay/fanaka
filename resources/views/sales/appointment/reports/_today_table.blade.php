<table class="table table-bordered table-striped table-secondary" id="reservation">
    <thead>
    <tr>
        <th>Appointment Date</th>
        <th>Client</th>
        <th>Marketer</th>
        <th>Projects Interested</th>
        <th>Phone Number</th>
        <th>Has Visited</th>
        <th>Is Interested</th>
        <th>Notes</th>
        <th>Actions</th>
    </tr>
    </thead>
    <tbody>
    @foreach($appointments as $appointment)

        <tr>
            <td>{!! $appointment['date_time'] !!}</td>
            <td>{{$appointment['full_name']}}</td>
            <td>{{$appointment['agent']}}</td>
            <td>{{$appointment['projects']}}</td>
            <td>{{$appointment['phone_number']}}</td>
            <td>{!! $appointment['has_visited'] !!}</td>
            <td>{!! $appointment['is_interested'] !!}</td>
            <td>{!! str_limit($appointment['comment'] ,100 , '[ read More ] ') !!}</td>
            <td>
                @component('layouts.button')

                    @can('access-module-component-functionality','sale_appointment_can-update')
                        <li><a
                                    data-toggle="modal"
                                    data-target="#updateReservationStatus"
                                    data-id="{{$appointment['id']}}"
                                    data-fullname="{{$appointment['full_name']}}"
                                    class="updateStatus">Update Status</a></li>
                    @endcan
                @endcomponent
            </td>
        </tr>

    @endforeach
    </tbody>
</table>



@component('layouts.partials.modal')
    @slot('id','updateReservationStatus')
    @slot('dialogClass','modal-lg')
    @slot('title')
        <h4>Add site visit details here for client <span id="clientName">@endslot


                <form class="form-bordered form-horizontal UpdateAppointmentForm" method="post"
                      action="{{route('sale_appointment_update')}}">

        @csrf

                    <div class="form-group">
            <label class="col-md-2 control-label">Visited ?</label>
            <div class="col-md-4">

                <input type="checkbox" data-render="switchery" checked id="hasVisited" name="has_visited"
                       data-theme="default">
            </div>
        </div>


        <div class="">


             <div class="form-group">
            <label class="col-md-2 control-label">Interested ?</label>
            <div class="col-md-4">

                <input type="checkbox" data-render="switchery" checked name="is_interested"
                       data-theme="default">
            </div>
             </div>

            <div class="form-group">
                <label class="col-md-2 control-label">Comment</label>
                <div class="col-md-10">

                    <textarea name="comments" id="comment"></textarea>

                </div>
                <input type="hidden" class="form-control" id="appointment_id" name="id">
            </div>




        </div>

        <div class="modal-footer form-group">
            <button type="submit" class=" btn btn-success">Update</button>
        </div>
    </form>



        @endcomponent