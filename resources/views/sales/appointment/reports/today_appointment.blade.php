@extends('layouts.master')
@section('title',"Today's Appointments ". \Carbon\Carbon::today()->format('Y-M-d'))

@section('content')

    @include('layouts.back')
@component('layouts.partials.panel')


    <div class="col-md-12 col-xs-12">

        <form class="form-horizontal form-bordered">
            <div class="form-group">
                <label class="col-md-2 control-label">Start Date</label>
                <div class="col-md-4">
                    <input type="text" class="form-control date" name="start_date" value="{{old('start_date')}}">
                </div>

                <label class="col-md-1 control-label">End Date</label>
                <div class="col-md-4">
                    <input type="text" class="form-control date" name="end_date" value="{{old('end_date')}}">
                </div>
            </div>

            <div class="form-group">
                <label class="col-md-2 control-label">Visit Options</label>
                <div class="col-md-4">
                    <select class="form-control selectpicker" data-live-search="true" name="options">
                        <option value="false" data-hidden="true">SELECT A OPTIONS</option>
                        <option value="visited">Visited</option>
                        <option value="not_visited">Not Visited</option>
                    </select>
                </div>

                <label class="col-md-1 control-label">Marketer</label>
                <div class="col-md-4">
                    <select class="form-control selectpicker" data-live-search="true" name="agent_id">
                        <option value="false" data-hidden="true">All</option>
                        @foreach($users as $user)
                        <option value="{{$user->id}}">{{$user->fullName()}}</option>
                        @endforeach
                    </select>
                </div>

                <label class="col-md-1 control-label">&nbsp;&nbsp;</label>
                <div class="col-md-4">
                    <button type="submit" class="btn btn-sm btn-primary">Pull</button>
                </div>
            </div>
        </form>
    </div>

@include('sales.appointment.reports._today_table')

@endcomponent
@endsection

@section('extra_js_footer')
    @include('layouts.form')
    @include('layouts.dataTables.tablebuttons')
    @include('layouts.dataTables.buttons')
    @include('layouts.form')
    @include('layouts.datePicker.datepicker')

    <script>
        $(function () {

            var date = new Date();
            date.setDate(date.getDate());

            $("input.date").datepicker({
                maxDate: date
            });


            FormSliderSwitcher.init();

            $('table#reservation').DataTable({
                sort:false,
                dom: 'Bfrtip',
                buttons: [{
                    extend: "copy",
                    className: "btn-sm",
                    exportOptions: {
                        columns: 'th:not(:last-child)'
                    }
                }, {
                    extend: "csv",
                    className: "btn-sm",
                    exportOptions: {
                        columns: 'th:not(:last-child)'
                    }
                }, {
                    extend: "excel",
                    className: "btn-sm",
                    exportOptions: {
                        columns: 'th:not(:last-child)'
                    }
                }, {
                    extend: "pdf",
                    className: "btn-sm",
                    exportOptions: {
                        columns: 'th:not(:last-child)'
                    }
                },],
                responsive: !0

            });



            $(document).on('click', 'a.updateStatus', function () {

                $("input#appointment_id").val($(this).data('id'));
                $("span#clientName").text($(this).data('fullname'));

            })

            $(document).on('change', '#hasVisited', function () {

                if($('#hasVisited').is(":checked"))
                {
                    $("div.comment").removeClass('hidden')
                }
                else{
                    $("div.comment").addClass('hidden')
                }

            })

            $(document).on('click','button.submitComment', function () {

                let formData = new FormData($("form.UpdateAppointmentForm")[0]);



                $(this).attr('disabled', true).text('Updating .... ');


                axios.post('/sale/appointment/update',formData)
                    .then((res) => {


                        $(this).attr('disabled', false).text('Update');
                        $("form.UpdateAppointmentForm").trigger('reset')
                        $("div.updateReservationStatus").modal('hide')

                        $.growl.notice({message: res.data.message})


                    })
                    .catch((err) => {

                        $.growl.warning({message: err.response.data.message})

                        $(this).attr('disabled', false).text('Update');
                    })

            })
        })
    </script>
    @endsection