<div class="boxed-layout col-md-12">
    {!! \Codex\Classes\Helper::reportWidget("Daily Appointment","sale_appointment_today-appointment","This report shows all appointment scheduled for today") !!}
    {!! \Codex\Classes\Helper::reportWidget("Absentees Appointment","sale_appointment_absent-appointment","This report shows all appointment scheduled for today") !!}
    {!! \Codex\Classes\Helper::reportWidget("Successfull Appointment","sale_appointment_absent-appointment","This report shows all appointment scheduled for today") !!}
</div>