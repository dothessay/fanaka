<form class="form-horizontal form-bordered " method="post" action="{{route('sale_appointment_store')}}">

    @csrf

    <div class="form-group">
        <label class="control-label col-md-2">Client Name</label>
        <div class="col-md-4">

            <input type="text" required name="full_name" class="form-control">
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-md-2">Phone Number</label>
        <div class="col-md-4">

            <input type="text" required id="phone_number" name="phone_number" class="form-control phone">
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-md-2">Date</label>
        <div class="col-md-4">

            <input type="text" required name="date" class="form-control date">
        </div>


       <label class="control-label col-md-1">Send Notification</label>
        <div class="col-md-2">

            <input type="checkbox" required name="send_notification" class="checkbox-inline">
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-md-2">Projects</label>
        <div class="col-md-10">
            @foreach($projects->chunk(2) as $project)

                <div class="row  col-md-4" style="border: thin">
                    @foreach($project as $value)
                        <div style="padding: 0px 12px 12px 0px;">
                            <input type="checkbox" name="project_id[]" value=" {{$value->id}}" class="email-checkbox ">

                            {{$value->name()}}
                        </div>

                        @endforeach
                </div>

                @endforeach

        </div>
    </div>

    <div class="modal-footer">

        <button type="submit" class="btn btn-success buttonSubmitAppointmentl">Book</button>
    </div>

</form>
