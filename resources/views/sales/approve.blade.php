    @extends('layouts.master')
    @section('title','detailed sale report')
    @section('content')
    <div id="app">

        @include('layouts.back')
        @component('layouts.partials.panel')

            <div class="table-responsive">
                <table class="table-bordered table-striped table" id="data-table" width="100%">
                    <thead>
                    <tr>
                        <th>Sale Reference</th>
                        <th>Entered By</th>
                        <th>Agent</th>
                        <th>Approved By</th>
                        <th>Customer</th>
                        <th>Total Amount</th>
                        <th>PrePaid Amount</th>
                        <th>Balance</th>
                        <th>Instalment Due Date</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>


                    <?php

                    $color = isset($sale->color) ? "<style>background:{$sale->color}; color: #fff !important</style>" : "";
                    $class = "style={$color}";

                    if (!$sale->is_approved) {
                        $class = "style=background:lightblue";
                    }

                    $approvedBy = isset($sale->approvedBy) ? $sale->approvedBy->fullName() : "Not Approved";
                    ?>
                    <tr>
                        <td class="{{ $color }}"> FSL_{{$sale->id}}</td>
                        <td {{$class}}> {{$sale->user->fullName()}}</td>
                        <td {{$class}}> {{$sale->soldBy->fullName()}}</td>
                        <td {{$class}}> {{$approvedBy}}</td>
                        <td {{$class}} width="15%"> {!! $sale->getCustomerHref() !!}</td>
                        <td {{$class}}> {{number_format((double)$sale->total_amount , 2)}}</td>
                        <td {{$class}}> {{number_format((double)$sale->getPrepaidAmount() , 2)}} </td>
                        <td {{$class}}> {{number_format((double)$sale->getBalance() , 2)}} </td>
                        <td {{$class}}> @if( isset($sale->installment)) {{ $sale->installment->completion_date }} @endif </td>
                        <td {{$class}}>
                            @component('layouts.button')

                                @can('access-module-component-functionality','sale_manage_update')
                                    @if(! $sale->is_approved)
                                        <li><a href="{{url('sale/manage/approve/'.$sale->id)}}">Approve</a></li>
                                        <li><a href="{{url('sale/manage/reject/'.$sale->id)}}">Cancel</a></li>
                                    @endif
                                @endcan



                                <li>
                                    <a href="{{url('sale/manage/receipt/'.$sale->id)}}" target="_blank">Receipt</a>
                                </li>

                                @if($sale->is_agreement_uploaded)
                                    <li>
                                        Agreement</a>
                                    </li>   <a href="{{url('sale/manage/uploaded-agreement/'.$sale->id)}}" target="_blank">Scanned

                                @endif

                                @if( (!$sale->is_agreement_uploaded) && $sale->is_agreement_downloaded)
                                    <li>
                                        <a data-toggle="modal"
                                           data-title="Upload Scanned Agreement"
                                           data-id="{{$sale->id}}"
                                           id="uploadAgreementNow"
                                           data-target="#UploadScannedAgreement">
                                            Upload Agreement
                                        </a>
                                    </li>



                                @endif

                                @if( (!$sale->is_agreement_uploaded) && (! $sale->is_agreement_downloaded))
                                                <li>
                                                    <a href="{{ url('sale/manage/edit-agreement/'. $sale->id)}}">Print
                                                        Agreement</a>
                                                </li>

                                @endif

                                <li>
                                    <a data-toggle="modal" data-target="#witness"
                                       data-witness="{{($sale->witnesses)}}"
                                       data-sale_id="{{($sale->id)}}"
                                       data-customer_id="{{($sale->customer_id)}}"
                                       id="addWitness">Witnesses</a>
                                </li>
                            @endcomponent
                        </td>
                    </tr>

                    </tbody>
                </table>


                <h4 style="text-align: center; text-transform: capitalize">Plot Details</h4>

                <table class="table table-bordered table-striped" id="data-table" width="100%" cellspacing="0"
                       cellpadding="0">
                    <thead>
                    <tr>
                        <th>Land Location</th>
                        <th>Price</th>
                        <th>Size</th>
                        <th>Plot No</th>
                        <th>Parcel No</th>
                        <th>Parcel No Issued</th>
                        <th>Issued By</th>
                        <th>Date Issued</th>
                        @can('access-module-component-functionality','plots_manage_release')

                            <th> Release</th>

                        @endcan


                    </tr>
                    </thead>
                    <tbody>

                    @foreach($sale->saleItems as $saleItem)
                        <tr>
                            <?php
                            $plot = $saleItem->plot;


                            ?>

                            <td>{{$plot->product->name()}}</td>


                            <td>{{number_format($saleItem->price , 2)}}</td>
                            <td>{{$plot->size->label}}</td>

                            <td><a href="{{route('plots_manage_details' ,['id' => $plot->id])}}">{{$plot->plot_no}}</a></td>
                            <td>{{$plot->title_no}}</td>

                            @php

                                if ($plot->is_title_issued )
                                { $titleIssued= 'primary';
                                }
                                else{

                                $titleIssued = "warning";

                                }


                            @endphp
                            <td>
                                <span class="label label-{{$plot->is_title_issued ? "primary" : "danger" }}">{{$plot->is_title_issued ? "Issued" : "Not Issued" }}</span>
                            </td>
                            <td>{{$plot->issuedBy? $plot->issuedBy->fullName() : ""}}</td>
                            <td>{{$plot->date_issued}}</td>
                            @can('access-module-component-functionality','plots_manage_release')

                                <td>
                                    <a data-type="sale" class="releasePlot" data-id="{{$plot->id}}" href="javascript::void(0)">Release</a>
                                </td>

                            @endcan
                        </tr>
                    @endforeach
                    </tbody>
                </table>

                <h4 style="text-align: center; text-transform: uppercase">Payment Details</h4>

                <table class="table table-bordered table-striped data-table" width="100%">
                    <thead>

                    <tr>
                        <th>Date Deposited</th>
                        <th>Date Received</th>
                        <th>Amount Paid</th>
                        <th>Payment Method</th>
                        <th>Reference No</th>
                        <th>Received By</th>
                    </tr>

                    </thead>

                    <tbody>

                    @foreach($payments as $payment)


                        <tr>
                            <td>{{$payment->deposit_date}}</td>
                            <td>{{$payment->created_at}}</td>
                            <td>{{number_format($payment->amount , 2)}}</td>
                            <td>{{$payment->payment_method}}</td>
                            <td>{{$payment->reference_code}}</td>
                            <th>{{$payment->user->fullName()}}</th>
                        </tr>

                    @endforeach
                    </tbody>
                    <tfoot>
                    <tr>
                        <td colspan="2"><strong>Totals</strong></td>
                        <td colspan="9"><strong>{{number_format($payments->sum('amount') , 2)}}</strong></td>
                    </tr>
                    </tfoot>


                </table>

                <h4 style="text-align: center; text-transform: uppercase">Scanned Documents</h4>

                <table class="table table-bordered table-striped data-table" width="100%">
                    <thead>

                    <tr>
                        <th>Date Scanned</th>
                        <th>Document Type</th>
                        <th>View Document</th>

                    </tr>

                    </thead>

                    <tbody>

                    <?php $sumPaid = [];?>

                    @foreach($sale->saleUploads->map(function ($q) { return $q->uploadsToArray() ;}) as $key => $upload)


                        <tr>
                            <td>{{$upload['date']}}</td>
                            <td>{{ $upload['type']}}</td>
                            <td><a data-toggle="modal" data-target="#viewDocument" class="view-document"
                                   data-file-name="{{$upload['name']}}" data-url="{{$upload['url']}}"
                                   target="_blank">View</a></td>


                        </tr>

                    @endforeach
                    </tbody>


                </table>


            </div>
        @endcomponent


        @component('layouts.partials.modal')

            @slot('dialogClass','modal-lg')
            @slot('id','viewDocument')
            @slot('title') <h6>Scanned Document</h6>@endslot


            <div class="document-details"></div>

        @endcomponent




        @component('layouts.partials.modal')

            @slot('dialogClass','modal-lg')
            @slot('id','details')
            @slot('title') <h6>Sale Details</h6>@endslot


            <div class="sale_details"></div>

        @endcomponent


        @component('layouts.partials.modal')

            @slot('dialogClass','modal-lg')
            @slot('id','witness')
            @slot('title') <h6>Witnesses</h6>@endslot

            <form method="post" action="{{route('sale_manage_store-witness')}}">
                {{csrf_field()}}
                <input type="hidden" name="sale_id" id="witness_sale_id">
                <input type="hidden" name="customer_id" id="witness_customer_id">
                <div class="form-group">
                    <button type="button" class=" btn btn-primary add-witness"><span class="fa fa-plus">Add Witness</span>
                    </button>
                </div>
                <table class="table table-bordered table-responsive" width="100%" id="witness-table">
                    <thead>
                    <tr>
                        <td>Witness Name</td>
                        <td>Witness ID</td>
                        <td></td>
                    </tr>
                    </thead>
                    <tbody></tbody>
                </table>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-success" id="witnessBtn" disabled="disabled">Add Witness</button>
                </div>

            </form>


        @endcomponent



        @component('layouts.partials.modal')

            @slot('dialogClass','modal-lg')
            @slot('id','UploadScannedAgreement')
            @slot('title') <h6>Upload Scanned Agreement for this Sale</h6>@endslot

            <form method="post" action="{{route('sale_manage_upload-scanned-agreement')}}" enctype="multipart/form-data">
                {{csrf_field()}}
                <input type="hidden" name="id" id="idUploadAgreement">

                <div class="form-group">
                    <label class="control-label col-md-2">Upload Agreement</label>
                    <input name="agreement" type="file" class="document-file">
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-success">Upload</button>
                </div>

            </form>


        @endcomponent




    </div>

    @endsection


    @section('extra_js_footer')
        @include('layouts.dataTables.datatable')

        @include('layouts.datePicker.datepicker')
        <script>


            $(document).on('click', 'a.view-document', function () {

                let source = $(this).data("url");


                $("div.document-details").empty();

                let filename = $(this).data('file-name');

                let pieces = filename.split(".");


                if (pieces[1] == 'pdf') {

                    $("div.document-details").append('<iframe src="' + source + '" style="width:718px; height:700px;" frameborder="0"></iframe>');

                    return;
                }


                $("div.document-details").append("<img src='" + source + "' class='img-responsive thumbnail'>");


            });

            (function () {

                $('#plot_no').val($("select#plot option:selected").attr('data-plot_no'))

            })();


            $(document).on('click', 'a#addWitness', function () {


                var witnesses = ($(this).data('witness'));

                $("#witness_sale_id").val($(this).data('sale_id'));
                $("#witness_customer_id").val($(this).data('customer_id'));

                var tbody = '';

                var url = "{{url('sale/manage/remove-witness/')}}";


                for (var i = 0; i < witnesses.length; i++) {

                    tbody += '<tr>' +
                        '<td>' + witnesses[i].name + '</td>' +
                        '<td>' + witnesses[i].id_no + '</td>' +
                        '<td><a href="' + url + '/' + witnesses[i].id + '" class="btn btn-danger">Remove </td>' +
                        '</tr>'

                }

                $("#witness-table>tbody").append(tbody);
                $("#witness-table").addClass('table-striped nowrap').DataTable({sort: false})

            });


            var witnessHtml = '<tr>' +
                ' <td> ' +

                '<input type="text" name="witness_name[]" class="form-control">' +
                '<td> ' +

                ' <input type="number" name="witness_id[]" class="form-control"> ' +
                '</td>' +
                '<td> ' +
                '<button class="btn btn-danger removeWitness" type="button">-</button>' +
                ' </td>' +
                '</tr>';


            $(document).on('click', 'button.add-witness', function () {


                $("#witness-table>tbody").append(witnessHtml);

                $("#witnessBtn").removeAttr('disabled');
            });


            $(document).on('click', 'button.removeWitness', function () {

                $(this).closest('tr').remove();


            });


            $(document).on('click', 'a#uploadAgreementNow', function () {

                $("#idUploadAgreement").val($(this).data('id'));

            });


            $(document).on('change', '#plot', function () {

                $('#plot_no').val($("select#plot option:selected").attr('data-plot_no'))

            });

            $('.datepicker').datepicker();

            $(document).on('change', '#payment_type', function () {


                if ($(this).val('full')) {
                    $(".next_date").slideUp();
                }
                if ($(this).val('installment')) {

                    $(".next_date").slideDown();
                }


            });


            $(document).on('click', 'button.AddMoreProduct', function () {

                $("#addSale>tbody").html('');


                var customerId = $('#customer').val();

                var customerFullName = $("select#customer option:selected").text();

                var plotId = $('#plot').val();
                var plotLabel = $("select#plot option:selected").text();
                var quantity = 1;
                var unitPrice = $("select#plot option:selected").attr('data-unit-price');

                var PlotNo = $("select#plot option:selected").attr('data-plot_no');


                var htmlObject = "<tr class='myRow[]' id='" + customerId + plotId + "'>" +
                    "<td><input type='hidden' name='customer_id' value='" + customerId + "'>" + customerFullName + "</td>" +
                    "<td><input type='hidden' name='plot_id' value='" + plotId + "'>" + plotLabel + "</td>" +
                    "<td><input type='text' class='col-md-4' name='plot_no' readonly value='" + PlotNo + "'></td>" +
                    "<td><input type='hidden' readonly='readonly' id='price" + customerId + plotId + "' class='col-md-12' name='price' value='" + unitPrice + "'>" + unitPrice + "</td>" +
                    "<td>" +
                    "<span id='subtotal" + customerId + plotId + "'  class='subtotal'>0.0</td>" +
                    "</tr>";

                var i = 1;

                var myTableRow = $("table#addSale>tbody>tr#" + customerId + plotId);
                console.log('rows' + myTableRow);

                if (myTableRow.length) {


                    return false;
                } else {
                    $("#addSale>tbody").append(htmlObject);

                    $(".addPayment").show();

                }
                compute_total_price();


            });


            var compute_total_price = function () {
                var total_price = 0;
                $('tr[class="myRow[]"]').each(function () {
                    var id = ($(this)[0]['id']);
                    var q = 1; ///from an input you can edit
                    var p = $("#price" + id).val();
                    console.info('price' + p + 'was');
                    if (!p) {
                        var pr = $("#price" + id).html();
                        p = pr;
                    }

                    console.info('price' + p + 'is');


                    $("#subtotal" + id).html(p * q);
                    total_price = total_price + p * q;
                    console.info(total_price)
                });


                var sum_sub_total = $("table#addSale>tbody>tr").find('span.subtotal');
                var su = 0;

                console.info('subtotal is ' + sum_sub_total);
                $.each(sum_sub_total, function () {

                    su = su + parseInt($(this).html());
                    //su +=total_price;
                });
                $("#total").html(su);
                $(".gtotal").val(su);
                $("#amountToPay").val(su);
    //                $("#balance").html(su);
                //calculate_balance();
                console.info('total_price is' + su)

            };

            $("#data-table").DataTable({
                sort: false

            });


            $(document).on('click', 'a#sale-detail', function () {


                $('.sale_details').html('');
                var saleItems = JSON.parse($(this).attr('data-saleItems'));

                var tbodyItems = '';


                for (var i = 0; i < saleItems.length; i++) {

                    tbodyItems += '<tr>' +
                        '<td>' + saleItems[i].plot_no + '</td>' +
                        '<td>' + saleItems[i].plot.product.name + '</td>' +
                        '<td>' + saleItems[i].plot.size.label + '</td>' +
                        '<td>' + saleItems[i].price + '</td>' +
                        '</tr>';

                }
                var table = '<table class="table-bordered table-striped table" width="100%"> ' +
                    '<thead>' +
                    '<tr>' +
                    '<th>Plot No</th>' +
                    '<th>Plot Name</th>' +
                    '<th>Size</th>' +
                    '<th>Plot Price</th>' +
                    '</tr>' +
                    '</thead>' +
                    '<tbody>' + tbodyItems +
                    '</tbody>' +
                    '</table>';
                $('.sale_details').html(table)


            })

        </script>
    @endsection
