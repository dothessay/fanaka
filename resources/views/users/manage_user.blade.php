@extends("layouts.master")
@section('title','Manage Users')
@section('content')

    @can('access-module-component-functionality', 'users_manage_add')
        <ul class="nav nav-pills">
            <li class="active">
                <a class="btn btn-info" data-toggle="modal" data-target="#addUser">Add User</a>
            </li>
        </ul>
    @endcan

    @component('layouts.partials.panel')
        @include('users._users_table')
    @endcomponent



    @component('layouts.partials.modal')
        @slot("id","addUser")
        @slot('dialogClass','modal-lg')
        @slot('title')<h6>
            Fill the form below to add a User
        </h6>@endslot


        <form class="form-horizontal form-bordered" method="post" action="{{route('users_manage_store')}}">
            {{csrf_field()}}
            <div class="form-group">
                <label class="control-label col-md-2">First Name <span class="required-uzapoint">*</span> </label>
                <div class="col-md-4">
                    <input type="text" class="form-control" name="first_name" placeholder="John">
                </div>

                <label class="control-label col-md-2">Last Name <span class="required-uzapoint">*</span> </label>
                <div class="col-md-4">
                    <input type="text" class="form-control" name="last_name" placeholder="Doe">
                </div>
            </div>


            <div class="form-group">
                <label class="control-label col-md-2">Phone Number <span class="required-uzapoint">*</span> </label>
                <div class="col-md-4">
                    <input type="text" class="form-control phone"  placeholder="254 712 345 678">
                </div>


                <label class="control-label col-md-2">E-mail <span class="required-uzapoint">*</span> </label>
                <div class="col-md-4">
                    <input type="text" class="form-control" name="email" placeholder="johndoe@fanaka.co.ke">
                </div>
            </div>


            <div class="form-group">


                <label class="control-label col-md-2"> Password <span class="required-uzapoint">*</span> </label>
                <div class="col-md-4">
                    <input type="password" class="form-control" name="password" placeholder="xxxxxx">
                </div>


                <label class="control-label col-md-2">Role</label>
                <div class="col-md-4">
                   <select name="role_id" class="form-control selectpicker" data-live-search="true" data-size="6">
                       @foreach($roles as $role)
                           <option value="{{$role->id}}">{{$role->label}}</option>
                           @endforeach
                   </select>
                </div>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-success">Submit</button>
            </div>
        </form>

    @endcomponent


    <!-- edit user -->
    @component('layouts.partials.modal')
        @slot("id","update-user")
        @slot('dialogClass','modal-lg')
        @slot('title')<h6>
            Edit User
        </h6>@endslot


        <form class="form-horizontal form-bordered" id="editUser" method="post" action="{{route('users_manage_update')}}" autocomplete="off">
            {{csrf_field()}}
            <div class="form-group">
                <label class="control-label col-md-2">First Name <span class="required-uzapoint">*</span> </label>
                <div class="col-md-4">
                    <input type="text" class="form-control" name="first_name" id="first_name" placeholder="John">
                </div>

                <label class="control-label col-md-2">Last Name <span class="required-uzapoint">*</span> </label>
                <div class="col-md-4">
                    <input type="text" class="form-control" name="last_name" id="last_name" placeholder="Doe">
                </div>
            </div>


            <div class="form-group">
                <label class="control-label col-md-2">Phone Number <span class="required-uzapoint">*</span> </label>
                <div class="col-md-4">
                    <input type="text" class="form-control" name="phone_number" id="phone_number" placeholder="254 712 345 678">
                </div>


                <label class="control-label col-md-2">E-mail <span class="required-uzapoint">*</span> </label>
                <div class="col-md-4">
                    <input type="text" class="form-control" name="email" id="email" placeholder="johndoe@fanaka.co.ke">
                </div>
            </div>


            <div class="form-group">


                <label class="control-label col-md-2"> Password <span class="required-uzapoint">*</span> </label>
                <div class="col-md-4">
                    <input type="password" class="form-control" name="password" id="password" placeholder="xxxxxx">
                </div>


                <label class="control-label col-md-2">Role</label>
                <div class="col-md-4">
                   <select name="role_id" id="role_id" class="form-control selectpicker" data-live-search="true" data-size="6">
                       @foreach($roles as $role)
                           <option value="{{$role->id}}">{{$role->label}}</option>
                           @endforeach
                   </select>
                </div>
            </div>

            <input type="hidden" name="id" id="id">

            <div class="modal-footer">
                <button type="submit" class="btn btn-success">Edit</button>
            </div>
        </form>

    @endcomponent

    <!-- end edit user -->

    @component('layouts.partials.modal')
        @slot('id','delete-user')
        @slot('title')
            <h5>Are you sure you want to deactivate this user? </h5>
        @endslot



        <form method="post" class="form-horizontal form-bordered" action="{{route('users_manage_delete')}}">
            {{csrf_field()}}
            <input type="hidden" id="id-user" name="id">

            <div class="form-group">
                <label class="control-label col-md-4">Assign Customers To</label>
                <div class="col-md-4">
                    <select name="user_id" id="selectUser" data-live-search="true" data-size="4" class="form-control selectpicker">
                        @foreach($users as $user)
                            <option value="{{$user->id}}">{{$user->fullName()}}</option>
                        @endforeach
                    </select>
                </div>
            </div>


            <div class="modal-footer">
                <button type="submit" class="btn btn-danger">Delete</button>
            </div>
        </form>


    @endcomponent




@endsection

@section('extra_js_footer')
    @include('layouts.dataTables.datatable')
    @include('layouts.form')
    <script>
        $("#data-table").DataTable({
            sort: false
        });

        $(document).ready(function () {
            $('#data-table').on('click', '#edit', function(){
                console.log($(this).data('role_id'));
                $("#editUser #first_name").val($(this).data('first_name'));
                $("#editUser #last_name").val($(this).data('last_name'));
                $("#editUser #phone_number").val($(this).data('phone_number'));
                $("#editUser #email").val($(this).data('email'));
                $("#editUser #role_id").val($(this).data('role_id'));
                $("#editUser #id").val($(this).data('id'));
            });
        });

        $(document).on('click','a#del-id', function () {

            var id = $(this).data('id');

            $("#id-user").val(id);
        });
    </script>
@endsection
