@extends('layouts.master')
@section('title','Permissions')
@section('content')


    <ul class="nav nav-tabs tabs-uzapoint">

        <?php $active = true ? "active " : ""; ?>

        @can('access-module-component-functionality','users_permissions_role')
            <li class="{{$active}}">
                <a href="#default-tab-1" data-toggle="tab">
                    <span class="hidden-xs">Role Permissions</span>
                </a>
            </li>
            <?php $active = false; ?>
        @endcan
        @can('access-module-component-functionality','users_permissions_user')
            <li class="{{$active}}">
                <a href="#default-tab-2" data-toggle="tab">
                    <span class="hidden-xs">User Permissions</span>
                </a>
            </li>
        @endcan

    </ul>


    <div class="tab-content">
        <?php $active = true ? "active " : ""; ?>

        @can('access-module-component-functionality','users_permissions_role')
            <div class="tab-pane fade {{$active}} in" id="default-tab-1">

                @include('users._role_permissions')
            </div>
        @endcan

            <?php $active = false ?>

        @can('access-module-component-functionality','users_permissions_user')
            <div class="tab-pane fade {{$active}} in" id="default-tab-2">

                @include('users._user_permission')
            </div>
        @endcan
    </div>
@endsection