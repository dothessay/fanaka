@component('layouts.partials.panel')
    @slot('class','default')
    <div class="table-responsive">
        <table class="table table-bordered table-striped" id="data-table">
            <thead>
            <tr>
                <th>User</th>
                <th>Description</th>
                <th>Action</th>
            </tr>
            </thead>
            <tbody>
            @foreach($users as $user)
                <tr>
                    <td>{{$user->fullName()}}</td>
                    <td>{{$user->role->label}}</td>
                    <td>

                        @component('layouts.button')

                            <li>
                                <a href="{{url('users/permissions/configure-user/'.$user->id)}}">Configure</a>
                            </li>
                        @endcomponent

                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>

@endcomponent