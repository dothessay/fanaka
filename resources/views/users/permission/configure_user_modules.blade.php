@extends('layouts.master')
@section('title','configure role module permissions for '. $user->fullName())
@section('content')



    @component('layouts.partials.panel')


        <table class="table table-responsive table-striped table-bordered table-bordered" width="100%">
            <thead>
            <tr>
                <th>Module</th>
                <th>Configure</th>
                <th>Components</th>
            </tr>
            </thead>
            <tbody>
            @foreach($modules  as $module)

                <tr>
                    <td>{{$module->label}}</td>
                    <td><input type="checkbox" data-render="switchery" id="module" data-theme="default" name="module_id" class="{{$module->id}}" value="{{$module->name}}"
                               @if(\Codex\Permission\Permissions::checkAccessToModule
                                               ($user,$module->name))
                               checked="checked" @endif
                        ></td>
                    <td><a href="{{url('users/permissions/configure-user-components/'.$user->id.'?module='.$module->id)}}">components</a> </td>
                </tr>

            @endforeach
            </tbody>
        </table>

    @endcomponent

@endsection

@section('extra_js_footer')
    @include('layouts.form')
    <script>
        FormSliderSwitcher.init();
        $(function() {

            $('input[type=checkbox]#module').change(function () {

                var url = "{{url('users/permissions/configure-user-module/'.$user->id)}}";
                $user_permission_id = ($(this).attr('class'));
                $module = ($(this).attr('value'));
                $user_permission_type = "module";
                $user_id = "{{$user->id}}"

                var data = {
                    'user_permission_id': $user_permission_id,
                    'user_permission_type': $user_permission_type,
                    'module': $module,
                    'role_id': $user_id
                }
                console.info(data);
                $.ajax({
                    url: url,
                    method: 'GET',
                    data: data,
                    success: function (data) {
                        console.log(data)
                        if (data == 'created') {
                            $.growl.notice({
                                message: 'Permission Granted'
                            })
                        } else {
                            $.growl.warning({
                                message: 'Permission Denied'
                            })
                        }
                        console.info(data);
                    }, error: function (err) {
                        console.error(err);
                    }
                })

            });

        })
    </script>


@endsection