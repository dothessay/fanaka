@extends('layouts.master')
@section('title','configure user module permissions for '. $module->name)
@section('content')



    @component('layouts.partials.panel')


        <table class="table table-responsive table-striped table-bordered table-bordered" width="100%">
            <thead>
            <tr>
                <th>Component</th>
                <th>Configure</th>
                <th>Fuctionality</th>
            </tr>
            </thead>
            <tbody>
            @foreach($components  as $component)

                <tr>
                    <td>{{$component->label}}</td>
                    <td><input type="checkbox" data-render="switchery" id="module" data-theme="default" name="module_id" class="{{$component->id}}" value="{{$component->name}}"
                               @if(\Codex\Permission\Permissions::checkAccessToModuleComponent
                                               ($user,$component->name))
                               checked="checked" @endif></td>
                    <td><a href="{{url('users/permissions/configure-user-functionality/'.$user->id.'?component='.$component->id)}}">functionality</a> </td>
                </tr>

            @endforeach
            </tbody>
        </table>

    @endcomponent

@endsection

@section('extra_js_footer')
    @include('layouts.form')
    <script>
        FormSliderSwitcher.init();
        $(function() {

            $('input[type=checkbox]#module').change(function () {

                var url = "{{url('users/permissions/store-configure-user-components')}}";
                $user_permission_id = ($(this).attr('class'));
                $component = ($(this).attr('value'));
                $user_permission_type = "component";
                $user_id = "{{$user->id}}"

                var data = {
                    'user_permission_id': $user_permission_id,
                    'user_permission_type': $user_permission_type,
                    'component': $component,
                    'user_id': $user_id
                }
                console.info(data);
                $.ajax({
                    url: url,
                    method: 'GET',
                    data: data,
                    success: function (data) {
                        console.log(data)
                        if (data == 'created') {
                            $.growl.notice({
                                message: 'Permission Granted'
                            })
                        } else {
                            $.growl.warning({
                                message: 'Permission Denied'
                            })
                        }
                        console.info(data);
                    }, error: function (err) {
                        console.error(err);
                    }
                })

            });

        })
    </script>


@endsection