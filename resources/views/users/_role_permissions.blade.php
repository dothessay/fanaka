@component('layouts.partials.panel')
    @slot('class','default')
    <div class="table-responsive">
        <table class="table table-bordered table-striped" id="data-table">
            <thead>
            <tr>
                <th>Role</th>
                <th>Description</th>
                <th>Action</th>
            </tr>
            </thead>
            <tbody>
            @foreach($roles as $role)
                <tr>
                    <td>{{$role->name}}</td>
                    <td>{{$role->description}}</td>
                    <td>

                        @component('layouts.button')

                            <li>
                                <a href="{{url('users/permissions/configure-role/'.$role->id)}}">Configure</a>
                            </li>
                        @endcomponent

                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>

@endcomponent