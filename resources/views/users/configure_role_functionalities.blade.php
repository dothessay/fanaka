@extends('layouts.master')
@section('title','configure role module permissions for '. $component->name)
@section('content')


    @component('layouts.partials.panel')


        <table class="table table-responsive table-striped table-bordered table-bordered" width="100%">
            <thead>
            <tr>
                <th>Functionality</th>
                <th>Configure</th>
            </tr>
            </thead>
            <tbody>
            @foreach($functionalities  as $functionility)

                <tr>
                    <td>{{ ucwords(strtolower(str_replace('-',' ', $functionility->label) ))}}</td>
                    <td><input type="checkbox" data-render="switchery" id="module" data-theme="default" name="module_id" class="{{$functionility->id}}" value="{{$functionility->name}}"
                               @if($role->canAccessFunctionalities($functionility->name)) checked @endif>
                    </td>
                    </tr>

            @endforeach
            </tbody>
        </table>

    @endcomponent

@endsection

@section('extra_js_footer')
    @include('layouts.form')
    <script>
        FormSliderSwitcher.init();
        $(function() {

            $('input[type=checkbox]#module').change(function () {

                var url = "{{url('users/permissions/store-configure-role-functionality')}}";
                $role_permission_id = ($(this).attr('class'));
                $functionility = ($(this).attr('value'));
                $role_permission_type = "functionality";
                $role_id = "{{$role->id}}"

                var data = {
                    'role_permission_id': $role_permission_id,
                    'role_permission_type': $role_permission_type,
                    'functionality': $functionility,
                    'role_id': $role_id
                }
                console.info(data);
                $.ajax({
                    url: url,
                    method: 'GET',
                    data: data,
                    success: function (data) {
                        console.log(data)
                        if (data == 'created') {
                            $.growl.notice({
                                message: 'Permission Granted'
                            })
                        } else {
                            $.growl.warning({
                                message: 'Permission Denied'
                            })
                        }
                        console.info(data);
                    }, error: function (err) {
                        console.error(err);
                    }
                })

            });

        })
    </script>


@endsection
