@extends('layouts.master')
@section('title','Manage Role')
@section('content')

    @can('access-module-component-functionality','users_role_add')

        <ul class="nav nav-pills">
            <li class="active">
                <a data-toggle="modal" data-target="#addRole">Add Role</a>
            </li>
        </ul>
    @endcan

    @component('layouts.partials.panel')

        <div class="table-responsive">
            <table class="table table-bordered table-striped" id="data-table">

                <thead>
                <tr>
                    <th>Role</th>
                    <th>Users</th>
                    <th>Description</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody>
                @foreach($roles as $role)
                    <tr>
                        <td><a href="{{$role->path()}}">{{$role->name}}</a> </td>
                        <td>{{$role->users->count()}} </td>
                        <td>{{$role->description}}</td>
                        <td>
                            @component('layouts.button')
                                <li>
                                    <a id="edit" data-toggle="modal" data-target="#editRole" data-id='{{$role->id}}' data-name='{{$role->name}}' data-description='{{$role->description}}'>Update</a>
                                    <a id="delete" data-toggle="modal" data-target="#deleteRole" data-id='{{$role->id}}'>Delete</a>
                                </li>
                            @endcomponent
                        </td>
                    </tr>

                @endforeach
                </tbody>

            </table>
        </div>

    @endcomponent



    @component('layouts.partials.modal')
        @slot('id', 'addRole')
        @slot('title') <h6>Fill the form below add the new role</h6> @endslot

        <form class="form-horizontal form-bordered" method="post" action="{{route('users_role_store')}}">
            {{csrf_field()}}
            <div class="form-group">
                <label class="control-label col-md-3">Name</label>
                <div class="col-md-6">
                    <input type="text" class="form-control" name="name">
                </div>
            </div>

            <div class="form-group">
                <label class="control-label col-md-3">Description</label>
                <div class="col-md-6">
                    <input type="text" class="form-control" name="description">
                </div>
            </div>

            <button type="submit" class="btn-sm btn-primary btn">Submit</button>

        </form>

    @endcomponent
    
    @component('layouts.partials.modal')
        @slot('id', 'editRole')
        @slot('title') <h6>Edit role</h6> @endslot

        <form id="formEdit" class="form-horizontal form-bordered" method="post" action="{{ route('users_role_update') }}">
            {{csrf_field()}}
            <div class="form-group">
                <label class="control-label col-md-3">Name</label>
                <div class="col-md-6">
                    <input type="text" class="form-control" name="name" id="name">
                </div>
            </div>

            <div class="form-group">
                <label class="control-label col-md-3">Description</label>
                <div class="col-md-6">
                    <input type="text" class="form-control" name="description" id="description">
                </div>
            </div>
            <input type="hidden" name="id" id="id">
            <button type="submit" class="btn-sm btn-primary btn">Edit</button>

        </form>

    @endcomponent
    
    @component('layouts.partials.modal')
        @slot('id','deleteRole')
        @slot('title')
            <h6>Delete Role</h6>
        @endslot
        
        <h5>ARE YOU YOU WANT TO DELETE THIS ROLE? </h5>
        
        <form method="post" action="{{ route('users_role_delete') }}">
            {{csrf_field()}}
            <input type="hidden" id="role_id" name="role_id">
            <div class="modal-footer">
                <button type="submit" class="btn btn-danger">Delete</button>
            </div>
        </form>
    @endcomponent

@endsection


@section('extra_js_footer')
    @include('layouts.dataTables.datatable')
    <script>
        $("#data-table").DataTable({
            sort:true
        });
        
        $(function(){
            $('#data-table').on('click', '#edit', function () {
                $("#formEdit #name").val($(this).data('name'));
                $("#formEdit #description").val($(this).data('description'));
                $("#formEdit #id").val($(this).data('id'));
            });
            
            //delete expense 
            $('#data-table').on('click', '#delete', function(){
                var roleId = $(this).data('id');
                
                $('#role_id').val(roleId);
            });
        });
    </script>

@endsection