@extends('layouts.master')


@section('title',$user->fullName() .' Full Details')



@section('content')

    @include('layouts.back')

    <ul class="nav nav-tabs tabs-uzapoint">


        <li class="active">
            <a href="#default-tab-1" data-toggle="tab">
                <span class="">List Sales</span>
                <span class="badge badge-danger"><sup>{{$user->sales->count()}}</sup></span>

            </a>
        </li>


        <li class=" ">
            <a href="#default-tab-2" data-toggle="tab">
                <span class="">Reservations</span>
                <span class="badge badge-danger"><sup>{{$user->offers->where('is_converted', false)->count()}}</sup></span>

            </a>
        </li>

        <li class="">
            <a href="#default-tab-3" data-toggle="tab">
                <span class="">Customers</span>
                <span class="badge badge-danger"><sup>{{$user->customers()->count()}}</sup></span>

            </a>
        </li>

        <li class="">
            <a href="#default-tab-4" data-toggle="tab">
                <span class="">Referrals</span>
                <span class="badge badge-danger"><sup>{{$user->referrals->count()}}</sup></span>

            </a>
        </li>

        <li class="">
            <a href="#default-tab-5" data-toggle="tab">
                <span class="">Leaves</span>
                <span class="badge badge-danger"><sup>{{$user->getLeaveRequests()->count()}}</sup></span>

            </a>
        </li>
        <li class="">
            <a href="#default-tab-6" data-toggle="tab">
                <span class="">Handled Leaves</span>
                <span class="badge badge-danger"><sup>{{$user->getHandlingRequests()->count()}}</sup></span>

            </a>
        </li>
        <li class="">
            <a href="#default-tab-7" data-toggle="tab">
                <span class="">Logs</span>
                <span class="badge badge-danger"><sup>{{$user->logs->count()}}</sup></span>

            </a>
        @if($members->count())
            <li class="">
                <a href="#default-tab-8" data-toggle="tab">
                    <span class="">See Sales for</span>
                    <span class="badge badge-danger"><sup>{{ $members->count() }}</sup></span>
                </a>
            </li>
        @endif


    </ul>


    <div class="tab-content">

        <div class="tab-pane fade active in" id="default-tab-1">

            @include('sales._list_table', ['sales' => $user->sales])

        </div>


        <div class="tab-pane fade  in" id="default-tab-2">


            @include('offer._table', ['offers' => $user->offers])


        </div>

        <div class="tab-pane fade  in" id="default-tab-3">


            @include('customers._table', ['customers' => $user->customers()])

        </div>

        <div class="tab-pane fade  in" id="default-tab-4">


            @include('customers.referrals._data', ['referrals' => $user->referrals->map(function ($referral) { return $referral->referralToArray() ; })])


        </div>

        <div class="tab-pane fade  in" id="default-tab-5">


            @include('users.leave.request.index',['leaves' => $user->getLeaveRequests()])


        </div>

        <div class="tab-pane fade  in" id="default-tab-6">


            @include('users.leave.request.index',['leaves' => $user->getHandlingRequests()])


        </div>

        <div class="tab-pane fade  in" id="default-tab-7">


            @include('users.details._logs',['logs' => $user->logs])


        </div>
        @can('access-module-component-functionality', 'users_manage_add-other-sales')
            <div class="tab-pane fade  in" id="default-tab-8">


                @include('users.details._member_of_department')

            </div>
        @endcan
    </div>



@endsection

@section('extra_js_footer')
    @include('layouts.dataTables.datatable')
    <script>


        $('table.customer-table tr').find('td:eq(11),th:eq(11)').remove();

        $('table.customer-table tr').find('td:eq(0),th:eq(0)').remove();
        $('table.customer-table tr').find('td:eq(1),th:eq(1)').remove();
        $('table.customer-table tr').find('td:eq(5),th:eq(5)').remove();
        $('table.customer-table tr').find('td:eq(0),th:eq(0)').remove();


        $('table#offer-table tr').find('td:eq(8),th:eq(8)').remove();


        /****
         * $('table.sales-table tr').find('td:eq(4),th:eq(4)').remove();
        $('table.sales-table tr').find('td:eq(3),th:eq(3)').remove();
        $('table.sales-table tr').find('td:eq(2),th:eq(2)').remove();
        $('table.sales-table tr').find('td:eq(8),th:eq(8)').remove();
        $('table.sales-table tr').find('td:eq(7),th:eq(7)').remove();
        $('table.sales-table tr').find('td:eq(6),th:eq(6)').remove();*/

        $(document).on('click', 'a.view-document', function () {

            let source = $(this).data("url")


            $("div.document-details").empty();

            let filename = $(this).data('file-name');

            let pieces = filename.split(".");

            if (pieces[1] == 'pdf') {


                $("div.document-details").append('<iframe src="' + source + '" style="width:718px; height:700px;" frameborder="0"></iframe>');

                return;
            }


            $("div.document-details").append("<img src='" + source + "' class='img-responsive thumbnail'>");


        });
    </script>

@endsection

