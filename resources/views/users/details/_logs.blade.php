<div class="table-responsive">
    <table class="table-bordered table-striped table">
        <thead>
        <tr>
            <th>User</th>
            <th>Login At</th>
            <th>Logout Out</th>
            <th>Ip</th>
            <th>Machine</th>
            <th>Date and Time</th>
        </tr>
        </thead>
        <tbody>
        @foreach($logs as $log)
            <?php $data = $log->getData(); ?>
            <tr>
                <td><a href="{{$log->user->path()}}">{!! $log->user->fullName() !!}</a></td>
                <td>{{isset($data['checked_in_at']) ? $data['checked_in_at']: null}}</td>
                <td>{{isset($data['checked_out_at']) ? $data['checked_out_at']: null}}</td>
                <td>{{isset($log->ip) ? $log->ip : null}}</td>
                <td>{{isset($log->machine) ? $log->machine : null}}</td>
                <td>{{$log->created_at}}</td>
            </tr>
        @endforeach

        </tbody>
    </table>
</div>
