<div class="p-20">
    <!-- begin row -->
    <div class="row">
        <!-- begin col-2 -->
        <div class="col-md-3">
            <div class="">
                <h5 class="m-t-20">Projects</h5>
                <ul class="nav nav-pills nav-stacked nav-inbox">

                    @foreach($members as $member)
                    <li class="active">
                        <a href="{{ route('users_manage_details', ['user' => $member->id]) }}"><i
                                    class="fa fa-inbox fa-fw m-r-5"></i>{{ $member->fullName() }} </a></li>

                        @endforeach
                </ul>
            </div>
        </div>
        <!-- end col-2 -->
        <!-- begin col-10 -->
        <div class="col-md-9">
            @if(isset($_GET['project']))
                <div class="email-btn-row hidden-xs">
                    <a href="{{url()->current().'?action=create'. (isset($_GET['project']) ? '&project='. $_GET['project'] : '')}}"
                       class="btn btn-sm btn-inverse"><i class="fa fa-plus m-r-5"></i> New</a>
                </div>
            @endif
            <div class="email-content">

                <div class="table-responsive">
                    <table class="table table-email" id="my-inbox-email">
                        <thead>
                        <tr>
                            <th>Purpose</th>
                            <th>Amount</th>
                            <th>Date</th>
                            <th>Download</th>
                        </tr>
                        </thead>
                        <tbody id="append-emails-here">

                        </tbody>

                    </table>
                </div>
                <div class="email-footer clearfix">


                </div>


            </div>
        </div>
        <!-- end col-10 -->
    </div>
    <!-- end row -->
</div>