@extends('layouts.master')
@section('title','New Leave Type')
@section('content')

    @component('layouts.partials.panel')

        @include('users.leave.types._form', ['type' => $type])

    @endcomponent
@stop