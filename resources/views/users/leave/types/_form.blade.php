<form
        method="post"
        action="{{route('users_leave_type_store')}}"
    class="form-horizontal form-bordered">
    {{csrf_field()}}


    <input
            type="hidden"
            value="{{$type->id}}"
            name="id"
    >
    <div class="form-group">
        <label class="control-label col-md-2">Name</label>
        <div class="col-md-4">
            <input
                    type="text"
                    name="name"
                    value="{{$type->name}}"
                    class="form-control"
            >
        </div>


        <label class="control-label col-md-2">Is this type paid</label>
        <div class="col-md-4">
            <input
                    type="checkbox"
                    name="is_paid"
                    {{$type->is_paid ? "checked" : "" }}
                    class="form-check"
            >
        </div>


    </div>

    <div class="form-group">
        <button type="submit" class="btn btn-success" style="margin-bottom: 30px ; margin-left: 20px">{{isset($type->id) ?  "Update" : "Submit"}}</button>
    </div>

</form>