
<a href="{{route('users_leave_type')}}" class="btn btn-success" style="margin-bottom: 8px; margin-left: 2px">New Type</a>


<div class="table-responsive">
    <table class="table table-striped table-bordered table-active" id="leave-type-table">
        <thead>
        <tr>
            <th>Name</th>
            <th>Paid</th>
            <th>Actions</th>
        </tr>
        </thead>
        <tbody>
        @foreach($types as $type)
            <?php

                    $paidClass = "success";
                    if (! $type->is_paid){
                        $paidClass = "warning";
                    }
            ?>
            <tr>
                <td>{{$type->name}}</td>
                <td><span class="badge badge-{{$paidClass}}">{{$type->is_paid ? "Yes" : "Not"}}</span></td>
                <td>
                    @component('layouts.button')
                    <li>
                        <a href="{{route('users_leave_type_find', ['type' => $type->id])}}">Edit</a>
                        <a href="{{route('users_leave_type_delete', ['type' => $type->id])}}">Delete</a>

                    </li>
                    @endcomponent
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>