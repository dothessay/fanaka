<div class="panel panel-default">
    <div class="panel-heading"></div>
    <div class="panel panel-body">
        {!!
            \Codex\Classes\Handlers\Widget::build()
            ->title("Accepted Leave Request")
            ->value($leaves->where('status','accepted')->count())
            ->uri(route('users_leave_requests_report_accepted',['status' => 'accepted']))
            ->output()
             !!}

        {!!
        \Codex\Classes\Handlers\Widget::build()
        ->title("Rejected Leave Request")
        ->value($leaves->where('status','rejected')->count())
        ->uri(route('users_leave_requests_report_rejected', ['status' => 'rejected']))
        ->output()
         !!}

        {!!
        \Codex\Classes\Handlers\Widget::build()
        ->title("Pending Leave Request")
        ->value($leaves->where('status','pending')->count())
        ->uri(route('users_leave_requests_report_pending' , ['status' => 'pending']))
        ->output()
         !!}

        {!!
        \Codex\Classes\Handlers\Widget::build()
        ->title("Leaves Remaining")
        ->value(auth()->user()->getLeaveBalance()." This Year")
        ->uri(route('users_leave_requests_report_remaining',['status' => 'balance']))
        ->output()
         !!}
    </div>
</div>