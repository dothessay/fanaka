<div class="table-responsive">
    <table class="table table-striped table-bordered">
        <thead>
        <tr>
            <th>User On Leave</th>
            <th>User Handling</th>
            <th>Last Day of Work</th>
            <th>Return At</th>
            <th>Days Of</th>
            <th>Return Days Of</th>
            <th>Leave Balance</th>
        </tr>
        </thead>
        <tbody>
        @foreach($leaves as $leave)

            <tr>
                <?php $color = $leave->statusColor(); $style = "{$color}"; ?>
                <td><a
                            href="{{$leave->user->path()}}">{{$leave->user->fullName()}}</a>
                </td>

                <td>
                    <a href="{{isset($leave->handleClient) && ! is_null($leave->handleClient) ? $leave->handleClient->path():""}}">{{ isset($leave->handleClient) && ! is_null($leave->handleClient) ? $leave->handleClient->fullName() : null}}</a>
                </td>
                <td>{{$leave->last_day}}</td>
                <td>{{$leave->return_at}}</td>
                <td>{{$leave->days_off}}</td>
                <td><span class="{{$style}}">{{$leave->status}}</span></td>
                <td>{{$leave->user->getLeaveBalance() . ' Days'}}</td>
            </tr>

        @endforeach
        </tbody>
    </table>
</div>