@extends('layouts.master')
@section('title','manage leave report')
@section('content')
    <style>
        a.badge {
            margin-bottom: 7px;
            padding: 12px;
        }
    </style>

    <a href="{{route('users_leave')}}" class="badge badge-warning">Back</a>

    @component('layouts.partials.panel')


        @can('access-module-component-functionality','users_leave_view-requests')
            <form class="form-horizontal form-bordered">

                <input type="hidden" value="{{$_GET['status']}}" name="status">
                <div class="form-group">
                    <label class="col-md-2 control-label">Users On Leave</label>
                    <div class="col-md-4">
                        <select
                                name="user_id"
                                class="form-control selectpicker"
                                data-live-search="true"
                                data-size="5"
                        >
                            <option
                                    value="">Select A User
                            </option>
                            @foreach($users as $user)

                                <option
                                        @if (isset($_GET['user_id']) && $_GET['user_id'] == $user->id)
                                        selected
                                        @endif
                                        value="{{$user->id}}">
                                    {{$user->fullName()}}
                                </option>


                            @endforeach
                        </select>
                    </div>



                    @if ($_GET['status'] != 'balance')
                    <label class="col-md-2 control-label">Handling</label>
                    <div class="col-md-4">
                        <select
                                name="handling_user_id"
                                class="form-control selectpicker"
                                data-live-search="true"
                                data-size="5"
                        >
                            <option

                                    value="">Select A User Handling
                            </option>
                            @foreach($users as $user)

                                <option
                                        @if (isset($_GET['handling_user_id']) && $_GET['handling_user_id'] == $user->id)
                                        selected
                                        @endif
                                        value="{{$user->id}}">
                                    {{$user->fullName()}}
                                </option>


                            @endforeach
                        </select>
                    </div>
                        @endif

                </div>

                <div class="modal-footer">
                    <button type="submit" class="btn btn-success">Pull</button>
                </div>
            </form>
        @endcan

        @if ($_GET['status'] == 'balance')

            @include('users.leave.request.report._report_balance', ['data' => []])

            @else


            @include('users.leave.request.report._report_data')


        @endif


    @endcomponent

@stop

@section('extra_js_footer')

    @include('layouts.dataTables.datatable')
    <script>
        $("table").DataTable();

    </script>
@stop