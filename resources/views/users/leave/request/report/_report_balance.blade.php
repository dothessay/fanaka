<table class="table table-bordered table-striped" id="data-table">
    <thead>
    <tr>
        <th>First Name</th>
        <th>Last Name</th>
        <th>Role</th>
        <th>Email</th>
        <th>Phone number</th>
        <th>Leave Balance</th>

    </tr>
    </thead>
    <tbody>
    @foreach($employees as $user)
        <tr>
            <?php $uri = $user->path(); ?>
            <td><a href="{{$uri}}">{{$user->first_name}}</a> </td>
            <td><a href="{{$uri}}">{{$user->last_name}}</a></td>
            <td><a href="{{$user->role->path()}}">{{$user->role->label}}</a></td>
            <td><a href="{{$uri}}">{{$user->email}}</a></td>
            <td><a href="{{$uri}}">{{$user->phone_number}}</a></td>
            <td><a href="{{$uri}}">{{$user->getLeaveBalance()}}</a></td>
        </tr>

    @endforeach
    </tbody>
</table>