

<div class="table-responsive">
    <table class="table table-bordered table-striped nowrap" id="handle-table">
        <thead>
        <tr>
            <th>User ON leave</th>
            <th>Handled by</th>
            <th>Clients Handled by</th>
            <th>Start Date</th>
            <th>End Date</th>
            <th>Total Days Off</th>
            <th>Action</th>
        </tr>
        </thead>
        <tbody>
        @foreach($leaves as $leave)
            <tr>
                <td><a href="{{route('users_manage_details', ['user' => $leave->user->id])}}">{{$leave->user->fullName()}}</a></td>
                <td><a href="{{route('users_manage_details', ['user' => $leave->handling->id])}}">{{$leave->handling->fullName()}}</a></td>
                <td>{{ isset($leave->handleClient) && ! is_null($leave->handleClient) ? $leave->handleClient->fullName() : null}}</td>
                <td>{{$leave->last_day}}</td>
                <td>{{$leave->return_at}}</td>
                <td>{{$leave->days_off}}</td>
                <td>
                    @component('layouts.button')
                        <li>
                            <a href="{{route('users_leave_request_accept', ['leave' => $leave->id])}}">Accept</a>
                            <a href="{{route('users_leave_request_reject', ['leave' => $leave->id])}}">Reject</a>

                        </li>
                    @endcomponent
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>