@extends('layouts.master')
@section('title','New Leave Request')

@section('extra_css_header')
    <style>
        fieldset legend h3 {
            color: #46bef1;
        }

        button {
            margin-left: 12em;
            margin-bottom: 3em;
            width: 9em;
        }

        p {
            margin-left: 2em;
        }

        textarea {
            margin-left: 1em;
        }

        #leave-form {
            display: grid;
            grid-template-columns: 50% 50%;
            grid-gap: 1em;

        }
        .panel-info {
            box-shadow: 0 5px 8px rgba(0, 0, 0, .3);


        }
    </style>
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
@endsection
@section('content')

    <fieldset>
    <legend><h3>{{__('Leave Balance ')}}<span class="balance">{{__(' '. auth()->user()->getLeaveBalance() .' ')}}</span> {{__(' Days')}} </h3></legend>
    </fieldset>


    <form
            method="post"
            action="{{route('users_leave_request_store')}}"
            class="form-horizontal"

    >
        {{csrf_field()}}

        <input
                type="hidden"
                name="id"
                value="{{$leave->id}}"
        >
        <input
                type="hidden"
                name="user_id"
                value="{{$leave->user_id}}"
        >

        <div id="leave-form">

            <div class="leave-item">

                @component('layouts.partials.panel')
                    <fieldset>
                        <legend><h3>{{__('Employer Details')}}</h3></legend>
                        <div class="form-group">
                            <label for="" class="control-label col-md-2">Name: </label>
                            <div class="col-md-10">
                                {{__(auth()->user()->fullName())}}
                            </div>

                        </div>

                    </fieldset>
                    <fieldset>
                        <legend><h3>{{__('Leave type')}}</h3></legend>
                        <div class="form-group">
                            <label for="" class="control-label col-md-2">{{__('Leave type')}}</label>
                            <div class="col-md-10">
                                <select
                                        name="leave_type_id"
                                        class="form-control selectpicker"
                                        data-live-search="true"
                                        data-size="5"

                                >
                                    @foreach($types as $type)
                                        <option
                                                value="{{$type->id}}"
                                                @if ($type->id  == $leave->leave_type_id)
                                                selected
                                                @endif
                                        >{{$type->name}} ({{$type->is_paid ? "Full pay" : "not paid"}})
                                        </option>
                                    @endforeach

                                </select>
                            </div>
                        </div>
                    </fieldset>
                    <fieldset>
                        <legend><h3>{{__('Period of leave ')}}</h3></legend>
                        <div class="form-group">
                            <label for="" class="control-label col-md-2">{{__('Last day of work')}}</label>
                            <div class="col-md-10">
                                <input
                                        type="text"
                                        name="last_day"
                                        class="form-control last_date"
                                        value="{{$leave->last_day}}"
                                >
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="" class="control-label col-md-2">{{__('Return to work date')}}</label>
                            <div class="col-md-10">
                                <input
                                        type="text"
                                        name="return_at"
                                        class="form-control return_at"
                                        value="{{$leave->return_at}}"
                                >
                            </div>
                        </div>
                        <div class="form-group">
                            <label for=""
                                   class="control-label col-md-2">{{__('Total number of working days off')}}</label>
                            <div class="col-md-10">
                                <input
                                        type="text"
                                        name="days_off"
                                        class="form-control days_off"
                                        value="{{$leave->days_off}}"
                                >
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="" class="control-label col-md-2">{{__('Comments')}}</label>
                            <div class="col-md-10">
                                <textarea name="comments">{!! $leave->comments !!}</textarea>
                            </div>
                        </div>
                    </fieldset>

                @endcomponent
            </div>

            <div class="leave-item">

                @component('layouts.partials.panel')



                    <fieldset>
                        <legend><h3>{{__('Handing Over Employee Details')}}</h3></legend>
                        <input type="hidden" class="min-date" value=" {{now()->format('Y-m-d')}}">

                        <div class="form-group">
                            <label for="" class="control-label col-md-2">Name: </label>
                            <div class="col-md-10">
                                <select
                                        name="handling_user_id"
                                        class="form-control selectpicker"
                                        data-live-search="true"
                                        id="employee_details"
                                        data-size="5"

                                >
                                    @foreach($users as $user)
                                        <option
                                                value="{{$user->id}}"
                                                @if ($user->id  == $leave->handling_user_id)
                                                selected
                                                @endif
                                                data-user_id = "{{$user->id}}"
                                                data-user_name = "{{$user->fullName()}}"
                                        >{{$user->fullName()}} </option>
                                    @endforeach

                                </select>
                            </div>

                        </div>
                    </fieldset>
                    <fieldset>
                        <legend><h3>{{__('Who will Handle you clients?')}}</h3></legend>
                        <div class="form-group">
                            <label for="" class="control-label col-md-2">Name:   </label>
                            <div class="col-md-10">
                                <input type="hidden" name="handle_clients_user_id" class="handle_clients_user_id">
                                <b><span id="handle_clients"></span></b>

                            </div>

                        </div>
                    </fieldset>
                    <fieldset>
                        <legend><h3>{{__('Duties and Responsibilities')}}</h3></legend>
                        <div class="form-group">
                            <label for="" class="control-label col-md-2">{{__('Duties')}}</label>
                            <div class="col-md-10">
                                    <textarea
                                            name="duties"
                                    >{!! $leave->duties !!}</textarea>
                            </div>
                        </div>
                    </fieldset>

                    <div class="form-group">
                        <?php $showSubmit = true ?>

                        @can('access-module_component-functionality','users_leave_approve-handing-request')

                            @if ($leave->handling_user_id == auth()->id() )

                                <?php $showSubmit = false ?>


                                <div class="form-group">
                                    <label for="" class="control-label col-md-2">{{__('Approve')}}</label>
                                    <div class="col-md-10">
                                        <input
                                                type="checkbox"
                                                name="is_approved"
                                                class="form-check-input"
                                        >
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="" class="control-label col-md-2">{{__('Tick to Reject')}}</label>
                                    <div class="col-md-10">
                                        <input
                                                type="checkbox"
                                                name="is_rejected"
                                                class="form-check-input"
                                        >
                                    </div>

                                    <fieldset>
                                        <legend><h3>{{__('Reason For Refusal')}}</h3></legend>
                                        <div class="form-group">
                                            <label for=""
                                                   class="control-label col-md-2">{{__('Reasons for res=fusal')}}</label>
                                            <div class="col-md-10">
                                                        <textarea
                                                                name="refusal_reason"
                                                        >{!! $leave->refusal_reason !!}</textarea>
                                            </div>
                                        </div>
                                    </fieldset>

                                    <button type="submit" class="btn btn-success">Approve Request</button>

                                    @endif

                                    @endcan

                                    @can('access-module_component-functionality','users_leave_approve-request')

                                        @if ( isset($leave->handling_user_id) )

                                            <?php $showSubmit = false ?>

                                            <fieldset>
                                                <div class="form-group">
                                                    <label for=""
                                                           class="control-label col-md-2">{{__('Tick to Approve')}}</label>
                                                    <div class="col-md-10">
                                                        <input
                                                                type="checkbox"
                                                                name="is_approved"
                                                                class="form-check-input"
                                                                value="{{$leave->is_approved}}"
                                                        >
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for=""
                                                           class="control-label col-md-2">{{__('Tick to Reject')}}</label>
                                                    <div class="col-md-10">
                                                        <input
                                                                type="checkbox"
                                                                name="is_rejected"
                                                                class="form-check-input"
                                                                value="{{$leave->is_approved}}"
                                                        >
                                                    </div>
                                                </div>

                                                <legend><h3>{{__('Reason For Refusal')}}</h3></legend>
                                                <div class="form-group">
                                                    <label for=""
                                                           class="control-label col-md-2">{{__('Reasons for res=fusal')}}</label>
                                                    <div class="col-md-10">
                                                        <textarea
                                                                name="refusal_reason"
                                                        >{!! $leave->refusal_reason !!}</textarea>
                                                    </div>
                                                </div>
                                            </fieldset>


                                            <button type="submit" class="btn btn-success">Approve Request</button>

                                        @endif

                                    @endcan

                                    @if ( isset($showSubmit) )


                                        <button type="submit"
                                                class="btn btn-success">{{isset($leave->id) ? "Update" : "Submit"}}</button>

                                    @endif

                                </div>


                                @endcomponent

                    </div>
            </div>
        </div>


    </form>



@stop

@section('extra_js_footer')

    @include('layouts.form')

    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

    <script>
        var today = $("input.min-date").val()



        $("input.last_date , input.return_at").datepicker({

            dateFormat: "yy-mm-dd",
            minDate: new Date(today)
        });


        $(document).on('change', "input.last_date", function () {

            var start= $("input.last_date ").datepicker("getDate");
            var end= $("input.return_at").datepicker("getDate");
            var days = (end- start) / (1000 * 60 * 60 * 24);

            $("input.days_off").val(days)




        })
        $(document).on('change', "input.return_at", function () {

            var start= $("input.last_date ").datepicker("getDate");
            var end= $("input.return_at").datepicker("getDate");
            var days = (end- start) / (1000 * 60 * 60 * 24);

            $("input.days_off").val(days)



        })


        $(document).on('change', 'select#employee_details' ,function () {


            let seleted = $("select#employee_details option:selected")

          $("input.handle_clients_user_id").val(seleted.data('user_id'))
           $("span#handle_clients").text(seleted.data('user_name'))
        })

        $(document).on('input', 'input.days_off' ,function () {

            let days = $(this).val();


            let balance = parseFloat("{{auth()->user()->getLeaveBalance()}}")

            $("fieldset>legend>h3>span.balance").text(balance - parseFloat(days))

        })




    </script>




@stop