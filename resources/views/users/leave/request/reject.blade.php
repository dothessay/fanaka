@extends('layouts.master')
@section('title','Leave a reason why you rejecting')


@section('extra_css_header')
    <style>
        fieldset legend h3 {
            color: #46bef1;
        }
        button{
            margin-left:26em;
            margin-top: 1em;
            width: 10em;
        }
        </style>
@endsection

@section('content')

    @component('layouts.partials.panel')

        <form
                method="post"
                action="{{route('users_leave_request_reject', ['leave' => $leave->id])}}"
        >
            {{csrf_field()}}
            {{method_field('PATCH')}}
            <fieldset>
                <legend><h3>{{__('Why are rejecting to handle '. $leave->user->fullName() . '`s Duties?')}}</h3></legend>
                <div class="form-group">

                    <div class="col-md-12">
                        <textarea name="reason"></textarea>
                    </div>
                </div>

                <div class="form-group">
                    <button type="submit" class="btn btn-success">Submit</button>
                </div>


            </fieldset>

        </form>
    @endcomponent

@stop