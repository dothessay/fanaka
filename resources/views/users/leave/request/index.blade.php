<div class="table-responsive">
    <table class="table table-bordered table-striped nowrap" id="leaves-table">
        <thead>
        <tr>
            <th>User ON leave</th>
            <th>Handled by</th>
            <th>Clients Handled by</th>
            <th>Start Date</th>
            <th>End Date</th>
            <th>Total Days Off</th>
            <th>Status</th>
            <th>Action</th>
        </tr>
        </thead>
        <tbody>
        @foreach($leaves as $leave)
            <tr>
                <?php $color = $leave->statusColor(); $style = "{$color}"; ?>
                <td ><a
                            href="{{route('users_manage_details', ['user' => $leave->user->id])}}">{{$leave->user->fullName()}}</a>
                </td>
                <td><a
                            href="{{route('users_manage_details', ['user' => $leave->handling->id])}}">{{$leave->handling->fullName()}}</a>
                </td>
                <td>{{ isset($leave->handleClient) && ! is_null($leave->handleClient) ? $leave->handleClient->fullName() : null}}</td>
                <td>{{$leave->last_day}}</td>
                <td>{{$leave->return_at}}</td>
                <td>{{$leave->days_off}}</td>
                <td><span class="{{$style}}">{{$leave->status}}</span> </td>
                <td>
                    @component('layouts.button')
                        @if ($leave->status == 'pending')

                            <li>
                                @can('access-module-component-functionality','users_leave_approve-request')
                                    <a href="{{route('users_leave_request_approve', ['leave' => $leave->id])}}">Approve</a>
                                @endcan


                            </li>
                            <li><a href="{{route('users_leave_request_find', ['leave' => $leave->id])}}">Edit</a></li>
                            <li><a href="{{route('users_leave_request_delete', ['leave' => $leave->id])}}">Delete</a>
                            </li>
                        @endif



                    @endcomponent
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>