@extends('layouts.master')
@section('title','Leave management Area')
@section('content')


    <ul class="nav nav-tabs tabs-uzapoint">

        <?php $active = true ?>


        @can('access-module-component-functionality','users_leave_make-request')

            <?php $active = $active ? "active" : "" ?>
            <li class="{{$active}}">
                <a href="#default-tab-1" data-toggle="tab">
                    <span class="">Make Request</span>


                </a>
            </li>
            <?php $active = false ?>
        @endcan

        @can('access-module-component-functionality','users_leave_leave-types')


            <?php $active = $active ? "active" : "" ?>

            <li class="{{$active}}">
                <a href="#default-tab-2" data-toggle="tab">
                    <span class="">Leave Types</span>


                </a>
            </li>
            <?php $active = false ?>
        @endcan





        @can('access-module-component-functionality','users_leave_reports')

            <?php $active = $active ? "active" : "" ?>
            <li class="{{$active}}">
                <a href="#default-tab-3" data-toggle="tab">
                    <span class="">Reports</span>


                </a>
            </li>
            <?php $active = false ?>
        @endcan

            <li class="{{$active}}">
                <a href="#default-tab-4" data-toggle="tab">
                    <span class="">Handle Requests</span>
                    <span class="badge badge-danger"><sup>{{auth()->user()
                ->handlingRequests
                ->where('is_approved', false)
                ->where('status','pending')->count()}}</sup></span>


                </a>
            </li>

    </ul>


    <div class="tab-content">

        <?php $active = true ?>
        @can('access-module-component-functionality','users_leave_make-request')

            <?php $active = $active ? "active" : "" ?>
            <div class="tab-pane fade {{$active}} in" id="default-tab-1">

                <a href="{{route('users_leave_requests')}}" class="btn btn-success" style="margin-bottom: 8px; margin-left: 2px">New Request</a>


                @include('users.leave.request.index',['leaves' => auth()->user()->getLeaveRequests()->where('status', "pending")])

            </div>

            <?php $active = false ?>
        @endcan

        @can('access-module-component-functionality','users_leave_leave-types')


            <?php $active = $active ? "active" : "" ?>
            <div class="tab-pane fade {{$active}} in" id="default-tab-2">
                @include('users.leave.types._index' )
            </div>

            <?php $active = false ?>
        @endcan


        @can('access-module-component-functionality','users_leave_reports')


            <?php $active = $active ? "active" : "" ?>
            <div class="tab-pane fade {{$active}} in" id="default-tab-3">
                @include('users.leave.request._report' , ['leaves' => auth()->user()->getLeaveRequests()])
            </div>

            <?php $active = false ?>
        @endcan


            <div class="tab-pane fade  in" id="default-tab-4">
                @include('users.leave.request._handle' ,
                ['leaves' => auth()->user()
                ->handlingRequests
                ->where('is_approved', false)
                ->where('status','pending')])
            </div>



    </div>

@endsection

@section('extra_js_footer')

    @include('layouts.dataTables.datatable')

    <script>
        $("table#handle-table").DataTable();
    </script>

@stop