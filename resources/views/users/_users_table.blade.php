<table class="table table-bordered table-striped" id="data-table">
    <thead>
    <tr>
        <th>First Name</th>
        <th>Last Name</th>
        <th>Role</th>
        <th>Email</th>
        <th>Phone number</th>
        <th>Action</th>
    </tr>
    </thead>
    <tbody>
    @foreach($users as $user)
        <tr>
            <?php $uri = url('users/manage/' . $user->id . '/details')?>
            <td><a href="{{$uri}}">{{$user->first_name}}</a></td>
            <td><a href="{{$uri}}">{{$user->last_name}}</a></td>
            <td><a href="{{$uri}}">{{$user->role->label}}</a></td>
            <td><a href="{{$uri}}">{{$user->email}}</a></td>
            <td><a href="{{$uri}}">{{$user->phone_number}}</a></td>
            <td>@component('layouts.button')
                    @can('access-module-component-functionality', 'users_manage_update')

                        <li><a data-toggle="modal" id="edit" data-target="#update-user"
                               data-first_name="{{$user->first_name}}" data-last_name="{{$user->last_name}}"
                               data-phone_number="{{$user->phone_number}}" data-email="{{$user->email}}"
                               data-role_id="{{$user->role_id}}" data-id="{{$user->id}}">Update</a>
                        </li>

                        <li>
                            <a href="{{ route('impersonate', ['user' => $user->id]) }}">Impersonate</a>
                        </li>

                    @endcan
                    @can('access-module-component-functionality', 'users_manage_delete')
                        @if ($user->is_active)

                            <li><a data-toggle="modal" data-target="#delete-user" id="del-id" data-id="{{$user->id}}">Deactivate</a>
                            </li>
                        @else

                            <li><a href="{{route('users_manage_activate', ['user'  => $user->id])}}">Activate</a></li>

                        @endif
                    @endcan
                @endcomponent</td>
        </tr>

    @endforeach
    </tbody>
</table>
