@extends('layouts.master')
@section('title','configure role module permissions for '. $role->name)
@section('content')



    @component('layouts.partials.panel')


        <table class="table table-responsive table-striped table-bordered table-bordered" width="100%">
            <thead>
            <tr>
                <th>Module</th>
                <th>Configure</th>
                <th>Components</th>
            </tr>
            </thead>
            <tbody>
            @foreach($modules  as $module)

                <tr>
                    <td>{{ ucwords(strtolower(str_replace('-',' ', $module->label) ))}}</td>
                    <td><input type="checkbox" data-render="switchery" id="module" data-theme="default" name="module_id" class="{{$module->id}}" value="{{$module->name}}"
                        @if($role->canAccessModule($module->name)) checked @endif></td>
                    <td><a href="{{url('users/permissions/configure-role-components/'.$role->id.'?module='.$module->id)}}">components</a> </td>
                </tr>

            @endforeach
            </tbody>
        </table>

    @endcomponent

@endsection

@section('extra_js_footer')
    @include('layouts.form')
    <script>
        FormSliderSwitcher.init();
        $(function() {

            $('input[type=checkbox]#module').change(function () {

                var url = "{{url('users/permissions/configure-role-module/'.$role->id)}}";
                $role_permission_id = ($(this).attr('class'));
                $module = ($(this).attr('value'));
                $role_permission_type = "module";
                $role_id = "{{$role->id}}"

                var data = {
                    'role_permission_id': $role_permission_id,
                    'role_permission_type': $role_permission_type,
                    'module': $module,
                    'role_id': $role_id
                }
                console.info(data);
                $.ajax({
                    url: url,
                    method: 'GET',
                    data: data,
                    success: function (data) {
                        console.log(data)
                        if (data == 'created') {
                            $.growl.notice({
                                message: 'Permission Granted'
                            })
                        } else {
                            $.growl.warning({
                                message: 'Permission Denied'
                            })
                        }
                        console.info(data);
                    }, error: function (err) {
                        console.error(err);
                    }
                })

            });

        })
    </script>


@endsection
