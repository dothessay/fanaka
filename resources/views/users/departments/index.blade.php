@extends('layouts.master')
@section('title','Department')
@section('content')
    <ul class="nav nav-pills">
        <li class="active">
            <a href="{{ route('users_department-head_create') }}" > Create </a>
        </li>
    </ul>

    @component('layouts.partials.panel')

        <div class="table-responsive">
            <table class="table table-striped table-bordered">
                <thead>
                <tr>
                    <th>name</th>
                    <th>Department</th>
                    <th>Member</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody>
                @foreach($departments as $department)
                    <tr>
                        <td>{{ $department->name }}</td>
                        <td> @if( isset($department->role->id)) <a href="{{ route('users_role_details', ['role' => $department->role->id]) }}"> {{ $department->role->name }} </a> @endif </td>
                        <td>{{ $department->user->fullName() }}</td>
                        <td>
                            @component('layouts.button')
                                <li><a href="{{ route('users_department-head_show',['id' => $department->id]) }}"> Edit</a></li>
                            @endcomponent
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>

        </div>
    @endcomponent
@endsection
