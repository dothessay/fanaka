@extends('layouts.master')
@section('title','New Department Head')
@section('content')
    <ul class="nav nav-pills">
        <li class="active">
            <a href="{{ route('users_department-head') }}" > Back </a>
        </li>
    </ul>

    <?php if (! isset($department)) { $department = new \App\DB\Business\DepartmentAdmin() ;}?>
    @component('layouts.partials.panel')
       <form
               class="form-horizontal form-bordered"
               method="post"
               action="{{ isset($department->id) ? route('users_department-head_update',['id' => $department->id]) : route('users_department-head_store') }}"
       >

           @csrf

           <div class="form-group">
               <label class="col-md-2 control-label">Name</label>
               <div class="col-md-4">
                   <input
                           name="name"
                           class="form-control"
                           value="{{ old('name' , $department->name) }}"
                   >
               </div>
           </div>

           <div class="form-group">
               <label class="col-md-2 control-label">Select Member</label>
               <div class="col-md-4">

                   <select
                           name="user_id"
                           class="form-control selectpicker"
                           data-live-search="true"
                   >
                       @foreach($users as $user)
                           <option
                                   @if(isset($department->user->id) && $department->user->id == $user->id) selected @endif
                           value="{{ $user->id }}">
                               {!! $user->fullName() !!}
                           </option>
                       @endforeach
                   </select>

               </div>
           </div>
           <div class="form-group">
               <label class="col-md-2 control-label">Select Role to Assign</label>
               <div class="col-md-4">

                   <select
                           name="role_id"
                           class="form-control selectpicker"
                           data-live-search="true"
                   >
                       @foreach($roles as $role)
                           <option
                                   @if(isset($department->role->id) && $department->role->id == $role->id) selected @endif
                           value="{{ $role->id }}">
                               {!! $role->name !!}
                           </option>
                       @endforeach
                   </select>

               </div>
           </div>

           <div class="email-footer">
               <button class="btn btn-success">Submit</button>
           </div>
       </form>
    @endcomponent
@endsection
@section('extra_js_footer')
    @include('layouts.form')
@endsection
