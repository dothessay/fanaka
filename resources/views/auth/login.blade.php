@extends('layouts.master_auth')

@section('title', 'login')

@section('content')

    <form action="{{ route('login') }}" method="POST" class="margin-bottom-0" autocomplete="off">

        {{ csrf_field() }}

        <div class="form-group m-b-20 {{ $errors->has('email') ? ' has-error' : '' }}">

            <input type="email" name="email" class="form-control input-lg inverse-mode no-border" value="{{old('email')}}" autocomplete="off" placeholder="E-MAIL ADDRESS" required autofocus/>
            @if ($errors->has('email'))
                <span class="help-block">
                    <strong>{{ trans($errors->first('email')) }}</strong>
                </span>
            @endif
        </div>
        <div class="form-group m-b-20 {{ $errors->has('password') ? ' has-error' : '' }}">
            <input type="password" name="password" class="form-control input-lg inverse-mode no-border" {{old('password')}} autocomplete="off" placeholder="PASSWORD" required/>
            @if ($errors->has('password'))
                <span class="help-block">
                    <strong>{{ trans($errors->first('password')) }}</strong>
                </span>
            @endif
        </div>
        <div class="checkbox m-b-20">
            <label>
                <input name="remember" type="checkbox" {{ old('remember') ? 'checked' : '' }} /> Remember Me
            </label>

            <label>
                <a class="form-group" href="{{ route('password.request') }}" style="color:#dee0e2">forgot password </a>
            </label>

        </div>
        <div class="login-buttons">
            <button type="submit" name="login" class="btn btn-success btn-block btn-lg" style="border-color: #0eba8f;background: #0eba8f;">Sign In</button>
        </div>
    </form>




@endsection

@section('extra_js_footer')
@endsection