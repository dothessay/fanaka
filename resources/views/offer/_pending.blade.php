<table class="table table-bordered table-striped nowrap" id="pending-table">
    <thead>
    <tr>
        <th>Date Reserved</th>
        <th>Days Reserved</th>
        <th>Offer Reference</th>
        <th>Payment Option</th>
        <th>Customer</th>
        <th>Plots</th>
        <th>Cash Option</th>
        <th>Installment Option</th>
        <th>Actions</th>

    </tr>
    </thead>

    <tbody>
    @foreach($offers->where('is_approved', false) as $offer)
        <tr>
            <td>{{$offer->created_at->format('Y-m-d H:i')}}</td>
            <td> {!! $offer->getMeta('reservation_days', fanakaConfig('reservation_days', 7)) !!} </td>
            <td><a href="{{route('offer_generate_approve' ,['offer' => $offer->id])}}">FRV/{{$offer->id}}</a> </td>
            <td>{{$offer->payment_option}}</td>
            <td>{!! $offer->getCustomerHref() !!}</td>
            <td>{{$offer->getCustomerPlots()}}</td>
            <td>{{$offer->cash_option}}</td>
            <td>{{$offer->installment_option}}</td>
            <td>
                @component('layouts.button')
                    <li>
                        <a data-toggle="modal"
                           data-id="{{$offer->id}}"
                           class="update-pending-reservation-payment"
                           data-target="#addPayment">Add Payment</a>
                    </li>

                    @can('access-module-component-functionality','offer_generate_receipt')
                        <li>
                            <a target="_blank" href="{{route('offer_generate_receipt' ,['offer_id' => $offer->id])}}">Receipt</a>
                        </li>

                    @endcan

                 @endcomponent
            </td>


        </tr>
    @endforeach
    </tbody>

</table>

@component('layouts.partials.modal')
    @slot('id','addPayment')
    @slot('modalClass','modal-lg')

    @slot('title')
        <h3>Fill the form below to update payment</h3>
    @endslot

    <form class="form-bordered form-horizontal" method="post" action="{{route('offer_generate_update-reservation-payment')}}">
        {{csrf_field()}}

        <input type="hidden" name="id" class="reservation-id">

        <div class="form-group">

            <label class="control-label col-md-2">Payment Method</label>
            <div class="col-md-4">

                @foreach(\Codex\Classes\Helper::system()->paymentMethods() as $paymentMethod)

                    <input type="radio" name="payment_method" class="method" value="{{$paymentMethod}}">{{$paymentMethod}}
                @endforeach

            </div>


            <label class="control-label col-md-2">Amount Paid</label>
            <div class="col-md-4">
                <input type="text" name="amount" class="form-control gtotal">
            </div>

        </div>

        <div class="form-group">
            <label class="control-label col-md-2">Deposit Date</label>
            <div class="col-md-3">
                <input type="text" name="deposit_date" class="form-control datepicker" id="">
            </div>


        </div>







        <div class="mpesa"  style="display: none">
            <div class="form-group">

                <label class="control-label col-md-2">Reference code  <span class="required-uzapoint">*</span></label>
                <div class="col-md-4">
                    <input type="text" name="mpesa_reference_code" class="form-control">
                </div>
            </div>
        </div>



        <div class="bank" style="display: none">

            <div class="form-group">

                <label class="control-label col-md-2">Bank Name <span class="required-uzapoint">*</span> </label>
                <div class="col-md-4">
                    <input type="text" name="bank_name"  value="{{config('config.equity_bank.name')}}" class="form-control">
                </div>

                <label class="control-label col-md-2">Account No  <span class="required-uzapoint">*</span></label>
                <div class="col-md-4">
                    <input type="text" name="account_no"  value="{{config('config.equity_bank.number')}}"  class="form-control">
                </div>
            </div>
            <div class="form-group">


                <label class="control-label col-md-2">Slip NO  <span class="required-uzapoint">*</span></label>
                <div class="col-md-4">
                    <input type="text" name="slip_no" class="form-control">
                </div>

            </div>
        </div>

        <div class="cheque"  style="display: none">

            <div class="form-group">
                <label class="control-label col-md-2">Bank Name  <span class="required-uzapoint">*</span></label>
                <div class="col-md-4">
                    <input type="text" name="check_bank_name" class="form-control">
                </div>

                <label class="control-label col-md-2">Cheque No  <span class="required-uzapoint">*</span></label>
                <div class="col-md-4">
                    <input type="text" name="cheque_no" class="form-control">
                </div>


                <label class="control-label col-md-2">Drawer  <span class="required-uzapoint">*</span></label>
                <div class="col-md-4">
                    <input type="text" name="drawer" class="form-control">
                </div>

            </div>


        </div>


        <div class="modal-footer">
            <button type="button" class="btn btn-success updatePendingReservation">Update Reservation Payment</button>
            <button type='submit' class="hidden submit-reservation-pending-payment"></button>
        </div>


    </form>



@endcomponent
