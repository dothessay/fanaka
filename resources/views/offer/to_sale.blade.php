@extends('layouts.master')

@section('title')Convert Reservation of {!! $offer->getCustomerHref() !!}  to  sale @endsection

@section('extra_css_header')
    <style>
        .modal-dialog {
            width: 100%;
            height: 100%;
            margin: 0;
            padding: 0;
        }

        .modal-content {
            height: auto;
            min-height: 100%;
            border-radius: 0;
        }
    </style>

    @endsection

@section('content')

    @include('layouts.back')

    @component('layouts.partials.panel')

        <div class="modal-footer">
            <button type="button" class="btn btn-primary addPayment"
                    data-toggle="collapse"
                    data-target="#add-sale-payment"
                    aria-expanded="false"
                    aria-controls="collapseExample">Convert To Sale
            </button>
        </div>

       <div id="add-sale-payment" class="collapse">
           @include('offer._converting_sale_payment')
       </div>


        <div class="table-responsive">
            <table class="table-bordered table-striped table" id="data-table" width="100%">
                <thead>
                <tr>
                    <th>Offer Reference</th>
                    <th>Days Reserved</th>
                    <th>Entered By</th>
                    <th>Agent</th>
                    <th>Approved By</th>
                    <th>Customer</th>
                    <th>Total Amount</th>
                    <th>Balance</th>
                    <th>Action</th>
                </tr>
                </thead>

                <tbody>
                <tr>
                    <td> FRV/{{$offer->id}}</td>
                    <td> {!! $offer->getMeta('reservation_days', fanakaConfig('reservation_days', 7)) !!} </td>
                    <td> {{$offer->user->fullName()}}</td>
                    <td> {{$offer->agent->fullName()}}</td>
                    <td> {!! $offer->approvedBy ? $offer->approvedBy->fullName() : "<span class='badge badge-warning'>Not Approved</span>" !!}</td>
                    <td width="15%"> {!! $offer->getCustomerHref() !!}</td>
                    <td> {{number_format((double)$offer->total_amount , 2)}}</td>
                    <td> {{number_format((double)$offer->getBalance() , 2)}} </td>
                    <td>

                    </td>
                </tr>
                </tbody>

            </table>


            <h4 style="text-align: center; text-transform: capitalize">Plot Details</h4>

            <table class="table table-bordered table-striped" id="data-table" width="100%" cellspacing="0"
                   cellpadding="0">
                <thead>
                <tr>
                    <th>Land Location</th>
                    <th>Price</th>
                    <th>Size</th>
                    <th>Plot No</th>
                    <th>Parcel No</th>
                    <th>Parcel No Issued</th>
                    <th>Issued By</th>
                    <th>Date Issued</th>

                </tr>
                </thead>
                <tbody>

                @foreach($offer->offerItems as $saleItem)
                    <tr>
                        <?php
                        $plot = $saleItem->plot;


                        ?>

                        <td>{{$plot->product->name()}}</td>


                        <td>{{number_format((double)$saleItem->plot->price , 2)}}</td>
                        <td>{{$plot->size->label}}</td>

                        <td>{{$plot->plot_no}}</td>
                        <td>{{$plot->title_no}}</td>

                        @php

                            if ($plot->is_title_issued )
                            { $titleIssued= 'primary';
                            }
                            else{

                            $titleIssued = "warning";

                            }


                        @endphp
                        <td>
                            <span class="label label-{{$plot->is_title_issued ? "primary" : "danger" }}">{{$plot->is_title_issued ? "Issued" : "Not Issued" }}</span>
                        </td>
                        <td>{{$plot->issuedBy? $plot->issuedBy->fullName() : ""}}</td>
                        <td>{{$plot->date_issued}}</td>
                    </tr>
                @endforeach
                </tbody>
            </table>

            <h4 style="text-align: center; text-transform: uppercase">Payment Details</h4>

            <table class="table table-bordered table-striped data-table" width="100%">
                <thead>

                <tr>
                    <th>Date Deposited</th>
                    <th>Amount Paid</th>
                    <th>Payment Method</th>
                    <th>Reference No</th>
                    <th>Bank Name</th>
                    <th>Account No</th>
                    <th>Cheque No</th>
                    <th>Slip No</th>
                    <th>Drawer</th>
                    <th>Received By</th>
                </tr>

                </thead>

                <tbody>

                @foreach($offer->offerPayments as $payment)


                    <tr>
                        <td>{{$payment->created_at}}</td>
                        <td>{{number_format($payment->amount , 2)}}</td>
                        <td>{{$payment->payment_method}}</td>
                        <td>{{$payment->reference_code}}</td>
                        <td>{{$payment->bank_name}}</td>
                        <td>{{$payment->account_no}} </td>
                        <td>{{$payment->cheque_no}}</td>
                        <td>{{$payment->slip_no}}</td>
                        <td>{{$payment->drawer}}</td>
                        <th>{{$payment->user->fullName()}}</th>
                    </tr>

                @endforeach
                </tbody>
                <tfoot>
                <tr>
                    <td><strong>Totals</strong></td>
                    <td colspan="9"><strong>{{number_format($offer->offerPayments->sum('amount') , 2)}}</strong></td>
                </tr>
                </tfoot>


            </table>


        </div>


        <div class="text-justify">
            @foreach($offer->uploads as $upload)
                <li><a href="{{route('offer_generate_download-receipt' ,['upload_id' => $upload->id])}}">{{$upload->file_name}}</a></li>
            @endforeach
        </div>





    @endcomponent

@endsection


@section('extra_js_footer')
    @include('layouts.dataTables.datatable')
    <script src="https://cdn.ckeditor.com/4.9.2/standard/ckeditor.js"></script>
    @include('layouts.datePicker.datepicker')
    @include('layouts.form')
    <script>


        var customerHtml = '<div class="moreCustomerDetails"  style="border-top: solid"> <div class="form-group">' +
            ' <label class="control-label col-md-2">First Name <span class="required-uzapoint">*</span> ' +
            '</label>' +
            ' <div class="col-md-4"> ' +
            '<input type="text" class="form-control" name="first_name[]"> </div> ' +
            '<label class="control-label col-md-2">Last Name <span class="required-uzapoint">*</span>' +
            ' </label> <div class="col-md-4">' +
            ' <input type="text" class="form-control" name="last_name[]" placeholder="500000"> </div> ' +
            '</div> <div class="form-group"> ' +
            '<label class="control-label col-md-2">Middle Name <span class="required-uzapoint">*</span> ' +
            '</label> <div class="col-md-4"> ' +
            '<input type="text" class="form-control" name="middle_name[]" value="."> </div>' +
            ' <label class="control-label col-md-2">Id No <span class="required-uzapoint">*</span>' +
            ' </label> <div class="col-md-4"> ' +
            '<input type="text" class="form-control" name="id_no[]" placeholder="12345678"> </div> ' +
            '</div> <div class="form-group">' +
            ' <label class="control-label col-md-2">Email <span class="required-uzapoint">*</span> ' +
            '</label> <div class="col-md-4"> ' +
            '<input type="text" class="form-control" name="email[]"> ' +
            '</div> <label class="control-label col-md-2">Phone number <span class="required-uzapoint">*</span> ' +
            '</label> <div class="col-md-4"> ' +
            '<input type="text" class="form-control phoneNumber" name="phone_number[]"> </div>' +
            ' </div><div class="modal-footer"> ' +
            '<button class="btn btn-danger removeCustomer" type="button">remove Customer</button> ' +
            '</div></div>';


        $(document).on('click', 'button.moreCustomer', function () {

            $(".addCustomerHtml").append(customerHtml)
        })

        $(document).on('click', 'button.removeCustomer', function () {

            $(this).closest('div.moreCustomerDetails').remove()
        })


        FormSliderSwitcher.init();

        $("select.selectpicker").selectpicker('refresh')


        var witnessHtml = '<div class="witnessDiv">' +
            ' <div class="form-group">' +
            ' <label class="control-label col-md-2">Name</label> ' +
            '<div class="col-md-3"> ' +
            '<input type="text" name="witness_name[]" class="form-control">' +
            ' </div> ' +
            '<label class="control-label col-md-2">ID NO:/</label> ' +
            '<div class="col-md-3">' +
            ' <input type="text" name="witness_id[]" class="form-control"> ' +
            '</div>' +
            ' <div class="col-md-1"> ' +
            '<button class="btn btn-primary addWintess" type="button">+</button> ' +
            '</div> <div class="col-md-1">' +
            ' <button class="btn btn-danger removeWitness" type="button">-</button>' +
            ' </div>' +
            ' </div>' +
            ' </div>';


        $(document).on('click', 'button.addWintess', function () {


            $(".add-witness").append(witnessHtml);


        });
        $(document).on('click', 'button.removeWitness', function () {

            $(this).closest('div.witnessDiv').remove();


        });

        //new customer


        //new customer

        var new_customer = function () {
            return $('#new_customer').is(":checked");
        };
        //
        if (!new_customer()) {
            $(".display_new_customer").slideUp();
        }
        $("#new_customer").on('change', function () {

            if (!new_customer()) {
                $(".display_new_customer").slideUp();
                $("div.hide-if-new").removeClass('hidden')
            } else {
                $(".display_new_customer").slideDown();
                $("div.hide-if-new").addClass('hidden')
            }
        });



        //new witness

        var witness = function () {
            return $('#hasWitness').is(":checked");
        };
        //
        if (!witness()) {
            $(".add-witness").slideUp();
        }
        $("#hasWitness").on('change', function () {

            if (!witness()) {
                $(".add-witness").slideUp();


            } else {
                $(".add-witness").slideDown();


            }
        });



        $(document).on('change','select#selected-customer' , function () {

            $("span.append-here").empty();

            let customerDetails = "Name:   "+ $("select#selected-customer option:selected").data('fullname');
            customerDetails += "<br/>I.D:   "+ $("select#selected-customer option:selected").data('id_no');
            customerDetails += "<br/>Phone Number:   "+ $("select#selected-customer option:selected").data('phone_number');
            customerDetails += "<br/>E-MAIL:  "+ $("select#selected-customer option:selected").data('email');
            $("span.append-here").append(customerDetails)
        })


        var PaymentNoteHmtl = '<div class="addCustomerNote"> ' +
            '<div class="form-group"> ' +
            '<label class="control-label col-md-2">Note</label> ' +
            '<div class="col-md-8">' +
            ' <textarea name="customer_payment_note[]" class="form-control" rows="10" cols="20"></textarea> ' +
            '</div> ' +
            '<div class="col-md-1"> ' +
            '<button class="btn btn-primary addCustomerDetails" type="button">+</button>' +
            ' </div> ' +
            '<div class="col-md-1"> ' +
            '<button class="btn btn-danger removeDetails" type="button">-</button>' +
            ' </div> ' +
            '</div>' +
            ' </div>';


        //new payment Note

        var paymentNote = function () {
            return $('#otherDetails').is(":checked");
        };
        //
        if (!paymentNote()) {
            $(".add-notes").slideUp();
        }
        $("#otherDetails").on('change', function () {

            if (!paymentNote()) {
                $(".add-notes").slideUp();
            } else {
                $(".add-notes").slideDown();
            }
        });


        $(document).on('click', 'button.addCustomerDetails', function () {


            $("div.addCustomerNote").append(PaymentNoteHmtl);


        });
        $(document).on('click', 'button.removeDetails', function () {

            $(this).closest('div.addCustomerNote').remove();


        });


        (function () {

            $('#plot_no').val($("select#plot option:selected").attr('data-plot_no'))


            $("#paid").on('input', function () {


                var amount = parseFloat($("#total").text());

                var paid = $(this).val();


                var balance = Math.ceil((numeral(amount).value()) - (numeral(paid).value()))


                console.log(balance, 'balance', amount, 'total', paid, 'paid amounr')

                $("#balance").val(balance);


                var installmenets = ((balance) / (numeral($("#month").val()).value()));

                $("#m_installment").val(Math.ceil(installmenets))


            })


            $("#month").on('keyup', function () {


                var balance = numeral($("#balance").val()).value();

                var installmenets = ((balance) / (numeral($(this).val()).value()));

                $("#m_installment").val(Math.ceil(installmenets));


                var today = new Date();

                var completion = today.getMonth() + numeral($(this).val()).value();


            })


        })()


        $(document).on('change', '#plot', function () {

            $('#plot_no').val($("select#plot option:selected").attr('data-plot_no'))

        })

        /*$('.datepicker').datepicker({
            date:new Date()
        });*/


        var dateToday = new Date();
        $(".datepicker").datepicker({
            minDate: dateToday

        });

        $(document).on('change', '#payment_type', function () {


            if ($(this).val('full')) {
                $(".next_date").slideUp();
            }
            if ($(this).val('installment')) {

                $(".next_date").slideDown();
                $("#balance").val((numeral($("#paid").val()).value()));


                var balance = numeral($("#balance").val()).value();

                var installmenets = ((balance) / (numeral($("#month").val()).value()));

                $("#m_installment").val(Math.ceil(installmenets))


                var today = new Date()


                var completion = today.getMonth() + numeral($("#month").val()).value();


                $("#complection").datepicker({

                    maxDate: "+7m "
                })


            }


        })


        $(document).on('click', 'button.AddMoreProduct', function () {

            //$("#addSale>tbody").html('')


            var customerId = $('#customer').val();

            var customerFullName = $("select#customer option:selected").text();

            var plotId = $('#plot').val();
            var plotLabel = $("select#plot option:selected").text();
            var quantity = 1;
            var unitPrice = $("select#plot option:selected").attr('data-unit-price');
            var CashPrice = $("select#plot option:selected").attr('data-unit-price') * (96/100);

            var PlotNo = $("select#plot option:selected").attr('data-plot_no');


            var htmlObject = "<tr class='myRow[]' id='" + customerId + plotId + "'>" +

                "<td><input type='hidden' name='plot_id[]' value='" + plotId + "'>" + plotLabel + "</td>" +
                "<td><input type='text' class='col-md-4' name='plot_no[]' readonly value='" + PlotNo + "'></td>" +
                "<td><input type='text' readonly id='price" + customerId + plotId + "' class='col-md-12 form-control item_price' name='price[]' value='" + CashPrice + "'></td>" +
                "<input type='hidden' id='price" + customerId + plotId + "' class='col-md-12 form-control item_price' name='cash_option[]' value='" + CashPrice + "'>" +
                "<td><input type='text' id='installment" + customerId + plotId + "' class='col-md-12 form-control item_installment' name='installment_option[]' value='" + unitPrice + "'></td>" +
                "<td>" +
                "<span id='subtotal" + customerId + plotId + "'  class='subtotal'>0.0</td>" +
                "<td><button class='btn btn-danger removeThis' type='button'> &times;</td>" +
                "</tr>";

            var i = 1;

            var myTableRow = $("table#addSale>tbody>tr#" + customerId + plotId);

            if (myTableRow.length) {


                return false;
            } else {
                $("#addSale>tbody").append(htmlObject);

                $(".addPayment").show();

            }
            compute_total_price();


        });


        var compute_total_price = function () {
            var total_price = 0;
            $('tr[class="myRow[]"]').each(function () {
                var id = ($(this)[0]['id']);
                var q = 1; ///from an input you can edit

                var p = $("#installment" + id).val();


                if($("input.payment_method_option:checked").val() === 'cash_option'){

                    p = $("#price" + id).val();

                }

                console.info('price' + p + 'was')
                if (!p) {
                    var pr = $("#price" + id).html();
                    p = pr;
                }


                $("#subtotal" + id).html(p * q);
                total_price = total_price + p * q;

            });


            var sum_sub_total = $("table#addSale>tbody>tr").find('span.subtotal');
            var su = 0;

            console.info('subtotal is ' + sum_sub_total);
            $.each(sum_sub_total, function () {

                su = su + parseInt($(this).html());
                //su +=total_price;
            });
            $("#total").html(su);
            $(".gtotal").val(su);
            $(".grand_total").val(su);
            $("#amountToPay").val(su);
//                $("#balance").html(su);
            //calculate_balance();
            console.info('total_price is' + su)

        };
        $("#pending-table").DataTable({
            sort: false

        });
        $("#request-table").DataTable({
            sort: false

        });
        $("#approved-table").DataTable({
            sort: false

        });


        $(document).on('click', '#detail', function () {


            $('.sale_details').html('')
            var saleItems = JSON.parse($('a#detail').attr('data-saleItems'));
            var table = '<table class="table-bordered table-striped table" width="100%"> ' +
                '<thead>' +
                '<tr>' +
                '<th>Plot No</th>' +
                '<th>Plot Name</th>' +
                '<th>Size</th>' +
                '<th>Plot Price</th>' +
                '</tr>' +
                '</thead>' +
                '<tbody>' +
                '<tr>' +
                '<td>' + saleItems.plot_no + '</td>' +
                '<td>' + saleItems.plot.product.name + '</td>' +
                '<td>' + saleItems.plot.size.label + '</td>' +
                '<td>' + saleItems.price + '</td>' +
                '</tr>' +
                '</tbody>' +
                '</table>';
            $('.sale_details').html(table)


        })


        $(document).on('change', '.method', function () {


            if ($(this).val() === 'Mpesa') {
                $(".mpesa").show();
                $(".bank").hide();
                $(".cheque").hide();
            }
            if ($(this).val() === 'Bank') {
                $(".mpesa").hide();
                $(".bank").show();
                $(".cheque").hide();
            }
            if ($(this).val() === 'Cheque') {
                $(".mpesa").hide();
                $(".bank").hide();
                $(".cheque").show();
            }
            if ($(this).val() === 'Cash') {
                $(".mpesa").hide();
                $(".bank").hide();
                $(".cheque").hide();
            }
        })

        $(document).on('click', 'button.removeThis', function () {

            $(this).closest('tr').remove();
            compute_total_price();

        })


        $(document).on('keyup', '.item_price', function () {
            compute_total_price();
        });

        $("#offer-table").DataTable({
            sort: false
        })


        $(document).on('click', 'button.OfferButton', function () {

            for (instance in CKEDITOR.instances) {
                CKEDITOR.instances[instance].updateElement();
            }

            $(this).attr('disabled', true).text('Writing ....')

            let formData = new FormData($("form.OfferForm")[0])

            axios.post('/offer/generate/store', formData)
                .then((res) => {

                    console.log(res);

                    $(this).attr('disabled', false).text('Write Offer')


                    $.growl.notice({
                        message: res.data.message,
                    });

                    $("form.OfferForm").trigger('reset')


                }).catch((err) => {
                console.log(err);

                $(this).attr('disabled', false).text('Write Offer')
                if(err.response.status === 422){

                    $.each(err.response.data.message, (error, value) => {

                        $.growl.warning({
                            message: value,
                        })

                    });


                    return ;

                }

                $.growl.warning({
                    message: err.response.data.message,
                });



            })


        })

        /*
                $(document).on('click', 'button.OfferButtonSubmit', ()=> {


                    swal({
                        title: "Are you sure?",
                        text: "You want to submit this sale reservation!",
                        icon: "warning",
                        buttons: true,
                        dangerMode: true,
                    })
                        .then((willDelete) => {
                            if (willDelete) {

                                $('button.OfferButtonSave').trigger('click')
                            }
                        });

                })*/

    </script>
@endsection
