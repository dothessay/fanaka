<form class="form-horizontal form-bordered OfferForm" method="post" action="{{route('offer_generate_store')}}"
      enctype="multipart/form-data">
    {{csrf_field()}}


    <div class="form-group hide-if-new">

        <label class="col-md-2 control-label">Existing Customer</label>
        <div class="col-md-4">

            <select class="form-control selectpicker" data-live-search="true" data-size="10" id="selected-customer"
                    name="customer_id">
                @foreach($customers as $customer)

                    <option
                            value="{{$customer->id}}"
                            data-fullname="{{$customer->fullName()}}"
                            data-email="{{$customer->email}}"
                            data-phone_number="{{$customer->phone_number}}"
                            data-id_no="{{$customer->id_no}}"
                            id="">{{$customer->fullName()}}</option>
                @endforeach
            </select>
        </div>

        <div class="customer-details col-md-6">
            <div style="
            background:#fff;
            box-shadow:0 20px 50px rgba(0,0,0,.1);
            border-radius:10px;
            font-size: small;
            font-style: italic;
            transition:0.5s;">
                <div class="box">
                    <span class="append-here"></span>
                </div>
            </div>
        </div>
    </div>

    <div class="form-group">
        <label class="control-label col-md-2">New Customer</label>
        <div class="col-md-4">
            <input type="checkbox" data-render="switchery" id="new_customer" name="new_customer"
                   data-theme="default">
        </div>

        <label class="control-label col-md-2">Reservation Days</label>

        <div class="col-md-4">
            <input
                    name="reservation_days"
                    class="form-control"
                    value="{{ fanakaConfig('reservation_days', 7) }}">
        </div>

    </div>


    <div class="display_new_customer" style="display: none">

        <div class="form-group">
            <label class="control-label col-md-2">First Name <span class="required-uzapoint">*</span>
            </label>

            <div class="col-md-4">
                <input type="text" class="form-control" name="first_name[]">
            </div>
            <label class="control-label col-md-2">Last Name <span class="required-uzapoint">*</span>
            </label>
            <div class="col-md-4">
                <input type="text" class="form-control" name="last_name[]" placeholder="500000">
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-md-2">Middle Name <span class="required-uzapoint">*</span>
            </label>
            <div class="col-md-4">
                <input type="text" class="form-control" name="middle_name[]" value=".">
            </div>
            <label class="control-label col-md-2">Id No <span class="required-uzapoint">*</span>
            </label>
            <div class="col-md-4">
                <input type="text" class="form-control" maxlength="8" minlength="7" name="id_no[]"
                       placeholder="12345678">
            </div>
        </div>
        <div class="form-group"><label class="control-label col-md-2">Email <span
                        class="required-uzapoint">*</span>
            </label>
            <div class="col-md-4">
                <input type="email" class="form-control" name="email[]">
            </div>
            <label class="control-label col-md-2">Phone number <span class="required-uzapoint">*</span>
            </label>
            <div class="col-md-4">
                <input type="text" class="form-control phoneNumber" maxlength="10" minlength="10" name="phone_number[]">
            </div>
        </div>
        <span></span>
        <span class="addCustomerHtml">

                       </span>
        <div class="modal-footer">
              <button class="btn btn-success moreCustomer" type="button">More Customer</button>
          </div>



    </div>


    <div class="form-group">


        <label class="control-label col-md-2">Method Of Payment <span class="required-uzapoint">*</span></label>

        <div class="col-md-4">
            <input type="radio" class="custom-radio payment_method_option" value="cash_option" name="payment_method">
            <span style="color: lightskyblue ; font-size: 10px">1 month payment Plan</span>
            <input type="radio" class="custom-radio payment_method_option" value="installment_option"
                   name="payment_method">
            Installment
        </div>

        <label class="control-label col-md-2">Agent <span class="required-uzapoint">*</span></label>

        <div class="col-md-4">


            <select name="agent" class="form-control selectpicker" data-live-search="true" data-size="10">

                @foreach($users as $user)

                    <option value="{{$user->id}}">{{$user->fullName()}}</option>
                @endforeach
            </select>


        </div>


    </div>


    <div class="form-group">


        <label class="control-label col-md-2">Plot</label>
        <div class="col-md-4">
            <select class="form-control selectpicker" id="plot" data-live-search="true" data-size="10">


                @foreach($plots as $plot)
                    <option value="{{$plot->id}}"
                            data-unit-price="{{$plot->price}}"
                            data-pricing="{{json_encode($plot->getPricing())}}"
                            data-size_id="{{$plot->size->id}}"
                            data-plot_no="{{$plot->plot_no}}">
                        <small>{{$plot->product->name . ' ' . $plot->size->label }}
                            (No:// {{$plot->plot_no}})
                        </small>
                    </option>
                @endforeach
            </select>
        </div>

        <label class="control-label col-md-2">Plot No</label>
        <div class="col-md-4">
            <input type="text" readonly="readonly" class="form-control" id="plot_no">
        </div>


        <div class="modal-footer">
            <button class="btn btn-primary AddMoreProduct" type="button">Add More</button>
        </div>

        <table class="table-bordered table-striped table" id="addSale" width="100%">
            <thead>
            <tr>
                <th>Plot</th>
                <th>Plot No</th>
                <th>Cash Option (4 %)</th>
                <th>Installment Option</th>
                <th>Sub Total</th>
                <th><i class="fa fa-trash-o"></i></th>
            </tr>
            </thead>
            <tbody class="product-list"></tbody>
            <tfoot>
            <tr>
                <td colspan="3"><strong>Total</strong></td>
                <td><span id="total"><b>0.0</b></span></td>
            </tr>
            </tfoot>
        </table>

    </div>

    <div class="modal-footer">
        <button type="button" class="btn btn-primary addPayment" style="display: none"
                data-toggle="modal"
                data-target="#payment">Add Payment
        </button>
    </div>

    @component('layouts.partials.modal')
        @slot('id','payment')
        @slot('dialogClass','modal-lg')
        @slot('title') <h6>Select payment method and complete the sale</h6> @endslot

        <div class="form-group">


            <label class="control-label col-md-2">Payment Method</label>
            <div class="col-md-4">
                {{--  <select name="payment_method"  id="method" class="form-control selectpicker" data-live-search="true"
                          data-size="5">--}}
                @foreach(\Codex\Classes\Helper::system()->paymentMethods() as $paymentMethod)

                    <input type="radio" name="payment_option" class="method"
                           value="{{$paymentMethod}}">{{$paymentMethod}}
                    {{--  <option value="{{$paymentMethod}}">{{$paymentMethod}}</option>--}}

                    @endforeach
                    </select>
            </div>

        </div>

        <div class="form-group">

            <label class="control-label col-md-2">Amount Paid</label>
            <div class="col-md-2">
                <input type="text" name="amount" class="form-control " value="0" id="paid">
                <input type="hidden" name="total" class="form-control grand_total">
            </div>

            <div class="next_date">


                <label class="control-label col-md-2">Balance</label>
                <div class="col-md-2">
                    <input type="text" name="balance" class="form-control" id="balance">
                </div>


                <label class="control-label col-md-1 hidden">Months</label>
                <div class="col-md-2 hidden">
                    <input type="text" name="months" class="form-control" value="6" id="month">
                </div>

            </div>
        </div>



        <div class="form-group">
            <label class="control-label col-md-2">Deposit Date</label>
            <div class="col-md-3">
                <input type="text" name="deposit_date" value="{{\Carbon\Carbon::today()}}" class="form-control datepicker" id="">
            </div>


        </div>

        <div class="form-group next_date hidden">

            <div class="control-label col-md-2">Monthly Installment</div>

            <div class="col-md-4">
                <input type="text" name="monthly_installment" class="form-control" id="m_installment">
            </div>
            {{--



                                <label class="control-label col-md-2">Completion Date</label>
                                <div class="col-md-4">
                                    <input type="text" name="due_date" class="form-control" id="complection">
                                </div>
            --}}

        </div>



        <div class="mpesa" style="display: none">
            <div class="form-group">

                <label class="control-label col-md-2">Reference code <span
                            class="required-uzapoint">*</span></label>
                <div class="col-md-4">
                    <input type="text" name="mpesa_reference_code" class="form-control">
                </div>
            </div>
        </div>



        <div class="bank" style="display: none">

            <div class="form-group">

                <label class="control-label col-md-2">Bank Name <span class="required-uzapoint">*</span>
                </label>
                <div class="col-md-4">
                    <input type="text" name="bank_name" value="{{config('config.equity_bank.name')}}"
                           class="form-control">
                </div>

                <label class="control-label col-md-2">Account No <span
                            class="required-uzapoint">*</span></label>
                <div class="col-md-4">
                    <input type="text" name="account_no" value="{{config('config.equity_bank.number')}}"
                           class="form-control">
                </div>
            </div>
            <div class="form-group">


                <label class="control-label col-md-2">Slip NO <span
                            class="required-uzapoint">*</span></label>
                <div class="col-md-4">
                    <input type="text" name="slip_no" class="form-control">
                </div>

            </div>
        </div>

        <div class="cheque" style="display: none">

            <div class="form-group">
                <label class="control-label col-md-2">Bank Name <span class="required-uzapoint">*</span></label>
                <div class="col-md-4">
                    <input type="text" name="check_bank_name" class="form-control">
                </div>

                <label class="control-label col-md-2">Cheque No <span class="required-uzapoint">*</span></label>
                <div class="col-md-4">
                    <input type="text" name="cheque_no" class="form-control">
                </div>

            </div>

            <div class="form-group">


                <label class="control-label col-md-2">Drawer <span
                            class="required-uzapoint">*</span></label>
                <div class="col-md-4">
                    <input type="text" name="drawer" class="form-control">
                </div>
            </div>

        </div>{{--

        <div class="form-group">
            <label class="col-md-1 control-label">Additional Information</label>
            <div class="col-md-10">
                <textarea class="target-element" name="note"></textarea>
            </div>
        </div>--}}

        <hr>

        <fieldset>
            <legend>Next Of kin</legend>

            <div class="form-group">

                <label class="col-md-2 control-label">Full Name</label>
                <div class="col-md-4">
                    <input type="text" name="next_of_kin_name" class="form-control">
                </div>


                <label class="col-md-2 control-label">Phone Number</label>
                <div class="col-md-4">
                    <input type="text" name="next_of_kin_phone_number" class="form-control">
                </div>
            </div>
            <div class="form-group">

                <label class="col-md-2 control-label">Relation</label>
                <div class="col-md-4">
                    <input type="text" name="next_of_kin_relation" class="form-control">
                </div>
            </div>

        </fieldset>

        <fieldset>
            <legend>Upload Receipts</legend>
            <div class="form-group">
                <label class="control-label col-md-2">Receipts</label>
                <div class="col-md-4">
                    <input type="file" multiple name="receipts[]">
                </div>
            </div>
        </fieldset>




        <div class="modal-footer">
            <button type="submit" class="btn btn-success OfferButtonSubmit">Write Offer</button>
            <button type="button" class="btn btn-success OfferButtonSaveYU hidden"></button>
        </div>
    @endcomponent


</form>
