@extends('layouts.master')

@section('title')Approve Reservation for {!! $offer->getCustomerHref() !!} @endsection

@section('content')
    @component('layouts.partials.panel')
        <div class="table-responsive">
            <table class="table-bordered table-striped table" id="data-table" width="100%">
                <thead>
                <tr>
                    <th>Offer Reference</th>
                    <th>Entered By</th>
                    <th>Agent</th>
                    <th>Approved By</th>
                    <th>Customer</th>
                    <th>Total Amount</th>
                    <th>Days</th>
                    <th>Balance</th>
                </tr>
                </thead>

                <tbody>
                <tr>
                    <td> FRV/{{ $offer->id }}</td>
                    <td> {{ $offer->user->fullName() }}</td>
                    <td> {{ $offer->agent->fullName() }}</td>
                    <td> {!! $offer->approvedBy ? $offer->approvedBy->fullName() : "<span class='badge badge-warning'>Not Approved</span>" !!}</td>
                    <td width="15%"> {!! $offer->getCustomerHref() !!}</td>
                    <td> {{ number_format((double)$offer->total_amount , 2)}}</td>
                    <td>{{ $offer->getMeta('reservation_days', fanakaConfig('reservation_days', 7))}}</td>
                    <td> {{ number_format((double)$offer->getBalance() , 2)}} </td>
                </tr>
                </tbody>

            </table>


            <h4 style="text-align: center; text-transform: capitalize">Plot Details</h4>

            <table class="table table-bordered table-striped" id="data-table" width="100%" cellspacing="0"
                   cellpadding="0">
                <thead>
                <tr>
                    <th>Land Location</th>
                    <th>Price</th>
                    <th>Size</th>
                    <th>Plot No</th>
                    <th>Parcel No</th>
                    <th>Parcel No Issued</th>
                    <th>Issued By</th>
                    <th>Date Issued</th>

                    @can('access-module-component-functionality','plots_manage_delete')
                        <th>Release</th>
                    @endcan
                </tr>
                </thead>
                <tbody>

                @foreach($offer->offerItems as $saleItem)
                    <tr>
                        <?php
                        $plot = $saleItem->plot;


                        ?>

                        <td>{{$plot->product->name()}}</td>


                        <td>{{number_format((double)$saleItem->selling_price , 2)}}</td>
                        <td>{{$plot->size->label}}</td>

                        <td>{{$plot->plot_no}}</td>
                        <td>{{$plot->title_no}}</td>

                        @php

                            if ($plot->is_title_issued )
                            { $titleIssued= 'primary';
                            }
                            else{

                            $titleIssued = "warning";

                            }


                        @endphp
                        <td>
                            <span class="label label-{{$plot->is_title_issued ? "primary" : "danger" }}">{{$plot->is_title_issued ? "Issued" : "Not Issued" }}</span>
                        </td>
                        <td>{{$plot->issuedBy? $plot->issuedBy->fullName() : ""}}</td>
                        <td>{{$plot->date_issued}}</td>


                            @can('access-module-component-functionality','plots_manage_delete') <td>

                                <a data-toggle="modal" data-target="#available" id="markAsAvailable"
                                   data-id="{{$plot->id}}">Un Reserved</a>
                                @endcan

                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>

            <h4 style="text-align: center; text-transform: uppercase">Payment Details</h4>

            <table class="table table-bordered table-striped data-table" width="100%">
                <thead>

                <tr>
                    <th>Date Deposited</th>
                    <th>Amount Paid</th>
                    <th>Payment Method</th>
                    <th>Reference No</th>
                    <th>Bank Name</th>
                    <th>Account No</th>
                    <th>Cheque No</th>
                    <th>Slip No</th>
                    <th>Drawer</th>
                    <th>Received By</th>
                </tr>

                </thead>

                <tbody>

                @foreach($offer->offerPayments as $payment)


                    <tr>
                        <td>{{$payment->created_at}}</td>
                        <td>{{number_format($payment->amount , 2)}}</td>
                        <td>{{$payment->payment_method}}</td>
                        <td>{{$payment->reference_code}}</td>
                        <td>{{$payment->bank_name}}</td>
                        <td>{{$payment->account_no}} </td>
                        <td>{{$payment->cheque_no}}</td>
                        <td>{{$payment->slip_no}}</td>
                        <td>{{$payment->drawer}}</td>
                        <th>{{$payment->user->fullName()}}</th>
                    </tr>

                @endforeach
                </tbody>
                <tfoot>
                <tr>
                    <td><strong>Totals</strong></td>
                    <td colspan="9"><strong>{{number_format($offer->offerPayments->sum('amount') , 2)}}</strong></td>
                </tr>
                </tfoot>


            </table>


        </div>


        <div class="text-justify">
            @foreach($offer->uploads as $upload)
                <li><a href="{{route('offer_generate_download-receipt' ,['upload_id' => $upload->id])}}">{{$upload->file_name}}</a></li>
            @endforeach
        </div>



        <div id="note">
            <h2 class="customer-note">
                Customer Notes
            </h2>
            <p>
                {!! $offer->getMeta('note') !!}
            </p>
        </div>



        <style>
            #note{
                background: turquoise;
                border: 0 solid;
            }
            #note h2.customer-note{

                margin-top: 1em;
                padding: 1em;
                text-align: center;
            }

            #note p{
                text-align: justify-all;
                padding: 2em;
            }
        </style>



    @endcomponent
    @can('access-module-component-functionality','offer_generate_can-approve')
       @if(! $offer->is_approved)
           <div class="email-footer">
               <form action="{{route('offer_generate_approving')}}" method="post">
                   @csrf
                   <input type="hidden" name="offer_id" value="{{$offer->id}}">
                   <button type="submit"  class="btn btn-success">Approved</button>
                   <button type="submit" name="reject" class="btn btn-danger">Cancel</button>

               </form>
           </div>
           @endif
    @endcan



    @component('layouts.partials.modal')
        @slot('id','available')
        @slot('title')
            <h6></h6>
        @endslot

        <p>
        <h5>ARE YOU YOU WANT TO MARK THIS PLOT AS AVAILABLE </h5>
        <h6>The following will be affected</h6>
        <ul>
            <li>All sales belonging to this plot will be lost forever</li>
        </ul>

        <form method="post" action="{{route('plots_manage_mark-available')}}">
            {{csrf_field()}}
            <input type="hidden" id="plot_id_available" name="id">


            <div class="modal-footer">
                <button type="submit" class="btn btn-danger">Mark Available</button>
            </div>
        </form>

        </p>
    @endcomponent


@endsection

@section('extra_js_footer')

    <script>
        $(document).on('click', 'a#markAsAvailable', function () {

            var code = $(this).attr('data-id')

            $("#plot_id_available").val(code)
        });

    </script>

@stop
