<table class="table table-bordered table-striped" id="offer-table">
    <thead>
    <tr>
        <th>Date Reserved</th>
        <th> Days Reserved</th>
        <th> Due Date </th>
        <th>Offer Reference</th>
        <th>Payment Option</th>
        <th>Customer</th>
        <th>Agent</th>
        <th>Plots</th>
        <th>Payment Option</th>
        <th>Converted To Sale</th>
        <th>Actions</th>
    </tr>
    </thead>


    <tbody>
    @foreach($offers->where('is_converted', false) as $offer)
        <?php

        $color = $offer->color;

        $offerColor = "style=background:".$color;
       // $class = "style=background:{$color}";

        ?>
        <tr>
            <td {{  $offerColor }}>{{$offer->created_at->format('Y-m-d H:i')}}</td>
            <td {!! $offerColor !!}> {!! $offer->getMeta('reservation_days', fanakaConfig('reservation_days', 7)) !!} </td>
            <td {!! $offerColor !!}> {!! \Carbon\Carbon::parse($offer->created_at)->addRealDays($offer->getMeta('reservation_days', fanakaConfig('reservation_days', 7))) !!} </td>
            <td {{$offerColor}}><a href="{{route('offer_generate_approve' ,['offer' => $offer->id])}}">FRV/{{$offer->id}}</a> </td>
            <td {{$offerColor}}>{{$offer->payment_option}}</td>
            <td {{$offerColor}}>{!! $offer->getCustomerHref() !!}</td>
            <td {{$offerColor}}><a href="{{route('users_manage_details',['user' => $offer->agent->id])}}">{!! $offer->agent->fullName() !!}</a></td>
            <td {{$offerColor}} width="15%">{{$offer->getCustomerPlots()}}</td>
            <td {{$offerColor}}>{{$offer->payment_option}}</td>
            <td {{$offerColor}}><span class="badge badge-{{$offer->is_converted ? "success" :"warning"}}">{{$offer->is_converted}}</span></td>
            <td>
                @component('layouts.button')

                    @can('access-module-component-functionality','offer_generate_edit')
                        <li>
                            <a target="_blank"
                               href="{{route('offer_generate_edit-details' ,['offer_id' => $offer->id])}}">Edit</a>
                        </li>

                    @endcan

                    @can('access-module-component-functionality','offer_generate_download')
                        <li>
                            <a target="_blank" href="{{route('offer_generate_edit' ,['offer_id' => $offer->id])}}">Download</a>
                        </li>

                    @endcan


                        @can('access-module-component-functionality','offer_generate_receipt')
                        <li>
                            <a target="_blank" href="{{route('offer_generate_receipt' ,['offer_id' => $offer->id])}}">Receipt</a>
                        </li>

                    @endcan

                        @if(! $offer->is_converted)
                            @can('access-module-component-functionality','offer_generate_convert-to-sale')
                                <li>
                                    <a target="_blank"
                                       href="{{route('offer_generate_convert-to-sale' ,['offer_id' => $offer->id])}}">Convert
                                        To Sale</a>
                                </li>

                            @endcan
                        @endif

                        <li>
                            <a target="_blank"
                               href="{{route('offer_generate_add-note' ,['offer_id' => $offer->id])}}">Add A note</a>
                        </li>


                @endcomponent
            </td>
        </tr>
        @endforeach
    </tbody>

</table>
