@extends('layouts.master')
@section('title','Generate Client Plot Offer')
{{--update-reservation-payment--}}
@section('extra_css_header')
    <style>

        .card {
            transform:translate(-50%,-50%);

            background:#fff;
            box-shadow:0 20px 50px rgba(0,0,0,.1);
            border-radius:10px;
            transition:0.5s;
        }
    </style>
    @endsection
@section('content')


    @php
        $active = true;

    @endphp




    <ul class="nav nav-tabs tabs-uzapoint">

        @can('access-module-component-functionality', 'offer_manage_add-offer')


            <li class="{{$active ? "active" : ''}}">
                <a href="#default-tab-1" data-toggle="tab">
                    <span class="visible-xs">Add Offers</span>
                    <span class="hidden-xs">Add Offers</span>
                </a>
            </li>
            @php
                $active = false;

            @endphp
        @endcan

        @can('access-module-component-functionality', 'offer_manage_list-offer')

            <li class="{{$active ? "active" : ''}}">
                <a href="#default-tab-2" data-toggle="tab">
                    <span class="visible-xs">List Offers</span>
                    <span class="hidden-xs">List Offers</span>
                </a>
            </li>

        @endcan

        @can('access-module-component-functionality', 'offer_generate_request')

            <li class="{{$active ? "active" : ''}}">
                <a href="#default-tab-3" data-toggle="tab">
                    <span class="visible-xs">Approval Request <span class= "badge badge-danger">{{$offers->where('is_approved', false)->count()}}</span> </span>
                    <span class="hidden-xs">Approval Request <span class="badge badge-danger">{{$offers->where('is_approved', false)->count()}}</span></span>
                </a>
            </li>

        @endcan

        @can('access-module-component-functionality', 'offer_generate_pending')

            <li class="{{$active ? "active" : ''}}">
                <a href="#default-tab-4" data-toggle="tab">
                    <span class="visible-xs">Pending <span class="badge badge-danger">{{$offers->where('is_approved', false)->count()}}</span></span>
                    <span class="hidden-xs">Pending <span class=" badge badge-danger">{{$offers->where('is_approved', false)->count()}}</span></span>
                </a>
            </li>

        @endcan

        @can('access-module-component-functionality', 'offer_generate_approved')

            <li class="{{$active ? "active" : ''}}">
                <a href="#default-tab-5" data-toggle="tab">
                    <span class="visible-xs">Approved <span class="badge badge-danger">{{$offers->where('is_approved', true)->count()}}</span></span>
                    <span class="hidden-xs">Approved <span class="badge badge-danger">{{$offers->where('is_approved', true)->count()}}</span></span>
                </a>
            </li>

        @endcan

    </ul>

    @php
        $active = true;

    @endphp

    <div class="tab-content">

        @can('access-module-component-functionality', 'offer_manage_add-offer')


            <div class="tab-pane fade {{$active ? "active" : ''}} in" id="default-tab-1">


                @include('offer._form')


            </div>

            @php
                $active = false;

            @endphp
        @endcan

        @can('access-module-component-functionality', 'offer_manage_list-offer')
            <div class="tab-pane {{$active ? "active" : ""}} fade in" id="default-tab-2">

                <div class="table-responsive">

                    @include('offer._table')
                </div>
            </div>
        @endcan

        @can('access-module-component-functionality', 'offer_generate_request')
            <div class="tab-pane {{$active ? "active" : ""}} fade in" id="default-tab-3">

                <div class="table-responsive">

                    @include('offer._request')
                </div>
            </div>
        @endcan

        @can('access-module-component-functionality', 'offer_generate_pending')
            <div class="tab-pane {{$active ? "active" : ""}} fade in" id="default-tab-4">

                <div class="table-responsive">

                    @include('offer._pending')

                </div>
            </div>
        @endcan

        @can('access-module-component-functionality', 'offer_generate_approved')
            <div class="tab-pane {{$active ? "active" : ""}} fade in" id="default-tab-5">

                <div class="table-responsive">

                    @include('offer._approved')
                </div>
            </div>
        @endcan
    </div>


@endsection


@section('extra_js_footer')
    @include('layouts.dataTables.datatable')
    <script src="https://cdn.ckeditor.com/4.9.2/standard/ckeditor.js"></script>
    @include('layouts.datePicker.datepicker')
    @include('layouts.form')
    <script>

        var pricing = [
            {
                paymentOption: '',
                status: Boolean,
                amount: '',
                size_id: ''
            }
        ]
        var customerHtml = '<div class="moreCustomerDetails"  style="border-top: solid"> <div class="form-group">' +
            ' <label class="control-label col-md-2">First Name <span class="required-uzapoint">*</span> ' +
            '</label>' +
            ' <div class="col-md-4"> ' +
            '<input type="text" class="form-control" name="first_name[]"> </div> ' +
            '<label class="control-label col-md-2">Last Name <span class="required-uzapoint">*</span>' +
            ' </label> <div class="col-md-4">' +
            ' <input type="text" class="form-control" name="last_name[]" placeholder="500000"> </div> ' +
            '</div> <div class="form-group"> ' +
            '<label class="control-label col-md-2">Middle Name <span class="required-uzapoint">*</span> ' +
            '</label> <div class="col-md-4"> ' +
            '<input type="text" class="form-control" name="middle_name[]" value="."> </div>' +
            ' <label class="control-label col-md-2">Id No <span class="required-uzapoint">*</span>' +
            ' </label> <div class="col-md-4"> ' +
            '<input type="text" class="form-control" name="id_no[]" minlength="7" maxlength="8" placeholder="12345678"> </div> ' +
            '</div> <div class="form-group">' +
            ' <label class="control-label col-md-2">Email <span class="required-uzapoint">*</span> ' +
            '</label> <div class="col-md-4"> ' +
            '<input type="email" class="form-control" name="email[]"> ' +
            '</div> <label class="control-label col-md-2">Phone number <span class="required-uzapoint">*</span> ' +
            '</label> <div class="col-md-4"> ' +
            '<input type="text" class="form-control phoneNumber" maxlength="10" minlength="10" name="phone_number[]"> </div>' +
            ' </div><div class="modal-footer"> ' +
            '<button class="btn btn-danger removeCustomer" type="button">remove Customer</button> ' +
            '</div></div>';


        $(document).on('click', 'button.moreCustomer', function () {

            $(".addCustomerHtml").append(customerHtml)
        })

        $(document).on('click', 'button.removeCustomer', function () {

            $(this).closest('div.moreCustomerDetails').remove()
        })


        FormSliderSwitcher.init();

        $("select.selectpicker").selectpicker('refresh')


        var witnessHtml = '<div class="witnessDiv">' +
            ' <div class="form-group">' +
            ' <label class="control-label col-md-2">Name</label> ' +
            '<div class="col-md-3"> ' +
            '<input type="text" name="witness_name[]" class="form-control">' +
            ' </div> ' +
            '<label class="control-label col-md-2">ID NO:/</label> ' +
            '<div class="col-md-3">' +
            ' <input type="text" name="witness_id[]" class="form-control"> ' +
            '</div>' +
            ' <div class="col-md-1"> ' +
            '<button class="btn btn-primary addWintess" type="button">+</button> ' +
            '</div> <div class="col-md-1">' +
            ' <button class="btn btn-danger removeWitness" type="button">-</button>' +
            ' </div>' +
            ' </div>' +
            ' </div>';


        $(document).on('click', 'button.addWintess', function () {


            $(".add-witness").append(witnessHtml);


        });
        $(document).on('click', 'button.removeWitness', function () {

            $(this).closest('div.witnessDiv').remove();


        });

        //new customer


        //new customer

        var new_customer = function () {
            return $('#new_customer').is(":checked");
        };
        //
        if (!new_customer()) {
            $(".display_new_customer").slideUp();
        }
        $("#new_customer").on('change', function () {

            if (!new_customer()) {
                $(".display_new_customer").slideUp();
                $("div.hide-if-new").removeClass('hidden')
            } else {
                $(".display_new_customer").slideDown();
                $("div.hide-if-new").addClass('hidden')
            }
        });



        //new witness

        var witness = function () {
            return $('#hasWitness').is(":checked");
        };
        //
        if (!witness()) {
            $(".add-witness").slideUp();
        }
        $("#hasWitness").on('change', function () {

            if (!witness()) {
                $(".add-witness").slideUp();


            } else {
                $(".add-witness").slideDown();


            }
        });



        $(document).on('change','select#selected-customer' , function () {

            $("span.append-here").empty();

           let customerDetails = "Name:   "+ $("select#selected-customer option:selected").data('fullname');
            customerDetails += "<br/>I.D:   "+ $("select#selected-customer option:selected").data('id_no');
            customerDetails += "<br/>Phone Number:   "+ $("select#selected-customer option:selected").data('phone_number');
            customerDetails += "<br/>E-MAIL:  "+ $("select#selected-customer option:selected").data('email');
            $("span.append-here").append(customerDetails)
        })


        var PaymentNoteHmtl = '<div class="addCustomerNote"> ' +
            '<div class="form-group"> ' +
            '<label class="control-label col-md-2">Note</label> ' +
            '<div class="col-md-8">' +
            ' <textarea name="customer_payment_note[]" class="form-control" rows="10" cols="20"></textarea> ' +
            '</div> ' +
            '<div class="col-md-1"> ' +
            '<button class="btn btn-primary addCustomerDetails" type="button">+</button>' +
            ' </div> ' +
            '<div class="col-md-1"> ' +
            '<button class="btn btn-danger removeDetails" type="button">-</button>' +
            ' </div> ' +
            '</div>' +
            ' </div>';


        //new payment Note

        var paymentNote = function () {
            return $('#otherDetails').is(":checked");
        };
        //
        if (!paymentNote()) {
            $(".add-notes").slideUp();
        }
        $("#otherDetails").on('change', function () {

            if (!paymentNote()) {
                $(".add-notes").slideUp();
            } else {
                $(".add-notes").slideDown();
            }
        });


        $(document).on('click', 'button.addCustomerDetails', function () {


            $("div.addCustomerNote").append(PaymentNoteHmtl);


        });
        $(document).on('click', 'button.removeDetails', function () {

            $(this).closest('div.addCustomerNote').remove();


        });


        (function () {

            $('#plot_no').val($("select#plot option:selected").attr('data-plot_no'))


            $("#paid").on('input', function () {


                var amount = parseFloat($("#total").text());

                var paid = $(this).val();


                var balance = Math.ceil((numeral(amount).value()) - (numeral(paid).value()))


                console.log(balance, 'balance', amount, 'total', paid, 'paid amounr')

                $("#balance").val(balance);


                var installmenets = ((balance) / (numeral($("#month").val()).value()));

                $("#m_installment").val(Math.ceil(installmenets))


            })


            $("#month").on('keyup', function () {


                var balance = numeral($("#balance").val()).value();

                var installmenets = ((balance) / (numeral($(this).val()).value()));

                $("#m_installment").val(Math.ceil(installmenets));


                var today = new Date();

                var completion = today.getMonth() + numeral($(this).val()).value();


            })


        })()


        $(document).on('change', '#plot', function () {

            $('#plot_no').val($("select#plot option:selected").attr('data-plot_no'))

        })

        /*$('.datepicker').datepicker({
            date:new Date()
        });*/


        var dateToday = new Date();

        $(".datepicker").datepicker({
            maxDate: dateToday

        });

        $(document).on('change', '#payment_type', function () {


            if ($(this).val('full')) {
                $(".next_date").slideUp();
            }
            if ($(this).val('installment')) {

                $(".next_date").slideDown();
                $("#balance").val((numeral($("#paid").val()).value()));


                var balance = numeral($("#balance").val()).value();

                var installmenets = ((balance) / (numeral($("#month").val()).value()));

                $("#m_installment").val(Math.ceil(installmenets))


                var today = new Date()


                var completion = today.getMonth() + numeral($("#month").val()).value();


                $("#complection").datepicker({

                    maxDate: "+7m "
                })


            }


        })


        $(document).on('click', 'button.AddMoreProduct', function () {

            //$("#addSale>tbody").html('')


            var customerId = $('#customer').val();

            var customerFullName = $("select#customer option:selected").text();

            var plotId = $('#plot').val();
            var plotLabel = $("select#plot option:selected").text();
            var quantity = 1;
            var unitPrice = $("select#plot option:selected").attr('data-unit-price');
            var CashPrice = $("select#plot option:selected").attr('data-unit-price');

            var PlotNo = $("select#plot option:selected").attr('data-plot_no');
            var sizeId = $("select#plot option:selected").attr('data-size_id');
             pricing= JSON.parse($("select#plot option:selected").attr('data-pricing'));



            var paymentOption = $("input[name='payment_method']:checked").val()

            paymentOption = paymentOption.split('_')[0];

            pricing.forEach((price, index) => {
                

                if (price.payment_option == paymentOption && price.size_id == sizeId){

                    CashPrice = price.amount


                }
                if (price.size_id == sizeId && paymentOption == 'cash' && price.status === true){

                    CashPrice = price.amount

                }


            });

            var htmlObject = "<tr class='myRow[]' id='" + customerId + plotId + "'>" +

                "<td><input type='hidden' name='plot_id[]' value='" + plotId + "'>" + plotLabel + "</td>" +
                "<td><input type='text' class='col-md-4' name='plot_no[]' readonly value='" + PlotNo + "'></td>" +
                "<td><input type='text'  id='price" + customerId + plotId + "' class='col-md-12 form-control item_price' name='price[]' value='" + CashPrice + "'></td>" +
                "<input type='hidden' id='price" + customerId + plotId + "' class='col-md-12 form-control item_price' name='cash_option[]' value='" + CashPrice + "'>" +
                "<td><input type='text' id='installment" + customerId + plotId + "' class='col-md-12 form-control item_installment' name='installment_option[]' value='" + unitPrice + "'></td>" +
                "<td>" +
                "<span id='subtotal" + customerId + plotId + "'  class='subtotal'>0.0</td>" +
                "<td><button class='btn btn-danger removeThis' type='button'> &times;</td>" +
                "</tr>";

            var i = 1;

            var myTableRow = $("table#addSale>tbody>tr#" + customerId + plotId);

            if (myTableRow.length) {


                return false;
            } else {
                $("#addSale>tbody").append(htmlObject);

                $(".addPayment").show();

            }
            compute_total_price();


        });


        var compute_total_price = function () {
            var total_price = 0;
            $('tr[class="myRow[]"]').each(function () {
                var id = ($(this)[0]['id']);
                var q = 1; ///from an input you can edit

                var p = $("#installment" + id).val();


                if($("input.payment_method_option:checked").val() === 'cash_option'){

                    p = $("#price" + id).val();

                }

                console.info('price' + p + 'was')
                if (!p) {
                    var pr = $("#price" + id).html();
                    p = pr;
                }


                $("#subtotal" + id).html(p * q);
                total_price = total_price + p * q;

            });


            var sum_sub_total = $("table#addSale>tbody>tr").find('span.subtotal');
            var su = 0;

            console.info('subtotal is ' + sum_sub_total);
            $.each(sum_sub_total, function () {

                su = su + parseInt($(this).html());
                //su +=total_price;
            });
            $("#total").html(su);
            $(".gtotal").val(su);
            $(".grand_total").val(su);
            $("#amountToPay").val(su);
//                $("#balance").html(su);
            //calculate_balance();
            console.info('total_price is' + su)

        };
        $("#pending-table").DataTable({
            sort: false

        });
        $("#request-table").DataTable({
            sort: false

        });
        $("#approved-table").DataTable({
            sort: false

        });


        $(document).on('click', '#detail', function () {


            $('.sale_details').html('')
            var saleItems = JSON.parse($('a#detail').attr('data-saleItems'));
            var table = '<table class="table-bordered table-striped table" width="100%"> ' +
                '<thead>' +
                '<tr>' +
                '<th>Plot No</th>' +
                '<th>Plot Name</th>' +
                '<th>Size</th>' +
                '<th>Plot Price</th>' +
                '</tr>' +
                '</thead>' +
                '<tbody>' +
                '<tr>' +
                '<td>' + saleItems.plot_no + '</td>' +
                '<td>' + saleItems.plot.product.name + '</td>' +
                '<td>' + saleItems.plot.size.label + '</td>' +
                '<td>' + saleItems.price + '</td>' +
                '</tr>' +
                '</tbody>' +
                '</table>';
            $('.sale_details').html(table)


        })


        $(document).on('click', 'a.update-reservation-payment', function() {

            let id = $(this).data("id")


            $("input.reservation-id").val(id)
        })

        $(document).on('click', 'a.update-pending-reservation-payment', function() {

            let id = $(this).data("id")


            $("input.reservation-id").val(id)
        })


        $(document).on('change', '.method', function () {


            if ($(this).val() === 'Mpesa') {
                $(".mpesa").show();
                $(".bank").hide();
                $(".cheque").hide();
            }
            if ($(this).val() === 'Bank') {
                $(".mpesa").hide();
                $(".bank").show();
                $(".cheque").hide();
            }
            if ($(this).val() === 'Cheque') {
                $(".mpesa").hide();
                $(".bank").hide();
                $(".cheque").show();
            }
            if ($(this).val() === 'Cash') {
                $(".mpesa").hide();
                $(".bank").hide();
                $(".cheque").hide();
            }
        })

        $(document).on('click','button.updateReservation',function () {

            $(this).attr('disabled', true).text('loading . . . ')

            $('button.submit-reservation-payment').trigger('click')
        })

        $(document).on('click','button.updatePendingReservation',function () {

            $(this).attr('disabled', true).text('loading . . . ')

            $('button.submit-reservation-pending-payment').trigger('click')
        })




        $(document).on('click', 'button.removeThis', function () {

            $(this).closest('tr').remove();
            compute_total_price();

        })


        $(document).on('keyup', '.item_price', function () {
            compute_total_price();
        });

        $("#offer-table").DataTable({
            sort: false
        })


        $(document).on('click', 'button.OfferButton', function () {

            for (instance in CKEDITOR.instances) {
                CKEDITOR.instances[instance].updateElement();
            }

            $(this).attr('disabled', true).text('Writing ....')

            let formData = new FormData($("form.OfferForm")[0])

            axios.post('/offer/generate/store', formData)
                .then((res) => {

                    console.log(res);

                    $(this).attr('disabled', false).text('Write Offer')


                    $.growl.notice({
                        message: res.data.message,
                    });

                    $("form.OfferForm").trigger('reset')


            }).catch((err) => {
                console.log(err);

                $(this).attr('disabled', false).text('Write Offer')
                if(err.response.status === 422){

                    $.each(err.response.data.message, (error, value) => {

                        $.growl.warning({
                            message: value,
                        })

                    });


                    return ;

                }

                $.growl.warning({
                    message: err.response.data.message,
                });



            })


        })

/*
        $(document).on('click', 'button.OfferButtonSubmit', ()=> {


            swal({
                title: "Are you sure?",
                text: "You want to submit this sale reservation!",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
                .then((willDelete) => {
                    if (willDelete) {

                        $('button.OfferButtonSave').trigger('click')
                    }
                });

        })*/



class Pricing{




}
$(document).on('input','input#paid', function() {
    var paid = $("#paid").val();
    var total = $(".grand_total").val();
    var balance = parseFloat(total) - parseFloat(paid);
    $("input#balance").val(balance)
})
        </script>

@endsection
