
@can('access-module-component-functionality','offer_generate_convert-to-sale')
    @if($offer->is_approved)
        <div class="email-footer">
            <form action="{{route('offer_generate_convert-to-sale')}}" class="form-horizontal form-bordered" method="post">
                @csrf

                <input type="hidden" name="offer_id", value="{{$offer->id}}">

                   <div style="display: none" class="hidden">
                       <div class="col-md-12">
                           <div class="col-md-8">

                               <div class="form-group">


                                   <label class="control-label col-md-2">Payment Method</label>
                                   <div class="col-md-4">
                                       {{--  <select name="payment_method"  id="method" class="form-control selectpicker" data-live-search="true"
                                                 data-size="5">--}}
                                       @foreach(\Codex\Classes\Helper::system()->paymentMethods() as $paymentMethod)

                                           <input type="radio" name="payment_option" class="method"
                                                  value="{{$paymentMethod}}">{{$paymentMethod}}
                                           {{--  <option value="{{$paymentMethod}}">{{$paymentMethod}}</option>--}}

                                           @endforeach
                                           </select>
                                   </div>

                               </div>

                               <div class="form-group">

                                   <label class="control-label col-md-2">Amount</label>
                                   <div class="col-md-4">
                                       <input type="text" name="amount" class="form-control " value="0" id="paid">
                                       <input type="hidden" name="total" class="form-control grand_total">
                                   </div>

                               </div>

                               <div class="form-group next_date hidden">

                                   <div class="control-label col-md-2">Monthly Installment</div>

                                   <div class="col-md-4">
                                       <input type="text" name="monthly_installment" class="form-control" id="m_installment">
                                   </div>
                                   {{--



                                                       <label class="control-label col-md-2">Completion Date</label>
                                                       <div class="col-md-4">
                                                           <input type="text" name="due_date" class="form-control" id="complection">
                                                       </div>
                                   --}}

                               </div>



                               <div class="mpesa" style="display: none">
                                   <div class="form-group">

                                       <label class="control-label col-md-2">Reference code <span
                                                   class="required-uzapoint">*</span></label>
                                       <div class="col-md-4">
                                           <input type="text" name="mpesa_reference_code" class="form-control">
                                       </div>
                                   </div>
                               </div>



                               <div class="bank" style="display: none">

                                   <div class="form-group">

                                       <label class="control-label col-md-2">Bank Name <span class="required-uzapoint">*</span>
                                       </label>
                                       <div class="col-md-4">
                                           <input type="text" name="bank_name" value="{{config('config.equity_bank.name')}}"
                                                  class="form-control">
                                       </div>

                                       <label class="control-label col-md-2">Account No <span
                                                   class="required-uzapoint">*</span></label>
                                       <div class="col-md-4">
                                           <input type="text" name="account_no" value="{{config('config.equity_bank.number')}}"
                                                  class="form-control">
                                       </div>
                                   </div>
                                   <div class="form-group">


                                       <label class="control-label col-md-2">Slip NO <span
                                                   class="required-uzapoint">*</span></label>
                                       <div class="col-md-4">
                                           <input type="text" name="slip_no" class="form-control">
                                       </div>

                                   </div>
                               </div>

                               <div class="cheque" style="display: none">

                                   <div class="form-group">
                                       <label class="control-label col-md-2">Bank Name <span class="required-uzapoint">*</span></label>
                                       <div class="col-md-4">
                                           <input type="text" name="check_bank_name" class="form-control">
                                       </div>

                                       <label class="control-label col-md-2">Cheque No <span class="required-uzapoint">*</span></label>
                                       <div class="col-md-4">
                                           <input type="text" name="cheque_no" class="form-control">
                                       </div>

                                   </div>

                                   <div class="form-group">


                                       <label class="control-label col-md-2">Drawer <span
                                                   class="required-uzapoint">*</span></label>
                                       <div class="col-md-4">
                                           <input type="text" name="drawer" class="form-control">
                                       </div>
                                   </div>

                               </div>


                               <div class="form-group">
                                   <label class="control-label col-md-2">More Customer</label>
                                   <div class="col-md-4">
                                       <input type="checkbox" data-render="switchery" id="new_customer" name="new_customer"
                                              data-theme="default">
                                   </div>

                               </div>
                               <div class="display_new_customer" style="display: none">

                                   <div class="form-group">
                                       <label class="control-label col-md-2">First Name <span class="required-uzapoint">*</span> </label>

                                       <div class="col-md-4">
                                           <input type="text" class="form-control" name="first_name[]">
                                       </div>
                                       <label class="control-label col-md-2">Last Name <span class="required-uzapoint">*</span> </label>
                                       <div class="col-md-4">
                                           <input type="text" class="form-control" name="last_name[]" placeholder="500000">
                                       </div>
                                   </div>
                                   <div class="form-group">
                                       <label class="control-label col-md-2">Middle Name <span class="required-uzapoint">*</span> </label>
                                       <div class="col-md-4">
                                           <input type="text" class="form-control" name="middle_name[]" value=".">
                                       </div>
                                       <label class="control-label col-md-2">Id No <span class="required-uzapoint">*</span> </label>
                                       <div class="col-md-4">
                                           <input type="text" class="form-control" name="id_no[]" placeholder="12345678">
                                       </div>
                                   </div>
                                   <div class="form-group"><label class="control-label col-md-2">Email <span class="required-uzapoint">*</span>
                                       </label>
                                       <div class="col-md-4">
                                           <input type="text" class="form-control" name="email[]">
                                       </div>
                                       <label class="control-label col-md-2">Phone number <span class="required-uzapoint">*</span> </label>
                                       <div class="col-md-4">
                                           <input type="text" class="form-control phoneNumber" name="phone_number[]"></div>
                                   </div>
                                   <span ></span>
                                   <span class="addCustomerHtml">

                                </span>

                                   <div class="modal-footer">
                                       <button class="btn btn-success moreCustomer" type="button">More Customer</button>
                                   </div>

                               </div>

                           </div>

                           <div class="col-md-4">

                               <fieldset>
                                   <div class="form-group">

                                       <div class="col-md-10">

                                           <label class="control-label col-md-4">Receipts</label>
                                           <input type="file" multiple name="receipts[]">
                                       </div>
                                   </div>

                               </fieldset>
                           </div>
                       </div>
                   </div>

                <input type="hidden" name="offer_id" value="{{$offer->id}}">
                <button type="submit"  class="btn btn-success">Submit</button>
                <button type="submit" name="reject" class="btn btn-danger">Cancel</button>

            </form>
        </div>
    @endif
@endcan
