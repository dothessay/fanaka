<table class="table table-bordered table-striped nowrap" id="request-table">
    <thead>
    <tr>
        <th>Date Reserved</th>
        <th>Days Reserved</th>
        <th>Offer Reference</th>
        <th>Payment Option</th>
        <th>Customer</th>
        <th>Plots</th>
        <th>Cash Option</th>
        <th>Installment Option</th>
        <th>Action</th>
    </tr>
    </thead>

    <tbody>
    @foreach($offers->where('is_approved', false) as $offer)
        <tr>
            <td>{{$offer->created_at->format('Y-m-d H:i')}}</td>
            <td> {!! $offer->getMeta('reservation_days', fanakaConfig('reservation_days', 7)) !!} </td>
            <td><a href="{{route('offer_generate_approve' ,['offer_id' => $offer->id])}}">FRV/{{$offer->id}}</a> </td>
            <td>{{$offer->payment_option}}</td>
            <td>{!! $offer->getCustomerHref() !!}</td>
            <td>{{$offer->getCustomerPlots()}}</td>
            <td>{{$offer->cash_option}}</td>
            <td>{{$offer->installment_option}}</td>
            <td>
                @component('layouts.button')

                    @can('access-module-component-functionality','offer_generate_can-approve')
                        <li>
                            <a target="_blank" href="{{route('offer_generate_approve' ,['offer_id' => $offer->id])}}">Approve</a>
                        </li>

                    @endcan
                @endcomponent
            </td>
        </tr>
    @endforeach
    </tbody>

</table>
