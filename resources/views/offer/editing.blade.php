@extends('layouts.master')

@section('title','Editing Reservation Letter')


@section('content')

    <form class="form-horizontal" method="POST" action="{{route('offer_generate_print-reservation')}}">

        @csrf

        <div class="row form-group">
            <div class="col-md-12">
                <input name="id" type="hidden" value="{{$offer->id}}">

                <textarea id="reservation" name="reservation" cols="130" rows="60">{!! $reservation !!}</textarea>

            </div>
        </div>

        <br>


        @can('access-module-component-functionality','offer_generate_can-print')
            <div class="form-group row">

                <label class="col-md-5"></label>
                <div class="col-md-5">
                    <button class="btn btn-success">Print</button>
                </div>


            </div>
        @endcan
    </form>

@endsection


@section('extra_js_footer')





@endsection