@extends('layouts.master')
@section('title','Reservations')

@section('extra_css_header')

    {!! $style !!}
    @endsection

@section('content')

    <form class="form-horizontal form-bordered" method="get" action="{{route('offer_generate_print-reservation')}}">

        @csrf

        <div class="form-group">

            <input type="hidden" class="form-control" value="{{@$_GET['offerId']}}">
            <div class="col-md-12">


                <textarea name="reservation" class="form-control" cols="30px" rows="50">



                        {!! $texts !!}

                </textarea>


            </div>
        </div>

        @can('access-module-component-functionality','offer_generate_can-print')

        <div class="modal-footer">

            <button type="submit" class="btn btn-primary">Print</button>

        </div>

            @endcan
    </form>

@endsection

