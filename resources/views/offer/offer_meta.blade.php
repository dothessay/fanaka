@extends('layouts.master')

@section('title','Add Offer Notes')


@section('content')

    @component('layouts.partials.panel')
        <form
                class="form-horizontal"
                method="POST"
                action="{{route('offer_generate_store-add-note')}}"
        >
            {{method_field('POST')}}
            {{csrf_field()}}

                <input
                        type="hidden"
                        name="id"
                        value="{{$meta->id}}"
                >
                <input
                        type="hidden"
                        name="offer_id"
                        value="{{$offerId}}"
                >



            <div class="form-group">
                <label for="" class="col-md-2">Note</label>
                <div class="col-md-10">
                    <textarea
                    name="note">
                        {!! $meta->value !!}
                    </textarea>
                </div>
            </div>

            <div class="col-md-3 offset-4 align-content-center">
                <button
                class="btn btn-success"><span>{{ is_null($meta->id) ? "Submit" : "Update"}}</span></button>
            </div>

        </form>
    @endcomponent

@stop
