@extends('layouts.master')


@section('title','Edit reservation  details')


@section('content')



    <ul class="nav nav-tabs tabs-uzapoint">

        <li class="active ">
            <a href="#default-tab-1" data-toggle="tab">
                <span class=""> Offer Details</span>
            </a>
        </li>


        <li class="">
            <a href="#default-tab-2" data-toggle="tab">
                <span class="">Next of Kin  </span>
            </a>
        </li>


        <li class="">
            <a href="#default-tab-3" data-toggle="tab">
                <span class=""> Customer Details</span>
            </a>
        </li>


        <li class="">
            <a href="#default-tab-5" data-toggle="tab">
                <span class=""> Add Directors </span>
            </a>
        </li>


    </ul>


    <div class="tab-content">

        <div class="tab-pane fade active  in" id="default-tab-1">
            <form class="form-horizontal form-bordered" method="post"
                  action="{{route('offer_generate_edit-details' ,['offerId' => $reservation->id])}}">
                @csrf


                <div class="form-group">

                    <label class="control-label col-md-2">Method Of Payment <span
                                class="required-uzapoint">*</span></label>

                    <div class="col-md-3">

                        <select
                                name="payment_method"

                                class="form-control">
                            <option
                                    @if(strtolower($reservation->payment_option) == "cash option") selected
                                    @endif value="cash_option"> Cash Option
                            </option>


                            <option @if(strtolower($reservation->payment_option) == "installment option") selected
                                    @endif value="installment_option">Installment Option
                            </option>
                        </select>

                    </div>

                    <label class="control-label col-md-2">Amount</label>

                    <div class="col-md-4">
                        <input
                                name="amount"
                                class="form-control"
                               value=" @if(strtolower($reservation->payment_option) == "cash option"){{$reservation->cash_option}}@else{{$reservation->installment_option}} @endif">
                    </div>
                </div>

                    <div class="form-group">

                        <label class="control-label col-md-2">Marketer <span class="required-uzapoint">*</span></label>

                        <div class="col-md-3">

                            <select
                                    name="agent_id"
                                    class="form-control">

                                @foreach($agents as $agent)
                                    <option
                                            value="{{$agent->id}}" {{$agent->id === $reservation->agent_id ? "selected": ""}}
                                    >
                                        {{$agent->fullName()}}
                                    </option>
                                @endforeach

                            </select>
                        </div>


                        <label class="control-label col-md-2">Reservation Days</label>

                        <div class="col-md-4">
                            <input
                                    name="reservation_days"
                                    class="form-control"
                                    value="{{ $reservation->getMeta('reservation_days', fanakaConfig('reservation_days', 7)) }}">
                        </div>


                    </div>


                <div class="email-footer">
                    <button type="submit" class="btn btn-sm btn-primary">Update</button>

                </div>

            </form>

        </div>
        <div class="tab-pane fade   in" id="default-tab-2">
            <div class="table-responsive">
                <form class="form-horizontal form-bordered" method="post"
                      action="{{route('offer_generate_post-kin-details' ,['offerId' => $reservation->id])}}">

                    @csrf

                    <table class="table-striped table-bordered nowrap table">
                        <thead>
                        <tr>
                            <th>Name</th>
                            <th>Phone Number</th>
                            <th>Relation</th>
                            <th>action</th>
                        </tr>
                        </thead>
                        <tbody>


                        @foreach($reservation->nextOfKin() as $kin)

                            <tr>
                                <td>{{$kin->full_name}}</td>
                                <td>{{$kin->phone_number}}</td>
                                <td>{{$kin->relation}}</td>
                                <td><a class="btn btn-sm btn-danger"
                                       href="{{route('offer_generate_delete-kin-details',['id' => $kin->id])}}">remove</a>
                                </td>
                            </tr>

                        @endforeach
                        <tr>
                            <td><input name="full_name"></td>
                            <td><input name="phone_number"></td>
                            <td><input name="relation"></td>
                            <td>
                                <button type="submit" class="btn btn-sm btn-primary">Add</button>
                            </td>
                        </tr>

                        </tbody>
                    </table>
                </form>

            </div>

        </div>
        <div class="tab-pane fade   in" id="default-tab-3">
            <div id="customer-sales">

                <div class="customer-item">
                    <table class="table table-bordered">
                        <thead>
                        <tr>
                            <th>Full Name</th>
                            <th>Id No</th>
                            <th>Email</th>
                            <th>Tel</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($reservation->customers as $customer)
                            <tr>
                                <td>{{$customer->fullName()}}</td>
                                <td>{{$customer->id_no}}</td>
                                <td>{{$customer->email}}</td>
                                <td>{{$customer->phone_number}}</td>
                                <td>
                                    @component('layouts.button')
                                        @if($reservation->customers->count() > 1)
                                            <li>
                                                <a href="{{route('offer_generate_customer_remove', ['id' => $customer->id ,'offer-id' => $reservation->id])}}">Remove</a>
                                            </li>
                                        @endif
                                    @endcomponent
                                </td>

                            </tr>
                        @endforeach
                        </tbody>

                    </table>
                </div>

                <div class="customer-item">

                    <form
                            class="form-horizontal form-bordered"
                            method="post"
                            action="{{route('offer_generate_store-customers')}}"
                    >
                        {{csrf_field()}}

                        <input
                                type="hidden"
                                class=""
                                name="offer_id"
                                value="{{$reservation->id}}"
                        >

                        <div class="form-group hide-if-new">

                            <label class="col-md-2 control-label">Existing Customer</label>
                            <div class="col-md-4">

                                <select class="form-control selectpicker" data-live-search="true" data-size="10"
                                        id="selected-customer" name="customer_id">
                                    <option value="">Select customer</option>

                                    @foreach($customers as $customer)

                                        <option
                                                value="{{$customer->id}}"
                                                data-fullname="{{$customer->fullName()}}"
                                                data-email="{{$customer->email}}"
                                                data-phone_number="{{$customer->phone_number}}"
                                                data-id_no="{{$customer->id_no}}"
                                                id="">{{$customer->fullName()}}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="customer-details col-md-6">
                                <div style="
            background:#fff;
            box-shadow:0 20px 50px rgba(0,0,0,.1);
            border-radius:10px;
            font-size: small;
            font-style: italic;
            transition:0.5s;">
                                    <div class="box">
                                        <span class="append-here"></span>
                                    </div>
                                </div>


                            </div>


                            <div class="form-group row">

                                <button type="submit" class="btn btn-success">Submit</button>
                            </div>
                        </div>


                    </form>


                </div>


            </div>
        </div>
        <div class="tab-pane fade   in" id="default-tab-5">
        </div>
    </div>



    <div id="app">


    </div>



@endsection

@section('extra_js_footer')

    <script src="{{asset('js/reservation.js')}}"></script>


@endsection
