<div class="form-group">
    <label for="full_name" class="control-label col-md-2">{{ $customer->fullName() }}</label>
    <input type="hidden" class="form-control" name="customerId[]" value="{{ $customer->id }}">
</div>
<div class="form-group">
    <label for="kra" class="control-label col-md-2">{{ __('KRA') }}</label>
    <div class="col-md-2">

        <input type="file" class="form-control" name="kra[{{ $customer->id }}]">
    </div>

    <label for="kra" class="control-label col-md-2">{{ __('PassPort Photo') }}</label>
    <div class="col-md-2">

        <input type="file" class="form-control" name="passport[{{ $customer->id }}]">
    </div>

    <label for="kra" class="control-label col-md-1">{{ __('ID') }}</label>
    <div class="col-md-2">

        <input type="file" class="form-control" name="id[{{ $customer->id }}]">
    </div>
</div>
