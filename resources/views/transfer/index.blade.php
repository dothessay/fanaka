@extends('layouts.master')
@section('title','Title Transfer process')
@section('content')
    @php
        $signedUri = \Illuminate\Support\Facades\URL::signedRoute('sale_transfer_list');
    @endphp
    <table class="table table-striped table-bordered table-info" width="100%">
        <thead>
        <tr>
            <th>Customer</th>
            <th>Balance</th>
            <th>Project</th>
            <th>Status</th>
            <th>Actions</th>
        </tr>
        </thead>
        <tbody></tbody>
    </table>
@endsection
@section('extra_js_footer')
    @include('layouts.dataTables.datatable')
    <script>
        $("table.table").DataTable({
            "processing": true,
            "serverSide": true,
            ajax: "{{ $signedUri }}",
            columns:[
                {"data" : "customer"},
                {"data" : "project"},
                {"data" : "balance"},
                {"data" : "status"},
                {"data" : "id",
                    "render": function (id, type, full, meta) {
                    console.log(id ,'id')
                    console.log(type ,'type')
                    console.log(full ,'full')
                    console.log(meta ,'meta')
                        return '<div class="btn-group">' +
                            ' <button type="button" class="btn btn-sm btn-success dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">' +
                            ' Action ' +
                            '<span class="caret"></span>' +
                            ' </button>' +
                            ' <ul class="dropdown-menu">' +
                            ' ' +full.nextStep
                            ' </ul> ' +
                            '</div>';
                    }
                },
            ]
        })
    </script>
@endsection
