<!DOCTYPE html>
<!--[if IE 8]>
<html lang="en" class="ie8"> <![endif]-->

<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->
<head>
    @include('layouts.partials.header')
    @yield('extra_css_header')
    @yield('extra_js_header')

</head>
<body onload="startTime()">
<!-- begin #page-loader -->
<div id="page-loader" class="fade in"><span class="spinner"></span></div>
<!-- end #page-loader -->

<!-- begin #page-container -->
<div id="page-container" class="fade page-sidebar-fixed page-header-fixed">
    <!-- begin #header -->
    <div id="header" class="header navbar navbar-default navbar-fixed-top">
        <!-- begin container-fluid -->
        <div class="container-fluid">

            <!-- begin mobile sidebar expand / collapse button -->
            <div class="navbar-header">
                <a href="/" class="navbar-brand">
                    <img src=" {{ url( \Illuminate\Support\Facades\Storage::url(request()->user()->business->logo))}}"
                         style="max-height: 35px;" alt="Fanaka Real Estate"/>
                </a>
                <button type="button" class="navbar-toggle" data-click="sidebar-toggled">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
            </div>
            <!-- end mobile sidebar expand / collapse button -->

            <!-- begin navbar-collapse -->
            <div class="collapse navbar-collapse pull-left" id="top-navbar">


                <ul class="nav navbar-nav">

                    <li class="dropdown navbar-user">
                        <a href="javascript:" class="dropdown-toggle" data-toggle="dropdown">
                            <i class="fa fa-angle-double-right"></i> Payments</a>

                        <ul class="dropdown-menu animated fadeInLeft">
                            <li class="arrow"></li>
                            <li><a href="{{ route('sale_invoice') }}"> Receive Payments</a></li>
                            <li><a href="{{ route('finance_requisition_index', [], false) }}"> Requisition</a></li>
                        </ul>
                    </li>

                    <li class="dropdown navbar-user">
                        <a href="javascript:" class="dropdown-toggle" data-toggle="dropdown">
                            <i class="fa fa-angle-double-right"></i> Reports</a>


                        <ul class="dropdown-menu animated fadeInLeft">
                            <li class="arrow"></li>
                            <li><a href="{{url('/projects/reports/unpaid?agent='.auth()->id())}}">Unpaid &
                                    Defaulters</a></li>
                            <li><a href="{{url('projects/reports/available?agent='.auth()->id())}}">Availability</a>
                            </li>
                            @can('access-module-component-functionality','sale_reports_customer')
                                <li><a href="{{ route('sale_reports_customer',  ['agent' => auth()->id()])}}">By
                                        Customer</a></li>
                            @endcan
                            @can('access-module-component-functionality','sale_reports_agent')
                                <li><a href="{{ route('sale_reports_agent', ['agent' => auth()->id()])}}">By Agent</a>
                                </li>
                            @endcan
                            @can('access-module-component-functionality','sale_reports_above-50')
                                <li><a href="{{ route('sale_reports_above-50',  ['agent' => auth()->id()])}}">Above
                                        50</a></li>
                            @endcan
                            @can('access-module-component-functionality','sale_reports_below-50')
                                <li><a href="{{ route('sale_reports_below-50',  ['agent' => auth()->id()])}}">Below
                                        50</a></li>
                            @endcan

                            @can('access-module-component-functionality','sale_reports_scanned-agreement')
                                <li><a href="{{ route('sale_reports_scanned-agreement',  ['agent' => auth()->id()])}}">Scanned
                                        Documents</a></li>
                            @endcan

                            @can('access-module-component-functionality','sale_reports_week-installment')
                                <li><a href="{{ route('sale_reports_week-installment',  ['agent' => auth()->id()])}}">Weekly
                                        Instalments</a></li>
                            @endcan

                            @can('access-module-component-functionality','sale_reports_profit-and-loss')
                                <li><a href="{{ route('sale_reports_profit-loss',  ['agent' => auth()->id()])}}">Profit
                                        & Loss</a></li>
                            @endcan

                            @can('access-module-component-functionality','sale_reports_by-project')
                                <li><a href="{{ route('sale_reports_by-project',  ['agent' => auth()->id()])}}">Cash
                                        Sale</a></li>
                            @endcan
                            <li><a href="{{ route('sale_reports_performance')}}">Performance</a></li>


                        </ul>
                    </li>

                    <li><a><i class="fa fa-angle-double-right"></i> <strong><span id="innox-time"></span></strong></a>
                    </li>
                </ul>
            </div>
            <!-- end navbar-collapse -->

            <!-- begin header navigation right -->
            <ul class="nav navbar-nav navbar-right">
                <li class="dropdown">


                        <form class="navbar-form full-width dropdown-toggle f-s-14"  data-toggle="dropdown">
                            <div class="form-group">
                                <input type="text" name="query" oninput="getCustomers()" onclick="getCustomers()" value="{{ request('query', '') }}" class="form-control" id="query"
                                       placeholder="Enter Customer Name"/>
                                <button type="button" class="btn btn-search"><i class="fa fa-search"></i></button>
                            </div>
                        </form>


                    <ul class="dropdown-menu media-list pull-right animated fadeInDown customers">
                    </ul>


                </li>


                @include('layouts.partials.notification')

                @include('layouts.partials.toptab')

            </ul>
            <!-- end header navigation right -->
        </div>
        <!-- end container-fluid -->
    </div>
    <!-- end #header -->

    <!-- begin #sidebar -->
    @include('layouts.partials.sidemenu')

    <div class="sidebar-bg"></div>
    <!-- end #sidebar -->

    <!-- begin #content -->
    <div id="content" class="content">

        <!-- begin breadcrumb -->
    @yield('breadcrumbs')
    <!-- end breadcrumb -->

        <!-- begin page-header -->
        <h1 class="page-header hidden-print">@yield('title','Portal') <small>@yield('sub_title')</small></h1>

        <!-- end page-header -->

        @include('layouts.partials.error')


        <div id="app">

            <div class="panel panel-body">
                @yield('content')
            </div>

        </div>

    </div>
    <!-- end #content -->

    <!-- begin scroll to top btn -->
    <a href="javascript:" class="btn btn-icon btn-circle btn-success btn-scroll-to-top fade" data-click="scroll-top"><i
                class="fa fa-angle-up"></i></a>
    <!-- end scroll to top btn -->
</div>


<!-- end page container -->
@include('layouts.partials.footer')
@include('layouts.partials.alerts')

<script src="https://unpkg.com/@ionic/core@latest/dist/ionic.js"></script>
<script src="https://cdn.jsdelivr.net/npm/vue@2.6.10/dist/vue.js"></script>
@yield('extra_js_footer')

<?php


/*print @\Codex\Classes\Helper::chatWidget() ;*/



?>


<script>
    $(document).ready(function () {
        App.init();
        $(".selectpicker").selectpicker("render");


    });

    function startTime() {
        var today = new Date();
        var h = today.getHours();
        var m = today.getMinutes();
        var s = today.getSeconds();
        m = checkTime(m);
        s = checkTime(s);
        document.getElementById('innox-time').innerHTML =
            h + ":" + m + ":" + s;
        var t = setTimeout(startTime, 500);
    }

    function checkTime(i) {
        if (i < 10) {
            i = "0" + i
        }
          // add zero in front of numbers < 10
        return i;
    }

    function getCustomers() {
        $("ul.customers").trigger('input');
        $("ul.customers").empty();
        $("ul.customers").innerHTML = "";
        let count = 1;
        let status = "loading";
        let theList = `  <li class="dropdown-header">${status}</li>`;
        if ($("input#query").val().trim().length > 2) {
            $("ul.customers").innerHTML = "";
            count = $("input#query").val().trim().length;
            window.axios.get('{{ route('home.search') }}/?query=' + $("input#query").val() +'&count='+count)
                .then(res => {
                    console.log(res.data.count , count)

                    if(res.data.count == count)
                    {
                        console.log(res.data)
                        let customers = res.data.data;


                        for (let i = 0; i < customers.length; i++) {

                            theList += `<li class="media"><a href="${customers[i]['uri']}"> <div class="media-left">   </div> <div class="media-body">
                        <h6 class="media-heading"> ${customers[i].name} (${customers[i]['sales']} Sales)</h6> <p> ${customers[i]['tel']}</p>
                        <div class="text-muted f-s-11"></div>
                        </div> </a>    </li>`
                        }

                        $("ul.customers").trigger('input');
                        $("ul.customers").append(theList);
                        status = ""
                    }

                })
                .catch(err => {

                });


        }
    }


</script>

{{--<script type="text/javascript">
 composer require iflylabs/iflychat-php
    var iflychat_app_id="9dd03481-277f-4490-ac2c-6cfc7dab30b7";

    var iflychat_external_cdn_host="cdn.iflychat.com",iflychat_bundle=document.createElement("SCRIPT");iflychat_bundle.src="//"+iflychat_external_cdn_host+"/js/iflychat-v2.min.js?app_id="+iflychat_app_id,iflychat_bundle.async="async",document.body.appendChild(iflychat_bundle);var iflychat_popup=document.createElement("DIV");iflychat_popup.className="iflychat-popup",document.body.appendChild(iflychat_popup);
</script>--}}
</body>
</html>
