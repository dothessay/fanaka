
    <link href="{{asset('assets/plugins/switchery.min.css')}}" rel="stylesheet">
    <link href="{{asset('assets/plugins/bootstrap-select.min.css')}}" rel="stylesheet">
    <link href="{{asset('assets/plugins/tagit/jquery.tagit.css')}}" rel="stylesheet"/>
    <link href="{{asset('assets/plugins/bootstrap3-wysihtml5.min.css')}}" rel="stylesheet"/>


<script src="{{asset('assets/plugins/gritter/js/form-slider-switcher.demo.min.js')}}"></script>
<script src="{{asset('assets/plugins/switchery.min.js')}}"></script>
<script src="{{asset('assets/plugins/tagit/tag-it.min.js')}}"></script>
<script src="{{asset('assets/plugins/bootstrap-select.min.js')}}"></script>
<script src="{{asset('assets/plugins/bootstrap3-wysihtml5.all.min.js')}}"></script>
<script src="{{asset('assets/plugins/ckeditor.js')}}"></script>


    <script>
    $(".phone , #phone").intlTelInput({
          allowDropdown: true,
         autoHideDialCode: false,
         autoPlaceholder: "on",
          dropdownContainer: "body",
        // excludeCountries: ["KE"],
        formatOnDisplay: true,
        geoIpLookup: function(callback) {
            $.get("https://ipinfo.io", function() {}, "jsonp").always(function(resp) {
                var countryCode = (resp && resp.country) ? resp.country : "";
                callback(countryCode);
            });
        },
        hiddenInput: "phone_number",
        initialCountry: "auto",
        nationalMode: true,
        //onlyCountries: ['KE'],
        placeholderNumberType: "MOBILE",
        preferredCountries: ['ke'],
        separateDialCode: true,
        utilsScript: "{{asset('assets/plugins/utils.js')}}"
    });
    $(".phone_customer").intlTelInput({
          allowDropdown: true,
         autoHideDialCode: false,
         autoPlaceholder: "on",
          dropdownContainer: "body",
        // excludeCountries: ["KE"],
        formatOnDisplay: true,
        geoIpLookup: function(callback) {
            $.get("https://ipinfo.io", function() {}, "jsonp").always(function(resp) {
                var countryCode = (resp && resp.country) ? resp.country : "";
                callback(countryCode);
            });
        },
        hiddenInput: "phone_number[]",
        initialCountry: "auto",
        nationalMode: true,
        //onlyCountries: ['KE'],
        placeholderNumberType: "MOBILE",
        preferredCountries: ['ug'],
        separateDialCode: true,
        utilsScript: "{{asset('assets/plugins/utils.js')}}"
    });
</script>


