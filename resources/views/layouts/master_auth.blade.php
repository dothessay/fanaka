<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->
<head>
    @include('layouts.partials.header')

</head>
<body class="pace-top">
<!-- begin #page-loader -->
<div id="page-loader" class="fade in"><span class="spinner"></span></div>
<!-- end #page-loader -->

<!-- begin #page-container -->
<div id="page-container" class="fade">
    <!-- beloginControllerogin -->
    <div class="login bg-black" data-pageload-addclass="animated fadeIn" style="">
        <!-- begin brand -->
        <div class="login-header" >
            <div class="brand">
               {{-- <span ><img src="{{ url(Storage::url($sale->business->logo)) }}"
                                        style="width: 100%;
                                                height: auto; max-height: 70px"
                            alt="Login"/></span>--}}
            </div>

        </div>
        <!-- end brand -->
        <div class="login-content">
            @yield('content')
        </div>
    </div>
    <!-- loginControllerogin -->

</div>
<!-- end page container -->

@include('layouts.partials.footer')

<script>
    $(document).ready(function() {
        App.init(ajax=true);
    });
</script>
@yield('extra_js_footer')
</body>
</html>
