
    <link href="{{asset('assets/plugins/DatePicker/bootstrap-datepicker.css')}}" rel="stylesheet" />
    <link href="{{asset('assets/plugins/DatePicker/bootstrap-datepicker3.css')}}" rel="stylesheet" />
    <link href="{{asset('assets/plugins/DatePicker/bootstrap-datetimepicker.min.css')}}" rel="stylesheet" />
    <link href="{{asset('assets/plugins/DatePicker/bootstrap-timepicker.min.css')}}" rel="stylesheet" />
    <link href="{{asset('assets/plugins/DatePicker/daterangepicker.css')}}" rel="stylesheet" />

    <script src="{{asset('assets/plugins/DatePicker/bootstrap-datepicker.js')}}"></script>
    <script src="{{asset('assets/plugins/DatePicker/bootstrap-timepicker.min.js')}}"></script>
    <script src="{{asset('assets/plugins/DatePicker/moment.js')}}"></script>
    <script src="{{asset('assets/plugins/DatePicker/bootstrap-datetimepicker.min.js')}}"></script>
    <script src="{{asset('assets/plugins/DatePicker/daterangepicker.js')}}"></script>

    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.css">
    <script src="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.js"></script>
