<!-- ================== BEGIN PAGE LEVEL STYLE ================== -->
<link href="{{asset('assets/plugins/DataTables/media/css/dataTables.bootstrap.min.css')}}"
      rel="stylesheet"/>
<link href="{{asset('assets/plugins/DataTables/extensions/Responsive/css/responsive.bootstrap.min.css')}}"
      rel="stylesheet"/>

<script src="{{asset('assets/plugins/DataTables/media/js/jquery.dataTables.js')}}"></script>
<script src="{{asset('assets/plugins/DataTables/media/js/dataTables.bootstrap.min.js')}}"></script>
<script src="{{asset('assets/plugins/DataTables/extensions/Responsive/js/dataTables.responsive.min.js')}}"></script>
<script src="{{asset('assets/js/table-manage-responsive.demo.min.js')}}"></script>
<link href="{{asset('assets/plugins/bootstrap-select.min.css')}}" rel="stylesheet">
<script src="{{asset('assets/plugins/bootstrap-select.min.js')}}"></script>
<script src="{{asset('assets/js/uzapoint_datatable_refresh.js')}}"></script>
<!-- ================== END PAGE LEVEL JS ================== -->

<script>
    $(document).ready(function () {


        $(".selectpicker").selectpicker("render");
    });


</script>
