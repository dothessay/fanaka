
<script>
    $(function () {

        let table = 0 !== $("#data-table, .data-table").length && $("#data-table,.data-table").DataTable({
                sort: false,
                dom: "Bfrtip",
                buttons: [{
                    extend: "copy",
                    className: "btn-sm"
                }, {
                    extend: "csv",
                    className: "btn-sm"
                }, {
                    extend: "excel",
                    className: "btn-sm"
                }, {
                    extend: "pdf",
                    className: "btn-sm"
                },],
                responsive: !0

            });

    })
</script>