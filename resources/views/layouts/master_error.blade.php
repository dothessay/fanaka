<!DOCTYPE html>
<!--[if IE 8]>
<html lang="en" class="ie8"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->
<head>
    @php
        $title = 'ERROR';


        if(method_exists($exception, 'getStatusCode')){
            $title = $exception->getStatusCode();

            if($title == 0){
                $title = 'ERROR';
            }

        }

    @endphp
    @section('title', $title)

    @include('layouts.partials.header')

</head>
<body class="pace-top">

<!-- begin #page-container -->
<div id="page-container" class="fade">
    <!-- begin error -->
    <div class="error">
        <div class="error-code m-b-20"><i class="fa fa-blind"></i> @yield('heading') !</div>
        <div class="error-content" style="background: lightblue">
            <div class="error-message m-b-10">

                @if(env('APP_DEBUG', false))
                    {{$exception->getMessage()}}
                    @php
                        if(method_exists($exception, 'getFile')){
                            echo ' at '.$exception->getFile();
                        }
                    @endphp
                    @php
                        if(method_exists($exception, 'getLine')){
                            echo ' on line '.$exception->getLine();
                        }
                    @endphp

                @else
                    @yield('message','Something is wrong here')
                @endif

            </div>

            <div class="m-b-20">
                <a href="{{URL::previous()}}" class="btn btn-success m-r-25">Go To Previous Page</a>

                <a href="{{url('/')}}" class="btn btn-success">Go To Dashboard</a>
            </div>

            <div class="row">

                @if(env('APP_DEBUG', false))
                    <div class="col-md-1"></div>
                    <div class="col-md-10" align="center">
                        <table class="table table-striped table-bordered">
                            <thead>
                            <tr>
                                <th>Line</th>
                                <th>File</th>
                                <th>Class</th>
                                <th>Method</th>
                                <th>Args</th>
                            </tr>
                            </thead>
                            <tbody>

                            @foreach($exception->getTrace() as $trace => $traceValue)

                                <tr>
                                    <td>{{@$traceValue['line']}}</td>
                                    <td>{{@$traceValue['file']}}</td>
                                    <td>
                                        @isset($traceValue['function'])
                                            {{$traceValue['function']}}
                                        @endisset
                                    </td>
                                    <td>
                                        @isset($traceValue['class'])
                                            {{$traceValue['class']}}
                                        @endisset
                                    </td>
                                    <td>
                                        {{collect(@$traceValue['args'])->toJson()}}
                                    </td>
                                </tr>
                            @endforeach

                            </tbody>
                        </table>
                    </div>
                @endif

                @if(!env('APP_DEBUG', false))
                    <div class="error-desc m-b-30">
                        @yield('remarks', 'We have taken a note of this,
                        for more information please contact us at '.env('ADMIN_MAIL', 'philip@codexartisan.com'))
                    </div>
                @endif

            </div>

        </div>


    </div>
    <!-- end error -->


    <!-- begin scroll to top btn -->
    <a href="javascript:;" class="btn btn-icon btn-circle btn-success btn-scroll-to-top fade" data-click="scroll-top"><i
                class="fa fa-angle-up"></i></a>
    <!-- end scroll to top btn -->
</div>
<!-- end page container -->

@include('layouts.partials.footer')

<script>
    $(document).ready(function () {
        App.init();
    });

</script>
</body>
</html>