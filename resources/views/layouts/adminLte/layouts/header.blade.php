<head>

    <!-- metas -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="description" content="Digital Marketing kenya Rental Management system"/>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="baseuri" content="{{ url('/') }}">
    <!-- title  -->
    <title> @yield('title','Property Management') </title>


    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">
    <!-- favicon -->
    <link rel="shortcut icon" href="{{ asset('assets/adminlte/img/logos/favicon.png') }}">
    <link rel="apple-touch-icon" href="{{  asset('assets/adminlte/img/logos/apple-touch-icon-57x57.png') }}">
    <link rel="apple-touch-icon" sizes="72x72" href="{{  asset('assets/adminlte/img/logos/apple-touch-icon-72x72.png') }}">
    <link rel="apple-touch-icon" sizes="114x114" href="{{  asset('assets/adminlte/img/logos/apple-touch-icon-114x114.png') }}">

    <!-- Bootstrap 3.3.7 -->
    <link rel="stylesheet" href="{{  asset('assets/adminlte/bower_components/bootstrap/dist/css/bootstrap.min.css') }}">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="{{  asset('assets/adminlte/bower_components/Ionicons/css/ionicons.min.css') }}">
    <!-- jvectormap -->
    <link rel="stylesheet" href="{{  asset('assets/adminlte/bower_components/jvectormap/jquery-jvectormap.css') }}">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{  asset('assets/adminlte/dist/css/AdminLTE.min.css') }}">

    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="{{  asset('assets/adminlte/dist/css/skins/_all-skins.min.css') }}">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!-- Google Font -->
    <link rel="stylesheet"
          href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">

    <!-- toastr plugin -->
    <link href="{{  asset('assets/adminlte/plugins/toastr/toastr.min.css') }}" rel="stylesheet"/>
    <link rel="stylesheet" href="{{  asset('assets/adminlte/css/app.css') }}">
    @yield('extra_css')
    @yield('css')

    <style>
        .btn .btn-outline-primary {
            background: green;
        }

        .btn .btn-outline-success {
            background: green;
        }

        .btn .btn-outline-warning {
            background: orange;
        }

        .btn .btn-outline-danger {
            background: red;
        }


    </style>

</head>
