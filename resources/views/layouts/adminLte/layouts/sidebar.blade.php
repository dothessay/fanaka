<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="../../dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">
            </div>
            <div class="pull-left info">
                <p>Alexander Pierce</p>
                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>
        <!-- search form -->
        <form action="#" method="get" class="sidebar-form">
            <div class="input-group">
                <input type="text" name="q" class="form-control" placeholder="Search...">
                <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
            </div>
        </form>
        <!-- /.search form -->
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu" data-widget="tree">
            <li class="header">MAIN NAVIGATION</li>

{!!
    \Codex\Classes\Helper::sidemenu()->section('dashboard')
        ->href('/')
        ->icon('ion-speedometer')
        ->routeName('dashboard')
        ->module('dashboard')
        ->output()
!!}


{!!
     \Codex\Classes\Helper::sidemenu()->section('Projects')
                ->icon('ion-briefcase')
                ->module('projects')
                ->item('Manage','projects/manage')
                ->item('expenses','projects/expenses')
                ->item('reports','projects/reports')
                ->output()
 !!}


{!!
\Codex\Classes\Helper::sidemenu()->section('plots')
                ->icon('ion-home')
                ->module('plots')
                ->item('Manage','plots/manage')
                ->item('Titles','plots/manage/titles')
                ->item('reports','plots/reports')
                ->output()

 !!}

{!!
        \Codex\Classes\Helper::sidemenu()->section('sale')
                ->icon('fa fa-money')
                ->module('sale')
                ->item('Manage','sale/manage')
                ->item('Invoice','sale/invoice')
                ->item('Instalment Plan','sale/manage/instalment/plan')
                ->item('Appointment','sale/appointment')
                ->item('Commissions','sale/commissions')
                ->item('interest','sale/interest')
                ->item('Payments','sale/payments')
                ->item('Calculator','sale/manage/calculator')
                ->item('reports','sale/reports')
                ->output()

 !!}



{!!
    \Codex\Classes\Helper::sidemenu()->section('Reservation')
        ->href('offer/generate')
        ->icon('ion-md-contacts')
        ->routeName('offer_generate')
        ->module('offer')
        ->output()
!!}


{!!
        \Codex\Classes\Helper::sidemenu()->section('Finance')
                ->icon('ion-ios-wallet')
                ->module('finance')
                ->item('Chart','finance/charts')
                ->item('reports','finance/reports')
                ->item('journal','finance/journal-entry')
                ->item('Expenses','finance/expenses')
                ->item('Requisition','finance/requisition')
                ->output()

 !!}

{!!
    \Codex\Classes\Helper::sidemenu()->section('customers')
        ->href('customers/manage')
        ->icon('ion-person')
        ->routeName('customers_manage')
        ->module('customers')
        ->output()
!!}


{!!
    \Codex\Classes\Helper::sidemenu()->section('account')
        ->href('account/list')
        ->icon('ion-ios-pulse')
        ->routeName('account_list')
        ->module('account')
        ->output()
!!}


{!!
      \Codex\Classes\Helper::sidemenu()->section('users')
                ->icon('ion-person-stalker')
                ->module('users')
                ->item('manage','users/manage')
                ->item('department Head','users/department-head')
                ->item('role','users/role')
                ->item('permissions','users/permissions')
                ->item('leave','users/leave')
                ->output()

 !!}
{!!
        \Codex\Classes\Helper::sidemenu()->section('Communication')
                ->icon('fa fa-envelope')
                ->module('communication')
                ->item('contacts','communication/manage/contacts')
                ->item('Send','communication/manage')
                ->output()

 !!}



{!!
        \Codex\Classes\Helper::sidemenu()->section('settings')
                ->icon('fa fa-cogs')
                ->module('settings')
                ->item('company','settings/company')
                ->item('system','settings/system')
                ->output()

 !!}
        </ul>
    </section>
</aside>
