<!DOCTYPE html>
<html lang="en">
@include('layouts.adminLte.layouts.header')
<body class="hold-transition skin-{{ setting('company_skin', "green") }} fixed sidebar-mini">

<div class="wrapper">

    <header class="main-header">

        <!-- Logo -->
        <a href="{{ url('/') }}" class="logo">

            <!-- mini logo for sidebar mini 50x50 pixels -->
            <span class="logo-mini">
                <img src="{{ asset('storage/'. setting('company_logo'))  }}" alt="logo" width="50px" height="50px"
                     class="image-container img-circle">
            </span>
            <!-- logo for regular state and mobile devices -->
            <span class="logo-lg">
                <img src="{{ asset('storage/'. setting('company_logo'))  }}" alt="logo" class="image-container img-circle"></span>
        </a>


        @include('layouts.adminLte.layouts.topbar')

    </header>
    <!-- Left side column. contains the logo and sidebar -->

    <div id="app">

            @include('layouts.adminLte.layouts.sidebar')
            <!-- sidebar menu: : style can be found in sidebar.less -->

        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <section class="content-header">
                <h1>
                    @yield('title','Dashboard')

                </h1>
                <ol class="breadcrumb">
                    <li><a href="{{ url('/') }}"><i class="fa fa-dashboard"></i> Home</a></li>
                    <li class="active">@yield('title','Dashboard')</li>
                </ol>
            </section>

            <!-- Main content -->
            <section class="content">


                @if ($errors->any())
                    <div class="alert alert-danger alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">
                            &times;
                        </button>
                        @foreach ($errors->all() as $error)
                            <p>{{ $error }}</p>
                        @endforeach
                    </div>

            @endif
           @include('flash::message')
            @yield('content')
            </section>


        </div>

        <footer class="main-footer">
            <div class="pull-right hidden-xs">
                <b>Version</b> {{ config('system.version') }}
            </div>
            <strong>Copyright &copy; {{ now()->format('Y') }}
        </footer>
    </div>
</div>

@include('layouts.adminLte.layouts.footer')
@yield('extra_js')
@yield('extra_js_footer')
@yield('js')
@yield('javascript')
</body>
</html>
