<div class="btn-group btn-group-sm">
    <button type="button" class="label label-success btn-sn dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
        Action <span class="caret"></span>
    </button>
    <ul class="dropdown-menu" role="menu">
        {{$slot}}
    </ul>
</div>
