<div class="panel panel-{{ isset($class) ? $class :  "success"}}">
    <div class="panel-heading">

        <h4 class="panel-title">{{ isset($heading) ? $heading :  ' ' }}</h4>
    </div>
    <div class="panel-body">

            {{$slot}}

    </div>
</div>
