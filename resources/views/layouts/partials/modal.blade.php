<div class="modal fade {{ isset($modalClass) ? $modalClass :  ""}}" id="{{$id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog {{ isset($dialogClass) ? $dialogClass : ""  }}" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close btn btn-danger" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="false">
                        <span class="btn-default btn-danger">&times;</span></span>
                </button>
                <h4 class="modal-title" id="myModalLabel"><h6>{{$title}}</h6></h4>
            </div>
            <div class="modal-body">
                {{$slot}}
            </div>
        </div>
    </div>
</div>
