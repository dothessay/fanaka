
@if (session('success'))

   {{-- {{session()->get('success')}}--}}
      <script type='text/javascript'>
        $(function () {

            var message = "{{ session('success') }}"
            $.growl.notice({message: message})
        })
    </script>

@endif
@if (session('error'))

   {{-- {{session()->get('error')}}--}}

  <script type='text/javascript'>
        $(function () {

            var message = "{{ session('error') }}"
            $.growl.error({message: message})
        })
    </script>
@endif