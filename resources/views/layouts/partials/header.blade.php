<meta charset="utf-8" />
<title>@yield('title',ucwords('Innox Africa ')) - {{config('app.name')}}</title>
<meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" name="viewport" />
<meta name="keywords" content="ready title deeds">
<meta name="csrf-token" content="{{csrf_token()}}">
<meta name="base_url" content="{{url('/')}}">
<!-- ================== BEGIN BASE CSS STYLE ================== -->
<link href="{{asset('assets/css/fonts.css')}}" rel="stylesheet">
<link href="{{asset('assets/plugins/jquery-ui.min.css')}}" rel="stylesheet" />
<link href="{{asset('assets/plugins/bootstrap.min.css')}}" rel="stylesheet" />
<link href="{{asset('assets/plugins/font-awesome.min.css')}}" rel="stylesheet" />
<link href="{{asset('assets/css/animate.min.css')}}" rel="stylesheet" />
<link href="{{asset('assets/css/style.min.css')}}" rel="stylesheet" />
<link href="{{asset('assets/css/style-responsive.min.css')}}" rel="stylesheet" />
<link href="{{asset('assets/css/default.css')}}" rel="stylesheet" id="theme" />
<link href="{{asset('assets/plugins/ionicons.min.css')}}" rel="stylesheet" />
<link href="https://unpkg.com/ionicons@4.2.2/dist/css/ionicons.min.css" rel="stylesheet">

<!-- ================== END BASE CSS STYLE ================== -->

<!-- ================== BEGIN BASE JS ================== -->
<script src="{{asset('assets/plugins/pace.min.js')}}"></script>
<!-- ================== END BASE JS ================== -->

<!-- ================== Favicons ================== -->

@if( isset(request()->user()->id))

    <link href="{{ url( \Illuminate\Support\Facades\Storage::url(request()->user()->business->logo))}}" rel="apple-touch-icon" type="image/png" sizes="144x144">
    <link href="{{ url( \Illuminate\Support\Facades\Storage::url(request()->user()->business->logo))}}" rel="apple-touch-icon" type="image/png" sizes="114x114">
    <link href="{{ url( \Illuminate\Support\Facades\Storage::url(request()->user()->business->logo))}}" rel="apple-touch-icon" type="image/png" sizes="72x72">
    <link href="{{ url( \Illuminate\Support\Facades\Storage::url(request()->user()->business->logo))}}" rel="apple-touch-icon" type="image/png">
    <link href="{{ url( \Illuminate\Support\Facades\Storage::url(request()->user()->business->logo))}}" rel="icon" type="image/png">
    <link href="{{ url( \Illuminate\Support\Facades\Storage::url(request()->user()->business->logo))}}" rel="shortcut icon">

    @else

    <link href="{{asset('assets/img/avatar.png')}}" rel="apple-touch-icon" type="image/png" sizes="144x144">
    <link href="{{asset('assets/img/avatar.png')}}" rel="apple-touch-icon" type="image/png" sizes="114x114">
    <link href="{{asset('assets/img/avatar.png')}}" rel="apple-touch-icon" type="image/png" sizes="72x72">
    <link href="{{asset('assets/img/avatar.png')}}" rel="apple-touch-icon" type="image/png">
    <link href="{{asset('assets/img/avatar.png')}}" rel="icon" type="image/png">
    <link href="{{asset('assets/img/avatar.png')}}" rel="shortcut icon">

@endif

<!-- ================== END Favicons ================== -->


<link href="{{asset('assets/plugins/gritter/stylesheets/jquery.growl.css')}}" rel="stylesheet" type="text/css"/>

<link href="{{asset('assets/css/intlTelInput.css')}}" rel="stylesheet"/>
<link href="{{asset('assets/uzapoint.css')}}" rel="stylesheet" />
<style>

    #app{
        box-shadow: 3px 2px 6px  6px rgba(204, 204, 204, 0.8 ) !important;

    }

</style>
