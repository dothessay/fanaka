
<li class="dropdown navbar-user">
    <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown">
        <img src="https://img.devidentify.com/{{auth()->user()->email}}" alt="" />
        <span class="hidden-xs">{{auth()->user()->fullName()}}</span> <b class="caret"></b>
    </a>
    <ul class="dropdown-menu animated fadeInLeft ">
        <li class="arrow"></li>
        <li><a href="{{url('profile/edit/'.auth()->id())}}">Profile</a></li>
        <li><a href="{{url('api/blank-agreement')}}">Blank Agreement</a></li>
        <li><a href="{{url('notification/add-issue/')}}">Add Issue</a></li>
        @if( session()->has('old_user'))
            <li><a href="{{ route('leave-impersonate') }}">Leave Impersonate</a></li>
        @endif
        <li><a href="https://space.vivawebhost.com:2096" target="_blank">Staff Email</a></li>


{{--l

        <li><a href="javascript:;">{{transl('general.chat')}}</a></li>
        <li><a href="{{Helper::route('setting_mobile')}}">{{transl('general.mobile Apps')}}</a></li>

        <li class="divider"></li>
        <li><a href="javascript:;">{{transl('general.partner')}}</a></li>
        <li><a href="javascript:;">{{transl('general.support')}}</a></li>--}}



        <li class="divider"></li>
        <li><a href="{{url('logout')}}">log out</a></li>
    </ul>
</li>
