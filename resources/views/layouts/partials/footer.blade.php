<!-- ================== BEGIN BASE JS ================== -->
<script src="{{asset('assets/plugins/jquery-1.9.1.min.js')}}"></script>
<script src="{{asset('assets/plugins/jquery-migrate-1.1.0.min.js')}}"></script>
<script src="{{asset('assets/plugins/jquery-ui.min.js')}}"></script>
<script src="{{asset('assets/plugins/bootstrap.min.js')}}"></script>
<!--[if lt IE 9]>
<script src="{{asset('assets/crossbrowserjs/html5shiv.js')}}"></script>
<script src="{{asset('assets/crossbrowserjs/respond.min.js')}}"></script>
<script src="{{asset('assets/crossbrowserjs/excanvas.min.js')}}"></script>
<![endif]-->
<script src="{{asset('assets/plugins/jquery.hashchange.min.js')}}"></script>
<script src="{{asset('assets/plugins/jquery.slimscroll.min.js')}}"></script>
<script src="{{asset('assets/plugins/jquery.cookie.js')}}"></script>
<script src="{{asset('assets/plugins/gritter/javascripts/jquery.growl.js')}}" type="text/javascript"></script>

<script src="{{asset('assets/plugins/intlTelInput.js')}}"></script>
<!-- ================== END BASE JS ================== -->

<!-- ================== BEGIN PAGE LEVEL JS ================== -->
<script src="{{asset('assets/js/apps.min.js')}}"></script>
<!-- ================== END PAGE LEVEL JS ================== -->


<script src="//cdnjs.cloudflare.com/ajax/libs/axios/0.18.0/axios.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.2/Chart.bundle.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.2/Chart.bundle.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.2/Chart.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.2/Chart.min.js"></script>


<script src="//unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script src="https://js.pusher.com/4.3/pusher.min.js"></script>-

{{--<script src="{{asset('vendor/unisharp/laravel-ckeditor/ckeditor.js')}}"></script>--}}
{{--<script src="{{asset('vendor/unisharp/laravel-ckeditor/adapters/jquery.js')}}"></script>--}}
<script src="https://cdn.ckeditor.com/ckeditor5/12.3.1/classic/ckeditor.js"></script>
<script src="https://cdn.ckeditor.com/ckeditor5/12.3.1/classic/ckeditor.js"></script>


<script src="{{asset('js/app.js')}}"></script>
<script>

    /************ALWAYS COME HERE COPY PASTE THIShahahaa**********/
    /*for (instance in CKEDITOR.instances) {
        CKEDITOR.instances[instance].updateElement();
    }*/
    //

    ClassicEditor
        .create( document.querySelector( 'textarea' ) )
        .then( editor => {
            console.log( editor );
        } )
        .catch( error => {
            console.error( error );
        } );
    // $('textarea').ckeditor({
    //     autoParagraph: false,
    //     toolbar: 'Full',
    //     enterMode : CKEDITOR.ENTER_BR,
    //     shiftEnterMode: CKEDITOR.ENTER_P
    // });
    // $('.textarea').ckeditor(); // if class is prefered.
</script>

@can('access-module-component','communication_issue')
    <script>
        // Enable pusher logging - don't include this in production
        Pusher.logToConsole = true;

        var pusher = new Pusher('b59fb47bbf17f1d5804d', {
            cluster: 'mt1',
            forceTLS: true
        });

        var channel = pusher.subscribe('issue');
        channel.bind('\\App\\Events\\IssueNotification', function (data) {
            (function () {
                $.growl.issue({

                    title: data.type,
                    message: data.user + ' has sent an issue on ' + data.type,
                });
                return false
            })();
        });
    </script>
@endcan
