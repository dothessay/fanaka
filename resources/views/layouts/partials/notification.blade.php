<li class="dropdown">
    <a href="javascript:;" data-toggle="dropdown" class="dropdown-toggle f-s-14">
        <i class="fa fa-envelope-o" title="issues"></i>
        @if(collect($issues)->count())
            <span class="label">{{$count  = collect($issues)->count()}}</span>
        @endif
    </a>

    <ul class="dropdown-menu media-list pull-right animated fadeInDown">


        <li class="dropdown-header">Notifications</li>
        @foreach($issues->take(5) as $item)
            <li class="media">

                <a href="{{url('notification/issue-details/'.$item->id)}}">

                    <div class="media-left">

                    </div>
                    <div class="media-body">
                        <h6 class="media-heading">Issue On {{$item->type}} By {{$item->user->fullName()}}</h6>
                        <p> {!! $item->issue !!}</p>
                        <div class="text-muted f-s-11">{{$item->created_at->DiffForHumans()}}</div>
                    </div>
                </a>
            </li>
        @endforeach

        <li class="dropdown-footer text-center">
            <div class="dataTables_paginate"></div>
        </li>
    </ul>

</li>






<li class="dropdown">
    <a href="javascript:;" data-toggle="dropdown" class="dropdown-toggle f-s-14">
        <i class="fa fa-bell-o"></i>
        @if($notification->count())
            <span class="label">{{$count=$notification->count()}}</span>
        @endif
    </a>

    <ul class="dropdown-menu media-list pull-right animated fadeInDown">


        <li class="dropdown-header">Notifications</li>
        @foreach($notification->take(5) as $item)
            <li class="media">

                <a href="{{url('notification/details/'.$item->id)}}">

                    <div class="media-left">

                    </div>
                    <div class="media-body">
                        <h6 class="media-heading">{{$item->type}}</h6>
                        <p> {{$item->message}}</p>
                        <div class="text-muted f-s-11">{{$item->created_at->DiffForHumans()}}</div>
                    </div>
                </a>
            </li>
        @endforeach

        <li class="dropdown-footer text-center">
            <div class="dataTables_paginate"></div>
        </li>
    </ul>

</li>

