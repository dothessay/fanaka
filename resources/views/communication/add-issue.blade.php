@extends('layouts.master')

@section('title','Add issues')

@section('content')

    <div class="">
        <div class="panel panel-primary">
            <div class="panel panel-heading">

            </div>
            <div class="panel panel-body">
                <div class="bg-light">
                    <form class="form-horizontal form-bordered issueForm">
                        @csrf
                        <div class="form-group">
                            <label class="control-label col-md-2">Type Of Issue<span class="required-uzapoint">*</span> </label>
                            <div class="col-md-4">
                                <select name="type" class="form-control" data-live-search="true" data-size="10">
                                    @foreach(\Codex\Classes\Helper::system()->typeOfIssues() as $typeOfIssue)
                                        <option value="{{$typeOfIssue}}">{{ucfirst($typeOfIssue)}}</option>
                                    @endforeach
                                </select>
                            </div>

                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-2">Description<span class="required-uzapoint">*</span> </label>
                            <div class="col-md-10">
                                <textarea name="issue" id="my-issue"></textarea>
                            </div>

                        </div>
                        <div class="email-footer">

                            <button type="button" class="issueButton btn btn-success">Submit Issue</button>
                            <button type="reset" class="resetButton btn btn-danger">Reset </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

@endsection


@section('extra_js_footer')
@include('layouts.form')
<script>
    $(function () {

        $(document).on('click', 'button.issueButton', function () {
            for (instance in CKEDITOR.instances) {
                CKEDITOR.instances[instance].updateElement();
            }

            $(this).attr('disabled',true).text('Submitting . . . ')

            let formData =  new FormData($("form.issueForm")[0]);


            axios.post('/notification/store-issue', formData)
                .then((res) => {


                    $(this).attr('disabled',false).text('Submit Issue')

                    $("form.issueForm").trigger('reset')


                    $.growl.notice({
                        message: res.data.message
                    });


                    location.reload(true);

                }).catch((err) => {

                $(this).attr('disabled',false).text('Submit Issue')

                    if (err.response.status === 422){

                        $.each(err.response.data.message , (indexes , messages) => {

                            $.growl.warning({
                                message: messages
                            })
                        })

                        return ;
                    }

                $.growl.warning({
                    message: err.response.data.message
                })

            })



        })

    })
</script>
@endsection