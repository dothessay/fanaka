@if(fanakaConfig("sms_gateway") == 'SendAfricanTalkingInnoxNotification')
    Africa Stalking Balance: <?= (new \Codex\Classes\Handlers\SendAfricanTalkingInnoxNotification())->getBalance()?>
    @endif
{{--
{!! dd((new \Codex\Classes\Handlers\SendAfricanTalkingInnoxNotification())->getMessages()) !!}--}}

<form
method="post"
class="form-horizontal form-bordered"
action="{{route('communication_manage_bulk-sms')}}"
enctype="multipart/form-data"
>
    {{csrf_field()}}

    <style>
        .row span a.btn.btn-warning:hover{
            color: #fff;
            background: #f59c1a;
            border-color: #f59c1a;;
        }
    </style>
    <div class="row">
        <p>
            Available tags to use on excel


        </p>
        <ul>
            <li>name</li>
            <li>phone</li>
            <li>balance</li>
            <li>expired on</li>
            <li>message : e.g.:Dear {client}, your payment period expired on {expiry_date} as per the Sales Agreement for {project_plot} & your balance is {balance}. You are required to come for a new Sale Agreement within the next 21 Days. Failure to do so, Fanaka Real Estate will be entitled to Revoke the Sale Agreement & thereafter be at liberty to resell the Property. EDWIN MAINA & CO. ASSOCIATES ADVOCATE
            </li>
        </ul>

        <span style="clear: both; margin: 3em"><a
            href="{{route('communication_manage_download-contacts')}}" class="btn btn-warning" >Download Format excel format</a></span>
    </div>


    <div class="well-lg">

        <div class="form-group">
            <label for="" class="col-md-2">Template</label>
            <div class="col-md-4">
                <select
                        name="template"
                        class="selectpicker form-control template"


                >
                    <option data-hidden="true">Select Type of Message to send</option>
                    <option
                            value="expired_notice"
                            data-value="{{ fanakaConfig('expired_notice') }}"
                    >21 days notice Message</option>
                    <option
                            value="activation_message"
                            data-value="{{fanakaConfig('activation_message')}}"
                    >Activation Message</option>

                </select>
            </div>
        </div>


        <div class="form-group">
            <label for="contacts" class="col-md-2">Message</label>
            <div class="col-md-8">
                <input
                        class="form-control"
                        type="text"
                        id="bulk-sms"
                        name="message"
                        style="padding-bottom: 8em; margin-top: 2px"
                >
            </div>
        </div>




        <div class="form-group">

            <label for="contacts" class="col-md-2">Upload Contacts</label>

            <div class="col-md-8">
                <input
                        required
                        type="file"
                        name="contacts"
                >
            </div>
        </div>
    </div>

    <div class="modal-footer">
        <button type="submit" class="btn btn-success">Send</button>
    </div>
</form>
