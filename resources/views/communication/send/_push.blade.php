<form
        action="{{route('communication_manage_store')}}"
        class="form-bordered form-horizontal"
        method="post"
        autocomplete="off"
>

    {{csrf_field()}}

    <div id="communication">

        <div class="item">



                <div class="form-group">
                    <label for="type" class="col-md-4">Customers</label>
                    <div class="col-md-7">
                        <select
                                name="customerId[]"
                                class="form-control selectpicker"
                                data-live-search="true"
                                multiple
                                data-size="5"
                        >

                            @foreach($customers as $customer)

                                <option
                                        value="{{$customer->id}}"
                                >{{$customer->fullName()}}</option>

                            @endforeach
                        </select>
                    </div>
                </div>







                <div class="form-group">
                    <label for="type" class="col-md-2">Message</label>
                    <div class="col-md-10">




                        <div class="is_not_sms">
                                <input
                                        class="form-control"
                                        type="text"
                                        name="text_message"
                                        id="editor-message"
                                        style="padding-bottom: 9em; margin-top: 2px"
                                >
                        </div>


                    </div>
                </div>

        </div>

        <div class="item">


                <div class="form-group">
                    <label for="type" class="col-md-2">Message Type</label>
                    <div class="col-md-7">
                        <select
                                name="type"
                                name="message_type"
                                class="form-control"

                        >

                            @foreach(\Codex\Classes\Helper::system()->messageTypes as $key => $type)

                                <option
                                        value="{{$key}}"
                                >{{$type}}</option>

                            @endforeach
                        </select>
                    </div>
                </div>


                <div class="form-group">
                    <button
                            class="btn btn-success"

                    >Send
                    </button>
                </div>


        </div>
    </div>


</form>
