@extends('layouts.master')

@section('title','Send Message to Clients')
@section('extra_css_header')
    <style>
        #communication {
            display: grid;
            grid-template-columns: 60% auto;
            grid-gap: 2em;
            width: 100%;

        }
        #communication .item {
            min-height: 600px;


        }

        @media (max-width: 800px) {
            #communication {
                display: grid;
                grid-template-columns: auto;
                grid-gap: 2em;
            }

            #communication .item {



            }
        }

        .sms_message{
            height: 200px;
            width: 100%
        }

        .open>.dropdown-menu {
            display: block;
            margin-right: -13px;
            width: 163px;
        }
    </style>
@endsection
@section('content')





    <ul class="nav nav-tabs tabs-uzapoint">


        <li class="active">
            <a href="#default-tab-1" data-toggle="tab">
                <span class="">Push Notifications</span>

            </a>
        </li>

        <li class="">
            <a href="#default-tab-2" data-toggle="tab">
                <span class="">Bulk Sms</span>

            </a>
        </li>




    </ul>


    <div class="tab-content">

        <div class="tab-pane fade active in" id="default-tab-1">

            @include('communication.send._push')
        </div>

        <div class="tab-pane fade in" id="default-tab-2">

            @include('communication.send._bulk_sms')
        </div>


    </div>




@stop

@section('extra_js_footer')
@include('layouts.form')
<script>

$(document).on('click','input#bulk-sms', function () {

    let value = $("select.template option:selected").data('value');


    var $txtarea = $("<textarea />");
    $txtarea.attr("id", this.id);
    $txtarea.attr("rows", 8);
    $txtarea.attr("name", "message");
    $txtarea.attr("cols", 60);
    $txtarea.val(value);
    $(this).replaceWith($txtarea);
    $(this).text(value)



    $("textarea#bulk-sms").focus();

});

$(document).on('change','select#messageType', function () {

    let value = $("select#messageType option:selected").data('value');

    $('input#bulk-sms').val(value)

});

$(document).on('click','input#editor-message', function () {


    var $txtarea = $("<textarea />");
    $txtarea.attr("id", this.id);
    $txtarea.attr("rows", 8);
    $txtarea.attr("name", "text_message");
    $txtarea.attr("cols", 60);
    $(this).replaceWith($txtarea);




   $("textarea#editor-message").focus();

})



$(document).on('change','.sms_checked', function () {

    let editorMessage = $("textarea#editor-message").val();
    let smsMessage = $("input#sms-message").val();


    if ($(this).is(':checked'))
    {

        $("div.is_sms").removeClass('hidden');
        $("div.is_not_sms").addClass('hidden');
    }else{

        $("div.is_sms").addClass('hidden');
        $("div.is_not_sms").removeClass('hidden');
    }
    
})


</script>

@endsection
