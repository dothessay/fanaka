@extends('layouts.master')

@section('title',' List Off all Your emails')


@section('content')


    <div class="p-20">
        <!-- begin row -->
        <div class="row">
            <!-- begin col-2 -->
            <div class="col-md-2">
                <form>
                    <div class="input-group m-b-15">
                        <input type="text" class="form-control input-sm input-white" placeholder="Search Mail" />
                        <span class="input-group-btn">
                                <button class="btn btn-sm btn-inverse" type="button"><i class="fa fa-search"></i></button>
                            </span>
                    </div>
                </form>
                <div class="hidden-sm hidden-xs">
                    <h5 class="m-t-20">Email</h5>
                    <ul class="nav nav-pills nav-stacked nav-inbox">
                        <li class="active">
                            <a href="#">
                                <i class="fa fa-inbox fa-fw m-r-5"></i> Inbox (10)
                            </a>
                        </li>
                        <li><a href="#"><i class="fa fa-inbox fa-fw m-r-5"></i> Sent</a></li>
                        <li><a href="#"><i class="fa fa-pencil fa-fw m-r-5"></i> Draft</a></li>
                        <li><a href="#"><i class="fa fa-trash-o fa-fw m-r-5"></i> Trash</a></li>
                        <li><a href="#"><i class="fa fa-star fa-fw m-r-5"></i> Archive</a></li>
                    </ul>
                    <h5 class="m-t-20">Folders</h5>
                    <ul class="nav nav-pills nav-stacked nav-inbox">
                        <li><a href="#"><i class="fa fa-folder fa-fw m-r-5"></i> Newsletter</a></li>
                        <li><a href="#"><i class="fa fa-folder fa-fw m-r-5"></i> Friend</a></li>
                        <li><a href="#"><i class="fa fa-folder fa-fw m-r-5"></i> Company</a></li>
                        <li><a href="#"><i class="fa fa-folder fa-fw m-r-5"></i> Downloaded</a></li>
                    </ul>
                </div>
            </div>
            <!-- end col-2 -->
            <!-- begin col-10 -->
            <div class="col-md-10">
                <div class="email-btn-row hidden-xs">
                    <a href="#" class="btn btn-sm btn-inverse"><i class="fa fa-plus m-r-5"></i> New</a>
                    <a href="#" class="btn btn-sm btn-default disabled">Reply</a>
                    <a href="#" class="btn btn-sm btn-default disabled">Delete</a>
                    <a href="#" class="btn btn-sm btn-default disabled">Archive</a>
                    <a href="#" class="btn btn-sm btn-default disabled">Junk</a>
                    <a href="#" class="btn btn-sm btn-default disabled">Swwep</a>
                    <a href="#" class="btn btn-sm btn-default disabled">Move to</a>
                    <a href="#" class="btn btn-sm btn-default disabled">Categories</a>
                </div>
                <div class="email-content">
                    <table class="table table-email" id="my-inbox-email">
                        <thead>
                        <tr>
                            <th class="email-select"><a href="#" data-click="email-select-all"><i class="fa fa-square-o fa-fw"></i></a></th>
                            <th colspan="2">
                                <div class="dropdown">
                                    <a href="#" class="email-header-link" data-toggle="dropdown">View All <i class="fa fa-angle-down m-l-5"></i></a>
                                    <ul class="dropdown-menu">
                                        <li class="active"><a href="#">All</a></li>
                                        <li><a href="#">Unread</a></li>
                                        <li><a href="#">Contacts</a></li>
                                        <li><a href="#">Groups</a></li>
                                        <li><a href="#">Newsletters</a></li>
                                        <li><a href="#">Social updates</a></li>
                                        <li><a href="#">Everything else</a></li>
                                    </ul>
                                </div>
                            </th>
                            <th>
                                <div class="dropdown">
                                    <a href="#" class="email-header-link" data-toggle="dropdown">Arrange by <i class="fa fa-angle-down m-l-5"></i></a>
                                    <ul class="dropdown-menu">
                                        <li class="active"><a href="#">Date</a></li>
                                        <li><a href="#">From</a></li>
                                        <li><a href="#">Subject</a></li>
                                        <li><a href="#">Size</a></li>
                                        <li><a href="#">Conversation</a></li>
                                    </ul>
                                </div>
                            </th>
                        </tr>
                        </thead>
                        <tbody id="append-emails-here">


                        </tbody>
                    </table>
                    <div class="email-footer clearfix">
                        737 messages
                        <ul class="pagination pagination-sm m-t-0 m-b-0 pull-right">
                            <li class="disabled"><a href="javascript:;"><i class="fa fa-angle-double-left"></i></a></li>
                            <li class="disabled"><a href="javascript:;"><i class="fa fa-angle-left"></i></a></li>
                            <li><a href="javascript:;"><i class="fa fa-angle-right"></i></a></li>
                            <li><a href="javascript:;"><i class="fa fa-angle-double-right"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <!-- end col-10 -->
        </div>
        <!-- end row -->
    </div>
    </div>
    <!-- end #content -->


@endsection

@section('extra_js_footer')

    <script src="{{asset('js/email_demo.js')}}"></script>


    <script>

        $(document).ready(function() {

            InboxV2.init();
        })

        let emails = ()=> {

            $("table#my-inbox-email>tbody").empty("")

            axios.get('/communication/manage/list').then(res => {

                let inbox = '';

                console.log(res.data.inboxes.length);


                for (let i = 0 ; i < res.data.inboxes.length ; i++) {

                    let uri = "{{url('communication/manage/')}}";

                    let url = uri+'/'+res.data.inboxes[i]['uid']+'/read'
                    inbox += ' <tr style="'+res.data.inboxes[i].getColorStyle+'">\n' +
                        '                            <td class="email-select"><a href="#" data-click="email-select-single">' +
                        '<i class="fa fa-square-o fa-fw"></i></a>' +
                        '</td>\n' +
                        '                            <td class="email-sender">\n' +
                        '                                <a href="'+url+'" >'+res.data.inboxes[i].from+'</a>' +
                        '                            </td>' +
                        '                            <td class="email-subject">\n' +
                        '                                <a href="'+url+'" class="email-btn" data-click="email-archive"><i class="fa fa-folder-open"></i></a>\n' +
                        '                                <a href="'+url+'" class="email-btn" data-click="email-remove"><i class="fa fa-trash-o"></i></a>\n' +
                        '                                <a href="'+url+'" class="email-btn" data-click="email-highlight"><i class="fa fa-flag"></i></a>\n' +
                        '                                '+res.data.inboxes[i].subject+'' +
                        '                            </td>\n' +
                        '                            <td class="email-date">'+res.data.inboxes[i].date+'</td>\n' +
                        '                        </tr>'


                }

                $("table#my-inbox-email>tbody").append(inbox)

            })
        }

        emails();



    </script>

@endsection
