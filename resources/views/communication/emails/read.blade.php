@extends('layouts.master')
@section('title','Emails')
@section('extra_css_header')
    <style>

        .email-body{

            color: black !important;
        }

        td {
            color: black !important;
        }
    </style>
@endsection
@section('content')

    @include('layouts.back')

    <div class="email-content">
        {!! $inbox !!}

        <?php



        ?>

        <p style="text-align: center"></p>
        <form class="form-horizontal" method="post" enctype="multipart/form-data">
            {{csrf_field()}}
            <input type="hidden" name="uid" value="{{$uid}}">
            <div class="form-group">
                <label class="col-md-1 control-label">Message</label>
                <div class="col-md-10">
                    <textarea id="textAreaId" name="message"></textarea>
                </div>
            </div>

            <div class="form-group">
                <label class="col-md-1 control-label">Attachment</label>
                <div class="col-md-10">
                    <input type="file" name="image">
                </div>
            </div>

            <div class="modal-footer">
                <button type="submit" class="btn btn-success pull-left">Reply</button>
            </div>
        </form>
    </div>

@endsection

@section('extra_js_footer')

    <script>
        $("#textAreaId").ckeditor()

    </script>

@endsection
