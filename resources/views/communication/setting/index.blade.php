@extends('layouts.master')

@section('title',' Kindly setup your email configurations')


@section('content')



    <form class="form-horizontal" method="post" action="{{route('communication_settings_store')}}">
        @csrf

        <div class="form-group">
            <label class="control-label col-md-2">E-Mail</label>
            <div class="col-md-4">
                <input class="form-control" type="email" name="email" value="{{isset($user) ? $user->email : null}}">
            </div>
        </div>

        <div class="form-group">
            <label class="control-label col-md-2">Password</label>
            <div class="col-md-4">
                <input class="form-control" type="password" name="password" value="{{isset($user) ? json_decode(base64_decode($user->password)) : null}}">
            </div>
        </div>

        <div class="form-group content-full-width center-block">
            <div class="col-md-6">
                <button type="submit" class="btn btn-success">Update</button>
            </div>
        </div>

    </form>


@endsection