<div class="table-responsive">

    <div class="email-footer">
        <form
                class="form-horizontal"
        >
            <div class="form-group">
                <label
                        class="control-label col-md-2">
                    Start Date
                </label>

                <div class="col-md-4">
                    <input
                            type="text"
                            name="startDate"
                            class="date form-control"
                            v-model="startDate"
                            id="start-date"
                    >
                </div>
                <label
                        class="control-label col-md-2">
                    End Date
                </label>

                <div class="col-md-4">
                    <input
                            type="text"
                            name="endDate"
                            class="date form-control"
                            id="end-date"
                            v-model="endDate"
                    >
                </div>

            </div>

            <div class="form-group">
                <div class="email-footer">
                    <button class="btn-success btn" @click.prevent="exportContacts">DownLoad Excel</button>
                    <button class="btn-success btn ">Pull</button>
                </div>
            </div>

        </form>
    </div>
    <div class="email-footer">
        <ul class="nav nav-pills">
            <li><a href="{{ route('communication_manage_contacts' , ['action' => 'thisMonth']) }}">This Month</a></li>
            <li><a href="{{ route('communication_manage_contacts' , ['action' => 'lastMonth']) }}">Last Month</a></li>
            <li><a href="{{ route('communication_manage_contacts' , ['action' => 'thisYear']) }}">This Year</a></li>
        </ul>
    </div>
    <div class="clearfix mt-5 mb-5"></div>
    <a v-if="checkedContacts.length" data-toggle="collapse" href="#collapseBulkSms" role="button" aria-expanded="false" aria-controls="collapseBulkSms" class="btn btn-sm btn-success mb-5">Send BulK SMS</a>

    <div class="collapse" id="collapseBulkSms" v-if="checkedContacts.length">
        <div class="panel">
            <div class="panel panel-body">
                <form
                        method="post"
                        action="{{route('communication_manage_bulk-sms')}}"
                        class="form-horizontal"
                        enctype="multipart/form-data"
                >
                    @csrf
                    <input
                            class="form-control"
                            id="bulk-sms"
                    >
                    <input type="hidden" name="phone" :value="checkedContacts">
                  <div class="email-footer">
                      <button class="btn btn-success btn-sm"> Send </button>
                  </div>
                </form>
            </div>
        </div>
    </div>


    <div class="mb-5 mt-5 table-responsive">
        <table class="table-bordered table table-striped" id="contact-table" v-loading="isLoading">
            <thead>
            <tr>
                <th><input type="checkbox" @change="selectAllChecked" class="select_all"></th>
                <th>Date</th>
                <th>Name</th>
                <th>Tel</th>
                @if( request()->user()->departmentAdmin || request()->user()->can('access-module-component-functionality', 'sale_manage_show-all-sales'))
                    <th> Agent </th>
                @endif
                <th>Edit</th>
            </tr>
            </thead>
            <tbody>

            @foreach($contacts as $contact)
                <tr>
                    <td><input type="checkbox" class="contact-checked" v-model="checkedContacts" name="contactId[]" value="{{ $contact->phone }}"></td>
                    <td> {{ $contact->date }}</td>
                    <td> {{ $contact->name }}</td>
                    <td> {{ $contact->phone }}</td>
                    @if( request()->user()->departmentAdmin || request()->user()->can('access-module-component-functionality', 'sale_manage_show-all-sales'))
                        <td> {{ $contact->agent }} </td>
                    @endif
                    <td><a href="{{ $contact->editPath }}">Edit</a></td>
                </tr>
            @endforeach

            </tbody>
        </table>
    </div>
</div>


