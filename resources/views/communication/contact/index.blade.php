@extends('layouts.master')
@section('title','System contacts')

@section('extra_css_header')
<style>
    input[type="text"].bulkSms{
        height:500px;
        width:500px;
        word-break: break-word;
    }
</style>
@endsection
@section('content')


    <ul class="nav nav-tabs tabs-uzapoint">


        <li class="active">
            <a href="#default-tab-1" data-toggle="tab">
                <span class="">Contact List</span>

            </a>
        </li>

        <li class="">
            <a href="#default-tab-2" data-toggle="tab">
                <span class="">New Contact</span>

            </a>
        </li>


    </ul>

    <div id="app">

        <div class="tab-content">

            <div class="tab-pane fade active in" id="default-tab-1">
                @include('communication.contact.list')
            </div>


            <div class="tab-pane fade in" id="default-tab-2">
                @include('communication.contact.create_edit')
            </div>


        </div>
        <ion-toast-controller></ion-toast-controller>
        <ion-loading-controller></ion-loading-controller>

    </div>



@stop

@section('extra_js_footer')
    @include('layouts.dataTables.tablebuttons')
    @include('layouts.datePicker.datepicker')
    <!-- import CSS -->
    <link rel="stylesheet" href="https://unpkg.com/element-ui/lib/theme-chalk/index.css">
    <!-- import JavaScript -->
    <script src="https://unpkg.com/element-ui/lib/index.js"></script>
    <script>
       $(function () {

           $('input.date').datepicker();

           $("table#contact-table").DataTable();

       })
    </script>
    <script>
        new Vue({
            el: '#app',
            data() {
                return {
                    name: '',
                    phone: '',
                    hasVisited: false,
                    timeout: { type: Number, default: 1000 },
                    contacts:[],
                    xls:'',
                    startDate: '',
                    endDate:'',
                    editPath:'',
                    isLoading: false,
                    checkedContacts: [],
                    date_collected:'',
                    comment:''

                }
            },
            created(){

               // this.appendToContactTable()
            },

            methods: {
                presentAlertCntrl() {
                    let alertCtrl = document.querySelector('ion-loading-controller');

                    return alertCtrl.create({
                        duration: 2000,
                        message: 'submitting ....'
                    })
                },
                async presentToast(message) {
                    const toastController = document.querySelector('ion-toast-controller');
                    const toast = await toastController.create({
                        message: message,
                        duration: 6000,
                        color: '#2befbe',
                        position:'top',
                        animated: true
                    });
                    return await toast.present();


                },
                addContact() {

                    let data = {
                        name: this.name,
                        phone: this.phone,
                        hasVisited: this.hasVisited,
                        editPath: this.editPath,
                    };

                    this.presentAlertCntrl().then(presentAlert => presentAlert.present());


                    let vm = this;

                    axios.post('{{route('communication_manage_contacts_store')}}', data).then(res => {

                        this.contact = []

                        swal({text: res.data.message , icon: 'success'})

                        //this.appendToContactTable();
                        this.name = '';
                        this.phone = '';
                        this.hasVisited = false;
                        this.editPath = '';
                        this.comment = '';
                        this.date_collected = '';


                    }).catch(err => {

                        vm.presentAlertCntrl().then(presentAlert => presentAlert.dismiss());
                        vm.presentAlertCntrl().then(l => {
                            setTimeout(function() {
                                l.dismiss()
                            }, this.timeout)
                            return l.present()
                        });

                        if (err.response.status === 500) {
                            swal({
                                title:err.response.data.errorText,
                                text:err.response.data.message,
                                icon: 'warning'
                            })
                        }
                        let messages = err.response.data.message;
                        if (err.response.status === 422) {

                            for (let key in messages) {
                                if (messages.hasOwnProperty(key)) {
                                    swal({
                                        title: `${key}`,
                                        text: `${messages[key]}`,
                                        icon: 'warning'
                                    })

                                    console.log(`${key} : ${messages[key]}`)
                                }
                            }
                            /*err.response.data.message.forEach((message , index) => {
                                swal({
                                    title: index,
                                    text: message,
                                    icon: 'warning'
                                })
                            })*/



                            console.error(messages)
                        }

                    });

                },

                removeTableContent(){
                    $("table#contact-table>tbody").empty()
                },
                appendDataTable(){
                    var table =  $("table#contact-table").DataTable();
                    table.destroy();

                    setTimeout((hand) =>{
                        $("table#contact-table").DataTable({
                            dom: "Bfrtip",
                            buttons: [{
                                extend: "copy",
                                className: "btn-sm"
                            }, {
                                extend: "csv",
                                className: "btn-sm"
                            }, {
                                extend: "excel",
                                className: "btn-sm"
                            }, {
                                extend: "pdf",
                                className: "btn-sm"
                            },],
                            responsive: !0
                        })
                    }, 1000)
                },
                appendToContactTable(){

                    axios.get('{{route('communication_manage_contacts_list')}}')
                        .then( res => {
                            this.contacts = [];
                            this.contacts = res.data.data;
                            this.appendDataTable();
                        });
                },

                destroyTable(){
                    var table =  $("table#contact-table").DataTable();
                    table.destroy();

                    $("table#contact-table>tbody").empty();
                    setTimeout((hand) =>{
                        $("table#contact-table").DataTable()
                    }, 1000)
                },


                ImportContact(){
                    let formData = new FormData($('form.import-form')[0])
                    $(this).attr('disabled', true)

                    this.presentAlertCntrl().then(presentAlert => presentAlert.present());
                    this.xls = '';

                    axios.post('{{route('communication_manage_contacts_import') }} ', formData)
                        .then( res => {
                            this.contacts = [];
                            this.appendToContactTable();
                            this.presentAlertCntrl().then(presentAlert => presentAlert.dismiss());
                            (this).attr('disabled', false)
                        }).catch( err => {
                            console.log(err.response)
                    });

                },


                /********** ON Pull ******/

                pullReport() {
                    this.endDate = $("#end-date").val();
                    this.startDate = $("#start-date").val();


                    axios.get('{{url('communication/manage/contacts/list')}}?startDate='+this.startDate+'&endDate='+this.endDate)
                        .then( res => {
                            console.log(res.data.data)
                            this.contacts = res.data.data;
                        });

                },

                /********** ON Pull ******/

                exportContacts() {
                    this.endDate = $("#end-date").val();
                    this.startDate = $("#start-date").val();

                    window.location.assign('{{url('communication/manage/contacts/export')}}?startDate='+this.startDate+'&endDate='+this.endDate)

                },
                GetAllVisited() {
                    this.contacts = [];

                    axios.get('{{url('communication/manage/contacts/list')}}?action=visited')
                        .then( res => {
                            this.contacts = [];
                            this.contacts = res.data.data;
                            this.destroyTable();
                        });
                },
                thisMonth() {
                    this.contacts = [];

                    $("table#contact-table>tbody").empty();

                    axios.get('{{url('communication/manage/contacts/list')}}?action=thisMonth')
                        .then( res => {
                            this.contacts = [];
                            this.contacts = res.data.data;
                            setTimeout((hand) =>{
                                $("table#contact-table").DataTable()
                            }, 1000)
                        });
                },
                lastMonth() {
                    this.contacts = [];
                    this.removeTableContent();
                    axios.get('{{url('communication/manage/contacts/list')}}?action=lastMonth')
                        .then( res => {
                            this.contacts = [];
                            this.contacts = res.data.data;
                            this.appendDataTable();
                        });

                },
                thisYear() {
                    this.contacts = [];
                    this.removeTableContent();
                    axios.get('{{url('communication/manage/contacts/list')}}?action=thisYear')
                        .then( res => {
                            this.contacts = [];
                            this.contacts = res.data.data;
                            this.appendDataTable()
                        });

                },
                all() {
                    this.contacts = [];
                    this.isLoading = true;
                    this.removeTableContent();
                    axios.get('{{url('communication/manage/contacts/list')}}')
                        .then( res => {
                            this.contacts = [];
                            this.contacts = res.data.data;
                            this.appendDataTable();
                            this.isLoading = false;
                        });


                },
                notVisited() {
                    this.contacts = [];
                    this.removeTableContent();
                    axios.get('{{url('communication/manage/contacts/list')}}?action=notVisited')
                        .then( res => {
                            console.table(this.contacts)
                            this.contacts = res.data.data;
                            this.appendDataTable();
                        });

                },
                selectAllChecked(){
                    let checked = [];
                    $('.contact-checked').each(function () {
                        if ($(this).is(':checked')) {
                            checked.push($(this).val());
                            console.log(checked , 'checked');

                            console.log(this.checkedContacts , 'checked contacts')

                            //this.checkedContacts.push($(this).val())

                        }
                    });
                    this.checkedContacts = checked;
                    console.log(this.checkedContacts , 'checked contacts')
                }
            }
        });


        $(document).on('click','input#bulk-sms', function () {

            let value = $("select.template option:selected").data('value');


            var $txtarea = $("<textarea />");
            $txtarea.attr("id", this.id);
            $txtarea.attr("rows", 8);
            $txtarea.attr("name", "message");
            $txtarea.attr("cols", 60);
            $txtarea.val(value);
            $(this).replaceWith($txtarea);
            $(this).text(value)



            $("textarea#bulk-sms").focus();

        });

        $('.contact-checked').each(function () {
            var total = $('.contact-checked').length;
            $(this).on('click', function () {
                var i = 0;
                $('.contact-checked').each(function () {
                    if ($(this).is(':checked')) {
                        i++;
                    }
                });
                if (i === total) {
                    $('.select_all').prop('checked', true);
                } else {
                    $('.select_all').prop('checked', false);
                }
            });
        });
        $('.select_all').on('click', function (e) {
            if ($(this).is(':checked')) {
                $('.contact-checked').each(function () {
                    $(this).prop('checked', true);
                });
            } else {
                $('.contact-checked').each(function () {
                    $(this).prop('checked', false);
                });
            }
        });
    </script>

@endsection
