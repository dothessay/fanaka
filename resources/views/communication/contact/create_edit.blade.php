<fieldset style="border: 1px ; margin-bottom:  40px">
    <legend>Upload CSV</legend>

    <span style="clear: both; margin: 3em">
        <a href="{{asset('uploads/samples/imports_contacts.xlsx')}}" target="_blank" class="btn btn-warning" >Download Format excel Sample</a>
    </span>

    <form method="post" class="import-form form-horizontal" enctype="multipart/form-data">
        <div class="form-group">
            <label for="Has Visited" class="control-label col-md-2">Select A Excel File</label>
            <div class="col-md-4">
                <input
                        type="file"
                        name="xls"
                        id="xls"
                        v-model="xls"
                >
            </div>
        </div>

        <div class="form-group col-md-3">
            <button type="button" class="btn btn-success" fill="outline"  @click.prevent="ImportContact()">Submit</button>
        </div>

    </form>
</fieldset>
<fieldset>
    <legend>New Contact</legend>
    <form method="post" class="form-horizontal">
        <div class="form-group">
            <label for="Name" class="control-label col-md-2">Name</label>
            <div class="col-md-4">
                <input
                        v-model="name"
                        class="form-control"
                        name="name"
                        id="name"
                >
            </div>

            <label for="Phone" class="control-label col-md-2">Phone</label>
            <div class="col-md-4">
                <input
                        v-model="phone"
                        class="form-control"
                        name="phone"
                        id="phone"
                >
            </div>
        </div>
        <div class="form-group">
            <label for="hasVisted" class="control-label col-md-2">Has Visited?</label>
            <div class="col-md-4">
                <input
                        type="checkbox"
                        class="form-check-input"
                        v-model="hasVisited"
                        name="hasVisted"
                        id="hasVisted"
                >
            </div>
            <label for="date_collected" class="control-label col-md-2">Date Collected?</label>
            <div class="col-md-4">
                <input
                        type="text"
                        v-model="date_collected"
                        name="date_collected"
                        id="date_collected"
                        class="form-control date"
                >
            </div>

        </div>

        <div class="form-group">
            <label for="comment" class="control-label col-md-2">Comment</label>
            <div class="col-md-10">

                    <textarea id="comment" name="comment">
                    </textarea>
                </div>
        </div>


        <div class="form-group row">
            <ion-button fill="outline" success @click.prevent="addContact()">Submit</ion-button>
        </div>

    </form>
</fieldset>

