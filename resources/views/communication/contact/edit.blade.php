@extends('layouts.master')
@section('title','Edit Contact details ' . $contact->name)
@section('content')

 @component('layouts.partials.panel')
     <section>
         <div class="col-md-10 offset-2">
             <form
                     class="form-bordered form-horizontal"
                     action="{{ route('communication_manage_contacts_update',['contact'  => $contact->id]) }}"
                     method="post"
                     enctype="multipart/form-data"
                     autocomplete="off"
             >
                 {{ csrf_field() }}
                 {{ method_field('PATCH') }}

                 <div class="form-group">
                     <label class="col-md-2 control-label">Client Name: <span class="required-uzapoint">*</span> </label>
                     <div class="col-md-4">
                         <input
                                 type="text"
                                 name="name"
                                 value="{{ old('name', $contact->name )}}"
                                 class="form-control"
                         >
                     </div>

                     <label class="col-md-2 control-label">Tel: <span class="required-uzapoint">*</span> </label>
                     <div class="col-md-4">
                         <input
                                 type="text"
                                 name="phone"
                                 value="{{ old('phone', $contact->phone )}}"
                                 class="form-control">
                     </div>
                 </div>

                 <div class="form-group">
                     <label class="col-md-2 control-label">Has Visited: </label>
                     <div class="col-md-4">
                         <input
                                 type="checkbox"
                                 name="has_visited"
                                 @if( $contact->hasVisited) checked @endif
                                 class="form-check-input">
                         <div class="help-block">
                                <span class="required-uzapoint">
                                    Tick if the client has visited
                                </span>
                         </div>
                     </div>

                     <label class="col-md-2 control-label">Date Visited: </label>
                     <div class="col-md-4">
                         <input
                                 type="text"
                                 name="date"
                                 id="date"
                                 value="{{ old('date', $contact->date )}}"
                                 class="form-control">

                         <div class="help-block">
                                <span class="required-uzapoint">
                                    If the client has visited then indicated the date
                                </span>
                         </div>
                     </div>
                 </div>

                 <div class="form-group">
                     <label class="col-md-2 control-label">Date Collected: </label>
                     <div class="col-md-4">
                         <input
                                 type="text"
                                 name="collected_at"
                                 id="dateCollected"
                                 value="{{ old('collected_at', $contact->getMeta('collected_at','')  )}}"
                                 class="form-control">

                         <div class="help-block">
                                    <span class="required-uzapoint">
                                        Enter the dated collected
                                    </span>
                         </div>
                     </div>

                     <label class="col-md-2 control-label">Project Visited: </label>
                     <div class="col-md-4">
                         <select class="form-control selectpicker"
                                 data-live-search="true"
                                 data-size="5"
                                 multiple
                                 name="project[]">
                             @foreach($products as $property)
                                 <option value="{{ $property->id }}"
                                         @if(in_array($property->id, $projects))
                                         selected
                                         @endif
                                 >
                                     {{ $property->name() }}
                                 </option>
                             @endforeach
                         </select>
                         <div class="help-block">
                             <span class="required-uzapoint">If the client has visited then indicated the projects</span>
                         </div>
                     </div>
                 </div>
                 <div class="email-footer text-center text-uzapoint-bold text-uppercase">Follow up Schedule </div>

                 <div class="form-group">
                     <label class="control-label col-md-2">Last Follow up Date</label>
                     <div class="col-md-4">
                         <input
                             type="text"
                             name="last_follow_up"
                             id="lastFollowupDate"
                             value="{{ old('last_follow_up', $contact->getMeta('last_follow_up','') )}}"
                             class="form-control">
                         <div class="help-block">
                             <span class="required-uzapoint">
                                 Enter the last called date
                             </span>
                         </div>
                     </div>

                     <label class="control-label col-md-2">Next Follow up date</label>
                     <div class="col-md-4">
                         <input
                                 type="text"
                                 name="next_follow_up"
                                 id="nextFollowupDate"
                                 value="{{ old('next_follow_up', $contact->getMeta('next_follow_up','') )}}"
                                 class="form-control">

                         <div class="help-block">
                                    <span class="required-uzapoint">
                                        Enter the last follow up date
                                    </span>
                         </div>
                     </div>

                 </div>
                 <div class="form-group">
                     <label class="col-md-2 control-label">Status</label>
                     <div class="col-md-4">
                         <select
                                 class="form-control selectpicker"
                                 data-live-search="true"
                                 data-size="3"
                                 data-theme="white"
                                 name="status"
                         >
                             <option value="cold" @if($contact->getMeta('status','') === 'cold') selected @endif>Cold</option>
                             <option value="warm" @if($contact->getMeta('status','') === 'warm') selected @endif>Warm</option>
                             <option value="hot" @if($contact->getMeta('status','') === 'hot') selected @endif>Hot</option>
                         </select>
                     </div>
                 </div>
                 <div class="form-group">
                     <label class="control-label col-md-2">Comment</label>
                     <div class="col-md-10">
                             <textarea
                                     name="comment"
                             >     {!!  old('comment', $contact->getMeta('comment','') )  !!}
                         </textarea>
                     </div>
                 </div>


                 <div class="email-footer">
                     <button class="btn btn-success">Update</button>
                 </div>
             </form>
         </div>
     </section>
    @endcomponent

@endsection

@section('extra_js_footer')
    @include('layouts.form')
    @include('layouts.datePicker.datepicker')
    <script>
        $("input#date").datepicker({
            maxDate: new Date()
        });
        $("input#lastFollowupDate").datepicker({
        });
        $("input#nextFollowupDate").datepicker({

        });

        $("input#dateCollected").datepicker({
            maxDate: new Date()
        })
    </script>
  @endsection

