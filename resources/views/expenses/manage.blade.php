@extends('layouts.master')
@section('title','Add Expenses')
@section('content')


    <ul class="nav nav-tabs tabs-uzapoint">


        <li class="active ">
            <a href="#default-tab-1" data-toggle="tab">
                <span>Add Expenses</span>
            </a>
        </li>
        <li class="">
            <a href="#default-tab-2" data-toggle="tab">
                <span>List Expenses</span>
            </a>
        </li>

    </ul>


    <div class="tab-content">

        <div class="tab-pane fade active  in" id="default-tab-1">


            <form class="form-horizontal form-bordered" action="{{route('expenses_manage_add_store')}}" method="post">
                {{csrf_field()}}

                <div class="form-group">
                    <label class="control-label col-md-2">Amount</label>
                    <div class="col-md-4">
                        <input name="amount" class="form-control">
                    </div>
                </div>

                <div class="form-group">


                    <label class="control-label col-md-2">Purpose</label>
                    <div class="col-md-10">
                        <textarea name="purpose" cols="25"
                                  rows="5"></textarea>
                    </div>
                </div>

                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">Add</button>
                </div>
            </form>

        </div>

        <div class="tab-pane fade in" id="default-tab-2">


            <form class="form-horizontal">
                <div class="form-group">
                    <label class="col-md-2">Start Date</label>
                    <div class="col-md-4">
                        <input type="text" class="form-control datepicker" name="start_date">
                    </div>

                    <label class="col-md-2">End Date</label>
                    <div class="col-md-4">
                        <input type="text" class="form-control datepicker" name="end_date">
                    </div>
                </div>


                <div class="modal-footer">
                    <button class="btn btn-sm btn-info" type="pull">Pull Report</button>
                </div>
            </form>



            <table class="table data-table table-bordered table-responsive table-striped" id="data-table" width="100%">
                <thead>
                <tr>
                    <th>Date</th>
                    <th>Amount</th>
                    <th>Purpose</th>
                    <th>Transacted By</th>
                </tr>
                </thead>
                <tbody>
                @foreach($expenses as $expense)
                    <tr>
                        <td>{{$expense->created_at}}</td>
                        <td>{{number_format((double) $expense->amount , 2)}}</td>
                        <td>{{$expense->purpose}}</td>
                        <td>{{$expense->user->fullName()}}</td>
                    </tr>
                @endforeach
                </tbody>
            </table>


        </div>
    </div>







@endsection

@section('extra_js_footer')

    @include('layouts.dataTables.tablebuttons')
    @include('layouts.dataTables.buttons')
    @include('layouts.datePicker.datepicker')
    <script>
        $('.datepicker').datepicker({
            minDate : new Date()
        });
    </script>

@endsection
