<div class="email-btn-row hidden-xs">
    <a role="button" type="button" data-toggle="collapse" data-target="#collapsePetty" aria-expanded="false"
       aria-controls="collapsePetty" class="btn btn-sm btn-inverse"><i class="fa fa-plus m-r-5"></i> Update Petty
        Cash</a>
</div>

<div class="collapse" id="collapsePetty">

    <form class="form-horizontal form-bordered" action="{{route('expenses_add_store-petty')}}" method="post">
        {{csrf_field()}}


        <div class="form-group">
            <label class="control-label col-md-2">Account</label>
            <div class="col-md-4">
                <select
                        name="chart"
                        class="form-group selectpicker"
                        data-live-search="true"
                        data-size="6"
                >
                    @foreach($charts as $chart)
                        <option value="{{ $chart->id }}"> {{ $chart->name . ' (' . $chart->code .' )'}}</option>
                    @endforeach
                </select>
            </div>
        </div>


        <div class="form-group">
            <label class="control-label col-md-2">Amount</label>
            <div class="col-md-4">
                <input name="amount" class="form-control">
            </div>
        </div>

        <div class="email-footer">
            <div class=" form-group">
                <label class="col-md-2"></label>
                <button type="submit" class="btn btn-primary">Credit</button>
            </div>
        </div>
    </form>

</div>

<table class="table table-striped data-table table-bordered" id="cashPetty">
    <thead>
    <tr>
        <th>Date</th>
        <th>Authorised By</th>
        <th>Credit</th>
        <th>Debit</th>
        <th>Running Balance</th>
        <th>Purpose</th>
    </tr>
    </thead>
    <tbody>
    <?php $debit = 0; $credit = 0; $balance = 0?>
    @foreach($petties as $petty)
        <?php $debit += floatval($petty['debit']); $credit += floatval($petty['credit']) ?>
        <tr>
            <td>{{$petty['date']}}</td>
            <td>{{$petty['user']}}</td>
            <td>{{ config('config.currency') .' '.  number_format(floatval($petty['credit']) , 2) }}</td>
            <td>{{config('config.currency') .' '.   number_format( floatval($petty['debit'] ), 2) }}</td>
            <td>{{ config('config.currency') .' '.  $petty['balance']  }}</td>
            <td width="0.06">{{$petty['note']}}</td>
        </tr>
    @endforeach
    </tbody>
    <tfoot>
    <tr>
        <td>Total:</td>
        <td></td>
        <td><strong>{!! config('config.currency') .' '.  number_format(floatval($credit) , 2) !!}</strong></td>
        <td><strong>{!! config('config.currency') .' '.  number_format(floatval($debit) , 2) !!}</strong></td>
        <td>
            <strong>{!! config('config.currency') .' '.  number_format(floatval($credit) - floatval($debit)  , 2) !!}</strong>
        </td>
        <td></td>
    </tr>
    </tfoot>
</table>

