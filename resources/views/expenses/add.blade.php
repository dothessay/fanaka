<div class="email-btn-row hidden-xs">
    <a role="button" type="button" data-toggle="collapse" data-target="#collapseExample" aria-expanded="false" aria-controls="collapseExample" class="btn btn-sm btn-inverse"><i class="fa fa-plus m-r-5"></i> New Expense</a>
</div>

<div class="collapse" id="collapseExample">
    <form class="form-horizontal form-bordered" action="{{route('expenses_manage_add_store')}}" method="post">
        {{csrf_field()}}

        <div class="form-group">
            <label class="control-label col-md-2">Account</label>
            <div class="col-md-4">
                <select
                        name="chart"
                        class="form-group selectpicker"
                        data-live-search="true"
                        data-size="6"
                >
                    @foreach($charts as $chart)
                        <option value="{{ $chart->id }}"> {{ $chart->name . ' (' . $chart->code .' )'}}</option>
                    @endforeach
                </select>
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-md-2">Amount</label>
            <div class="col-md-4">
                <input name="amount" class="form-control">
            </div>
        </div>


        <div class="form-group">


            <label class="control-label col-md-2">Purpose</label>
            <div class="col-md-4">
                        <input type="text" name="purpose" class="form-control" style="min-height: 100px ; padding: 0px">
            </div>
        </div>

        <div class="email-footer">
            <button type="submit" class="btn btn-primary">Add</button>
        </div>
    </form>

</div>
<form class="form-horizontal">
    <div class="form-group">
        <label class="col-md-2">Start Date</label>
        <div class="col-md-4">
            <input type="text" class="form-control datepicker" name="start_date">
        </div>

        <label class="col-md-2">End Date</label>
        <div class="col-md-4">
            <input type="text" class="form-control datepicker" name="end_date">
        </div>
    </div>


    <div class="email-footer">
        <button class="btn btn-sm btn-info" type="pull">Pull Report</button>
    </div>
</form>



<table class="table data-table table-bordered table-responsive table-striped" id="data-table" width="100%">
    <thead>
    <tr>
        <th>Date</th>
        <th>Amount</th>
        <th>Purpose</th>
        <th>Account</th>
        <th>Transacted By</th>
    </tr>
    </thead>
    <tbody>
    <?php $total = 0?>
    @foreach($officeExpenses as $expense)
        <?php $total += floatval( $expense->amount) ?>
        <tr>
            <td>{{$expense->created_at}}</td>
            <td>{{config('config.currency') .' '.  number_format( floatval( $expense->amount ) , 2)}}</td>
            <td>{{$expense->purpose}}</td>
            <td>{{$expense->chart ? $expense->chart->name : ''}}</td>
            <td>{{$expense->user->fullName()}}</td>
        </tr>
    @endforeach
    </tbody>
    <tfoot>
    <tr>
        <td>Total: </td>
        <td><strong>{{ config('config.currency') .' '. number_format(floatval($total) , 2) }}</strong> </td>
        <td></td>
        <td></td>
    </tr>
    </tfoot>
</table>
