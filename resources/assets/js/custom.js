import swal from 'sweetalert';

$(document).on('click','a.releasePlot', function () {

    let releaseType = $(this).data('type');

    let plotId = $(this).data('id');


    swal({
        title: "Are you sure?",
        text: "Once released, you will not be able to recover this this "+releaseType +" !",
        icon: "warning",
        buttons: true,
        dangerMode: true,
    })
        .then((willDelete) => {
            if (willDelete) {

                axios.post('plots/manage/release', {id: plotId}).then(res => {
                    swal("Plot has been released successfully", {
                        icon: "success",
                    });
                    window.location.href = window.axios.defaults.baseURL+"/plots/manage/details/"+plotId

                }).catch(err => {

                    swal("We could release the plot!", {
                        icon: "danger",
                    });

                })

            } else {


            }
        });
})




$(document).on('click','input.sms', function () {

    let value = $(this).val();

    let name = $(this).attr('name');

    var $txtarea = $("<textarea />");
    $txtarea.attr("id", this.id);
    $txtarea.attr("rows", 8);
    $txtarea.attr("name", name);
    $txtarea.attr("cols", 60);
    $txtarea.val(value);

})



$(document).on('change','select#type', function () {

    var myText = "Id No";

    if ($("select#type option:selected").text() == 'company'){

        $("span.replaceId").html("Registration no")
        $("span.replaceId").text("Registration no")
        return '';

    }
    $("span.replaceId").html(myText)
    $("span.replaceId").text(myText)
    
})
