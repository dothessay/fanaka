<?php

namespace Codex\Permission;

use App\DB\Role\Component;
use App\DB\Role\Functionality;
use App\DB\Role\Module;
use App\User;
use Illuminate\Support\Facades\Gate;


class Permissions
{

    public static function defineGates()
    {
        Gate::define('access-module', 'Codex\Permission\Permissions@checkAccessToModule');

        Gate::define('access-module-component', 'Codex\Permission\Permissions@checkAccessToModuleComponent');

        Gate::define('access-module-component-functionality',
            'Codex\Permission\Permissions@checkAccessToModuleComponentFunctionality');

    }

    public static function checkAccessToModule(User $user , $module)
    {

        if(! $user->role->canAccessModule($module)){

            if(self::userHasSpecialPermissionToModule($user, $module)){
                return true;
            }
            return false;
        }
        if($user->isModuleDenied($module)){
            return false;
        }

        return true;

    }

    private static function userHasSpecialPermissionToModule(User $user, $module)
    {
        if($user->isModuleAllowed($module)){
            return true;
        }
        return false;
    }




        /*if ( ! $user->role->canAccessModule($module))
        {


            if ($user->isModuleDenied($user , $module))
            {


            }



        }

         return true;

        $hasAccess = true;

        if (! self::userHasSpecialAccessToModule($user, $module))
        {

            $hasAccess = false;





        }


        return $hasAccess;

    }


    private static function userHasSpecialAccessToModule(User $user , $module)
    {
        if (  $user->isModuleDenied($module))
        {
            return false;
        }
        return true;
    }*/

    /*public static function checkAccessToModuleComponent(User $user ,  $component)
    {

        $hasAccess = true;


        if (! self::userHasSpecialAccessToComponent($user, $component))
        {

            $hasAccess = false;


            if (! $user->role->canAccessComponent($component))
            {

                $hasAccess = false;


            }

        }
        return $hasAccess;




    }


    private static function userHasSpecialAccessToComponent(User $user , $component)
    {
        
        if ( ! $user->isComponentsDenied($component))
        {
            return true;

        }

        return false;
    }*/



    public static function checkAccessToModuleComponent(User $user, $component)
    {


        if(! $user->role->canAccessComponent($component)){
            if(self::userHasSpecialPermissionToComponent($user, $component)){
                return true;
            }
            return false;
        }
        if($user->isComponentsDenied($component)){
            return false;
        }

        return true;


    }

    private static function userHasSpecialPermissionToComponent(User $user, $component){
        if($user->isComponentsAllowed($component)){
            return true;
        }
        return false;
    }



    /*public static function checkAccessToModuleComponentFunctionality(User $user ,  $functionality)
    {

        $hasAccess =  true;


        if( ! self::userHasSpecialAccessToFunctionality($user , $functionality))
        {
            $hasAccess= false;

            if (  ! $user->role->canAccessFunctionalities($functionality))
            {
                $hasAccess = false;



            }


        }

        return $hasAccess;


    }


    private static function userHasSpecialAccessToFunctionality(User $user , $functionality)
    {


        if (! $user->isFunctionalitiesDenied($functionality))
        {
            return true;

        }

        return false;
    }*/


    public static function checkAccessToModuleComponentFunctionality(User $user, $functionality)
    {

        if(!$user->role->canAccessFunctionalities($functionality)){

            if(self::userHasSpecialPermissionToFunctionality($user, $functionality)){
                return true;
            }
            return false;
        }
        if($user->isFunctionalitiesDenied($functionality)){
            return false;
        }

        return true;
    }

    private static function userHasSpecialPermissionToFunctionality(User $user, $functionality){
        if($user->isFunctionalitiesAllowed($functionality)){
            return true;
        }
        return false;
    }


}

