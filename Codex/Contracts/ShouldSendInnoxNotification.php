<?php
/**
 * Created by Philip Njuguna.
 * User: Philip Njuguna
 * Date: 11/12/18
 * Time: 9:08 AM
 */

namespace Codex\Contracts;


interface ShouldSendInnoxNotification
{

    public function sendNotification(array  $data);
}