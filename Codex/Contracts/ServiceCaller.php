<?php
/**
 * Created by PhpStorm.
 * User: philip
 * Date: 2/25/18
 * Time: 9:42 AM
 */

namespace Codex\Contracts;


interface ServiceCaller
{

    public function successResponse($message);
    public function failureResponse($message);


}