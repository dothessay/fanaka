<?php

namespace Codex\Contracts;


use Illuminate\Support\Collection;

interface PdfOutput
{
    public function outPut(Collection $collection);

}