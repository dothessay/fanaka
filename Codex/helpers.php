<?php

use App\User;

function statisticWidget($title, $data, $href = "javascript:void(0)", $color = "bg-orange")
{

    $outpout = '<a href="'.$href.'"> <div class="col-md-3 col-sm-6">';
    $outpout .= '<div class="widget widget-stats '.$color.'">';
    $outpout .= '<div class="stats-icon stats-icon-lg">';
    $outpout .= '<i class="fa fa-globe ion-information-circled"></i></div>';
    $outpout .= '<div class="stats-title">';
    $outpout .= $title;
    $outpout .= '</div><div class="stats-number">';
    $outpout .= $data;
    $outpout .= '</div></div></div></a>';


    return $outpout;



}



function reportWidget($title, $href)
{
    $outpout = '<a href="'.$href.'"> <div class="col-md-3 col-sm-6">';
    $outpout .= '<div class="widget widget-stats " style="background-color: #1b6d85">';
    $outpout .= '<div class="stats-icon stats-icon-lg">';
    $outpout .= '<i class="fa fa-globe ion-information-circled"></i></div>';
    $outpout .= '<div class="stats-title">';
    $outpout .= $title;
    $outpout .= '</div><div class="stats-number">';
    $outpout .= '</div></div></div></a>';

    return $outpout;

}


function generateDBCode($businessCode, $code)
{
    return $businessCode . '_' . cleanCode($code);
}


function cleanCode($dirtyCode)
{
    $cleanCode = filter_var($dirtyCode, FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_HIGH);

    $cleanCode = preg_replace('/[^A-Za-z0-9\-]/', '_', strtolower(trim($cleanCode)));

    return $cleanCode;

}

function trim_all( $str , $what = NULL , $with = ' ' )
{
    if( $what === NULL )
    {
        //  Character      Decimal      Use
        //  "\0"            0           Null Character
        //  "\t"            9           Tab
        //  "\n"           10           New line
        //  "\x0B"         11           Vertical Tab
        //  "\r"           13           New Line in Mac
        //  " "            32           Space

        $what   = "\\x00-\\x20";    //all white-spaces and control chars
    }

    return trim( preg_replace( "/[".$what."]+/" , $with , $str ) , $what );
}



function fanakaConfig($value , $default = "")
{



    return isset( \App\DB\Setting\Setting::where('key',$value)->first()->value) ? \App\DB\Setting\Setting::where('key',$value)->first()->value : $default;
}


function setting($value , $default = "")
{



    return isset( \App\DB\Setting\Setting::where('key',$value)->first()->value) ? \App\DB\Setting\Setting::where('key',$value)->first()->value : $default;
}



define('NUM_BIG_BLOCK_DEPOT_BLOCKS_POS', 0x2c);
define('SMALL_BLOCK_DEPOT_BLOCK_POS', 0x3c);
define('ROOT_START_BLOCK_POS', 0x30);
define('BIG_BLOCK_SIZE', 0x200);
define('SMALL_BLOCK_SIZE', 0x40);
define('EXTENSION_BLOCK_POS', 0x44);
define('NUM_EXTENSION_BLOCK_POS', 0x48);
define('PROPERTY_STORAGE_BLOCK_SIZE', 0x80);
define('BIG_BLOCK_DEPOT_BLOCKS_POS', 0x4c);
define('SMALL_BLOCK_THRESHOLD', 0x1000);
// property storage offsets
define('SIZE_OF_NAME_POS', 0x40);
define('TYPE_POS', 0x42);
define('START_BLOCK_POS', 0x74);
define('SIZE_POS', 0x78);
define('IDENTIFIER_OLE', pack("CCCCCCCC",0xd0,0xcf,0x11,0xe0,0xa1,0xb1,0x1a,0xe1));


function GetInt4d($data, $pos) {
    $value = ord($data[$pos]) | (ord($data[$pos+1])	<< 8) | (ord($data[$pos+2]) << 16) | (ord($data[$pos+3]) << 24);
    if ($value>=4294967294) {
        $value=-2;
    }
    return $value;
}

// http://uk.php.net/manual/en/function.getdate.php
function gmgetdate($ts = null){
    $k = array('seconds','minutes','hours','mday','wday','mon','year','yday','weekday','month',0);
    return(array_comb($k,split(":",gmdate('s:i:G:j:w:n:Y:z:l:F:U',is_null($ts)?time():$ts))));
}

// Added for PHP4 compatibility
function array_comb($array1, $array2) {
    $out = array();
    foreach ($array1 as $key => $value) {
        $out[$value] = $array2[$key];
    }
    return $out;
}

function v($data,$pos) {
    return ord($data[$pos]) | ord($data[$pos+1])<<8;
}

