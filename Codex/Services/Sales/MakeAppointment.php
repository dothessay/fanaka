<?php 


namespace Codex\Services\Sales;
use App\DB\Sale\Appointment;
use App\Jobs\SendSmsNotifications;
use Carbon\Carbon;
use Codex\Classes\ServiceAbstract;

Class MakeAppointment extends ServiceAbstract
{

	/**
	* @param array $data
	* @param array $options
	*/
	protected function execute(array $data, array $options = [])
	{


	    $appointment = Appointment::create([
	        'full_name'  => $data['full_name'],
	        'agent_id'   => auth()->id(),
	        'user_id'    => auth()->id(),
	        'phone_number'    => $data['phone_number'],
	        'date_time'  => Carbon::parse($data['date']),
            'project_ids' => serialize($data['project_id']),
            'send_notification'  => isset($data['send_notification']) ? true : false
        ]);

	    /*dispatch(new SendSmsNotifications([
	        'to'  => $data['phone_number'],
            "message"   => "Hi " . $appointment->full_name . ",                                                         Please note that you have been booked for a site visit to our project(s);{$appointment->getProjects()} on " .Carbon::parse( $appointment->date_time)->format('l jS \\ F Y '). " at ".Carbon::parse( $appointment->date_time)->format('h:i A') .". Kindly ensure that you arrive early for convenience. Thank You and have a great day!"
        ]));*/

	    return "successfully made an appointment on ".Carbon::parse($data['date'])->format('Y-F-D H:i:s');
	}

	/**
	* @param array $data
	* @param array $options
	*/
	protected function filter(array $data, array $options = [])
	{

	// TODO: Implement execute() method.
	}
} 
