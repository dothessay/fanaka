<?php
/**
 * Created by PhpStorm.
 * User: philip
 * Date: 3/6/18
 * Time: 1:14 PM
 */

namespace Codex\Services\Sales;


use App\DB\Accounting\Account;
use App\Sale\Invoice;
use App\Sale\SalePayment;
use Carbon\Carbon;
use Codex\Classes\Handlers\SystemSms;
use Codex\Classes\ServiceAbstract;

class UpdateInvoiceService extends ServiceAbstract
{

    /**
     * @param array $data
     * @param array $options
     */
    protected function execute(array $data, array $options = [])
    {
        $invoice = Invoice::where('id', $data['id'])->first();

        $request = request();

        $request['upload_type'] =  'receipt';

        $data['amount'] = str_replace(',','',$data['amount']);
        $data['amount'] = str_replace(' ','',$data['amount']);

        $invoice->due_date =  Carbon::now()->addMonth(1);
        $invoice->total_paid = $invoice->total_paid + (double)$data['amount'];

        if ($invoice->total_paid == $invoice->total_amount)
        {
            $invoice->is_paid = true;
            foreach ($invoice->invoiceItems as $invoiceItem) {

                $plot = $invoiceItem->plot;

                $plot->is_sold = true;
                $plot->is_completely_paid = true;

                $plot->save();


            }
        }
        else{
            $data['payment_type'] = "installment";
        }

        /*if ($invoice->total_paid > $invoice->total_amount)
        {
           return "You cannot pay more than the balance, try again!";
        }*/
        $invoice->save();

        $sale = $invoice->sale;

        $data['reference_code'] = $data['payment_method'];


        if (isset($data['check_bank_name']))
        {
            $data['bank_name'] = $data['check_bank_name'];
        }


        if (isset($data['mpesa_reference_code']))
        {
            $data['reference_code'] = $data['mpesa_reference_code'];
        }

        $references = SalePayment::where([
            'payment_type' => $data['payment_type'],
            'payment_method' => $data['payment_method'],
            'reference_code' => $data['reference_code'],
        ])->where('reference_code','!=', $data['payment_method'])->get();


        if ($references->count())
        {
            throw  new \Exception("We have received a payment with the same reference number");
        }



        $payment = $sale->salePayments()->create([
            'payment_type' => $data['payment_type'],
            'payment_method' => $data['payment_method'],
            'amount' => $data['amount'],
            'user_id' => auth()->id(),
            'reference_code' => $data['reference_code'],
            'bank_name' => isset($data['bank_name']) ? $data['bank_name'] : "",
            'account_no' => isset($data['account_no']) ? $data['account_no'] : "",
            'cheque_no' => isset($data['cheque_no']) ? $data['cheque_no'] : "",
            'drawer' => isset($data['drawer']) ? $data['drawer'] : "",
            'slip_no' => isset($data['slip_no']) ? $data['slip_no'] : "",
            'deposit_date' => isset($data['deposit_date']) ? Carbon::parse($data['deposit_date']) : Carbon::now()
        ]);



        /***********CREATE ACCOUNT****************/


        $account = Account::create([

            'user_id' => auth()->id(),
            'amount' => $data['amount'],
            'source' => 'installment',
            'deposit_date' => isset($data['deposit_date']) ? Carbon::parse($data['deposit_date']) : Carbon::now()
        ]);


        // upload files


        $request = request();



        if ($request->file('receipts')) {


            //$path = $request->file('agreement')->store('agreements');

            try {
                $path = $request->file('receipts')->store('agreements');

                //$sale->agreement_url= $path;


                $sale->saleUploads()->create([
                    'agreement_url' => $path,
                    'file_name' => $request->file('receipts')->getClientOriginalName(),
                    'type' => $request['upload_type']
                ]);

                $sale->is_agreement_uploaded = true;

                $sale->save();
            } catch (\Exception $exception) {
                throw new \Exception($exception->getMessage());

            }

        }

        $sale->account($account);

        $payment->savePayable($account);



        $sale->color = "default";
        $sale->save();


        return "successfully updated invoice";


    }

    protected function filter(array $data, array $options = [])
    {
        // TODO: Implement filter() method.
    }
}
