<?php
/**
 * Created by PhpStorm.
 * User: philip
 * Date: 2/26/18
 * Time: 8:11 PM
 */

namespace Codex\Services\Sales;


use App\Sale\Sale;
use Carbon\Carbon;
use Codex\Classes\Filters\AgentFilter;
use Codex\Classes\Filters\CommissionDateBetweenFilter;
use Codex\Classes\Filters\CommissionFilter;
use Codex\Classes\Filters\DateBetweenFilter;
use Codex\Classes\Filters\IdFilter;
use Codex\Classes\Repository\SalesRepository;
use Codex\Classes\ServiceAbstract;
use Illuminate\Pipeline\Pipeline;

class ListSalesService extends ServiceAbstract
{

    /**
     * @param array $data
     * @param array $options
     */
    protected function execute(array $data, array $options = [])
    {
        return $this->filter($data, $options);
    }

    protected function filter(array $data, array $options = [])
    {

        return $this->getSales($data, $options);

    }

    public function getSales(array $data, array $options = [])
    {
        $request = request();

        request()->request->add($data);
        request()->request->add($options);

        return (new SalesRepository())
            ->all($request);


    }
}
