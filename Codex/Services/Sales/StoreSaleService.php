<?php
/**
 * Created by PhpStorm.
 * User: philip
 * Date: 2/27/18
 * Time: 6:31 PM
 */

namespace Codex\Services\Sales;


use App\Customer\Customer;
use App\DB\Accounting\Account;
use App\DB\Business\Meta;
use App\DB\Notification\Notification;
use App\DB\Role\Role;
use App\DB\Sale\Calculator;
use App\DB\Sale\Installment;
use App\DB\Sale\OfferLetterItem;
use App\Jobs\SendSmsNotifications;
use App\Sale\Invoice;
use App\Sale\Sale;
use App\Sale\SaleItem;
use App\Sale\SalePayment;
use App\Traits\IsNextOfKin;
use App\User;
use Carbon\Carbon;
use Codex\Classes\Handlers\Calculator\CalculatorHandlerFacade;
use Codex\Classes\Handlers\SystemSms;
use Codex\Classes\Repository\SalesRepository;
use Codex\Classes\ServiceAbstract;
use Codex\Contracts\ServiceCaller;
use Codex\Services\Plots\ListPlotsService;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;

class
StoreSaleService extends ServiceAbstract
{

    use  IsNextOfKin;

    protected $saleRepository;

    public function __construct(ServiceCaller $caller)
    {
        parent::__construct($caller);

        $this->saleRepository = new SalesRepository();
    }

    /**
     * @param array $data
     * @param array $options
     */
    protected function execute(array $data, array $options = [])
    {
        try {
            $this->begin();


            $request = request();
            $data['reference_code'] = $data['payment_method'];
            $months = isset($request['months']) ? $request['months'] : fanakaConfig('installment_months', 6) ;

            $request->request->add(['months' =>  $months ]);

            if ( isset($request['paymentOptionChecker']) && strtolower($request['paymentOptionChecker']) === 'cash')
            {
                $request->request->add(['months' => 1]);
                $request['months'] = 1;
            }

            $request['upload_type'] = 'receipt';
            $request['is_converted'] = true;

            $data['months'] = $request['months'];


            $request['monthly_installment']  = isset($request['monthly_installment']) ?
                $request['monthly_installment'] :
                floatval($data['total']) /  floatval($request['months']);
            $request->request->add(['monthly_amount' => $request['monthly_installment']]);


            $sale = $this->createSale($data, $options);
            /*if (isset($data['new_customer'])) {


                $this->storeSaleWithNewCustomer($data, $options);


                $this->commit();


                return "successfully made a sale you can download the agreement";


            }

            $this->storeSale($data, $options);*/


            $this->commit();

            


            return "successfully made a sale you can download the agreement";


        } catch (\Exception $e) {
            $this->rollback();


            Mail::raw( json_encode($e), function ($mail) use ($e) {
                $mail->to("philipnjuguna66@gmail.com")
                    ->subject("Error adding a sale " . $e->getMessage());
            });
            throw new \Exception( json_encode($e->getMessage()));

        }

    }

    private function createSale($data, $options = [])
    {
        $request = request();

        $request->request->add($data);
        $request->request->add($options);

        return $this->saleRepository->createSale($request);
    }

    private function storeSale($data, $options)
    {
        for ($i = 0; $i < sizeof($data['plot_id']); $i++) {


            $sale = $this->createSale($data, $options);

            $this->saveMeta($sale);

            $customer = Customer::where(['id' => $data['customer_id']])->first();

            if (!empty($data['next_of_kin_name']) && !empty($data['next_of_kin_phone_number']) && !empty($data['next_of_kin_relation'])) {
                $nextOfKinData = ([
                    'full_name' => $data['next_of_kin_name'],
                    'phone_number' => $data['next_of_kin_phone_number'],
                    'relation' => $data['next_of_kin_relation'],

                ]);


                $this->nextOfKin($customer, $nextOfKinData);


            }


            $sale->customerSales()->create([
                'customer_id' => $data['customer_id'],
            ]);



           // save sale Items

            $this->saveSaleItems($sale, [
                    'business_code' => $this->businessCode,
                    'plot_id' => $data['plot_id'][$i],
                    'price' => $data['price'][$i],
                    'plot_no' => $data['plot_no'][$i]
                ] , $data);



            $plot = (new ListPlotsService($this->caller))->list(['id' => $data['plot_id'][$i]])->first();


            $plot->is_reserved = true;
            $plot->is_sold = true;
            $plot->can_sell = false;

            $plot->save();


            if (($data['amount'] < $data['total']) || $data['payment_type'] === 'installment' && ($data['amount'] != $data['total'])) {
                $invoice = Invoice::create([
                    'business_code' => $this->businessCode,
                    'user_id' => auth()->id(),
                    'sale_id' => $sale->id,
                    'customer_id' => $data['customer_id'],
                    'total_amount' => $data['total'],
                    'total_paid' => floatval($data['amount']) / sizeof($data['plot_id']),
                    'due_date' => Carbon::now()->addDays(30)
                ]);


                $data['payment_type'] = "installment";


                $plot = (new ListPlotsService($this->caller))->list(['id' => $data['plot_id'][$i]])->first();


                $plot->is_completely_paid = false;

                $plot->save();


                //create installment



                $instalment = Installment::create([
                    'sale_id' => $sale->id,
                    'months' => $data['months'],
                    'amount' => floatval($data['total']) / floatval($data['months']),
                    'completion_date' => Carbon::now()->addMonths($data['months'])
                ]);


            }

            if (isset($data['check_bank_name'])) {
                $data['bank_name'] = $data['check_bank_name'];
            }
            if (isset($data['mpesa_reference_code'])) {
                $data['reference_code'] = $data['mpesa_reference_code'];
            }


            /*$references = SalePayment::where([
                'payment_type' => $data['payment_type'],
                'payment_method' => $data['payment_method'],
                'reference_code' => $data['reference_code'],
            ])->where('reference_code', '!=', $data['payment_method'])->get();


            if ($references->count()) {
                throw  new \Exception("We have received a payment with the same reference number");
            }*/


            $request['is_approved'] = false;
            if (strtolower($data['payment_method']) == 'mpesa') {


                $request['is_approved'] = true;


            }

            $payment = $sale->salePayments()->create([
                'payment_type' => $data['payment_type'],
                'payment_method' => $data['payment_method'],
                'amount' =>  floatval($data['amount']) / sizeof($data['plot_id']),
                'user_id' => auth()->id(),
                'is_approved' => $request['is_approved'],
                'reference_code' => isset($data['reference_code']) ? $data['reference_code'] : $data['payment_method'],
                'bank_name' => isset($data['bank_name']) ? $data['bank_name'] : "",
                'account_no' => isset($data['account_no']) ? $data['account_no'] : "",
                'cheque_no' => isset($data['cheque_no']) ? $data['cheque_no'] : "",
                'drawer' => isset($data['drawer']) ? $data['drawer'] : "",
                'slip_no' => isset($data['slip_no']) ? $data['slip_no'] : "",
                'deposit_date' => isset($data['deposit_date']) ? Carbon::parse($data['deposit_date']) : Carbon::now()
            ]);


            /***********CREATE ACCOUNT****************/


            $account = Account::create([
                'user_id' => auth()->id(),
                'amount' => $data['amount'],
                'source' => 'new sale',
                'deposit_date' => isset($data['deposit_date']) ? Carbon::parse($data['deposit_date']) : Carbon::now()
            ]);


            $sale->account($account);


            $payment->savePayable($account);

            // upload files


            $request = request();

            if ($request->file('receipts')) {


                //$path = $request->file('agreement')->store('agreements');

                try {
                    $path = $request->file('receipts')->store('agreements');

                    //$sale->agreement_url= $path;


                    $sale->saleUploads()->create([
                        'agreement_url' => $path,
                        'file_name' => $request->file('receipts')->getClientOriginalName(),
                        'type' => $request['upload_type']
                    ]);

                    $sale->is_agreement_uploaded = true;

                    $sale->save();
                } catch (\Exception $exception) {
                    throw new \Exception($exception->getMessage());

                }

            }


            if (isset($data['has_witness'])) {
                for ($j = 0; $j < sizeof($data['witness_name']); $j++) {
                    $sale->witnesses()->create([
                        'name' => $data['witness_name'][$j],
                        'id_no' => $data['witness_id'][$j],
                        'type' => $data['type'][$j],
                    ]);
                }
            }

            $this->addNote($sale, $data);


            $this->sendNotification($sale);
        }


    }

    public function storeSaleWithNewCustomer($data, $options)
    {

        for ($i = 0; $i < sizeof($data['plot_id']); $i++) {
            

            $sale = $this->createSale($data, $options);

            $this->saveMeta($sale);

            for ($customerIndex = 0; $customerIndex < sizeof($data['first_name']); $customerIndex++) {

                $customer = Customer::store([
                    'business_code' => auth()->user()->business->code,
                    'first_name' => $data['first_name'][$customerIndex],
                    'last_name' => $data['last_name'][$customerIndex],
                    'email' => $data['email'][$customerIndex],
                    'phone_number' => $data['phone_number'][$customerIndex],
                    'id_no' => $data['id_no'][$customerIndex],
                    'middle_name' => $data['middle_name'][$customerIndex],
                    'user_id' => auth()->id(),
                ]);

                if (!empty($data['next_of_kin_name']) && !empty($data['next_of_kin_phone_number']) && !empty($data['next_of_kin_relation'])) {
                    $nextOfKinData = ([
                        'full_name' => $data['next_of_kin_name'],
                        'phone_number' => $data['next_of_kin_phone_number'],
                        'relation' => $data['next_of_kin_relation'],

                    ]);
                    $this->nextOfKin($customer, $nextOfKinData);

                }

                $data['customer_id'] = $customer->id;

                $sale->customerSales()->create([
                    'customer_id' => $customer->id,
                ]);

            }
            // save Payments

            $this->saveSaleItems($sale, [
                'business_code' => $this->businessCode,
                'plot_id' => $data['plot_id'][$i],
                'price' => $data['price'][$i],
                'plot_no' => $data['plot_no'][$i]

            ], $data);

            $plot = (new ListPlotsService($this->caller))->list(['id' => $data['plot_id'][$i]])->first();

            $plot->is_reserved = true;
            $plot->is_sold = true;
            $plot->can_sell = false;

            $plot->save();


            if (($data['amount'] < $data['total']) || $data['payment_type'] === 'installment' && ($data['amount'] != $data['total'])) {
                Invoice::create([
                    'business_code' => $this->businessCode,
                    'user_id' => auth()->id(),
                    'sale_id' => $sale->id,
                    'customer_id' => $data['customer_id'],
                    'total_amount' => $data['total'],
                    'total_paid' => $data['amount'],
                    'due_date' => Carbon::now()->addDays(30)
                ]);

                $data['payment_type'] = "installment";

                $plot = (new ListPlotsService($this->caller))->list(['id' => $data['plot_id'][$i]])->first();

                $plot->is_completely_paid = false;
                $plot->save();
                

                //create installment

                Installment::create([
                    'sale_id' => $sale->id,
                    'months' => $data['months'],
                    'amount' => floatval($data['total']) / floatval($data['months']),
                    'completion_date' => Carbon::now()->addMonths($data['months'])
                ]);

            }

            if (isset($data['check_bank_name'])) {
                $data['bank_name'] = $data['check_bank_name'];
            }
            if (isset($data['mpesa_reference_code'])) {
                $data['reference_code'] = $data['mpesa_reference_code'];
            }

            /*$references = SalePayment::where([
                'payment_type' => $data['payment_type'],
                'payment_method' => $data['payment_method'],
                'reference_code' => $data['reference_code'],
            ])->where('reference_code', '!=', $data['payment_method'])->get();


            if ($references->count()) {
                throw  new \Exception("We have received a payment with the same reference number");
            }*/

            $payment = $sale->salePayments()->create([
                'payment_type' => $data['payment_type'],
                'payment_method' => $data['payment_method'],
                'amount' => $data['amount'],
                'user_id' => auth()->id(),
                'reference_code' => isset($data['reference_code']) ? $data['reference_code'] : $data['payment_method'],
                'bank_name' => isset($data['bank_name']) ? $data['bank_name'] : "",
                'account_no' => isset($data['account_no']) ? $data['account_no'] : "",
                'cheque_no' => isset($data['cheque_no']) ? $data['cheque_no'] : "",
                'drawer' => isset($data['drawer']) ? $data['drawer'] : "",
                'slip_no' => isset($data['slip_no']) ? $data['slip_no'] : "",
                'deposit_date' => isset($data['deposit_date']) ? Carbon::parse($data['deposit_date']) : Carbon::now()
            ]);


            /***********CREATE ACCOUNT****************/


            $account = Account::create([

                'user_id' => auth()->id(),
                'amount' => $data['amount'],
                'source' => 'new sale',
                'deposit_date' => isset($data['deposit_date']) ? Carbon::parse($data['deposit_date']) : Carbon::now()
            ]);

            $sale->account($account);


            $payment->savePayable($account);


            if (isset($data['has_witness'])) {
                for ($j = 0; $j < sizeof($data['witness_name']); $j++) {
                    $sale->witnesses()->create([
                        'name' => $data['witness_name'][$j],
                        'id_no' => $data['witness_id'][$j],
                        'type' => $data['type'][$j],
                    ]);
                }
            }


            //receipts


            $request = request();

            if ($request->file('receipts')) {


                //$path = $request->file('agreement')->store('agreements');

                try {
                    $path = $request->file('receipts')->store('agreements');

                    //$sale->agreement_url= $path;


                    $sale->saleUploads()->create([
                        'agreement_url' => $path,
                        'file_name' => $request->file('receipts')->getClientOriginalName(),
                        'type' => $request['upload_type']
                    ]);
                } catch (\Exception $exception) {
                    throw new \Exception($exception->getMessage());

                }

            }


            $this->addNote($sale, $data);


            $this->sendNotification($sale);
        }
    }


    private function addNote(Sale $sale, $data)
    {


        if (isset($data['other_details'])) {
            for ($i = 0; $i < sizeof($data['customer_payment_note']); $i++) {
                $sale->notes()->create([
                    'note' => $data['customer_payment_note'][$i]
                ]);
            }
        }
    }


    private function sendNotification(Sale $sale)
    {

        SystemSms::newSale($sale);


    }


    private  function saveMeta(Sale $sale){

        if ($sale->getMeta('save_calculator', false))
        {
            $meta =  Meta::create([
                'key' => 'save_calculator',
                'value'   => true
            ]);
            $sale->saveMeta($meta);

        }
        if ($sale->getMeta('payment_option', false))
        {
            $this->metable()->delete();
        }

        $meta = Meta::create([
            'key' => 'payment_option',
            'value' => request('paymentOptionChecker')
        ]);
        $sale->saveMeta($meta);
    }

    private function calculate(Sale $sale ,$principal, $deposit , $months = 1)
    {
        $calculates = CalculatorHandlerFacade::getRate($months)->calculate($principal, $deposit);

        $sum = 0;

        foreach ($calculates as $calculate) {
            $sum += $calculate['monthlyPayment'];

            $sale->calculators()->create([
                'amount'  => $calculate['monthlyPayment'],
                'month'   => Carbon::now()->addRealMonths($calculate['month'])->format('Y-m')
            ]);

        }

        return $sum;

    }
    private function saveSaleItems(Sale $sale , $saleItemData, $data)
    {

        if(! SaleItem::where('plot_id' ,$saleItemData['plot_id'])->count()){

            if (SaleItem::where('plot_id', $saleItemData['plot_id'])->count() ||
                OfferLetterItem:: where('plot_id', $saleItemData['plot_id'])->count())
            {
                throw new \Exception( " That plot is already sold");
            }

            $price = $this->calculate($sale ,$saleItemData['price'] , 0 , $data['months']);


            return  $sale->saleItems()->create([

                'business_code' => $this->businessCode,
                'plot_id' => $saleItemData['plot_id'],
                'price' => $price,
                'plot_no' => $saleItemData['plot_no']

            ]);

        }
         // validate


    }

    protected function filter(array $data, array $options = [])
    {
        // TODO: Implement filter() method.
    }
}
