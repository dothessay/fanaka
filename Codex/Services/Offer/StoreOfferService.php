<?php
/**
 * Created by PhpStorm.
 * User: philip
 * Date: 9/15/18
 * Time: 2:41 PM
 */

namespace Codex\Services\Offer;


use App\Customer\Customer;
use App\DB\Accounting\Account;
use App\DB\Customer\NextOfKin;
use App\DB\Offer\OfferMeta;
use App\DB\Sale\OfferLetter;
use App\DB\Sale\OfferLetterItem;
use App\Http\Requests\Customer\StoreCustomer;
use App\Jobs\SendSmsNotifications;
use App\Traits\IsNextOfKin;
use Carbon\Carbon;
use Codex\Classes\Handlers\SystemSms;
use Codex\Classes\ServiceAbstract;
use Codex\Services\Plots\ListPlotsService;
use Monolog\Handler\IFTTTHandler;

class StoreOfferService extends ServiceAbstract
{

    use  IsNextOfKin;

    /**
     * @param array $data
     * @param array $options
     */
    protected function execute(array $data, array $options = [])
    {


        $offers = [];

        foreach ($data['plot_id'] as $key => $plotId) {


            $offer = OfferLetter::create([
                'business_code' => $this->businessCode,
                'user_id' => auth()->id(),
                'total_amount' => $data['total'],
                'payment_option' => $data['payment_method'],
                'cash_option' => (96 / 100) * ($data['installment_option'][$key]),
                'installment_option' => ($data['installment_option'][$key]),
                'agent_id' => $data['agent'],
                'note' => isset($data['note']) ? $data['note'] : null,
                'is_approved'  => true,
                'approved_by'  => auth()->id()

            ]);
        

            if (!empty($data['next_of_kin_name']) && !empty($data['next_of_kin_phone_number']) && !empty($data['next_of_kin_relation'])) {
                $nextOfKinData = ([
                    'full_name' => $data['next_of_kin_name'],
                    'phone_number' => $data['next_of_kin_phone_number'],
                    'relation' => $data['next_of_kin_relation'],

                ]);


            }

            if (isset($data['new_customer'])) {
                for ($customerIndex = 0; $customerIndex < sizeof($data['first_name']); $customerIndex++) {
                    $customer = Customer::store([
                        'business_code' => auth()->user()->business->code,
                        'first_name' => $data['first_name'][$customerIndex],
                        'last_name' => $data['last_name'][$customerIndex],
                        'email' => $data['email'][$customerIndex],
                        'phone_number' => $data['phone_number'][$customerIndex],
                        'id_no' => $data['id_no'][$customerIndex],
                        'middle_name' => $data['middle_name'][$customerIndex],
                        'user_id' => auth()->id(),
                    ]);


                    $data['customer_id'] = $customer->id;

                    $offer->offerCustomers()->create([
                        'customer_id' => $customer->id,
                    ]);

                }
            } else {


                $customer = Customer::where('id', $data['customer_id'])->first();


                $offer->offerCustomers()->create([
                    'customer_id' => $customer->id,
                ]);
            }


            $customer = Customer::where('id', $data['customer_id'])->first();


            if (isset($nextOfKinData)) {

                $nextOfKins = $this->nextOfKin($customer, $nextOfKinData);


                $offer->next_of_kin_id = $nextOfKins->id;

                $offer->save();


            }

            $data['monthly_installment'] = floatval($data['balance']) / floatval($data['months']);

            $offer->offerInstallment()->create([
                'months' => $data['months'],
                'amount' => $data['monthly_installment'],
                'completion_date' => Carbon::now()->addMonths($data['months'])
            ]);


            $price = 0;

            if ($data['payment_method'] === "cash_option") {
                $price = $data['cash_option'][$key];


            }
            if ($data['payment_method'] === "installment_option") {
                $price = $data['installment_option'][$key];
            }

            $saleItem = $offer->offerItems()->create([

                'plot_id' => $plotId,
                'selling_price' => $price,

            ]);

            $plot = (new ListPlotsService($this->caller))->list(['id' => $plotId])->first();


            $plot->is_reserved = true;

            $plot->can_sell = false;


            $plot->save();


            if (isset($data['check_bank_name'])) {
                $data['bank_name'] = $data['check_bank_name'];
            }
            if (isset($data['mpesa_reference_code'])) {
                $data['reference_code'] = $data['mpesa_reference_code'];
            }

            $payment = $offer->offerPayments()->create([
                'payment_method' => $data['payment_option'],
                'amount' => floatval($data['amount']) / sizeof($data['plot_id']),
                'user_id' => auth()->id(),
                'reference_code' => isset($data['reference_code']) ? $data['reference_code'] : $data['payment_option'],
                'bank_name' => isset($data['bank_name']) ? $data['bank_name'] : "",
                'account_no' => isset($data['account_no']) ? $data['account_no'] : "",
                'cheque_no' => isset($data['cheque_no']) ? $data['cheque_no'] : "",
                'drawer' => isset($data['drawer']) ? $data['drawer'] : "",
                'slip_no' => isset($data['slip_no']) ? $data['slip_no'] : "",
                "deposit_date" => isset($data['deposit_date']) ? Carbon::parse($data['deposit_date']) : Carbon::now()
            ]);


            /***********CREATE ACCOUNT****************/


            $account = Account::create([
                'user_id' => auth()->id(),
                'amount' => $data['amount'],
                'source' => 'new Offer Deposit',
                'deposit_date' => Carbon::parse($data['deposit_date'])
            ]);


            $offer->account($account);
            $payment->savePayable($account);

            $request = request();

            if ($request->file('receipts')) {
                foreach ($request->receipts as $index => $agreement) {


                    //$path = $request->file('agreement')->store('agreements');

                    try {
                        $path = $agreement->store('agreements');

                        //$sale->agreement_url= $path;


                        $offer->uploads()->create([
                            'file_url' => $path,
                            'file_name' => $agreement->getClientOriginalName(),

                        ]);


                    } catch (\Exception $exception) {
                        throw new \Exception('could not upload the files . ' . $exception->getMessage());
                    }
                }
            }

            if (isset($data['reservation_days']))
            {
                OfferMeta::create([
                    'offer_id' => $offer->id,
                    'key'      => 'reservation_days',
                    'value'    => $data['reservation_days']
                ]);
            }

            if ( ! $offer->is_approved)
            {
                SystemSms::reservationRequest($offer);
            }


            $offers[] = $offer;
        }

        return "offer successfully created";

    }


    protected function filter(array $data, array $options = [])
    {
        // TODO: Implement filter() method.
    }
}
