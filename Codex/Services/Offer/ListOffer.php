<?php
/**
 * Created by PhpStorm.
 * User: philip
 * Date: 9/15/18
 * Time: 12:54 PM
 */

namespace Codex\Services\Offer;


use App\DB\Sale\OfferLetter;
use Bugsnag\Pipeline;
use Carbon\Carbon;
use Codex\Classes\Filters\AgentIdFilter;
use Codex\Classes\Filters\DateBetweenFilter;
use Codex\Classes\Filters\IdFilter;
use Codex\Classes\Filters\UserFilter;
use Codex\Classes\Repository\ReservationRepository;
use Codex\Classes\ServiceAbstract;

class ListOffer extends ServiceAbstract
{

    /**
     * @param array $data
     * @param array $options
     */

    protected function execute(array $data, array $options = [])
    {
        return $this->filter($data, $options);
    }

    protected function filter(array $data, array $options = [])
    {

        return $this->getOffers($data, $options);

    }
    public function getOffers(array $data, array $options = [])
    {

        if (!isset($data['is_converted'])) {
            $data['is_converted'] = false;
        }

        request()->request->add($data);
        request()->request->add($options);



        return (new ReservationRepository())
            ->all();



    }
}


