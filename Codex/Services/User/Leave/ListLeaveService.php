<?php 


namespace Codex\Services\User\Leave;
use App\DB\Leave\LeaveRequest;
use Codex\Classes\ServiceAbstract;

Class ListLeaveService extends ServiceAbstract
{

	/**
	* @param array $data
	* @param array $options
	*/
	protected function execute(array $data, array $options = [])
	{

	    return $this->filter($data , $options);

	}

	/**
	* @param array $data
	* @param array $options
	*/
	protected function filter(array $data, array $options = [])
	{

	    $leaves = LeaveRequest::where($data);




        if (isset($options['user_id']))
        {

            $leaves = $leaves->where('user_id',$options['user_id']);
        }

        if (! auth()->user()->can('access-module-component-functionality','users_leave_view-requests'))
        {
            $leaves = $leaves->where('user_id',auth()->id());
        }

            return $leaves->get();

	}
} 