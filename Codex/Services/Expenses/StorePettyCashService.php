<?php
/**
 * Created by Philip Njuguna.
 * User: Philip Njuguna
 * Date: 11/12/18
 * Time: 1:07 PM
 */

namespace Codex\Services\Expenses;


use App\DB\Exepense\PettyCash;
use App\DB\Exepense\PettyCashLog;
use App\DB\Finance\ChartOfAccount;
use App\Traits\IsJournable;
use Codex\Classes\ServiceAbstract;

class StorePettyCashService extends ServiceAbstract
{
    use IsJournable;


    /**
     * @param array $data
     * @param array $options
     */
    protected function execute(array $data, array $options = [])
    {
        if (PettyCash::get()->count())
        {
            $petty = PettyCash::first();
            $petty->amount = $petty->amount + (double)$data['amount'];
            $petty->save();

        }

        else{
            $petty =  PettyCash::create([
                'user_id'  => auth()->id(),
                'amount'   => $data['amount'],
                'chart_id' => $data['chart'],
                'note'     => "new Deposit"
            ]);
        }

        $log = PettyCashLog::create([
            'user_id'  => auth()->id(),
            'credit'   => $data['amount'],
            'note'     => "new Deposit",
           'balance'   => $petty->amount,

        ]);

        $journal = $this->addToJournal([
            'chart_id'    => $data['chart'],
            'debit'       => 0,
            'credit'      => $data['amount']
        ]);

        $petty->addjournalable($journal);

        $chart = ChartOfAccount::where('code', '8450/000')->first();

        if (! is_null($chart))
        {
            $journal = $this->addToJournal([
                'chart_id'    => $chart->id,
                'debit'       => $data['amount'],
                'credit'      => 0
            ]);

            $petty->addjournalable($journal);
        }


        $petty->addLoggable($log);


        return "successfully credit a petty cash cash";

    }

    protected function filter(array $data, array $options = [])
    {
        // TODO: Implement filter() method.
    }
}
