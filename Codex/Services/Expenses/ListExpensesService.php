<?php
/**
 * Created by PhpStorm.
 * User: philip
 * Date: 3/13/18
 * Time: 10:20 PM
 */

namespace Codex\Services\Expenses;


use App\DB\Expense\Expense;
use Carbon\Carbon;
use Codex\Classes\ServiceAbstract;

class ListExpensesService extends ServiceAbstract
{

    protected function execute(array $data, array $options = [])
    {
        return $this->filter($data, $options);
    }

    protected function filter(array $data, array $options = [])
    {
        $options['start_date'] = isset($options['start_date']) ? $options['start_date'] : Carbon::now()->startOfMonth();
        $options['end_date'] = isset($options['end_date']) ? $options['end_date'] : Carbon::now()->endOfMonth();


        return Expense::whereBetween('created_at',[
                Carbon::parse($options['start_date'])->startOfDay() , Carbon::parse($options['end_date'])->endOfDay()
            ])->get();

    }
}