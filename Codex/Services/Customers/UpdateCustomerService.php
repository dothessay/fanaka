<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Codex\Services\Customers;
use Codex\Classes\ServiceAbstract;
use App\Customer\Customer;
use Illuminate\Support\Facades\DB;

/**
 * Description of UpdateCustomerService
 *
 * @author HP
 */
class UpdateCustomerService extends ServiceAbstract {


    protected $withDBTransaction = false;

    protected function execute(array $data, array $options = array()) 
    {

        try{
            $customer = Customer::where('id', $data['id'])->first();

            if($customer)
            {
                $customer->first_name = $data['first_name'];
                $customer->last_name = $data['last_name'];
                $customer->middle_name = $data['middle_name'];
                $customer->phone_number = $data['phone_number'];
                $customer->email = $data['email'];
                $customer->id_no = $data['id_no'];
                $customer->user_id = auth()->id();
                $customer->password = bcrypt($customer->id_no);
                $customer->save();
            }


            return "Successfully updated customer";


        }
        catch (\Exception $exception)
        {
            throw  new \Exception($exception->getMessage());
        }

    }

    protected function filter(array $data, array $options = array()) {
        
    }

}
