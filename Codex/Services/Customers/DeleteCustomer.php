<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Codex\Services\Customers;
use Codex\Classes\ServiceAbstract;
use App\Customer\Customer;

/**
 * Description of DeleteCustomer
 *
 * @author HP
 */
class DeleteCustomer extends ServiceAbstract
{
    
    protected function execute(array $data, array $options = array()) 
    {
                
        Customer::where('id',$data['id'])->forceDelete();


        return "The customer has been deleted";
    }

    protected function filter(array $data, array $options = array()) {
        
    }

}
