<?php
/**
 * Created by PhpStorm.
 * User: philip
 * Date: 2/26/18
 * Time: 3:55 PM
 */

namespace Codex\Services\Customers;


use App\Customer\Customer;
use Codex\Classes\ServiceAbstract;

class ListCustomerService extends ServiceAbstract
{

    /**
     * @param array $data
     * @param array $options
     */
    protected function execute(array $data, array $options = [])
    {
        return $this->filter($data, $options);

    }

    protected function filter(array $data, array $options = [])
    {

        if (!auth()->user()->can('access-module-component-functionality', 'sale_manage_show-all-sales')) {


            return auth()->user()->customers();


        }


        return Customer::where($data)->get();
    }
}