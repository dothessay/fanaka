<?php
/**
 * Created by PhpStorm.
 * User: philip
 * Date: 2/26/18
 * Time: 4:37 PM
 */

namespace Codex\Services\Customers;


use App\Customer\Customer;
use Codex\Classes\ServiceAbstract;

class StoreCustomerService extends ServiceAbstract
{

    /**
     * @param array $data
     * @param array $options
     */
    protected function execute(array $data, array $options = [])
    {
        Customer::store($data);

        return "Successfully registered customer";
    }

    protected function filter(array $data, array $options = [])
    {
        // TODO: Implement filter() method.
    }
}