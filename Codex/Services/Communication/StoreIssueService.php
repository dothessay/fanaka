<?php 


namespace Codex\Services\Communication;
use App\DB\Notification\Issue;
use App\Events\IssueNotification;
use Codex\Classes\Handlers\InnoxBroadcast;
use Codex\Classes\ServiceAbstract;
use Pusher\Pusher;

Class StoreIssueService extends ServiceAbstract
{

	/**
	* @param array $data
	* @param array $options
	*/
	protected function execute(array $data, array $options = [])
	{

	    $issue = Issue::create([
	        'type'  => $data['type'],
	        'issue'  => $data['issue'],
            'user_id'  => auth()->id()
        ]);

	   $data = [
	       'message'  => $issue->issue,
           'type'     => $issue->type,
           'user'     => $issue->user->fullName(),

       ];



	    event(new IssueNotification($data));


       broadcast(new IssueNotification($issue->toArray()))->toOthers();


        InnoxBroadcast::index($data);


	    return "successfully submitted the issue";
	}

	/**
	* @param array $data
	* @param array $options
	*/
	protected function filter(array $data, array $options = [])
	{

	// TODO: Implement execute() method.
	}
} 