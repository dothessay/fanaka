<?php
namespace Codex\Services\Users;

use Codex\Classes\ServiceAbstract;
use App\DB\Role\Role;

class UpdateRoleService extends ServiceAbstract
{
    
    protected function execute(array $data, array $options = array())
    {
        $role = Role::where('id', $data['id'])->first();
        
        if($role)
        {
            $role->label = $data['name'];
            $role->description = $data['description'];
            $role->name = $data['name'];
            
            $role->save();
        }
        
        return "Successfully updated role";
    }

    protected function filter(array $data, array $options = array()) 
    {
        
    }

}
