<?php

namespace Codex\Services\Users;

use Codex\Classes\ServiceAbstract;
use App\DB\Role\Role;

class DeleteRoleService extends ServiceAbstract
{
    
    protected function execute(array $data, array $options = array()) 
    {
        Role::where('id',$data['role_id'])->delete();

        return "The role has been deleted successfully";
    }

    protected function filter(array $data, array $options = array()) 
    {
        
    }

}
