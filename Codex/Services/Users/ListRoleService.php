<?php
/**
 * Created by PhpStorm.
 * User: philip
 * Date: 3/4/18
 * Time: 11:53 AM
 */

namespace Codex\Services\Users;


use App\DB\Role\Role;
use Codex\Classes\ServiceAbstract;

class ListRoleService extends ServiceAbstract
{

    /**
     * @param array $data
     * @param array $options
     */
    protected function execute(array $data, array $options = [])
    {
       return Role::where($data)->get();
    }

    protected function filter(array $data, array $options = [])
    {
        // TODO: Implement filter() method.
    }
}