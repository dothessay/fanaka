<?php
/**
 * Created by PhpStorm.
 * User: philip
 * Date: 2/25/18
 * Time: 5:41 PM
 */

namespace Codex\Services\Users;


use App\User;
use Codex\Classes\Repository\UserRepository;
use Codex\Classes\ServiceAbstract;

class ListUsersService extends ServiceAbstract
{

    /**
     * @param array $data
     * @param array $options
     */
    protected function execute(array $data, array $options = [])
    {
        $isActive = isset($data['is_active']) ? : false;
      return $this->filter($data, $options);
    }

    protected function filter(array $data, array $options = [])
    {
        return (new UserRepository())->all();

    }
}
