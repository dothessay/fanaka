<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Codex\Services\Users;

use Codex\Classes\ServiceAbstract;
use App\User;
use Illuminate\Support\Facades\Hash;

/**
 * Description of UpdateUserService
 *
 * @author HP
 */
class UpdateUserService extends ServiceAbstract
{

    protected function execute(array $data, array $options = array())
    {
        $user = User::where('id', $data['id'])->first();

        if($user)
        {
            if( ! empty($data['password'])  || ! is_null($data['password']))
            {
                $user->password = (Hash::check( $data['password'] ,  $user->password) ? $user->password : bcrypt( $data['password']));
            }
            else
            {
                $user->first_name =  isset($data['first_name']) ? $data['first_name'] : $user->first_name;
                $user->last_name =  isset($data['last_name']) ? $data['last_name'] : $user->last_name;
                $user->phone_number = isset($data['phone_number']) ? $data['phone_number'] : $user->phone_number;
                $user->email = isset($data['email']) ? $data['email'] : $user->email;
                $user->role_id = isset($data['role_id']) ? $data['role_id'] : $user->role->id ;

            }

            $user->save();
        }

        return "Successfully updated user";
    }

    protected function filter(array $data, array $options = array()) {

    }

}
