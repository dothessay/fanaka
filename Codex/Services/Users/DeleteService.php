<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Codex\Services\Users;

use App\DB\User\DeactivatedAgentCustomer;
use App\User;
use Codex\Classes\ServiceAbstract;

/**
 * Description of DeleteService
 *
 * @author HP
 */
class DeleteService extends ServiceAbstract
{
    
    protected function execute(array $data, array $options = array()) 
    {
        $user = User::where('id',$data['id'])->first();


        $user->is_active = false;

        DeactivatedAgentCustomer::create([
            'agent_id' => $data['id'],
            'user_id'  => $data['user_id']
        ]);

        $user->save();




        return "The user has been deleted";
    }

    protected function filter(array $data, array $options = array()) {
        
    }

}
