<?php
/**
 * Created by PhpStorm.
 * User: philip
 * Date: 2/25/18
 * Time: 9:55 PM
 */

namespace Codex\Services\Users;


use App\User;
use Codex\Classes\Repository\UserRepository;
use Codex\Classes\ServiceAbstract;

class StoreUserService extends ServiceAbstract
{

    /**
     * @param array $data
     * @param array $options
     */
    protected function execute(array $data, array $options = [])
    {
        $request =  request();

        $request->request->add($data);
        $request->request->add($options);


        (new UserRepository())->store($request);

       return "User Created successfully";

    }

    protected function filter(array $data, array $options = [])
    {
        // TODO: Implement filter() method.
    }
}
