<?php
/**
 * Created by PhpStorm.
 * User: philip
 * Date: 3/4/18
 * Time: 12:56 PM
 */

namespace Codex\Services\Users;


use App\DB\Role\Role;
use Codex\Classes\ServiceAbstract;

class StoreRoleService extends ServiceAbstract
{

    /**
     * @param array $data
     * @param array $options
     */
    protected function execute(array $data, array $options = [])
    {
        Role::create([
            'business_code' => auth()->user()->business->code,
            'label' => $data['name'],
            'description' => $data['description'],
            'name' => ucwords($data['name']),
        ]);


        return "role saved successfully";
    }

    protected function filter(array $data, array $options = [])
    {
        // TODO: Implement filter() method.
    }
}