<?php

namespace Codex\Services\Products;

use Codex\Classes\ServiceAbstract;
use App\DB\Product\ProjectExpense;

class ListProjectExpensesService extends ServiceAbstract
{
    
    protected function execute(array $data, array $options = array()) 
    {
        return $this->filter($data, $options);
    }

    protected function filter(array $data, array $options = array()) 
    {
        $projectExpenses = ProjectExpense::where($data)
                            ->groupBy('project_id')
                            ->get();
        return $projectExpenses;
    }

}
