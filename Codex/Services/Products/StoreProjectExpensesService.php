<?php
namespace Codex\Services\Products;

use Codex\Classes\ServiceAbstract;
use App\DB\Product\ProjectExpense;
use App\DB\Product\ProjectExpensePurpose;
use App\DB\Product\Product;

class StoreProjectExpensesService extends ServiceAbstract
{
    
    protected function execute(array $data, array $options = array()) 
    {
        $projectExpense = ProjectExpense::create([
            'project_id' => $data['project_id'],
        ]);
        
        if($projectExpense)
        {
            if (isset($data['no_of_expenses']))
            {
                for ($i = 0; $i < count($data['cost']); $i++)
                {
                    ProjectExpensePurpose::create([
                        'expense_id' => $projectExpense->id,
                        'cost' => $data['cost'][$i],
                        'purpose' => $data['purpose'][$i],                
                    ]);
                }
                
                //update project price
                $project = Product::find( $data['project_id'] );
                $project->increment('cost', array_sum($data['cost']));
            }
            else
            {    
                ProjectExpensePurpose::create([
                    'expense_id' => $projectExpense->id,
                    'cost' => $data['cost'][0],
                    'purpose' => $data['purpose'][0],                
                ]);

                //update project price
                $project = Product::find( $data['project_id'] );
                $project->increment('cost',$data['cost'][0]);
            }
        }    
        
        $projectExpense->save();
        
        return "Successfully added expenses";
    }

    protected function filter(array $data, array $options = array()) 
    {
        
    }

}
