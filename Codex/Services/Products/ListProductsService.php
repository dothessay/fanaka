<?php
/**
 * Created by PhpStorm.
 * User: philip
 * Date: 2/25/18
 * Time: 9:46 AM
 */

namespace Codex\Services\Products;


use App\DB\Product\Product;
use Carbon\Carbon;
use Codex\Classes\Filters\DateBetweenFilter;
use Codex\Classes\Filters\IdFilter;
use Codex\Classes\ServiceAbstract;
use Illuminate\Pipeline\Pipeline;

class ListProductsService extends ServiceAbstract
{

    /**
     * @param array $data
     * @param array $options
     */
    protected function execute(array $data, array $options = [])
    {
        request()->request->add($data);
        request()->request->add($options);

        return $this->filter($data, $options);

    }

    protected function filter(array $data, array $options = [])
    {


        return app(Pipeline::class)
            ->send(Product::query())
            ->through([
                IdFilter::class,
                DateBetweenFilter::class
            ])
            ->thenReturn()
            ->get();
    }
}
