<?php

namespace Codex\Services\Products;

use Codex\Classes\ServiceAbstract;
use App\DB\Product\ProjectExpensePurpose;

class DeleteProjectExpensesService extends ServiceAbstract
{
    //put your code here
    protected function execute(array $data, array $options = array()) 
    {
        ProjectExpensePurpose::where('id',$data['expense_id'])->delete();

        return "The expense has been deleted successfully";
    }

    protected function filter(array $data, array $options = array()) 
    {
        
    }

}
