<?php
/**
 * Created by PhpStorm.
 * User: philip
 * Date: 2/25/18
 * Time: 6:15 PM
 */

namespace Codex\Services\Products;


use App\DB\Product\Product;
use App\Plot\Plot;
use Codex\Classes\ServiceAbstract;
use Codex\Services\Plots\StorePlotService;

class StoreProductsService extends ServiceAbstract
{

    /**
     * @param array $data
     * @param array $options
     */
    protected function execute(array $data, array $options = [])
    {

        if (Product::where([
            'land_location' => $data['land_location'],
            'phase' => $data['phase'],
        ])->count())
        {
            throw new \Exception("That project already exists");
        }
        $product = Product::create([
            'business_code' => auth()->user()->business->code,
            'land_location' => $data['land_location'],
            'code' => generateDBCode(auth()->user()->business->code,$data['land_location']),
            'size' => (isset($data['size'] ) && isset($data['size']) ) ? $data['size'].' '.$data['size_type'] : null,
            'title_no' => $data['title_no'],
            'user_id' => auth()->id(),
            'phase' => $data['phase'],
            'cost' =>  isset($data['cost']) ? $data['cost'] : null,
            'display_phase'  => isset($data['display_phase']) ? true : false,
        ]);



        if (isset($data['no_of_plots']))
        {

            for ($i = 0 ; $i < $data['no_of_plots'] ; $i ++)
            {

               Plot::create([
                    'business_code' => auth()->user()->business->code,
                    'product_code' => $product->code,
                    'size_id' => $data['size_id'][$i],
                    'user_id' => auth()->id(),
                    'price' => $data['child_price'][$i],
                    'plot_no' => $data['child_plot_no'][$i],
                    'title_no' => isset($data['child_title']) ? $data['child_title'][$i] : null,
                ]);



            }
        }


        $product->slug = $product->name();
        $product->save();

        return "Successfully created Mother PLot";

    }

    protected function filter(array $data, array $options = [])
    {
        // TODO: Implement filter() method.
    }
}