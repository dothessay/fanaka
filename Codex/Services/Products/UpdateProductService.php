<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Codex\Services\Products;
use Codex\Classes\ServiceAbstract;
use App\DB\Product\Product;

/**
 * Description of UpdateProductService
 *
 * @author HP
 */
class UpdateProductService  extends ServiceAbstract{
    
    protected function execute(array $data, array $options = array()) 
    {
        
        $product = Product::where('id', $data['id'])->first();
        
        if($product)
        {
            $product->land_location =  isset($data['land_location']) ? $data['land_location'] :  $product->land_location;
            $product->size =  isset($data['size']) && isset($data['size_type']) ?$data['size'] .' '. $data['size_type'] : $product->size;
            $product->title_no = isset($data['title_no']) ? $data['title_no'] : $product->title_no;
            $product->phase = isset($data['phase']) ? $data['phase'] :  $product->phase;
            $product->cost = isset( $data['cost']) ?  $data['cost'] : $product->cost;
            $product->user_id = auth()->id();
            
            $product->save();
        }
        
        return "Successfully updated product";
    }

    protected function filter(array $data, array $options = array()) {
        
    }
    
    

}
