<?php
/**
 * Created by PhpStorm.
 * User: philip
 * Date: 2/25/18
 * Time: 6:47 PM
 */

namespace Codex\Services\Products;


use App\DB\Product\Product;
use Codex\Classes\ServiceAbstract;

class DeleteService extends ServiceAbstract
{

    /**
     * @param array $data
     * @param array $options
     */
    protected function execute(array $data, array $options = [])
    {
        Product::where('code',$data['code'])->delete();


        return "The mother plot has been removed permanently";

    }

    protected function filter(array $data, array $options = [])
    {
        // TODO: Implement filter() method.
    }
}