<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Codex\Services\Plots;

use Codex\Classes\ServiceAbstract;
use App\Plot\Plot;

/**
 * Description of PlotReportsService
 *
 * @author HP
 */
class PlotReportsService extends ServiceAbstract
{
    protected function execute(array $data, array $options = array()) 
    {
        return $this->filter($data, $options);
    }

    protected function filter(array $data, array $options = array()) 
    {
        $plots = Plot::where($data)
            ->where('is_sold', 1)
            ->orWhere('is_reserved', 1)    
            ->get();
        
        
        
        return $plots;
    }

}
