<?php
/**
 * Created by PhpStorm.
 * User: philip
 * Date: 2/26/18
 * Time: 8:20 AM
 */

namespace Codex\Services\Plots;


use App\Plot\Plot;
use Carbon\Carbon;
use Codex\Classes\Repository\PlotRepository;
use Codex\Classes\ServiceAbstract;

class ListPlotsService extends ServiceAbstract
{

    /**
     * @param array $data
     * @param array $options
     */
    protected function execute(array $data, array $options = [])
    {
        //$data['is_sold'] =  isset($data['is_sold']) ? $data['is_sold']  : tr;
        return $this->filter($data, $options);
    }

    protected function filter(array $data, array $options = [])
    {
        isset($options['start_date']) ? $options['start_date'] : request()->request->add(['start_date' =>  now()->startOfMonth()]);
        isset($options['end_date']) ? $options['end_date'] : request()->request->add(['start_date' =>  now()->endOfDay()]);
        request()->request->add($data);
        request()->request->add($options);
        return (new PlotRepository())
            ->all();
    }
}
