<?php
/**
 * Created by PhpStorm.
 * User: philip
 * Date: 3/4/18
 * Time: 7:01 PM
 */

namespace Codex\Services\Plots;


use App\Plot\Plot;
use Codex\Classes\ServiceAbstract;

class UpdatePlotService extends ServiceAbstract
{

    /**
     * @param array $data
     * @param array $options
     */
    protected function execute(array $data, array $options = [])
    {
        $plot = Plot::where('id', $data['id'])->first();
        if ($plot)
        {
           // $plot->product_code = $data['product_code'];
            $plot->price = $data['price'];
            $plot->size_id = $data['size_id'];
            $plot->plot_no = $data['plot_no'];
            $plot->title_no = isset( $data['title_no']) ? $data['title_no'] : $plot->title_no;


            $plot->save();

            return "successfully update product";
        }

        return "something went wrong";
    }

    protected function filter(array $data, array $options = [])
    {
        // TODO: Implement filter() method.
    }
}