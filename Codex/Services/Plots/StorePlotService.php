<?php
/**
 * Created by PhpStorm.
 * User: philip
 * Date: 2/26/18
 * Time: 8:59 AM
 */

namespace Codex\Services\Plots;


use App\Plot\Plot;
use Codex\Classes\ServiceAbstract;

class StorePlotService extends ServiceAbstract
{

    /**
     * @param array $data
     * @param array $options
     */
    protected function execute(array $data, array $options = [])
    {


        Plot::create([
            'business_code' => auth()->user()->business->code,
            'product_code' => $data['product_code'],
            'size_id' => $data['size_id'],
            'user_id' => auth()->id(),
            'price' => $data['price'],
            'plot_no' => $data['plot_no'],
            'title_no' => isset($data['title_no']) ? $data['title_no'] : null,
        ]);


        return "PLot added successfully";
    }

    protected function filter(array $data, array $options = [])
    {
        // TODO: Implement filter() method.
    }
}