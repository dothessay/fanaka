<?php


namespace Codex\Services\Plot;
use App\Plot\Plot;
use Codex\Classes\ServiceAbstract;

Class UnHoldPlot extends ServiceAbstract
{

	/**
	* @param array $data
	* @param array $options
	*/
	protected function execute(array $data, array $options = [])
	{
        foreach ($data['plot_id'] as $index => $plotId) {


            $plot = Plot::where('id', $plotId)->first();

            $plot->can_sell = true;

            $plot->save();

            if ($plot->getMeta('reason' , false)){

                $plot->metable()->delete();
            }
            if ($plot->getMeta('date' , false)){

                $plot->metable()->delete();
            }


	    }


	    return "successfully set the plot to sell";
	}

	/**
	* @param array $data
	* @param array $options
	*/
	protected function filter(array $data, array $options = [])
	{

	// TODO: Implement execute() method.
	}
}
