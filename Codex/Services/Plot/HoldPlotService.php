<?php


namespace Codex\Services\Plot;
use App\DB\Business\Meta;
use App\Jobs\Plot\HoldPlotJob;
use App\Plot\Plot;
use Carbon\Carbon;
use Codex\Classes\Repository\PlotHoldingRequestRepository;
use Codex\Classes\ServiceAbstract;

Class HoldPlotService extends ServiceAbstract
{

	/**
	* @param array $data
	* @param array $options
	*/
	protected function execute(array $data, array $options = [])
	{

        foreach ($data['plot_id'] as $index => $plotId) {



            $plot = Plot::where('id', $plotId)->first();

            $plot->can_sell = false;

            $plot->save();

            (new PlotHoldingRequestRepository())
                ->addMetaToHoldPlot($plot,  [
                    'reason'   => $data['reason'][$plotId],
                    'date'     => Carbon::parse($data['date'][$plotId])
                ]);
        }


        return "successfully held the plot";

	}

	/**
	* @param array $data
	* @param array $options
	*/
	protected function filter(array $data, array $options = [])
	{

	// TODO: Implement execute() method.
	}
}
