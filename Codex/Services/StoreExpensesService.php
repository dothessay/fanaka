<?php
/**
 * Created by PhpStorm.
 * User: philip
 * Date: 3/13/18
 * Time: 9:41 PM
 */

namespace Codex\Services;


use App\DB\Exepense\PettyCash;
use App\DB\Exepense\PettyCashLog;
use App\DB\Expense\Expense;
use App\DB\Finance\ChartOfAccount;
use App\Traits\IsJournable;
use Codex\Classes\Repository\ExpensesRepository;
use Codex\Classes\ServiceAbstract;

class StoreExpensesService extends ServiceAbstract
{

    use IsJournable;

    /**
     * @param array $data
     * @param array $options
     */
    protected function execute(array $data, array $options = [])
    {
        $request = request();
        $request->request->add($data);
        $request->request->add($options);
        (new ExpensesRepository())
            ->store($request);
       return "Successfully added an expenses";
    }

    protected function filter(array $data, array $options = [])
    {
        // TODO: Implement filter() method.
    }
}
