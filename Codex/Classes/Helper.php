<?php
/**
 * Created by PhpStorm.
 * User: philip
 * Date: 2/24/18
 * Time: 5:26 PM
 */

namespace Codex\Classes;


use App\User;
use Carbon\Carbon;
use Codex\Permission\Permissions;
use Iflylabs\iFlyChat;
use Illuminate\Support\Facades\Route;

class Helper
{
    public static function system()
    {
        return new System();
    }
    public static function sideMenu()
    {
        return new SideMenu();
    }
    public static function getUser($userId)
    {
        return User::where('id', $userId)->first();
    }

    public static function getSystemModules()
    {
        return self::system()->getModules();
    }

    public static function getDefaultBusinessRoles()
    {
        return self::system()->getDefaultUsers();
    }

    public static function getActiveUserType()
    {
        $user = auth()->user();

        return isset($user) ? auth()->user()->userType() : "";
    }

    public static function routeModule()
    {
        $routeItems = explode('_', Route::currentRouteName());
        return $routeItems[0];
    }

    public static function routeModuleComponent()
    {
        $routeItems = explode('_', Route::currentRouteName());


        return $routeItems[1];



    }
    public static function getArraySpecialNames(array $items)
    {
        $result = [];

        foreach ($items as $key => $value) {
            $name = $value;

            if (is_array($value)) {
                $name = $key;
            }

            array_push($result, $name);
        }

        return $result;
    }

    public static function getArraySpecialValues(array $items)
    {
        $result = [];

        foreach ($items as $key => $value) {
            $name = $value;

            if (is_array($value)) {
                $name = $value;
            }

            array_push($result, $name);
        }

        return $result;
    }

    public static function getArraySpecialNamesBySearch(array $items, $name)
    {
        $result = [];

        foreach ($items as $key => $value) {
            if (is_array($value) && $name == $key) {

                return self::getArraySpecialNames($value);
            }
        }

        return $result;
    }

    public static function componentFullName($module, $component)
    {
        return $module . '_' . $component;
    }

    public static function functionalityFullName($module, $component, $functionality)
    {
        return $module . '_' . $component . '_' . $functionality;
    }


    public static function isActiveRoute($routeIdentifier, $returnFullClass = false, $delimiter = '|')
    {

        $result = '';
        $routes = explode($delimiter, $routeIdentifier);
        $currentRoute = implode('_', collect(explode('_', Route::currentRouteName()))->take(2)->toArray());


        foreach ($routes as $route) {
            if ($currentRoute == $route) {
                $result = ' active';
            }
        }

        $result = $returnFullClass ? 'class="' . trim($result) . '"' : $result;

        return $result;

    }

    public  static  function numberToWord(float $num = 0 )
    {
        $num    = ( string ) ( ( int ) $num );

        if( ( int ) ( $num ) && ctype_digit( $num ) )
        {
            $words  = array( );

            $num    = str_replace( array( ',' , ' ' ) , '' , trim( $num ) );

            $list1  = array('','one','two','three','four','five','six','seven',
                'eight','nine','ten','eleven','twelve','thirteen','fourteen',
                'fifteen','sixteen','seventeen','eighteen','nineteen');

            $list2  = array('','ten','twenty','thirty','forty','fifty','sixty',
                'seventy','eighty','ninety','hundred');

            $list3  = array('','thousand','million','billion','trillion',
                'quadrillion','quintillion','sextillion','septillion',
                'octillion','nonillion','decillion','undecillion',
                'duodecillion','tredecillion','quattuordecillion',
                'quindecillion','sexdecillion','septendecillion',
                'octodecillion','novemdecillion','vigintillion');

            $num_length = strlen( $num );
            $levels = ( int ) ( ( $num_length + 2 ) / 3 );
            $max_length = $levels * 3;
            $num    = substr( '00'.$num , -$max_length );
            $num_levels = str_split( $num , 3 );

            foreach( $num_levels as $num_part )
            {
                $levels--;
                $hundreds   = ( int ) ( $num_part / 100 );
                $hundreds   = ( $hundreds ? ' ' . $list1[$hundreds] . ' Hundred' . ( $hundreds == 1 ? '' : '  ' ) . ' ' : '' );
                $tens       = ( int ) ( $num_part % 100 );
                $singles    = '';

                if( $tens < 20 )
                {
                    $tens   = ( $tens ? ' ' . $list1[$tens] . ' ' : '' );
                }
                else
                {

                    $tens   = ( int ) ( $tens / 10 );
                    $tens   = ' ' . $list2[$tens] . ' ';
                    $singles    = ( int ) ( $num_part % 10 );
                    $singles    = ' ' . $list1[$singles] . ' ';
                }
                $words[]    = $hundreds . $tens . $singles . ( ( $levels && ( int ) ( $num_part ) ) ? ' ' . $list3[$levels] . ' ' : '' );
            }

            $commas = count( $words );

            if( $commas > 1 )
            {
                $commas = $commas - 1;
            }

            $words  = implode( ', ' , $words );

            //Some Finishing Touch
            //Replacing multiples of spaces with one space
            $words  = trim( str_replace( ' ,' , ',' , trim_all( ucwords( $words ) ) ) , ', ' );
            if( $commas )
            {
                $words  = str_replace_last( ',' , ' and ' , $words );
            }

            return $words . "  " .config('config.currency_string')." Only" ;
        }
        else if( ! ( ( int ) $num ) )
        {
            return 'Zero';
        }
        return '';
    }



    public static function reportWidget($title, $href, $desc, $test = false, $suffix = 'REPORT')
    {
        $outpout = '';
        $shouldReturn = $test;

        if (!$test) {

            if (Permissions::checkAccessToModuleComponentFunctionality(auth()->user(), $href)) {
                $shouldReturn = true;

            }

        }

        if ($shouldReturn) {

            $outpout = '<div class="col-md-3 col-lg-4 col-sm-6 col-sx-6"><a href="' . route($href) . '">
					<div class="card card-inverse card-primary text-center widget widget-stats bg-uzapoint">
						<div class="card-block stats-desc text-uzapoint-light">
							<blockquote class="card-blockquote">
								<p class="f-s-14 text-uzapoint text-uzapoint-bold">' . strtoupper($title . ' ' . $suffix . '') . '</p>
								<footer class="f-s-12  text-uzapoint-light">' . $desc . '</footer>
							</blockquote>
						</div>
					</div>
					</a>
					</div>';


            /*$outpout = '<div class="col-md-3 col-md-6"><a href="' . self::route($href) . '">
            <div class="widget widget-stats bg-uzapoint">
            <div class="stats-title text-uzapoint text-uzapoint-bold">' . strtoupper($title . ' '.$suffix.'') . '</div>
            <div class="stats-desc text-uzapoint-light">' . $desc . '</div>
            </div></a></div>';*/
        }

        return $outpout;
    }

    public static function isCurrentWeek($date = null, $ofSameYear = false)
    {
        return (new Carbon())->isSameAs($ofSameYear ? 'Y-m' : 'w', $date);
    }

    public static function chatWidget()
    {
        $iflyChat = new iFlyChat("9dd03481-277f-4490-ac2c-6cfc7dab30b7","B3QMUphQxdUgNFuKbAv9M4bapcAo6DQhpYfzZRcJR8gW42704");




            $userId = auth()->id() ? : 1;

            $userName = auth()->user()->fullName() ? : "Guest";
            $image = auth()->user()->image() ? : " ";

            $isAdmin = FALSE;


            if (auth()->user()->getUserType() === 'admin' || auth()->user()->getUserType() === 'developer'  )
            {
                $isAdmin = TRUE;

            }

            $user = array(
                'user_name' => "{$userName}", // string(required)
                'user_id' => "{$userId}",
                'is_admin' => $isAdmin,
                'user_avatar_url' => "{$image}", // string (optional)
                'user_profile_url' => url('profile/edit/'.$userId), // string (optional)
            );






        $iflyChat->setUser($user);



        $iflyChatCode = $iflyChat->getHtmlCode();



        return $iflyChatCode;
    }

}
