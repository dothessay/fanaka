<?php
/**
 * Created by IntelliJ IDEA.
 * User: philip
 * Date: 6/11/19
 * Time: 3:40 PM
 */

namespace Codex\Classes\Handlers\Exports;


use App\DB\Communication\Contact;
use Carbon\Carbon;
use Illuminate\Contracts\View\View;
use Illuminate\Database\Query\Builder;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\WithHeadings;

class ContactsExports implements FromQuery , WithHeadings
{


    use Exportable;

    private $startDate;
    private $endDate;

    function __construct($startDate , $endDate)
    {
        $this->startDate = $startDate;
        $this->endDate = $endDate;
    }

    /**
     * @return Builder
     */
    public function query()
    {
        return Contact::query()
            ->whereBetween('created_at', [Carbon::parse($this->startDate)->startOfDay() , Carbon::parse($this->endDate)->endOfDay()])
            ->where('user_id', auth()->id())
            ->select('name','phone','hasvisited');



    }

    /**
     * @return array
     */
    public function headings(): array
    {
        return [
            'Name',
            'phone',
            'hasVisited',
        ];
    }
}
