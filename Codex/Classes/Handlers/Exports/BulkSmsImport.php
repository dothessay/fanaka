<?php
/**
 * Created by IntelliJ IDEA.
 * User: philip
 * Date: 6/18/19
 * Time: 9:59 AM
 */

namespace Codex\Classes\Handlers\Exports;


use Illuminate\Support\Collection;
use App\Jobs\SendSmsNotifications;
use Carbon\Carbon;
use Codex\Classes\Handlers\SendAdvantaAfricaInnoxNotification;
use Codex\Classes\Handlers\SendInnoxNotification;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Mail;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromArray;
use Maatwebsite\Excel\Concerns\ToArray;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithCalculatedFormulas;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\WithHeadings;
use PhpOffice\PhpSpreadsheet\Shared\Date;

class BulkSmsImport implements ToModel , WithHeadingRow , FromArray,WithHeadings
{
    use   Exportable;
    private $message;
    private $phone_number;

    public function __construct($message = null)
    {
        $this->message = $message;
    }


    private function dateFormat($date)
    {
        return Carbon::instance(Date::excelToDateTimeObject($date))->toFormattedDateString();
    }

    public function model(array  $row)
    {

        $message =  ($this->message);
        if (isset($row['message']))
        {
            $message = $row['message'];
        }

        // $message = str_slug(ucfirst($message),  '_');
        //$message = str_replace('_', ' ', $message);

        if (isset($row['name']))
        {
            $message = str_replace('{client}',$row['name'] , $message);
        }
        if (isset($row['expired_on']))
        {
            $date =  $this->dateFormat($row['expired_on']);
            $message = str_replace('{expiry_date}',$date, $message);
        }
        if (isset($row['sale_date']))
        {
            $date =  $this->dateFormat($row['sale_date']);

            $message = str_replace('{sale_date}',$date , $message);
        }
        if (isset($row['project_plot']))
        {

            $message = str_replace('{project_plot}',$row['project_plot'] , $message);
        }
        if (isset($row['balance']))
        {
            $message = str_replace('{balance}', number_format( floatval($row['balance'])) , $message);
        }
        $phone = explode('.', $row['phone'])[0];
        $this->message = $message;
        $this->phone_number = $phone;

        dispatch(new SendSmsNotifications([
            'to' => $this->phone_number,
            'message' =>   $this->message,
        ]));
    }

    /**
     * @inheritDoc
     */
    public function array(): array
    {
        return  [
            [
                $this->message,
                $this->phone_number
            ]
        ];

    }

    /**
     * @inheritDoc
     */
    public function headings(): array
    {
        return  [
            'mobile number',
            'message'
        ];
    }



}
