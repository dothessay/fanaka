<?php



namespace Codex\Classes\Handlers\Exports;


use Illuminate\Contracts\View\View;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\FromView;

class ExportSaleType implements FromView
{
    /**
     * @var Collection $sales
     */
    private $sales;

    public function __construct(Collection $sales)
    {
        $this->sales = $sales;
    }

   /**
     * @inheritDoc
     */
    public function view(): View
    {
        return view('sales.report.project.data')->with([
            'sales' => $this->sales
        ]);
    }
}
