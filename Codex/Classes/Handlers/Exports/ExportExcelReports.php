<?php


namespace Codex\Classes\Handlers\Exports;


use Illuminate\Contracts\View\View;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\FromView;

class ExportExcelReports implements FromView
{
    /**
     * @var  $view
     */
    private $view;

    public function __construct( $view)
    {
        $this->view = $view;
    }

    /**
     * @inheritDoc
     */
    public function view(): View
    {
        return  $this->view;
    }
}
