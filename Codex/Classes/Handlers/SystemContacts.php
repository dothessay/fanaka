<?php
/**
 * Created by Philip Njuguna.
 * User: Philip Njuguna
 * Date: 11/19/18
 * Time: 3:38 PM
 */

namespace Codex\Classes\Handlers;


use App\Customer\Customer;
use App\DB\Communication\Contact;
use App\DB\Sale\OfferLetter;

class SystemContacts
{


    public static  function build()
    {
        return new self();


    }

    public function index()
    {
        $contacts =  collect();

        if (auth()->user())
        {
            foreach (auth()->user()->customers() as $customer) {

                $clientContacts = new GetClientsContactHandler();


                $clientContacts->getPhone($customer->getPhoneNumber());
                $clientContacts->getFullName($customer->fullName());

                $clientContacts->getHasSiteVisit(true);
                $clientContacts->getIsCustomer(true);

                $contacts->push($clientContacts);

            }

            foreach (Contact::where('user_id', auth()->id())->get() as $contact) {

                $clientContacts = new GetClientsContactHandler();


                $clientContacts->getPhone($contact->phone);
                $clientContacts->getFullName($contact->name);
                $clientContacts->getHasSiteVisit($contact->hasVisited ? " YES" : "NO");
                $clientContacts->getIsCustomer(false);

                $contacts->push($clientContacts);

            }
        }
        /*foreach (Customer::all() as $customer)
        {

            $clientContacts = new GetClientsContactHandler();



            $clientContacts->getPhone($customer->getPhoneNumber());
            $clientContacts->getFullName($customer->fullName());

            $contacts->push($clientContacts);

        }*/


        return $contacts->filter()->toArray();


    }
}
