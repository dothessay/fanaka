<?php
/**
 * Created by IntelliJ IDEA.
 * User: philip
 * Date: 4/1/19
 * Time: 12:09 PM
 */

namespace Codex\Classes\Handlers;


use Pusher\Pusher;

class PusherMessages
{


    private $pusher;

    public function __construct()
    {

        $options = array(
            'cluster' => 'ap2',
            'useTLS' => true
        );

        $this->pusher = new Pusher(
            '3fb0427f92482c9a4df7',
            '0d0cc5905b5190b9972d',
            '749187',
            $options
        );
    }


    public static function build()
    {
        return new  self();

    }

    public function send($data)
    {

        $this->pusher->trigger('customer', 'customer', $data);

    }
}