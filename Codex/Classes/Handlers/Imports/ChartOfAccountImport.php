<?php


namespace Codex\Classes\Handlers\Imports;


use App\DB\Finance\ChartOfAccount;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class ChartOfAccountImport implements ToModel , WithHeadingRow
{
    /**
     * @param array $row
     *
     * @return Model|Model[]|null
     */
    public function model(array $row)
    {
        return new ChartOfAccount([
            'code' => $row['code'],
            'name' => $row['name'],
        ]);
    }
}
