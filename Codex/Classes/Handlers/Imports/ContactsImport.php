<?php
namespace Codex\Classes\Handlers\Imports;
use App\DB\Business\Meta;
use App\DB\Communication\Contact;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithCalculatedFormulas;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class ContactsImport implements ToModel , WithHeadingRow , WithCalculatedFormulas
{

    /**
     * @param array $row
     *
     * @return Model|Model[]|null
     */
    public function model(array $row)
    {

        if (! Contact::where(['phone' => $row['phone']])->count() && ! is_null($row['phone']))
        {

            $contact =  new Contact([
                'phone' => $row['phone'],
                'name' => $row['name'],
                'hasVisited' =>  isset($row['hasvisited']) ? true : false,
                'user_id' =>  isset($row['agentid']) ? $row['agentid'] : auth()->id() ,
                'date' => isset($row['date']) ? Carbon::parse($row['date']) : null,
            ]);

            if ( isset($row['comment']))
            {
                $meta = Meta::create([
                    'key' => "status",
                    'value' => "comment"
                ]);
                $contact->saveMeta($meta);

            }

            return  $contact;
        }

    }
}
