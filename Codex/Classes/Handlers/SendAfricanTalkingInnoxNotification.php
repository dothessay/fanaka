<?php
/**
 * Created by Philip Njuguna.
 * User: Philip Njuguna
 * Date: 11/9/18
 * Time: 11:37 AM
 */

namespace Codex\Classes\Handlers;

use AfricasTalking\SDK\AfricasTalking;
use Codex\Contracts\ShouldSendInnoxNotification;


class SendAfricanTalkingInnoxNotification implements ShouldSendInnoxNotification
{

    public $africasTalking;

    public function __construct()
    {
        $this->africasTalking = new AfricasTalking(fanakaConfig('afrika_talking_username'), fanakaConfig('afrika_takling_api_key'));

    }


    public function sendNotification(array $data)
    {
        if (!fanakaConfig('send_sms')) {
            return true;

        }
        try {

            $sms = $this->africasTalking->sms();
            $result = $sms->send([
                'to' => $data['to'],
                'message' => $data['message'],
                //'from'    =>  "AFRICASTKNG"
            ]);



            return $result;

        } catch (\Exception $e) {

            return $e;
        }

    }

    public function makeCall($data = [])
    {
        try {

            /*$from     = "+254714686511";

            // Set the numbers you want to call to in a comma-separated list
            $to       = "+25487698173";

            try {
                // Make the call
                $results = $this->africasTalking->sms()->call([
                    'from' => $from,
                    'to'   => $to
                ]);

                print_r($results);
            } catch (\Exception $e) {
                echo "Error: ".$e->getMessage();
            }*/

        } catch (\Exception $e) {

            return $e;
        }
    }


    public function getBalance()
    {
        $result =  $this->africasTalking->application()->doFetchApplication();


        $balance = $result['data']->UserData->balance;


        return $balance;


    }

    public function getMessages()
    {
        $result =  $this->africasTalking->sms()->fetchMessages(['lastReceivedId' => 1]);

        $messages = $result['data']->SMSMessageData->Messages;


        return $messages;


    }
}
