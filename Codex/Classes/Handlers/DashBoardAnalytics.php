<?php
/**
 * Created by PhpStorm.
 * User: philip
 * Date: 2/28/19
 * Time: 10:35 AM
 */

namespace Codex\Classes\Handlers;

use App\DB\Accounting\Account;
use App\Sale\SalePayment;
use Carbon\Carbon;

class DashBoardAnalytics
{

    private $performance;

    private  $startOfLastYear;

    private $endOfLastYear;
    private $endOfLastMonth;
    private $endOfPreviousDate;


    public function __construct()
    {
        $this->endOfLastYear = Carbon::now()->subMonth(Carbon::MONTHS_PER_YEAR);

        $this->endOfLastMonth = Carbon::now()->subMonth(1);

        $this->startOfLastYear = Carbon::now()->subMonth(Carbon::MONTHS_PER_YEAR)->startOfYear();
    }


    public static function build()
    {
        return new self();

    }

    public function output()
    {


        return $this->performance;

    }


    public function year()
    {

        $current = ($this->thisYearSale() < 1) ? 1 : $this->thisYearSale();

        $previous = $this->lastYearSale() < 1 ? $current: $this->lastYearSale();


        $this->performance = $this->performace("This Year Income",$current, $previous , 'Compared to Last Year','year');

        return $this;


    }
    public function month()
    {
        $current = ($this->thisMonthSale() < 1) ? 1 : $this->thisMonthSale();

        $previous = ($this->lastMonthSale() < 1) ? $current : $this->lastMonthSale();


        $this->performance = $this->performace("This Month Income",$current, $previous ,'Compared to Last Month','month');


        return $this;


    }

    public function today()
    {

        $current = ($this->thisDaySale() < 1) ? 1 : $this->thisDaySale();

        $previous = ($this->lastDaySale() < 1) ? $current : $this->lastDaySale();

        $this->performance = $this->performace("Today Income",$current, $previous ,'Compared To Yesterday','today');

        return $this;


    }

    private function thisYearSale()
    {
        $sales = Account::approved()->whereBetween(
            'created_at' ,[
                Carbon::now()->startOfYear(),
                Carbon::now()->endOfYear()
        ])->get()->sum('amount');


        return floatval($sales);

    }

    private function lastYearSale()
    {
        $sales = Account::approved()->whereBetween(
            'created_at' ,[
                $this->startOfLastYear,
                $this->endOfLastYear
        ])->sum('amount');

        return floatval($sales);

    }


    private function thisMonthSale()
    {
        $sales = Account::approved()->whereBetween(
            'created_at' ,[
                Carbon::now()->startOfMonth(),
                Carbon::now()->endOfMonth()
        ])->get()->sum('amount');


        return floatval($sales);

    }

    private function lastMonthSale()
    {
        $sales = Account::approved()->whereBetween(
            'created_at' ,[
                Carbon::now()->startOfMonth()->subMonth(1)->startOfMonth(),
                $this->endOfLastMonth
        ])->sum('amount');

        return floatval($sales);

    }
    private function thisDaySale()
    {
        $sales = Account::approved()->whereBetween(
            'created_at' ,[
                    Carbon::now()->startOfDay(),
                Carbon::now()->endOfDay(),

        ])->get()->sum('amount');


        return floatval($sales);

    }

    private function lastDaySale()
    {
        $sales = Account::approved()->whereBetween(
            'created_at' ,[
                Carbon::now()->startOfDay()->subDay(1)->startOfDay(),
                Carbon::now()->startOfDay()->subDay(1)->endOfDay()
        ])->sum('amount');

        return floatval($sales);

    }


    private function performace($title , $current , $previous , $compared , $filter)
    {

        $percentage =  (( ($current - $previous) / $previous) * 100 );


        $percentage = $percentage < 1 ? $percentage * -1 : $percentage;

        $performance  = '';


        if ($previous < $current) {

            $performance =  "<i class='ion-md-trending-up'>  (". $percentage . "%)  ". $compared."</i>";
        }
        if ($previous > $current) {

            $performance = "<i class='ion-md-trending-down'> (". $percentage . "%)  ". $compared."</i>";
        }
        if ($previous == $current) {

            $performance =  "<i class='ion-md-remove'> (". $percentage .")% ".$compared. " </i>";
        }

        return  $this->htmlOutPut($title,$current , $performance , $filter);



    }

    private function htmlOutPut($title , $figure , $perfomance ,$filter)
    {
        ob_start();
        ?>

        <div class="col-lg-4 col-md-6">
            <div class="widget widget-stats bg-blue-lighter">
                <div class="stats-icon stats-icon-lg"><i class="fa fa-money fa-fw"></i></div>
                <div class="stats-content">
                    <div class="stats-title"><?= $title?></div>
                    <div class="stats-number"><?= config('config.currency') .' '.  number_format(floatval($figure) , 2)?></div>
                    <div class="stats-progress progress">
                        <div class="progress-bar" style="width: 70.1%;"></div>
                    </div>
                    <div class="stats-desc" style="color: black"><?= $perfomance ?>
                    </div>
                    <div class="stats-link">
                        <a href="<?= url('sale/manage?filter='.$filter)?>">View Detail <i class="fa fa-arrow-alt-circle-right"></i></a>
                    </div>
                </div>
            </div>
        </div>


        <?php

        return ob_get_clean();

    }
}