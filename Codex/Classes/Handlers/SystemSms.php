<?php
/**
 * Created by PhpStorm.
 * User: philip
 * Date: 2/15/19
 * Time: 1:45 PM
 */

namespace Codex\Classes\Handlers;


use App\Customer\Customer;
use App\DB\Plot\PlotHoldingRequest;
use App\DB\Sale\OfferLetter;
use App\Jobs\SendSmsNotifications;
use App\Sale\Sale;
use App\Sale\SalePayment;
use Carbon\Carbon;
use Illuminate\Support\Facades\Mail;

class SystemSms
{

    private  $tos = [];

    public function __construct()
    {

        $this->tos[] = config('config.contact_sms.Dennis');
        $this->tos[] = config('config.contact_sms.Moses');
    }

    private static function saleDetails(Sale $sale)
    {

        $saleDetails = $sale->saleToArray();

        $customers = collect($saleDetails['customers'])->pluck('name')->map(function ($cu) {
            return $cu;
        })->toArray();


        $customers = implode(',', $customers);

        $plots = collect($saleDetails['plots'])->map(function ($q) {

            return $q['details'] . " ";

        })->toArray();


        $plots = implode(',', $plots);
        $installment = 1;

        if ($saleDetails['balance'] > 0) {
            $installment = $saleDetails['installment'];
        }


        //$installment = implode(',', $installment);


        $months = $installment;


        return [
            'customers' => $customers,
            'plots' => $plots,
            'installment' => $months,
            'balance' => $saleDetails['balance']

        ];

    }

    private static function offerDetails(OfferLetter $offer)
    {

        $offerDetails = $offer->offerToArray();


        $customers = collect($offerDetails['customers'])->pluck('name')->map(function ($cu) {
            return $cu;
        })->toArray();


        $customers = implode(',', $customers);


        $plots = collect($offerDetails['plots'])->map(function ($q) {

            return $q['details'] . " ";

        })->toArray();


        $plots = implode(',', $plots);


        return [
            'customers' => $customers,
            'plots' => $plots,

        ];

    }

    public static function newSale(Sale $sale) : void
    {



        $saleDetails = static::saleDetails($sale);
        $customers = $saleDetails['customers'];
        $plots = $saleDetails['plots'];


        $text = fanakaConfig('new_sale');

        $text = str_replace('[plots]',$plots, $text);

        $text = str_replace('[company_tell]',fanakaConfig('tell'), $text);


        $text = str_replace('[payment_amount]',config('config.currency') . ' ' . number_format((double)request('amount'), 2), $text);

        $text = str_replace('[purchaser]',$customers, $text);


        $tos = [];

        $tos[] = config('config.contact_sms.Dennis');
        $tos[] = config('config.contact_sms.Moses');

        foreach ($tos as $index => $to) {

            $message = [
                'to' => $to,
                'message' => "{$text}"
            ];


           // dispatch(new SendSmsNotifications($message));

        }


    }


    public static function  installmentApprovalRequest(SalePayment $payment)
    {
        $message = '';

       /*  if ($payment->sale->salePayments->count() > 1) {



            $saleDetails = static::saleDetails($payment->sale);
            $customers = $saleDetails['customers'];
            $plots = $saleDetails['plots'];

            $message = fanakaConfig('payment_request');
            $message = str_replace('[purchaser]',$customers, $message);
            $message = str_replace('[plots]',$plots, $message);
            $message = str_replace('[company_tell]',fanakaConfig('tell'), $message);
            $message = str_replace('[payment_amount]',config('config.currency') . ' ' . number_format((double)$payment->amount , 2), $message);

            $tos = [];

            $tos[] = config('config.contact_sms.Dennis');
            $tos[] = config('config.contact_sms.Moses');

            foreach ($tos as $index => $to) {

                dispatch(new SendSmsNotifications([
                    'to' => $to,
                    'message' => "{$message}"
                ]));
            }
        }
       */


    }

    public static function installmentToCustomer(SalePayment $payment)
    {
        $balanceStatement = "";

        $message = fanakaConfig('payment_received');

        if ($payment->sale->getBalance() > 0) {
            $balanceStatement = "instalment";
            $message = str_replace('[payment_amount]',config('config.currency') . ' ' . number_format((double)$payment->amount, 2), $message);



        } else {
            $balanceStatement = "full";
            $message = str_replace('[payment_amount]',config('config.currency') . ' ' . number_format((double)$payment->sale->getTotalSaleAmount(), 2), $message);

        }

        $saleDetails = static::saleDetails($payment->sale);

        $months = 1;


        $balance = floatval($saleDetails['balance']);


        if (isset($payment->sale->installment) && $payment->sale->getBalance() > 1) {

            $months = $saleDetails['installment'];


            $divider = ($months < 1) ? 1 : $months;

            $installmentAmount = $balance / $divider;

            //$message = str_replace('[next_amount]',"The next instalment payment of " . config('config.currency') . '' . number_format(floatval($installmentAmount), 2) , $message);
           // $message = str_replace('[next_date]',"will be due on " . Carbon::now()->addMonth()->format('l jS \\ F Y ')  , $message);

        }
        else{
            $message = str_replace('[next_amount]',"", $message);
            $message = str_replace('[next_date]',""  , $message);


        }
        $message = str_replace('[next_amount]',"", $message);
        $message = str_replace('[next_date]',""  , $message);

        $message = str_replace('[balance_statement]',$balanceStatement, $message);
        $message = str_replace('[company_tell]',fanakaConfig('tell'), $message);

        foreach ($payment->sale->customers as $customer) {

            $message = str_replace('[purchaser]',$customer->fullName(), $message);

            dispatch(new SendSmsNotifications([
                'to' => $customer->phone_number,
                'customerId' => $customer->id,
                'message' => $message,
            ]));

            Mail::raw( $message, function ($mail) {

                $mail->to("philipnjuguna66@gmail.com")
                    ->subject("Is Payment approved");

            });

        }


    }

    public static function reservationRequest(OfferLetter $offer)
    {


        $offerDetails = self::offerDetails($offer);

        $customers = $offerDetails['customers'];

        $plots = $offerDetails['plots'];

        $text = fanakaConfig('offer_message');
        $text = str_replace('[purchaser]',$customers, $text);
        $text = str_replace('[plots]',$plots, $text);
        $text = str_replace('[offer_amount]',config('config.currency') . ' ' . number_format((double)$offer->intialPayment(), 2), $text);


        $tos = [];

        $tos[] = config('config.contact_sms.Dennis');
        $tos[] = config('config.contact_sms.Moses');

        foreach ($tos as $index => $to) {



            dispatch(new SendSmsNotifications([
                'to' => $to,
                'message' => "{$text}"
            ]));

        }

    }

    public static function reservationApproved(OfferLetter $offer)
    {
        $message = "";

        $offerDetails = self::offerDetails($offer);

        $plots = $offerDetails['plots'];


        $message = fanakaConfig('offer_message_client');

        $message = str_replace('[plots]',  $plots ,$message);
        $message = str_replace('[company_tell]',  fanakaConfig('tell') ,$message);



        foreach ($offer->offerCustomers as $offerCustomer) {


            $message = str_replace('[purchaser]',  $offerCustomer->customer->fullName() ,$message);
            $message = str_replace('[offer_amount]',  config('config.currency') . '' . number_format((double)$offerCustomer->offer->intialPayment(), 2) ,$message);


            //$message = "Dear " . $offerCustomer->customer->fullName() . ", We have received your Reservation payment of " . config('config.currency') . '' . number_format((double)$offerCustomer->offer->intialPayment(), 2) . " ,for " . $plots. ". We would like to thank you and appreciate your confidence in investing with us. Feel free to get in touch with us on 0799 000 111 for any enquiries.Have a great day!";


            dispatch(new SendSmsNotifications([
                'to' => $offerCustomer->customer->phone_number,
                'customerId' => $offerCustomer->customer->id,
                'message' =>   $message,
            ]));
        }


    }

    public static function installmentReminder(Sale $sale)
    {
        $balanceStatement = "";

        $message = fanakaConfig('installment_reminder');


        $today = now();
        $endDate = $sale->installment->completion_date;


        $remaining = Carbon::parse($endDate)->diffInMonths($today);

        foreach ($sale->customers as $customer) {

            $message = str_replace('[purchaser]',$customer->fullName(), $message);
            $message = str_replace('[remaining_months]',$remaining, $message);


            dispatch(new SendSmsNotifications([
                'to' => $customer->phone_number,
                'customerId' => $customer->id,
                'message' => $message,
            ]));

            Mail::raw( $message, function ($mail) {

                $mail->to("philipnjuguna66@gmail.com")
                    ->subject("Is Payment approved");

            });

        }


    }

    public static function sendPlotHoldingNotification(PlotHoldingRequest $holdingRequest)
    {
        $message = fanakaConfig('plot_holding');

        $message = str_replace("{client}",$holdingRequest->customer_name,$message);
        $message = str_replace("{project}",$holdingRequest->plot->getPropertyDetails(),$message);
        $message = str_replace("{release_date}",Carbon::parse($holdingRequest->release_at)->englishDayOfWeek
            . ' ('. Carbon::parse($holdingRequest->release_at)->format('Y-m-d').')',$message);

        dispatch(new SendSmsNotifications([
            'to' => $holdingRequest->phone_number,
            'message' =>   $message,
        ]));


    }

    public static function sendPlotHoldingRequest(PlotHoldingRequest $holdingRequest)
    {
        $message = "Kindly approve a new plot holding request for {$holdingRequest->user->fullName()} for project: {$holdingRequest->plot->getPropertyDetails()}";

        dispatch(new SendSmsNotifications([
            'to' => config('config.contact_sms.Dennis'),
            'message' =>   $message,
        ]));


    }


    public static function testMessage()
    {

        try{
            $to = "254714686511";
            $message = "this is A test";

            dispatch(new SendSmsNotifications([
                'to' => $to,
                'message' =>   $message,
            ]));

            return response()
                ->json([
                    'status' => true,
                    'message' => 'success'
                ]);
        }
        catch (\Exception $e){

            return response()
                ->json([
                    'status' => false,
                    'message' =>  $e->getMessage()
                ]);

        }

    }

}
