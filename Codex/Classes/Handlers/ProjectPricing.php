<?php
/**
 * Created by IntelliJ IDEA.
 * User: philip
 * Date: 5/11/19
 * Time: 12:10 PM
 */

namespace Codex\Classes\Handlers;

use \App\DB\Plot\ProjectPricing as PlotPricing;


use App\Plot\Plot;

class ProjectPricing
{


    /**
     * @var Plot
     */
    private $plot;

    public function __construct(Plot $plot)
    {

        $this->plot = $plot;
    }

    public function getPricing()
    {

        $pricing = PlotPricing::where('project_id', $this->plot->product->id)
            ->where('start_at' ,'>=', now())
            ->where('end_at' , '<=', now())
            ->get();

    }

}
