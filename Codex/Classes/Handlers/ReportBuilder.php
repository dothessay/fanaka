<?php
/**
 * Created by PhpStorm.
 * User: philip
 * Date: 1/8/19
 * Time: 1:47 PM
 */

namespace Codex\Classes\Handlers;


use App\DB\Product\Product;
use App\DB\Sale\OfferLetter;
use App\DB\Sale\OfferLetterItem;
use App\Plot\Plot;
use App\Sale\Sale;
use App\Sale\SaleItem;
use Carbon\Carbon;

class ReportBuilder
{


    public static function build()
    {
        return new self();

    }

    public function below30K()
    {

        $sales =  $this->salesBelow30K();


        $reservations = $this->reservationsBelow30K();


        $toReturn = [];


        foreach ($sales as $sale) {

            $toReturn[]  = [
                'ref'     => "FSL/".$sale->id,
                'customer' => $sale->getCustomerHref(),
                'type'     => "sale",
                'plot'    => $sale->getCustomerPlotNos(),
                'amount'   => $sale->getPaidAmount(),
                'date'   => $sale->created_at,
            ];

        }

        foreach ($reservations as $reservation) {

            $toReturn[]  = [
                'ref'      => "RES/".$reservation->id,
                'customer' => $reservation->getCustomerHref(),
                'type'     => "reservation",
                'plot'     => $reservation->getCustomerPlotNos(),
                'amount'   => $reservation->getPaidAmount(),
                'date'     => $reservation->created_at
            ];

        }

        return $toReturn;

    }

    public function salesBelow30K()
    {
        $sales = collect();

        foreach (Sale::all() as $sale) {

            if (floatval($sale->getPaidAmount()) <= 30000){

                $sales->push($sale);

            }

        }

        return $sales;

    }

    private function reservationsBelow30K(){

        $reservations = collect();

        foreach (OfferLetter::where('is_converted', false)->get() as $offerLetter) {

            if (floatval($offerLetter->getPaidAmount()) <= 30000){

                $reservations->push($offerLetter);

            }

        }
        return $reservations;

    }

    public function allUnPaid($project , $onlyOverDue = false)
    {

        $sales = collect();

        if ($project)
        {
            $plots = $project->plots;

            foreach ($plots as $plot) {

                $saleItem = $plot->saleItem;

                if ($saleItem)
                {
                    $sale = $saleItem->sale;
                    if (! auth()->user()->can('access-module-component-functionality', 'sale_manage_show-all-sales')) {
                        $sale = $saleItem->sale->where('agent', auth()->id())->first();
                    }
                    if (floatval($sale->getBalance()) > 1 ) {

                        $sales->push($sale);
                    }
                }

            }

        }

        $toReturn = [];


        foreach ($sales as $sale) {

            $color = '';

            if ( isset($sale->payments()->first()->created_at) && ! Carbon::parse($sale->payments()->first()->created_at)->isLastMonth() && ! Carbon::parse($sale->payments()->first()->created_at)->isCurrentMonth())
            {
                $color = 'orange';
            }

            if ( isset($sale->installment) && Carbon::parse($sale->installment->completion_date)->lessThan(now()))
            {
                $color = "orange";
                $toReturn[] = $this->toReturn($sale , $color);
            }
            else{
                if ( ! $onlyOverDue)
                {
                    $toReturn[] = $this->toReturn($sale , $color);
                }

            }

        }

        return collect($toReturn)->unique('ref')->toArray();

    }

    public function allPlotsWithClients( $product = null)
    {

        //all reserved
        
        $plotArray = [];

        if ( is_null($product)){
            return $plotArray;

        }

        foreach ($product->plots as $plot)

        {
            foreach (OfferLetterItem::where('plot_id', $plot->id)->get() as $reservation) {


                if ($reservation->plot && ! $reservation->offer->is_converted){

                    $plotArray[] = [
                        'customer'      => $reservation->offer->getCustomerHref(),
                        'phone_number'  => $reservation->offer->getCustomerPhoneNumber(),
                        'plot'          => $reservation->offer->getCustomerPlotNos(),
                        'plotNo'        => $reservation->plot->plot_no,
                        'amount'        => $reservation->offer->getPaidAmount(),
                        'balance'       => floatval($reservation->offer->getBalance()) - floatval($reservation->offer->getPaidAmount()),
                        'plotId'        => $reservation->plot->id,
                        'agent'         => $reservation->offer->agent->fullName(),
                        'refCode'       => $reservation->offer->id,
                        'date'          => $reservation->offer->created_at,
                        'type'          => "Reservation",

                    ];
                }
            }

            foreach (SaleItem::where('plot_id', $plot->id)->get() as $saleItem) {

                if ($saleItem->plot){

                    $plotArray[] = [
                        'customer'      => $saleItem->sale->getCustomerHref(),
                        'phone_number'  => $saleItem->sale->getCustomerPhoneNumber(),
                        'plot'          => $saleItem->sale->getCustomerPlotNos(),
                        'plotNo'        => $saleItem->plot->plot_no,
                        'amount'        => $saleItem->sale->getPaidAmount(),
                        'balance'       => $saleItem->sale->getBalance(),
                        'plotId'        => $saleItem->plot->id,
                        'agent'         => $saleItem->sale->soldBy->fullName(),
                        'refCode'       => $saleItem->sale->id,
                        'date'          => $saleItem->sale->created_at,
                        'type'          => "Sale",

                    ];
                }
            }
        }
        return collect($plotArray)->unique('plotId')->all();
    }

    private function toReturn(Sale $sale , $color)
    {
        $data = [
            'ref'       => "FSL/".$sale->id,
            'customer'  => $sale->getCustomerHref(),
            'phone_number'  => $sale->getCustomerPhoneNumber(),
            'agent'     => $sale->soldBy->fullname(),
            'type'      => "sale",
            'color'     => $color,
            'plot'      => $sale->getCustomerPlotNos(),
            'amount'    => $sale->getPaidAmount(),
            'balance'   => $sale->getBalance(),
            'percentage'=> $sale->getPercentage(),
            'date'      =>  Carbon::parse($sale->created_at)->format('Y-m-d'),
            'last_amount'       => $sale->payments()->first()->amount,
            'last_date'         => Carbon::parse($sale->payments()->first()->created_at)->format('Y-m-d'),
            'completion_date'   => Carbon::parse($sale->created_at)->addMonths($sale->installment ? $sale->installment->months : 6)->format('Y-m-d'),
        ];

        return  $data;

    }


    public function profitAndLoss()
    {
        
    }

}
