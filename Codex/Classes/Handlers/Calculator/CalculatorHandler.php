<?php
namespace Codex\Classes\Handlers\Calculator;

use App\DB\Product\Product;
use App\Plot\Plot;
use RuntimeException;

class CalculatorHandler
{
    private $months = 1;
    private $rate;
    private $range = [];

    public function __construct()
    {
        $this->months = 1;
        $this->rate = 0;
        $this->range[0] =  range(0,1);
        $this->range[12] = range(2,3);
        $this->range[15] = range(4,6);
        $this->range[18] = range(7,9);
        $this->range[21] = range(10,12);
    }


    public function getRate(int  $month)
    {
        $rate = 0;
       foreach ($this->range as $key => $rangeRange){
           if (in_array($month , $rangeRange)){
               $rate = $key;
           }
       }
       $this->months = $month;
       if ($this->months < 1)
       {
           $this->months = 1;
       }
       $this->rate = $rate;
       return $this;
    }

    public function calculate($price, $deposit)
    {

        $month = 1;
        $principle = $price;
        $output = collect();

        //$rate = round(($this->rate * $this->months) / 12,0, PHP_ROUND_HALF_UP );  // That has worked


        $rate = 10000 *  floatval($this->months);

        $totalInstalment = ( floatval($price) + $rate ) - floatval($deposit);

        if ($this->months < 2)
        {
            $totalInstalment = floatval($price) -  floatval($deposit);

        }

        $monthlyPayment = ($totalInstalment / $this->months);

        for($i = $this->months; $i >= 1; $i--){

            $result = [
                'month'  => $month,
                'rate' => $rate,
                'monthlyPayment'  => $monthlyPayment,

            ];
            $output->push($result);

            $month++;

        }
        return $output;
    }
    public function calculateBk($price, $deposit)
    {

        $month = 1;
        $principle = $price;
        $output = collect();

        //$rate = round(($this->rate * $this->months) / 12,0, PHP_ROUND_HALF_UP );  // That has worked

        $rate = (($this->rate) / 12 ) * $this->months;

        for($i =$this->months; $i >= 1; $i--){

            $balance = floatval($principle) - floatval($deposit);

            $depositInterest =  round((($rate * ($balance)) / $this->months) / 100 ,  0, PHP_ROUND_HALF_UP );

            $monthlyPrincipal = round($balance / $i,0, PHP_ROUND_HALF_UP);
            $monthlyPayment =round($monthlyPrincipal + $depositInterest ,0, PHP_ROUND_HALF_UP);
            $result = [
                'month'  => $month,
                'rate' => $rate,
                'balance'  => $balance,
                'depositInterest'  => $depositInterest,
                'monthlyPrincipal'  => $monthlyPrincipal,
                'monthlyPayment'  => $monthlyPayment,
            ];
            $output->push($result);
            $principle = $balance;
            $deposit = $monthlyPrincipal;
            $month++;

        }
        return $output;
    }
}
