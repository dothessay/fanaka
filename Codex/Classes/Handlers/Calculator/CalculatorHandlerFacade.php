<?php
namespace Codex\Classes\Handlers\Calculator;

use RuntimeException;

class CalculatorHandlerFacade
{

    private static function resolved($name)
    {
         return app()[$name];
    }


    public static function __callStatic($method , $args)
    {


        return (self::resolved('Calculator'))->$method(...$args);


    }


}
