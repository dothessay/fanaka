<?php
/**
 * Created by Philip Njuguna.
 * User: Philip Njuguna
 * Date: 11/22/18
 * Time: 2:22 PM
 */

namespace Codex\Classes\Handlers;


use Pusher\Pusher;

class InnoxBroadcast
{


    public static function index($data)
    {

        $options = array(
            'cluster' => 'mt1',
            //'useTLS' => true
        );
        $pusher = new Pusher(
            'b59fb47bbf17f1d5804d',
            'a403a758b190b4585cc6',
            '314505',
            $options
        );

        $pusher->trigger('issue', '\\App\\Events\\IssueNotification', $data);

    }
}