<?php
namespace Codex\Classes\Handlers;
use App\DB\Customer\CustomerFileUpload;
use App\DB\Sale\TitleProcess;

class TitleTransferProcess
{
    private $sale;
    private $titleProcess;

    /**
     * @param mixed $sale
     * @return TitleTransferProcess
     */
    public function setSale($sale)
    {
        $this->sale = $sale;
        return $this;
    }
    /**
     * @param mixed $titleProcess
     * @return TitleTransferProcess
     */
    public function setTitleProcess(TitleProcess $titleProcess)
    {
        $this->titleProcess = $titleProcess;
        return $this;
    }


    public function completed()
    {

    }

    // upload the documents client documents
    public function uploadDocuments()
    {
        try{

            $request = request();
            foreach ($request['customerId'] as $customerId)
            {
                if ($request->hasFile('kra') && ! is_null($request->file('kra')[$customerId]))
                {
                    $path = ($request->file('kra')[$customerId])->store('agreements');

                    CustomerFileUpload::create([
                        'customer_id' => $customerId,
                        'file_type' => 'KRA Pin',
                        'file_url' => $path
                    ]);
                }
                if ($request->hasFile('passport') && ! is_null($request->file('passport')[$customerId]))
                {
                    $path = ($request->file('passport')[$customerId])->store('agreements');

                    CustomerFileUpload::create([
                        'customer_id' => $customerId,
                        'file_type' => 'Passport Photo',
                        'file_url' => $path
                    ]);
                }
                if ($request->hasFile('id') && ! is_null($request->file('id')[$customerId]))
                {
                    $path = ($request->file('id')[$customerId])->store('agreements');

                    CustomerFileUpload::create([
                        'customer_id' => $customerId,
                        'file_type' => 'ID Front',
                        'file_url' => $path
                    ]);
                }

            }
            return true;
        }catch (\Exception $e)
        {
            return $e;
        }

    }

    public function startProcessing()
    {

        $this->titleProcess->start_processing_at =  now();

        $this->titleProcess->save();



        // surveyor collects document for processing
    }
    public function receiveTitle()
    {
        $this->titleProcess->received_at =  now();

        $this->titleProcess->save();

    }

    public function returnToProcessing()
    {
        //had issue
        $this->titleProcess->has_issue_at =  now();
        $this->titleProcess->save();

        //set has issue
        $this->startProcessing();
        // surveyor returns for processing
    }

    public function clientCollectTitle()
    {
        $this->titleProcess->client_collect_at =  now();
        $this->titleProcess->save();
    }

}
