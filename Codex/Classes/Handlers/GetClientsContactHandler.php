<?php
/**
 * Created by Philip Njuguna.
 * User: Philip Njuguna
 * Date: 11/19/18
 * Time: 3:30 PM
 */

namespace Codex\Classes\Handlers;


use phpDocumentor\Reflection\Types\Boolean;

class GetClientsContactHandler
{




    public function setAttribute($name,$value)
    {
        $this->$name = $value;

        return $this;
    }


    /**
     * @return mixed
     */
    public function getPhone($phone)
    {
        return $this->setAttribute('phone' , $phone);
    }

    /**
     * @return mixed
     */
    public function getFullName($name)
    {
        return $this->setAttribute('name' , $name);
    }

    public function getHasSiteVisit( $boolean)
    {
        return $this->setAttribute('hasVisited' , $boolean);
    }


    public function getIsCustomer( $boolean)
    {
        return $this->setAttribute('isCustomer' , $boolean);
    }

    public function getEditPath( $uri)
    {
        return $this->setAttribute('editPath' ,  $uri);
    }

    public function getprojects( $projects)
    {
        return $this->setAttribute('projects' ,  $projects);
    }
    public function getMeta($key , $value)
    {
        return $this->setAttribute($key ,  $value);
    }


    public function toArray()
    {
        return get_object_vars($this);
    }
}
