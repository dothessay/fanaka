<?php
namespace Codex\Classes\Handlers;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;

class Table {

    private $limit;
    private  $offset;
    private  $query;
    private  $builder;
    private  $filterResult;
    private  $search;
    private  $output;

    private  $count;



    public static function build()
    {
        return new self();
    }

    /**
     * @param mixed $limit
     * @return Table
     */
    public function setLimit($limit)
    {
        $this->limit = $limit;

        return $this;
    }

    /**
     * @param mixed $offset
     * @return Table
     */
    public function setOffset($offset)
    {
        $this->offset = floatval( $offset);

        return $this;
    }

    /**
     * @param Builder $builder
     * @return Table
     */
    public function setBuilder(Builder $builder)
    {
        $this->builder = $builder;

        return $this;
    }

    public function output()
    {
        if ( $this->search instanceof Builder)
        {
            $this->output =  $this->search;
        }

        $outPut = $this->output
            ->get()
            ->map
            ->modelToArray()
            ->filter();

        $this->output = $outPut->count();

        return $outPut;

    }
    public function process()
    {
        $output =  $this->builder
            ->where($this->query)
            ->offset($this->offset)
            ->limit($this->limit);


        $this->output = $output;

        return $this;
    }

    /**
     * @param mixed $filterResult
     * @return Table
     */
    public function setFilterResult($filterResult)
    {
        $this->filterResult = $filterResult;

        return $this;
    }

    public function dateBetween(Carbon $startDate , Carbon $endDate)
    {
         $this->builder = $this->builder->whereBetween('created_at', [
            $startDate , $endDate
        ]);

         return $this;
    }

    public function search($value , array $columns)
    {
        if (! is_null($value)){

            foreach ($columns as $index => $column)
            {
                $search = $this->output->orWhere($column , 'LIKE', '%'.$value.'%');
                $this->search = $search;
           }
            return $this;

        }
        $this->search = $this->output;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCount()
    {
        return $this->count;
    }

}
