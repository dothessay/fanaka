<?php
/**
 * Created by Philip Njuguna.
 * User: Philip Njuguna
 * Date: 11/12/18
 * Time: 9:07 AM
 */

namespace Codex\Classes\Handlers;


use Carbon\Carbon;
use Codex\Contracts\ShouldSendInnoxNotification;
use Illuminate\Support\Facades\Mail;

class SendInnoxNotification
{
    public static function build()
    {
        return new self();


    }


    public function sendSms(ShouldSendInnoxNotification $sendSms , array  $data)
    {


        try{
            return $sendSms->sendNotification($data);
        }catch (\Exception $exception){
            Mail::raw($exception->getMessage(), function ($mail) {
                $mail->to("philipnjuguna66@gmail.com")
                    ->subject('fanaka error');
            });
        }
    }

}
