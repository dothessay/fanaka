<?php


namespace Codex\Classes\Handlers\Facade;

use Illuminate\Support\Facades\Facade;

/**
 * @method static  guessHasPrePaid();
 * @method static  hasPaidRequiredPercentage();
 * @method static  getRequiredPercentage();
 * @method static  getMonthlyPlanAmount();
 * @method static  getRemainingMonths();
 * @method static  getPassedMonths();
 * @method static  exceptedAmountToBePaidByNow();
 * @method static  getDifferencePaidAmount();
 * @method static  getPrePaidAmount();
*/

class PrepaidPlans extends Facade
{



    /**
     * @return string
     */
    protected static function getFacadeAccessor()
    {

        return 'Codex\Classes\Reports\GuessPrepaid';
    }
}
