<?php
/**
 * Created by PhpStorm.
 * User: philip
 * Date: 2/28/19
 * Time: 10:53 AM
 */

namespace Codex\Classes\Handlers\Facade;


use Illuminate\Support\Facades\Facade;

class DashBoardAnalytics extends Facade
{

    /**
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'DashBoardAnalytics';
    }

}