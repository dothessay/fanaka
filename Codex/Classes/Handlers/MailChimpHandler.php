<?php


namespace Codex\Classes\Handlers;


use MailchimpAPI\Mailchimp;

class MailChimpHandler
{
    protected $mailChimp;
    protected $list;

    public function __construct()
    {
        $this->mailChimp = new Mailchimp(env('MAILCHIMP_KEY'));
        $this->list =  env('MAILCHIMP_LIST');
    }

    public function getMember($email)
    {
       return $this->mailChimp->lists($this->list)->members($email)->get();
    }

    public function store(array $data)
    {
       return  $this->mailChimp->lists($this->list)->members()->post([
            'email_address' => $data['email'],
            'status'=>'subscribed',
            'merge_fields'  => [
                'First NAME'  => $data['name'],
                'Phone Number'   => $data['tel']
            ]

        ]);
    }

    public function fullList()
    {
        return $this->mailChimp->lists($this->list)->members();

    }
}
