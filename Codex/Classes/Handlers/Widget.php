<?php
/**
 * Created by IntelliJ IDEA.
 * User: philip
 * Date: 3/26/19
 * Time: 2:26 PM
 */

namespace Codex\Classes\Handlers;


class Widget
{
    private  $title ;
    private  $uri ;
    private  $value ;


    public static function build()
    {
        return new self();
    }


    public function output()
    {
        ob_start();
        ?>
        <div class="col-lg-4 col-md-6">
            <div class="widget widget-stats bg-blue-lighter">
                <div class="stats-icon stats-icon-lg"><i class="fa fa-money fa-fw"></i></div>
                <div class="stats-content">
                    <div class="stats-title"><h6><?= $this->title?></h6></div>
                    <div class="stats-number"><?= $this->value ?></div>
                    <div class="stats-progress progress">
                        <div class="progress-bar" style="width: 70.1%;"></div>
                    </div>

                    <div class="stats-link">
                        <a href="<?= $this->uri ?>">View Detail <i class="fa fa-arrow-alt-circle-right"></i></a>
                    </div>
                </div>
            </div>
        </div>

        <?php

        return ob_get_clean();

    }

    public function title($title)
    {



        $this->title = $title;

        return $this;

    } public function value($value)
    {


        $this->value = $value;


        return $this;

    }
    public function uri($url)
    {
        $this->uri = $url;


        return $this;


    }
}