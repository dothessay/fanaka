<?php

namespace Codex\Classes\Handlers;


use App\User;

class ImapHandler
{

    public $user;
    public $mailBoxPath;
    public $password;
    public $userName;

    public $folder;


    public $mimeType = false;


    public function __construct($user = null)
    {
        $this->user = $user;

        $this->mailBoxPath = "mail.fanaka.co.ke:143/imap/notls/novalidate-cert";

        $this->userName = $this->user->email;
        $this->password = json_decode(base64_decode($this->user->password));


        $this->mimeType = true;


        // $folders = imap_list($this->connect(), "{" . $this->mailBoxPath . "}", "INBOX");


        //$this->folder = $folders;


    }

    public function inbox($count = 5)
    {

        try {
            $imap = $this->connect();


            $folders = imap_list($imap, "{" . $this->mailBoxPath . "}", "INBOX");

            $emails = [];
            foreach ($folders as $folder) {

                $folder = str_replace("{" . $this->mailBoxPath . "}", "", imap_utf7_decode($folder));

                $emailDetails = $this->listMailHeaders($imap, $folder, $count);

                $emails[] = [
                    'folders' => $folder,
                    'emailDetails' => $emailDetails

                ];

            }

            return $emails;

        } catch (\Exception $e) {
            return ($e->getMessage());
        }

    }

    public function connect()
    {

        //return imap_open("{" . $this->mailBoxPath . "}/INBOX", $this->userName, $this->password);
        //dd($this->mailBoxPath , $this->userName , $this->password);

        //return imap_open ("{$this->mailBoxPath}",  $this->userName, $this->password);
        try {
            if (!$this->mimeType) {
                return imap_open("{" . $this->mailBoxPath . '/authuser=' . $this->userName . "}", $this->userName, $this->password);
            }

            return imap_open("{" . $this->mailBoxPath . "}", $this->userName, $this->password);
        } catch (\Exception $exception) {
            throw new \Exception($exception->getMessage());
        }

    }

    private function listMailHeaders($imap, $folder, $mailCounter = 50000)
    {

        try {
            $emailDetails = [];

            //$emails = $this->search('no-reply@accounts.google.com', 'FROM', $mailCounter);
            $emails = imap_num_msg ( $this->connect() );

            $sort = imap_sort($this->connect(),SORTDATE,1);




            for ($i = 1 ; $i <= $emails ; $i++) {

                $email = $i;


                $header = imap_header($imap, $i);

                $fromInfo = $header->from[0];


                $replyInfo = isset($header->reply_to[0]) ? $header->reply_to[0] : isset($header->to[0]) ? $header->to[0] : "";


                $details = array(
                    "fromAddr" => (isset($fromInfo->mailbox) && isset($fromInfo->host)) ? $fromInfo->mailbox . "@" . $fromInfo->host : "",
                    "fromName" => (isset($fromInfo->personal))
                        ? $fromInfo->personal : "",
                    "replyAddr" => (isset($replyInfo->mailbox) && isset($replyInfo->host))
                        ? $replyInfo->mailbox . "@" . $replyInfo->host : "",
                    "replyName" => (isset($replyInfo->personal)) ? $replyInfo->personal : "",
                    "subject" => (isset($header->subject))
                        ? $header->subject : "",
                    "udate" => (isset($header->udate)) ? \Carbon\Carbon::createFromTimestamp($header->udate)->format('Y-M-d') : "",
                    "read" => (($header->Unseen === 'U')) ? "font-weight: bold; color:black;background:rgba(255,255,255,0.6)" : "color:black;",
                    'is_read' => (($header->Unseen === 'U')) ? true : false,
                    'body' => $this->getBody($email, "TEXT/PLAIN"),
                    'is_attached' => $this->hasAttachment($i)//false
                );

                $uid = imap_uid($imap, $email);

                //$num = imap_num_msg($imap);


                $emailDetails[] = [
                    'is_read' => $details['is_read'],
                    'uid' => $email,
                    'body' => $details['body'],
                    'date' => $details['udate'],
                    'from' => $details["fromAddr"],
                    'subject' => $details["subject"],
                    'getColorStyle' => $details['read'],
                    'is_attached' => $details['is_attached'],
                    'nextEmail' => 10
                ];


            }

             rsort($emailDetails);

            $toReturn = [];

            foreach ($emailDetails as $key => $val) {


               $toReturn[] = $val;
            }

            return $toReturn;


        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }


    function getPart($imap, $uid, $mimetype, $structure = false, $partNumber = false)
    {
        if (!$structure) {
            $structure = imap_fetchstructure($imap, $uid);
        }
        if ($structure) {
            if ($mimetype == $this->getMimeType($structure)) {
                if (!$partNumber) {
                    $partNumber = 1;
                }
                $text = imap_fetchbody($imap, $uid, $partNumber);
                switch ($structure->encoding) {
                    case 3:
                        return imap_base64($text);
                    case 4:
                        return imap_qprint($text);
                    default:
                        return $text;
                }
            }


            $file = $this->downloadAttachment($uid);


            // multipart
            if ($structure->type == 1) {
                foreach ($structure->parts as $index => $subStruct) {
                    $prefix = "";
                    if ($partNumber) {
                        $prefix = $partNumber . ".";
                    }
                    $data = $this->getPart($imap, $uid, $mimetype, $subStruct, $prefix . ($index + 1));
                    if ($data) {
                        return $data;
                    }
                }
            }
        }
        return false;
    }

    function hasAttachments($msgno)
    {
        $struct = imap_fetchstructure($this->connect(), $msgno, FT_UID);
        $existAttachments = $this->existAttachment($struct);

        return $existAttachments;
    }


    public function hasAttachment($uid)
    {
        $files = '';

        $inbox = $this->connect();


        /* get mail structure */
        $structure = imap_fetchstructure($inbox, $uid);

        $attachments = array();


        /* if any attachments found... */
        if (isset($structure->parts) && count($structure->parts)) {
            for ($i = 0; $i < count($structure->parts); $i++) {
                $attachments[$i] = array(
                    'is_attachment' => false,
                    'filename' => '',
                    'name' => '',
                    'attachment' => ''
                );

                if ($structure->parts[$i]->ifdparameters) {
                    foreach ($structure->parts[$i]->dparameters as $object) {
                        if (strtolower($object->attribute) == 'filename') {
                            $attachments[$i]['is_attachment'] = true;
                            $attachments[$i]['filename'] = $object->value;
                        }
                    }
                }

                if ($structure->parts[$i]->ifparameters) {
                    foreach ($structure->parts[$i]->parameters as $object) {
                        if (strtolower($object->attribute) == 'name') {
                            $attachments[$i]['is_attachment'] = true;
                            $attachments[$i]['name'] = $object->value;
                        }
                    }
                }

                if ($attachments[$i]['is_attachment']) {
                    $attachments[$i]['attachment'] = imap_fetchbody($inbox, $uid, $i + 1);

                    /* 4 = QUOTED-PRINTABLE encoding */
                    if ($structure->parts[$i]->encoding == 3) {
                        $attachments[$i]['attachment'] = base64_decode($attachments[$i]['attachment']);
                    } /* 3 = BASE64 encoding */
                    elseif ($structure->parts[$i]->encoding == 4) {
                        $attachments[$i]['attachment'] = quoted_printable_decode($attachments[$i]['attachment']);
                    }
                }
            }
        }

        /* iterate through each attachment and save it */
        foreach ($attachments as $attachment) {
            if ($attachment['is_attachment'] == 1) {
                $filename = $attachment['name'];
                if (empty($filename)) $filename = $attachment['filename'];

                if (empty($filename)) $filename = time() . ".pdf";

                /* prefix the email number to the filename in case two emails
                 * have the attachment with the same file name.
                 */

                $files = $uid . "-" . $filename;

                $fp = fopen($uid . "-" . $filename, "w+");
                fwrite($fp, $attachment['attachment']);
                fclose($fp);


                return true;
            }

        }

        return false;
    }

    public function getBody($uid, $mimeType = false)
    {
        if (!$mimeType) {


            return imap_body($this->connect(), $uid);

        }
        $body = $this->getPart($this->connect(), $uid, "TEXT/HTML");
        // if HTML body is empty, try getting text body
        if ($body == "") {
            $body = $this->getPart($this->connect(), $uid, "TEXT/PLAIN");
        }
        return $body;
    }


    public function getFromAddress($uid)
    {
        $header = imap_header($this->connect(), $uid);

        $fromInfo = $header->from[0];
        $replyInfo = isset($header->reply_to[0]) ? $header->reply_to[0] : isset($header->to[0]) ? $header->to[0] : "";

        return (isset($fromInfo->mailbox) && isset($fromInfo->host)) ? $fromInfo->mailbox . "@" . $fromInfo->host : "";
    }


    public function getThread()
    {
        return imap_thread($this->connect());
    }


    private function search($search = 'no-reply@accounts.google.com', $criteria = 'FROM', $counter = 5)
    {
        $myFilters = [];

        $counter = 100000;


        try {
            $search = "no-reply@accounts.google.com";

            $search = "";

            $criteria = $criteria . ' "' . $search . '"';


           // $filtered = imap_sort($this->connect(), SORTDATE, 1, 0, $criteria);


            // dd('is null ', is_null($filtered));

            $countLimit = $counter;


            $maximun = $countLimit;

            $minimum = $counter - 5;


            $totalMsgCount = imap_num_msg($this->connect());


            for ($i = $totalMsgCount; ($i > (($totalMsgCount) - $maximun) && $i > 0); $i--) {

                $filtered[] = $i;

            }


            for ($start = $minimum; $start <= $maximun; $start++) {
                $myFilters[] = $filtered[$start];
            }


        } catch (\Exception $e) {

        }


        return $myFilters;

    }


    function getMimeType(&$structure)
    {
        $primary_mime_type = array("TEXT", "MULTIPART", "MESSAGE", "APPLICATION", "AUDIO", "IMAGE", "VIDEO", "OTHER");
        if ($structure->subtype) {
            return $primary_mime_type[(int)$structure->type] . '/' . $structure->subtype;
        }
        return "TEXT/PLAIN";
    }


    public function downloadAttachment($uid)
    {

        $files = '';

        $inbox = $this->connect();


        $overview = imap_fetch_overview($inbox, $uid, 0);

        /* get mail message */
        $message = imap_fetchbody($inbox, $uid, 2);

        /* get mail structure */
        $structure = imap_fetchstructure($inbox, $uid);

        $attachments = array();


        /* if any attachments found... */
        if (isset($structure->parts) && count($structure->parts)) {
            for ($i = 0; $i < count($structure->parts); $i++) {
                $attachments[$i] = array(
                    'is_attachment' => false,
                    'filename' => '',
                    'name' => '',
                    'attachment' => ''
                );

                if ($structure->parts[$i]->ifdparameters) {
                    foreach ($structure->parts[$i]->dparameters as $object) {
                        if (strtolower($object->attribute) == 'filename') {
                            $attachments[$i]['is_attachment'] = true;
                            $attachments[$i]['filename'] = $object->value;
                        }
                    }
                }

                if ($structure->parts[$i]->ifparameters) {
                    foreach ($structure->parts[$i]->parameters as $object) {
                        if (strtolower($object->attribute) == 'name') {
                            $attachments[$i]['is_attachment'] = true;
                            $attachments[$i]['name'] = $object->value;
                        }
                    }
                }

                if ($attachments[$i]['is_attachment']) {
                    $attachments[$i]['attachment'] = imap_fetchbody($inbox, $uid, $i + 1);

                    /* 4 = QUOTED-PRINTABLE encoding */
                    if ($structure->parts[$i]->encoding == 3) {
                        $attachments[$i]['attachment'] = base64_decode($attachments[$i]['attachment']);
                    } /* 3 = BASE64 encoding */
                    elseif ($structure->parts[$i]->encoding == 4) {
                        $attachments[$i]['attachment'] = quoted_printable_decode($attachments[$i]['attachment']);
                    }
                }
            }
        }

        /* iterate through each attachment and save it */
        foreach ($attachments as $attachment) {
            if ($attachment['is_attachment'] == 1) {
                $filename = $attachment['name'];
                if (empty($filename)) $filename = $attachment['filename'];

                if (empty($filename)) $filename = time() . ".pdf";

                /* prefix the email number to the filename in case two emails
                 * have the attachment with the same file name.
                 */

                $files = $uid . "-" . $filename;

                $fp = fopen($uid . "-" . $filename, "w+");
                fwrite($fp, $attachment['attachment']);
                fclose($fp);

                //response()->download($uid . "-" . $filename);
            }

        }


        return $files;


    }


}