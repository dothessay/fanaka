<?php
/**
 * Created by IntelliJ IDEA.
 * User: philip
 * Date: 4/17/19
 * Time: 2:03 PM
 */

namespace Codex\Classes\Handlers;


use Codex\Contracts\ShouldSendInnoxNotification;

class OneSignalHandler implements ShouldSendInnoxNotification
{

    private $appId;

    private $restKey;


    public function __construct()
    {
        $this->appId = "0492c9ca-fd1b-45af-af1d-343e1dc7688c";
        $this->restKey = "OTgwOTI4ZjYtYTdiMS00ODUzLWE2ZDYtMTg1MjU1M2FlY2Nl";

    }

    public static function build()
    {
        return new self();

    }

    public function sendMessage($message , $customerId)
    {
        $content = array(
            "en" => $message
        );


        $fields = array(
            'app_id' => $this->appId,
            'filters' => array(array("field" => "tag", "key" => "customerId", "relation" => "=", "value" => $customerId)),
            'data' => ['message' => $message],
            'contents' => $content,
            'headings' => ['en' => 'Fanaka Updates'],
            'title' => 'Fanaka'
        );

        $fields = json_encode($fields);

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "https://onesignal.com/api/v1/notifications");
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json; charset=utf-8',
            'Authorization: Basic ' . $this->restKey));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_HEADER, FALSE);
        curl_setopt($ch, CURLOPT_POST, TRUE);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);

        $response = curl_exec($ch);
        curl_close($ch);

        return $response;
    }

    public function getDevices()
    {

        $app_id = $this->appId;
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "https://onesignal.com/api/v1/players?app_id=" . $app_id);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json',
            'Authorization: Basic '.$this->restKey));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_HEADER, FALSE);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        $response = curl_exec($ch);
        curl_close($ch);
        return json_decode($response , 2);

    }


    public function sendNotification(array $data)
    {
        return $this->sendMessage($data['message'],$data['customerId']);
    }
}
