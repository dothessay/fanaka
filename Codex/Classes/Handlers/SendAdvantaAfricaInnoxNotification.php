<?php
/**
 * Created by Philip Njuguna.
 * User: Philip Njuguna
 * Date: 11/12/18
 * Time: 12:33 PM
 */

namespace Codex\Classes\Handlers;


use Codex\Contracts\ShouldSendInnoxNotification;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Mail;

class SendAdvantaAfricaInnoxNotification implements ShouldSendInnoxNotification
{

    public function sendNotification(array $data)
    {
        try {

            $url = 'https://quicksms.advantasms.com/api/services/sendsms/';
            $curl = curl_init();
            curl_setopt($curl, CURLOPT_URL, $url);
            curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-Type:application/json')); //setting custom header
            $curl_post_data = array(
                //Fill in the request parameters with valid values
                'partnerID' => '909',
                'apikey' => '8910d124f9872750b99dbf8c9b98cb5e',
                'mobile' => "{$data['to']}",
                'message' => "{$data['message']}",
                'shortcode' => 'FANAKA_LTD',
                'pass_type' => 'plain', //bm5 {base64 encode} or plain
            );

            $data_string = json_encode($curl_post_data);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($curl, CURLOPT_POST, true);
            curl_setopt($curl, CURLOPT_POSTFIELDS, $data_string);
            $curl_response = curl_exec($curl);

            return $curl_response;
        } catch (\Exception $exception) {

            Mail::raw($exception->getMessage(), function ($mail) {
                $mail->to('philipnjuguna66@gmail.com')->subject("error advanta sms");
            });

        }
        
        
    }
    public function sendNotificationOld(array $data)
    {
        try {

            $username = fanakaConfig('advanta_username');
            $password = fanakaConfig('advanta_password');
            $senderId = fanakaConfig('advanta_sender_id');
            if (isset($username) && isset($password) && isset($senderId)) {

                $message = ltrim($data['message']);
                $xml_data = "<?xml version='1.0'?><smslist><sms><user>{$username}</user><password>{$password}</password><message><![CDATA[{$message}]]></message><mobiles>{$data['to']}</mobiles><senderid>{$senderId}</senderid><clientsmsid>1</clientsmsid></sms></smslist>";

                $URL = "http://messaging.advantasms.com/bulksms/sendsms.jsp?";

                $ch = curl_init($URL);
                curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
                curl_setopt($ch, CURLOPT_POST, 1);
                curl_setopt($ch, CURLOPT_ENCODING, 'UTF-8');
                curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/xml'));
                curl_setopt($ch, CURLOPT_POSTFIELDS, "$xml_data");
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                $output = curl_exec($ch);
                curl_close($ch);

                Mail::raw(json_encode($output), function ($mail) {

                    $mail->to('philipnjuguna66@gmail.com')->subject('Success message');
                });

                return json_decode($output, true);
            }
        } catch (\Exception $exception) {

            Mail::raw($exception->getMessage(), function ($mail) {
                $mail->to('philipnjuguna66@gmail.com')->subject("error advanta sms");
            });

        }


    }

    public function newPlatform(array  $data)
    {

        $partnerID = "909";
        $apikey = "8910d124f9872750b99dbf8c9b98cb5e";
        $shortcode = "fanakaltd";

        $mobile = $data['to']; // Bulk messages can be comma separated
        $message =  $data['message'];

        $finalURL = "https://quicksms.advantasms.com/api/services/sendsms/?apikey=" . urlencode($apikey) . "&partnerID=" . urlencode($partnerID) . "&message=" . urlencode($message) . "&shortcode=$shortcode&mobile=$mobile";
        $ch = curl_init();
        \curl_setopt($ch, CURLOPT_URL, $finalURL);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        $response = curl_exec($ch);
        curl_close($ch);
        echo "Response: $response";
        
    }

}


