<?php
/**
 * Created by IntelliJ IDEA.
 * User: philip
 * Date: 5/4/19
 * Time: 7:07 PM
 */

namespace Codex\Classes\Handlers;


use App\DB\Log\Log;

trait FanakaLog
{


    public function log($data)
    {
        //ALTER TABLE `logs` ADD `user_id` INT(191) NOT NULL AFTER `id`, ADD `service` VARCHAR(191) NOT NULL AFTER `user_id`, ADD `data` TEXT NOT NULL AFTER `service`;

        //ALTER TABLE `users` ADD `checked_in_at` TIMESTAMP NULL DEFAULT NULL AFTER `is_active`, ADD `checked_out_at` TIMESTAMP NULL DEFAULT NULL AFTER `checked_in_at`;

        if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
            $ip = $_SERVER['HTTP_CLIENT_IP'];
        } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
            $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
        } else {
            $ip = $_SERVER['REMOTE_ADDR'];
        }


        if (auth()->user())
        {
            Log::create([
                'user_id'           => auth()->id(),
                'service'           => $data['class'],
                'data'              => $data['action'],
                'ip'                => $ip,
                'machine'           => $_SERVER['HTTP_USER_AGENT'],
                'forwarded_ip'      => isset($_SERVER['HTTP_X_FORWARDED_FOR']) ? $_SERVER['HTTP_X_FORWARDED_FOR'] : $ip
             ]);
        }

    }

}
