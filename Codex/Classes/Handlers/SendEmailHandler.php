<?php
/**
 * Created by PhpStorm.
 * User: philip
 * Date: 12/18/18
 * Time: 12:21 PM
 */

namespace Codex\Classes\Handlers;


use Codex\Contracts\ShouldSendInnoxNotification;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Support\Facades\Mail;

class SendEmailHandler implements ShouldSendInnoxNotification
{


    public function sendNotification(array $data)
    {

        Mail::raw($data['message'] , function ($mail) use($data) {

           $mail->to($data['to'])
               ->subject($data['subject']);

       });
    }
}