<?php


namespace Codex\Classes;


class TransformToObject
{
    protected $name;

    public function setAttribute($name,$value)
    {
        $this->$name = $value;

        return $this;
    }

    public function attribute($attribute , $value)
    {
        return $this->setAttribute($attribute ,$value);
    }

    public function toArray()
    {
        return get_object_vars($this);
    }
}