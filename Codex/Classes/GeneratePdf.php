<?php
/**
 * Created by PhpStorm.
 * User: philip
 * Date: 2/28/18
 * Time: 7:10 PM
 */

namespace Codex\Classes;


use Carbon\Carbon;
use Codex\Contracts\PdfOutput;
use Codex\Contracts\ReportOutput;
use Illuminate\Support\Collection;
use Mpdf\Mpdf;

class GeneratePdf
{
    public function generate(PdfOutput $output, Collection $collection)
    {
        return $output->outPut($collection);

    }
    public function pdfDocument(ReportOutput $output, $html)
    {
        return $output->outPut($html);

    }
}