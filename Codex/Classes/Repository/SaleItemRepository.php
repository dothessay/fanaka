<?php 


namespace Codex\Classes\Repository;
use App\Plot\Plot;
use App\Sale\Sale;
use App\Sale\SaleItem;
use Codex\Classes\Filters\IdFilter;
use Codex\Classes\Filters\ProductFilter;
use Illuminate\Http\Request;
use Illuminate\Pipeline\Pipeline;

Class SaleItemRepository
{

    private  $sale;

    /**
     *param Request $request
     * @param Request $request
     * @return
     */
	public function all(Request $request)
	{

        return app(Pipeline::class)
            ->send(SaleItem::query())
            ->through([
                IdFilter::class,
                ProductFilter::class
            ])
            ->thenReturn()
            ->orderBy('product_id')
            ->get();
	// TODO: Implement all() method.
	}

	public function store(Request $request)
	{
        if (is_array($request['plot_id'])) {

            for ($i =0 ; $i < sizeof($request['plot_id']) ; $i++)
            {
                $plot = Plot::find($request['plot_id'][$i]);


                $this->sale->saleItems()->create([
                    'business_code' => $this->sale->business->code,
                    'plot_id' => $request['plot_id'][$i],
                    'price' => $request['total'],
                    'plot_no' => $plot->plot_no
                ]);

                // update plot to taken

                $plot->is_sold = true;
                $plot->save();

            }
        }
        else{

            $plot = Plot::find($request['plot_id']);


            $this->sale->saleItems()->create([
                'business_code' => $this->sale->business->code,
                'plot_id' => $request['plot_id'],
                'price' => $request['total'],
                'plot_no' => $plot->plot_no
            ]);

            // update plot to taken

            $plot->is_sold = true;
            $plot->save();
        }

	}
	public function update(Request $request)
	{

	// TODO: Implement update() method.
	}
	public function delete(Request $request)
	{

	// TODO: Implement delete() method.
	}

    /**
     * @param mixed $sale
     * @return SaleItemRepository
     */
    public function setSale(Sale $sale)
    {
        $this->sale = $sale;

        return $this;
    }
} 
