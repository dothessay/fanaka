<?php 


namespace Codex\Classes\Repository;
use App\Sale\Sale;
use Illuminate\Http\Request;

Class CustomerSaleRepository
{

    private  $sale;

    /**
     * @param Request $request
     */
	public function all(Request $request)
	{

	// TODO: Implement all() method.
	}

	public function store(Request $request)
	{
	    // check if sale has this user

        if (is_array($request['customer_id']))
        {
            if (! $this->sale->customerSales()->whereIn('customer_id', $request['customer_id'])->count())
            {
                foreach ($request['customer_id'] as $customerId) {

                    $this->sale->customerSales()->create([
                        'customer_id'  => $customerId
                    ]);

               }
            }
        }
        else{
            $this->sale->customerSales()->create([
                'customer_id'  => $request['customer_id']
            ]);
        }

	// TODO: Implement store() method.
	}
	public function update(Request $request)
	{

	// TODO: Implement update() method.
	}
	public function delete(Request $request)
	{

	// TODO: Implement delete() method.
	}

    /**
     * @param mixed $sale
     * @return CustomerSaleRepository
     */
    public function setSale(Sale $sale)
    {
        $this->sale = $sale;
        return $this;
    }

    public function handle($request , \Closure $next)
    {
        $this->store(\request());
        return $next($request);
    }

} 
