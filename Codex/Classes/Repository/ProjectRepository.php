<?php
namespace Codex\Classes\Repository;
use App\DB\Product\Product;
use Codex\Classes\Filters\IdFilter;
use Illuminate\Pipeline\Pipeline;

class ProjectRepository
{
    public function all()
    {
        return app(Pipeline::class)
            ->send(Product::query())
            ->through([
                IdFilter::class,
            ])
            ->thenReturn()
            ->get();
    }

}
