<?php


namespace Codex\Classes\Repository;


use App\DB\Exepense\PettyCash;
use App\DB\Exepense\PettyCashLog;
use App\DB\Expense\Expense;
use App\DB\Finance\ChartOfAccount;
use App\Traits\IsJournable;
use Codex\Classes\Filters\DateBetweenFilter;
use Codex\Classes\Filters\IdFilter;
use Illuminate\Http\Request;
use Illuminate\Pipeline\Pipeline;

class ExpensesRepository
{
    use IsJournable;

    public function all()
    {
        return app(Pipeline::class)
            ->send(Expense::query())
            ->through([
                IdFilter::class,
                DateBetweenFilter::class
            ])
            ->thenReturn()
            ->orderBy('product_code','ASC')
            ->get();
    }

    public function store(Request $request)
    {

        $expense =  Expense::create([
            'business_code' => $request->user()->business->code,
            'amount' => $request['amount'],
            'purpose' => $request['purpose'],
            'user_id' => auth()->id(),
            'chart_id' => $request['chart']
        ]);


        $journal = $this->addToJournal([
            'chart_id' => $request['chart'],
            'debit'    => $request['amount'],
            'credit'   => 0
        ]);


        $expense->addjournalable($journal);

        $chart = ChartOfAccount::where('code', '8410/000')->first();

        if (! is_null($chart))
        {
            $journal = $this->addToJournal([
                'chart_id' => $chart->id,
                'debit'    => 0,
                'credit'   => $request['amount']
            ]);

            $expense->addjournalable($journal);
        }

        $log = PettyCashLog::create([
            'user_id'  => auth()->id(),
            'debit'   => $request['amount'],
            'note'     => $request['purpose'],
            'balance'   => PettyCash::first()->amount,

        ]);

        $expense->addLoggable($log);

    }

}
