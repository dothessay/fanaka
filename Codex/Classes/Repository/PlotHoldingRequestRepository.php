<?php

namespace Codex\Classes\Repository;

use App\DB\Business\Meta;
use App\DB\Plot\PlotHoldingRequest;
use App\Plot\Plot;
use Carbon\Carbon;
use Codex\Classes\Filters\IdFilter;
use Codex\Classes\Filters\IsApprovedFilter;
use Codex\Classes\Filters\UserFilter;
use Codex\Classes\Handlers\SystemSms;
use Illuminate\Http\Request;
use Illuminate\Pipeline\Pipeline;

class PlotHoldingRequestRepository
{
    public function all()
    {
        $requests =  app(Pipeline::class)
            ->send(PlotHoldingRequest::query())
            ->through([
                IdFilter::class,
                UserFilter::class,
                IsApprovedFilter::class
            ])
            ->thenReturn()
            ->orderBy('user_id')
            ->get();

        if (\request()->user()->departmentAdmin)
        {

            $userIds = \request()->user()->departmentAdmin->role->users->pluck('id')->toArray();

            return $requests->whereIn('user_id', $userIds);

        }

        $plots = collect();

        foreach ($requests as $request) {

            if ((! $request->plot->is_sold || ! $request->plot->is_reserved)  && Carbon::parse($request->release_at)->endOfDay()->greaterThan(now()))
            {
                $plots->push($request);
            }

        }
        return $plots;

    }

    public function store(Request $request)
    {
        if (Carbon::parse($request['release_at'])->startOfDay()->greaterThan(Carbon::parse($request['release_at'])->addRealDays(3)->endOfDay()))
        {
            return back()
                ->withErrors([
                    'error'  => 'Release date cannot be more than 3 days'
                ])
                ->withInput()
                ->setStatusCode(406)
                ->send();
        }

        // validate only if request i

        if(PlotHoldingRequest::where([
            'plot_id'   => $request['plot_id'],
            'is_approved'  => false,


        ])->where('release_at' ,'>', now()->endOfDay())->count() )
        {
            return back()
                ->withErrors([
                    'error'  => 'Plot request Already sent by another agent'
                ])
                ->withInput()
                ->setStatusCode(406)
                ->send();

        }
        $plot = Plot::findOrFail($request['plot_id']);

        if ($plot->product->availablePlots() <= 5)
        {
            return back()
                ->withErrors([
                    'error'  => 'Project has less than 5 plots remaining , note yo cannot hold'
                ])
                ->withInput()
                ->setStatusCode(406)
                ->send();
        }



        $holdRequest  = PlotHoldingRequest::create([
            'plot_id'        => $request['plot_id'],
            'user_id'        => $request->user()->id,
            'customer_name'  => $request['customer_name'],
            'phone_number'   => $request['phone_number'],
            'release_at'     => Carbon::parse( $request['release_at']),

        ]);

        // Notify Dennis to approve the hold request

        SystemSms::sendPlotHoldingRequest($holdRequest);

        return $holdRequest;


    }

    public function update(Request $request , PlotHoldingRequest $holdingRequest)
    {
        if($request->has('reject'))
        {
            $holdingRequest->is_approved = false;
        }
        if ($request->has('approve'))
        {
            $holdingRequest->is_approved = true;
            // hold plot
            $plot = $holdingRequest->plot;
            $plot->can_sell = false;
            $plot->save();
            $this->addMetaToHoldPlot($plot, [
                'reason'   => $holdingRequest->user->fullName() .': '. $holdingRequest->customer_name,
                'date'     => $holdingRequest->release_at
            ]);

            //Notify the customer that plot has been held

            SystemSms::sendPlotHoldingNotification($holdingRequest);

        }
        $holdingRequest->approved_by = $request->user()->id;
        $holdingRequest->approved_at = now();
        $holdingRequest->save();
    }

    public function addMetaToHoldPlot(Plot $plot , array  $data)
    {
        if ($plot->getMeta('reason' , false)){

            $plot->metable()->delete();
        }
        if ($plot->getMeta('date' , false)){

            $plot->metable()->delete();
        }
        $meta = Meta::create([
            'key'  => 'reason',
            'value' => $data['reason']
        ]);
        $plot->saveMeta($meta);
        $meta = Meta::create([
            'key'  => 'date',
            'value' => $data['date']
        ]);
        $plot->saveMeta($meta);

    }
}
