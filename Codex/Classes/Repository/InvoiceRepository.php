<?php 


namespace Codex\Classes\Repository;
use App\Sale\Invoice;
use Carbon\Carbon;
use Codex\Classes\Filters\DateBetweenFilter;
use Codex\Classes\Filters\IdFilter;
use Codex\Classes\Filters\SaleFilter;
use Codex\Services\Plots\ListPlotsService;
use Illuminate\Http\Request;
use Illuminate\Pipeline\Pipeline;

Class InvoiceRepository
{
    private  $sale;

    /**
     *param Request $request
     * @param Request $request
     * @return \Illuminate\Support\Collection
     */
	public function all(Request $request)
	{
	    $invoices =  $this->departmentAdmin($this->filter())->unique('sale_id');

	    if ($request->has('customer_id'))
        {

            $newInvoices = collect();
            foreach ($invoices as $invoice)
            {

                $customerIds = $invoice->sale->customers->pluck('id')->toArray();

                if (in_array($request['customer_id'] , $customerIds) )
                {
                    $newInvoices->push($invoice);
                }


            }
            return $newInvoices;

        }
	    return $invoices;

	}


    private function departmentAdmin($invoices)
    {

        $newInvoices = [];

        $request = \request();

        if ( $request->user()->departmentAdmin)
        {
            $usersIds = $request->user()->departmentAdmin->role->users->pluck('id')->toArray();

            foreach ($invoices as $invoice) {


                if (isset($invoice->sale->id))
                {
                    if ($invoice->sale->getBalance() > 1 && in_array($invoice->sale->agent, $usersIds))
                    {

                        $newInvoices[] = $invoice;
                    }
                }
            }

            return collect($newInvoices);
        }

        return $this->agent($invoices);

	}

	private function agent($invoices)
    {

        $newInvoices = [];

        $request = \request();

        if (! $request->user()->can('access-module-component-functionality', 'sale_manage_show-all-sales') && ! $request->user()->departmentAdmin)
        {


            foreach ($invoices as $invoice) {

                if (isset($invoice->sale->id)) {
                    if ($invoice->sale->getBalance() > 1 && $invoice->sale->agent === $request->user()->id) {

                        $newInvoices[] = $invoice;
                    }
                }
            }

            return collect($newInvoices);

        }

        foreach ($invoices as $invoice) {

            if (isset($invoice->sale->id)) {
                if ($invoice->sale->getBalance() > 1 ) {

                    $newInvoices[] = $invoice;
                }
            }
        }


        return collect($newInvoices);

	}

    private function filter()
    {
        return app(Pipeline::class)
            ->send(Invoice::query())
            ->through([
                IdFilter::class,
                DateBetweenFilter::class,
                SaleFilter::class
            ])
            ->thenReturn()
            ->get();
	}
	public function store(Request $request)
	{
        Invoice::create([
            'business_code' => $this->sale->business_code,
            'user_id' => auth()->id(),
            'sale_id' => $this->sale->id,
           'customer_id' => $request['customer_id'][0],
            'total_amount' => $this->sale->total_amount,
            'total_paid' => $request['amount'],
            'due_date' => Carbon::now()->addDays(30)
        ]);

	}
	public function update(Request $request)
	{

	// TODO: Implement update() method.
	}
	public function delete(Request $request)
	{

	// TODO: Implement delete() method.
	}

    /**
     * @param mixed $sale
     * @return InvoiceRepository
     */
    public function setSale($sale)
    {
        $this->sale = $sale;

        return $this;
    }
} 
