<?php


namespace Codex\Classes\Repository;


use App\Customer\Customer;
use Codex\Classes\Filters\IdFilter;
use Illuminate\Http\Request;
use Illuminate\Pipeline\Pipeline;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

class CustomerRepository
{

    public function all()
    {
        return app(Pipeline::class)
            ->send(Customer::query())
            ->through([
                IdFilter::class
            ])
            ->thenReturn()
            ->get();

    }
    public function paginate()
    {
        return app(Pipeline::class)
            ->send(Customer::query())
            ->through([
                IdFilter::class
            ])
            ->thenReturn()
            ->paginate(10);

    }

    public function store()
    {

        $this->validateCreateCustomer();
        try {
            DB::beginTransaction();
            $request = \request();

            $customerIds = [];

            for ($i = 0; $i < sizeof($request['first_name']); $i++)
            {
                $customerData = [
                    'first_name' => $request['first_name'][$i],
                    'last_name' => $request['last_name'][$i],
                    'email' => $request['email'][$i],
                    'phone_number' => $request['phone_number'][$i],
                    'id_no' => $request['id_no'][$i],
                    'middle_name' => $request['middle_name'][$i],
                    'user_id' => $request->user()->id,
                ];

                $customer = Customer::store($customerData);


                $customerIds[]  = $customer->id;
            }
            $request->request->add(['customer_id' => $customerIds]);


            if (isset($request['new_sale']))
            {
                $sale = (new SalesRepository())->createSale($request);

                DB::commit();

                return redirect(url('sale/manage/edit-agreement/'. $sale->id));
            }

            DB::commit();

            Session::flash('success', "successfully created the user");

            return back();

        }
        catch (\Exception $exception)
        {



            Session::flash('error', $exception->getMessage());

           return back();
        }

    }

    private function validateCreateCustomer()
    {
        $validator = Validator::make(request()->all(), [
            'id_no' => [
                'required',
                'array',
                Rule::unique('customers', 'id_no')
            ],
            'phone_number' => [
                'required',
                'array',
                'nullable',
                Rule::unique('customers', 'phone_number')
            ],
            'email' => [
                'required',
                'nullable',
                'array',
                Rule::unique('customers', 'email')
            ]
        ],
            [
                'id_no.required' => 'ID no is required',
                'id_no.unique' => 'ID no is required',
            ]);

        if ($validator->fails()) {

            return redirect()
                ->back()
                ->withErrors($validator)
                ->withInput();
        }
    }
}
