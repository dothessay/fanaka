<?php


namespace Codex\Classes\Repository;


use App\User;
use Illuminate\Http\Request;

class UserRepository
{
    public function all()
    {
        if (!\request()->user()->can('access-module-component-functionality', 'sale_manage_show-all-sales') && ! isset(request()->user()->departmentAdmin)) {

            $users = User::where('id', \request()->user()->id)->get();
        }

        if ( isset(\request()->user()->departmentAdmin))
        {
            $users = \request()->user()->departmentAdmin->role->users;
        }
        if ( \request()->user()->can('access-module-component-functionality', 'sale_manage_show-all-sales')) {

            $users = User::active()->get();
        }

        return $users;
    }

    public function store(Request $request)
    {
        return User::create([
            'business_code' => \request()->user()->business->code,
            'first_name' => $request['first_name'],
            'last_name' => $request['last_name'],
            'email' => $request['email'],
            'phone_number' => $request['phone_number'],
            'password' => bcrypt($request['password']),
            'role_id' => $request['role_id'],
        ]);

    }

}
