<?php
namespace Codex\Classes\Repository;
use App\Plot\Plot;
use App\Sale\SaleItem;
use Illuminate\Http\Request;

Class ChangeSalePlotRepository
{
    private  $saleItem;

	/**
	*@param Request $request
     */
	public function all(Request $request)
	{

	// TODO: Implement all() method.
	}

	public function store(Request $request)
	{

	// TODO: Implement store() method.
	}
	public function update(Request $request)
	{
	  //  dd($request->all());
	    $saleItem = $this->saleItem;

	    $plotBefore = $this->saleItem->plot;
        $plotBefore->is_sold =  false;
        $plotBefore->is_reserved =  false;
        $plotBefore->can_sell =  true;
        $plotBefore->save();

	    $saleItem->plot_id = $request['plot_id'];
	    $saleItem->price = $request['price'];


	    $saleItem->save();

	    $plot = Plot::find($request['plot_id']);

	    $plot->is_sold = true;
	    $plot->is_reserved = true;
	    $plot->can_sell = false;

	    $plot->save();

	    $saleItem->save();



	}
	public function delete(Request $request)
	{

	// TODO: Implement delete() method.
	}

    /**
     * @param mixed SaleItem $saleItem
     * @return ChangeSalePlotRepository
     */
    public function setSaleItem(SaleItem $saleItem)
    {
        $this->saleItem = $saleItem;
        return $this;
    }
} 
