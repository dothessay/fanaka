<?php


namespace Codex\Classes\Repository;


use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

class MakeSaleHandler
{
    public function handle()
    {
        try {
            $request = request();
            $sale = (new SalesRepository())->createSale($request);

            (new MonthlyInstallmentPlan())->setSale($sale)->store($request);

            // create a customer

            if($request->has('first_name') && ! is_null($request['first_name'][0]))
            {
                (new CustomerRepository())->store();
            }

            // attach sale to a customer
            (new CustomerSaleRepository())->setSale($sale)->store($request);

            (new  SaleItemRepository())->setSale($sale)->store($request);

            // invoice

            (new  InvoiceRepository())->setSale($sale)->store($request);

            // payments
            (new  SalePaymentRepository())->setSale($sale)->store($request);

            return $sale;
        }
        catch (\Exception $exception)
        {
            throw new \Exception($exception);
        }

    }


}
