<?php 


namespace Codex\Classes\Repository;
use App\DB\Accounting\Account;
use Carbon\Carbon;
use Illuminate\Http\Request;

Class SalePaymentRepository
{

    private $sale;

	/**
	*param Request $request*/
	public function all(Request $request)
	{

	// TODO: Implement all() method.
	}

	public function store(Request $request)
	{
	    $paymentType = $request['months'] > 1 ? "instalment" : "Cash";
	    $request->request->add(['payment_type' => $paymentType]);

        $payment = $this->sale->salePayments()->create([
            'payment_type' => $request['payment_type'],
            'payment_method' => $request['payment_method'],
            'amount' => $request['amount'],
            'user_id' => \request()->user()->id,
            'reference_code' => isset($request['reference_code']) ? $request['reference_code'] : $request['payment_method'],
            'bank_name' => isset($request['bank_name']) ? $request['bank_name'] : "",
            'account_no' => isset($data['account_no']) ? $request['account_no'] : "",
            'cheque_no' => isset($request['cheque_no']) ? $request['cheque_no'] : "",
            'drawer' => isset($request['drawer']) ? $request['drawer'] : "",
            'slip_no' => isset($request['slip_no']) ? $request['slip_no'] : "",
            'is_approved' => true,
            'deposit_date' => isset($request['deposit_date']) ? Carbon::parse($request['deposit_date']) : Carbon::now()
        ]);


        /***********CREATE ACCOUNT****************/


        $account = Account::create([

            'user_id' => auth()->id(),
            'amount' => $request['amount'],
            'source' => 'new sale',
            'deposit_date' => isset($request['deposit_date']) ? Carbon::parse($request['deposit_date']) : Carbon::now()
        ]);

        $this->sale->account($account);


        $payment->savePayable($account);

        return $payment;


	// TODO: Implement store() method.
	}
	public function update(Request $request)
	{

	// TODO: Implement update() method.
	}
	public function delete(Request $request)
	{

	// TODO: Implement delete() method.
	}

    /**
     * @param mixed $sale
     * @return SalePaymentRepository
     */
    public function setSale($sale)
    {
        $this->sale = $sale;
        return $this;
    }
} 
