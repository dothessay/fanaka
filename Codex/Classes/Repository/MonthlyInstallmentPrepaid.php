<?php 


namespace Codex\Classes\Repository;
use App\DB\Sale\MonthlyInstallmentPlan;
use App\Sale\Sale;
use App\Sale\SalePayment;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\DB\Sale\MonthlyInstallmentPrepaid as InstallmentPrepaid;

Class MonthlyInstallmentPrepaid
{

    /**
     * @var Sale $sale
     *
     */

    private $sale;

    /**
     * @param mixed $sale
     * @return MonthlyInstallmentPrepaid
     */
    public function setSale($sale)
    {
        $this->sale = $sale;

        return $this;
    }

    /**
     *param Request $request
     * @param Request $request
     */
	protected function all(Request $request)
	{

	// TODO: Implement all() method.
	}

	public function store(Request $request)
	{
	    ini_set('max_execution_time', 60000);

	    $prepaidAmount = $this->prePaid();

        // check if previous instalment was not fully paid

        $previousInstalment  = MonthlyInstallmentPlan::where([
            'sale_id'  => $this->sale->id,
            'month'    => Carbon::parse($request['month'])->subRealMonths(1)->format('Y-m'),
            'is_paid'  => false
        ])
            ->first();

        // update previous as paid
        if ($previousInstalment )
        {

            $previousInstalment->is_paid = true;
            $previousInstalment->save();
        }


	    if ($prepaidAmount)
        {
            $monthlyPayment = MonthlyInstallmentPlan::where([
                'sale_id' =>  $this->sale->id,
                'month'   => Carbon::parse($request['month'])->format('Y-m')
            ])->first();

            $months = $prepaidAmount / $monthlyPayment->amount;
            $total = 0;
            $amount =  $monthlyPayment->amount;

            //update the plan as paid

            $plan = MonthlyInstallmentPlan::where([
                'sale_id'    => $this->sale->id,
                'month'      =>  Carbon::parse($request['month'])->format('Y-m')
            ])->first();

            if ($request['amount'] >= $plan->amount){
                $plan->is_paid =  true;
                $plan->save();
            }

            if($months % 1 == 0) {

                $months = $months + 1;

            }

            for ($i = 1  ; $i <=  $months ; $i++)
            {
                $total += $amount;
                $balance =  $prepaidAmount - $total;

                $month = Carbon::parse($request['month'])->addRealMonths($i)->format('Y-m');
                $pre[] = [
                    'month'  => $month,
                    'amount'   => $amount
                ];
                InstallmentPrepaid::updateOrCreate([
                    'sale_id'  => $this->sale->id,
                    'month'    => $month,
                ],[
                    'sale_id'  => $this->sale->id,
                    'month'    => $month,
                    'amount'    => $amount,

                ]);


                if ( $balance < $monthlyPayment->amount)
                {
                    $amount =  $balance;
                }
            }
        }
	}
	protected function update(Request $request)
	{

	// TODO: Implement update() method.
	}
	protected function delete(Request $request)
	{

	// TODO: Implement delete() method.
	}

	private function prePaid(){

        // create a prepaid

        //check if monthly payment is done

        $monthlyPayment = MonthlyInstallmentPlan::where([
            'sale_id'  => $this->sale->id,
        ])
            ->first();
        if (!  is_null($monthlyPayment))
        {

            // get this month payment

            $payments = SalePayment::where('sale_id', $this->sale->id)
                ->whereBetween('created_at', [
                    Carbon::parse($monthlyPayment->month.'-'. $monthlyPayment->day)->subRealDays(31)->startOfDay(),
                    Carbon::parse(date('Y-m-'. $monthlyPayment->day))->endOfDay()

                ])
                ->get();



            if (! $monthlyPayment->is_paid  && $payments->sum('amount') === $monthlyPayment->amount)
            {
                $monthlyPayment->is_paid = true;
            }
            if ( $payments->sum('amount') > $monthlyPayment->amount)
            {
                return ($payments->sum('amount') - $monthlyPayment->amount);

            }

        }
        return 0;
    }
} 
