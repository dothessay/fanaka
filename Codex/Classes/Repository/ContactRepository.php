<?php 


namespace Codex\Classes\Repository;
use App\DB\Communication\Contact;
use Carbon\Carbon;
use Codex\Classes\Handlers\GetClientsContactHandler;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;

Class ContactRepository
{

    /**
     *param Request $request
     * @param Request $request
     * @return Collection
     */
	public function all(Request $request)
	{
	    $contact  = Contact::query();
	    $startDate = isset($request['startDate']) ? Carbon::parse($request['startDate']) : Carbon::now()->startOfCentury();
        $endDate = isset($request['endDate']) ? Carbon::parse($request['endDate']) : Carbon::now()->endOfMonth();
        $whereIn = [];
        if (! $request->user()->can('access-module-component-functionality', 'sale_manage_show-all-sales') && !  $request->user()->departmentAdmin)
        {
            $contact = $contact->where('user_id',$request->user()->id);
        }
        if ( $request->user()->departmentAdmin)
        {
            $usersIds = $request->user()->departmentAdmin->role->users->pluck('id')->toArray();
            $contact = $contact->whereIn('user_id', $usersIds);
            //$whereIn = $usersIds;
        }


        $contacts = collect();


        if (isset($request['action']) && $request['action'] === 'thisMonth') {
            $startDate = Carbon::now()->startOfMonth();
            $endDate = Carbon::now()->endOfMonth();
        }
        if (isset($request['action']) && $request['action'] === 'lastMonth') {
            $startDate = Carbon::now()->subRealMonth()->startOfMonth();
            $endDate = Carbon::now()->subRealMonth()->endOfMonth();

        }
        if (isset($request['action']) && $request['action'] === 'thisYear') {
            $startDate = Carbon::now()->startOfYear();

        }
        foreach ($contact->whereBetween('created_at', [$startDate, $endDate])->get() as $contact) {
            $clientContacts = new GetClientsContactHandler();
            $clientContacts->getPhone($contact->phone);
            $clientContacts->getFullName($contact->name);
            $clientContacts->getHasSiteVisit($contact->hasVisited ? " YES" : "NO");
            $clientContacts->getIsCustomer(false);
            $clientContacts->getEditPath($contact->path() . '/edit');
            $clientContacts->getprojects($contact->getVisitedProjects());
            $clientContacts->getMeta('status', $contact->getMeta('status', ''));
            $clientContacts->getMeta('date', Carbon::parse($contact->created_at)->format('Y-m-d'));
            $clientContacts->getMeta('agent',  ucwords($contact->user->fullName()));
            $clientContacts->getMeta('id',  $contact->id);
            $color = "";
            if ($clientContacts->status == 'warm') {
                $color = "badge badge-warning";
            }
            if ($clientContacts->status == 'cold') {
                $color = "badge badge-danger";
            }
            if ($clientContacts->status == 'hot') {
                $color = "badge badge-success";
            }
            $clientContacts->getMeta('color', $color);

            $contacts->push($clientContacts);

        }

        return $contacts->filter();
	}

	public function store(Request $request)
	{

	// TODO: Implement store() method.
	}
	public function update(Request $request)
	{

	// TODO: Implement update() method.
	}
	public function delete(Request $request)
	{

	// TODO: Implement delete() method.
	}
} 
