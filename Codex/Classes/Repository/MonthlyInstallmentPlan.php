<?php 


namespace Codex\Classes\Repository;
use App\Sale\Sale;
use Carbon\Carbon;
use Carbon\CarbonPeriod;
use Illuminate\Http\Request;
use \App\DB\Sale\MonthlyInstallmentPlan as InstallmentPlan;
Class MonthlyInstallmentPlan
{


    /**
     * @var Sale
     */
    private $sale;


    /**
     * @param Request $request
     */
    public function all( Request $request)
	{

	// TODO: Implement all() method.
	}

	public function store(Request $request)
	{
	    // remove all plans
        $this->removeAllPlansForThisSale();

	    // check if first payment is 30%
        $total = $this->sale->getTotalSaleAmount();

        $percentage = (0.30 * floatval($total))  ;

        $balanceFromPercentage = $total - $percentage;

        try {
            $instalmentAmount =  ceil($balanceFromPercentage / ($request['months']));
        } catch (\Exception $exception)
        {
            $instalmentAmount = ceil($balanceFromPercentage / 1);
        }

        $cummurative = $percentage;
        $myArray = ['cummurative' => $percentage];

        // create first month  instalment plan

        $isPaid = ($request['amount'] === $percentage) ? true : false;

        InstallmentPlan::create([
            'sale_id'   => $this->sale->id,
            'month'     =>  Carbon::parse($this->sale->created_at)->copy()->format('Y-m'),
            'day'       =>  Carbon::parse($this->sale->created_at)->copy()->format('d'),
            'is_paid'   =>  $isPaid,
            'amount'    =>  ceil($percentage),
            'penalty'   => 0
        ]);

        $date = $this->sale->created_at->copy()->addRealMonth();


        for ($i =0 ; $i < floatval($request['months']) ; $i++)
        {
            if ($cummurative + $instalmentAmount > $total)
            {
                $instalmentAmount = $instalmentAmount - ( $cummurative + $instalmentAmount - $total);

            }
            InstallmentPlan::create( [
                'sale_id'   => $this->sale->id,
                'month'     =>  $date->copy()->addRealMonths($i)->format('Y-m'),
                'day'       =>  Carbon::parse($this->sale->created_at)->copy()->format('d'),
                'is_paid'   =>  false,
                'amount'    =>  ceil($instalmentAmount),
                'penalty'   => 0
            ]);

            $cummurative =  $cummurative + $instalmentAmount;




            //dd($total , $this->sale->installmentPlans->sum('amount') );

        }



	}
    public function update(Request $request)
	{

	// TODO: Implement update() method.
	}
    public function delete(Request $request)
	{

	// TODO: Implement delete() method.
	}

    /**
     * @param mixed Sale $sale
     * @return MonthlyInstallmentPlan
     */
    public function setSale(Sale $sale)
    {
        $this->sale = $sale;

        return $this;
    }

    private function removeAllPlansForThisSale()
    {
        $this->sale->installmentPlans()->delete();
//        foreach ($this->sale->installmentPlans as $installmentPlan) {
//
//            $installmentPlan->delete();
//
//        }

    }


} 
