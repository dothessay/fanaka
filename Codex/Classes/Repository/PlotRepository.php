<?php


namespace Codex\Classes\Repository;


use App\Plot\Plot;
use App\User;
use Codex\Classes\Filters\CanSellFilter;
use Codex\Classes\Filters\DateBetweenFilter;
use Codex\Classes\Filters\IdFilter;
use Codex\Classes\Filters\IsReservedFilter;
use Codex\Classes\Filters\IsSoldFilter;
use Codex\Classes\Filters\ProductCodeFilter;
use Codex\Classes\Filters\ProductFilter;
use Codex\Classes\Filters\ProjectFilter;
use Codex\Classes\Filters\SlugFilter;
use Illuminate\Pipeline\Pipeline;

class PlotRepository
{
    public function all()
    {
        return app(Pipeline::class)
            ->send(Plot::query())
            ->through([
                IdFilter::class,
                CanSellFilter::class,
                IsReservedFilter::class,
                IsSoldFilter::class,
                ProductCodeFilter::class
            ])
            ->thenReturn()
            ->orderBy('product_code','ASC')
            ->get();
    }

}
