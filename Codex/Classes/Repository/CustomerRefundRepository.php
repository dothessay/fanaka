<?php 


namespace Codex\Classes\Repository;
use App\DB\Customer\CustomerRefund;
use App\Sale\Sale;
use Illuminate\Http\Request;

Class CustomerRefundRepository
{

    /**
     *@var Sale $sale
     */

    protected $sale;

	/**
	*@param Request $request
	*/
	public function all(Request $request)
	{

	// TODO: Implement all() method.
	}

	public function store(Request $request)
	{
	    CustomerRefund::create([
	        'amount'  => $this->sale->getPaidAmount(),
            'customer_ids'  => serialize($this->sale->getCustomerId()),
            'agent_id'  => $this->sale->soldBy->id,
            'plot_ids'  => serialize($this->sale->getPlotIds()),
            'selling_price'   => $this->sale->getTotalSaleAmount(),
        ]);
	}
	public function update(Request $request)
	{

	// TODO: Implement update() method.
	}
	public function delete(Request $request)
	{

	// TODO: Implement delete() method.
	}

    public function setSale(Sale $sale) : void
    {
        if($this->sale)
        {
            $this->sale = $sale;
        }
	}
} 
