<?php


namespace Codex\Classes\Repository;


use App\DB\Business\Meta;
use App\DB\Sale\Installment;
use App\Sale\Sale;
use Carbon\Carbon;
use Codex\Classes\Filters\AgentFilter;
use Codex\Classes\Filters\CommissionDateBetweenFilter;
use Codex\Classes\Filters\CommissionFilter;
use Codex\Classes\Filters\DateBetweenFilter;
use Codex\Classes\Filters\IdFilter;
use Illuminate\Http\Request;
use Illuminate\Pipeline\Pipeline;
use Illuminate\Support\Facades\DB;

class SalesRepository
{

    public function all(Request $request)
    {
        if ( $request->user()->departmentAdmin)
        {
            return $this->departmentSales($request);
        }
        return $this->allSales($request);
        
    }

    public function percentage($percentage)
    {
        $request = \request();
        $sales = collect();
        foreach ($this->all($request) as $sale)
        {

            if ($percentage == $sale->getPercentage())
            {
                $sales->push($sale);
            }
        }

        return $sales;
    }
    private function departmentSales(Request $request)
    {
        $sales  = $this->allSales($request);
        // get departmental users

        if ( $request->user()->departmentAdmin)
        {
            $usersIds = $request->user()->departmentAdmin->role->users->pluck('id')->toArray();

            $sales = $sales->whereIn('agent', $usersIds);
        }

        return $sales;


    }
    private function allSales(Request $request){

        if (! $request->user()->can('access-module-component-functionality', 'sale_manage_show-all-sales')
            &&
            !  $request->user()->departmentAdmin)
        {

            request()->request->add(['agent'  => $request->user()->id]);

        }

        return app(Pipeline::class)
            ->send(Sale::query())
            ->through([
                AgentFilter::class,
                IdFilter::class,
                DateBetweenFilter::class,
                CommissionDateBetweenFilter::class,
                CommissionFilter::class
            ])
            ->thenReturn()
            ->orderBy('agent', 'ASC')
            ->get();
    }

    public function createSale(Request $request)
    {

        try {

            DB::beginTransaction();
            $sale =  Sale::create([
                'business_code' => $request->user()->business->code,
                'user_id' => auth()->id(),
                'agent' => $request['agent'],
                'total_amount' => $request['total'],
                'balance' => (double)$request['total'] - (double)$request['amount'],
                'title_fee' => isset($request['title_fee']) ? true : false,
                'is_paying' => isset($request['is_paying']) ? true : false,
            ]);

            $meta = Meta::create([
                'key'  => 'discount',
                'value'  => isset($request['discount']) ? $request['discount'] : 0
            ]);



            $sale->saveMeta($meta);

            $instalment = Installment::create([
                'sale_id' => $sale->id,
                'months' => $request['months'],
                'amount' => floatval($request['total']) / floatval($request['months']),
                'completion_date' => Carbon::now()->addMonths($request['months'])
            ]);



            // attach sale to a customer
            (new CustomerSaleRepository())->setSale($sale)->store($request);

            (new  SaleItemRepository())->setSale($sale)->store($request);

            // invoice
            (new  InvoiceRepository())->setSale($sale)->store($request);

            // payments

            (new  SalePaymentRepository())->setSale($sale)->store($request);

            // create monthly instalment

            (new MonthlyInstallmentPlan())->setSale($sale)->store($request);

            DB::commit();
            return $sale;

        }
        catch (\Exception $exception)
        {

            DB::rollBack();

            throw new \Exception( $exception->getMessage());
        }

    }

    public function handle()
    {

    }

}
