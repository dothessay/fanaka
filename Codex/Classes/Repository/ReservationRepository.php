<?php


namespace Codex\Classes\Repository;


use App\DB\Sale\OfferLetter;
use Codex\Classes\Filters\AgentIdFilter;
use Codex\Classes\Filters\DateBetweenFilter;
use Codex\Classes\Filters\IdFilter;
use Codex\Classes\Filters\IsConvertedFilter;
use Codex\Classes\Filters\UserFilter;
use Illuminate\Pipeline\Pipeline;

class ReservationRepository
{
    public function all()
    {

        if ( auth()->user()->departmentAdmin)
        {
            return $this->departmentOffers();
        }
        return $this->allOffers();

    }
    private function departmentOffers()
    {
        $offers  = $this->allOffers();
        // get departmental users

        if ( auth()->user()->departmentAdmin)
        {
            $usersIds = auth()->user()->departmentAdmin->role->users->pluck('id')->toArray();

            $offers = $offers->whereIn('agent_id', $usersIds);
        }

        return $offers;


    }
    private function allOffers()
    {
        if (!auth()->user()->can('access-module-component-functionality', 'sale_manage_show-all-sales') && !  auth()->user()->departmentAdmin  ) {

            request()->request->add(['agent_id'  => auth()->user()->id]);



        }
        $request = request();

        if (is_null(request('agent_id'))){
            unset($request['agent_id']);

        }

        return app(Pipeline::class)
            ->send(OfferLetter::query())
            ->through([
                AgentIdFilter::class,
                IdFilter::class,
                DateBetweenFilter::class,
                UserFilter::class,
                IsConvertedFilter::class
            ])
            ->thenReturn()
            ->get();
    }

}
