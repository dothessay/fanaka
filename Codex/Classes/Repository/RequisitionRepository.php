<?php


namespace Codex\Classes\Repository;


use App\DB\Account\PettyCashRequisitionRequest;
use App\DB\Finance\ChartOfAccount;
use Codex\Classes\Filters\ApprovedByFilter;
use Codex\Classes\Filters\DateBetweenFilter;
use Codex\Classes\Filters\IdFilter;
use Codex\Classes\Filters\PendingFilter;
use Codex\Classes\Filters\UserFilter;
use Illuminate\Http\Request;
use Illuminate\Pipeline\Pipeline;

class RequisitionRepository
{

    private function getDepartmental($requisitions)
    {
        if ( \request()->user()->departmentAdmin) {

            $usersIds = request()->user()->departmentAdmin->role->users->pluck('id')->toArray();

            $requisitions = $requisitions->whereIn('user_id', $usersIds);

        }
        return $requisitions;

    }
    public function all()
    {
        $orderBy = 'user_id';
        if (request()->has('order_by') )
        {
            $orderBy = request('order_by');
        }

        if (! request()->user()->can('access-module-component-functionality', 'sale_manage_show-all-sales') &&
            !  request()->user()->departmentAdmin) {

            request()->request->add(['user_id'  => request()->user()->id]);
        }

        $requisitions =  app(Pipeline::class)
            ->send(PettyCashRequisitionRequest::query())
            ->through([
                IdFilter::class,
                UserFilter::class,
                DateBetweenFilter::class,
                ApprovedByFilter::class,
                PendingFilter::class,
            ])
            ->thenReturn()
            ->orderBy($orderBy,'ASC')
            ->get();
        return $this->getDepartmental($requisitions);
    }

    public function store(Request $request)
    {
        return PettyCashRequisitionRequest::create([
            'user_id'  => $request->user()->id,
            'amount'   => $request['amount'],
            'reason'   => $request['reason'],
        ]);

    }
    public function update(Request $request, PettyCashRequisitionRequest $cashRequisitionRequest)
    {
        return $cashRequisitionRequest->update([
            'amount'   => $request['amount'],
            'reason'   => $request['reason'],
        ]);

    }
    public function approve(Request $request , PettyCashRequisitionRequest $requisitionRequest)  : void
    {
        if ( is_null($requisitionRequest->approved_at ))
        {
            $requisitionRequest->approved_by = $request->user()->id;
            $requisitionRequest->approved_at = now();
            $requisitionRequest->save();
        }
    }
    public function reject(Request $request , PettyCashRequisitionRequest $requisitionRequest) : void
    {
        if ( is_null($requisitionRequest->rejected_at ))
        {
            $requisitionRequest->rejected_by = $request->user()->id;
            $requisitionRequest->rejected_at = now();
            $requisitionRequest->reject_reason = $request->reject_reason;
            $requisitionRequest->save();
        }
    }
    public function disburse(Request $request , PettyCashRequisitionRequest $requisitionRequest)  : void
    {
        if ( is_null($requisitionRequest->disbursed_at ))
        {
            $requisitionRequest->disbursed_by = $request->user()->id;
            $requisitionRequest->disbursed_at  = now();
            $requisitionRequest->save();

            // insert into Petty cash expenses

            if (! $chart = ChartOfAccount::where('name', 'requisition')->first()) {

                $chart = ChartOfAccount::create([
                    'name'  => 'requisition',
                    'code'  => '3300/060',
                    'type'  => 'expenses'
                ]);
            }

            $request->request->add([
                'amount'  =>  $requisitionRequest->amount,
                'purpose' =>  $requisitionRequest->reason,
                'chart'   => $chart->id
            ]);

            (new ExpensesRepository())
                ->store($request);
        }


    }
}
