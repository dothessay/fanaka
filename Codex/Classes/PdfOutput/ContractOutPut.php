<?php
/**
 * Created by PhpStorm.
 * User: philip
 * Date: 3/2/18
 * Time: 12:07 PM
 */

namespace Codex\Classes\PdfOutput;


use App\DB\Role\Business;
use App\DB\Sale\Calculator;
use App\Sale\Sale;
use Carbon\Carbon;
use Codex\Classes\Handlers\Calculator\CalculatorHandler;
use Codex\Classes\Handlers\Calculator\CalculatorHandlerFacade;
use Codex\Classes\Helper;
use Codex\Contracts\PdfOutput;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Storage;
use Mpdf\Mpdf;

class ContractOutPut
{
    private $customer;
    private $content;
    private $logo;
    private $sale;


    public static  function build()
    {

        return new self();

    }



    public function generate(Sale $sale , $html)
    {
        $this->sale = $sale;
            $logo = url(Storage::url($sale->business->logo));

        $this->logo = $logo;
        $plotDetails = $sale->getPlotDetails();
        $mpdf = new Mpdf();
        $mpdf->fontsizes = 13;
        $mpdf->defaultAlign = "justify";
        $mpdf->default_font = 'tohama';

        $mpdf->default_font_size = 12;


        $mpdf->SetWatermarkImage($logo);

        $mpdf->SetWatermarkImage($logo);
        $mpdf->showWatermarkImage = true;

        $frontPage = fanakaConfig('front_page');
        $frontPage = str_replace("{date}",now()->format('l jS \\of F Y'), $frontPage);
        $frontPage = str_replace("{customerDetails}",$this->sale->getCustomerDetails(), $frontPage);
        $frontPage = str_replace("{plots}",$plotDetails, $frontPage );

        $firstPage = "<style> .body{
                  font-family: serif;
                  font-size: 12px;

                  }</style>
                   <div class='body'>
                   <p style=\"font-weight: bold; font-size: 12pt; align-content: center; text-align: center\">

                     <img src='" . $logo . "' style=\"width: 100%;height: auto; max-height: 70px\"alt=\"Fanaka Real Estate\"/>
                     <div style=\"font-weight: bold; font-size: 12pt; align-content: center; text-align: center\">".$frontPage."</div>";

        $mpdf->WriteHTML($firstPage);



        $mpdf->SetWatermarkImage($logo);

        // add new page

        $mpdf->AddPage();

        $pageTwo = '<p style="font-weight: bold; font-size: 12pt; align-content: center; text-align: center">
                    REPUBLIC OF KENYA  <br/> AGREEMENT FOR SALE OF LAND  <br/></p>';

        $mpdf->WriteHTML($pageTwo);
        $mpdf->WriteHTML($html);

        //ast Page
        $mpdf->AddPage();

        $winteness  = '';

        $footer = fanakaConfig('agreement_footer');

        $footer = str_replace("{director}", fanakaConfig('director') , $footer);
        $footer = str_replace("{vendor}", fanakaConfig('vendor') , $footer);
        $footer = str_replace("{purchaserSignature}", $this->sale->signatories() , $footer);
        $footer = str_replace("{date}", now()->format('l jS \\of F Y'), $footer);


        $winteness .= $footer;

        foreach ($this->sale->witnesses as $witness) {

            $title = "<br/>SIGNED BY THE WITNESS<br/>";

            $winteness .= $title ."<b><br/>

                            NAME: {$witness->fullname()}	………………………………. <br/>
                            ID NO: {$witness->id_no} <br/>
                            </b>";
        }


        if ($this->sale->getHasNextOfKin())
        {

            $winteness .= "<br/>Next Of Kin<br/>" .$this->sale->getNextOfKin();
        }


        $mpdf->WriteHTML($winteness);
        $mpdf->WriteHTML("</div>");




        // calculator

//       // if ($sale->getMeta('save_calculator', true)) {
//            $mpdf->AddPage();
//            $months = 1;
//
//            if (isset($sale->installment->months)) {
//                $months = $sale->installment->months;
//            }
//
//            $deposit = isset($sale->payments()->last()->amount) ? $sale->payments()->last()->amount : 0;
//            $principle = $sale->saleItems->sum('price');
//
//            //dd($deposit, $principle);
//
//            $rates = (new  CalculatorHandler())->getRate($months)->calculate($sale->getTotalSaleAmount());
//            $html = view('sales.calculator._data')->with(['payments' => $rates,'showPaid' => true])->render();
//
//            $mpdf->WriteHTML($this->style());
//            $mpdf->WriteHTML($html);
//       // }

        $mpdf->SetWatermarkImage($logo);

        $mpdf->SetTitle($this->sale->getCustomerDetails() .' -  Agreement');

        return  $mpdf->Output('agreement.pdf',\Mpdf\Output\Destination::INLINE);
    }
  


    public function outPut(Collection $collection)
    {
        $this->setHeader($collection);
        $this->setContent($collection);


        $logo = asset('assets/img/fanaka_logo.png');

        $this->logo = $logo;

        $content = $this->content;


        $pdf = new Mpdf();


        $pdf->WriteHTML($this->style());

        $pdf->WriteHTML($content);
        $pdf->WriteHTML($this->content());
        $pdf->watermarkImage = $logo;

        $pdf->Output();
        return $this->generate();

    }

    /**
     * @param Collection $collection
     * @return $this
     */
    public function setHeader(Collection $collection)
    {
        $sale = $collection->first();
        $this->sale = $sale;

        $header = $sale->getCustomerDetails();

        $this->customer = $sale->customers;

        $this->pdfTitle = $sale->getCustomerDetails();

       // $header .= "<br/>\r\n" . $sale->contactPoint() . "<br/>";
        $header .= "\r\n Sale Reference FSL_" . $sale->id . "<br/>";

        $this->header = $header;

        $business = auth()->user()->business;

        $businessHeader = '<span style="font-weight: bold; ">' . $business->name . "</span>";
        $businessHeader .= "<br/>" . $business->address . "<br/>";
        $businessHeader .= "<br/>" . $business->slug . "<br/>";

        $this->headerBusiness = $businessHeader;

        return $this;

    }

    public function setContent(Collection $collection)
    {

        $this->content = '<span style="align-content: center ; font-size: x-large" ><u>RE: PLOT SALE AGREEMENT</u></span>';
        return $this;

    }

    public function content()
    {



        $logo = url('assets/img/fanaka_logo.png');


        //$logo = "http://fanaka.co.ke/wp-content/uploads/2018/04/fanaka-1.png";

        $this->logo = $logo;

        $plotNo = [];
        $plotSize = [];

        $plotDetails = '';
        $date = Carbon::now()->format('Y-m-d');

        $invoice = $this->sale->invoices;




        if (isset($invoice)) {
            $balance = $invoice->balance();
            $balance = isset($balance) ? $invoice->balance() : 0;
        } else {
            $balance = 0;
        }

        $balanceToWord = $balance;

        $balance = number_format($balance, 2);


        $months = isset($this->sale->installment->months) ? $this->sale->installment->months : "(0)";
        $installmentAmount = isset($this->sale->installment->amount) ? $this->sale->installment->amount : "(0)";

        $initialAmountToWord = $this->sale->salePayments->last()->amount;

        if ($this->sale->getBalance() === (double)0 )
        {
            $initialAmount = number_format($this->sale->salePayments->sum('amount'));
        }


        $installmentAmountToWord = $installmentAmount;

        $installmentAmount = number_format((double)$installmentAmount, 2);

        foreach ($this->sale->saleItems as $saleItem) {

            $plotNo[] = $saleItem->plot->product->name() .' :'.ucfirst( $saleItem->plot->title_no). ' Plot  no: ' . $saleItem->plot_no . ' '.$saleItem->plot->getPropertyDetails();
            $plotSize[] = $saleItem->plot->size->label;
            $plotDetails = '';

        }


        $business = Business::first();

        $businessDetails = $business->name;
        $businessDetails .= "\r\n<br>".$business->address;



        $plotDetails = $plotDetails . ' ' . implode($plotNo, ' , ');









        //return response()->download('philip.docx');



        //dd('closer');


        $mpdf = new Mpdf();
        $mpdf->fontsizes = 13;
        $mpdf->defaultAlign = "justify";
        $mpdf->default_font = 'tohama';

        $mpdf->default_font_size = 12;


        $mpdf->SetWatermarkImage($logo);

        $mpdf->SetWatermarkImage($logo);
       $mpdf->showWatermarkImage = true;

        //$mpdf->showWatermarkImage = true;

       // $mpdf->showWatermarkImage = true;
        //<img src='" . $logo . "' style=\"width: 100%;height: auto; max-height: 70px\"alt=\"Fanaka Real Estate\"/>

        $html = "<style> .body{ 
                  font-family: serif; 
                  font-size: 12px;
                   
                  }</style>
                   <div class='body'> <p style=\"font-weight: bold; font-size: 12pt; align-content: center; text-align: center\">
                  
                     <img src='" . $logo . "' style=\"width: 100%;height: auto; max-height: 70px\"alt=\"Fanaka Real Estate\"/>
     
                    <p style='text-align: center; font-family: serif;  font-weight: bolder; font-size: 16px; '>DATED….." . Carbon::now()->format('Y-F-d') . "	
                                                 <br/>
                                                 <br/>
                                                 <br/>
                                                 <br/>
                                                 <br/>
                                                 <br/>
                                                 <br/>AGREEMENT FOR SALE
                                                  <br/>
                                                 <br/>
                                                 <br/>
                                                 <br/>
                                                 <br/>
                                                 <br/>
                                                 <br/>FANAKA REAL ESTATE LTD
                                                 <br/>
                                                 <br/>
                                                 <br/>
                                                 <br/>
                                                 <br/>
                                                 (Vendor)
                                                    <br/>
                                                 <br/>
                                                 <br/>
                                                 <br/>
                                                 <br/>
                                                 <br/>
                                                   <br/>{$this->sale->getCustomerDetails()}
                                                   
                                                    <br/>
                                                 <br/>
                                                 <br/>
                                                 
                                                   <br/>(Purchaser)
                                                 <br/>
                                                 <br/>
                                                 <br/>
                                                 <br/>
                                                 <br/>
                                                 <br/>
                                                   <br/>{$plotDetails}                                           
                                                   
                         </p>";




        $mpdf->WriteHTML($html);



        $mpdf->SetWatermarkImage($logo);

        $mpdf->AddPage();





        $winteness = "<p style='text-align: justify ;  margin-top: 50%'>
IN WITNESS WHEREFORE the parties have herein appended their respective Signature on the date and year herein above mentioned.
 SEALED with the common seal of the company	
</p>
<br/>
<br/>

VENDOR<br/>
<br/>
<br/>
<b>
Fanaka Real Estate Limited,<br/>
Dune Plaza, Suite D1 , Ruai Town, <br/>
P.o Box 252-00222 , Nairobi, <br/>
Tel: +254 799 000 111, <br/>
Email: info@fanaka.co.ke.</b>

<br/>
<br/>

DIRECTOR

<br/>
<br/>
<b>

NAME: MOSES MURIITHI KIHUNII	…………………………… <br/>
ID NO: 25876155 <br/>
 <br/><br/><br/><br/><br/>

SIGNED BY THE PURCHASER<br/>
 {$this->sale->signatories()}<br/>";
       /* if ($this->sale->witnesses->count()) {
            $winteness .= "<br/><br/><br/><br/>SIGNED BY THE WITNESS<br/><br/><br/><br/>";

        }*/

       $title = "";


        foreach ($this->sale->witnesses as $witness) {

            if ($witness->type === 'advocate')
            {
                $title = "<br/>SIGNED BY THE ADVOCATE<br/>";
            }else{
                $title = "<br/>SIGNED BY THE WITNESS<br/>";
            }

            $winteness .= $title ."<b><br/>

                            NAME: {$witness->fullname()}	………………………………. <br/>
                            ID NO: {$witness->id_no} <br/>
                            </b>";
        }





        if ($this->sale->getHasNextOfKin())
        {

            $winteness .= "<br/>Next Of Kin<br/>" .$this->sale->getNextOfKin();
        }


        $mpdf->WriteHTML($winteness);
        $mpdf->WriteHTML("</div>");


        $mpdf->SetWatermarkImage($logo);




        $mpdf->SetTitle($this->sale->getCustomerDetails() .' -  Agreement');


        return $mpdf->Output();

       // return $mpdf->Output($this->sale->getCustomerDetails() . '.pdf', 'F');


    }

    public function contentBk()
    {
        $htmlPageTwo = "<p style='text-align: center ; letter-spacing: 3px'>
      <h1 style='align-content: center ; text-align: center'>
      <b>REPUBLIC OF KENYA</b></h1>
       <br/>
       <br/>
      
       </p> 
       
       <p  style='align-content: center ; text-align: center'>
        <b><u style='line-height: 1cm;'>SALE AGREEMENT</u></b>
</p>
       <p style='line-height: 1cm;'>THIS AGREEMENT made on this… …..<b>{$date}</b> BETWEEN FANAKA REAL ESTATE a limited liability company duly incorporated under the company’s Act laws of Kenya and of post office Box1480-00100 NAIROBI, within the Republic aforesaid (herein referred to as the “VENDOR” which expression where the context so admits shall include its heirs, assigns and legal representatives) of the one part AND
, <b>{$this->sale->getCustomerDetailsForAgreement()}  </b> within the republic aforesaid (herein referred to as the “PURCHASER” which expression where the context so admits shall include her or his heirs, assigns and legal representatives) of the other part";


        $htmlPageTwo .= "<p style='line-height: 1cm;'><ol>
            
            <li>The property sold is <b>{$plotDetails}</b>.</b> The interests are freehold.
            </li><br/>
            <li>The vendor has agreed to sell and the purchaser has agreed to
 purchase the property for a sum of<b> " . Helper::numberToWord($this->sale->total_amount) . " (" . number_format($this->sale->total_amount, 2) . " )  </b> 
 should be made through account name FANAKA REAL ESTATE LIMITED ACCOUNT NUMBER 1440269339566 EQUITY BANK.
</li><br/>

    
        <li>The purchaser has already paid <b> ".$this->sale->getContractAmountInStatement()." </b>";


        if (($this->sale->getBalance() >  (double) 0))
        {
            if ($this->sale->installment->months > 1) {


               $htmlPageTwo .= " The purchaser will be depositing ". strtolower(ucfirst(" <b> " . Helper::numberToWord((double)$this->sale->getInstallment()) . "  ( " . number_format($this->sale->getInstallment()  , 2). ")</b> EVERY MONTH FOR THE NEXT <b> " . $months . "  months</b>"));


               // $htmlPageTwo .="The Purchaser has deposited <b>Three Hundred And Seventy Thousand Kenya Shillings only on 12 <sup>th</sup> January, 2019.</b> ";


            }else{
                $htmlPageTwo .= ". The " .(" balance of <b> " . Helper::numberToWord((double)$balanceToWord) . " (" . $balance . " )</b> to be paid within <b>" . $months . " </b> month (s) from the date of execution of the sales agreement. ");




            }
        }


        /*if ( $this->sale->title_fee) {
            $htmlPageTwo .= "&nbsp; which is inclusive of the title transfer fee. ";

        }*/


        if ( $this->sale->notes) {
            foreach ($this->sale->notes as $note) {
                $htmlPageTwo .= $note->note .".";

            }

        }


        $htmlPageTwo .=" </li>";

        /*if (! $this->sale->title_fee) {
            $htmlPageTwo .= "<li>The title transfer fee is THIRTY THOUSAND (30,000) for each parcel. To be paid after completion of full amount. The title fee is inclusive of the lawyer’s fee, stamp duty and all fees related to transfer</li> ";

        }*/


        $htmlPageTwo .= "<li> The vendor shall obtain consent to transfer and execution of transfer form in favor of purchaser.
</li>
<li>The sale is subject to the law society conditions of sale (1989 edition) in so far as they are not inconsistent with the conditions contained in this agreement.
</li><br/>
<li>The properties are sold subject in vacant possession and against full payment of the purchase price and the purchasers shall take possession of the same upon execution of this agreement.
</li><br/>
<li>The properties are sold subject to provisions of the law under which it is held and special conditions (if any) contained in the title deed but otherwise free from all encumbrances.
</li>
<li>The vendor further irrevocably undertakes to execute any other documents now and/or in future to facilitate the issuance of the duty registered title deed over the properties in the name of purchaser.
</li><br>
<li>Any party in breach of this agreement will pay the innocent party the penalty of 10% of the purchase price and all money incidentals to this agreement.
</li><br/>
<li>The property/properties is not transferable from one project to another i.e the purchaser cannot transfer the terms of an agreement made for a particular plot to another plot for a different project.</li>
<br/>
<li>The properties are sold in the state or condition as it at the date hereof and the purchaser have inspected the property to its satisfaction.
</li><br/>
<li>The vendor hereby confirms to the purchaser that:-

<ol>
<li> There is no other claimant over the said properties sold and should there be any, the vendor hereby undertakes to indemnity the purchaser fully and wholly in respect of such claims.
</li><br/><li> The property is not on a buffer to there being no adverse report by a surveyor or any relevant Government ministry and/or government department that the property falls on public land.
</li><br/><li> The vendor has complied with all by-laws and regulations of the relevant local authority relating to the property.
</li><br/><li>The vendor has necessary power and authority to sell the property on the terms and conditions set out this agreement.
</li><br/><li>There is no law or decree or similar enactment, mortgage, indenture, trust, deed, caveat, contract or agreement which would conflict with or prevent the vendor from entering into performing and observing the terms of this agreement.
</li><br/><li>The vendor is not engaged in nor threatened by any litigation, arbitration or administrative proceedings relating to this property.
</li><br/><li> There is no adverse claim on the property, dispute regarding ownership boundary interests and has no intention of so doing.
</li><br/><li> The vendor has not given away any rights of way. Easement or any overriding interests and has no intention of so doing.
</li><br/><li> The vendor has not received any notice from any government or municipal authority or from owners of adjoining property which remain to be complied with and has disclosed all such notices received to the purchaser.
</li>

</ol>";
        //$mpdf->AddPage();


        $mpdf->WriteHTML($htmlPageTwo);

        //$mpdf->Output('PDF.PDF','F');
        $mpdf->SetWatermarkImage($logo);
        $mpdf->AddPage();

    }


    protected function style()
    {
        $header = '';

        return " {$header}  
                       <style>
        .invoice-box {
         width: 100%;
        font-size: 16px;
        line-height: 24px;
        font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;
      
    }
     table {
                                    border-collapse: collapse;
                                    width: 100%;
                                    border: solid 1px;
                                    font-size: 10px
                                    ;
                                   }

                            th, td {
                                     text-align: left;
                                     padding: 8px;
                                     border: solid 1px;
    
                                    }
                                    table.table-bordered {
                                    
                                    border: solid 1px;
                                    }
                                    
     tr:nth-child(even){
      
      background-color: lightblue;

      }
      tr:nth-child(odd){
      
      background-color: ;

      }
      table.table-bordered , th, td {
   border: 1px solid black;
   table-border-color-light: #0b2e13;
}

    .invoice-box table {
        width: 100%;
        text-align: left;
    }
    
    .no-border {
    border-style: none;
    }
    
    .invoice-box table td {
        
        vertical-align: top;
    }
    
    .invoice-box table tr td:nth-child(2) {
        text-align: right;
    }
    
    .invoice-box table tr.top table td {
        padding-bottom: 20px;
    }
    
    .invoice-box table tr.top table td.title {
        font-size: 45px;
        line-height: 45px;
        color: #333;
         text-align: left;
    }
    
    .invoice-box table tr.information table td {
        padding-bottom: 40px;
    }
    
    .invoice-box table tr.heading td {
      
        border-bottom: 1px solid #ddd;
        font-weight: bold;
    }
    
    .invoice-box table tr.details td {
        padding-bottom: 20px;
    }
    
    .invoice-box table tr.item td{
        border-bottom: 1px solid #eee;
    }
    
    .invoice-box table tr.item.last td {
        border-bottom: none;
    }
    
    .invoice-box table tr.total td:nth-child(2) {
        border-top: 2px solid ;
        font-weight: bold;
    }
    
    tr:nth-child(even){
             

         }

         th {
              
             color: black;
             border:solid 1px black;
             text-transform: uppercase;
          }
    
    @media only screen and (max-width: 600px) {
        .invoice-box table tr.top table td {
            width: 100%;
            display: block;
            text-align: center;
        }
        
        .invoice-box table tr.information table td {
            width: 100%;
            display: block;
            text-align: center;
        }
    }
    
    /** RTL **/
    .rtl {
        direction: rtl;
        font-family: Tahoma, 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;
    }
    
    .rtl table {
        text-align: right;
    }
    
    .rtl table tr td:nth-child(2) {
        text-align: left;
    }
                </style>";



    }
}
