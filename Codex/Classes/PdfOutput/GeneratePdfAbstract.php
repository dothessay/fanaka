<?php
/**
 * Created by PhpStorm.
 * User: philip
 * Date: 3/2/18
 * Time: 12:18 PM
 */

namespace Codex\Classes\PdfOutput;


use App\DB\Role\Business;
use Illuminate\Support\Facades\Storage;
use Mpdf\HTMLParserMode;
use Mpdf\Mpdf;

abstract class GeneratePdfAbstract
{

    public $header;
    public $content;
    public $headerBusiness;
    public $noOfFiles = 3;
    public $pdfTitle;

    public  $logo ;

   public $footer;

   public function __construct()
   {
       $this->logo = url(Storage::url(Business::first()->logo));
   }

    public function setLogo()
    {
        $logo = Business::first()->logo;

        $this->logo = $logo;

        return $this;

    }
    public function generate($fileTitle = '')
    {


        $content = $this->content;



        $pdf = $this->intiatePdf();

       // $pdf->SetImportUse();


       $pdf->WriteHTML($this->style());

        $pdf->WriteHTML($content);
        $pdf->WriteHTML($this->content());

   ;
        $pdf->showWatermarkImage = true;

        $pdf->SetWatermarkImage($this->logo);

        if (! is_null($this->footer))
        {
            $pdf->SetHTMLFooter($this->footer);
        }
        if (! is_null($fileTitle))
        {
            $pdf->SetTitle($fileTitle);

            $pdf->Output($fileTitle.'.pdf',\Mpdf\Output\Destination::INLINE);
        }
        else{

            $pdf->Output();
        }


    }

    public function addPage()
    {
        $mpdf = $this->intiatePdf();

        return $mpdf->AddPage();
    }

    private function intiatePdf()
    {

        $pdf =  new Mpdf();

        //$pdf->WriteHTML($this->style() ,HTMLParserMode::DEFAULT_MODE,  false, $close = false);

        return $pdf;
    }

    protected function style()
    {



        $header = '<!--mpdf
    <htmlpageheader name="letterheader">
        <table width="100%" style=" font-family: sans-serif;">
            <tr>
                <td width="25%" >
                    <span style="font-size: 12pt;">
                    ' . ucwords($this->headerBusiness) . '
                    </span>
                    </td>
                    <td width="25%" >
                    <span style="font-weight: bold; font-size: 12pt;">
                    <img src="' . $this->logo . '"style="width: 100%;height: auto; max-height: 70px"alt="Fanaka Real Estate"/>
                    </span>
                    </td>
                <td width="25%" style="text-align: right; vertical-align: top;">
                    <strong>' . ucwords($this->header) . '</strong>
                    
                </td>
            </tr>
        </table>
        
        <div style="margin-top: 1cm; text-align: right; font-family: sans-serif;">
            {DATE jS F Y}
        </div>
    </htmlpageheader>
    
    <htmlpagefooter name="letterfooter2">
        <div style="border-top: 1px solid #000000; font-size: 9pt; text-align: center; padding-top: 3mm; font-family: sans-serif; ">
            Page {PAGENO} of {nbpg}
        </div>
    </htmlpagefooter>
mpdf-->

<style>
    @page {
        margin-top: 2.5cm;
        margin-bottom: 2.5cm;
        margin-left: 2cm;
        margin-right: 2cm;
        footer: html_letterfooter2;
        ba
    }
  
    @page :first {
        margin-top: 8cm;
        margin-bottom: 4cm;
        header: html_letterheader;
        footer: _blank;
        resetpagenum: 1;
        footer: html_letterfooter2;
        
    }
  
    @page letterhead {
        margin-top: 2.5cm;
        margin-bottom: 2.5cm;
        margin-left: 2cm;
        margin-right: 2cm;
        footer: html_letterfooter2;
        
    }
  
    @page letterhead :first {
        margin-top: 8cm;
        margin-bottom: 4cm;
        header: html_letterheader;
        footer: _blank;
        resetpagenum: 1;
        footer: html_letterfooter2;
        
    }
</style>';

        return $header;

    }

    abstract public function content();

}
