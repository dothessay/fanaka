<?php
/**
 * Created by PhpStorm.
 * User: philip
 * Date: 3/23/18
 * Time: 9:43 AM
 */

namespace Codex\Classes\PdfOutput;


use App\Sale\SalePayment;
use Carbon\Carbon;
use Codex\Classes\Handlers\Calculator\CalculatorHandlerFacade;
use Codex\Contracts\PdfOutput;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Storage;

class ReceiptOutPut extends GeneratePdfAbstract implements PdfOutput
{

    private  $customer;
    private  $sale;

    public function outPut(Collection $collection)
    {
        $this->setHeader($collection);
        $this->setContent($collection);

        return $this->generate("Receipt");
    }

    /**
     * @param Collection $collection
     * @return $this
     */
    public function setHeader(Collection $collection)
    {
        $sale = $collection->first();
        $this->sale = $sale;

        $header = $sale->getCustomerDetails();

        $this->customer = $sale->customers;

        $this->pdfTitle = $this->sale->getCustomerDetails();

        $header .= "<br/>\r\n" . $this->sale->contactPoint() . "<br/>";
        $header .= "\r\n Sale Reference FSL_" . $sale->id . "<br/>";

        $this->header = $header;

        $businessHeader = fanakaConfig('vendor');

        $this->headerBusiness = $businessHeader;

        $this->footer = "You Were served By ".auth()->user()->fullName() ." on:" .Carbon::now()->toDayDateTimeString();

        return $this;

    }


    public function setContent($collection)
    {
        $this->content = "";


        return $this;

    }

    public function content()
    {

        $plotNo = [];
        $plotSize = [];


        foreach ($this->sale->saleItems as $saleItem) {

            $plotNo[] =  $saleItem->plot->product->name() .' Plot  no: ' .$saleItem->plot_no;
            $plotSize[] = $saleItem->plot->size->label;

        }


        $balance = $this->sale->total_amount - $this->sale->salePayments->sum('amount');

        $html = '<div class="invoice-box">
<p>Plot No : '.collect($plotNo)->implode(',' ,'').'  Total Amount ' . $this->sale->getTotalSaleAmount().'</p>
        <table cellpadding="0" cellspacing="0">
           
            
            <tr class="heading">
                <td>
                    Payment Method
                </td>
                
                <td>
                    Transaction Code 
                </td>
                
                <td>
                    Amount 
                </td>
                
                <td>
                    Date 
                </td>
            </tr>';


        $salePayments = SalePayment::where([
            'sale_id' => $this->sale->id,
            'is_approved' => true

        ])->oldest()->get();

        foreach ($salePayments as $salePayment) {


            $html .= '<tr class="item">
                <td>
                    '.$salePayment->payment_method.'
                </td> 
                <td>
                    '.$salePayment->reference_code.'
                </td>
                
                <td>
                    '.number_format($salePayment->amount , 2).'
                </td>
                
                <td>
                    '. $salePayment->deposit_date .'
                </td>
                
            </tr>';

        }



            $html .= '<tr class="total">
                  <td>Total Paid: </td> 
                  <td></td>
                           
                <td>
                    '.number_format($this->sale->salePayments->sum('amount') , 2).'
                </td>
                </tr>
                <tr class="total">
                <td>Balance: </td>
                <td></td>
                 <td>'.number_format($this->sale->getBalance() , 2).'</td>
            </tr>
        </table>
    </div>';

        // check if the sale have saved calculator

        /*if ($this->sale->calculators->count())
        {
            $payments = CalculatorHandlerFacade::getRate($this->sale->calculators->count())
                ->calculate($thi˚s->sale->saleItems->sum('price') , $this->sale->salePayments->sum('amount'));
            $html .= view('sales.calculator._data')->with(['payments' => $payments])->render();
        }*/

       return $html;

    }


    protected function style()
    {
        return "   <table>
                    <thead>
                        <tr>
                        <th width=\"25%\">{$this->headerBusiness}</th>
                        <th width=\"25%\"><img src=".url(Storage::url($this->sale->business->logo))." style='width: 100%;height: auto; max-height: 70px'></th>
                        <th width=\"25%\">{$this->header}</th>
                        </tr>
                    </thead>
                   
                    </table>
                       <style>
        .invoice-box {
         width: 100%;
        font-size: 16px;
        line-height: 24px;
        font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;
      
    }
     table {
                                    border-collapse: collapse;
                                    width: 100%;
                                   }

                            th, td {
                                     text-align: left;
                                     padding: 8px;
    
                                    }
                                    
     tr:nth-child(even){
      

      }
    .invoice-box table {
        width: 100%;
        text-align: left;
    }
    
    
    .invoice-box table td {
        
        vertical-align: top;
    }
    
    .invoice-box table tr td:nth-child(2) {
        text-align: right;
    }
    
    .invoice-box table tr.top table td {
        padding-bottom: 20px;
    }
    
    .invoice-box table tr.top table td.title {
        font-size: 45px;
        line-height: 45px;
        
         text-align: left;
    }
    
    .invoice-box table tr.information table td {
        padding-bottom: 40px;
    }
    
    .invoice-box table tr.heading td {
      
        border-bottom: 1px solid #ddd;
        font-weight: bold;
    }
    
    .invoice-box table tr.details td {
        padding-bottom: 20px;
    }
    
    .invoice-box table tr.item td{
        border-bottom: 1px solid #eee;
    }
    
    .invoice-box table tr.item.last td {
        border-bottom: none;
    }
    
    .invoice-box table tr.total td:nth-child(3) {
        border-top: 2px solid ;
        font-weight: bold;
    }
    
    tr:nth-child(even){
             

         }

         th {
              font-weight: lighter;
              font-size: 12px;
             text-transform:capitalize;
          }
    
    @media only screen and (max-width: 600px) {
        .invoice-box table tr.top table td {
            width: 100%;
            display: block;
            text-align: center;
        }
        
        .invoice-box table tr.information table td {
            width: 100%;
            display: block;
            text-align: center;
        }
    }
    
    /** RTL **/
    .rtl {
        direction: rtl;
        font-family: Tahoma, 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;
    }
    
    .rtl table {
        text-align: right;
    }
    
    .rtl table tr td:nth-child(2) {
        text-align: left;
    }
                </style>";



    }
}
