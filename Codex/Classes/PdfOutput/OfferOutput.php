<?php
/**
 * Created by PhpStorm.
 * User: philip
 * Date: 9/9/18
 * Time: 7:30 PM
 */

namespace Codex\Classes\PdfOutput;


use App\DB\Role\Business;
use App\DB\Sale\OfferLetter;
use App\Sale\Sale;
use Carbon\Carbon;
use Codex\Classes\Helper;
use Mpdf\Mpdf;
use phpDocumentor\Reflection\Types\Self_;

class OfferOutput
{
    private $mpdf;

    private $pageOne;
    private $pageTwo;
    private $businessDetails;

    private $logo;



    public function __construct()
    {
        $this->mpdf = new Mpdf();

    }

    public static function build()
    {
        return new static();

    }


    public function generate(OfferLetter $offerLetter, $html)
    {
        $logo = asset('assets/img/fanaka_logo.png');

        $this->logo = $logo;

        $business = Business::first();

        $this->businessDetails =  $business->name . "\r\n<br/>";
        $this->businessDetails .= "\r\n" . $business->address ;


        $this->businessDetails = ucwords(strtolower($this->businessDetails));

        $html .="<p style='line-height: 0.7cm;'>";

        if ($offerLetter->note) {
            $html .= "<li>" . $offerLetter->note . "</li>";

        }
        $html .= " </ol>";



        $html .= $offerLetter->signatories();


        $html .= $offerLetter->getNextOfKin();


/*
        if ($offerLetter->nextOfKin()) {
            $html .= "<br/>Next of kin:
      Name:&nbsp;   <b>" . $offerLetter->nextOfKin->full_name . "</b>          &nbsp;  &nbsp;  &nbsp;  &nbsp;  &nbsp;  &nbsp;        &nbsp;  &nbsp;  &nbsp;  &nbsp;  &nbsp;  &nbsp;        Phone:    &nbsp;  <b> " . $offerLetter->nextOfKin->phone_number . " </b>  &nbsp;  &nbsp;  &nbsp;  &nbsp;  &nbsp;  &nbsp;                        Relation:&nbsp; <b>" . $offerLetter->nextOfKin->relation . ". </b>";
        }*/
        $html .= "</div></div>";





        $html .= "
       <br/><img src=" . asset('assets/img/facebook.png') . " width='35px' height='35px'> @Fanaka REAL Estate Ltd 
       <img src=" . asset('assets/img/intagram.png') . " width='35px' height='35px'> Fanakaltd 
       <img src=" . asset('assets/img/twitter.png') . " width='35px' height='35px'> @FanakaLtd
       </p>";


        $pdf = new Mpdf();

        $pdf->SetImportUse();

        $pdf->default_font = "tohama";

        $pdf->default_font_size = "2";

        $pdf->SetTopMargin(50);

        $header = '';

        //$pdf->SetHTMLHeader($header);
        $pdf->SetHTMLHeader(" <table border='0' class='no-border' style='border-style: none'>
                    <thead>
                        <tr>
                       
                        <th style='border-style: none' ><br/><small>     </small></th>
                        <th style='border-style: none' ><br/><small>     </small></th>
                        <th style='border-style: none'>&nbsp;&nbsp;&nbsp;&nbsp;<img src=" . $logo . " style='width: 100%;height: auto; max-height: 80px'> </th>
                        
                        <th style='border-style: none'>&nbsp;&nbsp;&nbsp;&nbsp;".str_repeat("&nbsp;", 50)." </th>
                        <th style='justify-content: left; align-content: stretch  ; border-style: none; font-weight: lighter; font-size: 16px;  text-transform: capitalize; line-height: 0.5cm'><small> ". ucwords(strtolower($this->businessDetails))."</small></th>
                       
                        </tr>
                    </thead>
                   
                    </table>");

        $pdf->WriteHTML($this->style());

        $pdf->WriteHTML($html);

        $pdf->showWatermarkImage = true;

        $pdf->SetWatermarkImage($logo);

        $pdf->SetHTMLFooter('printed @ '.Carbon::now()->format('Y-m-d') . ' by '. auth()->user()->fullName());

        $pdf->Output();

    }


    public function generateOld(OfferLetter $offerLetter, $output = "D", $print = false, $name = null)
    {

        $logo = asset('assets/img/fanaka_logo.png');

        $this->logo = $logo;

        $business = Business::first();

        $this->businessDetails = $business->name . "\r\n";
        $this->businessDetails .= "\r\n<br><br>" . $business->address;

        $html = "<div class='invoice-box'>";


        if (is_null($name)) {
            $name = $offerLetter->getCustomerDetails();
        }


        $html .= "<div style='line-height: 0.7cm ; line-break: anywhere; font-size: 12px ; font-family:  'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif'>
        <div style='text-align: center'><b>RE: " . strtoupper($offerLetter->getCustomerPlots()) . "</b></div>
        We are pleased to make an offer on the above parcel subject to the following terms and conditions:<br/>";

        $html .= "<ol>
                              <li><b>Vendor:</b> We are pleased to make an offer on the above parcel subject to the following terms and conditions </li>
                              <li><b>Purchaser:</b> {$offerLetter->getCustomerDetailsForAgreement()} <br/>
                              <li><b>Property:</b> {$offerLetter->getPropertyDetails()}</li>
                              <li><b>Plot Selected:</b> {$offerLetter->getPlotNos()}</li>
                               <span style='font-size: small; color: -moz-dragtargetzone'>Method of payment <b>{$offerLetter->payment_option} </b></span> </li>
                               <li><b>Price  : </b>
                              	    Cash Option Kes.<b>  " . number_format((double)$offerLetter->cash_option, 2) . "/= only </b><br/>
                                    Installment Option	Kes.<b> " . number_format((double)$offerLetter->installment_option, 2) . "/= </b>only<br/>
                                    <i style='font-size: small'>Reservation Amount In Words:<b>" . Helper::numberToWord((double)$offerLetter->intialPayment()) . "</b> </i><br/>
                                    <i style='font-size: small'>Reservation Amount In Figures:<b>" . number_format((double)$offerLetter->intialPayment(), 2) . " Shillings</b></i>
                                          	  
                                           	  </li>
                              <li><b>Reservation Terms:</b>	Payment should be done within 14 days from the date of booking. In the event that payment is not received within the stipulated period or the purchaser opts to terminate the transaction for whatever reason, the Company will be at liberty to resell the property without any reference or notice to the purchaser and subsequently deduct 20% of the money paid being administrative cost</li>
                              <li><b>Refund Terms:</b> 	The Company will require the purchaser to make a formal refund request by filling in a client refund form. The Company will process the refunds two (2) months when the request was made, payable by the last Friday of every month. If the refund request was made before the elapse of the period stipulated in “Reservation Terms”, the amount will be refunded in full.</li>
                              <li><b>Confirmation:</b>	Kindly confirm your acceptance of this offer by signing the duplicate copies of this offer and returning the same to us.</li>";


        if ($offerLetter->note) {
            $html .= "<li>" . $offerLetter->note . "</li>";

        }
        $html .= " </ol>";

        $html .= "<p style='line-height: 0.7cm;'>
              <b>I {$offerLetter->getCustomerDetails()}  </b> confirm my / Our acceptance of the above terms and conditions.";

        if ($offerLetter->nextOfKin) {
            $html .= "<br/>Next of kin: 
      Name:&nbsp;   <b>" . $offerLetter->nextOfKin->full_name . "</b>          &nbsp;  &nbsp;  &nbsp;  &nbsp;  &nbsp;  &nbsp;        &nbsp;  &nbsp;  &nbsp;  &nbsp;  &nbsp;  &nbsp;        Phone:    &nbsp;  <b> " . $offerLetter->nextOfKin->phone_number . " </b>  &nbsp;  &nbsp;  &nbsp;  &nbsp;  &nbsp;  &nbsp;                        Relation:&nbsp; <b>" . $offerLetter->nextOfKin->relation . ". </b>";
        }
        $html .= "</div></div>";


        if ($print) {

            $html .= "  <br/>Signed: .............................................                                 Date:  " . Carbon::now()->format('Y-m-d : H:i:s') . "
       <br/><img src=" . asset('assets/img/facebook.png') . " width='35px' height='35px'> @Fanaka REAL Estate Ltd 
       <img src=" . asset('assets/img/intagram.png') . " width='35px' height='35px'> Fanakaltd 
       <img src=" . asset('assets/img/twitter.png') . " width='35px' height='35px'> @FanakaLtd
       </p>";

            return $html;
        }


        $pdf = new Mpdf();

        $pdf->SetImportUse();

        $pdf->default_font = "tohama";

        $pdf->default_font_size = "2";


        $pdf->WriteHTML($this->style());

        $pdf->WriteHTML($html);


        if ($print) {
            $pdf->showWatermarkImage = true;

            $pdf->SetWatermarkImage($logo);


            $pdf->SetHTMLHeader("<table border='0' class='no-border' style='border-style: none'>
                    <thead>
                        <tr>
                       
                        <th style='text-align: center ; border-style: none' ><br/><small><img src=" . $logo . " style='width: 100%;height: auto; max-height: 70px'> </small> <br><br><br> <br/> <br/> <br/></th>
                        <th style='text-align: center ; border-style: none'><small> {$this->businessDetails}</small></th>
                       
                        </tr>
                    </thead>
                   
                    </table>");


            $pdf->SetTopMargin(50);

            $pdf->SetHTMLFooter('{DATE j-m-Y}  printed By  ' . auth()->user()->fullName());
        }

        $pdf->Output(str_slug($name) . ".pdf", $output);
    }

    public function style()
    {
        return "   <!--mpdf
    <htmlpageheader name=\"letterheader\">
        <table width=\"100%\" style=\" font-family: sans-serif;\">
            <tr>
                <td width=\"25%\" >
                    <span style=\"font-size: 12pt;\">
                    ' . ucwords($this->businessDetails) . '
                    </span>
                    </td>
                    <td width=\"25%\" >
                    <span style=\"font-weight: bold; font-size: 12pt;\">
                    <img src=\"' . $this->logo . '\"style=\"width: 100%;height: auto; max-height: 70px\"alt=\"Fanaka Real Estate\"/>
                    </span>
                    </td>
                <td width=\"25%\" style=\"text-align: right; vertical-align: top;\">
                  
                    
                </td>
            </tr>
        </table>
        
        <div style=\"margin-top: 1cm; text-align: right; font-family: sans-serif;\">
            {DATE jS F Y}
        </div>
    </htmlpageheader>
    
    <htmlpagefooter name=\"letterfooter2\">
        <div style=\"border-top: 1px solid #000000; font-size: 9pt; text-align: center; padding-top: 3mm; font-family: sans-serif; \">
            Page {PAGENO} of {nbpg}
        </div>
    </htmlpagefooter>
mpdf-->
                       <style>
        .invoice-box {
         width: 100%;
        font-size: 16px;
        line-height: 200%;
        text-align: justify-all;
        font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;
      
    }
     table {
             border-collapse: collapse;
             width: 100%;
             border: solid 1px;
             font-size: 10px;
          }
          table.offer-body{
              border-collapse: collapse;
             width: 100%;
             border: solid 1px;
             font-size: 14px;
          }
          th, td {
              text-align: left;
              padding: 8px;
              border: solid 1px;
          }
                                    
           table.table-bordered {                                               
              border: solid 1px;
          }
                                    
     tr:nth-child(even){
      
      background-color: lightblue;

      }
      table.offer-body,tr:nth-child(even){
      
      background-color: none;

      }
      tr:nth-child(odd){
      
      background-color: ;

      }
      table.table-bordered , th, td {
   border: 1px solid black;
   table-border-color-light: #0b2e13;
}

    .invoice-box table {
        width: 100%;
        text-align: left;
    }
    
    .no-border {
    border-style: none;
    }
    
    .invoice-box table td {
        
        vertical-align: top;
    }
    
    .invoice-box table tr td:nth-child(2) {
        text-align: right;
    }
    
    .invoice-box table tr.top table td {
        padding-bottom: 20px;
    }
    
    .invoice-box table tr.top table td.title {
        font-size: 45px;
        line-height: 45px;
        color: #333;
         text-align: left;
    }
    
    .invoice-box table tr.information table td {
        padding-bottom: 40px;
    }
    
    .invoice-box table tr.heading td {
      
        border-bottom: 1px solid #ddd;
        font-weight: bold;
    }
    
    .invoice-box table tr.details td {
        padding-bottom: 20px;
    }
    
    .invoice-box table tr.item td{
        border-bottom: 1px solid #eee;
    }
    
    .invoice-box table tr.item.last td {
        border-bottom: none;
    }
    
    .invoice-box table tr.total td:nth-child(2) {
        border-top: 2px solid ;
        font-weight: bold;
    }
    
    tr:nth-child(even){
             

         }

         th {
              
             color: black;
             border:solid 1px black;
             text-transform: uppercase;
          }
    
    @media only screen and (max-width: 600px) {
        .invoice-box table tr.top table td {
            width: 100%;
            display: block;
            text-align: center;
        }
        
        .invoice-box table tr.information table td {
            width: 100%;
            display: block;
            text-align: center;
        }
    }
    
    /** RTL **/
    .rtl {
        direction: rtl;
        font-family: Tahoma, 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;
    }
    
    .rtl table {
        text-align: right;
    }
    
    .rtl table tr td:nth-child(2) {
        text-align: left;
    }
                </style>";


    }

}
