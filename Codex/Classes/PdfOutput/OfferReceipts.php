<?php
/**
 * Created by Philip Njuguna.
 * User: Philip Njuguna
 * Date: 11/8/18
 * Time: 1:46 PM
 */

namespace Codex\Classes\PdfOutput;


use App\DB\Sale\OfferLetter;
use Carbon\Carbon;
use Codex\Contracts\PdfOutput;
use Illuminate\Support\Collection;

class OfferReceipts extends GeneratePdfAbstract implements PdfOutput
{


    private  $customer;
    private  $sale;

    public function outPut(Collection $offerLetter)
    {
        $this->setHeader($offerLetter);
        $this->setContent($offerLetter);

        return $this->generate();

    }

    /**
     * @return $this
     */
    public function setHeader($offerLetter)
    {
        $sale = $offerLetter->first();
        $this->sale = $sale;

        $header = $sale->getCustomerDetails();

        $this->customer = $sale->customers;

        $this->pdfTitle = $this->sale->getCustomerDetails();

        $header .= "<br/>\r\n" . $this->sale->contactPoint() . "<br/>";
        $header .= "\r\n Offer Reference FOFR_" . $sale->id . "<br/>";

        $this->header = $header;

        $business = auth()->user()->business;

        $businessHeader = '<span style="font-weight: bold; ">' . $business->name . "</span>";
        $businessHeader .= "<br/>" . $business->address . "<br/>";
        $businessHeader .= "<br/>" . $business->slug . "<br/>";

        $this->headerBusiness = $businessHeader;

        $this->footer = "You Were served By ". auth()->user()->fullName() ." On: ". Carbon::now()->format('Y-m-d');

        return $this;

    }


    public function setContent($collection)
    {
        $this->content = "";


        return $this;

    }

    public function content()
    {

        $plotNo = [];
        $plotSize = [];


        foreach ($this->sale->offerItems as $saleItem) {

            $plotNo[] =  $saleItem->plot->getPropertyDetails() ;
            $plotSize[] = $saleItem->plot->size->label;

        }


        $balance = $this->sale->total_amount - $this->sale->offerPayments->sum('amount');

        $html = '<div class="invoice-box">

<p>Plot No : '.implode($plotNo,' , ').'  Total Amount ' . config('config.currency'). ' '  .number_format((double) $this->sale->getTotalOfferAmount() , 2).'</p>
        <table cellpadding="0" cellspacing="0">
           
            
            <tr class="heading">
                <td>
                    Payment Method
                </td>
                
                <td>
                    Transaction Code 
                </td>
                
                <td>
                    Amount 
                </td>
                
                <td>
                    Date 
                </td>
            </tr>';


        foreach ($this->sale->offerPayments as $salePayment) {


            $html .= '<tr class="item">
                <td>
                    '.$salePayment->payment_method.'
                </td> 
                <td>
                    '.$salePayment->reference_code.'
                </td>
                
                <td>
                    ' .config('config.currency') .' '.number_format($salePayment->amount , 2).'
                </td>
                
                <td>
                    '.  $salePayment->deposit_date.'
                </td>
                
            </tr>';

        }



        $html .= '<tr class="total">
                  <td>Total Paid: </td>            
                <td>
                    '. config('config.currency'). '  '. number_format($this->sale->offerPayments->sum('amount') , 2).'
                </td>
                
        </table>
    </div>';


        /**
         * </tr>
        <tr class="total">
        <td>Balance: </td>
        <td>'.number_format($this->sale->getBalance() , 2).'</td>
        </tr>
        */

        return $html;



    }


    protected function style()
    {
        return "   <table>
                    <thead>
                        <tr>
                        <th width=\"25%\" style='color: #000'><small>{$this->headerBusiness}</small></th>
                        <th width=\"25%\" style='color: #000'><img src=".$this->logo." style='width: 100%;height: auto; max-height: 70px'><br/><small>Offer Receipt</small></th>
                        <th width=\"25%\" style='color: #000;'><small>{$this->header}</small></th>
                        </tr>
                    </thead>
                   
                    </table>
                       <style>
        .invoice-box {
         width: 100%;
        font-size: 16px;
        line-height: 24px;
        font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;
      
    }
     table {
     border-collapse: collapse;
      width: 100%;
      color: #000;
       }

                            th, td {
                                     text-align: left;
                                     padding: 8px;
                                     color: #000;
    
                                    }
                                    
     tr:nth-child(even){
      

      }
    .invoice-box table {
        width: 100%;
        text-align: left;
    }
    
    
    .invoice-box table td {
        
        vertical-align: top;
    }
    
    .invoice-box table tr td:nth-child(2) {
        text-align: right;
    }
    
    .invoice-box table tr.top table td {
        padding-bottom: 20px;
    }
    
    .invoice-box table tr.top table td.title {
        font-size: 45px;
        line-height: 45px;
        color: #333;
         text-align: left;
    }
    
    .invoice-box table tr.information table td {
        padding-bottom: 40px;
    }
    
    .invoice-box table tr.heading td {
      
        border-bottom: 1px solid #ddd;
        font-weight: bold;
    }
    
    .invoice-box table tr.details td {
        padding-bottom: 20px;
    }
    
    .invoice-box table tr.item td{
        border-bottom: 1px solid #eee;
    }
    
    .invoice-box table tr.item.last td {
        border-bottom: none;
    }
    
    .invoice-box table tr.total td:nth-child(2) {
        border-top: 2px solid ;
        font-weight: bold;
    }
    
    tr:nth-child(even){
             

         }

         th {
              
             color: lightblue;
             text-transform:capitalize;
          }
    
    @media only screen and (max-width: 600px) {
        .invoice-box table tr.top table td {
            width: 100%;
            display: block;
            text-align: center;
        }
        
        .invoice-box table tr.information table td {
            width: 100%;
            display: block;
            text-align: center;
        }
    }
    
    /** RTL **/
    .rtl {
        direction: rtl;
        font-family: Tahoma, 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;
    }
    
    .rtl table {
        text-align: right;
    }
    
    .rtl table tr td:nth-child(2) {
        text-align: left;
    }
                </style>";



    }


}
