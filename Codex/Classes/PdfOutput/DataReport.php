<?php
/**
 * Created by PhpStorm.
 * User: philip
 * Date: 3/23/18
 * Time: 9:43 AM
 */

namespace Codex\Classes\PdfOutput;


use App\DB\Role\Business;
use Carbon\Carbon;
use Codex\Contracts\PdfOutput;
use Codex\Contracts\ReportOutput;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Route;
use Mpdf\Mpdf;

/**
 * @property string logo
 */
class DataReport
{

    public $businessDetails;


    private $name;

    private  $pdf ;

    public function __construct(array $design = null)
    {
        if (! is_null($design))
        {
            $this->pdf = new Mpdf([
                'orientation' => $design['orientation']
            ]);
        }
        else{

            $this->pdf = new Mpdf();
        }
    }


    public function outPut($html, $name = "new_report" , $outPut = \Mpdf\Output\Destination::INLINE , $showPreparedBy = true)
    {

        $this->name = $name;

        $logo = asset('assets/img/fanaka_logo.png');

        $this->logo = $logo;

        $business = Business::first();

        $this->businessDetails = $business->name;
        $this->businessDetails .= "\r\n<br>".$business->address;

        $this->businessDetails = ucwords(strtolower($this->businessDetails));


        $content = '
<style>hr{ background: #00ad9c}</style>
<htmlpageheader name="MyHeader1">
    <div style="text-align: center ; !important; ">
   
     <table border="0" class="no-border" style="border-style: none; margin-top: 200px">
                    <thead>
                        <tr>
                        <th  style="text-align: left ; border-style: none" >  <img src="'.$this->logo.'"  style="width: 100%;height: auto; max-height: 70px"></th>
                       <th style="border: none"></th>
                       <th style="border: none">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                       <th style="border: none">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                       <th style="border: none">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                       <th style="border: none">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                        <th style="text-align: justify; margin-right: 0px  ;position:relative;  border-style: none" ><span style="text-align: justify">'. fanakaConfig('vendor').'</span> </th>
                        
                       
                        </tr>
                    </thead>
                   
                    </table>
   <hr/>
   </div>
   
</htmlpageheader>

<htmlpagefooter name="MyFooter1">
    {DATE j-m-Y}  (Page: {PAGENO}/{nbpg})  printed By ' .auth()->user()->fullName() .' <hr/>
</htmlpagefooter>

<sethtmlpageheader name="MyHeader1" value="on" show-this-page="1" />
<sethtmlpagefooter name="MyFooter1" value="on" />

<div>'.$html.'</div>';






        $pdf = $this->pdf;


        //$pdf->SetImportUse();

        $pdf->default_font= "tohama";

        $pdf->default_font_size = "2";

        $pdf->showWatermarkImage = true;

        $pdf->SetWatermarkImage($logo);


        $pdf->SetTopMargin(35);


        $pdf->WriteHTML($this->style());

        $pdf->WriteHTML($content);

         if ($showPreparedBy)
         {
             $pdf->WriteHTML($this->signatories());
         }



        //$pdf->WriteHTML(Carbon::now()->format('Y-M-d H:i:s').'  printed By  ' .auth()->user()->fullName());


       // $pdf->Output();
        $pdf->Output($name.'.pdf',$outPut);

    }


    public function emailOutPut($html, $name = "new_report" , $outPut = "D")
    {
        $logo = asset('assets/img/fanaka_logo.png');

        $this->logo = $logo;

        $business = Business::first();

        $this->businessDetails = $business->name;
        $this->businessDetails .= "\r\n<br>".$business->address;

        $content = $html;




        $content = '
<htmlpageheader name="MyHeader1">
    <div style="text-align: center ; margin-top: 200px !important;">
   
     <table border="0" class="no-border" style="border-style: none; margin-top: 200px">
                    <thead>
                        <tr>
                        <th  style="text-align: left ; border-style: none" >  <img src="'.$this->logo.'"  style="width: 100%;height: auto; max-height: 70px"></th>
                       
                        <th style="text-align:right; margin-right: 0px  ;position:relative;  border-style: none" >'.$this->name.' <br/>
                        <br/><span style="text-align: justify   "> '.$this->businessDetails .'</span><br/> <br/> <br/></th>
                        
                       
                        </tr>
                    </thead>
                   
                    </table>
   
   </div>
</htmlpageheader>

<htmlpagefooter name="MyFooter1">
    {DATE j-m-Y}  ({PAGENO}/{nbpg})  printed By' .auth()->user()->fullName() .'
</htmlpagefooter>

<sethtmlpageheader name="MyHeader1" value="on" show-this-page="1" />
<sethtmlpagefooter name="MyFooter1" value="on" />

<div>'.$html.'</div>';






        $pdf = new Mpdf();



        $pdf->default_font= "tohama";

        $pdf->default_font_size = "2";

        $pdf->showWatermarkImage = true;

        $pdf->SetWatermarkImage($logo);

        $pdf->SetTopMargin(40);

        $pdf->WriteHTML($this->style());

        $pdf->WriteHTML($content);




        $pdf->SetHTMLFooter(now()->format('Y-m-d H:i') .' printed By  ' .auth()->user()->fullName() ,'DOUBLE-SIDED');

        //$url =  url('');

        $pdf->Output($name.'.pdf',$outPut);

    }


    private function signatories()
    {
        $html = "";
        $html .= "<br/>";
        $html .= "<i>Prepared By:  ...............................................................</i>";
        $html .= "<br/>";
        $html .= "<i>Sign:  ...............................................................</i>";
        $html .= "<br/>";
        $html .= "<i>Date:  ...............................................................</i>";

        $html .= "<br/>";
        $html .= "<br/>";
        $html .= "<i>Authorised By:  ...............................................................</i>";
        $html .= "<br/>";
        $html .= "<i>Sign:  ...............................................................</i>";
        $html .= "<br/>";
        $html .= "<i>Date:  ...............................................................</i>";
        $html .= "<br/>";

        return $html;

    }





    protected function style()
    {
        $header = '';

        return " {$header}  
                       <style>
        .invoice-box {
         width: 100%;
        font-size: 16px;
        line-height: 24px;
        font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;
      
    }
     table {
                                    border-collapse: collapse;
                                    width: 100%;
                                    border: solid 1px;
                                    font-size: 10px
                                    ;
                                   }

                            th, td {
                                     text-align: left;
                                     padding: 8px;
                                     border: solid 1px;
    
                                    }
                                    table.table-bordered {
                                    
                                    border: solid 1px;
                                    }
                                    
     tr:nth-child(even){
      
      background-color: lightblue;

      }
      tr:nth-child(odd){
      
      background-color: ;

      }
      table.table-bordered , th, td {
   border: 1px solid black;
   table-border-color-light: #0b2e13;
}

    .invoice-box table {
        width: 100%;
        text-align: left;
    }
    
    .no-border {
    border-style: none;
    }
    
    .invoice-box table td {
        
        vertical-align: top;
    }
    
    .invoice-box table tr td:nth-child(2) {
        text-align: right;
    }
    
    .invoice-box table tr.top table td {
        padding-bottom: 20px;
    }
    
    .invoice-box table tr.top table td.title {
        font-size: 45px;
        line-height: 45px;
        color: #333;
         text-align: left;
    }
    
    .invoice-box table tr.information table td {
        padding-bottom: 40px;
    }
    
    .invoice-box table tr.heading td {
      
        border-bottom: 1px solid #ddd;
        font-weight: bold;
    }
    
    .invoice-box table tr.details td {
        padding-bottom: 20px;
    }
    
    .invoice-box table tr.item td{
        border-bottom: 1px solid #eee;
    }
    
    .invoice-box table tr.item.last td {
        border-bottom: none;
    }
    
    .invoice-box table tr.total td:nth-child(2) {
        border-top: 2px solid ;
        font-weight: bold;
    }
    
    tr:nth-child(even){
             

         }

         th {
              
             color: black;
             border:solid 1px black;
             text-transform: uppercase;
          }
    
    @media only screen and (max-width: 600px) {
        .invoice-box table tr.top table td {
            width: 100%;
            display: block;
            text-align: center;
        }
        
        .invoice-box table tr.information table td {
            width: 100%;
            display: block;
            text-align: center;
        }
    }
    
    /** RTL **/
    .rtl {
        direction: rtl;
        font-family: Tahoma, 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;
    }
    
    .rtl table {
        text-align: right;
    }
    
    .rtl table tr td:nth-child(2) {
        text-align: left;
    }
                </style>";



    }
}
