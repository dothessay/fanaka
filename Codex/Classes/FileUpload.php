<?php


namespace Codex\Classes;


use Illuminate\Http\Request;
use Intervention\Image\Image;

class FileUpload
{

    public static function  uploadFile(Request $request, $directoryTarget , $field = 'image', $imagePrefix = 'fanaka-real-estate-')
    {

        if($request->file($field) || $request->file($field))
        {
            try{


                $path = $request->file($field)->store('agreements');



                //$ext = $image->getClientOriginalExtension();


                //$imageName =  str_slug($imagePrefix.(time())).'.'.$ext ;


                //$filename = $path.$imageName;

                //file_put_contents($filename, $request->file($field));

                //$newPth = $request->file($field)->store($path);

                //dd(move_uploaded_file($_FILES[$field]["tmp_name"],$filename) , $newPth);


                return $path;
            }
            catch (\Exception $exception)
            {
               return true;
            }


        }else{

            return null;
        }

    }


    public static function uploadBase64Image(Request $request, $directoryTarget, $imagePrefix = 'customer-')
    {

        try{

            $path = "uploads" . DIRECTORY_SEPARATOR . "{$directoryTarget}/";

            $ext = "png";

            $imageName = $imagePrefix . (time()) . '.' . $ext;


            $path = $path . $imageName;

            $data = $request['image'];

            $data = utf8_encode ( $data );


            $newData = str_replace("*;charset=utf-8","",$data);
            list($type, $newData) = explode(';', $newData);
            list(, $newData)      = explode(',', $newData);
            $data = base64_decode($newData);



            /*$imageMade = Image::make($data)
                ->resize(220, 350)
                ->save($path);*/

            file_put_contents($path, $data);


            return $path;
        }catch (\Exception $e){

            return $e->getMessage();
        }

    }

}
