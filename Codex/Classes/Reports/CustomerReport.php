<?php


namespace Codex\Classes\Reports;

use Carbon\Carbon;

class CustomerReport
{
    public $request;
    public  $result;
    public $customers;
    public  $sales;
    function __construct()
    {
        $this->request = request();
    }

    public function output()
    {
        $this->allCustomers()
            ->sales()
            ->percentage()
            ->dateBetween()
            ->agent()
            ->hasCount()
            ->byProject();


        return $this->result = $this->sales;
    }

    public function hasCount()
    {
        // count greater than one

        $sales = collect();
        $count = [];
        if ($this->request->has('count') && floatval($this->request->query('count')) > 1)
        {

            foreach ($this->sales as $sale) {

                $count[$sale->pivot->customer_id][] = $sale;

                if (sizeof($count[$sale->pivot->customer_id]) >=  floatval($this->request->query('count')))
                {
                    try{
                        for ($i = 0 ; $i <= sizeof($count[$sale->pivot->customer_id]) ; $i++)
                        {
                            $sales->push($count[$sale->pivot->customer_id][$i]);
                        }
                    }catch (\Exception $exception)
                    {

                    }
                }


            }
            $this->sales =  $sales->unique('id');

        }
        return $this;
    }

    /**
     * @return CustomerReport
     */
    private function byProject(): CustomerReport
    {
        // has to have project in request

        if ($this->request->has('project_id') && $this->request['project_id'] != null)
        {
            $sales = collect();

            $this->sales->each(function ($sale)  use ($sales)
            {
                $sale->saleItems->where('product_id', $this->request->query('project_id'))
                     ->each(function ($item) use ($sales) {

                         $sales->push($item->sale);
                     });

            });

            $this->sales = $sales;

        }

        return $this;
    }

    private function percentage()
    {
        $sales = collect();

        if ($this->request->has('percentage') && ! is_null($this->request['percentage']))
        {
            $this->sales->each(function ($sale)  use ( $sales ){

                if ($this->request['percentage'] == 'below-30') {

                    if ($sale->getPercentage() < 30) {

                         $sales->push($sale);
                    }
                }
                if ($this->request['percentage'] == 'above-30') {

                    if ($sale->getPercentage() >= 30) {

                         $sales->push($sale);
                    }
                }

                 $this->sales = $sales;
            });
            return $this;

        }
        return $this;
    }

    private function agent() : CustomerReport
    {
        if ($this->request->has('agent') && $this->request['agent'] != null)
        {
            $this->sales =  $this->sales->where('agent',  $this->request['agent']);
        }

        return $this;

    }

    private function allCustomers()
    {
        if (! $this->customers)
        {
            $this->customers =  $this->request->user()->customers();
            if ($this->request->has('customer_id')){
                // get customers in array
                collect($this->request->query('customer_id'))->each(function ($customer){
                   if ($customer != 'all'){
                       return $this->customers = $this->request->user()->customers()->where('id', $customer);
                   }
                });
            }
        }


        return $this;

    }

    private function sales()
    {
        $sales = collect();
        $this->customers->each(function ($customer) use ($sales) {
            return $customer->sales->each(function ($sale) use ($sales) {
                return $sales->push($sale);
            });
        });
        $this->sales = $sales;

        return $this;
    }

    private function dateBetween()
    {

        $this->sales =  $this->sales->whereBetween('created_at', [
            Carbon::parse($this->request['start_date']) , Carbon::parse($this->request['end_date'])
        ]);

         return $this;
    }


}
