<?php
/**
 * Created by PhpStorm.
 * User: philip
 * Date: 3/1/19
 * Time: 9:43 AM
 */

namespace Codex\Classes\Reports\Sales;


use App\Sale\Sale;
use Carbon\Carbon;
use Illuminate\Support\Collection;

trait SaleReportGenerator
{


    public function seeOnlyMine()
    {
        
    }


    public function hasAboveHalf(Collection $sales, array $dataRage )
    {



        return $sales
            ->where('half_payment_at','>', Carbon::parse($dataRage['start_date'])->startOfDay())
            ->where('half_payment_at','<', Carbon::parse($dataRage['end_date'])->endOfDay());

    }



    public function percentage(Sale $sale , $percentage = ">")
    {

        if ($percentage == ">")
        {
            if ($sale->getPercentage() >= 50 && $sale->getPercentage() < 100)
            {
                return $sale;
            }

        }

        return $sale;

    }
}