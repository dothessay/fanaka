<?php


namespace Codex\Classes\Reports\Sales;


use App\Sale\Sale;
use App\Sale\SalePayment;
use App\User;
use Carbon\Carbon;

class Income
{
    public function sales()
    {
        $request = request();
        $request['start_date'] = $request->has('start_date') ? Carbon::parse($request['start_date'] )->subRealMonth()->format('Y-m-25') : Carbon::now()->startOfMonth();
        $request['end_date'] = $request->has('end_date')  ? Carbon::parse($request['end_date'] )->format('Y-m-25'): Carbon::now()->endOfMonth();
        $where = [];
        if ($request->has('agentId'))
        {
            $where = ['agent'  => $request['agentId']];
        }
        $payments = Sale::where($where)->where('sales.commission_at', null)
            ->join('sale_payments', 'sales.id', '=', 'sale_payments.sale_id')
            ->whereBetween('sale_payments.created_at', [
                Carbon::parse($request['start_date']),
                Carbon::parse($request['end_date']),
            ])
            ->select('sales.*', 'sale_payments.amount','sale_payments.created_at as date','sale_payments.deposit_date as deposit_date')
            ->orderBy('date','DESC')
            ->get();
        return $payments;

    }

    public function all($sales)
    {
        $agent = [];
        $sales = $sales->groupBy(function ($sale) { return  $sale->agent ; });
        foreach ($sales as $index => $sale)
        {
            $marketer =User::find($index);
            $agent[$index] = [
                    'name' => $marketer->fullName() ,
                    'amount' => ceil( $sale->sum('amount') ),
                    'id' => $index
           ];
        }
        return $agent;
    }
    public function single($sales)
    {
        $agent = [];
        $sales = $sales->groupBy(function ($sale) { return  Carbon::parse($sale->date)->format('Y-F') ; });
        foreach ($sales as $index => $payments) {
            foreach ($payments as $payment) {
                $user = $payment->soldBy;
                $agent[$index] = [
                    'name' =>  $user->fullName() ,
                    'amount' =>  ceil($payments->sum('amount')) ,
                    'id' => $user,
                ];
            }
        }

        return $agent;
    }

}