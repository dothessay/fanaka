<?php


namespace Codex\Classes\Reports\Sales\Perfomance;


use App\Sale\Sale;
use Carbon\Carbon;
use Codex\Scopes\MySales;

class Sales
{
    public function builder()
    {
        return Sale::query();
    }

    public function all()
    {

        return $this->builder()->get();

    }

    public function isMonthlyInstalmentPaid(Sale $sale , $month)
    {

       return $sale->salePayments()->whereBetween('created_at', [
            Carbon::parse($month)->startOfMonth(),
            Carbon::parse($month)->endOfMonth(),
        ])->sum('amount');

    }
}