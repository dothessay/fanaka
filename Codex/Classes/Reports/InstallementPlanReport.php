<?php


namespace Codex\Classes\Reports;


use App\DB\Sale\MonthlyInstallmentPlan;
use App\Sale\SalePayment;
use Carbon\Carbon;
use Codex\Classes\Repository\MonthlyInstallmentPrepaid;
use Illuminate\Http\Request;

class InstallementPlanReport
{
    private $sale;

    private $startDate;
    private $endDate;


    public function plan()
    {
        $request = request();


        $startDate = Carbon::parse($this->startDate);
        $endDate = Carbon::parse($this->endDate);


        $invoices = $this->getPlans();

        $plans = collect();

        foreach ($invoices as $invoice) {
            if (isset($invoice->sale) && $invoice->sale->getBalance() > 0) {

                if (Carbon::parse($invoice->payment_date)->between($startDate, $endDate) && ! $invoice->sale->lastPaymentDate()->isCurrentMonth())
                {
                    // if agent viewing

                    if (! $request->user()->can('access-module-component-functionality', 'sale_manage_show-all-sales') && !  $request->user()->departmentAdmin)
                    {

                        if ($invoice->sale->agent === $request->user()->id)
                        {


                            $plans->push($invoice);
                        }
                    }

                    // if COD Is viewing
                    elseif ($request->user()->departmentAdmin)
                    {
                        $usersIds = $request->user()->departmentAdmin->role->users->pluck('id')->toArray();

                        if (in_array($invoice->sale->agent, $usersIds))
                        {
                            $plans->push($invoice);

                        }
                    }
                    // if admin is viewing
                    else
                    {
                        $plans->push($invoice);

                    }
                }
            }
        }


        return $plans;
    }

    /**
     * @param mixed $endDate
     *  * @return InstallementPlanReport
     */
    public function setEndDate($endDate) : InstallementPlanReport
    {
        $this->endDate = $endDate;

        return $this;
    }

    /**
     * @param mixed $startDate
     * @return InstallementPlanReport
     */
    public function setStartDate($startDate)
    {
        $this->startDate = $startDate;

        return $this;
    }

    private function getPlans()
    {
        $request = request();


        $startDate = Carbon::parse($this->startDate);
        $endDate = Carbon::parse($this->endDate);


        $invoices = MonthlyInstallmentPlan::where('is_paid', false)
            ->whereBetween('month', [
                $startDate->format('Y-m'),
                $endDate->format('Y-m'),
            ])->get();


        return $invoices;
    }


}
