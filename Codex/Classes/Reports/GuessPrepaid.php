<?php


namespace Codex\Classes\Reports;


use App\Sale\Sale;
use Carbon\Carbon;

class GuessPrepaid
{

    private static $instance;

    /**
     * @var Sale $sale
    */

    private $sale;

    /**
     * GuessPrepaid constructor.
     * @param Sale $sale
     */

    public function __construct(Sale $sale)
    {
        $this->sale = $sale;

    }


    public static function __callStatic($name, $arguments)
    {

       if ( ! method_exists($name))
       {
           throw new \Exception("Method not found");
       }
       return (new self())->$name($arguments);
    }


    public static function getInstance(?Sale $sale)
    {
        // Check is $_instance has been set
        if(!isset(self::$instance))
        {
            // Creates sets object to instance
            self::$instance = new static( $sale);
        }

        // Returns the instance
        return self::$instance;
    }


    /**
     * @return bool
     */
    public function guessHasPrePaid() : bool
    {
        if ($this->getDifferencePaidAmount() < 1)
        {
            return false;
        }
        return true;
        
    }

    /**
     * @return bool
     */
    private function hasPaidRequiredPercentage(): bool
    {
        if ($this->sale->getPercentage() >= fanakaConfig('commission_percentage', 30))
        {
            return true;

        }
        return false;

    }

    public function getRequiredPercentage()
    {
        return ( floatval($this->sale->getTotalSaleAmount()) * ( floatval(fanakaConfig('commission_percentage', 30)) / 100));
    }
    /**
     * @return int
     */
    public function getMonthlyPlanAmount(): int
    {

        $balance  = $this->sale->getTotalSaleAmount() - ($this->sale->getTotalSaleAmount() * 0.7);

        //$amountToBePaidAfterRequiredPercentage = floatval($this->sale->getTotalSaleAmount()) - ( floatval($this->sale->getTotalSaleAmount()) * ( floatval(fanakaConfig('commission_percentage', 30)) / 100));

        try {
            return (int)floatval($balance) / ($this->sale->getInstalmentMonths() - 1);
        }
        catch (\Exception $exception)
        {
            return  $balance;
        }

    }

    /**
     * @return int
     */
    public function getRemainingMonths() : int
    {
        $finishDate = $this->sale->created_at->addRealMonths($this->sale->getInstalmentMonths());
        $today = now();
        return Carbon::parse($finishDate)->diffInMonths($today);
    }

    /**
     * @return int
     */
    public function getPassedMonths() : int
    {
        return $this->sale->getInstalmentMonths() - $this->getRemainingMonths();
    }


    /**
     * @return float|int
     */
    public function exceptedAmountToBePaidByNow()
    {
       // dd($this->sale->guessPrepaid()->getMonthlyPlanAmount() , $this->getMonthlyPlanAmount());

        return  $this->getRequiredPercentage() + ($this->getMonthlyPlanAmount() * ($this->getPassedMonths() - 1));

    }
    /**
     * @return float|int
     */
    public function exceptedAmountToBePaidInMonth($months , $amount)
    {
        return  $this->getRequiredPercentage() ;

    }

    /**
     * @return float|int|mixed
     */
    public function getDifferencePaidAmount()
    {


        return  $this->sale->getPaidAmount() - ($this->getRequiredPercentage() + ($this->getMonthlyPlanAmount() * ($this->getPassedMonths() - 1))) ;
    }
    /**
     * @return float|int|mixed
     */
    public function getPrePaidAmount()
    {
        if ($this->guessHasPrePaid())
        {
            return  $this->sale->getPrepaidAmount();
        }
        return  0;
    }

    /**
     * @return Sale
     */
    public function getSale(): Sale
    {
        return $this->sale;
    }

}
