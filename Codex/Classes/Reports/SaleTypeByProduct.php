<?php


namespace Codex\Classes\Reports;


use Carbon\Carbon;
use Codex\Classes\Handlers\Exports\ExportSaleType;
use Codex\Classes\PdfOutput\DataReport;
use Codex\Classes\Repository\ProjectRepository;
use Codex\Classes\Repository\SaleItemRepository;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

class SaleTypeByProduct
{

    public function all(Request $request)
    {
        ini_set('max_execution_time', 50000);

        if ($request->has('product_id')  && $request['product_id'] === 'all')
        {
            $request['sale_type'] = isset($request['sale_type']) ? $request['sale_type'] : "Instalment";

            //$request['with_balance'] = $request->has('with_balance') ? $request['with_balance'] :  true;

            $request->request->remove('product_id');

        }
        $sales = collect();
        $saleItems = (new SaleItemRepository())
            ->all($request);

        foreach ($saleItems as $saleItem) {

            if (\request()->has('sale_type')) {
                if (strtolower($saleItem->sale_type) === strtolower($request['sale_type'])) {


                    if (strtolower($saleItem->sale_type === 'cash') && $saleItem->sale->installmentPlans->count() < 2) {
                        $sales->push($saleItem->sale);
                    }
                    else{

                        $sales->push($saleItem->sale);
                    }
                }
            }

        }

        if ($request->has('with_balance')) {
            $sales = $sales->filter(function ($sale) {
                if (($sale->getBalance() > floatval(1))) {
                    return $sale;
                }
            });

        }

        if ($request->has('pass_month')) {
            $createdAt = now()->subRealMonths(1);

            $sales = $sales->filter(function ($sale) use ($createdAt) {
                if (Carbon::parse($sale->created_at)->lessThan($createdAt)) {

                    return $sale;
                }
            });
        }

        $sales = $sales->unique('id')->sortByDesc('created_at');

        if ($request->has('pdf')) {

            $html = view('sales.report.project.data')->with([
                'sales' => $sales
            ])->render();

           return (new DataReport())->outPut($html, $request->sale_type . ' Report', \Mpdf\Output\Destination::INLINE);

        }

        if ($request->has('excel')) {

            return Excel::download(new ExportSaleType($sales), 'sale_type_cash_and_instalment.xlsx');

        }

        return $sales;

    }


}
