<?php

namespace Codex\Classes;


use App\DB\Role\Business;
use App\DB\Role\Component;
use App\DB\Role\Functionality;
use App\DB\Role\Module;
use App\DB\Role\Role;
use App\User;

class System
{


    public $messageTypes = [
        'new_project'  => 'New Project',
        'updates'      => 'Updates'


    ];
    public $chartTypes = [
        'Equity',
        'Income',
        'Expenses',
        'Bank',
        'Assets',
        'Liabilities',
        'Cost of Goods Sold'
    ];

    public $projectExpenseTypes = [
        'subdivison',
        'beaconing',
        'fencing',
        'grading',
        'map printing',
        'surveyor',
        'title',
        'others'
    ];

    public $customerTypes = [
        'individual',
        'company'


    ];
    public function paymentMethods()
    {
        return [
            'Cheque',
            'Mpesa',
            'Bank'
        ];

    }
    public function typeOfIssues()
    {
        return [
            'project',
            'plot',
            'appointment',
            'reservation',
            'agreement',
            'receipt',
            'payment',
            'title',
        ];

    }


    private  $schema = [
        'dashboard' => [
            'statistics',
            'project-statistics',
            'see_department_details'
        ],
        'projects' => [
            'manage' => [
                'add',
                'add-buying-price',
                'add-size',
                'delete',
                'update'
            ],
            'expenses',
            'reports' => [
                'above-50',
                'below-50',
                'availability',
                'below30',
                'unpaid',
                'plot-with-clients',
                'collections'

            ],
        ],
        'finance' => [
            'charts'=> [
                'reconcile'
                ],
            'expenses' => [
                'can-add-petty',
                'can-add-project',
                'can-expense'
            ],
            'journal-entry',
            'reports'=> [
                'collection',
                'analytics'
            ],
            'requisition' => [
                'browse',
                'read',
                'add',
                'approve-and-reject',
                'disburse'
            ]
        ],
        'plots' => [
            'manage' => [
                'add',
                'issue-title-deed',
                'delete',
                'update',
                'mark-can-sell',
                'release'
            ],
            'reports'
        ],
        'customers' => [
            'manage' => [
                'add',
                'delete',
                'update',
                'upload'
            ],
            'reports'
        ],
        'sale' => [
            'manage' => [
                'sale-statistics',
                'add',
                'list',
                'show-all-sales',
                'update',
                'agreement-request',
                'payment-request',
                'payment-update',
                'payment-approve',
                'new-sale',
                'pay-title',
                'approve',
                'edit-agreement',
                'approve-agreement',
                'print-title-acknowledgement-note',
                'edit-sale-details',
                'change-project',
                'change-customers',
                'change-director'
            ],
            'interest',
            'invoice' => [
                'update',
            ],
            'commissions' => [
                'list',
                'see-mine',
                'approve',
            ],
            'appointment'  => [
                'add',
                'view-all',
                'can-update',
                'follow-up',
                'reports',
                'today-appointment',
                'absent-appointment'
            ],
            'reports' => [
                'above-50',
                'late-installment',
                'week-installment',
                'below-50',
                'by-project',
                'by-date',
                'availability',
                'completed',
                'scanned-agreement',
                'show-figures',
                'unpaid',
                'reservation',
                'customer',
                'agent',
                'profit-and-loss'
            ],
            'payments',
            'transfer',
        ],
        'offer' => [
            'generate' => [
                'list-offer',
                'add-offer',
                'download',
                'convert-to-sale',
                'receipt',
                'request',
                'pending',
                'approved',
                'can-approve',
                'can-print',
                'edit'
            ]

        ],
        'account' => [
            'list' => ['update','delete']
        ],
        'users' => [
            'manage' => [
                'add',
                'delete',
                'update',
                'add-other-sales',
            ],
            'department-head',
            'role' => [
                'add',
                'update',
                'delete',
            ],
            'permissions' => [
                'user',
                'role'
            ],
            'leave' => [
                'leave-types',
                'make-request',
                'view-requests',
                'approve-request',
                'approve-handing-request',
                'reports'

            ],
            'reports'
        ],
        'expenses' => [
            'add',
            'manage' => [
                'add',
                'delete',
                'update'
            ],
            'reports'
        ],
        'settings' => [
            'company',
            'system'
        ],
        'communication' => [
            'settings',
            'manage' => [
                'send',
                'contacts'
            ],
            'issue',
        ]

    ];

    private $defaultRoles = [
        'admin' => [
            'dashboard',
            'projects',
            'plots',
            'sale',
            'users',
            'customers',
            'expenses',
            'settings'
        ],

        'marketers' => [
            'dashboard',
            'plots',
            'sale' => ['manage'],
            'customers' => ['manage'],

        ],

        'secretary' => [
            'dashboard',
            'plots',
            'users',
            'customers',
            'settings'
        ],
        'accountant' => [
            'dashboard',
            'expenses'
        ],
    ];


    public function availableFeatures()
    {
        return $this->schema;
    }




    public function getModules()
    {
        return Helper::getArraySpecialNames($this->schema);
    }


    public function getModuleComponents($moduleToCheck)
    {
        $componentCollection = collect(Helper::getArraySpecialNamesBySearch($this->schema, $moduleToCheck));

        return $componentCollection->transform(function ($component) use ($moduleToCheck) {
            return Helper::componentFullName($moduleToCheck, $component);
        })->toArray();
    }



    public function getModuleComponentsFunctionalities($moduleToCheck, $componentToCheck)
    {
        $moduleComponentsFunctionalities = [];
        $actualCompToCheck = explode('_', $componentToCheck)[1];

        foreach ($this->schema as $module => $components) {

            if ($module == $moduleToCheck) {

                if (is_array($components)) {

                    $funCollection = collect(Helper::getArraySpecialNamesBySearch
                    ($components, $actualCompToCheck));

                    return $funCollection->transform(function ($functionality) use (
                        $moduleToCheck,
                        $actualCompToCheck
                    ) {
                        return Helper::functionalityFullName($moduleToCheck, $actualCompToCheck, $functionality);
                    })->toArray();

                }

            }

        }

        return $moduleComponentsFunctionalities;
    }



    public function getDefaultUsers()
    {
        $defaultUsers = [];

        foreach ($this->defaultRoles as $user => $modules) {

            array_push($defaultUsers, $user);
        }

        return $defaultUsers;
    }

    public function getDefaultUsersModules($userToSearch)
    {
        return Helper::getArraySpecialNamesBySearch($this->defaultRoles, $userToSearch);
    }


    public function getDefaultUsersModuleComponent($userToSearch, $moduleToSearch)
    {
        $componentsResults = [];


        foreach ($this->defaultRoles as $user => $modules) {

            if ($userToSearch == $user) {

                $components = Helper::getArraySpecialNamesBySearch($modules, $moduleToSearch);

                if (empty($components) && in_array($moduleToSearch, $this->getDefaultUsersModules($user))) {
                    return $this->getModuleComponents($moduleToSearch);
                }

                return collect($components)->transform(function ($component) use ($moduleToSearch) {
                    return Helper::componentFullName($moduleToSearch, $component);
                })->toArray();
            }
        }

        return $componentsResults;
    }


    public function getDefaultUsersModuleComponentFunctionality($userToSearch, $moduleToSearch, $componentToSearch)
    {
        $functionalitiesResults = [];

        $componentToSearch = explode('_', $componentToSearch)[1];


        foreach ($this->defaultRoles as $user => $modules) {

            if ($userToSearch == $user) {

                foreach ($modules as $module => $components) {

                    if (is_string($components)) {
                        $components = $this->getDefaultUsersModuleComponent($userToSearch, $moduleToSearch);
                    }

                    if (is_array($components) && $module == $moduleToSearch) {

                        $functionalities = Helper::getArraySpecialNamesBySearch($components, $componentToSearch);

                        if (empty($functionalities) &&
                            in_array(Helper::componentFullName($moduleToSearch, $componentToSearch)
                                , $this->getDefaultUsersModuleComponent($userToSearch, $moduleToSearch))
                        ) {

                            return $this->getModuleComponentsFunctionalities($moduleToSearch,
                                Helper::componentFullName($moduleToSearch, $componentToSearch));
                        }

                        return collect($functionalities)->transform(function ($functionality) use (
                            $moduleToSearch,
                            $componentToSearch
                        ) {
                            return Helper::functionalityFullName($moduleToSearch, $componentToSearch, $functionality);
                        })->toArray();
                    }

                }
            }
        }

        return $functionalitiesResults;
    }


    public static function setupModulesAndComponents()
    {
        foreach (Helper::getSystemModules() as $moduleName) {

            $createdModule = Module::create([
                'name' => $moduleName,
                'label' => ucfirst($moduleName),
            ]);

            foreach (Helper::system()->getModuleComponents($moduleName) as $componentName) {

                $createdComponent = Component::create([
                    'module_id' => $createdModule['id'],
                    'name' => $componentName,
                    'label' => ucfirst(explode('_', $componentName)[1]),

                ]);


                foreach (Helper::system()
                             ->getModuleComponentsFunctionalities($moduleName, $componentName) as $fun) {

                   Functionality::create([
                        'component_id' => $createdComponent['id'],
                        'name' => $fun,
                        'label' => ucfirst(explode('_', $fun)[2]),

                    ]);

                }

            }
        }
    }


    public static function setupDefaultBusinessRoles($businessCode)
    {

        $business = Business::where('code', $businessCode)->first();



        foreach (Helper::getDefaultBusinessRoles() as $roleName) {

            $role = Role::create([
                'business_code' => $businessCode,
                'name' => $roleName,
                'label' => ucfirst($roleName),
                'description' => \Faker\Factory::create()->sentence
            ]);


            //-- Modules
            foreach (Helper::system()->getDefaultUsersModules($role->name) as $module) {

                //dd($module);


                ($role->addModulePermission(Module::where('name', $module)->first()));

            }

            //-- Components


            foreach ($business->modules as $module) {

                foreach (Helper::system()->getDefaultUsersModuleComponent($role->name,
                    $module->name) as $component) {



                    if (!empty($component)) {

                        $role->addComponentsPermission(Component::where('name',
                            $component)->first());

                    }

                }

            }


            //-- Functionalities
            foreach ($business->modules as $modules) {

                foreach ($modules->components as $components) {

                    foreach (Helper::system()->getDefaultUsersModuleComponentFunctionality(
                        $role->name, $modules->name, $components->name) as $functionality) {

                        if (!empty($functionality)) {

                            $role->addFunctionalitiesPermission
                            (Functionality::where('name', $functionality)->first());

                        }

                    }


                }

            }


        }
    }



    public static function setupDefaultBusinessPermissions(Business $business)
    {
        //--Module
        foreach (Module::all() as $module) {
            $business->addModulePermission($module);

        }

        //-- Component
        foreach ($business->modules as $module) {

            foreach (Helper::system()->getModuleComponents($module->name) as $components) {

                if (!is_null($components)) {

                    $business->addComponentsPermission(Component::where('name',
                        $components)->first());


                }
            }
        }

        //-- Functionality
        foreach ($business->modules as $module) {

            foreach (Helper::system()->getModuleComponents($module->name) as $components) {

                $fun = Helper::system()->getModuleComponentsFunctionalities($module->name, $components);

                if (!empty($components) && !empty($fun)) {

                    foreach ($fun as $functionality) {
                        $business->addFunctionalitiesPermission(
                            Functionality::where('name', $functionality)->first());
                        if (!is_null($functionality)) {

                        }
                    }
                }
            }
        }

        //End of setupDefaultBusinessPermissions
    }

    public static function setupUser($businessCode, Role $role, $emailPostfix = '')
    {
        $data = [
            'business_code' => $businessCode,
            'first_name' => \Faker\Factory::create()->firstName,
            'last_name' => \Faker\Factory::create()->lastName,
            'email' => $businessCode . $role->name . $emailPostfix . '@fanaka.co.ke',
            'role_id' => $role->id,
            'password' => bcrypt('1qazwsx'),
            'phone_number' => '254713' . \Faker\Factory::create()->randomNumber(6, true),

        ];

        $user = User::create($data);

        return $user;

    }

    public static function setupHQUsers($businessCode)
    {

        foreach (Role::all() as $role)
        {
            self::setupUser($businessCode, $role);

        }


    }

}
