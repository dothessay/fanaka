<?php

namespace Codex\Classes;


use Carbon\Carbon;
use Codex\Classes\Handlers\FanakaLog;
use Codex\Contracts\ServiceCaller;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;

abstract class ServiceAbstract
{
    use DispatchesJobs , FanakaLog;

    protected $caller;
    protected $user;
    protected $withDBTransaction = true;
    protected $logger;
    protected $shouldLog = true;
    protected $businessCode;

    function __construct(ServiceCaller $caller)
    {

        $this->caller = $caller;
        $this->businessCode = auth()->user()->business->code;
    }

    protected function begin()
    {
        if ($this->withDBTransaction) {
            DB::beginTransaction();
        }
    }

    protected function commit()
    {
        if ($this->withDBTransaction) {
            DB::commit();
        }
    }

    protected function rollback()
    {
        if ($this->withDBTransaction) {
            DB::rollback();
        }
    }

    public function runWithDBTransaction(array $data, array $options = [])
    {
        $this->withDBTransaction = true;
        return $this->run($data, $options);
    }

    public function run(array $data, array $options = [])
    {
        try {

            request()->request->add($data);
            request()->request->add($options);

            $this->begin();

            $response = $this->execute($data, $options);

            $this->commit();

            return $this->successfulResponse($response);
        } catch (\Exception $exception) {

            $this->rollback();

            Mail::raw($exception, function ($mail) use ($exception) {
                $mail->to(config('config.error_email.philip'))->subject($exception->getMessage());

            });

            return $this->failedResponse($exception->getMessage() . ' '. $exception->getTraceAsString());
        }
    }

    public function list(array $data, array $options = [])
    {
        try {

            $this->begin();

            $response = $this->execute($data, $options);
            $this->commit();

            return $response;
        } catch (\Exception $exception) {

            $this->rollback();

            return $this->failedResponse($exception->getMessage());
        }
    }

    /**
     * @param array $data
     * @param array $options
     */
    abstract protected function execute(array $data, array $options = []);
    abstract protected function filter(array $data, array $options = []);

    protected function successfulResponse($messageResponse)
    {
        return $this->caller->successResponse($messageResponse);
    }

    protected function failedResponse($message = '')
    {
        return $this->caller->failureResponse($message);
    }

    public function runRollback(array $data, array $options = [])
    {
        try {

            $this->begin();

            $response = $this->dexecute($data, $options);


            $this->commit();

            return $this->successfulResponse($response);
        } catch (\Exception $exception) {


            $this->rollback();

            return $this->failedResponse($exception->getMessage());
        }
    }

    protected function dexecute(array $data, array $options = [])
    {

        return true;
    }



}
