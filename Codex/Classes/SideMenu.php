<?php
/**
 * Created by PhpStorm.
 * User: philip
 * Date: 2/24/18
 * Time: 10:42 PM
 */

namespace Codex\Classes;


use Codex\Permission\Permissions;

class SideMenu extends SideMenuAbstract
{
    private  $test = false;

    public function hasPermissionToModule()
    {
        if ($this->test) {
            return true;
        }
        return  Permissions::checkAccessToModule(auth()->user(), $this->module);
    }

    public function hasPermissionToComponent($component)
    {
        if ($this->test) {
            return true;
        }
        return Permissions::checkAccessToModuleComponent(auth()->user(), $this->module . '_' . $component);
    }
}