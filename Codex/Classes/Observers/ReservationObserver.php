<?php
/**
 * Created by Philip Njuguna.
 * User: Philip Njuguna
 * Date: 11/19/18
 * Time: 9:52 AM
 */

namespace Codex\Classes\Observers;


use App\DB\Notification\Notification;
use App\DB\Sale\OfferLetter;
use App\DB\Sale\OfferLetterItem;
use App\User;

class ReservationObserver
{

    public function created(OfferLetterItem $letterItem)
    {


        foreach (User::where('is_active',true)->get() as $user)
        {
            Notification::create([
                'user_id' => $user->id,
                'type' => 'plot Taken',
                'url' =>  url('sale/manage/approve-sale/'.$letterItem->offer->id),
                'message' => "Please note that the plot ".$letterItem->plot->plot_no . "Are no longer available for sale",
            ]);
        }

    }
}
