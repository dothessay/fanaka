<?php



namespace Codex\Classes\Observers;


use App\DB\Finance\ChartOfAccount;
use App\DB\Sale\Calculator;
use App\DB\Sale\MonthlyInstallmentPlan;
use App\Jobs\SendSmsNotifications;
use App\Sale\Sale;
use App\Sale\SalePayment;
use App\Traits\IsJournable;
use Carbon\Carbon;
use Codex\Classes\Handlers\SendAdvantaAfricaInnoxNotification;
use Codex\Classes\Handlers\SendAfricanTalkingInnoxNotification;
use Codex\Classes\Handlers\SendInnoxNotification;
use Codex\Classes\Handlers\SystemSms;
use Codex\Classes\Repository\MonthlyInstallmentPrepaid;
use Illuminate\Support\Facades\Mail;

class PaymentObserver
{

    use IsJournable;

    public function created(SalePayment $payment)
    {

        if (request( 'is_conversion')) {

            return true;
        }

        SystemSms::installmentApprovalRequest($payment);


    }



    public function updated(SalePayment $payment)
    {
        $request = request();


       // $this->halfPayment($payment);


        if (isset($request['is_conversion'])) {

            return true;
        }


        foreach ($payment->sale->customers as $customer) {

            if (isset($payment->getDirty()['is_approved']))
            {

                $chart = ChartOfAccount::where('code', '5600/000')->first();

                if (! is_null($chart))
                {
                    $journal = $this->addToJournal([
                        'chart_id'  => $chart->id,
                        'credit'    => $payment->amount,
                        'debit'    => 0
                    ]);

                    $payment->saveJournal($journal);

                }
                $chart = ChartOfAccount::where('code', '8450/000')->first();

                if (! is_null($chart))
                {
                    $journal = $this->addToJournal([
                        'chart_id'  => $chart->id,
                        'debit'    => $payment->amount,
                        'credit'    => 0
                    ]);

                    $payment->saveJournal($journal);

                }

                $payable = $payment->payable;

                //$payable->created_at = Carbon::parse($payment->created_at);
                $payable->is_active =  true;

                $payable->save();

                $calculator = Calculator::where([
                    'sale_id' => $payment->sale->id,
                    'month'  => now()->format('Y-m'),
                ])->first();

                if ( ! is_null($calculator))
                {
                    $calculator->is_paid = true;
                    $calculator->amount_paid = $payment->amount;
                    $calculator->save();
                }

                SystemSms::installmentToCustomer($payment);
                $request->request->add(['amount' => $payment->amount]);
                (new MonthlyInstallmentPrepaid())
                    ->setSale($payment->sale)
                    ->store($request);
            }

        }




        return true;
    }


    public function deleted(SalePayment $payment)
    {

        $payable = $payment->payable;

        if ($payable) {
            $payable->forceDelete();
        }
        $journals = $payment->journalable;

        foreach ($journals as $journal){

            $journal->delete();
        }


    }



    private function halfPayment(SalePayment $payment)
    {

        $todayAmount = $payment->amount;

        $previousAmount = floatval($payment->sale->salePayments->sum('amount')) - floatval($todayAmount);


        $previousPercentage = (floatval($previousAmount) * 100 ) / floatval($payment->sale->getTotalSaleAmount());


        if (isset($payment->getDirty()['is_approved']))
        {

            $sale = $payment->sale;

            if ( $payment->sale->getPercentage() >= 50.00 &&  !is_null($sale->half_payment_at)) {


                $sale->half_payment_at = Carbon::parse($payment->created_at);


            }

            $sale->save();
        }
    }

    /**
     * @param Sale $sale
     */
    public function completePayment(Sale $sale)
    {
        $prepaid = $sale->guessPrepaid();

        $cashPrice = $sale->getTotalSaleAmount();

        $discount = $sale->getMeta('discount', 0);

        // discount = plot price * discount %



        $sellingPrice =

        $paymentPeriod = $prepaid;

    }
}


