<?php
/**
 * Created by PhpStorm.
 * User: philip
 * Date: 12/21/18
 * Time: 1:26 PM
 */

namespace Codex\Classes\Observers;


use App\DB\Notification\Notification;
use App\DB\Role\Role;
use App\Sale\SaleItem;
use App\User;
use Codex\Classes\Handlers\Calculator\CalculatorHandlerFacade;

class SaleItemObserver
{

    public function created(SaleItem $saleItem)
    {

        //
        $saleItem->product_id = $saleItem->plot->product->id;
        $saleItem->save();

        foreach (User::where('role_id',Role::where('name','admin')->first()->id)->get() as $user)
        {
            Notification::create([
                'user_id' => $user->id,
                'type' => 'New sale',
                'url' =>  url('sale/manage/approve-sale/'.$saleItem->sale->id),
                'message' => "Please approve the the sale with reference no FSL_{$saleItem->sale->id}",
            ]);
        }
        foreach (User::where('is_active',true)->get() as $user)
        {
            Notification::create([
                'user_id' => $user->id,
                'type' => 'plot Taken',
                'url' =>  url('sale/manage/approve-sale/'.$saleItem->sale->id),
                'message' => "Please not that the plot ".$saleItem->plot->plot_no . "Are no longer available for sale",
            ]);
        }

    }

}
