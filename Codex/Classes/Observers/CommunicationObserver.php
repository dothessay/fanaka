<?php
/**
 * Created by IntelliJ IDEA.
 * User: philip
 * Date: 4/20/19
 * Time: 1:37 PM
 */

namespace Codex\Classes\Observers;


use App\DB\Communication\Communication;
use App\DB\Communication\CommunicationMeta;
use Codex\Classes\Handlers\OneSignalHandler;
use Illuminate\Support\Facades\Mail;

class CommunicationObserver
{
    /**
     * send push notification to selected clients when a new communication is sent
     * @param Communication $communication
     */
    public function created(CommunicationMeta $meta)
    {

        OneSignalHandler::build()
            ->sendMessage($meta->communication->message, $meta->customer->id);


    }


}
