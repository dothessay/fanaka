<?php
/**
 * Created by PhpStorm.
 * User: philip
 * Date: 5/19/18
 * Time: 2:14 PM
 */

namespace Codex\Classes\Observers;


use App\DB\Accounting\Account;
use App\DB\Notification\Notification;
use App\DB\Role\Role;
use App\Jobs\SendSmsNotifications;
use App\Sale\Sale;
use App\User;

class SaleObserver
{

    public function created(Sale $sale)
    {





    }

    public function deleting(Sale $sale)
    {
        foreach ($sale->saleItems as $saleItem) {

            $plot = $saleItem->plot;

            $plot->is_reserved = false;
            $plot->is_sold = false;
            $plot->can_sell = true;
            $plot->save();




        }

    }
}
