<?php
/**
 * Created by IntelliJ IDEA.
 * User: philip
 * Date: 5/15/19
 * Time: 11:00 AM
 */

namespace Codex\Classes\Observers;


use App\Customer\Customer;
use Illuminate\Support\Facades\Hash;

class CustomerObserver
{

    public function created(Customer $customer)
    {
        $customer->password = bcrypt($customer->id_no);

        $customer->save();

    }

    public function updated(Customer $customer)
    {
        //ini_set('max_execution_time', 10000);
        //$customer->password = Hash::make($customer->id_no);

        //$customer->save();

    }

}
