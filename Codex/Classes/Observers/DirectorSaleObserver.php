<?php
namespace Codex\Classes\Observers;
use App\DB\Sale\CustomerSale;
use App\DB\Sale\SaleDirector;

class DirectorSaleObserver
{
    public function created(SaleDirector $sale)
    {
        $sale->customer->user_id = $sale->sale->agent;

        $sale->customer->save();
    }

    public function updated(SaleDirector $sale)
    {
        $sale->customer->user_id = $sale->sale->agent;

        $sale->customer->save();
    }
}