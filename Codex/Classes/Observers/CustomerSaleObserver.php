<?php


namespace Codex\Classes\Observers;


use App\DB\Sale\CustomerSale;

class CustomerSaleObserver
{

    public function created(CustomerSale $sale)
    {
        $sale->customer->user_id = $sale->sale->agent;

        $sale->customer->save();
    }

    public function updated(CustomerSale $sale)
    {
        $sale->customer->user_id = $sale->sale->agent;

        $sale->customer->save();
    }
}