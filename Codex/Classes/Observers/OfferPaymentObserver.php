<?php
/**
 * Created by IntelliJ IDEA.
 * User: philip
 * Date: 4/8/19
 * Time: 11:04 AM
 */

namespace Codex\Classes\Observers;


use App\DB\Sale\OfferPayment;
use Carbon\Carbon;

class OfferPaymentObserver
{

    public function created(OfferPayment $payment)
    {

    }

    public function deleted(OfferPayment $payment)
    {

        $payable = $payment->payable;

        $payable->forceDelete();


    }

}