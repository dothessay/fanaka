<?php
namespace Codex\Classes;

abstract class SideMenuAbstract
{
    private $items = [];

    private $name = '';

    private $icon = '';

    private $href = ''; // Used on single item menu

    private $routeName = ''; // Used on single item menu

    public $module = '';

    private $test = false;


    public function section($menuName, $module = '', $routeName = '', $menuIcon = '', $href = '')
    {

        $this->name = $menuName;
        $this->module = $module;
        $this->routeName = $routeName;
        $this->icon = $menuIcon;
        $this->href = $href;

        return $this;
    }

    public function icon($iconName)
    {
        $this->icon = $iconName;

        return $this;
    }

    public function href($path)
    {
        $this->href = $path;

        return $this;
    }

    public function routeName($name)
    {
        $this->routeName = $name;

        return $this;
    }

    public function module($module)
    {
        $this->module = $module;

        return $this;
    }

    public function test($test)
    {
        $this->test = $test;

        return $this;
    }

    public function item($name, $routePath, $routeName = '')
    {
        $this->items[$name] = [
            'path' => $routePath,
            'route_name' => ($routeName == '') ? str_replace('/', '_', $routePath) : $routeName
        ];

        return $this;
    }

    public function output($active = false)
    {

        $this->canReturnOutput();

        $output = $this->getSectionWithoutComponentsHTML($active);

        if ($this->containsComponents()) {

            $output = $this->getSectionWithComponentHTML($active);

        }

        $this->clearSectionForNextOutPut();

        return $this->hasPermissionToModule() ? $output : '';

    }

    private function canReturnOutput()
    {
        if ($this->name == '') {
            throw new \Exception('No items setup on side menu');
        }

        return true;
    }

    private function containsComponents()
    {
        return count($this->items) > 1;
    }

    private function getItemsRouteList()
    {
        $this->canReturnOutput();

        $routeList = '';
        $noOfItems = count($this->items);
        $noOfItemsCount = 1;

        foreach ($this->items as $item => $itemValue) {
            $routeList .= $itemValue['route_name'];

            if ($noOfItemsCount == $noOfItems) {
                break;
            }

            $routeList .= '|';
            $noOfItemsCount++;
        }

        return $routeList;
    }

    private function getComponentHTML($active = false)
    {
        $html = '';

        foreach ($this->items as $item => $itemValue) {
            if (!$this->hasPermissionToComponent($this->getRouteComponent($itemValue['route_name']))) {
                continue;
            }
            $activeClass = Helper::isActiveRoute($itemValue['route_name'], true);

            if ($active ) {
                $activeClass = 'class=" btn btn-bg btn-group"';
            }




            $html .= "\n" . '<li ';

            $html .= $activeClass;

            $html .= '><a href="';

            $html .= url($itemValue['path']);


            $html .= '">' . ucwords($item) . '</a></li>';
        }

        return $html;
    }

    private function clearSectionForNextOutPut()
    {
        $this->items = [];

        $this->name = '';

        $this->icon = '';

        $this->href = '';

        $this->routeName = '';

    }

    private function getSectionWithoutComponentsHTML($active = false)
    {


        $activeClass = Helper::isActiveRoute($this->routeName,
            true);
        if ($active)
        {
            $activeClass = "active";
        }


        return '<li ' . $activeClass . '><a href="' . url($this->href) . '"><i class="' . $this->icon . '"></i>
                    <span>' . ucwords($this->name) . '</span></a></li>';
    }

    private function getSectionWithComponentHTML($active = false)
    {
        $routeList = $this->getItemsRouteList();


        $sublink = '';

        $activeClass = Helper::isActiveRoute($routeList);
        if ($active)
        {
            $activeClass = "active";

        }



        $output = '<li class=" treeview has-sub';

        $output .= $activeClass;

        $output .= '">' . "\n" . '<a>
                    <i class="';

        $output .= $this->icon;

        $output .= '"></i>
                    <b class="caret pull-right"></b>
                    <span>';

        $output .= ucwords($this->name);

        $output .= '</span>
                </a>
                <ul class="sub-menu treeview-menu">';

        $output .= $this->getComponentHTML($active);

        $output .= "\n" . '</ul>
                </li>' . "\n";

        return $output;
    }

    abstract protected function hasPermissionToModule();

    abstract protected function hasPermissionToComponent($component);


    private function getRouteComponent($routeName)
    {
        return explode('_', $routeName)[1];
    }


}