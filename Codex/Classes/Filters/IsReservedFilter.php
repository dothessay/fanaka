<?php
namespace Codex\Classes\Filters;
class IsReservedFilter
{
    public function handle($request , \Closure $next)
    {

        $builder = $next($request);
        if (! request()->has('is_reserved'))
        {
            return $builder;
        }

        return $builder->where('is_reserved', request('is_reserved'));
    }

}
