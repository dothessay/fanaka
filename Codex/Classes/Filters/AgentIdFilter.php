<?php


namespace Codex\Classes\Filters;


class AgentIdFilter
{
    public function handle($request , \Closure $next)
    {

        $builder = $next($request);
        if (! request()->has('agent_id'))
        {
            return $builder;
        }

        return $builder->where('agent_id', request('agent_id'));
    }

}
