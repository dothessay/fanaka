<?php


namespace Codex\Classes\Filters;


class ActiveFilter
{
    public function handle($request , \Closure $next)
    {

        $builder = $next($request);
        if (! request()->has('active'))
        {
            return $builder;
        }

        return $builder->where('active', request('active'));
    }

}
