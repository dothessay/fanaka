<?php


namespace Codex\Classes\Filters;


class ProductCodeFilter
{
    public function handle($request , \Closure $next)
    {
        $builder = $next($request);
        if (! request()->has('product_code'))
        {
            return $builder;

        }
        return $builder->where('product_code', request('product_code'));
    }

}
