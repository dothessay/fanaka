<?php
namespace Codex\Classes\Filters;
class ProjectFilter
{
    public function handle($request , \Closure $next)
    {
        $builder = $next($request);
        if (! request()->has('project_id'))
        {
            return $builder;

        }
        return $builder->where('product_id', request('project_id'));
    }

}
