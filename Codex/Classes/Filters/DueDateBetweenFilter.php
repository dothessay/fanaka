<?php


namespace Codex\Classes\Filters;


use Carbon\Carbon;

class DueDateBetweenFilter
{
    public function handle($request , \Closure $next)
    {

        $builder = $next($request);
        if (! request()->has('start_date') && ! request()->has('end_date'))
        {
            return $builder;
        }

        return $builder->whereBetween('due_date', [
            Carbon::parse(request('start_date'))->startOfDay(),
            Carbon::parse(request('end_date'))->endOfDay()
        ]);
    }

}
