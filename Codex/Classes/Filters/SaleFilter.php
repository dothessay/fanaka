<?php


namespace Codex\Classes\Filters;


class SaleFilter
{
    public function handle($request , \Closure $next)
    {

        $builder = $next($request);
        if (! request()->has('sale_id'))
        {
            return $builder;
        }

        return $builder->where('sale_id', request('sale_id'));
    }

}
