<?php


namespace Codex\Classes\Filters;


class IsApprovedFilter
{
    public function handle($request , \Closure $next)
    {
        $builder = $next($request);


        if (! request()->has('is_approved'))
        {
            return $builder;
        }
        return $builder->where('is_approved', request('is_approved'));
    }

}
