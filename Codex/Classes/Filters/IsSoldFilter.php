<?php


namespace Codex\Classes\Filters;


class IsSoldFilter
{
    public function handle($request , \Closure $next)
    {

        $builder = $next($request);
        if (! request()->has('is_sold'))
        {
            return $builder;
        }

        return $builder->where('is_sold', request('is_sold'));
    }

}
