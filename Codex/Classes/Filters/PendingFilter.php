<?php


namespace Codex\Classes\Filters;


class PendingFilter
{
    public function handle($request , \Closure $next)
    {

        $builder = $next($request);

        return $builder->where([
            'approved_at' =>  null,
            'rejected_at' =>  null
            ]);
    }

}
