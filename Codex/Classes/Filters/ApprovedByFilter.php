<?php


namespace Codex\Classes\Filters;


class ApprovedByFilter
{
    public function handle($request , \Closure $next)
    {

        $builder = $next($request);
        if (! request()->has('approved_by'))
        {
            return $builder;
        }

        return $builder->where('approved_by', request('approved_by'));
    }

}
