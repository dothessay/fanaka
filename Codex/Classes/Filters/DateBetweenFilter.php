<?php


namespace Codex\Classes\Filters;


use Carbon\Carbon;

class DateBetweenFilter
{
    public function handle($request , \Closure $next)
    {
        $builder = $next($request);
        if (! request()->has('start_date') && ! request()->has('end_date'))
        {
            return $builder;
        }
        return $builder->whereBetween('created_at', [
            Carbon::parse(request('start_date'))->startOfDay(),
            Carbon::parse(request('end_date'))->endOfDay()
        ]);
    }

}
