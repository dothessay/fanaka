<?php


namespace Codex\Classes\Filters;


class AgentFilter
{
    public function handle($request , \Closure $next)
    {


        $builder = $next($request);
        if (! request()->has('agent'))
        {
            return $builder;
        }


        return $builder->where('agent', request('agent'));
    }

}
