<?php


namespace Codex\Classes\Filters;


class CustomerFilter
{
    public function handle($request , \Closure $next)
    {

        $builder = $next($request);
        if (! request()->has('customer_id'))
        {
            return $builder;
        }

        return $builder->where('customer_id', request('customer_id'));
    }

}
