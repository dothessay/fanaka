<?php


namespace Codex\Classes\Filters;


class UserFilter
{
    public function handle($request , \Closure $next)
    {

        $builder = $next($request);
        if (! request()->has('user_id'))
        {
            return $builder;
        }

        return $builder->where('user_id', request('user_id'));
    }

}
