<?php
namespace Codex\Classes\Filters;

use Closure;

class IsConvertedFilter
{
    public function handle($request , Closure $next)
    {
        $builder = $next($request);

        return $builder->where('is_converted', false);
    }
}
