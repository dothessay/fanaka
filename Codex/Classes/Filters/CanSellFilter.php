<?php


namespace Codex\Classes\Filters;


class CanSellFilter
{
    public function handle($request , \Closure $next)
    {

        $builder = $next($request);
        if (! request()->has('can_sell'))
        {
            return $builder;
        }

        return $builder->where('can_sell', request('can_sell'));
    }

}
