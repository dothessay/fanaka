<?php


namespace Codex\Classes\Filters;


class CommissionFilter
{
    public function handle($request , \Closure $next)
    {
        $builder = $next($request);

        if (! request()->has('commission_at'))
        {
            return $builder;
        }

        return $builder->where('commission_at', null);
    }

}
