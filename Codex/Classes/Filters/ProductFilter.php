<?php


namespace Codex\Classes\Filters;


class ProductFilter
{
    public function handle($request , \Closure $next)
    {
        $builder = $next($request);
        if (! request()->has('product_id'))
        {
            return $builder;

        }
        return $builder->where('product_id', request('product_id'));
    }

}
