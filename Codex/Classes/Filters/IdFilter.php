<?php


namespace Codex\Classes\Filters;


class IdFilter
{
    public function handle($request , \Closure $next)
    {

        $builder = $next($request);
        if (! request()->has('id'))
        {
            return $builder;
        }

        return $builder->where('id', request('id'));
    }

}
