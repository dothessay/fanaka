<?php


namespace Codex\Classes\Filters;


use Carbon\Carbon;

class CommissionDateBetweenFilter
{
    public function handle($request , \Closure $next)
    {

        $builder = $next($request);
        if (! request()->has('commission_start_date') && ! request()->has('commission_end_date'))
        {
            return $builder;
        }

        return $builder->whereBetween('half_payment_at', [
            Carbon::parse(request('commission_start_date'))->startOfDay(),
            Carbon::parse(request('commission_end_date'))->endOfDay()
        ]);
    }

}
