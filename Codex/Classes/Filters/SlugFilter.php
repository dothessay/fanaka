<?php


namespace Codex\Classes\Filters;


class SlugFilter
{
    public function handle($request , \Closure $next)
    {

        $builder = $next($request);
        if (! request()->has('slug'))
        {
            return $builder;
        }

        return $builder->where('slug', request('slug'));
    }

}
