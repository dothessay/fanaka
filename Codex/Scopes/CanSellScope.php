<?php
/**
 * Created by Philip Njuguna.
 * User: Philip Njuguna
 * Date: 11/13/18
 * Time: 11:08 AM
 */

namespace Codex\Scopes;


use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Scope;

class CanSellScope implements Scope
{


    /**
     * Apply the scope to a given Eloquent query builder.
     *
     * @param  \Illuminate\Database\Eloquent\Builder $builder
     * @param  \Illuminate\Database\Eloquent\Model $model
     * @return void
     */
    public function apply(Builder $builder, Model $model)
    {

        $builder->where('can_sell', true);

    }
}