<?php


namespace Codex\Scopes;


use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Scope;

class MyInvoices implements Scope
{

    /**
     * Apply the scope to a given Eloquent query builder.
     *
     * @param Builder $builder
     * @param Model $model
     * @return void
     */
    public function apply(Builder $builder, Model $model) : void
    {
        if ( isset(request()->user()->departmentAdmin))
        {
            if (! request()->user()->can('access-module-component-functionality', 'sale_manage_show-all-sales') && ! request()->user()->departmentAdmin )
            {
               $table = $model->getTable();

               $builder->where($table . '.agent',  request()->user()->id );
           }
       }
    }
}
