<?php


namespace Codex\Scopes;


use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Scope;

class MySales implements Scope
{

    /**
     * Apply the scope to a given Eloquent query builder.
     *
     * @param Builder $builder
     * @param Model $model
     * @return void
     */
    public function apply(Builder $builder, Model $model) : void
    {
        if (isset(auth()->user()->id))
        {

            if (! auth()->user()->can('access-module-component-functionality', 'sale_manage_show-all-sales') && ! auth()->user()->departmentAdmin )
            {
                $table = $model->getTable();

                $builder->where($table . '.agent',  request()->user()->id );
            }

        }
    }
}
