<?php
/**
 * Created by PhpStorm.
 * User: philip
 * Date: 2/11/19
 * Time: 12:34 PM
 */

namespace Codex\Scopes;


use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Scope;

class AccountLog implements Scope
{

    /**
     * @param Builder $builder
     * @param Model $model
     */
    public function apply(Builder $builder, Model $model)
    {
        $table = $model->getTable();

        $builder->where($table . '.source','!=', 'Transfer From Offer To The Sale' );
    }

}