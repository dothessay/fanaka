<?php

namespace App\Traits;


use App\DB\Role\Role;
use App\User;

trait CanPermit
{


    public function role()
    {
        return $this->morphToMany(Role::class, 'role_permission')->withPivot('id')->withTimestamps();;
    }

    /**
     * @return mixed
     */
    public function user()
    {
        return $this->morphToMany(User::class, 'user_permission')->withPivot('id','type')->withTimestamps();;
    }

    /*
     * IF a "CanPermit" is added of a MODEL ensure adding a method to query
     * for it imn the "IsPermissible" and another to link to it
     *
     * */

}