<?php

namespace App\Traits;

use App\DB\Role\Component;
use App\DB\Role\Functionality;
use App\DB\Role\Module;
use Illuminate\Support\Facades\Auth;

trait IsPermissible
{

    public $permissionRelation = '';

    public function permissionRelation()
    {
        return $this->permissionRelation;
    }

    public function modules()
    {
        return $this->morphedByMany(Module::class, $this->permissionRelation() . '_permission')
            ->withPivot('id')->withTimestamps();
    }

    public function canAccessModule($module)
    {


        return $this->modules->where('name', $module)->count();
    }

    public function addModulePermission($module)
    {
       $this->modules()->attach($module);



    }

    public function deleteModulePermission(Module $module)
    {

        $this->modules()->detach($module);



    }


    public function components()
    {
        return $this->morphedByMany(Component::class, $this->permissionRelation() . '_permission')->withPivot('id')->withTimestamps();
    }

    public function canAccessComponent($component)
    {
        return $this->components->where('name', $component)->count();

    }

    public function addComponentsPermission(Component $component)
    {
        $this->components()->attach($component);


    }

    public function deleteComponentsPermission($component)
    {
        $this->components()->detach($component);


    }


    public function functionalities()
    {
        return $this->morphedByMany(Functionality::class, $this->permissionRelation(). '_permission')->withPivot('id')->withTimestamps();;
    }

    public function canAccessFunctionalities($functionality)
    {
        return $this->functionalities->where('name', $functionality)->count();
    }

    public function addFunctionalitiesPermission(Functionality $functionality)
    {
        $this->functionalities()->attach($functionality);




    }

    public function deleteFunctionalitiesPermission(Functionality $functionality)
    {
        $this->functionalities()->detach($functionality);


    }


    /*
     * THINGS TO NOTE WHEN IMPLEMENTING IS PERMISSIBLE
     *
     *
     1. Modify the Model using this trait to set the $this->permissionRelation.

        public function __construct(array $attributes = [])
        {
            parent::__construct($attributes);

            $this->permissionRelation = 'role';
        }

      2. Must add a table with the name '$permissionRelation_permission' on the Model migration

        Schema::create('business_permissions', function (Blueprint $table) {

            $table->integer('business_id')->unsigned();
            $table->integer('business_permission_id')->unsigned();
            $table->string('business_permission_type');
            $table->timestamps();

            $table->foreign('business_id')
                ->references('id')->on('businesses')
                ->onDelete('cascade')->onUpdate('cascade');

        });

      3. Register the relation in the "CanPermit" Trait using a method like below

        public function business()
        {
            return $this->morphToMany(Business::class, 'business_permission');
        }
     *
     *
     * NOTE: IF a "CanPermit" is added of a MODEL ensure adding a method to query for it
     *         in the "IsPermissible" and another to link to it
     *
     * NOTE: [business === $this->permissionRelation]
     *
     */

}