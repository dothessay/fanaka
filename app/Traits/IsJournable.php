<?php


namespace App\Traits;


use App\DB\Finance\JournalEntry;

trait IsJournable
{

    public function addToJournal(array $data)
    {
       return JournalEntry::create([
            'chart_id'  => $data['chart_id'],
            'debit'     => $data['debit'],
            'credit'    => $data['credit'],
        ]);

    }
}
