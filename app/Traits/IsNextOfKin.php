<?php
/**
 * Created by PhpStorm.
 * User: philip
 * Date: 12/22/18
 * Time: 11:16 AM
 */

namespace App\Traits;


use App\Customer\Customer;
use App\DB\Customer\NextOfKin;

trait IsNextOfKin
{

    public function nextOfKin(Customer $customer , $data)
    {

        $count = NextOfKin::where([
            'customer_id'  => $customer->id,
            'full_name'    => strtolower(ucwords($data['full_name'])),
            'phone_number' => $data['phone_number'],
            'relation'     => $data['relation']
        ])->get();



        if (! $count->count())
        {
            return NextOfKin::create([
                'customer_id'  => $customer->id,
                'full_name'    => $data['full_name'],
                'phone_number' => $data['phone_number'],
                'relation'     => $data['relation']
            ]);
        }




    }

}