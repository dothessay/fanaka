<?php


namespace App\Traits;


use App\DB\Business\Meta;
use Illuminate\Database\Eloquent\Model;


trait IsMetable
{
    public function metable()
    {
        return $this->morphMany(Meta::class,'metable');
        
    }

    public function saveMeta(Meta $meta)
    {
        return $this->metable()->save($meta);
    }

    public function getMeta($key , $default = '')
    {
        return  isset($this->metable()->where('key', $key)->first()->value) ? $this->metable()->where('key', $key)->first()->value : $default;
    }
}
