<?php

namespace App\Traits;

use App\DB\Role\Component;
use App\DB\Role\Functionality;
use App\DB\Role\Module;
use App\Traits\IsPermissible;


trait IsPermissibleUser
{
    use IsPermissible;

    public function permissionRelation()
    {
        return $this->permissionRelation;
    }

    public function modules()
    {
        return $this->morphedByMany(Module::class, $this->permissionRelation() . '_permission')
            ->withPivot('id')
            ->wherePivotIn('type', ['allow'])
            ->withTimestamps();
    }

    public function modulesDenied()
    {
        return $this->morphedByMany(Module::class, $this->permissionRelation() . '_permission')
            ->withPivot('id')
            ->wherePivotIn('type', ['deny'])
            ->withTimestamps();
    }

    public function modulesAllowed()
    {


        return $this->morphedByMany(Module::class, $this->permissionRelation() . '_permission')
            ->withPivot('id')
            ->wherePivotIn('type', ['allow'])
            ->withTimestamps();
    }




    public function isModuleDenied($module)
    {


        return $this->modulesDenied()->where('name',$module)->count() ;


    }
    public function isModuleAllowed($module)
    {

        return $this->modules()->where('name',$module)->count() ;


    }






    public function addModulePermission($module, $type = 'allow')
    {


        if($this->modules()->where('name',$module->name)->count())
        {
            $this->modules()->detach($module);
        }


        if($this->modulesDenied()->where('name',$module->name)->count())
        {
            $this->modulesDenied()->detach($module);
        }


        $this->modules()->attach($module,['type'=> $type]);



    }

    public function deleteModulePermission($module)
    {

        $this->modules()->detach($module);

        $this->modules()->attach($module, ['type' => 'deny']);

    }


    public function components()
    {
        return $this->morphedByMany(Component::class, $this->permissionRelation() . '_permission')
            ->withPivot('id')
            ->wherePivotIn('type', ['allow'])
            ->withTimestamps();;
    }


    public function componentsDenied()
    {
        return $this->morphedByMany(Component::class, $this->permissionRelation() . '_permission')
            ->withPivot('id')
            ->wherePivotIn('type', ['deny'])
            ->withTimestamps();
    }



    public function isComponentsDenied($components)
    {

        return $this->componentsDenied()->where('name',$components)->count();

    }

    public function isComponentsAllowed($components)
    {

        return $this->components()->where('name',$components)->count();

    }

    public function addComponentsPermission($component, $type = 'allow')
    {


        if($this->components()->where('name',$component->name)->count())
        {
            $this->components()->detach($component);
        }


        if($this->componentsDenied()->where('name',$component->name)->count())
        {
            $this->componentsDenied()->detach($component);
        }


        $this->components()->attach($component,['type'=> $type]);


    }

    public function deleteComponentsPermission($component)
    {


        if($this->components()->where('name',$component->name)->count())
        {
            $this->components()->detach($component);

        }

        if($this->componentsDenied()->where('name',$component->name)->count())
        {
            $this->componentsDenied()->detach($component);

        }


        if($this->isComponentsAllowed($component))
        {
            $this->isComponentsAllowed($component)->detach($component);

        }

        $this->components()->attach($component, ['type' => 'deny']);


    }


    public function functionalities()
    {
        return $this->morphedByMany(Functionality::class, $this->permissionRelation() . '_permission')
            ->withPivot('id')
            ->wherePivotIn('type', ['allow'])
            ->withTimestamps();;
    }

    public function functionalitiesDenied()
    {
        return $this->morphedByMany(Functionality::class, $this->permissionRelation() . '_permission')
            ->withPivot('id')
            ->wherePivotIn('type', ['deny'])
            ->withTimestamps();
    }

    public function isFunctionalitiesDenied($functionality)
    {
        return $this->functionalitiesDenied()->where('name',$functionality)->count();
    }

    public function isFunctionalitiesAllowed($functionality)
    {
        return $this->functionalities()->where('name',$functionality)->count();
    }

    public function addFunctionalitiesPermission($functionality, $type = 'allow')
    {



        if($this->functionalities()->where('name',$functionality->name)->count())
        {
            $this->functionalities()->detach($functionality);
        }

        if($this->functionalitiesDenied()->where('name',$functionality->name)->count())
        {
            $this->functionalitiesDenied()->detach($functionality);
        }

        $this->functionalities()->attach($functionality, ['type' => $type]);

    }

    public function deleteFunctionalitiesPermission($functionality)
    {


        $this->functionalitiesDenied()->detach($functionality);
        $this->functionalities()->detach($functionality);
        $this->functionalities()->attach($functionality, ['type' => 'deny']);

    }

}