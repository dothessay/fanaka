<?php

namespace App\Plot;

use App\DB\Product\Product;
use App\DB\Sale\OfferLetter;
use App\DB\Sale\OfferLetterItem;
use App\Plots\PlotUpload;
use App\Sale\Sale;
use App\Sale\SaleItem;
use App\Sale\SalePayment;
use App\Setting\PlotSizeSetting;
use App\Traits\IsMetable;
use App\User;
use Codex\Scopes\CanSellScope;
use Illuminate\Database\Eloquent\Model;

class Plot extends Model
{
    use IsMetable;
    protected $guarded = [];

    protected $casts = [
        'is_sold' => "boolean",
        'is_reserved' => "boolean",
        'is_title_issued ' => 'boolean'
    ];

    protected $with = ['product', 'size'];


    protected $appends = [
        'hold_release_date','hold_reason'
    ];


    public function product()
    {
        return $this->belongsTo(Product::class, 'product_code', 'code');

    }

    public function salePayments()
    {
        return $this->hasManyThrough(
            SalePayment::class ,
            SaleItem::class ,
            'sale_id',
            'plot_id',
            'id',
            'plot_id'
        );

    }

    public function size()
    {
        return $this->belongsTo(PlotSizeSetting::class, 'size_id', 'id');

    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');

    }

    public function issuedBy()
    {
        return $this->belongsTo(User::class, 'issued_by', 'id');

    }

    public function saleItem()
    {
        return $this->hasOne(SaleItem::class, 'plot_id', 'id');

    }

    public function offerItem()
    {
        return $this->hasOne(OfferLetterItem::class, 'plot_id', 'id');

    }

    public function plotUploads()
    {
        return $this->hasMany(PlotUpload::class, 'plot_id', 'id');

    }

    public function setPriceAttribute($value)
    {

        return $this->attributes['price'] = str_replace(',', '', $value);

    }

    public function getPriceAttribute($value)
    {

        return $this->attributes['price'] = (double)str_replace(',', '', $value);

    }

    public function getPricing()
    {
        if (($this->product->pricing->count())) {


            return $this->product->pricing->map(function ($q) {

                return [
                    'payment_option' => $q->payment_option,
                    'size_id' => $q->size->id,
                    'amount' => $q->amount,
                    'start_at' => $q->start_at,
                    'end_at' => $q->end_at,
                    'status'  => $q->getOfferStatus(),
                ];
            });
        }
        return [
            [
                'payment_option' => "cash",
                'size_id' => $this->size->id,
                'amount' => floatval($this->price),
                'start_at' => now()->startOfCentury(),
                'end_at' => now()->endOfCentury(),
                'status'  => false,
            ],
            [
                'payment_option' => "installment",
                'size_id' => $this->size->id,
                'amount' => $this->price,
                'start_at' => now()->startOfCentury(),
                'status'  => false,
                'end_at' => now()->endOfCentury(),
            ]
        ];


    }

    public function getIsReservedAttribute($value)
    {

        $isReserved = $value;

        if ($this->is_sold) {


            $isReserved = true;

        }

        return $this->attributes['is_reserved'] = (boolean)$isReserved;

    }

    public function name()
    {
        return $this->product->name();
    }


    public function scopeIsHeld($query)
    {
        return $query->where('can_sell', true);
    }

    public function scopeIsAvailable($query)
    {
        return $query->where([
            'can_sell' => true,
            'is_reserved' => false
        ]);
    }

    public function getSaleDate()
    {

        if (!$this->available()) {
            $saleItem = SaleItem::where('plot_id', $this->id)->first();

            if ($saleItem) {
                return $saleItem->created_at;
            }


            $offerItem = OfferLetterItem::where('plot_id', $this->id)->first();


            if ($offerItem) {

                return $offerItem->created_at;

            }

            return "Taken";

        }

        return "Not Sold";

    }

    public function available()
    {
        $isAvalable = true;


        if (! $this->can_sell) {

            $isAvalable = false;

        }

        if ( $this->can_sell) {

            if ($this->is_reserved || $this->is_sold) {


                $isAvalable = false;

            }

        }

        return $isAvalable;


    }

    public function getSaleBalance()
    {


        if (!$this->available()) {
            $saleItem = SaleItem::where('plot_id', $this->id)->first();
            if ($saleItem) {
                return $saleItem->sale->getBalance();

            }

            $offerItem = OfferLetterItem::where('plot_id', $this->id)->first();


            if ($offerItem) {

                return $offerItem->offer->getBalance();

            }
            return "Taken";
        }

        return "Not Sold";

    }

    public function getAmountPaid()
    {


        if (!$this->available()) {
            $saleItem = SaleItem::where('plot_id', $this->id)->first();
            if ($saleItem) {
                return $saleItem->sale->salePayments->sum('amount');

            }
            $offerItem = OfferLetterItem::where('plot_id', $this->id)->first();


            if ($offerItem) {

                return $offerItem->offer->offerPayments->sum('amount');

            }

            return "Taken";
        }

        return "Not Sold";

    }

    public function getSaleServedBy()
    {

        if (!$this->available()) {

            $saleItem = SaleItem::where('plot_id', $this->id)->first();

            if ($saleItem) {
                return $saleItem->sale->user->fullName();
            }

            $offerItem = OfferLetterItem::where('plot_id', $this->id)->first();


            if ($offerItem) {

                return $offerItem->offer->user->fullName();

            }

            return "Taken";
        }

        return "Not Sold";

    }

    public function getCustomers()
    {


        if (!$this->available()) {
            $saleItem = $this->saleItem;


            if ($saleItem) {
                return $saleItem->sale->getCustomerHref();
            }

            $offerItem = OfferLetterItem::where('plot_id', $this->id)->first();


            if ($offerItem) {

                return $offerItem->offer->getCustomerHref();

            }

            return "Taken";
        }
        return "Not Sold";
    }

    public function getPercentage()
    {

        if (!$this->available()) {
            $saleItem = $this->saleItem;
            if ($saleItem) {
                if ($saleItem->sale->getPercentage() >= 99.999) {
                    return $saleItem->sale->getPercentage();

                }
                return round(floatval($saleItem->sale->getPercentage()) - 2);
            }

            $offerItem = OfferLetterItem::where('plot_id', $this->id)->first();


            if ($offerItem) {

                return $offerItem->offer->getPercentage();

            }
            return "Taken";
        }


        return "Not Sold";
    }

    public function getSaleRef()
    {

        if (!$this->available()) {
            $saleItem = SaleItem::where('plot_id', $this->id)->first();

            if ($saleItem) {
                return $saleItem->sale->id;
            }

            $offerItem = OfferLetterItem::where('plot_id', $this->id)->first();


            if ($offerItem) {

                return $offerItem->offer->id;

            }

            return "Taken";

        }

        return "Not Sold";
    }

    public function getSellingPrice()
    {

        if (!$this->available()) {
            $saleItem = SaleItem::where('plot_id', $this->id)->first();

            if ($saleItem) {
                return $saleItem->price;
            }

            $offerItem = OfferLetterItem::where('plot_id', $this->id)->first();


            if ($offerItem) {

                return $offerItem->selling_price;

            }

            return "Taken";

        }
        return "Not Sold";
    }

    public function isSold()
    {

        if (!$this->available()) {

            $saleItem = SaleItem::where('plot_id', $this->id)->first();

            if ($saleItem) {
                return true;
            }
            $offerItem = OfferLetterItem::where('plot_id', $this->id)->first();


            if ($offerItem) {

                return true;

            }

        }

        return false;
    }

    public function getDiscount()
    {

        $saleItem = SaleItem::where('plot_id', $this->id)->first();

        if ($saleItem) {
            return 100 - (100 * (double)($saleItem->price)) / (double)$this->price . " %";
        }

        $offerItem = OfferLetterItem::where('plot_id', $this->id)->first();


        if ($offerItem) {

            return 100 - (100 * (double)$offerItem->selling_price / (double)$this->price) . ' %';

        }

        return "Not Sold";
    }

    public function getStatus()
    {
        return $this->setStatus();

    }

    private function setStatus()
    {

        $url = url('plots/manage/details/' . $this->id);

        if ($this->isPlotTaken()) {
            return "<span class='badge badge-danger'> <a href='" . $url . "'>Taken</a></span>";
        }
        if ($this->available()) {

            return "<span class='badge badge-primary'> <a href='" . $url . "'>Available</a></span>";

        }
        // return "<span class='badge badge-danger'>'.$this->is_reserved.'  '.$this->is_sold.' '.$this->can_sell.'</span>";


    }

    public function isPlotTaken()
    {
        $isTaken = false;


        // taken if  can not sell and is reserved or is sold


        if (!$this->can_sell && ($this->is_reserved)) {


            $isTaken = true;

        }

        if ($this->isPlotOnHold()) {

            $isTaken = true;
        }


        return $isTaken;


    }

    public function isPlotOnHold()
    {

        $isHold = false;


        /**
         * it is on hold if
         *
         * can not sell
         * and
         *
         * it is not reserved on is not sold
         *
         *

         */


        if (!$this->can_sell && (!$this->is_reserved)) {


            $isHold = true;

        }


        return $isHold;


    }

    public function availableForSale()
    {

        if (($this->is_reserved || $this->is_sold) && $this->can_sell) {

            return false;
        }


        return true;
    }

    public function getSales()
    {
        if ($this->saleItem) {

            return $this->saleItem->sale;

        }
        return new Sale();

    }

    public function getOffers()
    {
        if ($this->offerItem) {

            return $this->offerItem->offer;

        }
        return new OfferLetter();

    }

    public function plotToArray()
    {
        return [
            'project_code' => $this->product->code,
            'landLocation' => $this->product->name(),
            'id' => $this->id,
            'plotNo' => $this->plot_no,
            'size' => $this->size->label,
            'size_id' => $this->size->id,
            'price' => number_format($this->price, 2),
            'sellingPrice' => $this->price,
            'details' => $this->getPropertyDetails(),
            'approximate' => $this->getPropertyApprox(),
        ];

    }

    public function getPropertyDetails()
    {

        if ($this->size->label == "40x80") {

            return $this->product->name() . ' :' . ucfirst($this->getTitle()) . ' Plot no: ' . $this->plot_no . ' ' . "Measuring Approx 0.030 Ha or equivalent";
        } else {

            return $this->product->name() . ' :' . ucfirst($this->getTitle()) . ' Plot no: ' . $this->plot_no . ' ' . " Measuring Approx 0.040 Ha or equivalent";

        }


    }

    public function getPropertyApprox()
    {

        if ($this->size->label == "40x80") {

            return "Approx 0.030 Ha or equivalent";
        } else {

            return "Approx 0.040 Ha or equivalent";

        }
    }

    public function sold()
    {
        return $this->saleItem;

    }
    // metas

    public function getHoldReasonAttribute()
    {
        return $this->attributes['hold_reason'] = $this->getMeta('reason', false) ? $this->getMeta('reason') : '';
    }
    public function getHoldReleaseDateAttribute()
    {
        return $this->attributes['hold_release_date'] = $this->getMeta('date', false) ? $this->getMeta('date') : '';
    }
    public function getTitle()
    {
        return  isset($this->title_no) ? $this->title_no : "( MOTHER TITLE) ". $this->product->title_no;
    }

}
