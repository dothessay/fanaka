<?php
/**
 * Created by Philip Njuguna.
 * User: Philip Njuguna
 * Date: 11/13/18
 * Time: 11:18 AM
 */

namespace App\Plot;


use Codex\Scopes\CanSellScope;

class CanSellPlot extends Plot
{


    protected $table = 'plots';



    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope(new  CanSellScope());
    }


    public function availableForSale()
    {

        if (($this->is_reserved || $this->is_sold) & ! $this->can_sell)
        {

            return false;
        }


        return true;
    }


}