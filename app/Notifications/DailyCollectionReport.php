<?php

namespace App\Notifications;

use App\DB\Accounting\Account;
use App\Sale\Sale;
use App\User;
use Carbon\Carbon;
use Codex\Classes\PdfOutput\DataReport;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Mail;

class DailyCollectionReport extends Notification
{
    use Queueable;
    /**
     * @var User
     */
    public $user;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(User $user)
    {
        //
        $this->user = $user;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        try{

            $time = "Daily_collection_report";
            $this->makeReport($time);
            Artisan::all("notification:agreement");

            return (new MailMessage)
                ->greeting('Good Evening ' . $this->user->fullName())
                ->bcc("philipnjuguna66@gmail.com")
                ->subject("Today's Collection")
                ->line("Find the attached document of today's  total collection")
                ->attach(base_path($time.'.pdf'));

        }catch (\Exception $exception)
        {
            return Mail::raw($exception, function ($mail) {
                $mail->to('philipnjuguna66@gmail.com');
            });
        }



    }

    private function makeReport($time)
    {

        $accounts = Account::whereBetween('created_at',[
            Carbon::now()->startOfDay() , Carbon::now()->endOfDay()
        ])->get();

        $html = view('account._data')->with([
            'accounts' => $accounts
        ])->render();

        return (new DataReport())->emailOutPut($html,$time,'F');


    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
