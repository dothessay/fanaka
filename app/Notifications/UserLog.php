<?php

namespace App\Notifications;

use App\DB\Log\Log;
use App\User;
use Carbon\Carbon;
use Codex\Classes\PdfOutput\DataReport;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Support\Facades\Mail;

class UserLog extends Notification
{
    use Queueable;
    /**
     * @var User
     */
    public $user;

    /**
     * Create a new notification instance.
     *
     * @param User $user
     */
    public function __construct(User $user)
    {
        //
        $this->user = $user;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */


    public function toMail($notifiable)
    {
        try{

            $time = "daily_user_log_report";
            $this->makeReport($time);

            return (new MailMessage)
                ->greeting('Good Evening ' . $this->user->fullName())
                ->bcc("philipnjuguna66@gmail.com")
                ->subject("Today's User Logs activates")
                ->line("Find the attached document of today's  users activates time logged and time logged out")
                ->line('Logout may be nullable')
                ->attach(base_path($time.'.pdf'));

        }catch (\Exception $exception)
        {
            return Mail::raw($exception, function ($mail) {
                $mail->to('philipnjuguna66@gmail.com');
            });
        }



    }

    private function makeReport($time)
    {


        $logs = Log::whereBetween('created_at',[Carbon::now()->startOfDay() , Carbon::now()])->get();


        $html = view('users.details._logs')->with([
            'logs' => $logs,
            ])->render();

        return (new DataReport())->emailOutPut($html,$time,'F');


    }


    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
