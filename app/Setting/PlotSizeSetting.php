<?php

namespace App\Setting;
use Codex\Classes\Model;


class PlotSizeSetting extends Model
{
    protected $guarded = [];
    
    public function scopeDescId($query)
    {
        return $query->orderBy('label' ,'desc');
    }
}
