<?php

namespace App\Plots;


use App\Plot\Plot;
use App\User;
use Codex\Classes\Model;

class PlotUpload extends Model
{

    protected $guarded = [];

    public function plot()
    {
        return $this->belongsTo(Plot::class, 'plot_id','id');

    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id','id');

    }

    public function uploadToArray()
    {

        return [
            'id'           => $this->id,
            'plot'           => $this->plot->plot_no,
            'uploaded_by'  => $this->user->fullname(),
            'url'          => $this->upload_url,
            'file_name'    => $this->file_name,
            'type'         => $this->type,
        ];

    }
}
