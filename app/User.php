<?php

namespace App;

use App\Customer\Customer;
use App\DB\Business\DepartmentAdmin;
use App\DB\Communication\EmailSetting;
use App\DB\Customer\Referral;
use App\DB\Leave\LeaveRequest;
use App\DB\Log\Log;
use App\DB\Role\Business;
use App\DB\Role\Role;
use App\DB\Sale\OfferLetter;
use App\DB\User\DeactivatedAgentCustomer;
use App\Sale\Sale;
use App\Traits\IsMetable;
use App\Traits\IsPermissibleUser;
use Carbon\Carbon;
use Codex\Classes\Reports\Sales\Perfomance\Sales;
use Codex\Scopes\IsActiveScope;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laravel\Passport\HasApiTokens;

class User extends Authenticatable
{

    use Notifiable, IsPermissibleUser, SoftDeletes , IsMetable, HasApiTokens;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = [];
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * @return bool
     */
    public function canBeImpersonated()
    {
        return true;
    }

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);

        $this->permissionRelation = 'user';
    }

    protected static function boot()
    {
        parent::boot();

        //static::addGlobalScope(new  IsActiveScope());
    }

    public function getFirstNameAttribute($value)
    {
        return $this->attributes['first_name'] = ucwords(strtolower($value));
    }

    public function getLastNameAttribute($value)
    {
        return $this->attributes['last_name'] =  ucwords(strtolower($value));
    }

    public function business()
    {
        return $this->belongsTo(Business::class, 'business_code', 'code');

    }

    public function role()
    {
        return $this->belongsTo(Role::class, 'role_id', 'id');

    }

    public function emailSetting()
    {
        return $this->hasOne(EmailSetting::class, 'user_id', 'id');

    }

    public function sales()
    {

        return $this->hasMany(Sale::class, 'agent', 'id');

    }

    public function referrals()
    {

        return $this->hasMany(Referral::class, 'agent_id', 'id');

    }

    public function offers()
    {

        return $this->hasMany(OfferLetter::class, 'agent_id', 'id');

    }

    public function leaveRequests()
    {

        return $this->hasMany(LeaveRequest::class, 'user_id', 'id');

    }

    public function handlingRequests()
    {


        return $this->hasMany(LeaveRequest::class, 'handling_user_id', 'id');

    }

    public function handleClients()
    {

        return $this->hasMany(LeaveRequest::class, 'handle_clients_user_id', 'id');

    }
    public function departmentAdmin()
    {

        return $this->hasOne(DepartmentAdmin::class, 'user_id', 'id');

    }

    public function deactivatedAgentCustomers()
    {
        return $this->hasMany(DeactivatedAgentCustomer::class ,'user_id','id');

    }


    public function logs()
    {
        return $this->hasMany(Log::class , 'user_id','id');

    }

    public function customers()
    {
        $myArray = collect();


        if (! request()->user()->can('access-module-component-functionality', 'sale_manage_show-all-sales')){


            $sales = $this->sales;

            foreach ($sales as $sale) {


                foreach ($sale->customers as $customer) {
                    $myArray->push($customer);
                }

            }

            foreach ($this->offers as $offer) {

                foreach ($offer->customers as $customer) {

                    $myArray->push($customer);
                }
            }
            if ( isset($this->departmentAdmin))
            {
                foreach($this->departmentAdmin->role->users as $user)
                {

                    foreach ($user->sales as $sale) {


                        foreach ($sale->customers as $customer) {
                            $myArray->push($customer);
                        }

                    }

                    foreach ($user->offers as $offer) {

                        foreach ($offer->customers as $customer) {

                            $myArray->push($customer);
                        }
                    }
                }
            }


            /**get handling clients on leave **/


            $leave = $this->handleClients
                ->where('last_day', '>=', now()->startOfDay())
                ->first();


            if (!is_null($leave)) {
                if (Carbon::parse($leave->return_at)->endOfDay()->lessThan(now()->endOfDay())) {

                } else {

                    $userHandling = $leave->handleClient;

                    if ($userHandling->id == $this->id) {
                        $user = $leave->user;

                        foreach ($user->sales as $sale) {


                            foreach ($sale->customers as $customer) {
                                $myArray->push($customer);
                            }

                        }
                        foreach ($user->offers as $offer) {

                            foreach ($offer->customers as $customer) {

                                $myArray->push($customer);
                            }
                        }

                    }


                }


            }

        }

        foreach ($this->deactivatedAgentCustomers as $deactivatedAgentCustomer) {

           foreach ( $deactivatedAgentCustomer->agentCustomers() as  $customer){
               $myArray->push($customer);
           }

        }

        if (request()->user()->can('access-module-component-functionality', 'sale_manage_show-all-sales')){


           foreach (Customer::all() as $customer)
           {
               $myArray->push($customer);

           }

           return $myArray;
        }




        return $myArray->unique('id');


    }


    public function getSales()
    {

    }

    public function scopeActive($query)
    {
        return $query->where('is_active', true);

    }

    public function userType()
    {
        return $this->role->label;

    }

    public function fullName()
    {
        return ucwords( strtolower( $this->first_name . " " . $this->last_name));

    }

    public function getUserType()
    {
        return $this->role->name;

    }

    public function scopeIsAgent($query)
    {

        return $query->where('role_id', 2);


    }

    public function image()
    {
        $userMail = $this->email;

        $imageWidth = '16'; //The image size
        $imageWidth = '16';
        $imgUrl = 'http://www.gravatar.com/avatar/' . md5($userMail) . 'fs=' . $imageWidth;

        return $imgUrl;

    }

    public function getLeaveRequests()
    {

        if (!$this->can('access-module-component-functionality', 'users_leave_view-requests')) {

            return $this->leaveRequests;


        }
        return LeaveRequest::all();

    }

    public function getHandlingRequests()
    {

        if (!$this->can('access-module-component-functionality', 'users_leave_view-requests')) {

            return $this->handlingRequests;


        }
        return LeaveRequest::all();

    }

    public function getLeaveBalance()
    {

        return $this->remaining_off_days;

    }

    public function path()
    {
        if (!auth()->user()->can('access-module-component-functionality', 'sale_manage_show-all-sales')) {

            return "javascript::void()";
        }
        return url('users/manage/' . $this->id . '/details');


    }

    public function userToArray()
    {
        return [
            'id'   => $this->id,
            'tel'  => $this->phone_number,
            'email' => $this->email,
            'name' => $this->fullName(),
        ];
    }
    public function salesToArray()
    {
        return $this->sales;
    }

    public function unpaid()
    {
        $invoices = collect();

        foreach ((new Sales())->all() as  $sale)
        {
            if ($sale->getBalance() > 1)
            {
                $invoices->push($sale->invoices);
            }
        }

        return $invoices;
    }
}
