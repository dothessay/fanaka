<?php

namespace App\Sale;

use App\DB\Product\Product;
use App\Plot\Plot;
use Illuminate\Database\Eloquent\Model;

class SaleItem extends Model
{
    protected $guarded =[];

    //protected $with = ['sale'];

    protected $appends = [
        'sale_type'
    ];


    public function plot()
    {
        return $this->belongsTo(Plot::class,'plot_id','id');
    }

    public function project()
    {
        return $this->belongsTo(Product::class,'product_id','id');
    }

    public function salePayment()
    {
        return $this->hasMany(SalePayment::class, 'sale_id', 'sale_id');

    }

    public function getPlotNoAttribute($value)
    {
        return $this->attributes['plot_no'] = $this->plot->plot_no;

    }

    public function sale()
    {
        return $this->belongsTo(Sale::class,'sale_id','id');
    }


    public function getPriceAttribute($value)
    {
        return $this->attributes['price'] = floatval($value);

    }

    public function saleItemToArray()
    {
        return [
            'project_code'  => $this->plot->product->code,
            'landLocation' => $this->plot->product->name(),
            'id'           =>  $this->plot->id,
            'plotNo'       =>  $this->plot->plot_no,
            'size'         =>  $this->size->label,
            'price'        =>  number_format($this->price , 2),
            'details'     =>  $this->plot->getPropertyDetails(),
        ];

    }


    public function getSaleTypeAttribute()
    {
        if ($this->price > $this->plot->price){
            return $this->attributes['sale_type'] = 'instalment';
        }
        if ($this->price <= $this->plot->price){
            return $this->attributes['sale_type'] = 'cash';
        }

    }
}
