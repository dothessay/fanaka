<?php

namespace App\Sale;

use App\Plot\Plot;
use Illuminate\Database\Eloquent\Model;

class InvoiceItem extends Model
{
    protected $guarded = [];

    public function plot()
    {
        return $this->belongsTo(Plot::class,'plot_id','id');


    }
    public function invoice()
    {
        return $this->belongsTo(Invoice::class,'invoice_id','id');


    }

}
