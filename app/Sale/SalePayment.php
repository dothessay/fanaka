<?php

namespace App\Sale;

use App\DB\Accounting\Account;
use App\DB\Finance\ChartOfAccount;
use App\DB\Finance\JournalEntry;
use App\Traits\IsJournable;
use App\User;
use Carbon\Carbon;
use Codex\Classes\Model;
use Codex\Scopes\IsApproved;

use Illuminate\Database\Eloquent\Builder;

class SalePayment extends Model
{
    protected $guarded = [''];

    use IsJournable;

    protected $dates = ['created_at','deleted_at','deposit_date'];


    protected $casts = [
        'amount' => 'double'
    ];

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);

        parent::boot();

        static::addGlobalScope(new IsApproved());

    }

    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope('created_at', function (Builder $builder) {
            $builder->orderBy('created_at', 'desc');
        });
    }



    public function payables()
    {
        return $this->morphMany(Account::class, 'payable');

    }

    public function payable()
    {
        return $this->morphOne(Account::class, 'payable');

    }

    public function journalable()
    {
        return $this->morphMany(JournalEntry::class ,'journalable');

    }

//    public function save(array  $options = [])
//    {
//        $chart = ChartOfAccount::where('code', '5600/000')->first();
//
//
//        if (! is_null($chart))
//        {
//
//            $journal = $this->addToJournal([
//                'chart_id'  => $chart->id,
//                'credit'    => $this->amount,
//                'debit'    => 0
//            ]);
//
//            $this->saveJournal($journal);
//
//        }
//
//        parent::save();
//
//    }




    public function savePayable(Account $account)
    {
        return $this->payables()->save($account);
    }

    public function saveJournal(JournalEntry $journalEntry)
    {
        return $this->journalable()->save($journalEntry);
    }


    public function setAmountAttribute($value) {

        // grab the title and slugify it
        return $this->attributes['amount'] = (double) (str_replace(',','',$value));
    }

    public function getAmountAttribute($value) {

        // grab the title and slugify it
        return $this->attributes['amount'] =  ceil((double)(str_replace(',','',$value)));
    }


    public function getCreatedAtAttribute($value)
    {
        return $this->attributes['created_at'] = Carbon::parse($value)->format('Y-m-d H:i');

    }


    public function getDepositDateAttribute($value)
    {
        return $this->attributes['deposit_date'] =  isset($value) ? Carbon::parse($value)->format('Y-m-d') : null;

    }

    public function getReferenceCodeAttribute($value)
    {
        $paymentMethod =  strtolower($this->payment_method);


        if ( $paymentMethod === 'bank'){
            return $this->attributes['reference_code'] = $this->slip_no;
        }
        if ($paymentMethod === 'cheque'){
            return $this->attributes['reference_code'] = "CHEQUE";
        }
        if (strtoupper($value) === 'N/A' || is_null($value))
        {
            $value = $this->payment_method;
        }

        return $this->attributes['reference_code'] = $value;



    }




    public function getSlipNoAttribute($value)
    {
        if (strtoupper($value) === 'N/A')
        {
            $value = '';
        }

        return $this->attributes['slip_no'] = $value;
    }



    public function user()
    {
        return $this->belongsTo(User::class, 'user_id','id');
    }


    public function sale()
    {
        return $this->belongsTo(Sale::class, 'sale_id','id');
    }

    public function paymentToArray()
    {

        return [
            'id'             => $this->id,
            'date'  =>   Carbon::parse($this->created_at)->format('Y-m-d H:i:s'),
            'deposit_date'  =>   $this->deposit_date,
            'amount'        => $this->amount,
            'payment_method'  => $this->payment_method,
            'reference_code'  => $this->reference_code,
            'customer'        => $this->sale->getCustomerHref(),
            'projects'        => $this->sale->getPropertyDetails()
        ];

    }

    public function path()
    {
        return url('sale/manage/approve-sale/'. $this->sale->id);

    }
}
