<?php

namespace App\Sale;

use App\Customer\Customer;
use App\DB\Accounting\Account;
use App\DB\Business\Meta;
use App\DB\PrintContract;
use App\DB\Role\Business;
use App\DB\Sale\Calculator;
use App\DB\Sale\CustomerSale;
use App\DB\Sale\Installment;
use App\DB\Sale\InterestSale;
use App\DB\Sale\MonthlyInstallmentPlan;
use App\DB\Sale\SaleDirector;
use App\DB\Sale\SaleExtraNote;
use App\DB\Sale\SaleUpload;
use App\DB\Witness;
use App\Traits\IsMetable;
use App\User;
use Carbon\Carbon;
use Codex\Classes\Helper;
use Codex\Classes\Model;
use Codex\Classes\Reports\GuessPrepaid;
use Codex\Scopes\MyCustomers;
use Codex\Scopes\MySales;

class Sale extends Model
{

     use IsMetable;

    protected $guarded = [];

    protected $casts = [
        'total_amount' => 'double',
        'is_approved' => 'boolean',
        'title_fee' => 'boolean',
        'agreement_request' => 'boolean',
    ];

    protected $metas = [
            'discount'
    ];

    protected $with = ['user','approvedBy','soldBy'];


    private $hasNextOfKin = false;

    protected $appends = [
        'customer_details',
        'color',
        'balance'
    ];
    /**
     * @var GuessPrepaid $instance
    */

    protected  $instance;


     public function __construct(array $attributes = [])
     {
         parent::__construct($attributes);

         static::addGlobalScope(new MySales());
     }




    public function business()
    {
        return $this->belongsTo(Business::class ,'business_code','code');
    }
    public function calculators()
    {
        return $this->hasMany(Calculator::class ,'sale_id','id');
    }

    public function interests()
    {
        return $this->hasMany(InterestSale::class ,'sale_id','id');
    }

    public function getCustomerDetailsAttribute()
    {
        return $this->getCustomerDetails();
    }

    public function accounts()
    {
        return $this->morphMany(Account::class, 'accountable');

    }

    public function directors()
    {
        return $this->hasMany(SaleDirector::class, 'sale_id','id');

    }
    public function saleItems()
    {
        return $this->hasMany(SaleItem::class, 'sale_id','id');
    }
    public function salePayments()
    {
        return $this->hasMany(SalePayment::class, 'sale_id','id');
    }
    public function approvedBy()
    {
        return $this->belongsTo(User::class, 'approved_by','id');
    }
    public function soldBy()
    {
        return $this->belongsTo(User::class, 'agent','id');
    }
    public function user()
    {
        return $this->belongsTo(User::class, 'user_id','id');
    }
    public function invoices()
    {
        return $this->hasOne(Invoice::class, 'sale_id','id');
    }
    public function invoice()
    {
        return $this->belongsTo(Invoice::class, 'sale_id','id');
    }
    public function notes()
    {
        return $this->hasMany(SaleExtraNote::class, 'sale_id','id');
    }


    public function saleUploads()
    {
        return $this->hasMany(SaleUpload::class,'sale_id','id');

    }



    public function agreement()
    {
        return $this->hasOne(PrintContract::class,'sale_id','id');

    }

    public function account(Account $account)
    {
        return $this->accounts()->save($account);
    }


    public function getTotalAmountAttribute($value) {


        return $this->attributes['total_amount'] =  floatval($this->saleItems->sum('price') > 1 ? $this->saleItems->sum('price') : 1 );
    }


    public function getBalanceAttribute($value) {


        return $this->attributes['balance'] =  $this->getBalance();
    }
    public function getMonthlyInstalment()
    {
        $amount = $this->getBalance();

        if ( isset($this->installment->amount))
        {
            $amount = (new GuessPrepaid($this))->getMonthlyPlanAmount();

        }
        return $amount;
    }
    public function getInstalmentMonths()
    {
        $months = 0;

        if ( $this->installmentPlans->count() - 1)
        {
            $months = $this->installmentPlans->count() - 1;
        }
        return $months ;
    }


    public function payments()
    {
        return $this->salePayments;

    }
    public function lastPaymentDate()
    {
        return isset($this->salePayments->first()->created_at) ? Carbon::parse($this->salePayments->first()->created_at) : null;

    }

    public function lastPaymentAmount()
    {
        return isset($this->salePayments->first()->created_at) ? $this->salePayments->first()->amount  : 0;

    }

    public function customer()
    {
        $customerDetails = [];
        foreach ($this->customers as $customer) {

            $customerDetails[] = $customer->contactPoint() ;

        }
        return  collect($customerDetails)->implode(',', '');
    }

    public function getTotalSaleAmount()
    {
        return (floatval($this->saleItems->sum('price')) -  floatval($this->getM));

    }
    public function getCustomerPlotNos()
    {

        $customerDetails = $this->saleToArray()['plots']->map(function ($plot) { return  $plot['landLocation']. ' Plot No: ' .$plot['plotNo'] ; })->toArray();

        return  collect($customerDetails)->implode(',', '');

    }

    public function contactPoint()
    {

        $customerDetails = [];

        foreach ($this->customers as $customer) {

            $customerDetails[] = $customer->contactPoint() ;

        }

        return  collect($customerDetails)->implode(',', '');
    }

    public function customers()
    {

        return $this->belongsToMany(Customer::class ,'customer_sales')->withoutGlobalScopes([ MyCustomers::class ]);
    }
    public function customerSales()
    {
        return $this->hasMany(CustomerSale::class, 'sale_id','id')->withoutGlobalScopes([ MyCustomers::class]);
    }
    public function installment()
    {
        return $this->hasOne(Installment::class, 'sale_id','id');
    }
    public function installmentPlans()
    {
        return $this->hasMany(MonthlyInstallmentPlan::class, 'sale_id','id');
    }
    public function witnesses()
    {
        return $this->hasMany(Witness::class, 'sale_id','id');
    }


    public function intialPayment()
    {
        return floatval($this->salePayments->last()->amount);

    }

    public function nextOfKins()
    {
        $nextOfKinArray = collect();

        foreach ($this->customers as $customer){

            foreach ($customer->nextOfKins as $nextOfKin) {

                $nextOfKinArray->push($nextOfKin);
            }
        }

        return $nextOfKinArray;
    }


    public function getHasNextOfKin()
    {
        foreach ($this->customers as $customer) {
            foreach ($customer->nextOfKins as $nextOfKin) {
                return true;
            }
            return  false;
        }
    }
    public function getNextOfKin()
    {
        $nextOfKinArray = [];

        foreach ($this->customers as $customer){

            //$customer = $customerSale->customer;

            foreach ($customer->nextOfKins as $nextOfKin) {

                $nextOfKinArray[] ="<br/><br/>Name: ". $nextOfKin->fullName() . "&nbsp;&nbsp;&nbsp;<br/>Phone Number  : &nbsp;".$nextOfKin->phone_number ."&nbsp;&nbsp;&nbsp; <br/>Relation :" .$nextOfKin->relation."</br>";
            }
        }
        //dd($nextOfKinArray);
        return  collect($nextOfKinArray)->implode(',', '');

    }

    public function getPercentage()
    {
        try{
            $totalPaid = $this->salePayments->sum('amount');

            $totalAmount = $this->getTotalSaleAmount();
            $percentage = ( floatval($totalPaid) * 100 ) / floatval($totalAmount) ;
            return floatval($percentage);
        }
        catch (\Exception $e){
            return 0;
        }
    }


    public function getBalance()
    {
        $balance =  floatval($this->saleItems->sum('price')) - floatval($this->getPaidAmount());

        return floatval($balance);

    }


    /**
     * @return mixed
     */
    public function getPaidAmount()
    {
        return floatval($this->salePayments->sum('amount') );

    }

    public function getInitialAmount()
    {
        if (! $this->salePayments->count()){

            return floatval(0);
        }

        $intitalDate = Carbon::parse($this->salePayments->last()->created_at)->format('Y-m-d');

        $amount = 0 ;
        $payments = SalePayment::where('sale_id', $this->id)
            ->whereBetween('created_at',[
                $intitalDate , Carbon::parse($intitalDate)->endOfDay()
            ])->get();


        foreach ($payments as $payment) {

            $amount += $payment->amount;

        }

        /*if ($this->hasAgreement()) {

            $amount = $this->salePayments->sum('amount');
        }*/

        return floatval($amount);

    }

    /**
     * @return string
     */
    public function getContractAmountInStatement()
    {

        $months = isset($this->installment->months) ? $this->installment->months : 6;

        //dd($this->installment->months);

        $installmentAmount = $this->getInstallment();

        $soFarPaid = "";


        if (floatval($this->getInitialAmount()) != floatval($this->getPaidAmount()))
        {
            $soFarPaid = "The Purchaser has so far deposited<b>
              ". Helper::numberToWord(floatval($this->getPaidAmount()))." (".  number_format((double)$this->getPaidAmount() , 2).   ") </b>";

        }


        if ($this->getBalance() > (double) 0)
        {


            $statement =  "an initial amount of <b>". Helper::numberToWord(floatval($this->getInitialAmount())) ." 
             ( ". number_format(floatval($this->getInitialAmount()), 2) ." ) </b> on <b>".Carbon::parse($this->salePayments->last()->created_at)->format('Y-F-d') ."</b> {$soFarPaid}. The purchaser will be 
             depositing ". strtolower(ucfirst(' <b> ' . Helper::numberToWord((double)$installmentAmount) . '  
             ( ' . number_format($installmentAmount  , 2). ')</b> EVERY MONTH FOR THE NEXT  '
                . $months ? : 1 . '  ')) . " months from <b>".Carbon::parse($this->salePayments->last()->created_at)->format('Y-F-d')." </b>.The full amount should be paid by or before <b>". Carbon::parse($this->salePayments->last()->created_at)->addMonths($months)->format('Y-F-d') .'</b>';


            return $statement;


        }
        return "full amount of " . Helper::numberToWord(floatval($this->salePayments->sum('amount'))) ."  ". number_format((double)$this->salePayments->sum('amount') , 2);
        //return "an initial amount of ". Helper::numberToWord(floatval(610000)) ."  ( ". number_format(floatval(610000), 2) ." )";

    }

    public function getInstallment()
    {
        return isset($this->installment) ? $this->installment->amount : 0 ;
    }


    /**
     * @return string
     */
    public function getCustomerDetails()
    {

        $customerDetails = [];

        foreach ($this->customers as $customer) {

            $customerDetails[] = $customer->fullName();
        }

        return  collect($customerDetails)->implode(',', '');

    } /**
     * @return string
     */
    public function getCustomerPhoneNumber()
    {

        $customerDetails = [];

        foreach ($this->customers as $customer)
        {
            $customerDetails[] = $customer->getPhoneNumber();


        }
        $customerDetails = collect($customerDetails)->filter()->toArray();

        return  collect($customerDetails)->implode(',', '');


    }

    public function getCustomerDetailsForAgreement()
    {

        $customerDetails = [];

        foreach ($this->customers as $customer) {

            $customerDetails[] = ' <b> ' . strtoupper($customer->fullName()) . ' </b> of ID  :   '.$customer->getCustomerId() .  '    ,and of phone number: ' . $customer->getPhoneNumber() . '   ';

        }
        return  collect($customerDetails)->implode(',', '');



    }

    public function getCustomerPlots()
    {
        $plots = collect($this->saleToArray()['plots'])->map(function ($q) {

            return $q['details']." ";

        })->toArray();

        return  collect($plots)->implode(',', '');

    }

    public function getPlotDetails()
    {

        $plotNo = [];
        foreach ($this->saleItems as $saleItem) {

            $plotNo[] =  $saleItem->plot->getPropertyDetails();
        }
        $plotNo = collect($plotNo)->unique()->toArray();

        return  collect($plotNo)->implode(',', '');

    }
    public function getProject()
    {

        $plotNo = [];
        foreach ($this->saleItems as $saleItem) {

            $plotNo[] =  $saleItem->plot->product->name();
        }
        $plotNo = collect($plotNo)->unique()->toArray();

        return  collect($plotNo)->implode(',', '');

    }
    public function getPlotNo()
    {

        $plotNo = [];

        foreach ($this->saleItems as $saleItem) {

            $plotNo[] =  $saleItem->plot->plot_no;
        }
        $plotNo = collect($plotNo)->unique()->toArray();

        return  collect($plotNo)->implode(',', '');

    }
    public function getPlotIds()
    {

        $plotNo = [];

        foreach ($this->saleItems as $saleItem) {

            $plotNo[] =  $saleItem->plot->id;
        }
        $plotNo = collect($plotNo)->unique()->toArray();

        return  collect($plotNo)->implode(',', '');

    }
    public function getMotherTitle()
    {
        $plotNo = [];
        foreach ($this->saleItems as $saleItem) {

            $plotNo[] =  $saleItem->plot->product->title_no;
        }
        $plotNo = collect($plotNo)->unique()->toArray();

        return  collect($plotNo)->implode(',', '');

    }

    public function getCustomerId()
    {
        $customerDetails = [];

        foreach ($this->customers as $customer) {

            $customerDetails[] = $customer->getCustomerId() .'';

        }

        return  collect($customerDetails)->implode(',', '');

    }
    public function getCustomerHref()
    {
        $customerDetails = [];
        foreach ($this->customers as $customer)
        {
            $customerDetails[] = '   <a title="Click to see customer details" target="_blank" href='.url('customers/manage/statement/'.$customer->id).' >'.$customer->fullName().'</a>   ';

        }

        return  collect($customerDetails)->implode(',', '');

    }


    public function signatories()
    {

        ob_start();

        if ($this->directors->count()){

            ?>

            <b>PURCHASER </b> <br/><br/><br/>
            Name: <?= $this->customers->first()->fullName()?><br>
            <?= $this->customers->first()->contactPoint()?><br>
           Registration Number: <?= $this->customers->first()->getCustomerId() ?><br>


            <br/><b>DIRECTOR ('s)</b> <br/><br/>
            <?php

            foreach ($this->directors as $director) {


                ?>

                <br/> Name: <?= $director->customer->fullName()?>: ..................................................<br/>
                    <?= $director->customer->contactPoint()?><br/>
                <br/>Id No:  <?= $director->customer->getCustomerId()?><br/><br/><br/>

                <?php

            }


            return ob_get_clean();

        }

        ?>
        <b>
            SIGNED BY THE PURCHASER<br/>
            <br/>
            <br/>
        </b>
        <?php
        foreach ($this->customers as $customer) {

            ?>
            Name: <?= $customer->fullName()?>: ..................................................<br/>
            <?= $customer->contactPoint()?><br/>
           Id:  <?= $customer->getCustomerId()?><br/><br/><br/>

            <?php

        }

        return ob_get_clean();

          //return implode($customerDetails, '  ');


    }

    public function F()
    {

        return isset($this->installment) ? $this->installment->amount : 0 ;
    }


    public function getProjects()
    {
        $projects = [];

        foreach ($this->saleItems as $saleItem) {

            $projects[] = $saleItem->plot->product->name();

        }
        $projects = collect($projects)->unique()->toArray();
        return  collect($projects)->implode(',', '');

    }

    public function getPropertyDetails()
    {
        $projects = [];

        foreach ($this->saleItems as $saleItem)
        {
            $projects[] = $saleItem->plot->product->name() .' plot number: '. $saleItem->plot->plot_no .' '. $saleItem->plot->getPropertyApprox() ;

        }
        $projects = collect($projects)->unique()->toArray();

        return  collect($projects)->implode(',', '');

    }
    public function witnessAndAgreement()
    {
        $winteness = "";

        foreach ($this->witnesses as $witness) {

            if ($witness->type == 'advocate')
            {
                $winteness .= "<br/>SIGNED BY THE ADVOCATE:<br/>";
                $winteness .= "<b><br/>
                            
                            NAME: {$witness->fullname()}	………………………………. <br/>
                            ID NO: {$witness->id_no} <br/></b>
                            ";
            }if ($witness->type == 'witness')
            {
                $winteness .= "<br/>SIGNED BY THE WITNESS: <br/>";

                $winteness .= "<b><br/>
                            
                            NAME: {$witness->fullname()}	………………………………. <br/>
                            ID NO: {$witness->id_no} <br/></b>";
            }
        }

        return $winteness;

    }

    public function isTitleInclusive()
    {
        $isInclusive = true;

        foreach ($this->saleItems as $saleItem) {

            if (! $saleItem->plot->product->is_inclusive_title){
                $isInclusive = false;

            }
        }
        return $isInclusive;
    }


    /********** GET THE 50 50 % PAYMENT DATE****/

    public function getHalfPaymentDate()
    {
        return $this->half_payment_at;
    }

    public function hasAgreement()
    {
        if ($this->is_agreement_approved)
        {
            return true;
        }
        return false;

    }

    public function saleToArray()
    {
       if ($this->getBalance() > 0) {
           $installment =  isset($this->installment) ?  $this->installment->installmentToArray()['months']: [];
       }else{
           $installment =  [];
       }
       return [
            'sale_ref'      => $this->id,
            'date'          => Carbon::parse($this->created_at)->format('Y-m-d H:i:s'),
            'sale_date'     => $this->created_at,
            'customers'     => $this->customers->map(function ($customer) { return $customer->customerToArray() ;}),
            'plots'         => $this->saleItems->map(function ($saleItem) { return $saleItem->plot->plotToArray() ;}),
            'balance'       => $this->getBalance(),
            'percentage'    => $this->getPercentage(),
            'amountPaid'    => $this->getPaidAmount(),
            'saleAmount'    => $this->getTotalSaleAmount(),
            'installment'   => $installment,
            'agent'         =>  ucwords(strtolower($this->soldBy->fullName())),
            '50-percent-at' => $this->half_payment_at,
            'payments'      => $this->salePayments->map(function ($payment) { return $payment->paymentToArray() ;}),
            'agreement'     => $this->getAgreement(),
            'uploads'       => $this->saleUploads->where('type','Agreement')->map(function ($upload) { return $upload->uploadsToArray();}),
            'saleItems'     => $this->saleItems->map(function ($item) { return ['price' => $item->price ,'details' => $item->plot->getPropertyDetails()] ; })

        ];
    }

    public function saleUploadsToArray()
    {
        return $this->saleUploads->map->saleUploads();

    }

    public function scopeIsCommissioned($query)
    {

        return $query->where('commission_at', null);
    }

    public function getAgreement()
    {

        return $this->agreement? $this->agreement->getAgreement() : '';
    }

    public function path()
    {
        return url('sale/manage/approve-sale/'. $this->id);

    }

    public function getSaleCompletionDate()
    {

        $completionDate = $this->created_at->copy()->addRealMonths($this->getInstalmentMonths());


        return Carbon::parse($completionDate->format('Y').'-'.$completionDate->format('m').'-'. $this->created_at->format('d'))->format('d F Y');
        
    }

    public function getBlockNo()
    {
        $block = $this->saleItems->map(function ($item) {
            return  $item->plot->getTitle();
        })->unique()->toArray();


        return  collect($block)->implode(',', '');

    }
    public function sizeApproximate()
    {
        $block = $this->saleItems->map(function ($item) {
            return $item->plot->getPropertyApprox() ;
        })->unique()->toArray();

        return  collect($block)->implode(',', '');

    }




    public function getColorAttribute()
    {
        return $this->attributes['color']  = "";
    }

    public function getPrepaidAmount()
    {
       $guessPrepaid =  $this->guessPrepaid();

       $amount = $this->getPaidAmount() - $guessPrepaid->exceptedAmountToBePaidByNow();

       if ($amount > 1)
       {
           return $amount;
       }
       return 0;
    }
    public function getMonthlyAmount()
    {
       return $this->guessPrepaid()->getMonthlyPlanAmount();

    }
    /**
     * @return  GuessPrepaid
    */
    public function guessPrepaid() : GuessPrepaid
    {
        if (! $this->instance)
        {
            $this->instance =  (new GuessPrepaid($this));
        }
        return $this->instance;
    }
}

