<?php

namespace App\Console\Commands;

use App\DB\Notification\Notification;
use App\Jobs\SendSmsNotifications;
use App\Sale\Invoice;
use App\Sale\Sale;
use App\User;
use Carbon\Carbon;
use Codex\Classes\Handlers\SendAdvantaAfricaInnoxNotification;
use Codex\Classes\Handlers\SendInnoxNotification;
use Codex\Classes\Handlers\SystemSms;
use Codex\Classes\Reports\GuessPrepaid;
use Codex\Classes\Reports\InstallementPlanReport;
use Codex\Classes\Repository\MonthlyInstallmentPrepaid;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Mail;

class InstalmentReminder extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'notification:installmentReminder';


    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'send notification an approaching due date';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->info('started');

        if (! isset( request()->user()->id)) {

            request()->setUserResolver(function () {
                return User::first();
            });
        }
        $this->getMessage();

        $this->info('finished sending');

    }

    private function getMessage()
    {
        $this->advanceMessage();
        //$this->afterDueMessage();
       // $this->afterNewDateMessage();
        //$this->penaltyMessage();

    }
    private function advanceMessage()
    {
        // get this week expected payments for the current week
        foreach ([7, 3,1]  as $startDate)
        {
            $nextWeekPayments = (new InstallementPlanReport())
                ->setEndDate(now()->subRealDays($startDate)->endOfDay())
                ->setStartDate(now()->endOfDay())
                ->plan();
            foreach ($nextWeekPayments as $plan)
            {

                $date = Carbon::parse($plan->payment_date);

                $daysRemaining = now()->diffInDays(Carbon::parse($plan->payment_date));

                $newDate = now()->addRealDays($daysRemaining);
                if (! $daysRemaining) {

                    $daysRemaining = "1";
                }


                $amount = number_format(round($plan->sale->guessPrepaid()->getMonthlyPlanAmount() - $plan->amount, PHP_ROUND_HALF_UP) + $plan->penalty , 2);


                if ($amount >1) {

                    $message = "Hi {$plan->sale->getCustomerDetails()}, your Instalment for{$date->format('\\ F Y ')}of Ksh {$amount} will be over due in {$daysRemaining} days. Kindly settle it by {$newDate->format('l jS \\of F Y h:i:s A')}. Call 0799000111 for any inquiries.";

                    foreach ($plan->sale->customers->pluck('phone_number')->toArray() as $phoneNumber) {
                        SendInnoxNotification::build()
                            ->sendSms(new SendAdvantaAfricaInnoxNotification(), [
                                'to' => $phoneNumber,
                                'message' => $message
                            ]);
                    }
                    $this->info($message);
                }




            }

        }
    }
    private function afterDueMessage()
    {

        // get More than last month defaulters penalty
        $lastMonth = (new InstallementPlanReport())
            ->setEndDate( Carbon::parse(now()->subRealDays(1))->endOfDay())
            ->setStartDate(now()->subRealDays(7)->startOfWeek())
            ->plan();
        foreach ($lastMonth as $plan)
        {
            $date = Carbon::parse($plan->payment_date);

            $daysRemaining = now()->diffInDays(Carbon::parse($plan->payment_date));

            $newDate = now()->addRealDays($daysRemaining);
            $amount = number_format(round($plan->amount, PHP_ROUND_HALF_UP) + $plan->penalty , 2);
            $message = "Hi {$plan->sale->getCustomerDetails()}, your Instalment for{$date->format('\\ F Y ')}of Ksh {$amount} is already over due by {$daysRemaining} days. Kindly settle it by {$newDate->format('l jS \\of F Y h:i:s A')} to avoid penalties. Call 0799000111 for any inquiries.";

            foreach ($plan->sale->customers->pluck('phone_number')->toArray() as $phoneNumber)
            {
                SendInnoxNotification::build()
                    ->sendSms( new SendAdvantaAfricaInnoxNotification() , [
                        'to'    => $phoneNumber,
                        'message'   => $message
                    ]);
            }
            $this->info($message);
        }
    }
    private function afterNewDateMessage()
    {

        //return "Hi {$sale->getCustomerDetails()}, your Instalment for{$date->format(' \\ F Y ')}of Ksh {$sale->monthly_instalment} is over due {$daysRemaining} days. Kindly settle it by {$newDate->format('l jS \\of F Y h:i:s A')}. Call 0799000111 for any inquiries";



    }
    private function penaltyMessage()
    {

        // get More than last month defaulters penalty
        $lastMonth = (new InstallementPlanReport())
            ->setEndDate( Carbon::parse(now()->subRealDays(1))->endOfDay())
            ->setStartDate(now()->subRealDays(7)->startOfWeek())
            ->plan();
        foreach ($lastMonth as $plan)
        {
           if (Carbon::parse($plan->sale->created_at)->greaterThan(Carbon::parse(date('2019/11/06'))))
           {
               $date = Carbon::parse($plan->payment_date);

               $daysRemaining = now()->diffInDays(Carbon::parse($plan->payment_date));

               $newDate = now()->addRealDays($daysRemaining);
               $amount = number_format(round($plan->sale->guessPrepaid()->getMonthlyPlanAmount() - $plan->amount, PHP_ROUND_HALF_UP) + $plan->penalty , 2);


               if ($amount >1)
               {
                   $message = "Hi {$plan->sale->getCustomerDetails()}, your Instalment for{$date->format('\\ F Y ')}of Ksh {$amount} is already over due by {$daysRemaining} days. Kindly settle it by {$newDate->format('l jS \\of F Y h:i:s A')} to avoid penalties. Call 0799000111 for any inquiries.";

                   foreach ($plan->sale->customers->pluck('phone_number')->toArray() as $phoneNumber)
                   {
                       SendInnoxNotification::build()
                            ->sendSms( new SendAdvantaAfricaInnoxNotification() , [
                                'to'    => $phoneNumber,
                                'message'   => $message
                            ]);
                   }
               }
               $this->info($message);
           }
        }

       // return "Hi {$sale->getCustomerDetails()}, your Instalment for{$date->format(' \\ F Y ')}of Ksh {$sale->monthly_instalment} is over due in {$daysRemaining} days. Kindly settle it by {$newDate->format('l jS \\of F Y h:i:s A')}. Call 0799000111 for any inquiries";

    }


}
