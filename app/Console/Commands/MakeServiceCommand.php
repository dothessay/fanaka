<?php
/**
 * Created by Philip Njuguna.
 * User: Philip Njuguna
 * Date: 11/13/18
 * Time: 5:19 PM
 */

namespace App\Console\Commands;


use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputArgument;

class MakeServiceCommand extends Command
{

    protected $signature = 'make:service {serviceName}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate a new service.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //
        $parameter = $this->arguments()['serviceName'];

        $structure = (explode('/',$parameter));

        $length = count($structure);


        //create directories
        $dir  = '/Codex/Services';
        $namespace  = 'Codex\Services';


        for ($i=0; $i < $length-1; $i++)
        {
            $dir = $dir.'/'.$structure[$i];
            $namespace = $namespace.'\\'.$structure[$i];
            if(! is_dir(base_path().$dir))
            {
                mkdir(base_path().$dir);
            }
        }


        $class = $structure[$length-1];


        $fileContent = "";
        $fileContent.="<?php \n\n";
        $fileContent.="\nnamespace ".$namespace.";";
        $fileContent.="\nuse Codex\Classes\ServiceAbstract;";
        $fileContent.="\n\nClass ".$class.' extends ServiceAbstract';
        $fileContent.="\n{";
        $fileContent.="\n";
        $fileContent.="\n\t/**\n\t* @param array \$data\n\t* @param array \$options\n\t*/";
        $fileContent.="\n\tprotected function execute(array \$data, array \$options = [])";
        $fileContent.="\n\t{";
        $fileContent.="\n";
        $fileContent.="\n\t// TODO: Implement execute() method.";
        $fileContent.="\n\t}";
        $fileContent.="\n";
        $fileContent.="\n\t/**\n\t* @param array \$data\n\t* @param array \$options\n\t*/";
        $fileContent.="\n\tprotected function filter(array \$data, array \$options = [])";
        $fileContent.="\n\t{";
        $fileContent.="\n";
        $fileContent.="\n\t// TODO: Implement execute() method.";
        $fileContent.="\n\t}";
        $fileContent.="\n} ";


        //if file already exist don't create overwrite.
        if(file_exists(base_path().$dir.'/'.$class.'.php')){
            $this->info('class '.$class.' already exists!');
            return;
        }


        $fp = fopen(base_path().$dir.'/'.$class.'.php','w');
        fwrite($fp,$fileContent);
        fclose($fp);


        $this->info('Successfully created '.$class);

    }

    protected function getOptions()
    {
        return parent::getOptions();
    }
    protected function getArguments()
    {
        return array(
            array('serviceName',InputArgument::REQUIRED,'The name of the service class.'),
        );
    }
}