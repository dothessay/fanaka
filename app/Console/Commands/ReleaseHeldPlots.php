<?php

namespace App\Console\Commands;

use App\DB\Product\Product;
use App\Plot\Plot;
use App\Sale\SaleItem;
use Carbon\Carbon;
use Illuminate\Console\Command;

class ReleaseHeldPlots extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'plot:release';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Release plots';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $projectsChecks = Product::all();

        foreach ($projectsChecks as $projectsCheck) {

            foreach ($projectsCheck->plots->where('can_sell', false)->where('is_reserved', false) as $plot) {

                if ($plot->getMeta('date', false))
                {
                    $date = $plot->getMeta('date');

                    if (Carbon::parse($date)->isToday()){

                        $plot->can_sell = true;

                        $plot->save();
                    }
                }

            }
        }

    }
}
