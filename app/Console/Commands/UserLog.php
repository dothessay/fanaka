<?php

namespace App\Console\Commands;

use App\User;
use Illuminate\Console\Command;

class UserLog extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'notification:userLog';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send the user login activities';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        $this->info('starting');


        $user = User::where('email','kmuriithi@fanaka.co.ke')->first();

        if (! $user){
            $user = new User();
            $user->email = 'kmuriithi@fanaka.co.ke';
            $user->last_name = 'Muriithi';
            $user->first_name = 'Moses';
        }

        auth()->login($user);



        $user->notify(new \App\Notifications\UserLog($user));

        $this->info('end of sending notification');

    }
}
