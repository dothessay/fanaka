<?php

namespace App\Console\Commands;

use App\DB\Sale\MonthlyInstallmentPrepaid as InstallmentPrepaid;
use App\DB\Sale\OfferLetterItem;
use App\DB\Sale\TitleProcess;
use App\Plot\Plot;
use App\Sale\Sale;
use App\Sale\SaleItem;
use App\Sale\SalePayment;
use App\User;
use Carbon\Carbon;
use Codex\Classes\Handlers\ImapHandler;
use Codex\Classes\Reports\GuessPrepaid;
use Codex\Classes\Repository\MonthlyInstallmentPrepaid;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;


class CommissionCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'commission:update';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update Commissions and prepaids';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //

        $this->calculate();


        $this->info('finished updating every details');

    }

    public function calculate()
    {
        $this->info('start for each payment');
        $sales = Sale::all();
        $request = request();


        foreach ($sales as $sale)
        {
            $request->setUserResolver(function () use ($sale) {
                return $sale->user;
            });
            if ($sale->getBalance() < 1)
            {
                // create a Transfer process

                if ( ! TitleProcess::where('sale_id', $sale->id)->count())
                {
                    TitleProcess::create([
                        'sale_id'  => $sale->id
                    ]);
                }
            }
            $sale->half_payment_at =  null;

            $sale->save();
            $paid = 0;
            $hasCommission = false;

            foreach ($sale->salePayments as $payment)
            {
                $paid += floatval($payment->amount);

                $request->request->add(['month', Carbon::parse($payment->created_at)]);

                $saleAmount = $sale->getTotalSaleAmount();
                $percentage = 0;

                if ($saleAmount)
                {
                    $percentage = ( (100 * $paid) / $saleAmount);
                }


                if ($percentage >= floatval(fanakaConfig('commission_percentage',50)) && ! $hasCommission)
                {
                    $this->info('start for each payment ' . $sale->id);
                    $sale->half_payment_at = Carbon::parse($payment->created_at);
                    $sale->save();
                    $hasCommission =  true;
                }
            }


        }
        $this->info("Finished sales");

        try {
            DB::beginTransaction();

            foreach (SaleItem::all() as $saleItem){
                $plot = $saleItem->plot;
                $plot->is_sold = true;
                $plot->is_reserved = true;
                $plot->can_sell = false;
                $plot->save();
            }
            DB::commit();
        }catch (\Exception $exception)
        {
            DB::rollBack();
        }

    }



}
