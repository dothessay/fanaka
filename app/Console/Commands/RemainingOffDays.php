<?php

namespace App\Console\Commands;

use App\DB\Leave\LeaveRequest;
use Carbon\Carbon;
use Illuminate\Console\Command;

class RemainingOffDays extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'leave:update';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update remaining leave days';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        foreach (LeaveRequest::where('status', 'accepted')
                     ->whereBetween('last_day', [Carbon::now()->startOfWeek() , Carbon::now()->endOfWeek()])->get()  as $leave) {
            $user = $leave->user;


            $user->remaining_off_days = floatval(is_null($user->remaining_off_days) ? 30 : $user->remaining_off_days) - floatval($leave->days_off);


            $user->save();

        }
    }
}
