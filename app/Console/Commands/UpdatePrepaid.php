<?php

namespace App\Console\Commands;

use App\DB\Sale\MonthlyInstallmentPrepaid as InstallmentPrepaid;
use App\Sale\Sale;
use Carbon\Carbon;
use Carbon\CarbonPeriod;
use Illuminate\Console\Command;

class UpdatePrepaid extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'prepaid:update';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update monthly prepaid';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $test = [];
        foreach (Sale::all() as $sale) {
            $date = $sale->created_at;

            request()->setUserResolver(function () use ( $sale){
                return $sale->soldBy;
            });

            $months = $sale->guessPrepaid()->getPassedMonths();

            $monthlyAmount = $sale->guessPrepaid()->getMonthlyPlanAmount();

            $expected = $sale->guessPrepaid()->getRequiredPercentage() + $monthlyAmount;

            $prepaid = $sale->getPaidAmount() - $expected;

            if ($prepaid >= 1)
            {
                //$prepaidMonths = $prepaid / $monthlyAmount;

                $prepaidMonths = ($sale->getPaidAmount() - $sale->guessPrepaid()->getRequiredPercentage() ) / $monthlyAmount;

                $newPeriods = [];

                $periods = CarbonPeriod::create($date->copy()->addRealMonth()->format('Y-m'), $date->copy()->addRealMonths($prepaidMonths)->format('Y-m'));

                // Convert the period to an array of dates
                $dates = $periods->toArray();
                foreach ($dates as $newDate) {

                    $newPeriods[$sale->id][] = $newDate->format('Y-m');
                }

                foreach ($newPeriods as $index => $newPeriod) {

                    $cummulative = 0;

                    foreach (collect($newPeriod)->unique() as $month) {
                        $cummulative += $monthlyAmount;

                        if ($cummulative > $prepaid) {

                            $monthlyAmount = $monthlyAmount - ($cummulative - $prepaid);

                        }

                        InstallmentPrepaid::updateOrCreate([
                            'sale_id' => $sale->id,
                            'month' => $month,
                        ],[
                            'sale_id' => $sale->id,
                            'month' => $month,
                            'amount' => $monthlyAmount,
                        ]);

                    }
                }
            }
        }
    }
}
