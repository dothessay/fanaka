<?php

namespace App\Console\Commands;

use App\DB\Product\Product;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class ReleaseFanakaTestPlots extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'project:release';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $product = Product::where('code', 'fanaka_philip_test')->first();


        if ($product){

            foreach ($product->plots as $plot){



                try{
                    DB::beginTransaction();

                    //mark as available


                    $plot->is_sold = false;
                    $plot->is_reserved = false;

                    $plot->can_sell = true;


                    $plot->save();



                    //soft delete the sale


                    $saleItem = $plot->saleItem;

                    $logData = [
                        'plot_id'  => $plot->id
                    ];


                    if($saleItem)
                    {
                        $sale = $saleItem->sale;


                        $sale->delete();
                    }


                    $offerItem = $plot->offerItem;


                    if ($offerItem) {


                        $offer = $offerItem->offer;

                        $offer->delete();

                    }

                    DB::commit();



                }catch (\Exception $exception)
                {
                    DB::rollBack();



                }

            }
        }


    }
}
