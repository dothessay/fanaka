<?php

namespace App\Console\Commands;

use App\DB\Accounting\Account;
use App\Mail\SendAgreementNotification;
use App\Notifications\DailyCollectionReport;
use App\Sale\Sale;
use App\Setting\Setting;
use App\User;
use Carbon\Carbon;
use Codex\Classes\PdfOutput\DataReport;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Mail;

class DownloadedAgreementReminder extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'notification:collections';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'send Notifications to Moses and Kate to Uploaded Agreement';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        $this->info('starting');


        $user = User::where('email','kmuriithi@fanaka.co.ke')->first();

        if (! $user){
            $user = new User();
            $user->email = 'kmuriithi@fanaka.co.ke';
            $user->last_name = 'Muriithi';
            $user->first_name = 'Moses';
        }

        auth()->login($user);

        $setting = Setting::where('key', 'send_sms')
            ->first();

        if ($setting)
        {
            $setting->value = true;
            $setting->save();
        }


        $user->notify(new DailyCollectionReport($user));

        $this->info('end of sending notification');




    }
}
