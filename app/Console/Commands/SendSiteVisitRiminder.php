<?php

namespace App\Console\Commands;

use App\DB\Sale\Appointment;
use App\Jobs\SendSmsNotifications;
use App\User;
use Carbon\Carbon;
use Illuminate\Console\Command;

class SendSiteVisitRiminder extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'notifications:site-reminder';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        $appointmentgroups = Appointment::where('has_visited', false)->get()->groupBy(function ($q) {

            return $q->agent->id;
        });

        foreach ($appointmentgroups as $index => $appointments) {

            $agent = User::where('id', $index)->first();


            foreach ($appointments as $appointment) {

                $this->info("Start sending reminder");
               if (Carbon::parse($appointment->date_time)->isTomorrow() && $appointment->send_notification) {


                   $message = fanakaConfig('appointment_message');


                   $message = str_replace('[purchaser]',$appointment->full_name, $message);
                   $message = str_replace('[visit_date]',Carbon::parse( $appointment->date_time)->format('l jS \\ F Y ') ."at ".Carbon::parse($appointment->date_time)->format('H:i A'), $message);

                    dispatch(new SendSmsNotifications([
                        'to' => $appointment->phone_number,
                        'message' => $message,
                    ]));
                }


                $this->info("end sending reminder");
            }

        }
    }
}
