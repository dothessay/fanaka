<?php

namespace App\Console\Commands;

use App\DB\Sale\OfferLetter;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Mail;

class ReservationExpire extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'notification:cancel-reservation';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'This will notify the client that their reservation will be automatically cancelled the following day';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $reservations = OfferLetter::where([
            'is_converted'  => false
        ])->get();



        foreach ($reservations as $reservation) {

            $reservation->color = "#FFF";

            $dueDate = $reservation->getMeta('reservation_days',fanakaConfig('reservation_days',7));


            $expires = Carbon::parse($reservation->created_at)->startOfDay()->addDays($dueDate);

            //check if is expires tomorrow

            if ($expires->isTomorrow()){

                // we notify the client offer will be deleted tomorrow

                $reservation->color = "orange";


            }


            if ($expires->greaterThan(now())){

                // we delete this offer


                $reservation->color = "red";

                //$reservation->delete();

            }

            $reservation->save();

        }
    }
}
