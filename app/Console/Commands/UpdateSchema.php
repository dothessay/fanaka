<?php

namespace App\Console\Commands;

use App\DB\Role\Component;
use App\DB\Role\Functionality;
use App\DB\Role\Module;
use Codex\Classes\Helper;
use Illuminate\Console\Command;

class UpdateSchema extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'schema:update';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */

    public function handle()
    {
        $this->getAllModules();

    }

    private function getAllModules()
    {

        //ini_set('pcre.jit', 0);

        $schemaModules = Helper::system()->getModules();

        foreach ($schemaModules as $key => $schemaModule)
        {
            $module = Module::where('name', $schemaModule)->first();

            if (! $module)
            {
                $this->info("Creating a new module");

                $module = Module::create([
                    'name' => $schemaModule,
                    'label' => ucfirst($schemaModule),

                ]);



                $this->info($module->label . " Created");
            }

            $this->getAllComponents($module->name , $module->id);
        }

        $this->info("Successfully update schema");

    }

    private function getAllComponents($moduleName, $moduleId)
    {



        $schemaComponents = Helper::system()->getModuleComponents($moduleName);


        foreach ($schemaComponents as $key => $schemaComponent) {


            $component = Component::where('name', $schemaComponent)->first();

            if (! $component)
            {

                $this->info("Creating a new Component");


                $component = Component::create([
                    'module_id' => $moduleId,
                    'name' => $schemaComponent,
                    'label' => ucfirst(explode('_', $schemaComponent)[1]),

                ]);


                $this->info("Component created successfully " .$component->label);
            }




            $schemaFuctionalities  = Helper::system()->getModuleComponentsFunctionalities($moduleName , $component->name);

            foreach ($schemaFuctionalities as $index => $schemaFuctionality) {

                $functionality = Functionality::where('name',$schemaFuctionality)->first();

                if (! $functionality)
                {
                    $this->info("Creating a new Functionality");
                    $functionality = Functionality::create([
                        'component_id' => $component->id,
                        'label' => ucfirst(explode('_', $schemaFuctionality)[2]),
                        'name' => $schemaFuctionality,

                    ]);


                    $this->info("Functionality created successfully " . $functionality->label);
                }

            }
        }


    }
}
