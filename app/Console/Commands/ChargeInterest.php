<?php

namespace App\Console\Commands;

use App\DB\Sale\InterestSale;
use App\Sale\Sale;
use Carbon\Carbon;
use Illuminate\Console\Command;

class ChargeInterest extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'interest:charge';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Charge Interest for sale passed due date';

    /**
     * Create a new command instance.
     *
     * @return void
     */

    private $rate;

    public function __construct()
    {
        parent::__construct();
        $this->rate = ( 12 / 12 ) / 100 ;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        foreach (Sale::all() as $sale) {

            if ($sale->getBalance() >= 1){
                $this->info('has balance');
                if (isset($sale->installment->completion_date))
                {
                    $completionDate = $sale->installment->completion_date;
                    $today = now();
                    if ($today->greaterThan($completionDate))
                    {
                        $this->info('interest should apply'. $completionDate);
                        $lastPaymentDate = Carbon::parse($sale->salePayments->last()->created_at);
                        $this->info('Last payment date'. $lastPaymentDate);

                        $chargeDate = $lastPaymentDate->addRealDays(31);

                        $this->info('Charge Date'. $chargeDate);

                        $fineDate = $chargeDate;

                        if ($today->greaterThanOrEqualTo($chargeDate))
                        {
                            // Determine the number of months to fine

                            $fineMonths = $today->diffInMonths($lastPaymentDate);

                            for ($month = 0 ; $month <= $fineMonths ; $month++)
                            {
                                $this->info(" charge for the {$month} ");

                                $this->applyCharge($sale, $fineDate);

                                $fineDate = $fineDate->addRealDays(31);
                            }

                        }
                    }

                }
            }

        }
    }

    private function applyCharge(Sale $sale, $interestDate)
    {
        // charge the interest

        $charge = floatval($sale->getBalance()) * $this->rate;

        $isFinedThisMonth = false;

        if (! InterestSale::where(['sale_id' => $sale->id ,'interest_date' => $interestDate ])->get()->count() && now()->greaterThan($interestDate))
        {
            InterestSale::create([
            'sale_id'  => $sale->id,
            'interest_date' => $interestDate,
            'percentage' => $this->rate * 100,
            'amount' => $charge,
        ]);
        }

        $this->info('charge is '.$charge );
        $this->info('balance '.$sale->getBalance() );
        $this->info('the rate '.$this->rate );


    }
}
