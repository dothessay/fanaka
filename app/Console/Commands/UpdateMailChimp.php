<?php

namespace App\Console\Commands;

use App\Customer\Customer;
use Codex\Classes\Handlers\MailChimpHandler;
use Illuminate\Console\Command;

class UpdateMailChimp extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'mailchimp:list';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update Mailchimp list';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->info('start updating');
        foreach (Customer::all() as $customer)
        {
            $response = (new MailChimpHandler())->getMember($customer->email);

            if ($response->getHttpCode() === 404)
            {
                (new MailChimpHandler())->store([
                    'email' => $customer->email,
                    'name'   => $customer->fullName(),
                    'tel'    => $customer->getPhoneNumber()
                ]);
            }

        }



        $this->info('finished updating');
    }
}
