<?php

namespace App\Console;

use App\Console\Commands\ChargeInterest;
use App\Console\Commands\DownloadedAgreementReminder;
use App\Console\Commands\InstalmentReminder;
use App\Console\Commands\MakeRepositoryCommand;
use App\Console\Commands\MakeServiceCommand;
use App\Console\Commands\ReleaseHeldPlots;
use App\Console\Commands\ReservationExpire;
use App\Console\Commands\SendSiteVisitRiminder;
use App\Console\Commands\CommissionCommand;
use App\Console\Commands\UpdateMailChimp;
use App\Console\Commands\UpdatePrepaid;
use App\Console\Commands\UpdateSchema;
use App\Console\Commands\UserLog;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        UpdateSchema::class,
        InstalmentReminder::class,
        MakeServiceCommand::class,
        SendSiteVisitRiminder::class,
        ReservationExpire::class,
        CommissionCommand::class,
        UserLog::class,
        ReleaseHeldPlots::class,
        ChargeInterest::class,
        MakeRepositoryCommand::class,
        DownloadedAgreementReminder::class,
        UpdatePrepaid::class,
        UpdateMailChimp::class
    ];
    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
       // $schedule->command('notification:installmentReminder')->dailyAt("08:00")->timezone('AFRICA/NAIROBI');
        $schedule->command('notification:collections')->dailyAt("19:40")->timezone('AFRICA/NAIROBI');
        $schedule->command('notification:userLog')->dailyAt("19:00")->timezone('AFRICA/NAIROBI');
        $schedule->command('notification:dueDate')->dailyAt("19:00")->timezone('AFRICA/NAIROBI');
        $schedule->command('notifications:site-reminder')->dailyAt("19:00")->timezone('AFRICA/NAIROBI');
        $schedule->command('notification:cancel-reservation')->hourly()->timezone('AFRICA/NAIROBI');
        $schedule->command('commission:update')->everyFiveMinutes();
       // $schedule->command('backup:clean')->dailyAt('19:00')->timezone('AFRICA/NAIROBI');
       // $schedule->command('backup:run')->dailyAt('19:00')->timezone('AFRICA/NAIROBI');
        $schedule->command('leave:update')->sundays()->timezone('AFRICA/NAIROBI');
        $schedule->command('plot:release')->dailyAt('19:00')->timezone('AFRICA/NAIROBI');
        $schedule->command('charge:interest')->dailyAt('19:00')->timezone('AFRICA/NAIROBI');
        $schedule->command('prepaid:update')->dailyAt('19:00')->timezone('AFRICA/NAIROBI');

        //$schedule->command('queue:work')->everyMinute()->timezone('AFRICA/NAIROBI');


    }

    /**
    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
