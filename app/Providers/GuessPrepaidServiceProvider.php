<?php

namespace App\Providers;

use App\Sale\Sale;
use Codex\Classes\Reports\GuessPrepaid;
use Illuminate\Support\ServiceProvider;

class GuessPrepaidServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
       parent::register();
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->app->singleton("Codex\Classes\Reports\GuessPrepaid");
    }
}
