<?php

namespace App\Providers;

use Illuminate\Support\Facades\Route;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;

class RouteServiceProvider extends ServiceProvider
{
    /**
     * This namespace is applied to your controller routes.
     *
     * In addition, it is set as the URL generator's root namespace.
     *
     * @var string
     */
    protected $namespace = 'App\Http\Controllers';

    /**
     * Define your route model bindings, pattern filters, etc.
     *
     * @return void
     */
    public function boot()
    {
        //

        parent::boot();
    }

    /**
     * Define the routes for the application.
     *
     * @return void
     */
    public function map()
    {
        $this->mapApiRoutes();

        $this->mapWebRoutes();

        //
    }

    /**
     * Define the "web" routes for the application.
     *
     * These routes all receive session state, CSRF protection, etc.
     *
     * @return void
     */
    protected function mapWebRoutes()
    {
        //general
        Route::middleware(['web'])
             ->namespace($this->namespace)
             ->group(base_path('routes/general.php'));


        Route::middleware(['web','auth'])
             ->namespace($this->namespace)
             ->group(base_path('routes/web.php'));


        Route::prefix('projects')
             ->middleware(['web','auth','access.module'])
             ->namespace("App\\Http\\Controllers\\Product")
             ->group(base_path('routes/products.php'));


        Route::prefix('users')
             ->middleware(['web','auth','access.module'])
             ->namespace("App\\Http\\Controllers\\Users")
             ->group(base_path('routes/users.php'));


        Route::prefix('plots')
             ->middleware(['web','auth','access.module'])
             ->namespace("App\\Http\\Controllers\\PLots")
             ->group(base_path('routes/plots.php'));


        Route::prefix('customers')
             ->middleware(['web','auth','access.module'])
             ->namespace("App\\Http\\Controllers\\Customers")
             ->group(base_path('routes/customers.php'));

        Route::prefix('sale')
             ->middleware(['web','auth','access.module'])
             ->namespace("App\\Http\\Controllers\\Sales")
             ->group(base_path('routes/sale.php'));


        Route::prefix('expenses')
             ->middleware(['web','auth','access.module'])
             ->namespace("App\\Http\\Controllers\\Expenses")
             ->group(base_path('routes/expenses.php'));


        Route::prefix('settings')
             ->middleware(['web','auth','access.module'])
             ->namespace("App\\Http\\Controllers\\Setting")
             ->group(base_path('routes/setting.php'));


        Route::prefix('account')
             ->middleware(['web','auth','access.module'])
             ->namespace("App\\Http\\Controllers\\Account")
             ->group(base_path('routes/account.php'));


        Route::prefix('communication')
             ->middleware(['web','auth'])
             ->namespace("App\\Http\\Controllers\\Communication")
             ->group(base_path('routes/communication.php'));


        Route::prefix('finance')
             ->middleware(['web','auth','access.module'])
             ->namespace("App\\Http\\Controllers\\Finance")
             ->group(base_path('routes/finance.php'));
    }

    /**
     * Define the "api" routes for the application.
     *
     * These routes are typically stateless.
     *
     * @return void
     */
    protected function mapApiRoutes()
    {
        Route::prefix('api')
             ->middleware('api')
             ->namespace($this->namespace)
             ->group(base_path('routes/api.php'));

        Route::prefix('api')
             ->middleware('api')
             ->namespace($this->namespace.'\\Api\\Client')
             ->group(base_path('routes/apis/clients.php'));

        Route::prefix('v2')
             ->middleware('api')
             ->namespace($this->namespace.'\\V2\\')
             ->group(base_path('routes/apis/v2/api.php'));
    }
}
