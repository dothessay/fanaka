<?php

namespace App\Providers;
use App\DB\Sale\CustomerSale;
use App\DB\Sale\SaleDirector;
use Codex\Classes\Observers\CustomerSaleObserver;
use Codex\Classes\Observers\DirectorSaleObserver;
use Illuminate\Support\Collection;
use Illuminate\Pagination\LengthAwarePaginator;
use App\Customer\Customer;
use App\DB\Accounting\Account;
use App\DB\Communication\Communication;
use App\DB\Communication\CommunicationMeta;
use App\DB\Communication\Contact;
use App\DB\Customer\CustomerFileUpload;
use App\DB\Customer\Referral;
use App\DB\Exepense\PettyCash;
use App\DB\Expense\Expense;
use App\DB\Finance\ChartOfAccount;
use App\DB\Notification\Issue;
use App\DB\Notification\Notification;
use App\DB\Product\Product;
use App\DB\Product\ProjectExpense;
use App\DB\Role\Component;
use App\DB\Role\Functionality;
use App\DB\Role\Module;
use App\DB\Role\Role;
use App\DB\Sale\OfferCustomers;
use App\DB\Sale\OfferLetter;
use App\DB\Sale\OfferLetterItem;
use App\DB\Sale\OfferPayment;
use App\Plot\Plot;
use App\Sale\Sale;
use App\Sale\SaleItem;
use App\Sale\SalePayment;
use App\User;
use Carbon\Carbon;
use Codex\Classes\Observers\CommunicationObserver;
use Codex\Classes\Observers\CustomerObserver;
use Codex\Classes\Observers\OfferCustomerObserver;
use Codex\Classes\Observers\OfferPaymentObserver;
use Codex\Classes\Observers\PaymentObserver;

use Codex\Classes\Observers\ReservationObserver;

use Codex\Classes\Observers\SaleItemObserver;
use Codex\Classes\Observers\SaleObserver;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\Relation;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;
use Illuminate\Database\Eloquent\Builder;
use function PHPSTORM_META\map;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {

        Schema::defaultStringLength(191);

        /** CHANGE DATABASE IN RUNTIME**/

        //

        Relation::morphMap([
            'module' => Module::class,
            'component' => Component::class,
            'functionality' => Functionality::class,
            'user' => User::class,
            'role' => Role::class,
            'sale' => Sale::class,
            'offer' => OfferLetter::class,
            'account' => Account::class,
            'offerPayment' => OfferPayment::class,
            'salePayment' => SalePayment::class,
            'chart' => ChartOfAccount::class,
            'expenses'  => Expense::class,
            'projectExpenses'  => ProjectExpense::class,
            'pettyCash'   => PettyCash::class,
            'product'     => Product::class,
            'contact'    => Contact::class,
            'plot'      => Plot::class
            ]);



        View::composer('layouts.partials.notification', function ($view) {
            $view->with('notification', (new Notification ())->notifications());
        });

        View::composer('layouts.partials.notification', function ($view) {
            $view->with('issues', (new Issue())->notifications());
        });
        View::composer('*', function ($view) {
            $date_format = 'd-m-Y';

            return $view->with('date_format', $date_format);
        });

        View::composer('_home_buttons', function ($view) {

           $uploads = collect();
           $referrals = collect();
           if (auth()->user())
            {

                $uploads = auth()->user()->customers()->map(function ($customer) { return $customer->uploads->where('is_seen', false)->count() ;})->filter()->count();
                $referrals = auth()->user()->customers()->map(function ($customer) { return $customer->referrals->count() ; })->filter()->count();
            }

            $view->with([
                'uploads' => $uploads,
                'referrals'  => $referrals,
            ]);
        });


        Model::addGlobalScope('created_at', function (Builder $builder) {
            $builder->orderBy('created_at', 'desc');
        });


        Sale::observe(SaleObserver::class);
        SalePayment::observe(PaymentObserver::class);
        CustomerSale::observe(CustomerSaleObserver::class);
        SaleDirector::observe(DirectorSaleObserver::class);


        OfferLetterItem::observe(ReservationObserver::class);
        OfferCustomers::observe(OfferCustomerObserver::class);
        OfferPayment::observe(OfferPaymentObserver::class);



        SaleItem::observe(SaleItemObserver::class);


        CommunicationMeta::observe(CommunicationObserver::class);

        /******** OBSERVING CUSTOMER MODEL ****/

        Customer::observe(CustomerObserver::class);

        Collection::macro('paginate', function($perPage, $total = null, $page = null, $pageName = 'page') {
            $page = $page ?: LengthAwarePaginator::resolveCurrentPage($pageName);
            return new LengthAwarePaginator(
                $this->forPage($page, $perPage),
                $total ?: $this->count(),
                $perPage,
                $page,
                [
                    'path' => LengthAwarePaginator::resolveCurrentPath(),
                    'pageName' => $pageName,
                ]
            );
        });

    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
