<?php

namespace App\Jobs;


use App\Customer\Customer;
use App\User;
use Codex\Classes\Handlers\OneSignalHandler;
use Codex\Classes\Handlers\SendAdvantaAfricaInnoxNotification;
use Codex\Classes\Handlers\SendInnoxNotification;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Mail;

class SendSmsNotifications implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     *
     *
     *
     */


    public $tries = 5;

    public  $data ;

    public function __construct(array  $data)
    {

        $this->data = $data;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {

        $gateway = fanakaconfig('sms_gateway' ,'OneSignalHandler');
        $gateway = "\\Codex\\Classes\\Handlers\\".$gateway;

        if (isset($this->data['customerId']) && ! is_null($this->data['customerId']))
        {
            $customer = Customer::where([
                'id' => $this->data['customerId'],
                'has_downloaded_app' => true
            ])->first();

            if ($customer) {

                SendInnoxNotification::build()
                    ->sendSms( new OneSignalHandler() , ['to' => $this->data['to'] ,'message'  => $this->data['message']]);

            }

        }

        SendInnoxNotification::build()
            ->sendSms( new SendAdvantaAfricaInnoxNotification() , ['to' => $this->data['to'] ,'message'  => $this->data['message']]);

    }

    public function fail(\Exception $exception)
    {

        Mail::raw(json_encode($exception) , function ($mail) use ($exception) {
            $mail->to('philipnjuguna66@gmail.com')
                ->subject($exception->getMessage());

        });

    }
}
