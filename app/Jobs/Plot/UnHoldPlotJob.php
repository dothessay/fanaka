<?php

namespace App\Jobs\Plot;

use Codex\Services\Plot\UnHoldPlot;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class UnHoldPlotJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */

    public $caller;

    public $data;

    public function __construct($caller , $data)
    {
        $this->caller = $caller ;

        $this->data = $data;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {

        return (new UnHoldPlot($this->caller))->runWithDBTransaction($this->data);
    }
}
