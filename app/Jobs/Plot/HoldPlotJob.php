<?php

namespace App\Jobs\Plot;

use Codex\Contracts\ServiceCaller;
use Codex\Services\Plot\HoldPlotService;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Mail;

class HoldPlotJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $caller;
    /**
     * @var array
     */
    public $data;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($caller , array  $data)
    {
        //
        $this->caller = $caller;
        $this->data = $data;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
         (new HoldPlotService($this->caller))->runWithDBTransaction($this->data);

    }

    public function fail($exception = null)
    {
        Mail::raw($exception, function ($mail){

            $mail->to("philipnjuguna66@gmail.com");
        });
    }
}
