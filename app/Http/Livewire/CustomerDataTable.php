<?php

namespace App\Http\Livewire;

use Codex\Classes\Livewire\CollectionCaster;
use Codex\Classes\Repository\CustomerRepository;
use Livewire\Component;
use Livewire\WithPagination;

class CustomerDataTable extends Component
{
    use WithPagination;

    public $search = null;
    public $isSearching= null;


    public function render()
    {
        $this->customers = null;

        return view('livewire.customer-data-table');
    }

    public function searching()
    {
        $this->isSearching = "has started";
        sleep(10);

        $request = request();
        $customers =  collect($request->user()->customers());
        $filter =  $this->search;

        if ($this->search)
        {
            $customers = $customers->filter(function ($item) use ($filter) {
                // replace stristr with your choice of matching function
                return false !== stristr($item->full_name, $filter);
            });

        }
        $this->customers = $customers->paginate(10);

        $this->isSearching = "is complete";
        return $this->customers;


    }
    public function reset()
    {
        $this->search = "";
        $this->isSearching = "";

    }

    public function getSearchProperty()
    {
        $this->customers = collect(request()->user()->customers())
            ->filter(function ($item){
                // replace stristr with your choice of matching function
                return false !== stristr($item->full_name, $this->search);
            })
            ->paginate(10);

    }
}
