<?php

namespace App\Http\Controllers\Setting;

use App\DB\Contract;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;

class SystemSettingController extends Controller
{

    public function index()
    {
        return view('setting.system')
            ->with([
                'contracts' => Contract::all()
            ]);
    }


    public function find($id)
    {
        return view('setting.system_update')
            ->with([
                'contract' => Contract::where('id', $id)->first()
            ]);
    }


    public function update(Request $request)
    {

        $contract = Contract::where('id', $request['id'])->first();


        if (! is_null($contract)) {

            $contract->value = $request['name'];

            $contract->save();

            Session::flash('success','successfully updated '. $contract->name );

            return back();
        }

        Session::flash('error','We could not update, please check all fields and try again');

        return back();

    }

}
