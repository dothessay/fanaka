<?php

namespace App\Http\Controllers\Setting;

use App\DB\Setting\Setting;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;

class CompanySettingController extends Controller
{
    public function index()
    {
        return view('setting.company')
            ->with([
                'business' => auth()->user()->business
            ]);
    }


    public function store(Request $request)
    {
        $requestData =[];

        $requestData['email'] = $request['email'];
        $requestData['phone'] = $request['tell'];
        $requestData['bank_name'] = $request['bank_name'];
        $requestData['account_name'] = $request['account_name'];
        $requestData['account_no'] = $request['account_no'];

        $data = [
            'name' => $request['name'],
            'address' => $request['address'],
            'slug' => $request['slug'],
            'plot_sizes' => $request['plot_sizes']
        ];

        $businessCode = auth()->user()->business->code;



        if ($request->hasFile('logo'))
        {

            //$path = $request->file('logo')->storeAs('uploads','logo.png');
            $path = $request->file('logo')->store('uploads');

            $requestData['image'] = $path;
            $data['image'] = $path;


        }


        $settingResponse = Setting::add($requestData, $businessCode, $data);


        Session::flash('success', $settingResponse);

        return back();



    }
    public function storeMessageTemplates(Request $request)
    {

        $requestData = $request->all();

        $requestData['appointment_message'] = $request['appointment_message'];
        $requestData['offer_message'] = $request['offer_message'];
        $requestData['new_sale'] = $request['new_sale'];
        $requestData['payment_request'] = $request['payment_request'];
        $requestData['payment_received'] = $request['payment_received'];
        $requestData['activation_message'] = $request['activation_message'];
        $requestData['installment_reminder'] = $request['installment_reminder'];


        $businessCode = auth()->user()->business_code;




        $settingResponse = Setting::add($requestData, $businessCode);


        Session::flash('success', $settingResponse);

        return back();



    }


    public function storeConfig(Request $request)
    {

        $requestData = $request->except('_token');

        if (isset($requestData['send_sms'])){

            $requestData['send_sms'] = true;
        }else{

            $requestData['send_sms'] = false;

        }

        $businessCode = auth()->user()->business_code;


        $settingResponse = Setting::add($requestData, $businessCode);


        Session::flash('success', $settingResponse);

        return back();

    }

    public function storeAgreementConfig(Request $request)
    {
        $requestData = $request->except('_token');

        $businessCode = auth()->user()->business_code;

        $settingResponse = Setting::add($requestData, $businessCode);


        Session::flash('success', $settingResponse);

        return back();
    }
}
