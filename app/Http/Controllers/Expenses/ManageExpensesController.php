<?php

namespace App\Http\Controllers\Expenses;

use Carbon\Carbon;
use Codex\Contracts\ServiceCaller;
use Codex\Services\Expenses\ListExpensesService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ManageExpensesController extends Controller implements ServiceCaller
{
    public function index(Request $request)
    {
        $options= [];

        $options['start_date'] = isset($request['start_date']) ? $request['start_date'] : Carbon::now()->startOfMonth();
        $options['end_date'] = isset($request['end_date']) ? $request['end_date'] : Carbon::now()->endOfMonth();



        return (new ListExpensesService($this))->run($request->all(), $options);

    }
    public function successResponse($message)
    {
        return view('expenses.manage')
            ->with([
                'expenses' => $message
            ]);

    }
}
