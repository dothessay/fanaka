<?php

namespace App\Http\Controllers\Expenses;

use App\DB\Exepense\PettyCash;
use App\DB\Exepense\PettyCashLog;
use Codex\Contracts\ServiceCaller;
use Codex\Services\Expenses\StorePettyCashService;
use Codex\Services\StoreExpensesService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;

class AddExpensesController extends Controller implements ServiceCaller
{
    public function index()
    {

        $petties = PettyCashLog::all()->map(function ($petty) {
             return $petty->pettyToArray();
        });



        return view('expenses.add_petty')->with([
            'petties'   => $petties
        ]);
    }

    public function store(Request $request)
    {
        $response = $this->validateForm($request, [
            'amount' => 'required|numeric',
            'purpose' => 'required',
            'chart'   => 'required'
        ],[
            'chart.required' => 'Please select the account',
            'amount.numeric'  => 'Amount should only contain numbers'
        ]);

        if (! $response instanceof  $request)
        {
            return $response;
        }


        return (new StoreExpensesService($this))->runWithDBTransaction($request->all());

    }
    public function storePettyCash(Request $request)
    {
        $response = $this->validateForm($request, [
            'amount' => 'required|numeric',
        ]);

        if (! $response instanceof  $request)
        {
            return $response;
        }

        return (new StorePettyCashService($this))->runWithDBTransaction($request->all());

    }

    public function successResponse($message)
    {
       Session::flash('success',$message);

       return back();
    }
    public function failureResponse($message)
    {
       Session::flash('error',$message);

       return back();
    }
}
