<?php

namespace App\Http\Controllers\Api\Client;

use App\Customer\Customer;
use Exception;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AgentsController extends ClientApiBaseController
{
    public function index()
    {
        try{

            $customer = Customer::where('id', $this->customerId)->first();

            $agents = collect();

            if ($customer)
            {
                $agents = $customer->agent()->map(function ($user) {
                    return [
                        'id' => $user->id,
                        'email'  => $user->email,
                        'name'  => $user->fullName(),
                        'phone'  => $user->phone_number
                    ];
                });

            }


            return response()
                ->json([
                    'data' => $agents
                ]);
        }catch (Exception $e){

            return $e->getMessage();
        }

    }
}
