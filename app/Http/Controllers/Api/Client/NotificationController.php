<?php

namespace App\Http\Controllers\Api\Client;

use App\DB\Communication\CommunicationMeta;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class NotificationController extends ClientApiBaseController
{
    public function index()
    {
        try{
            $metas = CommunicationMeta::where([
                'customer_id' =>  $this->customerId,
            ])->get();


            $communications = collect();


            if ($metas->count())
            {
                foreach ($metas as $meta) {
                    if ($meta->key == 'is_read')
                    {
                        if ($meta->value != true)
                        {
                            $communications->push($meta->communication->communicationToArray());
                        }
                    }

                }
            }

            return response()
                ->json([
                    'data'  => $communications
                ]);

        }catch (\Exception $exception) {


            return response()
                ->json([
                    'message'  => $exception->getMessage()
                ] , 422);

        }

    }

    public function read(Request $request)
    {
        $meta = CommunicationMeta::where([
            'customer_id'  => $this->customerId,
            'communication_id'  => $request['communication_id']
        ])->first();

        if ($meta) {
            if($meta->key == 'is_read')
            {
                $meta->value =  true;

                $meta->save();
            }

        }

        return response()
            ->json([
                'message'  => "Successfully marked as read",
                'data'     => []
            ]);




    }
}
