<?php

namespace App\Http\Controllers\Api\Client;

use App\Customer\Customer;
use Exception;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;

class ClientLoginController extends Controller
{
    public function index(Request $request)
    {

        try{

            $client = Customer::where('id_no', $request['idno'])->first();

            if (! $client){

                throw new  Exception(trans('auth.failed'));
            }

            if (! Hash::check($request['password'] ,$client->password)){

                throw new  Exception(trans('auth.failed'));
            }

            if (! $client->has_downloaded_app)
            {
                $client->has_downloaded_app = true;

                $client->save();

            }

            return response()
                ->json([
                    'data' =>  $client->customerToArray()
                ]);

        }
        catch (Exception $exception)
        {
            return response()
                ->json([
                    'message' => $exception->getMessage()
                ], 401);

        }


    }
}
