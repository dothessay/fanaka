<?php

namespace App\Http\Controllers\Api\Client;

use App\Customer\Customer;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class UpdateClientProfileController extends ClientApiBaseController
{
    public function index(Request $request)
    {
        $customer = Customer::where('id', $this->customerId)->first();

        $customer->first_name = $request['first_name'];
        $customer->last_name = $request['last_name'];
        $customer->middle_name = $request['middle_name'];
        $customer->phone_number = $request['phone_number'];
        $customer->email = $request['email'];



        if (! is_null($request['password']) && ! empty($request['password']))
        {
            $customer->password = bcrypt($request['password']);
        }

        $customer->save();


        return response()
            ->json([
                'message'  => 'successfully updated a profile',
                'data'     => $customer->customerToArray()
            ]);
    }
}
