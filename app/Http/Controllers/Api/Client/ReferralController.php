<?php

namespace App\Http\Controllers\Api\Client;

use App\Customer\Customer;
use App\DB\Customer\Referral;
use Exception;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class ReferralController extends ClientApiBaseController
{
    public function index()
    {
        try{
            $referrals = Referral::where('referrer_id', $this->customerId)
                ->get()
                ->map(function ($ref) {
                    return $ref->referralToArray();
                });

            return response()
                ->json([
                    'data' => $referrals
                ]);
        }catch (Exception $e){

            return response()
                ->json([
                    'data' => $e->getMessage()
                ], 500);
        }

    }

    public function store(Request $request)
    {
        try{



            DB::beginTransaction();

            $fullName = explode(' ',$request['fullName']);

            $lastName= "";


            if (isset($fullName[1])){
                $lastName = $fullName[1];
            }

            $customer = Customer::store([
                'first_name' => $fullName[0],
                'last_name' => $lastName,
                'email' => null,
                'phone_number' => $request['phoneNumber'],
                'id_no'=> null,
                'middle_name' => null,

            ]);
            Referral::create([
                'customer_id' => $customer->id,
                'referrer_id'  => $this->customerId,
                'agent_id'  => request('agentId')
            ]);


           DB::commit();

            return response()
                ->json([
                    'message' => "success "
                ]);
        }catch (Exception $e){

            DB::rollBack();
            return response()
                ->json([
                    'message' => $e->getMessage()
                ], 500);
        }



    }
}
