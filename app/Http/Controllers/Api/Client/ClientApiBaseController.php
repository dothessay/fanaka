<?php

namespace App\Http\Controllers\Api\Client;


use App\Customer\Customer;
use App\Http\Controllers\Controller;

class ClientApiBaseController extends Controller
{
    public $customerId;
    public $customer;


    public function __construct()
    {

        $this->customerId = isset(getallheaders()['customer_id']) ? getallheaders()['customer_id']: request('customer_id') ;



        $this->customer = Customer::find($this->customerId);
    }

}
