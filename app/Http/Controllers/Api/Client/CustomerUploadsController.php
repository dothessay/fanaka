<?php

namespace App\Http\Controllers\Api\Client;

use App\DB\Customer\CustomerFileUpload;
use App\DB\Sale\CustomerSale;
use App\DB\Sale\SaleUpload;
use App\Jobs\SendSmsNotifications;
use Codex\Classes\FileUpload;
use Exception;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CustomerUploadsController extends ClientApiBaseController
{
    public function index(Request $request)
    {
        try {

            $path = FileUpload::uploadBase64Image($request, 'agreements');

            CustomerFileUpload::create([
                'customer_id' => $this->customerId,
                'file_type' => $request['documentType'],
                'file_url' => $path
            ]);
            dispatch(new SendSmsNotifications([
                'to' => config('config.upload_notification.Tabby'),
                'message' => "There is a new upload from the app> ". $this->customer->fullName(),
            ]));

            return response()
                ->json([
                    'message' => "uploaded successfully"
                ], 200);
        } catch (Exception $e) {

            return response()
                ->json([
                    'message' => $e->getMessage(),
                ], 500);
        }

    }

    public function getUploads()
    {

        $uploads = CustomerFileUpload::where('customer_id', $this->customerId)
            ->get()
            ->map(function ($upload) {
                return $upload->uploadsToArray();
            });

        return response()
            ->json([
                'data' => $uploads
            ]);


    }

    public function download()
    {
        $files = CustomerSale::where('customer_id', $this->customerId)
            ->get()
            ->map(function ($sale) {
                return $sale->uploads;
            });

        $toReturn = [];

        foreach ($files as $file) {

            foreach ($file as $item) {

                $toReturn[] = $item->uploadsToArray();

            }

        }

        return response()
            ->json([
                'data' => $toReturn
            ]);


    }

    public function downloadFile($id)
    {
        try {
            $saleUploads = SaleUpload::where(['id' => $id])->first();

            return response()->json(url($saleUploads->agreement_url));


        } catch (Exception $exception) {

            return response()->json($exception->getMessage(), 500);

        }

    }

    public function delete(CustomerFileUpload $upload)
    {
        $upload->notSeen()->forceDelete();

        return response()
            ->json([
                'data' => "",
                'message' => 'removed successfully'
            ]);

    }
}
