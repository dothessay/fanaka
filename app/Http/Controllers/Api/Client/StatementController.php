<?php

namespace App\Http\Controllers\Api\Client;

use App\DB\Sale\CustomerSale;
use App\Sale\Sale;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class StatementController extends ClientApiBaseController
{
    public function index()
    {
        $sales =  CustomerSale::where(['customer_id' => $this->customerId])
            ->get()
            ->map(function ($q){


                return $q->sale->saleToArray();
            });

        return response()
            ->json([
                'data' => $sales
            ])->withHeaders([
                'token' => '1veoetnew89yr89yq2wu9$gcufzuf$#@xcfggg'
            ]);


    }

    public function agreement(Request $request)
    {

    }
    public function receipt(Request $request)
    {
        $sale = Sale::where('id', $request['id'])->first();
        if ( $sale) {
            return response()
                ->json([
                    'data' => $sale->saleToArray()
                ]);
        }
        return response()
            ->json([
                'message' => "No Payment was found"
            ], 500);

    }
}
