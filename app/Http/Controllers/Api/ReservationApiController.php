<?php

namespace App\Http\Controllers\Api;

use Codex\Contracts\ServiceCaller;
use Codex\Services\Offer\ListOffer;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ReservationApiController extends Controller implements ServiceCaller
{

    public function index(Request $request)
    {

        if (is_null($request['userId'])){

            return response()
                ->json([
                    'status'  => 'ERROR',
                    'message'  => "Un Authorised Access"
                ] , 401);
        }
        $data = [];

        auth()->loginUsingId($request['userId']);


        $data['is_converted'] = false;

        if (! auth()->user()->can('access-module-component-functionality','sale_manage_show-all-sales'))
        {
            $data['agent_id'] = $request['userId'];
            $data['is_approved'] = true;

        }

        return (new ListOffer($this))->run($data);
    }

    public function successResponse($message)
    {
        return response()
            ->json([
                'status'  => 'OK',
                'data'  => $message->map(function ($q) { return $q->offerToArray() ;})
            ]);


    }

    public function failureResponse($message)
    {
        return response()
            ->json([
                'status'  => 'ERROR',
                'message'  => $message
            ] , 500);

    }
}
