<?php

namespace App\Http\Controllers\Api;

use App\DB\Sale\Appointment;
use Carbon\Carbon;
use Codex\Contracts\ServiceCaller;
use Illuminate\Http\Request;
use App\Http\Controllers\API\ApiBaseController;


class AppointmentApiController extends ApiBaseController implements ServiceCaller
{



    public function index(Request $request)
    {

        if (auth()->guest())
        {
            return response()
                ->json([
                    'message' => 'Un Authorised access'
                ] , 200);
        }

        $where = [];

        if (! auth()->user()->can('access-module-component-functionality','sale_appointment_view-all'))
        {
            $where['agent_id'] = auth()->id();
        }

        $startDate = isset($request['start_date']) ? $request['start_date'] : Carbon::now();



        $appointments = Appointment::where($where)->where('date_time', '>=', Carbon::parse($startDate))->get()->map(function ($q) {
            return $q->appointmentToArray();
        });


        return $this->successResponse($appointments);



    }

    public function successResponse($message)
    {
        return response()
            ->json([
                'data' => $message
            ]);

    }
}
