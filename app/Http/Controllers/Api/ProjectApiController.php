<?php

namespace App\Http\Controllers\Api;

use App\DB\Product\Product;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;


class ProjectApiController extends Controller
{
    public function index(Request $request)
    {
        $projects = Product::all();
        $projectArray  =  [];
        foreach ($projects as $project) {
            if ($project->availablePlots() > 0){
                $projectArray[] = [
                    'project_name' =>  $project->name(),
                    'project_code' =>  $project->code,
                    'no_available_plots' => $project->availablePlots(),
                    'available_plots' => $project->availablePlotsToArray(),
                ];
            }
        }

        return response()->json([
            'status' => 'OK',
            'data'   => $projectArray
        ] , 200);

    }
}
