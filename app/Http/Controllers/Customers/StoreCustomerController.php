<?php

namespace App\Http\Controllers\Customers;

use App\Customer\Customer;
use App\Http\Requests\Customer\StoreCustomer;
use Codex\Classes\Repository\CustomerRepository;
use Codex\Contracts\ServiceCaller;
use Codex\Services\Customers\StoreCustomerService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use Illuminate\Validation\Rule;
use Codex\Services\Customers\UpdateCustomerService;
use Codex\Services\Customers\DeleteCustomer;

class StoreCustomerController extends Controller implements  ServiceCaller
{
    public function index(Request $request)
    {
        return  (new  CustomerRepository())->store();

    }
    public function successResponse($message)
    {
        Session::flash('success', $message);

        return back();


    }

    public function update(Request $request)
    {

        ini_set('max_execution_time', 10000);

        $phoneNumber = str_split($request['phone_number']);


        if (sizeof($phoneNumber)  <=   4 )
        {
            $request['phone_number'] = '';

        }



        $response = $this->validateForm($request, [
            "first_name" => [
                'required',
            ],
            "last_name" => [
                'required',
            ],
            "email" => [
                'nullable',
                Rule::unique('customers')->ignore($request['id'])->whereNotNull('email')
            ],
            "id_no" => [

                'nullable',
                Rule::unique('customers')->ignore($request['id'])->whereNotNull('id_no')
            ],
            "phone_number" => [
                'nullable',
                Rule::unique('customers')->ignore($request['id'])->whereNotNull('phone_number')
            ],

        ]);




        if (! $response instanceof  $request)
        {
            return $response;
        }

        try{
            $data = $request;

            $customer = Customer::where('id', $data['id'])->first();

            if($customer)
            {
                $customer->first_name = $data['first_name'];
                $customer->last_name = $data['last_name'];
                $customer->middle_name = $data['middle_name'];
                $customer->phone_number = $data['phone_number'];
                $customer->email = $data['email'];
                $customer->id_no = $data['id_no'];
                $customer->user_id = auth()->id();
                $customer->password = Hash::make($customer->id_no);
                $customer->save();
            }


            $message =  "Successfully updated customer";

            Session::flash('success', $message);

            return back();


        }
        catch (\Exception $exception)
        {
            $message =  $exception->getMessage();

            Session::flash('error', $message);

            return back();
        }
    }


    public function uploadPhoto(Request $request)
    {

        if ($request->hasFile('photo')){

            try{
                $path = $request->file('photo')->store('agreements');

                //$sale->agreement_url= $path;

                $customer = Customer::where('id', $request['customer_id'])->first();


                $customer->image = $path;

                $customer->save();

                Session::flash('success','successfully uploaded the photo');


                return back();



            }
            catch (\Exception $exception)
            {
                return back()->with('error','could not upload the files . '. $exception->getMessage());
            }
        }

        return back()->with('error','could not upload the files . Kindly select an image');

    }



    public function delete(Request $request)
    {
        return (new DeleteCustomer($this))->run($request->all());

    }
}
