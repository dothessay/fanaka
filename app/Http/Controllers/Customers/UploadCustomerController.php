<?php

namespace App\Http\Controllers\Customers;

use App\Customer\Customer;
use App\DB\Customer\CustomerFileUpload;
use Codex\Classes\Handlers\TitleTransferProcess;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Http\Response;

class UploadCustomerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param Customer $customer
     * @return void
     */
    public function create(Customer $customer)
    {
        return view('customers.profile.create_upload')
            ->with([
                'customer'  => $customer
            ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @param Customer $customer
     * @return Response
     */
    public function store(Request $request , Customer $customer)
    {
        try{

            (new TitleTransferProcess())
                ->uploadDocuments();
        }
        catch (\Exception $e)
        {

        }

        return  redirect()
            ->route('customers_manage_statement', ['id' => $customer->id]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }
}
