<?php

namespace App\Http\Controllers\Customers;

use App\DB\Sale\CustomerSale;
use Codex\Classes\GeneratePdf;
use Codex\Classes\PdfOutput\DataReport;
use Codex\Contracts\ServiceCaller;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SaleStatementController extends Controller implements ServiceCaller
{
    public function index(Request $request , $id)
    {

        if(isset($request['sale'])){
            $sale = CustomerSale::where([
                'customer_id' => $id,
                'sale_id' => $request['sale']
            ])->first();

            $partners =  CustomerSale::where([
                'sale_id' => $request['sale']
            ])->where('customer_id','!=', $id)->get();


        }else{

            $sale = CustomerSale::where([
                'customer_id' => $id,
            ])->first();



        }

        // partner



        $otherCustomers = [];

        if ($partners->count())
        {
            foreach ($partners as $partner) {

                $otherCustomers[] = [
                    'name' => $partner->customer->fullName(),
                    'id' => $partner->customer->id,
                    'phone_number' => $partner->customer->phone_number,
                    ];

            }


        }


        if (isset($request['download']))
        {

            $hmtl = view('customers._sale_details')->with([
                'sale' => $sale,
                'groups' => collect($otherCustomers)
            ])->render();



            return (new DataReport())->outPut($hmtl);
        }







        return view('customers.sale_details')
            ->with([
                'sale' => $sale,
                'groups' => collect($otherCustomers)
            ]);


    }
    public function successResponse($message)
    {
        // TODO: Implement successResponse() method.
    }
}
