<?php

namespace App\Http\Controllers\Customers;

use App\Customer\Customer;
use App\DB\Sale\CustomerSale;
use App\DB\Sale\OfferCustomers;
use App\Sale\Invoice;
use App\Sale\Sale;
use Codex\Classes\Handlers\Table;
use Codex\Classes\PdfOutput\DataReport;
use Codex\Classes\Repository\InvoiceRepository;
use Codex\Classes\Repository\SalesRepository;
use Codex\Contracts\ServiceCaller;
use Codex\Services\Customers\ListCustomerService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ManageCustomerController extends Controller implements ServiceCaller
{
    public function index(Request $request)
    {

        $invoices  = (new InvoiceRepository())
            ->all($request);

        $sales = (new SalesRepository())->percentage(100);

        return view('customers.browse')->with([
            'invoices'  => $invoices,
            'sales'    =>  $sales
        ]);
        //return (new ListCustomerService($this))->run($request->all());

    }

    public function create()
    {

        return view('customers.profile.create');
    }

    public function list(Request $request)
    {
        $limit = 10;
        $offset = \request('start', 0);
        $customers = Customer::search(strtolower($request['search']['value']))->get()->skip($offset)->take($limit)->map->modelToArray();
        $count = Customer::count();

       /* if (!is_null($request['search']['value'])) {
            $customers = $customers->filter(function ($item) use ($request) {
                // replace stristr with your choice of matching function
                //return false !== stristr( strtolower($item->full_name), strtolower($request['search']['value']));
                return strpos(strtolower($item->full_name), strtolower($request['search']['value'])) !== false;
            });
        }
        $customers = $customers->skip($offset)->take($limit)->map->modelToArray();*/

        $customers = collect($customers)->values();

        return response()
            ->json(
                array(
                    "draw" => isset ($request['draw']) ? intval($request['draw']) : 0,
                    "recordsTotal" => intval($customers->count()),
                    "recordsFiltered" => intval($count),
                    "data" => $customers
                )
            );

    }

    public function successResponse($message)
    {
        return view('customers.manage_customer')
            ->with([
                'customers' => $message
            ]);
    }

    public function statement(Request $request, $id)
    {

        $request->request->add(['customer_id'  => $id]);

        $customer = Customer::where('id', $id)->first();

        $title = [];

        $sales = [];

        $plots = [];

        $payments = [];

        $groups = [];


        $uploads = [];


        foreach ($customer->customerStatementToArray() as $statement) {


            $title[] = $statement['title'];
            $sales[] = $statement['sales'];
            $payments[] = $statement['payments'];
            $plots[] = $statement['plots'];
            $groups[] = $statement['partners'];
            $uploads[] = $statement['uploads'];
        }


        $toReturn = [
            'title' => $title,
            'customer' => $customer,
            'sales' => $sales,
            'plots' => $plots,
            'payments' => $payments,
            'groups' => $groups,
            'uploads' => $uploads,
        ];

        if (isset($request['download'])) {
            $html = view('customers._statement')
                ->with($toReturn)->render();


            return (new DataReport())->outPut($html, "customer Report");

        }

//        return view('customers.statement')
//            ->with($toReturn);


        return view('customers._profile')
            ->with($toReturn);

    }

    public function validating(Request $request)
    {
        if ($request->has('id'))
        {

            if ( ! is_numeric( $request['id'] ) )
            {
                return response()
                    ->json([
                        'message' => "Id no cannot contains alphabets",
                        'title' => 'Id No'
                    ], 500);

            }
            if (Customer::where(['id_no' =>  $request['id']])->count()) {
                return response()
                    ->json([
                        'message' => "Id already taken",
                        'title' => 'Id No'
                    ], 500);
            }

            return response()
                ->json([
                    'message' =>'Id No is available',
                    'title' => "id"
                ], 200);
        }

        if ($request->has('email'))
        {
            if (Customer::where('email' , $request['email'])->count()) {
                return response()
                    ->json([
                        'message' => "Email already taken",
                    ], 500);
            }

            return response()
                ->json([
                    'message' =>'Email is available',
                ], 200);
        }
    }

    public function unpaid(Request $request)
    {
        $invoices = (new InvoiceRepository())
            ->all($request);


        return view('customers.profile.un_paid')
            ->with([
                'invoices'  => $invoices,
                'customers' => Customer::all()
            ]);
    }

    public function percentage(Request $request)
    {
        $sales = (new SalesRepository())
            ->percentage($request->query('percentage',100));


        return view('customers.profile.sale')
            ->with([
                'sales'  => $sales,
            ]);
    }
}
