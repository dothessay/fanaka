<?php

namespace App\Http\Controllers\Customers;

use App\DB\Customer\Referral;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CustomerReferralsController extends Controller
{
    public function index($id)
    {


        $referrals = Referral::where('referrer_id', $id)
            ->get()
            ->map(function ($q) {
            return $q->referralToArray();
        });




        return view('customers.referrals.index')
            ->with([
                'referrals' => $referrals
            ]);

    }
}
