<?php

namespace App\Http\Controllers;

use App\Customer\Customer;
use App\DB\Accounting\Account;
use App\DB\Product\Product;
use App\Sale\Invoice;
use App\Sale\Sale;
use App\Sale\SalePayment;
use Carbon\Carbon;
use Codex\Classes\Reports\InstallementPlanReport;
use Codex\Classes\TransformToObject;
use Codex\Contracts\ServiceCaller;
use Codex\Services\Sales\ListSalesService;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class HomeController extends Controller implements ServiceCaller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return Response
     */
    public function index()
    {
        $unPaid = Invoice::where('is_paid', false)->count();
        $percentages = [];
        $products = Product::all();


        foreach ($products as $product) {

            if ($product->hasAvailablePlots()) {
                $percentages[] = [
                    'product' => $product->name(),
                    'id' => $product->id,
                    'percentage' => $product->getAvailablePercentage() . " Remaining",
                    'available' => $product->availablePlots(),
                    'total' => $product->plots->count(),
                ];
            }

        }

        $lastWeekDefaulters = (new InstallementPlanReport())
            ->setStartDate(Carbon::now()->subRealWeek()->startOfWeek())
            ->setEndDate(Carbon::now()->subRealWeek()->endOfWeek())
            ->plan();



        $lastMonthDefaulters = (new InstallementPlanReport())
            ->setStartDate(Carbon::now()->subRealMonth()->startOfMonth())
            ->setEndDate(Carbon::now()->subRealMonth()->endOfMonth())
            ->plan();

        $thisWeekDefaulters = (new InstallementPlanReport())
            ->setStartDate(Carbon::now()->startOfWeek())
            ->setEndDate(Carbon::now()->startOfDay())
            ->plan();
        //  dd(Carbon::now()->startOfWeek() , Carbon::now()->startOfDay());

        $todaysPayment = (new InstallementPlanReport())
            ->setStartDate(Carbon::now()->startOfDay())
            ->setEndDate(Carbon::now()->endOfDay())
            ->plan();

        if (auth()->user()->role->name == 'admin' || ucfirst(auth()->user()->role->name) === "Developer" || ucfirst(auth()->user()->role->name) == "Super Admin") {
            return view('home')->with([
                'unpaid' => $unPaid,
                'yearSale' => number_format($this->thisYearSale(), 2),
                'monthSale' => number_format($this->thisMonthSale(), 2),
                'daySale' => number_format($this->thisDaySale(), 2),
                'availabilities' => $percentages,
                'lastWeekDefaulters' => $lastWeekDefaulters,
                'todaysPayment' => $todaysPayment,
                'thisWeekDefaulters' => $thisWeekDefaulters,
                'lastMonthDefaulters' => $lastMonthDefaulters


            ]);
        }


        return view('welcome')->with([
            'unpaid' => $unPaid,
            'yearSale' => number_format($this->thisYearSale(), 2),
            'monthSale' => (double)floatval($this->thisMonthSale()),
            'daySale' => (double)floatval($this->thisDaySale()),
            'availability' => $percentages,
            'lastWeekDefaulters' => $lastWeekDefaulters,
            'todaysPayment' => $todaysPayment,
            'thisWeekDefaulters' => $thisWeekDefaulters,
            'lastMonthDefaulters' => $lastMonthDefaulters
        ]);


    }

    private function thisYearSale()
    {
        $salePayments = SalePayment::whereBetween('created_at', [
            Carbon::now()->startOfYear(), Carbon::now()->endOfYear()
        ])->get();

        $sum = 0;

        foreach ($salePayments as $salePayment) {

            $sum += (double)$salePayment->amount;

        }

        return (($sum));

    }

    private function thisMonthSale()
    {
        $salePayments = SalePayment::whereBetween('created_at', [
            Carbon::now()->startOfMonth(), Carbon::now()->endOfMonth()
        ])->get();

        $sum = 0;

        foreach ($salePayments as $salePayment) {

            $sum += (double)$salePayment->amount;

        }

        return $sum;

    }

    private function thisDaySale()
    {
        $salePayments = SalePayment::whereBetween('created_at', [
            Carbon::now()->startOfDay(), Carbon::now()->endOfDay()
        ])->get();

        $sum = 0;

        foreach ($salePayments as $salePayment) {

            $sum += (double)$salePayment->amount;

        }

        return $sum;
    }

    public function graph()
    {
        return ['data' => $this->graphData()];
    }

    private function graphData()
    {
        $graphs = Account::whereBetween('created_at', [
            Carbon::now()->startOfYear(), Carbon::now()->endOfYear()
        ])->get()->groupBy(function ($query) {
            return Carbon::parse($query->created_at)->format('F-Y');
        });

        $data = [];


        $sum = 0;

        foreach ($graphs as $key => $graph) {
            $data['labels'][] = $key;


            $data['values'][] = $graph->sum('amount');


        }

        return $data;
    }

    public function unpaidPlots()
    {

        $invoices = Invoice::where('is_paid', false)->get();


        if (!auth()->user()->can('access-module-component-functionality', 'sale_manage_show-all-sales')) {


            $request['agent'] = auth()->user();

            $sales = (new ListSalesService($this))->run($request->all(), $options);


            $invoices = collect();

            foreach ($sales as $sale) {

                $invoices = $sale->invoices;

            }

//$invoices = Invoice::where('is_paid', false)->get();
        }


        $customers = collect();

        foreach (Customer::all() as $customer) {

            if ($customer->customerInvoices()->count()) {

                $customers->push($customer);
            }
        }


        return view('sales.manage_invoice')
            ->with([
                'invoices' => $invoices,
                'customers' => $customers,
            ]);
    }

    public function successResponse($message)
    {
        // TODO: Implement successResponse() method.
    }

    public function search(Request $request)
    {
        $customers = Customer::search($request['query'])->get();

        $output = (new TransformToObject());

        $result = collect();

        foreach ($customers as $customer) {


            $result->push([
                'name'  => $customer->full_name,
                'id' => $customer->id,
                'email' => $customer->email,
                'tel' => $customer->phone_number,
                'sales' => $customer->sales->count(),
                'uri' => url('/customers/manage/statement/'. $customer->id),
            ]);

        }

        return response()
            ->json([
                'data' => $result,
                'count' => $request['count']
            ]);

    }
}
