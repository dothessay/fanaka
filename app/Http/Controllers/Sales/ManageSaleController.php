<?php

namespace App\Http\Controllers\Sales;

use App\Customer\Customer;
use App\DB\Sale\CustomerSale;
use App\DB\Sale\SaleUpload;
use App\Jobs\SendSmsNotifications;
use App\Plot\Plot;
use App\Sale\Sale;
use App\Sale\SalePayment;
use App\User;
use Carbon\Carbon;
use Codex\Classes\FileUpload;
use Codex\Classes\GeneratePdf;
use Codex\Classes\Handlers\SendEmailHandler;
use Codex\Classes\Handlers\SendInnoxNotification;
use Codex\Classes\PdfOutput\ContractOutPut;
use Codex\Classes\PdfOutput\ReceiptOutPut;
use Codex\Contracts\ServiceCaller;
use Codex\Permission\Permissions;
use Codex\Scopes\IsApproved;
use Codex\Services\Customers\ListCustomerService;
use Codex\Services\Plots\ListPlotsService;
use Codex\Services\Sales\ListSalesService;
use foo\bar;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;

class ManageSaleController extends Controller implements ServiceCaller
{
    /**
     * @param Request $request
     * @return mixed
     *
     *
     */


    public function index(Request $request)
    {
        $options['start_date'] = isset($request['start_date']) ? $request['start_date'] : Carbon::now()->startOfDay() ;
        $options['end_date'] = isset($request['end_date']) ? $request['end_date'] : Carbon::now()->endOfDay();


        if (isset($request['filter'])) {

            if ($request['filter'] == 'year')
            {
                $options['start_date'] =  Carbon::now()->startOfYear() ;
                $options['end_date'] =  Carbon::now()->endOfYear();
            }
            if ($request['filter'] == 'month')
            {
                $options['start_date'] =  Carbon::now()->startOfMonth() ;
                $options['end_date'] =  Carbon::now()->endOfMonth();
            }
            if ($request['filter'] == 'today')
            {
                $options['start_date'] =  Carbon::now()->startOfDay() ;
                $options['end_date'] =  Carbon::now()->endOfDay();
            }
        }
        $where = $request->except(['filter','page']);

        if (!auth()->user()->can('access-module-component-functionality', 'sale_manage_show-all-sales')) {

            $where['agent'] = auth()->id();
        }
        if (isset($where['customer_id'])) {

            $customerSales = CustomerSale::where('customer_id', $request['customer_id'])->get();

            $filteredSales = [];


            foreach ($customerSales as $customerSale) {

                $filteredSales[] = $customerSale->sale;

            }


            return view('sales.manage_sales')
                ->with($this->toReturnData($filteredSales));


        }

        $request->request->add($options);


        return (new ListSalesService($this))->run($where, $options);
    }

    /**
     * @param $message
     * @return $this
     */
    public function successResponse($message)
    {


        return view('sales.manage_sales')
            ->with($this->toReturnData($message));
    }

    public function toReturnData($salesToReturn)
    {
        $sales = Sale::all();


        $yearlySales = collect($sales)->where('created_at', '>=', Carbon::now()->startOfYear())
            ->where('created_at', '<=', Carbon::now()->endOfYear());


        $monthlySales = collect($sales)->where('created_at', '>=', Carbon::now()->startOfMonth())
            ->where('created_at', '<=', Carbon::now()->endOfMonth());


        $weeklySales = collect($sales)->where('created_at', '>=', Carbon::now()->startOfWeek())
            ->where('created_at', '<=', Carbon::now()->endOfWeek());

        $dailySales = SalePayment::whereBetween('created_at', [
            Carbon::now()->startOfDay(), Carbon::now()->endOfDay()
        ])->get();


        $yearlySum = 0;

        foreach ($yearlySales as $yearlySale) {


            $yearlySum += $yearlySale->salePayments->sum('amount');

        }

        $monthlySum = 0;

        foreach ($monthlySales as $monthlySale) {


            $monthlySum += $monthlySale->salePayments->sum('amount');

        }

        $weeklySum = 0;

        foreach ($weeklySales as $weeklySale) {


            $weeklySum += $weeklySale->salePayments->sum('amount');

        }

        $daySum = 0;

        foreach ($dailySales as $salePayment) {

            $daySum += (double)$salePayment->amount;

        }

        $customers = auth()->user()->customers();



        return [
            'sales' => $salesToReturn,
            'customers' => $customers,
            'approved' => $sales->where('is_approved', false),
            'users' => User::active()->get(),
            'agreementRequests' => $sales->where('is_agreement_approved', false)->where('agreement_request', true),
            'paymentRequests' => SalePayment::withoutGlobalScope(new IsApproved())
                                    ->where('is_approved', false)
                                    ->get(),
        ];

    }


    private function thisYearSale()
    {
        $salePayments = SalePayment::whereBetween('created_at', [
            Carbon::now()->startOfYear(), Carbon::now()->endOfYear()
        ])->get();

        $sum = 0;

        foreach ($salePayments as $salePayment) {

            $sum += (double)$salePayment->amount;

        }

        return (($sum));

    }

    private function thisWeeklySale()
    {
        $salePayments = SalePayment::whereBetween('created_at', [
            Carbon::now()->startOfWeek(), Carbon::now()->endOfWeek()
        ])->get();

        $sum = 0;

        foreach ($salePayments as $salePayment) {

            $sum += (double)$salePayment->amount;

        }

        return (($sum));

    }

    private function thisMonthSale()
    {
        $salePayments = SalePayment::whereBetween('created_at', [
            Carbon::now()->startOfMonth(), Carbon::now()->endOfMonth()
        ])->get();

        $sum = 0;

        foreach ($salePayments as $salePayment) {

            $sum += (double)$salePayment->amount;

        }

        return $sum;

    }

    private function thisDaySale()
    {
        $salePayments = SalePayment::whereBetween('created_at', [
            Carbon::now()->startOfDay(), Carbon::now()->endOfDay()
        ])->get();

        $sum = 0;

        foreach ($salePayments as $salePayment) {

            $sum += (double)$salePayment->amount;

        }

        return $sum;
    }

    /**
     * @param $saleId
     * @return $this
     */
    public function getSale($saleId)
    {
        $sale = Sale::where('id', $saleId)->first();

        $salePayments = SalePayment::withoutGlobalScope(IsApproved::class)->where([
            'sale_id'  => $saleId
        ])->get();

        return view('sales.approve')->with([
            'sale' => $sale,
            'payments'   => $salePayments
        ]);

    }

    /**
     * @return $this
     */
    public function add()
    {


        $plots = (new ListPlotsService($this))->list([
            'is_reserved' => false,
            'is_sold' => false,
            'can_sell' => true,
        ]);


        return view('sales._add')
            ->with([
                'customers' => Customer::all(),
                'users' => User::all(),
                'plots' => $plots,
            ]);
    }

    /**
     * @param $id
     * @return $this
     */
    public function createSale($id)
    {

        return view('sales._create')
            ->with([
                'customers' => Customer::all(),
                'users' => User::all(),
                'plot' => (new ListPlotsService($this))->list([
                    'is_reserved' => false,
                    'is_sold' => false,
                    'can_sell' => true,
                    'id' => $id])->first(),
            ]);
    }


    /**
     * @param $id
     * @return string
     */
    public function agreement($id)
    {



        return redirect(url('sale/manage/approve-agreement/'. $id));


        $sales = Sale::where('id', $id)->get();

        $sale = $sales->first();




        if (!$sale->agreement_request && !$sale->is_agreement_approved) {
            $sale->agreement_request = true;


            $sale->save();


            $tos = config('config.admin_emails');


            $approveAggrementUri = route('sale_manage_approve-agreemt' ,['saleId' => $sale->id]);


            foreach ($tos as $index => $to) {

                $data = [
                    'to' => $to,
                    'subject' => 'Print Agreement Request',
                    'message' => "Hi {$index},                                                         Please note that there is a agreement print request for: {$sale->getCustomerDetails()} for project(s);{$sale->getCustomerPlots()} amount paid " . config('config.currency') . ' ' . number_format((double)$sale->salepayments->sum('amount'), 2) . '    '. $approveAggrementUri."That requires your urgent approval.                                                          Thank You!"
                ];

                SendInnoxNotification::build()
                    ->sendSms(new SendEmailHandler(), $data);

                //dispatch(new SendSmsNotifications($data));
            }


            Session::flash("success", "You have sent a print request successfully");


            return back();

        }
        if ($sale->agreement_request && !$sale->is_agreement_approved) {



            Session::flash("error", "Please wait for print agreement request to be reviewed and approved");


            return back();

        }

        $sale->is_agreement_downloaded = true;


        $sale->save();


        $output = '';


        ContractOutPut::build()->generate($sale , "");

       // $output = (new GeneratePdf())->generate(new ContractOutPut(), collect($sales));


        return $output;

    }

    public function approveAgreement($id)
    {
        $sale = Sale::where('id', $id)->first();


        $sale->is_agreement_approved = true;

        $sale->save();


        Session::flash("success", "You have Agreement has been approved successfully");


        return back();


    }

    /**
     * @param $id
     * @return \Symfony\Component\HttpFoundation\BinaryFileResponse
     */
    public function uploadedAgreement($id)
    {

        $sale = SaleUpload::where('id', $id)->first();

        if ( $sale)
        {
            if (Storage::disk('local')->exists($sale->agreement_url))
            {

                return Storage::download($sale->agreement_url);
            }
            return response()->download(($sale->agreement_url));
        }

        Session::flash('error', "No Upload was found");

        return back();


    }

    public function uploadScannedAgreement(Request $request)
    {

        $sale = Sale::where('id', $request['id'])->first();


        // $path = FileUpload::uploadFile($request , 'agreements','agreemerent','fanaka-real-estate-'.$sale->id);

        foreach ($request->agreement as $index => $agreement) {


            //$path = $request->file('agreement')->store('agreements');

            try {
                $path = $agreement->store('agreements');

                //$sale->agreement_url= $path;


                $sale->saleUploads()->create([
                    'agreement_url' => $path,
                    'file_name' => $agreement->getClientOriginalName(),
                    'type' => $request['upload_type'][$index]
                ]);

                $sale->is_agreement_uploaded = true;

                $sale->save();
            } catch (\Exception $exception) {
                return back()->with('error', 'could not upload the files . ' . $exception->getMessage());
            }


        }

        return back()->with('success', 'successfully uploaded image');

    }


    /**
     * @param $id
     * @return mixed
     */
    public function receipt($id)
    {
        $sales = Sale::where('id', $id)->get();


        $output = (new GeneratePdf())->generate(new ReceiptOutPut(), collect($sales));

        return $output;

    }


    /**
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function approve($id)
    {
        $sale = Sale::where(['id' => $id, 'is_approved' => false])->first();

        if ($sale) {
            $sale->is_approved = true;

            $sale->approved_by = auth()->id();

            foreach ($sale->saleItems as $saleItem) {

                $plot = $saleItem->plot;

                $plot->is_reserved = false;

                if ($sale->salePayments->sum('amount') === $plot->price) {

                    $plot->is_completely_paid = true;
                }

                $plot->is_sold = true;

                $plot->save();

            }

            $sale->save();


            /*foreach ($sale->customerSales as $customerSale)
            {
                dispatch(new SendSmsNotifications([
                    'to' => $customerSale->customer->phone_number,
                    'message' => "Hi {$customerSale->customer->fullName()},                                                         Please note that there is a new sale Fsl/{$sale->id} for: {$offerCustomer->customer->fullName()} for project(s);{$sale->getCustomerPlots()}of ".config('config.currency').''. number_format((double)$sale->salePayments->sum('amount'), 2)." that requires your urgent approval.                                                          Thank You!"
                ]));
            }*/


            Session::flash('success', 'sale has been approved');

            return back();
        }

        Session::flash('error', 'error approving try again');

        return back();
    }

    /**
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function reject($id)
    {
        $sale = Sale::where(['id' => $id, 'is_approved' => false])->first();


        if ($sale) {
            foreach ($sale->saleItems as $saleItem) {
                $plot = $saleItem->plot;

                $plot->is_reserved = false;
                $plot->is_sold = false;
                $plot->can_sell = true;
                $plot->save();

                $saleItem->forceDelete();


            }


            $sale->accounts->count() ? $sale->accounts->first()->delete() : null;

            $sale->forceDelete();

            Session::flash('success', 'sale has been canceled');

            return redirect()->route('sale_manage');
        }

        Session::flash('error', 'error canceling try again');

        return back();
    }


    public function viewUploadedAgreement($saleId)
    {
        $sale = Sale::with('saleUploads')->where('id', $saleId)->first();


        return view('sales.report.agreements.view')->with([
            'saleUploads' => $sale->saleUploads,
            'sale' => $sale,
        ]);
    }

    public function destroy(SaleUpload $upload)
    {
        $sale = $upload->sale;

        try{
            $path = file_exists($upload->agreement_url) ? ($upload->agreement_url) :  ('storage/'.$upload->agreement_url);

            if (file_exists( $path))
            {
                unlink($path);
            }

            if ($upload->type === 'agreement')
            {


                $sale->is_agreement_uploaded = false;

                $sale->save();

            }

            $upload->forceDelete();


            \session()->flash('successfully removed the document');

            return redirect()
                ->route('sale_reports_scanned-agreement');
        }catch (\Exception $e)
        {
            \session()->flash($e->getMessage());

            return redirect()
                ->route('sale_reports_scanned-agreement');
        }



    }
}
