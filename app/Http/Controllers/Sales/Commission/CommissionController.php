<?php

namespace App\Http\Controllers\Sales\Commission;

use App\Sale\Sale;
use App\User;
use Carbon\Carbon;
use Codex\Classes\GeneratePdf;
use Codex\Classes\PdfOutput\DataReport;
use Codex\Classes\Repository\UserRepository;
use Codex\Contracts\ServiceCaller;
use Codex\Services\Sales\ListSalesService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;

class CommissionController extends Controller implements ServiceCaller
{

    protected  $canShowAction = true;

    private $exclude = [];

    public function __construct()
    {
        $this->exclude = \session('exclude_commissions');
    }

    public function index(Request $request)
    {
        $where = [];

        $options =[];

        $request->request->add(['commission_at' => true]);
        if (! $request->has('commission_start_date')  && ! $request->has('commission_end_date'))
        {
            \request()->request->add([
                'commission_start_date' =>  Carbon::parse(date('y-m-25'))->subRealMonths(1)->startOfDay() ,
                'commission_end_date' => Carbon::parse(date('y-m-25'))->endOfDay()
            ]);
        }



        if( isset($request['agent_id']) && ! is_null($request['agent_id'])) {
            $request->request->add(['agent' =>  $request['agent_id']]);

        }
        if (isset($request['pdf'])) {

            $this->canShowAction = false;

        }

        return (new ListSalesService($this))->run($where , $options);
    }

    public function successResponse($message)
    {
        $agents = (new UserRepository())->all();

        if (! $this->canShowAction) {

            $html =  view('sales.commission._data')
                ->with([
                    'showActions' => $this->canShowAction,
                    'sales'       => $message->map(function ($q) { return $q->saleToArray() ; })
                ])->render();

            return (new DataReport())->outPut($html , "Commissions Reports" ,\Mpdf\Output\Destination::INLINE);

        }


        return view('sales.commission.index')
            ->with([
                'showActions' => $this->canShowAction,
                'agents'      => $agents,
                'sales'       => $message->map(function ($q) { return $q->saleToArray() ; })
            ]);

    }

    public function update(Request $request)
    {

        session()->forget('exclude_commissions');
        if(isset($request['exclude']))
        {
            foreach ($request['sale_id'] as $saleId) {
                $this->exclude[] = $saleId;
            }
          \session()->put('exclude_commissions', $this->exclude);
        }
        else{

            foreach ($request['sale_id'] as $saleId) {


                $sale = Sale::where('id', $saleId)->first();


                if ($sale) {

                    $sale->commission_at = now();

                    $sale->save();
                }
            }
        }

        Session::flash('success','successfully market agent to have received commission');

        return back();


    }
}
