<?php

namespace App\Http\Controllers\Sales\Interest;

use App\DB\Sale\InterestSale;
use App\Sale\Sale;
use Codex\Classes\Filters\DateBetweenFilter;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Pipeline\Pipeline;

class InterestController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        \request()->has('start_date') ? \request('start_date') : \request()->request->add(['start_date', now()->startOfMonth()]);
        \request()->has('end_date') ? \request('end_date') : \request()->request->add(['end_date', now()->endOfMonth()]);

        $interest = app(Pipeline::class)
            ->send(InterestSale::query())
            ->through([
                DateBetweenFilter::class
            ])
            ->thenReturn()
            ->get()
            ->groupBy('sale_id');



        $fines = [];

        foreach ($interest as $key => $item) {
            $sale = Sale::find($key);
            $fines[] = [
                'sale_id' => $key,
                'lastPaymentDate' => $sale->salePayments->last()->created_at,
                'customer'   => $sale->getCustomerDetails(),
                'salePath'    => $sale->path(),
                'balance'     => $sale->getBalance(),
                'totalAmount'  => $item->sum('amount'),
                'totalMonths'       => implode($item->pluck('interest_date')->toArray() ,' <br/>'),
                'count'      => $item->count(),
                'projects'   => $sale->getPropertyDetails()

            ];

        }



        return view('sales.interest.index')->with([
            'interests'   => $fines
        ]);
    }

    public function saleDetailsInterest(Sale $sale)
    {
        return view('sales.interest.show')
            ->with([
                'interests' =>  $sale->interests,
                'sale'      => $sale
            ]);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
