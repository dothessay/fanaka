<?php

namespace App\Http\Controllers\Sales;

use Carbon\Carbon;
use Codex\Classes\Repository\InvoiceRepository;
use Codex\Classes\Repository\SaleItemRepository;
use Codex\Classes\Repository\SalePaymentRepository;
use Codex\Classes\Repository\SalesRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\Rule;

class SaleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return void
     */
    public function store(Request $request)
    {

        $response = $this->validateSale($request);

        if (! $response instanceof $request)
        {
            return $response;
        }

        try{

            DB::beginTransaction();
            $sale = (new SalesRepository())
                ->createSale($request);

            // sale Items

            DB::commit();


            return redirect()
                ->route('sale_manage_edit_agreement', ['sale' => $sale->id]);
        }catch (\Exception $e) {
            DB::rollBack();

            session()->flash('error', $e->getMessage());
            return back();
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    private function validateSale(Request $request){
        $rules = [
            'plot_id' => [
                'required',
                Rule::unique('sale_items','plot_id')
            ],
            'agent' => 'required',
        ];

        $validation = $this->validateForm($request , $rules, [
            'plot_id.unique'  => 'Plot already sold',
            'plot_id.required' => 'Kindly select project and plot',
            'amount.required' => 'Total Sale Amount is required',
            'deposit_date.required' => 'Deposit date is required'
        ]);

        if (! $validation instanceof $request)
        {
            return $validation;
        }

        if (isset($request['deposit_date']) && Carbon::parse($request['deposit_date'])->greaterThan(Carbon::now()))
        {
            session()->flash('error','Deposit Date cannot be in future');
            return back();

        }
        return $request;

    }
}
