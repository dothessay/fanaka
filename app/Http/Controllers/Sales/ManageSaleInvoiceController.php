<?php

namespace App\Http\Controllers\Sales;

use App\Customer\Customer;
use App\DB\Sale\CustomerSale;
use App\Sale\Invoice;
use App\Sale\SalePayment;
use Carbon\Carbon;
use Codex\Classes\GeneratePdf;
use Codex\Classes\PdfOutput\DataReport;
use Codex\Contracts\ReportOutput;
use Codex\Contracts\ServiceCaller;
use Codex\Scopes\IsApproved;
use Codex\Services\Sales\ListSalesService;
use Codex\Services\Sales\UpdateInvoiceService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;

class ManageSaleInvoiceController extends Controller implements ServiceCaller
{
    public function index(Request $request)
    {
        $options = [];

        $request->request->add($options);

        $invoices = collect();

        $request->request->add($options);

        if (!\request()->user()->can('access-module-component-functionality', 'sale_manage_show-all-sales')) {

            $request['agent'] = auth()->user();

            $sales = (new ListSalesService($this))->run($request->all(), $options);

            foreach ($sales as $sale) {

                $invoices->push($sale->invoices);

            }

            //$invoices = Invoice::where('is_paid', false)->get();
        }
        if (isset($request['customer_id'])) {
            $customer = Customer::where('id', $request['customer_id'])->first();

            $invoices = $customer->customerInvoices();
        }

        $customers = collect();

        foreach (Invoice::where('is_paid', false)->get() as $invoice) {

            foreach ($invoice->sale->customerSales as $customerSale)
            {
                if ($invoice->balance() > 0 )
                {
                    $customers->push($customerSale->customer);
                }
            }
        }

        return view('sales.manage_invoice')
            ->with([
                'invoices' => $invoices,
                'customers' => $customers->unique('id'),
            ]);
    }

    public function details($id)
    {
        $invoice = Invoice::where('id', $id)->first();

        $payments = SalePayment::withoutGlobalScope(IsApproved::class)->where('sale_id', $invoice->sale->id)->get();

        return view('sales.update_invoice')->with([
            'invoice' => $invoice,
            'payments'  => $payments,
        ]);


    }

    public function outPut($id)
    {
        $invoice = Invoice::where('id', $id)->first();
        $payments = SalePayment::withoutGlobalScope(IsApproved::class)->where('sale_id', $invoice->sale->id)->get();

        $html = view('sales._invoice_upload')->with([
            'invoice' => $invoice,
            'payments'  => $payments
        ])->render();


        return (new DataReport())->outPut($html);


    }

    public function update(Request $request)
    {

        $rule = [
            'deposit_date' => 'required',
            'amount' => 'required',

        ];
        if (strtolower($request['payment_method']) === 'bank') {
            $rule["slip_no"] = [
                'required',
            ];


        }
        if (strtolower($request['payment_method']) === 'mpesa') {
            $rule["mpesa_reference_code"] = [
                'required',
            ];
        }
        if (strtolower($request['payment_method']) === 'cheque') {
            $rule["cheque_no"] = [
                'required',
            ];$rule["receipts"] = [
                'required',
            ];
        }

        $response = $this->validateForm($request, $rule);

        if (Carbon::parse($request['deposit_date']) > Carbon::now()) {


            Session::flash('error', "Kindly enter a Deposit date not in future");
            return back();

        }


        if (!$response instanceof $request) {
            return $response;
        }


        return (new UpdateInvoiceService($this))->run($request->all());

    }

    public function successResponse($message)
    {
        Session::flash($message);

        return back()->with('success', $message);
    }
}
