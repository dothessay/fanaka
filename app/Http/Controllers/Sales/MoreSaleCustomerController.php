<?php

namespace App\Http\Controllers\Sales;

use App\Customer\Customer;
use App\DB\Accounting\Account;
use App\DB\Sale\CustomerSale;
use App\DB\Sale\Installment;
use App\DB\Sale\SaleDirector;
use App\Plot\Plot;
use App\Sale\Invoice;
use App\Sale\Sale;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Illuminate\Validation\Rule;

class MoreSaleCustomerController extends Controller
{
    public function index(Request $request)
    {
        try {
            DB::beginTransaction();

            $sale = Sale::where('id', $request['sale_id'])->first();


            if (isset($request['customer_id']) && !is_null($request['customer_id'])) {

                if ($sale->customerSales->where('customer_id', $request['customer_id'])->count()) {

                    throw new \Exception("customer must not have more record on a single sale");
                }

                $sale->customerSales()->create([
                    'customer_id' => $request['customer_id'],
                ]);

            }

            if (isset($_REQUEST['new_customer'])) {
                for ($customerIndex = 0; $customerIndex < sizeof($_REQUEST['first_name']); $customerIndex++) {


                    $customer = Customer::store([
                        'business_code' => auth()->user()->business->code,
                        'first_name' => $_REQUEST['first_name'][$customerIndex],
                        'last_name' => $_REQUEST['last_name'][$customerIndex],
                        'email' => $_REQUEST['email'][$customerIndex],
                        'phone_number' => $_REQUEST['phone_number'][$customerIndex],
                        'id_no' => $_REQUEST['id_no'][$customerIndex],
                        'middle_name' => $_REQUEST['middle_name'][$customerIndex],
                        'user_id' => auth()->id(),
                    ]);

                    $sale->customerSales()->create([
                        'customer_id' => $customer->id,
                    ]);
                }
            }

            DB::commit();

            Session::flash('success', 'Added successfully');

            return back();


        } catch (\Exception $exception) {

            DB::rollBack();

            Session::flash('error', 'could not finish successfully');

            return back();

        }

    }public function directors(Request $request)
    {
        try {
            DB::beginTransaction();

            $validator = $this->validateForm($request , [
                'customer_id'  => [
                    Rule::unique('sale_directors','customer_id')
                    ->where('sale_id', $request['sale_id'])
                ]
            ],[
                'customer_id.unique' => "The director already added"
            ]);


            if (! $validator instanceof  $request)
            {
                return $validator;

            }


            $sale = Sale::where('id', $request['sale_id'])->first();


            if (isset($request['customer_id']) && !is_null($request['customer_id'])) {

                if ($sale->customerSales->where('customer_id', $request['customer_id'])->count()) {

                    throw new \Exception("customer must not have more record on a single sale");
                }

                $sale->directors()->create([
                    'customer_id' => $request['customer_id'],
                ]);

            }

            if (isset($_REQUEST['new_customer'])) {
                for ($customerIndex = 0; $customerIndex < sizeof($_REQUEST['first_name']); $customerIndex++) {


                    $customer = Customer::store([
                        'business_code' => auth()->user()->business->code,
                        'first_name' => $_REQUEST['first_name'][$customerIndex],
                        'last_name' => $_REQUEST['last_name'][$customerIndex],
                        'email' => $_REQUEST['email'][$customerIndex],
                        'phone_number' => $_REQUEST['phone_number'][$customerIndex],
                        'id_no' => $_REQUEST['id_no'][$customerIndex],
                        'middle_name' => $_REQUEST['middle_name'][$customerIndex],
                        'user_id' => auth()->id(),
                    ]);

                    $sale->directors()->create([
                        'customer_id' => $customer->id,
                    ]);
                }
            }

            DB::commit();

            Session::flash('success', 'Added successfully');

            return back();


        } catch (\Exception $exception) {

            DB::rollBack();

            Session::flash('error', 'could not finish successfully');

            return back();

        }

    }

    public function remove($id, Request $request)
    {
        $sale = Sale::where('id', $request['sale-id'])->first();


        $customerSales = CustomerSale::where([
            'sale_id' => $sale->id,
            'customer_id' => $id
        ])->forceDelete();


        Session::flash('success', 'removed successfully');

        return back();


    }

    public function removeDirectors($id, Request $request)
    {
        $sale = Sale::where('id', $request['sale-id'])->first();



        $director= SaleDirector::where([
            'id' => $id
        ])->forceDelete();



        Session::flash('success', 'removed director successfully');

        return back();


    }


    public function separate(Request $request)
    {
        try {
            DB::beginTransaction();


            $request['is_conversion'] = true;


            $oldSale = Sale::where('id', $request['sale_id'])->first();


            $oldInstallment = Installment::where('sale_id', $oldSale->id)->first();
            $oldInstallmentArray = [];

            if ($oldInstallment) {

                $oldInstallmentArray = $oldInstallment->attributesToArray();
                unset($oldInstallmentArray['id']);
            }


            $oldSaleArray = $oldSale->attributesToArray();

            $oldSalePayment = $oldSale->salePayments->map(function ($payment) {
                $array = $payment->attributesToArray();
                unset($array['id']);
                unset($array['sale_id']);
                unset($array['amount']);
                return $array;
            })->toArray()[0];


            //sale itens


            $saleItemsOld = $oldSale->saleItems->map(function ($item) {
                $array = $item->attributesToArray();
                unset($array['id']);
                unset($array['sale_id']);
                unset($array['plot_id']);
                unset($array['price']);
                unset($array['plot_no']);
                return $array;
            })->toArray();


            unset($oldSalePayment['id']);


            unset($oldSaleArray['id']);
            unset($oldSaleArray['customer_details']);


            foreach ($request['customer_id'] as $index => $customerId) {

                $plot = Plot::where('id', $request['plot_id'][$customerId])->first();

                unset($oldSaleArray['total_amount']);

                $oldSalePayment['amount'] = (double)$request['amount'][$customerId];

                $oldSaleArray['total_amount'] = $plot->price;


                $newSale = Sale::create([
                    'business_code' => $oldSaleArray['business_code'],
                    'user_id' => auth()->id(),
                    'agent' => $oldSaleArray['agent'],
                    'total_amount' => $oldSaleArray['total_amount'],
                    'balance' => (double)$oldSaleArray['total_amount'] - (double)$request['amount'][$customerId],
                    'title_fee' => isset($oldSaleArray['title_fee']) ? true : false,
                    'created_at' => Carbon::parse($oldSaleArray['created_at']),

                ]);


                // create sale items


                $saleItemsOld['plot_id'] = $plot->id;
                $saleItemsOld['plot_no'] = $plot->plot_no;


                $saleItem = $newSale->saleItems()->create([

                    'business_code' => $newSale->businessCode(),
                    'plot_id' => $plot->id,
                    'price' => $plot->price,
                    'plot_no' => $plot->plot_no,
                    'created_at' => Carbon::parse($oldSaleArray['created_at']),

                ]);


                //create sale payments

                $payment = $newSale->salePayments()->create([
                    'payment_type' => $oldSalePayment['payment_type'],
                    'payment_method' => $oldSalePayment['payment_method'],
                    'amount' => $oldSalePayment['amount'],
                    'user_id' => auth()->id(),
                    'is_approved' => true,
                    'reference_code' => isset($oldSalePayment['reference_code']) ? $oldSalePayment['reference_code'] : $oldSalePayment['payment_method'],
                    'bank_name' => isset($oldSalePayment['bank_name']) ? $oldSalePayment['bank_name'] : "",
                    'account_no' => isset($oldSalePayment['account_no']) ? $oldSalePayment['account_no'] : "",
                    'cheque_no' => isset($oldSalePayment['cheque_no']) ? $oldSalePayment['cheque_no'] : "",
                    'drawer' => isset($oldSalePayment['drawer']) ? $oldSalePayment['drawer'] : "",
                    'slip_no' => isset($oldSalePayment['slip_no']) ? $oldSalePayment['slip_no'] : "",
                    'deposit_date' => isset($oldSalePayment['deposit_date']) ? Carbon::parse($oldSalePayment['deposit_date']) : Carbon::now(),
                    'created_at' => Carbon::parse($oldSalePayment['created_at']),
                ]);


                //create invoice


                $invoice = Invoice::create([
                    'business_code' => $newSale->businessCode(),
                    'user_id' => auth()->id(),
                    'sale_id' => $newSale->id,
                    'customer_id' => $customerId,
                    'total_amount' => $oldSaleArray['total_amount'],
                    'total_paid' => $payment->amount,
                    'due_date' => Carbon::now()->addDays(30),
                    'created_at' => Carbon::parse($oldSaleArray['created_at']),
                ]);


                //create invoice items
                $invoice->invoiceItems()->create([

                    'business_code' => $newSale->businessCode(),
                    'plot_id' => $plot->id,
                    'price' => $plot->price,
                    'plot_no' => $plot->plot_no,
                    'created_at' => Carbon::parse($oldSaleArray['created_at']),
                ]);


                // customer sales


                $newSale->customerSales()->create([
                    'customer_id' => $customerId
                ]);


                // account


                $account = Account::create([

                    'user_id' => auth()->id(),
                    'amount' => $payment->amount,
                    'is_active' => true,
                    'source' => 'new sale',
                    'deposit_date' => isset($payment->deposit_date) ? Carbon::parse($payment->deposit_date) : Carbon::now(),
                    'created_at' => Carbon::parse($payment->created_at),
                ]);

                $newSale->account($account);


                //installment


                if (collect($oldInstallmentArray)->count() > 0) {
                    Installment::create([
                        'sale_id' => $newSale->id,
                        'months' => $oldInstallmentArray['months'],
                        'amount' => $oldInstallmentArray['amount'],
                        'completion_date' => Carbon::now()->addMonths($oldInstallmentArray['months'])
                    ]);
                }


                $oldWitness = $newSale->witnesses->map(function ($witness) {


                    $toUnset = $witness->attributesToArray();

                    unset($toUnset['id']);
                    unset($toUnset['sale_id']);

                    $toUnset['created_at'] = Carbon::parse($toUnset['created_at']);
                    $toUnset['updated_at'] = Carbon::parse($toUnset['updated_at']);

                    return $toUnset;


                })->toArray();

                if (collect($oldWitness)->count()) {
                    for ($j = 0; $j < sizeof($oldWitness['name']); $j++) {
                        $newSale->witnesses()->create([
                            'name' => $oldWitness['witness_name'][$j],
                            'id_no' => $oldWitness['witness_id'][$j],
                            'type' => $oldWitness['type'][$j],
                            'created_at' => $oldWitness['created_at'][$j],
                            'updated_at_at' => $oldWitness['updated_at'][$j],
                        ]);
                    }
                }


                //uploads



                if ($oldSale->saleUploads->count()){

                    foreach ($oldSale->saleUploads as $saleUpload) {
                        $newSale->saleUploads()->create([
                            'agreement_url' => $saleUpload->agreement_url,
                            'file_name' => $saleUpload->file_name,
                            'type' => $saleUpload->type
                        ]);


                    }
                }


                $oldSale->forceDelete();



                $plot->is_reserved = true;
                $plot->is_sold = true;
                $plot->can_sell = false;

                $plot->save();



            }


            DB::commit();

            Session::flash('success','Successfully separated the sale');

            return redirect(route('sale_manage'));


        } catch (\Exception $exception) {

            DB::rollBack();


            Session::flash('success','Successfully separated the sale');

            return back();

        }

    }
}
