<?php

namespace App\Http\Controllers\Sales;

use App\DB\Accounting\Account;
use App\Sale\Sale;
use Codex\Contracts\ServiceCaller;
use Codex\Services\Sales\ListSalesService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class PayTitleController extends Controller implements ServiceCaller
{
    public function index(Request $request , $saleId)
    {
        $sale = (new ListSalesService($this))->getSales(['id' => $saleId])->first();


        return view('sales.pay_for_title')->with([
            'sale' => $sale
        ]);

    }

    public function pay(Request $request)
    {
        try{
            DB::beginTransaction();

            $sale = Sale::where('id', $request['sale_id'])->first();

            if (isset($request['mpesa_reference_code']))
            {
                $request['reference_code'] = $request['mpesa_reference_code'];
            }


            $sale->salePayments()->create([
                'payment_type' => $request['payment_type'],
                'payment_method' => $request['payment_method'],
                'amount' => $request['amount'],
                'user_id' => auth()->id(),
                'reference_code' => isset($request['reference_code']) ? $request['reference_code'] : $request['payment_method'],
                'bank_name' => isset($request['bank_name']) ? $request['bank_name'] : "",
                'account_no' => isset($request['account_no']) ? $request['account_no'] : "",
                'cheque_no' => isset($request['cheque_no']) ? $request['cheque_no'] : "",
                'drawer' => isset($request['drawer']) ? $request['drawer'] : "",
                'slip_no' => isset($request['slip_no']) ? $request['slip_no'] : "",
            ]);

            /***********CREATE ACCOUNT****************/


            $account = Account::create([
                'user_id' => auth()->id(),
                'amount' => $request['amount'],
                'source' => 'Title Payment',
            ]);


            $sale->account($account);

            DB::commit();


            return response()->json([
                'status' => 'OK',
                "message" => "successfully paid for the title"
            ]);
        }
        catch (\Exception $exception)
        {

            DB::rollBack();



            return response()->json([
                'status' => 'ERROR',
                "message" => $exception->getMessage()
            ] , 500);
        }



    }
    public function successResponse($message)
    {
        // TODO: Implement successResponse() method.
    }
}
