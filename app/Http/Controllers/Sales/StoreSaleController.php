<?php

namespace App\Http\Controllers\Sales;

use App\DB\Customer\NextOfKin;
use App\DB\Witness;
use App\Http\Controllers\Controller;
use App\Sale\Sale;
use App\Traits\IsNextOfKin;
use Carbon\Carbon;
use Codex\Contracts\ServiceCaller;
use Codex\Services\Sales\StoreSaleService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Illuminate\Validation\Rule;

class StoreSaleController extends Controller implements ServiceCaller
{

    use IsNextOfKin;

    public function index(Request $request)
    {
        $rule = [

            'deposit_date' => 'required'
        ];

        if (isset($data['new_customer'])) {
            $rule = [
                "first_name" => [
                    'required|array',
                ],
                "last_name" => [
                    'required|array',
                ],

                "middle_name" => [
                    'required|array',
                ],

            ];

        }


        if (isset($request['is_paying']))
        {
            $rule["amount"] = [
                'required',
            ];
        }




        if ( strtolower($request['payment_method']) === 'bank' )
        {
            $rule["slip_no"] = [
                'required',
            ];

        }
        if ( strtolower($request['payment_method']) === 'mpesa' )
        {
            $rule["mpesa_reference_code"] = [
                'required',
            ];
        }
        if ( strtolower($request['payment_method']) === 'cheque' )
        {
            $rule["cheque_no"] = [
                'required',
            ];

            $rule["receipts"] = [
                'required',
            ];
        }



        $response = $this->validateAjaxForm($request, $rule);

        $request['months']  = isset($request['months']) ? $request['months'] : fanakaConfig('installment_months', 6) ;

        if ( isset($request['paymentOptionChecker']) && strtolower($request['paymentOptionChecker']) === 'cash')
        {
            $request['months'] = 1;
        }


        if (!$response instanceof $request) {
            return $response;
        }

        if (Carbon::parse($request['deposit_date']) > Carbon::now()){

            throw new \Exception("Kindly enter a deposit date not in future");

        }

        return (new StoreSaleService($this))->run($request->all());

    }

    public function witness(Request $request)
    {

        $sale = Sale::where('id', $request['sale_id'])->first();


        try{

            DB::beginTransaction();
            if (isset($request['next_of_kin_name']))
            {


               //foreach ($sale->customers as $customer) {

                    if (! empty($request['next_of_kin_name']) && !empty($request['next_of_kin_phone_number']) && !empty($request['next_of_kin_relation']))
                    {
                        $nextOfKinData = ([
                            'full_name'  => $request['next_of_kin_name'],
                            'phone_number'  => $request['next_of_kin_phone_number'],
                            'relation'  => $request['next_of_kin_relation'],

                        ]);

                        $responseNextOfKin = $this->nextOfKin($sale->customers->first() , $nextOfKinData);




                    }


                //}
            }

                if (isset($request['witness_name'])) {


                    for ($j = 0; $j < sizeof($request['witness_name']); $j++) {

                        if (! Witness::where([
                            'sale_id' => $sale->id,
                            'name' => $request['witness_name'][$j],
                            'id_no' => $request['witness_id'][$j],
                            'type' => $request['type'][$j],

                        ])->count())

                        {
                            $sale->witnesses()->create([
                                'name' => $request['witness_name'][$j],
                                'id_no' => $request['witness_id'][$j],
                                'type' => $request['type'][$j],
                            ]);
                        }
                    }
                }
                DB::commit();



                Session::flash('success', "Successfully added new witness");

                return back();

            }

            catch (\Exception $exception)
            {
                DB::rollBack();

                Session::flash('error', $exception->getMessage());

                return back();


        }
    }

    public function removeWitness($witnessId)
    {
        Witness::where('id', $witnessId)->forcedelete();
        Session::flash('success', "successfully removed the witness");

        return back();




    }
    public function removeKins($kinId)
    {

        NextOfKin::where('id', $kinId)->forcedelete();
        Session::flash('success', "successfully removed the next of kin");

        return back();




    }

    public function successResponse($message)
    {
       // Session::flash('success', $message);

        return response()
            ->json([
                'message' => $message
            ]);


        return redirect(route('sale_manage'));
    }

    public function failureResponse($message)
    {
       // Session::flash('success', $message);


        return response()
            ->json([
                'message' => $message
            ] , 500);


        return redirect(route('sale_manage'));
    }
}
