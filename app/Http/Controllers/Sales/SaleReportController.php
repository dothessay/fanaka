<?php

namespace App\Http\Controllers\Sales;

use App\Charts\AgentsPerformance;
use App\Customer\Customer;
use App\DB\Expense\Expense;
use App\DB\PrintContract;
use App\DB\Product\Product;
use App\DB\Product\ProjectExpense;
use App\DB\Sale\CustomerSale;
use App\DB\Sale\MonthlyInstallmentPlan;
use App\Sale\Invoice;
use App\Sale\Sale;
use App\Sale\SalePayment;
use App\User;
use Carbon\Carbon;
use Codex\Classes\Filters\AgentFilter;
use Codex\Classes\Filters\DateBetweenFilter;
use Codex\Classes\Filters\DueDateBetweenFilter;
use Codex\Classes\GeneratePdf;
use Codex\Classes\Handlers\Exports\ExportExcelReports;
use Codex\Classes\Handlers\Exports\ExportSaleType;
use Codex\Classes\PdfOutput\DataReport;
use Codex\Classes\Reports\CustomerReport;
use Codex\Classes\Reports\InstallementPlanReport;
use Codex\Classes\Reports\Sales\Income;
use Codex\Classes\Reports\Sales\SaleReportGenerator;
use Codex\Classes\Repository\ProjectRepository;
use Codex\Classes\Repository\SaleItemRepository;
use Codex\Classes\Repository\SalesRepository;
use Codex\Classes\Repository\UserRepository;
use Codex\Contracts\ServiceCaller;
use Codex\Services\Offer\ListOffer;
use Codex\Services\Plots\ListPlotsService;
use Codex\Services\Products\ListProductsService;
use Codex\Services\Sales\ListSalesService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Pipeline\Pipeline;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;

class SaleReportController extends Controller implements ServiceCaller
{

    use SaleReportGenerator;

    public function index(Request $request)
    {

        return view('sales.report.reports');

        /*$options['start_date'] = isset($request['start_date']) ? $request['start_date'] : Carbon::now()->startOfCentury();
        $options['end_date'] = isset($request['end_date']) ? $request['end_date'] : Carbon::now();

        return (new ListSalesService($this))->run([], $options);*/

    }


    public function above50(Request $request)
    {

        $startDate = isset($request['start_date']) ? $request['start_date'] : Carbon::now()->startOfYear();
        $endDate = isset($request['end_date']) ? $request['end_date'] : Carbon::now()->endOfYear();


        if (isset($request['project_code'])) {

            $sales = Product::where('code', $request['project_code'])->first()->sales();


        } else {
            $sales = (new ListSalesService($this))->getSales([]);
        }


        //agents

        $agentId = '';

        if (isset($request['agent']) && !is_null($request['agent'])) {
            $agentId = $request['agent'];


            $sales = (collect($sales)->where('agent', $agentId));

        }

        $agents = (new  UserRepository())->all();

        $salesArray = collect();

        //get for the data ranges;

        $sales = $this->hasAboveHalf($sales, ['start_date' => $startDate, 'end_date' => $endDate]);


        foreach ($sales as $sale) {


            $salesArray->push($this->percentage($sale, ">"));


        }


        $salesArray = collect($salesArray);

        if (isset($request['pdf'])) {
            $html = view('sales.report.above.data')->with(['sales' => $salesArray])->render();

            return (new DataReport())->outPut($html);
        }


        return view('sales.report.above50.above')->with([
            'sales' => $salesArray,
            'projects' => Product::all(),
            'agents' => $agents,

        ]);
    }

    public function below50(Request $request)
    {

        $startDate = isset($request['start_date']) ? $request['start_date'] : Carbon::now()->startOfYear();
        $endDate = isset($request['end_date']) ? $request['end_date'] : Carbon::now()->endOfYear();


        if (isset($request['project_code'])) {

            $sales = Product::where('code', $request['project_code'])->first()->sales();


        } else {
            $sales = (new ListSalesService($this))->getSales([]);
        }


        //agents

        $agentId = '';


        //if can only see my sales

        if (!auth()->user()->can('access-module-component-functionality', 'sale_manage_show-all-sales')) {

            $agentId = auth()->id();


        }
        if (auth()->user()->can('access-module-component-functionality', 'dashboard_see-department-details')) {

        }

        if (isset($request['agent']) && !is_null($request['agent'])) {
            $agentId = $request['agent'];


            $sales = (collect($sales)->where('agent', $agentId));

        }

        $agents = (new  UserRepository())->all();

        $salesArray = collect();
        foreach ($sales as $sale) {


            $percentage = $sale->getPercentage();
            $salesToPush = $sale;

            if ($percentage < 50) {
                $salesArray->push($salesToPush);

            }


            $totalPaid = $sale->salePayments->sum('amount');


        }


        $salesArray = collect($salesArray);

        if (isset($request['pdf'])) {
            $html = view('sales.report.below50.data')->with(['sales' => $salesArray])->render();

            return (new DataReport())->outPut($html);
        }


        return view('sales.report.below50.below')->with([
            'sales' => $salesArray,
            'projects' => Product::all(),
            'agents' => $agents,

        ]);
    }

    public function unpaid(Request $request)
    {

        $startDate = isset($request['start_date']) ? $request['start_date'] : Carbon::now()->startOfYear();
        $endDate = isset($request['end_date']) ? $request['end_date'] : Carbon::now()->endOfYear();

        if (isset($request['project_code'])) {

            $sales = Product::where('code', $request['project_code'])->first()->sales();


        } else {
            $sales = (new ListSalesService($this))->getSales([]);
        }


        $salesArray = collect();
        foreach ($sales as $sale) {
            $totalPaid = $sale->salePayments->sum('amount');


            $percentage = $sale->getPercentage();

            if ($percentage < 100) {
                $salesArray = $salesArray->push($sale);
            }


        }


        $salesArray = collect($salesArray);


        return view('sales.report.unpaid.index')->with([
            'sales' => $salesArray,
            'projects' => Product::all(),
        ]);
    }

    public function completed(Request $request)
    {

        $salesArray = collect();


        $projects = (new ProjectRepository())->all();


        if (isset($request['product_id']))
        {
           $salesItems =  (new SaleItemRepository())->all($request);


           foreach ($salesItems as $salesItem)
           {
               if ($salesItem->sale->getBalance() < 1 )
               {
                   $salesArray->push($salesItem->sale);
               }
           }
        }

        if (isset($request['pdf'])) {
            $html = view('sales.report.completed.data')->with(['completed' => $salesArray])->render();

            return (new DataReport())->outPut($html);
        }
        if (isset($request['excel'])) {
            $html = view('sales.report.completed.data')->with(['completed' => $salesArray]);

            return Excel::download(new ExportExcelReports($html), 'completed_report'.now()->format('y-m-d').'.xlsx');

        }


        return view('sales.report.completed.completed')->with(['completed' => $salesArray, 'projects'  => $projects]);
    }

    public function byProject(Request $request)
    {
        ini_set('max_execution_time', 50000);

        if ($request->has('product_id')  && $request['product_id'] === 'all')
        {
            $request['sale_type'] = isset($request['sale_type']) ? $request['sale_type'] : "Instalment";

            //$request['with_balance'] = $request->has('with_balance') ? $request['with_balance'] :  true;

            $request->request->remove('product_id');

        }

        $projects = (new ProjectRepository())
            ->all();

        $sales = collect();
        $saleItems = (new SaleItemRepository())
            ->all($request);

        foreach ($saleItems as $saleItem) {

            if (\request()->has('sale_type')) {
                if (strtolower($saleItem->sale_type) === strtolower($request['sale_type'])) {


                    if (strtolower($saleItem->sale_type === 'cash') && $saleItem->sale->installmentPlans->count() < 2) {
                        $sales->push($saleItem->sale);
                    }
                    else{

                        $sales->push($saleItem->sale);
                    }
                }
            }

        }

        if ($request->has('with_balance')) {
            $sales = $sales->filter(function ($sale) {
                if (($sale->getBalance() > floatval(1))) {
                    return $sale;
                }
            });

        }

        if ($request->has('pass_month')) {
            $createdAt = now()->subRealMonths(1);

            $sales = $sales->filter(function ($sale) use ($createdAt) {
                if (Carbon::parse($sale->created_at)->lessThan($createdAt)) {

                    return $sale;
                }
            });
        }

        $sales = $sales->unique('id')->sortByDesc('created_at');

        if ($request->has('pdf')) {

            $html = view('sales.report.project.data')->with([
                'sales' => $sales
            ])->render();

            (new DataReport())->outPut($html, $request->sale_type . ' Report', \Mpdf\Output\Destination::INLINE);

        }

        if ($request->has('excel')) {

            ///return Excel::download(new ExportSaleType($sales), 'sale_type_cash_and_instalment.xlsx');

            $view = view('sales.report.project.data')->with([
                'sales' => $sales
            ]);

            return Excel::download(new ExportExcelReports($view), 'sale_type_cash_and_instalment_'.now()->format('y-m-d').'.xlsx');

        }
        return view('sales.report.project.project')->with([
            'projects' => $projects,
            'sales' => $sales->sortByDesc('created_at')
        ]);

    }

    public function successResponse($message)
    {
        $sales = $message;


        $saleArray = [];


        foreach ($sales as $sale) {


            $saleArray[$sale->customer->fullName()][] = [
                'sale_amount' => $sale->total_amount,
                'sale_balance' => (isset($sale->invoices) ? $sale->invoices->sum('total_amount') : 0) - (isset($sale->invoices) ? $sale->invoices->sum('total_paid') : 0),
                'plots' => $sale->saleItems->pluck('plot_no')->toArray(),
                'sale_date' => $sale->created_at,
                'total_sale_amount' => $sale->sum('total_amount')


            ];


        }

        $pdf = \request('pdf');

        if (isset($pdf)) {
            $html = view('sales._customer_data')->with(['sales' => collect($saleArray)])->render();

            return (new DataReport())->outPut($html);

        }

        return view('sales.customer_report')->with([
            'sales' => collect($saleArray)
        ]);
    }


    public function byDate(Request $request)
    {
        $startDate = isset($request['start_date']) ? $request['start_date'] : Carbon::parse('12:00')->subDays(1);
        $endDate = isset($request['end_date']) ? $request['end_date'] : Carbon::now()->endOfYear();
        $sales = (new ListSalesService($this))->getSales([], [
            'start_date' => $startDate,
            'end_date' => $endDate
        ]);

        $saleCollections = collect();

        if ($request['sale_type'] == 'cash') {
            foreach ($sales as $sale) {


                if (isset($sale->installment->months) && $sale->installment->months <= 1 && $sale->getBalance() > 0 || $sale->getMeta('payment_option') == 'cash') {

                    $saleCollections->push($sale);
                }
            }
        } else {
            foreach ($sales as $sale) {
                $saleCollections->push($sale);
            }

        }

        if (isset($request['pdf'])) {
            $html = view('sales.report.date.data')->with(['sales' => $saleCollections])->render();

            return (new DataReport())->outPut($html);
        }


        return view('sales.report.date.index')->with(['sales' => $saleCollections]);

    }


    public function availability(Request $request)
    {
        $isReserved = isset($request['is_reserved']) ? $request['is_reserved'] : false;

        $data = [];

        //  dd($request->all());


        if (isset($request['product_code'])) {
            $data['product_code'] = isset($request['product_code']) ? $request['product_code'] : null;
        }

        $data['is_reserved'] = $isReserved;

        $options = [
            'is_sold' => $isReserved
        ];


        $plots = (new ListPlotsService($this))->list($data, $options);

        if ($isReserved == false) {
            $plots = collect((new ListPlotsService($this))->list($data, $options))->where('is_sold', false);
        }


        $projects = (new ListProductsService($this))->list([]);


        return view('sales.report.availability.project')->with([
            'plots' => $plots,
            'projects' => $projects
        ]);

    }

    public function scannedAgreement(Request $request)
    {
        $unprinted = collect();
        if ($request->has('un_printed')) {
            $printedIds = PrintContract::all()->pluck('sale_id')->toArray();

            $unprinted = Sale::whereNotIn('id', $printedIds)
                ->whereBetween('created_at', [
                    Carbon::parse($request->start_date), Carbon::parse($request->end_date)
                ])
                ->Where([
                    'is_agreement_approved' => false,
                    'is_agreement_downloaded' => false
                ])
                ->get();

            if (\request()->has('pdf')) {
                $html = view('sales._list_table', ['sales' => $unprinted, 'show_actions' => false])->render();
                (new DataReport())->outPut($html);
            }

        }

        $sales = Sale::where([
            'is_agreement_uploaded' => true
        ])->paginate(10);

        //dd($sales);

        return view('sales.report.agreements.scanned')->with([
            'sales' => $sales,
            'unprinted' => $unprinted
        ]);

    }


    public function lateInstallment(Request $request)
    {


        $startDate = isset($request['start_date']) ? $request['start_date'] : Carbon::now()->startOfMonth();
        $dueDate = isset($request['end_date']) ? $request['end_date'] : Carbon::today();
        $invoices = Invoice::where('is_paid', false)
            ->whereBetween('due_date', [Carbon::parse($startDate), Carbon::parse($dueDate)])
            ->get();


        if (isset($request['pdf']) && !is_null($request['pdf'])) {

            $html = view('sales.report.late._data')
                ->with([
                    'invoices' => $invoices
                ]);

            return (new DataReport())->outPut($html, "Late installment Report");


        }


        return view('sales.report.late.index')
            ->with([
                'invoices' => $invoices
            ]);


    }

    public function weekInstallment(Request $request)
    {
        $request['start_date'] = $request->has('start_date') ? $request['start_date'] : now()->startOfWeek();
        $request['end_date'] = $request->has('end_date') ? $request['end_date'] : now()->endOfWeek();


        $plans = (new InstallementPlanReport())
            ->setStartDate($request['start_date'])
            ->setEndDate($request['end_date'])
            ->plan();

        if ($request->has('pdf')) {
            $html = View('sales.report.week._data')
                ->with([
                    'plans' => $plans
                ])->render();


            return (new DataReport())
                ->outPut($html, 'Late Payment Reports');

        }
        if ($request->has('excel')) {
            $html = View('sales.report.week._data')
                ->with([
                    'plans' => $plans
                ]);

           return  Excel::download(new ExportExcelReports($html), 'weekely_report_'.now()->format('y-m-d').'.xlsx');
        }


        return view('sales.report.week.index')
            ->with([
                'plans' => $plans
            ]);


    }

    public function reservation(Request $request)
    {


        $startDate = isset($request['start_date']) ? Carbon::parse($request['start_date']) : Carbon::now()->startOfDay();
        $endDate = isset($request['end_date']) ? Carbon::parse($request['end_date']) : Carbon::now()->endOfDay();

        $options = [
            'start_date' => $startDate,
            'end_date' => $endDate
        ];
        $reservations = (new ListOffer($this))->list([], $options);

        return view('sales.report.reservation.index')->with([
            'reservations' => $reservations
        ]);

    }

    public function byCustomer(Request $request)
    {
        $request->request->add([
            'start_date' => isset($request['start_date']) ? Carbon::parse($request['start_date']) : Carbon::now()->startOfMonth(),
            'end_date' => isset($request['end_date']) ? Carbon::parse($request['end_date']) : Carbon::now()->endOfDay()
        ]);


        try {
            $sales = (new CustomerReport())->output();
        } catch (\Exception $exception) {
            $sales = collect();
        }

        $projects = Product::all();

        $customers = $request->user()->customers();

        $agents = (new  UserRepository())->all();

        if ($request->has('pdf')) {
            $html = view('sales.report.customer._data')
                ->with([
                    'sales' => $sales
                ])
                ->render();

            (new DataReport())
                ->outPut($html, 'Customer Report');
        }

        return view('sales.report.customer.index')->with([
            'customers' => $customers,
            'sales' => $sales,
            'agents' => $agents,
            'projects' => $projects
        ]);


    }


    public function byAgent(Request $request)
    {

        $where = [];


        $users = (new  UserRepository())->all();


        $startDate = isset($request['start_date']) ? Carbon::parse($request['start_date']) : Carbon::now()->startOfCentury();
        $endDate = isset($request['end_date']) ? Carbon::parse($request['end_date']) : Carbon::now()->endOfDay();


        $options = [
            'start_date' => $startDate,
            'end_date' => $endDate
        ];


        $sales = collect();


        if (isset($request['type']) && $request['type'] === 'sale') {

            if (is_null($request['agent'])) {
                unset($request['agent']);
            }

            $sales = (new  SalesRepository())->all($request);

        }

        if (isset($request['type']) && $request['type'] === 'reservation') {

            unset($where['agent']);

            if (isset($request['agent']) && !is_null($request['agent'])) {

                $where['agent_id'] = $request['agent'];
            }

            $sales = (new ListOffer($this))->getOffers($where, $options);
        }


        if (isset($request['pdf'])) {


            $html = view('sales.report.agent._data')->with([
                'agents' => $users,
                'sales' => $sales
            ])->render();


            return (new DataReport())->outPut($html, "Report By Agent");


        }

        if (isset($request['excel'])) {


            $html = view('sales.report.agent._data')->with([
                'agents' => $users,
                'sales' => $sales
            ]);

            return  Excel::download(new ExportExcelReports($html), 'weekely_report_'.now()->format('y-m-d').'.xlsx');


        }


        return view('sales.report.agent.index')->with([
            'agents' => $users,
            'sales' => $sales
        ]);


    }

    public function profitLoss(Request $request)
    {
        if (!auth()->user()->can('access-module-component-functionality', 'sale_reports_profit-and-loss')) {
            return redirect()->route('dashboard')->with(['error' => 'Access Denied',])->setStatusCode('403', 'Access Denied');
        }
        // this income - this expenses

        $income = app(Pipeline::class)
            ->send(SalePayment::query())
            ->through([
                DateBetweenFilter::class
            ])
            ->thenReturn()
            ->get()
            ->sum('amount');


        $expenses = app(Pipeline::class)
            ->send(Expense::query())
            ->through([
                DateBetweenFilter::class
            ])
            ->thenReturn()
            ->get()
            ->sum('amount');

        $projectExpenses = app(Pipeline::class)
            ->send(ProjectExpense::query())
            ->through([
                DateBetweenFilter::class
            ])
            ->thenReturn()
            ->get()
            ->sum('amount');

        $expenses = $expenses + $projectExpenses;

        return view('sales.report.profitLoss.browse')
            ->with([
                'expenses' => $expenses,
                'income' => $income
            ]);
    }

    public function performance(Request $request)
    {
        $request['start_date'] = $request->has('start_date') ? $request['start_date'] : Carbon::now()->startOfYear();
        $request['end_date'] = $request->has('end_date') ? $request['end_date'] : Carbon::now()->endOfYear();

        $income = (new Income());

        $payments = $income->sales();
        $agents = $income->all($payments);

        $chart = (new  AgentsPerformance());
        $labels = [];
        $dataSet = [];
        foreach ($agents as $index  => $agent) {

            $labels[] = $agent['name'];
            $dataSet[] = $agent['amount'];
        }

        $chart->labels( $labels)
            ->dataset("Agents",'bar', $dataSet )
            ->color($chart->setColors())
            ->backgroundColor(collect($chart->setColors()));


        //$chart = (new  AgentsPerformance())->dataset('')


        if ($request->has('pdf'))
        {
            $html = view('sales.report.performance._data')->with([
                'agents'  => $agents,
                'chart'  => $chart
            ])->render();

            return (new DataReport())->outPut($html , "All_Marketers_performance".now()->format('y-m-d'));
        }
        if ($request->has('excel'))
        {
            $html = view('sales.report.performance._data')->with([
                'agents'  => $agents,
            ]);


            return  Excel::download(new ExportExcelReports($html), 'All_Marketers_performance'.now()->format('y-m-d').'.xlsx');
        }
        return view('sales.report.performance.index')->with([
            'agents'  => $agents,
            'chart'  => $chart
        ]);
    }
    public function agentPerformance(Request $request , User $user)
    {
        $request->request->add(['agentId' => $user->id]);

        $request['start_date'] = $request->has('start_date') ? $request['start_date'] : Carbon::now()->startOfYear();
        $request['end_date'] = $request->has('end_date') ? $request['end_date'] : Carbon::now()->endOfYear();

        $income = (new Income());

        $payments = $income->sales();
        $agents = $income->single($payments);

        $chart = (new  AgentsPerformance());
       $labels = [];
       $dataSet = [];

        foreach ($agents as $index  => $agent) {

            $labels[] = $index;
            $dataSet[] = $agent['amount'];

        }
        $type = "bar";
        if (sizeof($labels) >= 2)
        {
            $type = "line";
        }

        $chart->labels( $labels)
            ->dataset($user->fullName(),$type, $dataSet );
            //->backgroundColor(collect($chart->setColors()));


        //$chart = (new  AgentsPerformance())->dataset('')
        return view('sales.report.performance.show.index')->with([
            'agents'  => $agents,
            'chart'  => $chart
        ]);
    }

    public function agentPerformanceDetails(Request $request ,User $user , $month)
    {

        $request->request->add([
            'start_date'  => Carbon::parse($month)->subRealMonth()->format('Y-m-25'),
            'end_date'  => Carbon::parse($month)->format('Y-m-25'),
            'agentId'   => $user->id
        ]);

        $sales = (new Income())->sales();

        return view('sales.report.performance.show.single_show.index')
            ->with([
                'sales'  => $sales,
                'user' => $user,
                'month'  => $month
            ]);
    }

    public function byAgentCommissions(Request $request)
    {
        $request->request->add([
            'commission_start_date'  => Carbon::parse($request['commission_start_date'])->subRealMonth()->format('Y-m-25'),
            'commission_end_date'  => Carbon::parse($request['commission_end_date'])->format('Y-m-25'),
        ]);


        $sales = (new SalesRepository())->all($request);

        $agents = (new UserRepository())->all();
        return view('sales.report.commissions.index')
            ->with([
                'sales' => $sales->map(function ($q) { return $q->saleToArray() ; }),
                'agents'  => $agents
            ]);

    }
}
