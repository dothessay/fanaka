<?php

namespace App\Http\Controllers\Sales;

use App\Sale\Sale;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ViewSaleController extends Controller
{
    public function index(Request $request)
    {


        if ($request['report'] === 'monthly')
        {
            $sales = Sale::whereBetween('created_at',[Carbon::now()->startOfMonth() , Carbon::now()->endOfWeek()])->get();
        }

        if ($request['report'] === 'yearly')
        {
            $sales = Sale::whereBetween('created_at',[Carbon::now()->startOfYear() , Carbon::now()->endOfYear()])->get();
        }

        if ($request['report'] === 'weekly')
        {
            $sales = Sale::whereBetween('created_at',[Carbon::now()->startOfWeek() , Carbon::now()->endOfWeek()])->get();
        }

        if ($request['report'] === 'daily')
        {
            $sales = Sale::whereBetween('created_at',[Carbon::now()->startOfDay() , Carbon::now()->endOfDay()])->get();
        }



        return view('sales.report.view_sales')->with([
            'report' => $request['report'],
            'sales' => $sales
        ]);

    }
}
