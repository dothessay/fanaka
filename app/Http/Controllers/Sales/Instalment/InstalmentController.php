<?php

namespace App\Http\Controllers\Sales\Instalment;

use App\Http\Controllers\Controller;
use App\Sale\Sale;
use Illuminate\Http\Request;

class InstalmentController extends Controller
{
    public function plan(Request $request)
    {
        $invoices = auth()->user()->unpaid()->filter();
        return view('sales.instalment.plan')->with([
            'invoices'   => $invoices
        ]);
        
    }

    public function show(Sale $sale)
    {
        $sale = $sale->load('installmentPlans');

        return view('sales.instalment.plan_details')
            ->with([
                'sale'  => $sale
            ]);

    }
}
