<?php

namespace App\Http\Controllers\Sales;

use App\Customer\Customer;
use App\DB\Contract;
use App\DB\PrintContract;
use App\DB\PrintReservation;
use App\DB\Sale\Installment;
use App\Sale\Sale;
use App\Sale\SaleItem;
use App\User;
use Carbon\Carbon;
use Codex\Classes\GeneratePdf;
use Codex\Classes\Handlers\SendEmailHandler;
use Codex\Classes\Handlers\SendInnoxNotification;
use Codex\Classes\Helper;
use Codex\Classes\PdfOutput\ContractOutPut;
use Codex\Classes\PdfOutput\ReceiptOutPut;
use Codex\Classes\Repository\ChangeSalePlotRepository;
use Codex\Classes\Repository\MonthlyInstallmentPlan;
use Codex\Contracts\ServiceCaller;
use Codex\Services\Sales\ListSalesService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

class EditSaleAgreementController extends Controller implements ServiceCaller
{
    public function index($saleId)
    {
        $sale = Sale::where('id', $saleId)->first();

        if (! isset($sale->salePayments->last()->created_at))
        {
            \session()->flash('error','Please approve the payment before writing agreement');
            return back();
        }


        if($reservationOutPut = PrintContract::where('sale_id', $saleId)->first()){
            $agreement = $reservationOutPut->value;
        }
        else{
            $contract  = Contract::where('name', 'agreement')->first();

            $agreement = $contract->value;
        }


        $output = (new Contract())->getSaleDetails($sale);

        /*
        $agreement = str_replace("#date",Carbon::now()->format('Y-m-d') , $agreement);
        $agreement = str_replace("#purchaser",$output->purchaser , $agreement);
        $agreement = str_replace("#plotDetails",$output->plotDetails , $agreement);
        $agreement = str_replace("#saleAmountInWords",$output->saleAmountInWords , $agreement);
        $agreement = str_replace("#saleAmountInWords",$output->saleAmountInWords , $agreement);
        $agreement = str_replace("#saleAmountInFigures",$output->saleAmountInFigures , $agreement);
        $agreement = str_replace("#paidAmount",$output->paidAmount , $agreement);
        $agreement = str_replace("#installmentStatement",$output->installmentStatement , $agreement);
        $agreement = str_replace("of 12%","<b>of 12%</b>" , $agreement);
        */

        $agreement = str_replace("{date}",Carbon::now()->format('Y-m-d') , $agreement);
        $agreement = str_replace("{purchaser}",$output->purchaser , $agreement);
        $agreement = str_replace("{motherTitle}",$output->motherTitle , $agreement);
        $agreement = str_replace("{project}",$output->project , $agreement);
        $agreement = str_replace("{plotNo}",$output->plotNo , $agreement);
        $agreement = str_replace("{plotDetails}",$output->plotDetails , $agreement);
        $agreement = str_replace("{saleAmountInWords}",$output->saleAmountInWords , $agreement);
        $agreement = str_replace("{saleAmountInWords}",$output->saleAmountInWords , $agreement);
        $agreement = str_replace("{saleAmountInFigures}", number_format(floatval($output->saleAmountInFigures ) , 2), $agreement);
        $agreement = str_replace("{paidAmount}",$output->paidAmount , $agreement);
        $agreement = str_replace("{installmentStatement}",$output->installmentStatement , $agreement);
        $agreement = str_replace("{director}", fanakaConfig('director') , $agreement);
        $agreement = str_replace("{vendor}", fanakaConfig('vendor') , $agreement);
        $agreement = str_replace("{purchaserSignature}", $sale->signatories() , $agreement);
        $agreement = str_replace("{balanceInFigures}",  number_format( floatval($sale->getBalance()) , 2) , $agreement);
        $agreement = str_replace("{balanceInWords}", Helper::numberToWord(floatval($sale->getBalance())) , $agreement);
        $agreement = str_replace("{paidAmountInWords}", Helper::numberToWord(floatval($sale->getPaidAmount())) , $agreement);
        $agreement = str_replace("{instalmentAmountInWord}", Helper::numberToWord(floatval($sale->getMonthlyInstalment())) , $agreement);
        $agreement = str_replace("{instalmentAmountInFigures}", floatval($sale->getMonthlyInstalment()) , $agreement);
        $agreement = str_replace("{months}",  $sale->getInstalmentMonths() , $agreement);
        $agreement = str_replace("{saleStartDate}",  Carbon::parse($sale->created_at)->format('d F Y'), $agreement);
        $agreement = str_replace("{paidAmountInFigures}", number_format(floatval($sale->getPaidAmount()) , 2) , $agreement);
        $agreement = str_replace("{datePaid}", Carbon::parse($sale->salePayments->last()->created_at)->format('d F Y') , $agreement);
        $agreement = str_replace("{blockNo}", $sale->getBlockNo() , $agreement);
        $agreement = str_replace("{sizeApproximate}", $sale->sizeApproximate() , $agreement);
        $agreement = str_replace("{instalmentCompletionDate}", $sale->getSaleCompletionDate() , $agreement);
        $agreement = str_replace("of 12%","<b>of 12%</b>" , $agreement);

        $plans = '<ul>';
        $count = 1 ;
        foreach ($sale->installmentPlans as $instalmentPlan) {

            $count ++;
            $plans .=  "<li><strong>". Helper::numberToWord( (int) $instalmentPlan->amount , 2). " (Kshs.". number_format( (int)
                $instalmentPlan->amount , 2).") </strong>shall be paid by the purchaser to the aforementioned vendor’s 
                bank account on or before <strong> ".$instalmentPlan->day .''. $instalmentPlan->getNthDate()." day of ".
                Carbon::parse($instalmentPlan->month)->format('F Y')."</strong></li>";
        }
        $plans .= "</ul>";

        $agreement = str_replace("{instalmentplan}", $plans , $agreement);

        return view('sales.agreement.edit')->with([
            'sale' => Sale::where('id', $saleId)->first(),

            'agreement' => $agreement
        ]);

    }

    public function update(Request $request , Sale $sale)
    {

        try {
            DB::beginTransaction();
            $html = $request['agreement'];



            if (! PrintContract::where('sale_id', $sale->id)->count())
            {
                PrintContract::create([
                    'value' => $html,
                    'sale_id'  => $sale->id,
                    'user_id' => auth()->id(),
                ]);
            }else{
                $reservation  = PrintContract::where('sale_id', $sale->id)->first();
                $reservation->value = $html;
                $reservation->save();
            }

            $sale = Sale::where('id', $sale->id)->first();


            if (auth()->user()->cannot('access-module-component-functionality','sale_manage_approve-agreement'))
            {

                if (!$sale->agreement_request && ! $sale->is_agreement_approved) {


                    $sale->agreement_request = true;


                    $sale->save();


                    $tos = config('config.admin_emails');


                    $approveAggrementUri = url('sale/manage/approve-agreement/' . $sale->id);


                    foreach ($tos as $index => $to) {

                        $data = [
                            'to' => $to,
                            'subject' => 'Print Agreement Request',
                            'message' => "Hi {$index},                                                         Please note that there is a agreement print request for: {$sale->getCustomerDetails()} for project(s);{$sale->getCustomerPlots()} amount paid " . config('config.currency') . ' ' . number_format((double)$sale->salepayments->sum('amount'), 2) . '    '. $approveAggrementUri."That requires your urgent approval.                                                          Thank You!"
                        ];

                        SendInnoxNotification::build()
                            ->sendSms(new SendEmailHandler(), $data);

                        //dispatch(new SendSmsNotifications($data));
                    }

                    DB::commit();

                    Session::flash("success", "You have sent a print request successfully");


                    return back();

                }
            }


            if(auth()->user()->can('access-module-component-functionality','sale_manage_approve-agreement'))
            {


                if (isset($request['approved'])) {

                    $sale->is_agreement_approved =  true;

                    $sale->save();

                    DB::commit();

                    Session::flash("success", "successfully approved the sale agreement");


                    return back();
                }

            }
            else{
                if ($sale->agreement_request && !$sale->is_agreement_approved) {



                    Session::flash("error", "Please wait for print agreement request to be reviewed and approved");


                    return back();

                }
            }
            $sale->is_agreement_downloaded = true;


            $sale->save();


            $output = '';
            DB::commit();

            return ContractOutPut::build()->generate($sale , $html);
        }
        catch (\Exception $exception)
        {
            DB::rollBack();

            Session::flash("error", $exception->getMessage());


            return back();
        }
    }


    public function editInstallment( $saleId)
    {

            \request()->request->add(['id' => $saleId]);


        //dd(Sale::latest('id')->first());

        $sale = (new ListSalesService($this))->getSales(['id' => $saleId])->first();

        $agents = User::active()->get();

        $customers = Customer::all();



        return view('sales.agreement.index')->with([
            'sale' => $sale,
            'agents' => $agents,
            'customers' => $customers
        ]);

    }

    public function  updateInstallment(Request $request , Sale $sale)
    {


        try{

            DB::beginTransaction();

            $sale->total_amount = $request['agreed_amount'];

            $sale->save();

            // create instalment months
            $request->request->add(['amount' =>  $sale->salePayments->sum('amount')]);
            $request->request->add(['months' =>  $request['installment_months']]);
            $request->request->add(['total' =>  $request['agreed_amount']]);

            (new MonthlyInstallmentPlan())
                ->setSale($sale)
                ->store($request);


            if ($sale->installment)
            {
                $installment =  $sale->installment;

                $installment->months = $request['installment_months'];
                $installment->amount = $request['installment_amount'];
                $installment->completion_date = Carbon::parse($installment->created_at)->addMonths($request['installment_months']);

                $sale->agent = $request['agent_id'];

                $sale->save();


                $installment->save();
            }



            foreach ($sale->saleItems as $saleItem) {

                $saleItem->price = $request['selling_price'][$saleItem->plot->id];

                $saleItem->save();
            }

            if ( $sale->invoices)
            {


                $invoice = $sale->invoices;
                $invoice->total_amount = $request['agreed_amount'];

                foreach ($invoice->invoiceItems as $invoiceItem) {

                    $invoiceItem->price = $request['selling_price'][$invoiceItem->plot->id];

                    $invoiceItem->save();

                }
            }



            if (! is_null($request['additional_info']))
            {


                $sale->notes()->create([
                    'note' => $request['additional_info']
                ]);
            }


            DB::commit();



            Session::flash('success', "Details updated successfully");

            return redirect(url('sale/manage?customer_id='.$sale->customers->first()->id));

        }Catch(\Exception $exception){
            Session::flash('error', $exception->getMessage());
            DB::rollBack();

            return back();

        }

    }
    public function successResponse($message)
    {
        // TODO: Implement successResponse() method.
    }

    public function changePlot(Request $request , Sale $sale)
    {

        if(! auth()->user()->can('access-module-component-functionality','sale_manage_change-project')) {

            Session::flash("error", "Permission Denied");


            return back();
        }
        if($request->price < 1){

            \session()->flash('error','Update the amount');

           return  back();
        }

        (new ChangeSalePlotRepository())
            ->setSaleItem(SaleItem::findOrFail($request['sale_id']))
            ->update($request);

        \session()->flash('success','Changed Plot successfully');

        return redirect()
            ->route('sale_manage');

    }

    function  test(){


    }
}
