<?php

namespace App\Http\Controllers\Sales\Transfer;

use App\Customer\Customer;
use App\DB\Sale\TitleProcess;
use Codex\Classes\Handlers\Table;
use Codex\Classes\Handlers\TitleTransferProcess;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class TransferController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return  view('transfer.index');
        
    }

    public function allTransfers(Request $request)
    {
        $count = TitleProcess::count();
        $limit = 10;
        $offset = $request['start'];;


        if (! is_null($request['search']['value'] ))
        {
            $limit = $count;


        }
        $customers = Table::build()
            ->setLimit($limit)
            ->setOffset($offset)
            ->setBuilder(TitleProcess::query())
            ->process()
            ->output();

        $customers = collect($customers);



        return response()
            ->json(
                array(
                    "draw"            => isset ( $request['draw'] ) ? intval( $request['draw'] ) : 0,
                    "recordsTotal"    => intval( $customers->count() ),
                    "recordsFiltered" => intval(  $count),
                    "data"            => $customers
                )
            );
        
    }

    public function startProcessing(TitleProcess $titleProcess)
    {

        (new TitleTransferProcess())
            ->setTitleProcess($titleProcess)
            ->startProcessing();
        session()->flash('success', 'started processing title');
        return back();
    }

    public function uploadDocuments(TitleProcess $titleProcess)
    {

        return view('transfer.upload')
            ->with([
                'sale' =>  $titleProcess->sale
            ]);

    }

    public function storeUploadDocuments(TitleProcess $titleProcess)
    {

        (new TitleTransferProcess())
            ->setTitleProcess($titleProcess)
            ->uploadDocuments();

        session()->flash('success','Uploaded successfully');

        return redirect()
            ->route('sale_transfer');

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
