<?php

namespace App\Http\Controllers\Sales\Payment;

use App\Sale\SalePayment;
use Codex\Scopes\IsApproved;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class ConfirmPaymentController extends Controller
{
    public function update(Request $request)
    {

       try{

           DB::beginTransaction();

           $payment = SalePayment::withoutGlobalScope(IsApproved::class)->where('id', $request['id'])->first();

           $payment->amount =  $request['amount'];

           $paymentMethod =  strtolower($payment->payment_method);


           if ( $paymentMethod === 'bank'){
               $payment->reference_code =  $request['reference_code'];
               $payment->slip_no =  $request['reference_code'];
           }
           if ($paymentMethod === 'cheque'){
               $payment->reference_code =  $request['reference_code'];
               $payment->cheque_no =  $request['reference_code'];
           }
           if ($paymentMethod === 'cash'){
               $payment->reference_code =  $request['reference_code'];
           }
           if ($paymentMethod === 'mpesa'){
               $payment->reference_code =  $request['reference_code'];
           }

           $account = $payment->payables->first();


           $account->amount = $payment->amount;

           $account->save();





           $payment->save();



           DB::commit();


           return response()
               ->json([
                   'message'  => "The payment has been updated"
               ]);


       }catch (\Exception $exception) {


           DB::rollBack();


           return response()
               ->json([
                   'message'  => "The payment could be updated, please try again"
               ], 500);


       }






    }
    public function index(Request $request)
    {

        $payment = SalePayment::withoutGlobalScope(IsApproved::class)->where('id', $request['id'])->first();

        $payment->is_approved = true;


        $payment->save();

        return response()
            ->json([
                'message'  => "The payment has been confirmed"
            ]);

    }
    public function reject(Request $request)
    {

        $payment = SalePayment::withoutGlobalScope(IsApproved::class)->where('id', $request['id'])->first();

        $payment->sale->accounts->count() ? $payment->sale->accounts->first()->delete() : null;




        if ($payment->sale->invoices)
        {

        }

        $payment->delete();

        return response()
            ->json([
                'message'  => "The payment has been rejected"
            ]);

    }
}
