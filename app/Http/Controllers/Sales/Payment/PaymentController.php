<?php

namespace App\Http\Controllers\Sales\Payment;

use App\Customer\Customer;
use App\Sale\SalePayment;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;

class PaymentController extends Controller
{
    public function index(Request $request)
    {
        $customers = auth()->user()->customers();


        if (isset($request['customer_id']))
        {
            $startDate = isset($request['start_date']) ? Carbon::parse($request['start_date']) : now()->startOfYear()->startOfDay();
            $endDate = isset($request['end_date']) ? Carbon::parse($request['end_date']) : now()->endOfDay();


            if (isset($request['customer_id'])){
                $customer = Customer::where('id', $request['customer_id'])->first();

                if (! $customers) {
                    return back();

                }

                $payments = $customer->salePayments->map(function ($q) {
                    return $q->paymentToArray();
                });
            }
            else{
                $payments = SalePayment::whereBetween('created_at', [$startDate , $endDate])->get()->map(function ($q) {
                    return $q->paymentToArray();
                });

            }



            return view('sales.payment.index')
                ->with([
                    'payments'  => collect($payments)->groupBy('projects')->toArray(),
                    'customers' => $customers
                ]);
        }
        return view('sales.payment.index')
            ->with([
                'payments'  => collect()->toArray(),
                'customers' => $customers
            ]);

    }

    public function delete(SalePayment $payment)
    {
        $payment->delete();

        Session::flash('success', "Successfully deleted the payment");
        return back();
    }
    public function edit(SalePayment $payment , Customer $customer)
    {
        return view('sales.payment.edit')
            ->with([
                'payment'  => $payment,
                'customer'  => $customer
            ]);

    }

    public function update(Request $request , SalePayment $payment , Customer $customer)
    {

        $request['is_conversion']  =  true;


        $payment->amount = ($request['amount']);



        $payment->payment_method = $request['payment_method'];

        $paymentMethod =  strtolower($request['payment_method']);


        if ( $paymentMethod === 'bank'){
            $payment->reference_code =  $request['reference_code'];
            $payment->slip_no =  $request['reference_code'];
        }
        if ($paymentMethod === 'cheque'){
            $payment->reference_code =  $request['reference_code'];
            $payment->cheque_no =  $request['reference_code'];
        }
        if ($paymentMethod === 'cash'){
            $payment->reference_code =  $request['reference_code'];
        }
        if ($paymentMethod === 'mpesa'){
            $payment->reference_code =  $request['reference_code'];
        }


        $account = $payment->payables->first();
        if ($account)
        {

            $account->amount = $payment->amount;


            $account->save();
        }

        $payment->deposit_date = Carbon::parse($request['deposit_date']);


        $payment->save();


        Session::flash('success', "Successfully updated the payment");
        return redirect()
            ->route('sale_payments' ,['customer_id' => $customer->id]);



    }
}
