<?php

namespace App\Http\Controllers\Sales\Appointment;

use App\DB\Sale\Appointment;
use App\User;
use Carbon\Carbon;
use Codex\Contracts\ServiceCaller;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class TodaysAppointmentReportsController extends Controller implements ServiceCaller
{
    public function index(Request $request)
    {

        $startDate = isset($request['start_date']) ? Carbon::parse($request['start_date']) : Carbon::now()->startOfWeek();
        $endDate = isset($request['end_date']) ? Carbon::parse($request['end_date']) : Carbon::now()->endOfDay();

        $appoints = Appointment::whereBetween('date_time',[
            $startDate,
            $endDate,
        ]);


        /******CHECK OPTIONS**********/
        if (isset($request['options']) && ($request['options'] != "false"))
        {

            $isVisited = ($request['options'] === 'visited') ? true : false ;


            $appoints = $appoints->where('has_visited', $isVisited);
        }


        /******CHECK AGENT**********/

        if (isset($request['agent_id']) && ($request['agent_id'] != "false"))
        {

            $appoints = $appoints->where('agent_id', $request['agent_id']);
        }

        $appoints = $appoints->get()
        ->map(function ($query) {

            return $query->appointmentToArray();
        });



        return view('sales.appointment.reports.today_appointment')->with([
            'appointments' => $appoints,
            'users'        => User::where('is_active', true)->get(),
        ]);
    }


    public function absentAppointment(Request $request)
    {


        $appoints = Appointment::where('date_time', '<' , Carbon::now()->endOfDay())
            ->where('has_visited', false)->get()
        ->map(function ($query) {

            return $query->appointmentToArray();
        });





        return view('sales.appointment.reports.absent_appointment')->with([
            'appointments' => $appoints
        ]);
    }
    public function updateAppointment(Request $request)
    {



        $appointment = Appointment::where('id', $request['id'])->first();

        $appointment->is_interested = isset($request['is_interested']) ? true :  false;
        $appointment->has_visited = isset($request['has_visited']) ? true :  false;
        $appointment->note = isset($request['comments']) ? $request['comments'] :  $appointment->note;
        $appointment->date_time = isset($request['date_time']) ? Carbon::parse($request['date_time']) :  $appointment->date_time;

        $appointment->save();


        return redirect()->back()->with(
            'success', "successfully updated the appointment details"
        );


    }
    public function successResponse($message)
    {
        // TODO: Implement successResponse() method.
    }
    public function failureResponse($message)
    {
        return redirect()->back()->with(
            'error', $message
        );
    }
}
