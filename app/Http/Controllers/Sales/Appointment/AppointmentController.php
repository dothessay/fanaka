<?php

namespace App\Http\Controllers\Sales\Appointment;

use App\DB\Product\Product;
use App\DB\Sale\Appointment;
use Carbon\Carbon;
use Codex\Contracts\ServiceCaller;
use Codex\Services\Sales\MakeAppointment;
use foo\bar;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;

class AppointmentController extends Controller implements ServiceCaller
{
    public function index(Request $request)
    {
        $projects = Product::all();


        $projectArray  =  [];

        foreach ($projects as $project) {

            if ($project->availablePlots() > 0){

                $projectArray[] = $project;
            }
        }

        $where = [];

        if (! auth()->user()->can('access-module-component-functionality','sale_appointment_view-all'))
        {
            $where['agent_id'] = auth()->id();
        }

        $startDate = isset($request['start_date']) ? $request['start_date'] : Carbon::now()->startOfDay();


        $appointments = Appointment::where($where)
            ->where('date_time', '>=', Carbon::parse($startDate))
            ->get()->map(function ($q) {
            return $q->appointmentToArray();
        });

        return view('sales.appointment.index')->with([
            'projects'      => collect($projectArray),
            'appointments'  => $appointments
        ]);


    }


    public function store(Request $request)
    {


        $response = $this->validateForm($request ,[
            'full_name'  => 'required',
            'phone_number'  => 'required',
            'date'        => 'required',
            'project_id'   => 'required'
        ] , [
            'full_name.required'   => "Client name is required",
            'project_id.required'  => 'Please select projects interested',
            'date.required'        => 'Please select appointment date and time'
        ]);

        if (! $response instanceof $request)
        {
            return $response;
        }

        return (new MakeAppointment($this))->runWithDBTransaction($request->all());

    }

    public function successResponse($message)
    {
        Session::flash('success',$message);

        return back();

        return response()->json([
            'message' => $message
        ]);
    }

    public function failureResponse($message)
    {


        Session::flash('error',$message);

        return back();

        return response()->json([
            'message' => $message
        ] , 500);
    }
}
