<?php

namespace App\Http\Controllers\Sales;

use App\Customer\Customer;
use App\Sale\Sale;
use Codex\Classes\PdfOutput\DataReport;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AcknowledgementController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($saleId, Request $request)
    {
        $acknowledgement = fanakaConfig('acknowledgement');

        $sale = Sale::find($saleId);

        $customer = Customer::find($request['customerId']);

        $acknowledgement = str_replace('{purchaser}',$customer->fullName() ,$acknowledgement);
        $acknowledgement = str_replace('{id}',$customer->getCustomerId() ,$acknowledgement);
        $acknowledgement = str_replace('{phone}',$customer->getPhoneNumber() ,$acknowledgement);
        $acknowledgement = str_replace('{ActiveUser}', auth()->user()->fullName() ,$acknowledgement);
        $acknowledgement = str_replace('{date}', now()->format('d-m-Y') ,$acknowledgement);
        $acknowledgement = str_replace('{project}', $sale->getPropertyDetails() ,$acknowledgement);


        return view('sales.acknowledgement.index')->with(['acknowledgement' => $acknowledgement ,'sale' => $sale , 'customer' => $customer]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function print($saleId , Request $request)
    {
        $acknowledgement = $request['content'];

        $sale = Sale::find($saleId);

        foreach ($sale->saleItems as $saleItem) {

            if ($saleItem->plot->product->getMeta('map', false))
            {
                $acknowledgement .= '<hr/><img src="'.$saleItem->plot->product->getMeta('map').'" width="640px" height="460px"> ';
            }

        }


        (new DataReport())->outPut($acknowledgement ,"Acknowledgement Note",\Mpdf\Output\Destination::INLINE, false);

        return redirect()
            ->route('sale_manage',['customer_id' => $request['customer_id']])
            ->send();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
