<?php

namespace App\Http\Controllers\Sales;

use App\DB\Product\Product;
use Codex\Classes\Handlers\Calculator\CalculatorHandlerFacade;
use Codex\Classes\PdfOutput\DataReport;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CalculatorController extends Controller
{
    public function index(Request $request)
    {
         // principal

        $principal = $request->has('principal') ? $request['principal'] : 0;

        // deposit
        $deposit = $request->has('deposit') ? $request['deposit'] : 0;

        // months

        $months = $request->has('months') ? $request['months'] : 1;

        $payments = CalculatorHandlerFacade::getRate($months)->calculate($principal , $deposit);

        $projects =  Product::all();

        $projectArray  =  collect();

        foreach ($projects as $project) {

            if ($project->availablePlots() > 0){

               $projectArray->push($project);
            }
        }

        if ($request->has('pdf'))
        {
            $html =  view('sales.calculator._data')->with(['payments'  => $payments])->render();
            (new DataReport())->outPut($html,'Calculator',\Mpdf\Output\Destination::INLINE);
        }

        return view(' sales.calculator.browse')->with(['payments'  => $payments, 'projects' => $projectArray]);

    }
}
