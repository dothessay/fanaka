<?php

namespace App\Http\Controllers;

use App\DB\Notification\Issue;
use App\DB\Notification\Notification;
use Codex\Contracts\ServiceCaller;
use Codex\Services\Communication\StoreIssueService;
use Illuminate\Http\Request;

class NotificationController extends Controller implements ServiceCaller
{
    public function index($id)
    {

        $notification = Notification::where('id', $id)->first();

        $notification->read = true;
        $notification->save();

        return view('notification_details')
            ->with([
                'notification' => $notification,
                'notifications' => Notification::where('user_id', auth()->id())->orderBy('read', 'ASC')->get(),
            ]);


    }
    public function issueDetails($id)
    {

        $notification = Issue::where('id', $id)->first();

        $notification->is_read = true;
        $notification->save();

        return view('communication.issue_details')
            ->with([
                'notification' => $notification,
                'notifications' => Issue::orderBy('is_read', 'ASC')->get(),
            ]);


    }
    public function issueDetailDelete()
    {


        $notifications = Issue::where('is_read', true)->get();


        foreach ($notifications as $notification) {

            $notification->delete();

        }


        return redirect()->route('dashboard');


    }

    public function addIssues()
    {
        return view('communication.add-issue');

    }

    public function storeIssues(Request $request)
    {

        $response = $this->validateAjaxForm($request ,[
            'type' => 'required',
            'issue'  => 'required'
        ]);

        if (! $response instanceof  $request)
        {
            return $response;
        }

        return (new StoreIssueService($this))->runWithDBTransaction($request->all());

    }

    public function successResponse($message)
    {
        return response()
            ->json([
                'message' => $message
            ]);

    }
    public function failureResponse($message)
    {
        return response()
            ->json([
                'message' => $message
            ]);

    }
}
