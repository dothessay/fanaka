<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\User;
use Codex\Classes\Handlers\FanakaLog;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers , FanakaLog;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }


    protected function authenticated(Request $request, $user)
    {
        if (! $user->is_active)
        {

            return $this->sendFailedLoginResponse($request);

            auth()->logout();
        }

        $now = now();

        $user = auth()->user();

        $user->checked_in_at = $now;
        $user->checked_out_at = null;

        $user->save();


        $logger = [];

        $logger['class'] =   get_class(auth()->user());
        $logger['action']   = serialize(['checked_in_at' => $now]);
        $this->log($logger);
        
    }

    public function logout()
    {

        if (session()->has('user_id'))
        {
            auth()->loginUsingId(session()->get('user_id'));

            session()->remove('user_id');
            return back();

        }
        $now = now();
        $logger['class'] =  get_class(auth()->user());
        $logger['action']   = serialize(['checked_out_at' => $now]);
        $this->log($logger);



        $user = auth()->user();

        $user->checked_in_at = null;
        $user->checked_out_at = $now;

        $user->save();



        auth()->logout();



        return redirect(url('/login'));
        
    }


}
