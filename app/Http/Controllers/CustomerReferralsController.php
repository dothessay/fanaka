<?php

namespace App\Http\Controllers;

use App\DB\Customer\Referral;
use Codex\Contracts\ServiceCaller;
use Codex\Services\Customers\ListCustomerService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class CustomerReferralsController extends Controller implements ServiceCaller
{

    public function index()
    {
        $customerIds = (new ListCustomerService($this))->list([])->pluck('id')->toArray();


        $referrals = Referral::whereIn('customer_id', $customerIds)
            ->get()
            ->map(function ($q) {
                return $q->referralToArray();
            });




        return view('customers.referrals.index')
            ->with([
                'referrals' => $referrals
            ]);

    }


    public function contact(Referral $referral)
    {

        $referral->is_contacted = true;

        $referral->save();


        Session::flash('success', "Marked as contacted");

        return back();


    }

    public function successResponse($message)
    {
        // TODO: Implement successResponse() method.
    }
}
