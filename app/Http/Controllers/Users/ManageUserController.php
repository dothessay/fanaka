<?php

namespace App\Http\Controllers\Users;

use App\DB\Business\DepartmentAdmin;
use App\DB\Role\Role;
use App\Sale\Sale;
use App\User;
use Codex\Classes\Repository\UserRepository;
use Codex\Contracts\ServiceCaller;
use Codex\Services\Users\ListUsersService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ManageUserController extends Controller implements ServiceCaller
{

    public function index(Request $request)
    {
        if (!auth()->user()->can('access-module-component-functionality', 'sale_manage_show-all-sales')) {



        }

        return (new ListUsersService($this))->run($request->all());

    }

    public function agents(Request $request)
    {
        $request->request->add(['is_active' => true]);
        return response()
            ->json([
                'data' => (new UserRepository())
                ->all()
                ->map->userToArray()
            ]);
    }



    public function details(User $user)
    {

        $members = collect();


        if(auth()->user()->can('access-module-component-functionality', 'users_manage_add-other-sales'))
        {
            $department = DepartmentAdmin::where('user_id', auth()->id()  )->first();

            if ($department)
            {
                $members = $department->user->role->users;
            }
        }

        return view('users.details.index')->with([
            'user' => $user,
            'members' => $members
        ]);

    }
    public function successResponse($message)
    {
        return view('users.manage_user')->with([
            'users' => $message,
            'roles' => Role::all(),
        ]);

    }


    public function impersonate(User $user)
    {
         session('user_id', $user->id);

         auth()->login($user);

         return back();

    }

    public function leaveImpersonate()
    {
        $id = \request()->has('user_id') ? \request('user_id') : session('user_id');

        auth()->logout();

        auth()->loginUsingId($id);

        session()->forget('user_id');

         return back();

    }

}
