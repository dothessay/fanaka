<?php

namespace App\Http\Controllers\Users;

use App\DB\User\DeactivatedAgentCustomer;
use App\User;
use Codex\Contracts\ServiceCaller;
use Codex\Services\Users\StoreUserService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;
use Illuminate\Validation\Rule;
use Codex\Services\Users\UpdateUserService;
use Codex\Services\Users\DeleteService;

class StoreUserController extends Controller implements ServiceCaller
{
    public function index(Request $request){

        $response = $this->validateForm($request, [
            "phone_number" => [
                'required',
                Rule::unique('users')
            ],
            "first_name" => [
                'required',
            ],
            "last_name" => [
                'required',
            ],
            "email" => [
                'required',
                Rule::unique('users')
            ],
            "password" => 'required|min:6',

            "role_id" => [
                'required',

            ],
        ]);


        if (! $response instanceof  $request)
        {
            return $response;
        }

        return (new StoreUserService($this))->run($request->all());
    }

    public function successResponse($message)
    {
        Session::flash('success',$message);
       return back();
    }
    
    public function update(Request $request)
    {
        if(!$request->password == '')
        {    
            $response = $this->validateForm($request, [
                "phone_number" => [
                    'required',
                    Rule::unique('users')->ignore($request->id)
                ],
                "first_name" => [
                    'required',
                ],
                "last_name" => [
                    'required',
                ],
                "email" => [
                    'required',
                    Rule::unique('users')->ignore($request->id)
                ],
                "password" => 'required|min:6',

                "role_id" => [
                    'required',

                ],
            ]);
        }
        else 
        {
            $response = $this->validateForm($request, [
                "phone_number" => [
                    'required',
                    Rule::unique('users')->ignore($request->id)
                ],
                "first_name" => [
                    'required',
                ],
                "last_name" => [
                    'required',
                ],
                "email" => [
                    'required',
                    Rule::unique('users')->ignore($request->id)
                ],
                
                "role_id" => [
                    'required',

                ],
            ]);
        }

        if (! $response instanceof  $request)
        {
            return $response;
        }

        return (new UpdateUserService($this))->run($request->all());
    }
    
    public function delete(Request $request)
    {
        return (new DeleteService($this))->run($request->all());

    }
    public function activate(User $user)
    {
        $user->is_active = true;

        $user->save();

        DeactivatedAgentCustomer::where('user_id', $user->id)->delete();


        Session::flash('success', "User is active again");

        return back();

    }
}
