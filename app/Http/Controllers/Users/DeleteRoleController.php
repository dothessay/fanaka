<?php

namespace App\Http\Controllers\Users;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Codex\Contracts\ServiceCaller;
use Codex\Services\Users\DeleteRoleService;

class DeleteRoleController extends Controller implements ServiceCaller
{
    public function index(Request $request)
    {
        return (new DeleteRoleService($this))->run($request->all());
    }    

    public function successResponse($message) 
    {
        return back()->with('success', $message);
    }
}
