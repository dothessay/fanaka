<?php

namespace App\Http\Controllers\Users\Department;

use App\DB\Business\DepartmentAdmin;
use App\DB\Role\Role;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;

class DepartmentController extends Controller
{
    public function index(Request $request)
    {
        $departments = DepartmentAdmin::all();

        return view('users.departments.index')
            ->with([
                'departments'  => $departments
            ]);
    }

    public function create()
    {
        $users = User::active()->get();

        return view('users.departments.create')
            ->with([
                'users' => $users,
                'roles' => Role::all()
            ]);
    }

    public function store(Request $request)
    {
        DepartmentAdmin::create([
            'name'  => $request->name,
            'user_id'  => $request->user_id,
            'role_id' => $request->role_id
        ]);

        Session::flash('success','Successfully created a new admin');

        return redirect()->route('users_department-head');
    }


    public function show($id)
    {
        $users = User::active()->get();

        return view('users.departments.create')
            ->with([
                'users' => $users,
                'roles' => Role::all(),
                'department'  => DepartmentAdmin::find($id)
            ]);
    }

    public function update($id , Request $request)
    {
        DepartmentAdmin::updateOrCreate([
            'id' => $id
            ],
            [
                'name'  => $request->name,
                'user_id'  => $request->user_id,
                'role_id' => $request->role_id
        ]);

        Session::flash('success','Successfully updated the department admin');

        return redirect()->route('users_department-head');
    }
}
