<?php

namespace App\Http\Controllers\Users;

use App\DB\Role\Role;
use App\User;
use Codex\Contracts\ServiceCaller;
use Codex\Services\Users\ListRoleService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ManageRoleController extends Controller implements ServiceCaller
{
    public function index(Request $request)
    {
        return (new ListRoleService($this))->run($request->all());

    }
    public function successResponse($message)
    {
        return view('users.manage_role')
            ->with([
                'roles' => $message
            ]);


    }

    public function details(Role $role)
    {
        $roles = Role::all();

        return view('users.roles.details')
            ->with([
                'role' => $role,
                'roles'  => $roles,
                'users' => User::active()->get(),
            ]);

    }
}
