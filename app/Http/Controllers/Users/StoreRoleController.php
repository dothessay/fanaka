<?php

namespace App\Http\Controllers\Users;

use Codex\Contracts\ServiceCaller;
use Codex\Services\Users\StoreRoleService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;
use Illuminate\Validation\Rule;

class StoreRoleController extends Controller implements ServiceCaller
{
    public function index(Request $request)
    {
        $response = $this->validateForm($request , [

            'name' => [
                'required',
                Rule::unique('roles')
            ]
        ]);

        if (! $response instanceof  $request){
            return $response;
        }

        return (new StoreRoleService($this))->run($request->all());

    }
    public function successResponse($message)
    {
       Session::flash('success', $message);

       return back();
    }
}
