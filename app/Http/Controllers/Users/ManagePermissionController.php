<?php

namespace App\Http\Controllers\Users;

use App\DB\Role\Component;
use App\DB\Role\Functionality;
use App\DB\Role\Module;
use App\User;
use Codex\Contracts\ServiceCaller;
use Codex\Permission\Permissions;
use Codex\Services\Users\ListRoleService;
use Codex\Services\Users\ListUsersService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ManagePermissionController extends Controller implements ServiceCaller
{

    public function index()
    {

        return view('users.permissions')
            ->with([
                'users' => (new ListUsersService($this))->list([]),
                'roles' => (new ListRoleService($this))->list([])
            ]);

    }

    public function configureRole($id)
    {

        return view('users.configure_role_modules')
            ->with([
                'role' => (new ListRoleService($this))->list(['id' => $id])->first(),
                'modules' => Module::all(),
            ]);

    }


    /**
     * @param $id
     * @return $this
     * configure user permissipons
     */
    public function configureUser($id)
    {

        return view('users.permission.configure_user_modules')
            ->with([
                'user' => (new ListUsersService($this))->list(['id' => $id])->first(),
                'modules' => Module::all(),
            ]);

    }

    /**
     * @param Request $request
     * @return string
     * configure user roles
     */
    public function configureRoleModule(Request $request)
    {
        $role = (new ListRoleService($this))->list(['id' => $request['role_id']])->first();

        $module = Module::where('id', $request['role_permission_id'])->first();
        if (!$role->canAccessModule($request['module'])) {

            $role->addModulePermission($module);

            return "created";

        }
        $role->deleteModulePermission($module);

        return "denied";
    }


    /**
     * @param Request $request
     * @return string
     */
    public function configureUserModule(Request $request)
    {
        $user = (new ListUsersService($this))->list(['id' => $request['role_id']])->first();

        $module = Module::where('id', $request['user_permission_id'])->first();

        $hasAccess = false;


        if (Permissions::checkAccessToModule($user, $module->name)) {


            $user->deleteModulePermission($module);

            return "denied";
        }
        $user->addModulePermission($module);

        return "created";


        /* if (! $user->canAccessModule($request['module']))
         {




         }*/
    }

    public function configureRoleComponent($id, Request $request)
    {
        return view('users.configure_role_components')
            ->with([
                'role' => (new ListRoleService($this))->list(['id' => $id])->first(),
                'components' => Component::where('module_id', $request['module'])->get(),
                'module' => Module::where('id', $request['module'])->first(),

            ]);
    }

    /**
     * @param $id
     * @param Request $request
     * @return $this
     * configure user permisson components
     */
    public function configureUserComponent($id, Request $request)
    {
        return view('users.permission.configure_user_components')
            ->with([
                'user' => (new ListUsersService($this))->list(['id' => $id])->first(),
                'components' => Component::where('module_id', $request['module'])->get(),
                'module' => Module::where('id', $request['module'])->first(),

            ]);
    }

    public function storeRoleComponent(Request $request)
    {
        $role = (new ListRoleService($this))->list(['id' => $request['role_id']])->first();

        $component = Component::where('id', $request['role_permission_id'])->first();
        if (!$role->canAccessComponent($request['component'])) {

            $role->addComponentsPermission($component);

            return "created";

        }
        $role->deleteComponentsPermission($component);

        return "denied";
    }

    /**
     * @param Request $request
     * @return string
     */
    public function storeUserComponent(Request $request)
    {
        $user = (new ListUsersService($this))->list(['id' => $request['user_id']])->first();

        $component = Component::where('id', $request['user_permission_id'])->first();


        if (Permissions::checkAccessToModuleComponent($user, $component->name)) {
            $user->deleteComponentsPermission($component);

            return "denied";
        }


        $user->addComponentsPermission($component, 'allow');

        return "created";


    }

    public function configureRoleFunctionality($id, Request $request)
    {
        $functionalities = Functionality::where('component_id', $request['component'])->get();

        return view('users.configure_role_functionalities', compact('functionalities'))
            ->with([
                'role' => (new ListRoleService($this))->list(['id' => $id])->first(),
                'component' => Component::where('id', $request['component'])->first(),
                'functionalities ' => Functionality::where('component_id', $request['component'])->get(),
            ]);
    }

    /**
     * @param $id
     * @param Request $request
     * @return $this
     * user special permissions
     *
     */
    public function configureUserFunctionality($id, Request $request)
    {
        $functionalities = Functionality::where('component_id', $request['component'])->get();

        return view('users.permission.configure_user_functionalities', compact('functionalities'))
            ->with([
                'user' => (new ListUsersService($this))->list(['id' => $id])->first(),
                'component' => Component::where('id', $request['component'])->first(),
                'functionalities ' => Functionality::where('component_id', $request['component'])->get(),
            ]);
    }


    public function storeRoleFunctionality(Request $request)
    {
        $role = (new ListRoleService($this))->list(['id' => $request['role_id']])->first();

        $functionlity = Functionality::where('id', $request['role_permission_id'])->first();
        if (!$role->canAccessFunctionalities($request['functionality'])) {

            $role->addFunctionalitiesPermission($functionlity);

            return "created";

        }
        $role->deleteFunctionalitiesPermission($functionlity);

        return "denied";
    }


    /**
     * @param Request $request
     * @return string
     *
     * user special permissions
     */
    public function storeUserFunctionality(Request $request)
    {
        $user = (new ListUsersService($this))->list(['id' => $request['user_id']])->first();

        $functionlity = Functionality::where('id', $request['user_permission_id'])->first();


        if (Permissions::checkAccessToModuleComponentFunctionality($user, $functionlity->name)) {

            $user->deleteFunctionalitiesPermission($functionlity);

            return "denied";
        }


        $user->addFunctionalitiesPermission($functionlity);

        return "created";


    }

    public function successResponse($message)
    {
        // TODO: Implement successResponse() method.
    }
}
