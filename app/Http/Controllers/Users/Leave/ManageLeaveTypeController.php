<?php

namespace App\Http\Controllers\Users\Leave;

use App\DB\Leave\LeaveType;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;
use Illuminate\Validation\Rule;

class ManageLeaveTypeController extends Controller
{
    public function index()
    {

        return view('users.leave.types.create_edit')->with([
            'type' => new LeaveType()
        ]);
    }



    public function store(Request $request)
    {

        $validator = $this->validateForm($request , [
            'name'  => [
                'required',
                Rule::unique('leave_types')->ignore($request['id'])
            ]
        ]);

        if (! $validator instanceof  $request){

            return $validator;

        }
        $type = LeaveType::updateOrCreate([
            'id'  => $request['id']
        ], [
            'name' => $request['name'],
            'is_paid' => isset($request['is_paid']) ? true : false,
        ]);


        Session::flash('success', "{$type->name} saved success");

        return redirect()->route('users_leave');

    }

    public function find(LeaveType $type)
    {

        return view('users.leave.types.create_edit')->with(['type'  => $type]);
    }
    public function delete(LeaveType $type)
    {
        $name = $type->name;

        $type->forceDelete();


        Session::flash('success', "{$name} removed successfully");

        return back();
    }


}
