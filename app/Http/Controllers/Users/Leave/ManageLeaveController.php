<?php

namespace App\Http\Controllers\Users\Leave;

use App\DB\Leave\LeaveRequest;
use App\DB\Leave\LeaveType;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ManageLeaveController extends Controller
{
    public function index()
    {

        $types = LeaveType::all();

        $users = User::active()->get();

        return view('users.leave.index')->with([
            'types'  => $types,
            'users'  => $users
        ]);


    }
}
