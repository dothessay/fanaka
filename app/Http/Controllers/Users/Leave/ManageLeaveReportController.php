<?php

namespace App\Http\Controllers\Users\Leave;

use App\User;
use Codex\Contracts\ServiceCaller;
use Codex\Services\User\Leave\ListLeaveService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ManageLeaveReportController extends Controller implements ServiceCaller
{
    public function index(Request $request)
    {

        $data = [];


        switch ($request['status']){
            case "accepted":
                $data['status'] =  "accepted";
                break ;

                case "rejected":

                    $data['status'] =  "rejected";
                break ;

                case "pending":

                    $data['status'] =  "pending";
                break ;

                case "balance":

                    $employees = User::active()->get();
                    if (isset($request['user_id']) && ! is_null($request['user_id']))
                    {
                        $employees = User::where('id', $request['user_id'])->get();
                    }


                    $users = User::active()->get();

                    if (! auth()->user()->can('access-module-component-functionality','users_leave_view-requests')){

                        $users = User::where('id', auth()->id())->get();
                    }

                    return view('users.leave.request.report.index')
                        ->with([
                            'users'      => $users,
                            'employees'   => $employees,
                        ]);


                break ;
            default:

                $data['status'] =  "accepted";
        }

        $options = [];

        if (isset($request['user_id']) && ! is_null($request['user_id'])){
            $options['user_id']  = $request['user_id'];

        }

        if (isset($request['handling_user_id']) && ! is_null($request['handling_user_id'])){
            $data['handling_user_id']  = $request['handling_user_id'];

        }


        return (new ListLeaveService($this))->run($data, $options);




    }


    public function successResponse($leaves)
    {
        $users = User::active()->get();

        if (! auth()->user()->can('access-module-component-functionality','users_leave_view-requests')){

            $users = User::where('id', auth()->id())->get();
        }

        return view('users.leave.request.report.index')
            ->with([
                'leaves'  => $leaves,
                'users'   => $users
            ]);
    }
}
