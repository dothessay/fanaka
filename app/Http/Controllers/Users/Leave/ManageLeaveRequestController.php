<?php

namespace App\Http\Controllers\Users\Leave;

use App\DB\Leave\LeaveRequest;
use App\DB\Leave\LeaveType;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;

class ManageLeaveRequestController extends Controller
{
    public function index()
    {


        return view('users.leave.request.create')
            ->with([
                'leave'  => new LeaveRequest(),
                'types'  => LeaveType::all(),
                'users'  => User::active()->get(),
            ]);
    }


    public function store(Request $request)
    {

        $validator = $this->validateForm($request ,[
            'leave_type_id' => 'required',
            'last_day' => 'required',
            'return_at' => 'required',
            'days_off' => 'required',
            'comments' => 'required',
            'handling_user_id' => 'required',
            'duties' => 'required',
        ]);

        if (! $validator instanceof $request){
            return $validator;
        }


        $leave = LeaveRequest::updateOrCreate([
            'id' => $request['id'],
            'user_id' => isset($request['user_id']) ? $request['user_id'] : auth()->id()
        ],[
            'user_id'                => isset($request['user_id']) && ! is_null($request['user_id']) ? $request['user_id'] : auth()->id(),
            'leave_type_id'          => $request['leave_type_id'],
            'last_day'               => Carbon::parse($request['last_day']),
            'return_at'              => Carbon::parse($request['return_at']),
            'days_off'               => $request['days_off'],
            'comments'               => $request['comments'],
            'handling_user_id'       => $request['handling_user_id'],
            'duties'                 => $request['duties'],
            'is_approved'            => isset($request['is_approved']) ? true : false,
            'handle_clients_user_id' => isset($request['handle_clients_user_id'])  && ! is_null($request['handle_clients_user_id'])? $request['handle_clients_user_id'] : null,
            'approved_at'            => isset($request['is_approved']) ? now() : null,
        ]);


        Session::flash('success', "Leave request sent successfully");

        return redirect()->route('users_leave');



    }

    public function approve(LeaveRequest $leave)
    {
        $leave->is_approved = true;
        $leave->status = "accepted";
        $leave->approved_at = now();

        $leave->save();

        Session::flash('success', "Leave request sent successfully");

        return redirect()->route('users_leave');



    }
    public function find(LeaveRequest $leave)
    {
        return view('users.leave.request.create')
            ->with([
                'leave'  => $leave,
                'types'  => LeaveType::all(),
                'users'  => User::active()->get(),
            ]);


    }

    public function delete(LeaveRequest $leave)
    {

        $leave->forceDelete();

        Session::flash('success', "Request removed successfully");

        return redirect()->route('users_leave');
    }

    public function accept(LeaveRequest $leave)
    {

        $leave->handling_user_accepted =  true;

        $leave->save();

        Session::flash('success', "Request approved successfully");

        return redirect()->route('users_leave');
    }

    public function reject(LeaveRequest $leave)
    {
        return view('users.leave.request.reject')
            ->with([
                'leave'  => $leave,
            ]);
    }

    public function updateReject(LeaveRequest $leave , Request $request)
    {

        $leave->refusal_reason =  $request['reason'];
        $leave->status = "rejected";

        $leave->save();

        Session::flash('success', "Request rejected successfully");

        return redirect()->route('users_leave');
    }
}
