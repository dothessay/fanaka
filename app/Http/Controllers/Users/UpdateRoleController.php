<?php

namespace App\Http\Controllers\Users;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Codex\Contracts\ServiceCaller;
use Codex\Services\Users\UpdateRoleService;
use Illuminate\Validation\Rule;
use Session;

class UpdateRoleController extends Controller implements ServiceCaller
{
    public function index(Request $request)
    {
        $response = $this->validateForm($request , [

            'name' => [
                'required',
                Rule::unique('roles')->ignore($request['id'])
            ]
        ]);

        if (! $response instanceof  $request){
            return $response;
        }

        return (new UpdateRoleService($this))->run($request->all());
    }

    public function successResponse($message) 
    {
        Session::flash($message);
        
        return back()->with([
            'success' => $message
        ]);
    }

}
