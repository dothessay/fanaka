<?php

namespace App\Http\Controllers\PLots;

use App\Plot\Plot;
use App\Plots\PlotUpload;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;

class UploadPlotDocumentController extends Controller
{

    public function index($plotId)
    {
        $plot = Plot::where('id', $plotId)->first();


        return view('plots.upload')->with([
            'uploads'  => $plot->plotUploads->map(function ($upload) { return $upload->uploadToArray() ;}),
            'plot'  => $plot,
        ]);

    }

    public function store(Request $request)
    {
        $plot = Plot::where('id', $request['id'])->first();


        // $path = FileUpload::uploadFile($request , 'agreements','agreement','fanaka-real-estate-'.$sale->id);


            //$path = $request->file('agreement')->store('agreements');

            try {
                $docfile = $request->file('doc_file');
                $path = $docfile->store('plots');

                //$sale->agreement_url= $path;

                $plot->plotUploads()->create([
                    'upload_url' => $path,
                    'file_name' => $docfile->getClientOriginalName(),
                    'type' => $request['type'],
                    'user_id'  => auth()->id()
                ]);
                return redirect()->back()->with('success' ,  'successfully uploaded a '.$request['type'] );

            } catch (\Exception $exception) {
                return redirect()->back()->with('error' ,  'could not upload the files . ' . $exception->getMessage() );
            }



    }

    public function downloadUpload($id)
    {

        $plotUpload = PlotUpload::where('id', $id)->first();

        return response()->download(('storage/'.$plotUpload->upload_url));

    }
}
