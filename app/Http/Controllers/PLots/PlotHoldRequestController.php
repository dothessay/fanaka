<?php

namespace App\Http\Controllers\PLots;

use App\DB\Plot\PlotHoldingRequest;
use Codex\Classes\Repository\PlotHoldingRequestRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PlotHoldRequestController extends Controller
{
    private $requestRepository;

    public function __construct()
    {
        $this->requestRepository = new PlotHoldingRequestRepository();

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if (!auth()->user()->can('access-module-component-functionality', 'sale_manage_show-all-sales') && !  auth()->user()->departmentAdmin)
        {

            $request->request->add(['user_id' => $request->user()->id]);

        }


        $requests = $this->requestRepository->all();

        return  view('plots.canSell.requests')
            ->with([
                'requests' => $requests
            ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request , [
            'customer_name'  => 'required',
            'phone_number'   => 'required',
            'release_at'    => 'required',
            'plot_id'       => 'required|numeric',
        ], [
            'plot_id.numeric'  => 'Please select plot ',
            'plot_id.required' => 'Please select plot'
        ]);


        $this->requestRepository->store($request);
        session()->flash('successfully send plot holding request');
        return  redirect()
            ->route('plots_manage_request_index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(PlotHoldingRequest $holdingRequest)
    {
        return  view('plots.canSell.read_request')
            ->with([
                'request'  => $holdingRequest
            ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, PlotHoldingRequest $holdingRequest)
    {
        $this->requestRepository->update($request , $holdingRequest);

        session()->flash('successfully send plot holding request');
        return  redirect()
            ->route('plots_manage_request_index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
