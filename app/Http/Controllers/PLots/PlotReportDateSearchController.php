<?php

namespace App\Http\Controllers\PLots;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Codex\Services\Plots\PlotReportDateSearchService;
use Codex\Contracts\ServiceCaller;

class PlotReportDateSearchController extends Controller implements ServiceCaller
{
    public function searchDate(Request $request)
    {
        
        
        $response = $this->validateForm($request, [

            "start_date" => [
                'required',
            ],
            "end_date" => [
                'required',
            ],
            
        ]);
        
        if (! $response instanceof  $request)
        {
            return $response;
        }

        return (new PlotReportDateSearchService($this))->run($request->all());
    }

    public function successResponse($message)
    {
        return view('plots.plot_reports')
            ->with([
                'plots' => $message
            ]);
    }

}
