<?php

namespace App\Http\Controllers\PLots;

use App\DB\Plot\PlotLog;
use App\Plot\Plot;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

class DeletePlotController extends Controller
{
    public function index(Request $request)
    {
        $plot = Plot::where('id', $request['code'])->forceDelete();


        Session::flash('success','PLot deleted successfully');


        return back();


    }
    public function markAvailable(Request $request)
    {


        try{
            DB::beginTransaction();
            $plot = Plot::where('id', $request['id'])->first();


            //mark as available


            $plot->is_sold = false;
            $plot->is_reserved = false;

            $plot->can_sell = true;


            $plot->save();



            //soft delete the sale


            $saleItem = $plot->saleItem;

            $logData = [
                'plot_id'  => $plot->id
            ];


            if($saleItem)
            {
                $sale = $saleItem->sale;

                $logData['customer_id'] = serialize($sale->customers->pluck('id')->toArray());

                $logData['agent_id']  = $sale->soldBy->id;

                $logData['reservation_type'] ="installment / Sale";

                $logData['amount_paid'] = serialize($sale->salePayments->pluck('amount')->toArray());
                $logData['payment_method'] = serialize($sale->salePayments->pluck('payment_method')->toArray());
                $logData['reference_no'] = serialize($sale->salePayments->pluck('reference_code')->toArray());


                $sale->delete();
            }


            $offerItem = $plot->offerItem;


            if ($offerItem) {


                $offer = $offerItem->offer;

                $logData['customer_id'] = serialize($offer->customers->pluck('id')->toArray());

                $logData['agent_id']  = $offer->agent->id;
                $logData['reservation_type']  = "reservation";

                $logData['amount_paid'] = serialize($offer->offerPayments->pluck('amount')->toArray());
                $logData['payment_method'] = serialize($offer->offerPayments->pluck('payment_method')->toArray());
                $logData['reference_no'] = serialize($offer->offerPayments->pluck('reference_code')->toArray());

                $offer->delete();

            }

            PlotLog::create([
                'plot_id'           => $logData['plot_id'],
                'customer_id'       => $logData['customer_id'],
                'agent_id'          => $logData['agent_id'],
                'amount_paid'       => $logData['amount_paid'],
                'user_id'           => auth()->id(),
                'reservation_type'  => $logData['reservation_type'],
                'payment_method'    => $logData['payment_method'],
                'reference_no'      => $logData['reference_no']
            ]);





            DB::commit();

            Session::flash('success', 'successfully marked plot as available');

            return redirect(route('offer_generate'));


        }catch (\Exception $exception)
        {
            DB::rollBack();

            Session::flash('error', $exception->getMessage());

            return back();

        }


    }public function release(Request $request)
    {


        try{
            DB::beginTransaction();
            $plot = Plot::where('id', $request['id'])->first();


            //mark as available


            $plot->is_sold = false;
            $plot->is_reserved = false;

            $plot->can_sell = true;


            $plot->save();



            //soft delete the sale


            $saleItem = $plot->saleItem;

            $logData = [
                'plot_id'  => $plot->id
            ];


            if($saleItem)
            {
                $sale = $saleItem->sale;

                $logData['customer_id'] = serialize($sale->customers->pluck('id')->toArray());

                $logData['agent_id']  = $sale->soldBy->id;

                $logData['reservation_type'] ="installment / Sale";

                $logData['amount_paid'] = serialize($sale->salePayments->pluck('amount')->toArray());
                $logData['payment_method'] = serialize($sale->salePayments->pluck('payment_method')->toArray());
                $logData['reference_no'] = serialize($sale->salePayments->pluck('reference_code')->toArray());


                $sale->delete();
            }


            $offerItem = $plot->offerItem;


            if ($offerItem) {


                $offer = $offerItem->offer;

                $logData['customer_id'] = serialize($offer->customers->pluck('id')->toArray());

                $logData['agent_id']  = $offer->agent->id;
                $logData['reservation_type']  = "reservation";

                $logData['amount_paid'] = serialize($offer->offerPayments->pluck('amount')->toArray());
                $logData['payment_method'] = serialize($offer->offerPayments->pluck('payment_method')->toArray());
                $logData['reference_no'] = serialize($offer->offerPayments->pluck('reference_code')->toArray());

                $offer->delete();

            }

            PlotLog::create([
                'plot_id'           => $logData['plot_id'],
                'customer_id'       => $logData['customer_id'],
                'agent_id'          => $logData['agent_id'],
                'amount_paid'       => $logData['amount_paid'],
                'user_id'           => auth()->id(),
                'reservation_type'  => $logData['reservation_type'],
                'payment_method'    => $logData['payment_method'],
                'reference_no'      => $logData['reference_no']
            ]);





            DB::commit();

            Session::flash('success', 'successfully marked plot as available');

            return response()
                ->json([
                    'message' => 'successfully marked plot as available'
                ], 200);


        }catch (\Exception $exception)
        {
            DB::rollBack();

            Session::flash('error', $exception->getMessage());

            return response()
                ->json([
                    'message' => 'failed '. $exception->getMessage()
                ], 500);

        }


    }
}
