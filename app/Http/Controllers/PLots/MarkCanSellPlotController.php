<?php

namespace App\Http\Controllers\PLots;

use App\DB\Product\Product;
use App\Jobs\Plot\HoldPlotJob;
use App\Jobs\Plot\UnHoldPlotJob;
use Codex\Contracts\ServiceCaller;
use Codex\Services\Plot\HoldPlotService;
use Codex\Services\Plot\MarkCanSell;
use Codex\Services\Plot\UnHoldPlot;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;

class MarkCanSellPlotController extends Controller implements ServiceCaller
{
    public function index()
    {


        $projectsChecks = Product::all();


        $projects = [];

        $helds = [];


        foreach ($projectsChecks as $projectsCheck) {


            foreach ($projectsCheck->plots->where('can_sell', true) as $plot)
            {


                if ($plot->available())
                {
                    $projects[] = $projectsCheck;
                }
            }

            foreach ($projectsCheck->plots->where('can_sell', false)->where('is_reserved', false) as $plot) {



                        $helds[] = $projectsCheck;



            }


        }

        $projects = collect($projects)->unique();
        $helds = collect($helds)->unique();



        return view('plots.canSell.index')
            ->with([
                'projects' => $projects,
                'holds'   =>  $helds
            ]);


    }

    public function hold(Request $request)
    {

        (new HoldPlotService($this))->runWithDBTransaction($request->all());

        //dispatch(new HoldPlotJob($this , $request->all()));

        Session::flash("success","successfully held the plots");

        return back();

       // ;

    }

    public function unHold(Request $request)
    {

        (new UnHoldPlot($this))->runWithDBTransaction($request->all());
        //dispatch(new UnHoldPlotJob($this , $request->all()));

        Session::flash("success","successfully held the plots");


        return back();

    }

    public function successResponse($message)
    {
        return response()
            ->json([
                'message'  => $message
            ]);
    }

    public function failureResponse($message)
    {
        return response()
            ->json([
                'message'  => $message
            ], 500);
    }
}
