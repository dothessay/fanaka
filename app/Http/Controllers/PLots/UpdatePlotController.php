<?php

namespace App\Http\Controllers\PLots;

use Codex\Contracts\ServiceCaller;
use Codex\Services\Plots\UpdatePlotService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;
use Illuminate\Validation\Rule;

class UpdatePlotController extends Controller implements ServiceCaller
{
    public function index(Request $request)
    {
        $response = $this->validateForm($request ,[
            "product_code" => [
                'required',
            ],

            "price" => [
                'required',
            ],
            "size_id" => [
                'required',
            ],
            "plot_no" => [
                'required',
            ],
            "title_no" => [
                'nullable',
                Rule::unique('plots')->ignore($request['id'])
            ],
        ]);

        if (! $response instanceof  $request)
        {
            return $response;
        }

        return (new UpdatePlotService($this))->run($request->all());

    }

    public function successResponse($message)
    {
        Session::flash($message);
        return back()->with([
            'success' => $message
        ]);
    }

}
