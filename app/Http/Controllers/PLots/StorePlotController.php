<?php

namespace App\Http\Controllers\PLots;

use Codex\Contracts\ServiceCaller;
use Codex\Services\Plots\StorePlotService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;
use Illuminate\Validation\Rule;

class StorePlotController extends Controller implements ServiceCaller
{
    public function index(Request $request)
    {

        $response = $this->validateForm($request, [
            "product_code" => [
                'required',
            ],

            "price" => [
                'required',
            ],
            "size_id" => [
                'required',
            ],
            "plot_no" => [
                'required',
            ],
            "title_no" => [
                Rule::unique('plots')->where('title_no', '!=',null)
            ],
        ]);


        if (! $response instanceof  $request)
        {
            return $response;
        }

        return (new StorePlotService($this))->run($request->all());

    }

    public function successResponse($message)
    {
        Session::flash($message);
        return back();
    }
}
