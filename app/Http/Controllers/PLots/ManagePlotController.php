<?php

namespace App\Http\Controllers\PLots;

use App\DB\Product\Product;
use App\Plot\Plot;
use App\Plots\PlotUpload;
use App\Sale\SalePayment;
use App\Setting\PlotSizeSetting;
use Carbon\Carbon;
use Codex\Classes\Repository\PlotRepository;
use Codex\Contracts\ServiceCaller;
use Codex\Services\Plots\ListPlotsService;
use Codex\Services\Products\ListProductsService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;

class ManagePlotController extends Controller implements ServiceCaller
{
    public function index(Request $request)
    {

        return (new ListPlotsService($this))->run($request->all());

    }
    public function getTitles(Request $request)
    {

        $projects = Product::all()->map(function ($project) {

            return [
                'project_code' => $project->code,
                'name' => $project->name,
                'plots' => $project->plotsToArray()
            ];
        });

        $uploads = collect();

        if (isset($request->plot_id))
        {
            $uploads = PlotUpload::where('plot_id', $request->plot_id)->get()->map(function ($upload) { return $upload->uploadToArray() ;});

        }


        return view('plots.titles.index')
            ->with([
                'projects' => $projects,
                'uploads'  => $uploads
            ]);

    }


    public function issueTitle($plotId)
    {

        $plot = Plot::where('id', $plotId)->first();

        $plot->is_title_issued = true;
        $plot->issued_by = auth()->id();
        $plot->date_issued = Carbon::now();

        $plot->save();

        Session::flash('success','marked has issued');

        return back();


    }
    public function successResponse($message)
    {
        $projectsChecks = Product::all();


        $projects = [];
        $helds = [];

        foreach ($projectsChecks as $projectsCheck) {

            foreach ($projectsCheck->plots->where('can_sell', true) as $plot)
            {

                if ($plot->available())
                {
                    $projects[] = $projectsCheck;
                }
            }

        }

        $projects = collect($projects)->unique();
        $helds = collect($helds)->unique();


       return view('plots.manage_plots')
           ->with([
               'plots' => $message,
               'sizes' => PlotSizeSetting::all(),
               'products' => $projectsChecks,
               'projects' => $projects,
               'holds'   => $helds
           ]);
    }


    public function details(Plot $plot)
    {

        $sale = $plot->getSales();

        $offer = $plot->getOffers();

        $paymentRequests = SalePayment::withoutGlobalScopes()->where([
            'sale_id'  => $sale->id,
            'is_approved' => false
        ])->get();


        return view('plots.details')
            ->with([
                'sale'            => $sale,
                'offer'           => $offer,
                'plot'            => $plot,
                'paymentRequests' => $paymentRequests
            ]);


    }
}
