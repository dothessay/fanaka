<?php

namespace App\Http\Controllers\PLots;

use App\DB\Product\Product;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Codex\Contracts\ServiceCaller;
use Codex\Services\Plots\PlotReportsService;
use Codex\Services\Plots\PlotReportDateSearchService;
use App\Plot\Plot;

class PlotReportsController extends Controller implements ServiceCaller
{
    public function index()
    {
        //return view('plots.plot_reports');
        return view('plots.index');

        //return (new PlotReportsService($this))->run($request->all());
    }
    public function avaialble(Request $request)
    {

        $productCode = isset($request['project_code']) ? $request['project_code'] : null;
        $plotId = isset($request['plot_id']) ? $request['plot_id'] : null;

        $plots = Plot::where([
            'product_code'  => $productCode,
            'id'  => $plotId,
        ])->get()->map(function ($plot) {
            return [
                'plot_no'        => $plot->plot_no,
                'project_name'   => $plot->product->name(),
                'unit_price'     =>  number_format((double)$plot->price , 2),
                'selling_price'  => number_format((double)$plot->getSellingPrice() , 2),
                'customers'      => $plot->getCustomers(),
                'sale_date'      => $plot->getSaleDate(),
                'sale_ref'       => $plot->getSaleRef(),
                'discount'       => $plot->getDiscount(),
                'served_by'      => $plot->getSaleServedBy(),
                'balance'        => $plot->getSaleBalance(),

            ];
        });



        $projects = Product::all()->map(function ($project) {

            return [
                'project_code' => $project->code,
                'name' => $project->name,
                'plots' => $project->plotsToArray()
            ];
        });




        return view('plots.reports.available')->with([
            'plots' => $plots,
            'projects'  => $projects
        ]);

        //return (new PlotReportsService($this))->run($request->all());
    }

    public function searchdate(Request $request)
    {
        $data = $request->all();
        
        $builder = Plot::query();
        
        $builder->where(function ($query) {
                        $query->where('is_sold', '1')
                              ->orWhere('is_reserved', 1);
                    });    
            
        
        if($data['is_date_search'] == "yes")
        {
            $start = $data['start_date'];
            $end = $data['end_date'];
            
            $builder->whereBetween('created_at', array($start, $end));
    
        }    
        
        if(isset($data["search"]["value"]))
        {
            
            $alias = $data["search"]["value"];

            $builder->where(function($query) use ($alias) {
                        return $query
                              ->where('created_at', 'like', '%' . $alias . '%')
                              ->orWhere('price', 'like', '%' . $alias . '%')
                              ->orWhere('plot_no', 'like', '%' . $alias . '%')
                              ->orWhere('title_no', 'like', '%' . $alias . '%');
                    });
        }
        
        $plots = $builder->get();
        
        $number_filter_row = $plots->count();
        
        $data2 = array();
        
        foreach ($plots as $plot)
        {
            $isSold = '';
            
            if ($plot->is_reserved)
            {
                $isSold = "Reserved";
            }

            if ($plot->is_sold) 
            {
                $isSold = "Sold Out";
            }
            
            $sub_array = array();
            
            $sub_array[] = date_format($plot->created_at, "Y-m-d H:i:s");
            $sub_array[] = $plot->product->name;
            $sub_array[] = number_format($plot->price , 2);
            $sub_array[] = $plot->size->label;
            $sub_array[] = $plot->plot_no;
            $sub_array[] = $plot->title_no;
            $sub_array[] = $isSold;
            $sub_array[] = $plot->user->fullName();
            
            $data2[] = $sub_array;
        }
        
        $output = array(
            "draw"    => intval($data["draw"]),
            "recordsTotal"  => $this->getTotalRecords(),
            "recordsFiltered" => $number_filter_row,
            "data"    => $data2
        );
        
        $response = response()->json($output);
        
        return $response;
    }

    public function getTotalRecords()
    {
        $records = Plot::where('is_sold', 1)
            ->orWhere('is_reserved', 1)    
            ->get();
        
        $records = $records->count();
        
        return $records;
        
    }

        public function successResponse($message) 
    {
        return view('plots.plot_reports')
            ->with([
                'plots' => $message
            ]);
    }
    
    

}
