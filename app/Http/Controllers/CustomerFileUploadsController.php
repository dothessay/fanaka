<?php

namespace App\Http\Controllers;

use App\DB\Customer\CustomerFileUpload;
use Codex\Contracts\ServiceCaller;
use Codex\Services\Customers\ListCustomerService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class CustomerFileUploadsController extends Controller implements ServiceCaller
{
    public function index()
    {
        $customerIds = (new ListCustomerService($this))->list([])->pluck('id')->toArray();


        $files = CustomerFileUpload::whereIn('customer_id', $customerIds)
            ->get()
            ->map(function ($q) {
                return $q->uploadsToArray();
            });



        return view('customers.uploads.index')
            ->with([
                'files' => $files
            ]);


    }

    public function markAsSeen(CustomerFileUpload $file)
    {
        $file->is_seen =  true;

        $file->save();
        Session::flash('success', "Marked as seen");

        return redirect()->back();

    }

    public function successResponse($message)
    {
        // TODO: Implement successResponse() method.
    }
}
