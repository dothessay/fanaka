<?php

namespace App\Http\Controllers;

use App\DB\Accounting\Account;
use Codex\Classes\Filters\DateBetweenFilter;
use Illuminate\Http\Request;
use Illuminate\Pipeline\Pipeline;

class IncomeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        // income = all accounts records

        $request['start_date'] = isset($request['start_date'])  && ! is_null($request['start_date'])? $request['start_date'] : now()->startOfMonth();
        $request['end_date'] = isset($request['end_date'])  && ! is_null($request['start_date'])  ? $request['end_date'] : now()->endOfYear();

        $income = app(Pipeline::class)
            ->send(Account::query())
            ->through([
                DateBetweenFilter::class
            ])
            ->thenReturn()
            ->get();

        return  view('sales.income')
            ->with(['incomes'  => $income]);


    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
