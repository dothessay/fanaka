<?php
/**
 * Created by PhpStorm.
 * User: philip
 * Date: 12/14/18
 * Time: 2:34 PM
 */

namespace App\Http\Controllers\API;


use App\Http\Controllers\Controller;

class ApiBaseController extends Controller
{
    public function __construct()
    {
        $headers = array_change_key_case(getallheaders(),CASE_UPPER);

        $userId = $headers['USERID'];

        if(isset($userId)){

            auth()->loginUsingId($userId);
        }
    }


    public function failureResponse($message)
    {

        return response()->json([
            'message' => $message
        ] , 500);
    }

}