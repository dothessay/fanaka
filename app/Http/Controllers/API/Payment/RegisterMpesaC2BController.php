<?php

namespace App\Http\Controllers\API\Payment;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class RegisterMpesaC2BController extends Controller
{
    public function index()
    {
        $mpesa = new \Safaricom\Mpesa\Mpesa();


        $token = $mpesa::generateSandBoxToken();


        if (env('MPESA_ENV') == 'sandbox'){
            $url = 'https://sandbox.safaricom.co.ke/mpesa/c2b/v1/registerurl';
        }else{
            $url = 'https://api.safaricom.co.ke/mpesa/c2b/v1/registerurl';
        }



        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_HTTPHEADER, [
            'Content-Type:application/json','Authorization:Bearer '.$token]); //setting custom header


        $curl_post_data = array(
            //Fill in the request parameters with valid values
            'ShortCode' => env('MPESA_SHORTCODE'),
            'ResponseType' => 'CustomerPayBillOnline',
            'ConfirmationURL' => url('/c2b/confirmation'),
            'ValidationURL' => url('/c2b/validation')
        );

        $data_string = json_encode($curl_post_data);

        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $data_string);

        $curl_response = curl_exec($curl);

        $result = json_decode($curl_response, true);


        return response()->json($result, 200);


    }
}
