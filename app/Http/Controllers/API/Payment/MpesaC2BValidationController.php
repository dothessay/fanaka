<?php

namespace App\Http\Controllers\API\Payment;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Safaricom\Mpesa\Mpesa;

class MpesaC2BValidationController extends Controller
{
    public function index(Request $request)
    {

        //Return a success response to m-pesa
        $response = array(
            'ResultCode' => 0,
            'ResultDesc' => 'Success'
        );
        echo json_encode($response);

        //Get input stream data and log it in a file
        $payload = file_get_contents('php://input');
        $file = 'validation_log.txt'; //Please make sure that this file exists and is writable
        $fh = fopen($file, 'a');
        fwrite($fh, "\n====".date("d-m-Y H:i:s")."====\n");
        fwrite($fh, $payload."\n");
        fclose($fh);

        $mpesa = new Mpesa();
        $mpesa->finishTransaction();
    }
}
