<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;


    public function validateForm(Request $request, array $rule , array $messages = [])
    {

        $validator = \Validator::make($request->all(), $rule , $messages);

        if ($validator->fails()) {

            if ($request->ajax())
            {
                return ['status' => "VALIDATE", "message" => $validator->getMessageBag()->toArray()];
            }
            if (! $request->ajax())
            {

                return redirect()
                    ->back()
                    ->withErrors($validator)
                    ->setStatusCode('422')
                    ->withInput();
            }
        }
        return $request;
    }

    public function validateAjaxForm(Request $request, array $rule , array $messages = [])
    {

        $validator = \Validator::make($request->all(), $rule , $messages);

        if ($validator->fails()) {

                return
                    response()
                    ->json(['status' => "VALIDATE", "message" => $validator->getMessageBag()->toArray()], 422);

        }
        return $request;
    }



    public function failureResponse($message)
    {
        if (request()->ajax())
        {
            return response()->json( [
                'status' => "ERROR",
                'message' => $message,
            ] , 500);
        }
        return redirect()->back()->with('error',$message);
    }


    public function businessCode()
    {
        return auth()->user()->business->code;
    }
}
