<?php

namespace App\Http\Controllers\Communication;

use App\Customer\Customer;
use App\DB\Business\Meta;
use App\DB\Communication\Communication;
use App\DB\Communication\Contact;
use App\DB\Product\Product;
use App\Jobs\SendSmsNotifications;
use Carbon\Carbon;
use Codex\Classes\Handlers\Exports\BulkSmsImport;
use Codex\Classes\Handlers\Exports\ContactsExports;
use Codex\Classes\Handlers\GetClientsContactHandler;
use Codex\Classes\Handlers\Imports\ContactsImport;
use Codex\Classes\Handlers\PusherMessages;
use Codex\Classes\Handlers\SendAfricanTalkingInnoxNotification;
use Codex\Classes\Handlers\SystemContacts;
use Codex\Classes\Handlers\SystemSms;
use Codex\Classes\Repository\ContactRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Codex\Classes\Handlers\Excel\ExcelReader;
use Illuminate\Validation\Rule;
use Maatwebsite\Excel\Facades\Excel;
use PhpOffice\PhpSpreadsheet\IOFactory;


class SendCommunicationController extends Controller
{

    public function index(Request $request)
    {

        $customers = Customer::all();

        return view('communication.send.index')->with([
            'customers' => $customers
        ]);

    }


    public function store(Request $request)
    {
        try {
            DB::beginTransaction();

            //create communication

            $message = !is_null($request['sms_message']) ? $request['sms_message'] : $request['text_message'];

            $communication = Communication::create([
                'message' => $message,
                'message_type' => $request['type'],

            ]);

            //create  metas

            foreach ($request['customerId'] as $index => $customerId) {

                $communication->metas()->create([
                    'customer_id' => $customerId,
                    'key' => 'is_read',
                    'value' => 'false'

                ]);

                $data = [
                    'message' => $communication->message,
                    'customerId' => $customerId,
                    'title' => $request['type']
                ];


                PusherMessages::build()
                    ->send($data);


            }


            DB::commit();

            Session::flash('success', "message sent successfully");

            return back();

        } catch (\Exception $exception) {
            DB::rollback();

            Session::flash('error', "message not sent successfully " . $exception->getMessage());


            return back();


        }

    }

    public function downloadContacts()
    {
        return response()
            ->download('uploads/samples/contacts.xlsx');


    }


    public function bulkSms(Request $request)
    {

        try {

            if ($request->hasFile('contacts') )
            {
                Excel::import(new BulkSmsImport($request['message']), request()->file('contacts'));

                Session::flash('success', "Bulk sms sent successfully");
            }
            if (isset($request['message']) && ! empty($request->message) && $request->has('phone') && ! is_null($request->phone))
            {
                $phoneNumbers = explode(',', $request['phone']);

                // convert numbers to  integers

              collect($phoneNumbers)->map( function ($tel) use ($request) {
                    if (strlen($tel) >= 12 && strlen($tel) <= 13 )
                    {
                        SendSmsNotifications::dispatch([
                            'to' =>  $tel,
                            'message' => $request->message ,
                        ]);
                    }
                })->filter()->toArray();

            }

        } catch (\Exception $exception) {

            dd($exception);
            Session::flash('error', "An error occurred while sending the messages! Kindly try again later");
        }


        return back();


    }

    public function contacts(Request $request)
    {


        return view('communication.contact.index')
            ->with([
                'contacts'  => $this->listContacts($request)
            ]);

    }

    public function storeContacts(Request $request)
    {
        try {
            $response = $this->validateAjaxForm($request , [
                'name'  => 'required',
                'phone'  => [
                    'required',
                    Rule::unique('contacts','phone')->ignore($request['id'])
                ]
            ] , [
                'name.required' => 'Name is field is required',
                'phone.unique'  => 'Phone number already registered'
            ]);

            if (! $response instanceof $request)
            {
               return $response;
            }

            $contact = Contact::updateOrCreate([
                'id' => $request['id']
            ], [
                'phone' => $request['phone'],
                'name' => $request['name'],
                'user_id' => auth()->id(),
                'hasVisited' => $request['hasVisited'],
            ]);
            $meta = Meta::create([
                'key' => "status",
                'value' => "warm"
            ]);
            $contact->saveMeta($meta);

            if(isset($request['comment']) && !is_null($request['comment']))
            {
                $meta = Meta::create([
                    'key' => "comment",
                    'value' => $request['comment']
                ]);

                $contact->saveMeta($meta);

            }

            return response()
                ->json([
                    'message' => 'success'
                ], 200);
        } catch (\Exception $exception) {
            return response()
                ->json([
                    'message' => $exception->getMessage()
                ], 500);
        }
    }

    public function importContacts(Request $request)
    {
        try {

            Excel::import(new ContactsImport(), request()->file('xls'));

            return response()
                ->json([
                    'message' => 'success'
                ], 200);

        } catch (\Exception $exception) {
            return response()
                ->json([
                    'message' => $exception->getMessage()
                ], 500);
        }
    }

    public function listContacts(Request $request)
    {
        $contacts = (new ContactRepository())
            ->all($request);


        return $contacts;

        return response()
            ->json([
                'data' => $contacts->filter()
            ]);

    }

    public function ImportsContactsSamples()
    {
        return response()
            ->download(url('uploads/samples/imports_contacts.xlsx'));


    }

    public function exportsContacts(Request $request)
    {
        $startDate = isset($request['startDate']) ? Carbon::parse($request['startDate'])->startOfDay() : Carbon::now()->startOfMonth();
        $endDate = isset($request['endDate']) ? Carbon::parse($request['endDate'])->endOfDay() : Carbon::now()->endOfMonth();

        return (new ContactsExports($startDate, $endDate))->download('contacts.xlsx');


    }

    public function webmail()
    {
        return view('communication.emails.index');

    }

    public function editContacts($contactId)
    {
        $contact = Contact::find($contactId);

        $projects = Product::all();


        $projectArray = collect();

        foreach ($projects as $project) {

            if ($project->availablePlots() > 0) {

                $projectArray->push($project);
            }
        }
        $projects = [];

        if ($contact->getMeta('projects', false)) {
            $projects = unserialize($contact->getMeta('projects'));
        }

        return view('communication.contact.edit')
            ->with([
                'contact' => $contact,
                'products' => $projectArray,
                'projects' => $projects
            ]);
    }

    public function updateContacts($contactId, Request $request)
    {
        $contact = Contact::where('id', $contactId)->first();
        $contact->name = $request['name'];
        $contact->phone = $request['phone'];
        $contact->hasVisited = isset($request['has_visited']) ? true : false;

        if (!is_null($request['date'])) {
            $contact->date = Carbon::parse($request['date']);

        }

        if ($contact->getMeta('projects', false)) {
            $contact->metable()->delete();
        }
        if ($contact->getMeta('collected_at', false)) {
            $contact->metable()->delete();
        }

        if ($contact->getMeta('comment', false)) {
            $contact->metable()->delete();
        }

        if ($contact->getMeta('status', false)) {
            $contact->metable()->delete();
        }
        if ($contact->getMeta('next_follow_up', false)) {
            $contact->metable()->delete();
        }
        if ($contact->getMeta('last_follow_up', false)) {
            $contact->metable()->delete();
        }

        if (!is_null($request['project'])) {
            $projects = serialize($request['project']);
            $meta = Meta::create([
                'key' => "projects",
                'value' => $projects
            ]);
            $contact->saveMeta($meta);
        }

        if (!is_null($request['last_follow_up'])) {
            $meta = Meta::create([
                'key' => "last_follow_up",
                'value' => Carbon::parse($request['last_follow_up'])
            ]);
            $contact->saveMeta($meta);
        }

        if (!is_null($request['next_follow_up'])) {
            $meta = Meta::create([
                'key' => "next_follow_up",
                'value' => Carbon::parse($request['next_follow_up'])
            ]);
            $contact->saveMeta($meta);
        }

        if (!is_null($request['collected_at'])) {
            $meta = Meta::create([
                'key' => "collected_at",
                'value' => Carbon::parse($request['collected_at'])
            ]);
            $contact->saveMeta($meta);
        }

        if (!is_null($request['status'])) {
            $meta = Meta::create([
                'key' => "status",
                'value' => $request['status']
            ]);
            $contact->saveMeta($meta);
        }
        if (!is_null($request['comment'])) {
            $meta = Meta::create([
                'key' => "comment",
                'value' => $request['comment']
            ]);
            $contact->saveMeta($meta);
        }
        $contact->save();

        //dd($contact, $request->all());

        Session::flash('success', 'Contact updated successfully');
        return redirect()->route('communication_manage_contacts')->send();
    }
}
