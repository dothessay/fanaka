<?php

namespace App\Http\Controllers\Communication;

use App\DB\Communication\EmailSetting;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;

class CommunicationController extends Controller
{
    public function index()
    {
        $user = EmailSetting::where('user_id', auth()->id())->first();

        return view('communication.setting.index')->with([
            'user' => $user
        ]);

    }

    public function store(Request $request)
    {

        $user = EmailSetting::where('user_id', auth()->id())->first();

        if (! $user) {

            $user = new EmailSetting();

        }

        $user->user_id = auth()->id();
        $user->email = $request['email'];

        $user->password = base64_encode(json_encode($request['password'] ));

        $user->save();


        return redirect(route('communication_settings'));



    }
}
