<?php

namespace App\Http\Controllers\Communication;

use Carbon\Carbon;
use Codex\Classes\Handlers\ImapHandler;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Cache;

class ManageCommunicationController extends Controller
{
    public function index()
    {



        return view('communication.emails.index');

    }

    public function listEmails()
    {


        $key = 'myInbox'.auth()->id();

        $inbox = $this->reactiveFromCache($key);


        return response()
            ->json([
                'inboxes'  => $inbox
            ]);


    }




    private function  reactiveFromCache($key)
    {

        return Cache::store('file')->get($key);

    }
    private function  reactiveBodyFromCache($key)
    {

        return Cache::store('file')->get($key);

    }


    public function readEmails($uid)
    {

        $mailBox =  'myInboxMail'.auth()->id().'_'.$uid;

        $mail = Cache::store('file')->get($mailBox);

       // $body = collect($mail)->where('uid', $mail['uid'])->first()['body'];



        return view('communication.emails.read')
            ->with([
                'inbox' => $mail,
                'uid'   => $uid
            ]);



    }
}

