<?php

namespace App\Http\Controllers\V2\Client;

use Codex\Classes\Repository\CustomerRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Validation\Rule;

class ClientController extends Controller
{
    private $customer;


     public function __construct()
     {
         $this->customer = new CustomerRepository();
     }

    /**
     *
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        try{
            $customers =  collect($request->user()->customers());
            if ($request->has('filter'))
            {
                $customers = $customers->filter(function ($item) use ($request) {
                        // replace stristr with your choice of matching function
                        return false !== stristr($item->full_name, $request['filter']);
                    });

            }
            $customers = $customers->map(function ($q) { return $q->customerToArray() ;});
            return  response()
                ->json([
                    'data'  => $customers->paginate(10)
                ]);
        }catch ( \Exception $exception)
        {
            return  response()
                ->json([
                    'error'  => 'Un authenticated' . $exception->getMessage()
                ] , 403);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $response = $this->validateAjaxForm($request, [
            'name'   => 'required',
            'phone_number' => [
                'nullable',
                Rule::unique('customers','phone_number')->whereNotNull('phone_number')
            ],
            'email' => [
                'nullable',
                Rule::unique('customers','email')->whereNotNull('email')
            ],
            'id_no' => [
                'required',
                Rule::unique('customers','id_no')->whereNotNull('id_no')
            ]
        ]);

        if(! $response instanceof $request){
            return  $response;
        }

        $name = explode(" ", $request->name);

        if (sizeof($name) < 2){
            return  response()
                ->json([
                    'message' => "Client must have at least two names"
                ], 500);
        }

        $request->request->add(['first_name' => $name[0]]);

        if(isset($request['name'][1]) && ! isset($name[2])){
            $request->request->add(['last_name' => $name[1]]);
        }
        if(isset($name[1]) &&  isset($name[2]))
        {
            $request->request->add(['middle_name' => $name[1]]);
            $request->request->add(['last_name' => $name[2]]);
        }
        else{

            $request->request->add(['middle_name' => '.']);
        }

        (new CustomerRepository())
            ->store($request);

        return  response()
            ->json([
                'message' => "successfully created the a client"
            ]);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        $request->request->add(['id' => $id]);

        try{
            return  response()
                ->json([
                    'data'  => $this->customer->all()->map(function ($q) { return $q->customerWithSalesToArray() ;})
                ]);
        }catch ( \Exception $exception)
        {
            return  response()
                ->json([
                    'error'  => 'Un authenticated'
                ] , 403);
        }
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
