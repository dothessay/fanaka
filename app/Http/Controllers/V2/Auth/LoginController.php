<?php

namespace App\Http\Controllers\V2\Auth;

use GuzzleHttp\Client;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class LoginController extends Controller
{
    public function login(Request $request)
    {
        $http = new Client();

        $response = $http->post(url('oauth/token'), [
            'form_params' => [
                'grant_type' => 'password',
                'client_id' => env('PASSPORT_CLIENT'),
                'client_secret' => env('PASSPORT_SECRET'),
                'username' => $request['email'],
                'password' => $request['password'],
                'scope' => '',
            ],
        ]);

        return json_decode((string) $response->getBody(), true);

    }

    public function profile(Request $request)
    {

        return response()
            ->json([
                'profile' => $request->user()->userToArray()
            ]);
    }
}
