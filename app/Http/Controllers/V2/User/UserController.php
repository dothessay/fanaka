<?php

namespace App\Http\Controllers\V2\User;

use Codex\Classes\Repository\UserRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class UserController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        try{
            return  response()
                ->json([
                    'agents' => (new UserRepository())->all()->map->userToArray()
                ]);

        }catch (\Exception $exception)
        {
            response()
                ->json([
                    'message' => $exception->getMessage()
                ], 500);

        }
    }

    public function sales(Request $request)
    {

        try{
            return  response()
                ->json([
                    'data' => collect( $request->user()->salesToArray())->paginate(10)
                ] , 200);

        }catch (\Exception $exception)
        {
            response()
                ->json([
                    'message' => $exception->getMessage()
                ], 500);

        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request , $id)
    {
        try{
            return  response()
                ->json([
                    'agents' => collect( $request->user()->saleToArray())->paginate(10)
                ]);

        }catch (\Exception $exception)
        {
            response()
                ->json([
                    'message' => $exception->getMessage()
                ], 500);

        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
