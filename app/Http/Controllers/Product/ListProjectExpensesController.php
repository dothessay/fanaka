<?php

namespace App\Http\Controllers\Product;

use App\DB\Product\Product;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Codex\Contracts\ServiceCaller;
use Codex\Services\Products\ListProjectExpensesService;

class ListProjectExpensesController extends Controller implements ServiceCaller
{
    public function index(Request $request)
    {

        return (new ListProjectExpensesService($this))->run($request->all());
    }

    public function successResponse($message) 
    {
        return view('product.projectexpenses', with([
            'projectexpenses'=>$message,
            'projects' => Product::all(),
        ]));
    }

    public function getExpenses($id)
    {
       return (new ListProjectExpensesService($this))->list(['id' => $id])->first();

    }

}
