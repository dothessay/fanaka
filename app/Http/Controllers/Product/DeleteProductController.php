<?php

namespace App\Http\Controllers\Product;

use Codex\Contracts\ServiceCaller;
use Codex\Services\Products\DeleteService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class DeleteProductController extends Controller implements ServiceCaller
{
    public function index(Request $request)
    {
        return (new DeleteService($this))->run($request->all());

    }
    public function successResponse($message)
    {
        return back()->with('success', $message);
    }
}
