<?php

namespace App\Http\Controllers\Product;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Codex\Contracts\ServiceCaller;
use Codex\Services\Products\StoreProjectExpensesService;
use App\DB\Product\ProjectExpensePurpose;

class StoreProjectExpensesController extends Controller implements ServiceCaller
{
    public function index(Request $request)
    {
        $response = $this->validateForm($request, [

            "project_id" => [
                'required',
            ],
            "cost" => [
                'required',
            ],
            "purpose" => [
                'required',
                
            ],
            
        ]);


        if (! $response instanceof  $request)
        {
            return $response;
        }

        return (new StoreProjectExpensesService($this))->runWithDBTransaction($request->all());
    }
    
    public function successResponse($message) 
    {
        return back()->with('success', $message);
    }

    
    public function loadExpenses($project_id)
    {
        $expenses = ProjectExpensePurpose::select('project_expense_purposes.id', 'project_expense_purposes.cost', 'project_expense_purposes.purpose')
            ->join('project_expenses', 'project_expenses.id', '=', 'project_expense_purposes.expense_id')
            ->where('project_expenses.project_id', $project_id)
            ->get();
                
        $response = response()->json($expenses);
        
        return $response;
    }
}
