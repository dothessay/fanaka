<?php

namespace App\Http\Controllers\Product;

use Codex\Contracts\ServiceCaller;
use Codex\Services\Products\StoreProductsService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Validation\Rule;
use Codex\Services\Products\UpdateProductService;

class StoreProductController extends Controller implements ServiceCaller
{
    public function index(Request $request)
    {
        $response = $this->validateAjaxForm($request, [

            "title_no" => [
                'required',
                Rule::unique('products')
            ],
            "phase" => [
                'required',
            ],
            "land_location" => [
                'required',
            ],
        ]);


        if (! $response instanceof  $request)
        {
            return $response;
        }

        return (new StoreProductsService($this))->runWithDBTransaction($request->all());

    }
    public function successResponse($message)
    {
        return response()->json(['message'  =>  $message]);
    }
    
    public function update(Request $request)
    {
        $response = $this->validateAjaxForm($request, [
            "phase" => [
                'required',
            ],
            "title_no" => [
                'required',
                Rule::unique('products')->ignore($request->id)
            ],
            "land_location" => [
                'required',
            ],
        ]);


        if (! $response instanceof  $request)
        {
            return $response;
        }

        return (new UpdateProductService($this))->runWithDBTransaction($request->all());
    }
}
