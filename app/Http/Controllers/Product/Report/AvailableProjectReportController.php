<?php

namespace App\Http\Controllers\Product\Report;

use App\DB\Product\Product;
use Codex\Classes\PdfOutput\DataReport;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AvailableProjectReportController extends Controller
{
    public function index(Request $request)
    {
        if (isset($request['product_code']))
        {
            $project = Product::where(['code' => $request['product_code']])->first();

        }
        else{
            $project = Product::where(['code' => $request['product_code']])->first();
        }



        $available = [];

        if ($project)
        {
            $plots = $project->plots;


            foreach ($plots as $plot) {

                if ($plot->available())
                {
                    $available[] = $plot;
                }

            }

        }

        if (isset($request['pdf']))
        {

            $html = view('product.report.available._available_data')->with([
                'plots' =>  collect($available),
                'projects' => Product::all(),
                'project' => $project
            ])->render();


            return (new DataReport())->outPut($html);
        }




        return view('product.report.available.available')->with([
            'plots' =>  collect($available),
            'projects' => Product::all(),
            'project' => $project
        ]);

    }

    public function printMap($id)
    {

        $html = Product::where('id', $id)->first();

        return (new DataReport())->outPut($html->generateMap());

    }
}
