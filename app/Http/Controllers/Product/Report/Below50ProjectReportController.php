<?php

namespace App\Http\Controllers\Product\Report;

use App\DB\Product\Product;
use Codex\Classes\PdfOutput\DataReport;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class Below50ProjectReportController extends Controller
{
    public function index(Request $request)
    {

        if (isset($request['project']))
        {
            $project = Product::where(['slug' => $request['project']])->first();

        }
        else{
            $project = Product::where(['slug' => $request['project']])->first();
        }

        $above50 = [];

        if ($project)
        {
            $plots = $project->plots;


            foreach ($plots as $plot) {

                if ($plot->isSold())
                {
                    $saleItem = $plot->saleItem;

                    if ($saleItem)
                    {
                        $sale = $saleItem->sale;
                        $percentage = $sale->getPercentage();
                        if ($percentage < 50) {
                            $above50[] = $plot;
                        }
                    }
                }

            }

        }

        if (isset($request['pdf']))
        {
            $html = view('product.report.below50.data')->with([
                'plots' =>  collect($above50),
                'projects' => Product::all(),
                'project' => $project
            ])->render();


            return (new DataReport())->outPut($html ,'Below_50%_in_'.$project->name());

        }


        return view('product.report.below50.below')->with([
            'plots' =>  collect($above50),
            'projects' => Product::all(),
            'project' => $project
        ]);

    }
}
