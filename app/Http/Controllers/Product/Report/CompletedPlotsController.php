<?php

namespace App\Http\Controllers\Product\Report;

use App\DB\Product\Product;
use Codex\Classes\PdfOutput\DataReport;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CompletedPlotsController extends Controller
{
    public function index(Request $request)
    {

        if (isset($request['product_code']))
        {
            $project = Product::where(['code' => $request['product_code']])->first();

        }
        else{
            $project = Product::where(['code' => $request['product_code']])->first();
        }

        $completed = [];

        if ($project)
        {
            $plots = $project->plots;


            foreach ($plots as $plot) {

                $saleItem = $plot->saleItem;

                if ($saleItem)
                {
                    $sale = $saleItem->sale;
                    $percentage = $sale->getPercentage();
                    if ($percentage > 99.99999) {
                        $completed[] = $plot;
                    }
                }

            }

        }




        if (isset($request['pdf']))
        {
            $html = view('product.report.completed.data')->with([
                'plots' =>  collect($completed),
                'projects' => Product::all(),
                'project' => $project
            ])->render();


            return (new DataReport())->outPut($html);
        }


        return view('product.report.completed.completed')->with([
            'plots' =>  collect($completed),
            'projects' => Product::all(),
            'project' => $project
        ]);

    }
}
