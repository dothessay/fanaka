<?php

namespace App\Http\Controllers\Product\Report;

use App\DB\Product\Product;
use App\Sale\Sale;
use App\Sale\SaleItem;
use App\Sale\SalePayment;
use Carbon\Carbon;
use Codex\Classes\Filters\AgentFilter;
use Codex\Classes\Filters\CommissionDateBetweenFilter;
use Codex\Classes\Filters\CommissionFilter;
use Codex\Classes\Filters\DateBetweenFilter;
use Codex\Classes\Filters\IdFilter;
use Codex\Classes\Handlers\Exports\ExportExcelReports;
use Codex\Classes\Handlers\ReportBuilder;
use Codex\Classes\PdfOutput\DataReport;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Pipeline\Pipeline;
use Maatwebsite\Excel\Facades\Excel;

class ProjectReportController extends Controller
{
    public function index()
    {
        return view('product.report');

    }

    public function below30K(Request $request)
    {

        $sales = ReportBuilder::build()->below30K();

        if (isset($request['print'])) {
            $html = view('product.report.below30k._data')->with([
                'sales' => $sales,
            ])->render();

            return (new DataReport())->outPut($html, 'All_below_30k');

        }


        return view('product.report.below30k.index')->with([
            'sales' => $sales,
        ]);

    }

    public function unpaid(Request $request)
    {
        $project = new Product();
        $saleArray = [];
        $newSales = [];
        if (isset($request['project']) &&  strtolower($request['project']) != 'all') {
            $project = Product::where(['id' => $request['project']])->firstOrFail();

        }

        if (! auth()->user()->can('access-module-component-functionality', 'sale_manage_show-all-sales')) {

            \request()->request->add(['agent' =>  auth()->id() ]);

        }

        $sales = app(Pipeline::class)
            ->send(Sale::query())
            ->through([
                AgentFilter::class,
                IdFilter::class,
                DateBetweenFilter::class,
                CommissionDateBetweenFilter::class,
                CommissionFilter::class
            ])
            ->thenReturn()
            ->with('saleItems')
            ->get();
        $color = "";

        $saleItem = collect();

        if ( $request->has('project') && strtolower($request->project ) != 'all')
        {
            $sales = Product::where('id', $request->project)->firstOrFail()->sales();


            if (! auth()->user()->can('access-module-component-functionality', 'sale_manage_show-all-sales')) {

                $sales = $sales->where('agent', auth()->id());

            }

        }

        foreach ($sales as $sale) {


            if ( isset($sale->payments()->first()->created_at)
                && ! Carbon::parse($sale->payments()->first()->created_at)->isLastMonth()
                && ! Carbon::parse($sale->payments()->first()->created_at)->isCurrentMonth())
            {
                $color = 'orange';
            }


            if ($sale->getBalance() >= 1 )
            {

               if ($request->has('past_due_date'))
               {
                   if ( isset($sale->installment) && Carbon::parse($sale->installment->completion_date)->lessThan(now()))
                   {
                       $color = 'orange';

                       $saleArray[] = $this->getUnpaidToArray($sale , $color);
                   }
               }
                else{

                    $saleArray[] = $this->getUnpaidToArray($sale , $color);
                }
            }

        }


        $newSales = $saleArray;
        if ($request->has('rate') && $request['rate'] != 'All')
        {
           // unset($newSales);


            foreach ($saleArray as $item)
            {
                if ($item['percentage'] >= $request['rate'])
                {
                    $newSales[] = $item;
                }

            }

        }

        if ($request->has('pdf')) {
            $html = view('product.report.unpaid._data')->with([
                'sales' => $newSales,
            ])->render();

            return (new DataReport())->outPut($html, 'All_un_unpaid_in_' . $project->name());

        }

        if ($request->has('excel'))
        {

            $view = view('product.report.unpaid._data')->with   ([
                'sales' => $newSales
            ]);

            return Excel::download(new ExportExcelReports($view), 'defaulters_report_'.now()->format('y-m-d').'.xlsx');

        }


        return view('product.report.unpaid.index')->with([
            'sales' => $newSales,
            'projects' => Product::all(),
            'project' => $project
        ]);

    }

    private  function getUnpaidToArray(Sale $sale, $color){
        return [
            'ref'       => "FSL/".$sale->id,
            'customer'  => $sale->getCustomerHref(),
            'phone_number'  => $sale->getCustomerPhoneNumber(),
            'agent'     => $sale->soldBy->fullname(),
            'type'      => "sale",
            'color'     => $color,
            'plot'      => $sale->getCustomerPlotNos(),
            'amount'    => $sale->getPaidAmount(),
            'balance'   => $sale->getBalance(),
            'percentage'=> $sale->getPercentage(),
            'date'      =>  Carbon::parse($sale->created_at)->format('Y-m-d'),
            'last_amount'       => isset($sale->payments()->first()->amount) ? $sale->payments()->first()->amount : 0,
            'last_date'         =>  isset($sale->payments()->first()->created_at) ? Carbon::parse($sale->payments()->first()->created_at)->format('Y-m-d') : "--",
            'completion_date'   => Carbon::parse($sale->created_at)->addMonths($sale->installment ? $sale->installment->months : 6)->format('Y-m-d'),
        ];
    }
    public function allPlotsWithClients(Request $request)
    {

        if (isset($request['project'])) {
            $project = Product::where(['slug' => $request['project']])->first();

        } else {
            $project = Product::where(['slug' => $request['project']])->first();
        }


        $sales = ReportBuilder::build()->allPlotsWithClients($project);


        if (isset($request['pdf'])) {
            $html = view('product.report.client_plots._data')->with([
                'sales' => $sales,
            ])->render();

            return (new DataReport(['orientation' => 'L']))->outPut($html, 'All_plots_with_' . $project->name());

        }

        if (isset($request['excel'])) {
            $view = view('product.report.client_plots._data')->with([
                'sales' => $sales,
            ]);

            return Excel::download(new ExportExcelReports($view), 'plots_with_client'. $project->name().'.xlsx');



        }

        return view('product.report.client_plots.index')->with([
            'sales' => $sales,
            'projects' => Product::all(),
            'project' => $project
        ]);

    }

    public function collection(Request $request)
    {
        $startDate = isset($request['start_date']) ? Carbon::parse($request['start_date']) : now()->startOfYear();
        $endDate = isset($request['end_date']) ? Carbon::parse($request['end_date']) : now()->endOfYear();

        $total = [];

        if (isset($request['project_id'])) {

            foreach ($request['project_id'] as $projectId) {

                $project = Product::where('id', $projectId)->first();


                $salePayments = $project->salePayments
                    ->whereBetween('created_at', [$startDate, $endDate])
                    ->groupBy(function ($p) {
                        return Carbon::parse($p->created_at)->format('Y-F');
                    });

                foreach ($salePayments as $index => $payment) {

                    $total[$project->name()][] = [
                        'months' => $index,
                        'payments' => $payment
                    ];
                }

            }
        }


        if (isset($request['pdf']))
        {

            $html = view('product.report.collections._collection_data')->with(['totals' => $total])->render();

               return  (new DataReport())->outPut($html , 'Project Collection Report');
        }

        return view('product.report.collections.index')
            ->with([
                'projects' => Product::all(),
                'totals'    => $total,
            ]);

    }
}
