<?php

namespace App\Http\Controllers\Product;

use App\DB\Product\Product;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AvailablePlotsController extends Controller
{
    public function index($slug)
    {

        $product = Product::where('slug',$slug)->first();


        $available = [];


        foreach ($product->plots as $plot) {

            if ($plot->available())
            {
                $available[] = $plot;
            }
        }


        return view('product.available')->with([
            'plots' => collect($available),
            'product' => $product
        ]);


    }
    public function sold($slug)
    {

        $product = Product::where('slug',$slug)->first();
        $helds = [];
        foreach ($product->plots->where('can_sell', false)->where('is_reserved', true) as $plot)
        {
            $helds[] = $plot;

        }

        return view('product.sold')->with([
            'plots' => collect($helds),
            'product' => $product
        ]);


    }

    public function available(Product $product)
    {
        $plots = [];
        foreach ($product->plots->where('can_sell', false)->where('is_reserved', false) as $plot)
        {

            $plots[] = $plot;
        }
        return view('product.onhold')
            ->with([
                'product'  => $product,
                'plots'  =>  $plots,
                'title'  => 'Available for sale Plots'
            ]);
    }
    // available for holding
    public function hold(Product $product)
    {
        $plots = [];
        foreach ($product->plots->where('can_sell', false)->where('is_reserved', false)->where('is_sold',  false) as $plot)
        {
            $plots[] = $plot;
        }
        return view('product.onhold')
            ->with([
                'product'  => $product,
                'plots'  =>  $plots,
                'title'   => 'Held Plots'
            ]);
    }
}
