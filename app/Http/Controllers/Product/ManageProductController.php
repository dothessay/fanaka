<?php

namespace App\Http\Controllers\Product;

use App\DB\Plot\ProjectPricing;
use App\DB\Product\Product;
use App\Http\Controllers\Controller;
use App\Setting\PlotSizeSetting;
use Carbon\Carbon;
use Codex\Contracts\ServiceCaller;
use Codex\Services\Products\ListProductsService;
use function GuzzleHttp\Psr7\str;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class ManageProductController extends Controller implements ServiceCaller
{
    public function index(Request $request)
    {
        return (new ListProductsService($this))->run($request->all());
    }

    public function successResponse($message)
    {


        return view('product.manage')
            ->with([
                'products' => $message,
                'sizes' => PlotSizeSetting::descId()->get(),
            ]);
    }
    public function details($slug)
    {
        \request()->request->add(['id' => $slug]);
        return view('product.plots')
            ->with([
                'product' =>  (new ListProductsService($this))->list(['slug' => $slug])->first(),
                'sizes' => PlotSizeSetting::descId()->get(),
            ]);
    }


    public function pricing(Product $product)
    {


        return view('product.pricing.index')
            ->with([
                'product'  => $product
            ]);

    }
    public function pricingEdit(Product $product, ProjectPricing $pricing)
    {


        return view('product.pricing.form')
            ->with([
                'product'  => $product,
                'pricing'  =>  $pricing,
                'sizes'    => PlotSizeSetting::descId()->get(),
            ]);

    }

    public function pricingCreate(Product $product)
    {

        return view('product.pricing.form')
            ->with([
                'product'  => $product,
                'sizes'    => PlotSizeSetting::descId()->get(),
            ]);

    }

    public function pricingStore(Product $product , Request $request)
    {

        ProjectPricing::updateOrCreate([
            'project_id'        => $product->id,
            'payment_option'    => $request['payment_option'],
            'size_id'           => $request['size_id'],
            'id'                => $request['price_id'],
        ], [
            'project_id'      => $product->id,
            'payment_option'  => $request['payment_option'],
            'size_id'         => $request['size_id'],
            'amount'          => floatval(str_replace(' ','', str_replace(',','', $request['amount']))),
            'start_at'        =>  Carbon::parse($request['start_at']),
            'end_at'        =>  Carbon::parse($request['end_at']),
        ]);

        $message = ! is_null($request['price_id']) ? "Updated" : 'Saved';
        Session::flash('success', "Successfully  {$message} the record");

        return redirect()->route('projects_manage_pricing', ['product'  => $product->id]);

    }
    public function pricingDelete(Product $product , ProjectPricing $pricing)
    {
        $pricing->delete();


        Session::flash('success', "Successfully removed the record");

        return redirect()->route('projects_manage_pricing', ['product'  => $product->id]);

    }


}
