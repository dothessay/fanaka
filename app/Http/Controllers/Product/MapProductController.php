<?php

namespace App\Http\Controllers\Product;

use App\DB\Business\Meta;
use App\DB\Product\Product;
use Codex\Classes\FileUpload;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use stringEncode\Exception;

class MapProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($productId)
    {
        $product = Product::find($productId);

        return view('product.map.index')
            ->with([
                'product' => $product
            ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $productId)
    {
        $path = '';

        try{

            $product = Product::find($productId);
            if( ! $request->hasFile('map')){
                return back()
                    ->with('error', "Select a file");
            }

            if ($product->getMeta('map', false))
            {
                return redirect()
                    ->route('projects_manage')
                    ->with('error', 'Project map already uploaded')
                    ->send();

            }
            $path = FileUpload::uploadFile($request,'uploads','map');

            $meta = Meta::create([
                'key' => 'map',
                'value' => $path
            ]);


            $product->saveMeta($meta);

            return redirect()
                ->route('projects_manage')
                ->with('success', 'Uploaded successfully')
                ->send();
        }
        catch (\Exception $exception)
        {
            unlink($path);
            return back()
                ->with('error', 'Uploaded successfully')
                ->send();

        }

    }

    public function download( $productId)
    {
        $product =Product::find($productId);

        return response()->download(($product->getMeta('map')));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $product = Product::find($id);

        $path = $product->getMeta('map');

        $product->metable()->delete();

       // unlink($path);
        return back()
            ->with('success', 'removed the map successfully')
            ->send();

    }
}
