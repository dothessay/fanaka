<?php

namespace App\Http\Controllers\Product;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Codex\Contracts\ServiceCaller;
use Codex\Services\Products\DeleteProjectExpensesService;

class DeleteProjeExpensesController extends Controller implements ServiceCaller
{
    public function index(Request $request)
    {
        return (new DeleteProjectExpensesService($this))->run($request->all());
    }    

    public function successResponse($message) 
    {
        return back()->with('success', $message);
    }

}
