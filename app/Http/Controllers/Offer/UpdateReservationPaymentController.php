<?php

namespace App\Http\Controllers\Offer;

use App\DB\Accounting\Account;
use App\DB\Sale\OfferLetter;
use App\DB\Sale\OfferPayment;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;

class UpdateReservationPaymentController extends Controller
{
    public function index(Request $request)
    {
        $referenceCode = "";

        if (! is_null($request['mpesa_reference_code'])){

            $referenceCode = $request['mpesa_reference_code'];
        }

        if (! is_null($request['slip_no'])){

            $referenceCode = $request['slip_no'];
        }

        if (! is_null($request['cheque_no'])){

            $referenceCode = $request['cheque_no'];
        }

        $request['reference_code'] = $referenceCode;

       //dd($request->all());

        $data = $request->all();




        $offer = OfferLetter::where('id', $request['id'])->first();
        $payment  = OfferPayment::create([
            'offer_id' => $request['id'],
            'payment_method' => $data['payment_method'],
            'amount' => $data['amount'],
            'user_id' => auth()->id(),
            'reference_code' => $referenceCode,
            'bank_name' => isset($data['bank_name']) ? $data['bank_name'] : "",
            'account_no' => isset($data['account_no']) ? $data['account_no'] : "",
            'cheque_no' => isset($data['cheque_no']) ? $data['cheque_no'] : "",
            'drawer' => isset($data['drawer']) ? $data['drawer'] : "",
            'slip_no' => isset($data['slip_no']) ? $data['slip_no'] : "",
            "deposit_date"  => isset($data['deposit_date']) ? Carbon::parse($data['deposit_date']) : Carbon::now()
        ]);




        /***********CREATE ACCOUNT****************/


        $account = Account::create([
            'user_id' => auth()->id(),
            'amount' => $data['amount'],
            'source' => 'new Offer Deposit',
            'deposit_date' => Carbon::parse($data['deposit_date'])
        ]);


        $offer->account($account);
        $payment->savePayable($account);


        Session::flash('success', "Successfully updated the payment");

        return back();

    }
}
