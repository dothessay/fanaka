<?php

namespace App\Http\Controllers\Offer;

use App\DB\Offer\OfferMeta;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;

class AddOfferNotesController extends Controller
{
    public function index(Request $request)
    {
        $meta = new OfferMeta();

        if (isset($request['offer_id'])){

            $metaObject = OfferMeta::where([
                'offer_id' => $request['offer_id'],
                'key' => 'note'
            ])->first();

            if ($metaObject){
                $meta = $metaObject;

            }
        }



        return view('offer.offer_meta')
            ->with([
                'meta' => $meta,
                'offerId' => $request['offer_id']
            ]);
    }

    public function store(Request $request)
    {

        OfferMeta::updateOrCreate([
            'id' => $request['id'],
            'key' => 'note'
        ],
            [
                'key'  => "note",
                "value" => $request['note'],
                'offer_id'  => $request['offer_id']
            ]);

        Session::flash('success', "Successfully created a note for the offer");

        return back();
    }
}
