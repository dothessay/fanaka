<?php

namespace App\Http\Controllers\Offer;

use App\Customer\Customer;
use App\DB\Accounting\Account;
use App\DB\Offer\OfferUpload;
use App\DB\PrintReservation;
use App\DB\Sale\Installment;
use App\DB\Sale\OfferLetter;
use App\Jobs\SendSmsNotifications;
use App\Sale\Invoice;
use App\Sale\Sale;
use App\Sale\SaleItem;
use App\User;
use Carbon\Carbon;
use Codex\Classes\GeneratePdf;
use Codex\Classes\Handlers\Calculator\CalculatorHandlerFacade;
use Codex\Classes\Handlers\SystemSms;
use Codex\Classes\PdfOutput\DataReport;
use Codex\Classes\PdfOutput\OfferOutput;
use Codex\Classes\PdfOutput\OfferReceipts;
use Codex\Contracts\ServiceCaller;
use Codex\Services\Offer\ListOffer;
use Codex\Services\Offer\StoreOfferService;
use Codex\Services\Plots\ListPlotsService;
use Codex\Services\Sales\ListSalesService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;
use Illuminate\Validation\Rule;

class GenerateOfferController extends Controller implements ServiceCaller
{
    public function index(Request $request)
    {

        $users = User::where('is_active', true)->get();

        if (!auth()->user()->can('access-module-component-functionality', 'sale_manage_show-all-sales')) {

            $request['agent_id'] = auth()->id();

            $users = User::where('id', auth()->id())->get();
        }

        $offers = (new ListOffer($this))->list($request->all());

        $plots = (new ListPlotsService($this))->list([
            'is_reserved' => false,
            'is_sold' => false,
            'can_sell' => true,
        ]);



        $customers = Customer::all();


        return view('offer.index')->with([
            'offers' => $offers,
            'plots' => $plots,
            'users' => $users,
            'customers' => $customers
        ]);

    }


    public function approve($offerId)
    {
        $offer = OfferLetter::where([
            'id' => $offerId,
        ])->first();


        if ($offer) {
            return view('offer.approve')
                ->with([
                    'offer' => $offer
                ]);
        }
        Session::flash('error', 'PLease try again later');

        return back();

    }

    public function approving(Request $request)
    {
        $offerId = $request['offer_id'];

        $offer = OfferLetter::where([
            'id' => $offerId,
            'is_approved' => false,
            'is_converted' => false

        ])->first();


        if ($offer) {


            if (isset($request['reject'])) {
                foreach ($offer->offerItems as $offerItem) {
                    $plot = $offerItem->plot;

                    $plot->is_reserved = false;
                    $plot->is_sold = false;
                    $plot->save();

                }


                $offer->accounts->count() ? $offer->accounts->first()->delete() : null;

                $offer->forceDelete();

                Session::flash('success', 'sale has been canceled');

                return redirect()->route('offer_generate');

            } else {
                $offer->is_approved = true;
                $offer->approved_by = auth()->id();
                $offer->save();


                foreach ($offer->accounts as $payable) {

                    $payable->is_active =  true;

                    $payable->save();

                }


               SystemSms::ReservationApproved($offer);


                Session::flash('success', 'offer successfully approved');
            }


            return redirect()->route('offer_generate');


        }
        Session::flash('error', 'PLease try again later');

        return back();

    }


    public function downloadReceipt($uploadId)
    {
        $upload = OfferUpload::where('id', $uploadId)->first();

        if (Storage::get($upload->file_url)) {
            return Storage::download($upload->file_url);
        }

        return response()->download(($upload->file_url));
    }

    public function store(Request $request)
    {


        $response = $this->validateAjaxForm($request, [
            "first_name" => [
                'required',

            ],
            "last_name" => [
                'required',

            ],

            "middle_name" => [
                'nullable',
                'array'
            ],

            "email" => [
                'nullable',
                Rule::unique('customers')->whereNotNull('email'),
                'array'
            ],
            "id_no" => [

                Rule::unique('customers')->whereNotNull('id_no'),
                'array'

            ],
            "phone_number" => [

                Rule::unique('customers')->whereNotNull('phone_number'),
                'array'
            ],
            'payment_option' => [
                'required'
            ],
            'payment_method' => [
                'required'
            ],
            'amount' => [
                'numeric'
            ]

        ]);


        if (!$response instanceof $request) {
            return $response;

        }


        return (new StoreOfferService($this))->runWithDBTransaction($request->all());

        // Session::flash('success', "successfully written an offer letter");

        //return  back();


    }

    public function convertToSale(Request $request)
    {
        $offer = OfferLetter::where([
            'id' => $request['offer_id'],
        ])->first();


        if ($offer) {
            return view('offer.to_sale')
                ->with([
                    'offer' => $offer
                ]);
        }
        Session::flash('error', 'Please try again later');

        return back();

    }

    public function convertingToSale(Request $request)
    {
        /*$response = $this->validateForm($request, [
            'amount' => 'required',
            'payment_option' => 'required'
        ]);


        if (!$response instanceof $request) {
            return $response;

        }
        */


        try {


            DB::beginTransaction();

            $offer = OfferLetter::where(['id' => $request['offer_id']])->first();

            if ($offer->is_converted){

                throw  new \Exception("Offer Was was converted to sale earlier");
            }

            $amount = 0;
            if(strtolower($offer->payment_option)  == "Cash Option" )
            {


                $months = 1;
                $amount = floatval($offer->cash_option);
            }
            else{
                $months = 6;

                $amountPrincipal = CalculatorHandlerFacade::getRate($months)->calculate( floatval($offer->cash_option), 0);

                foreach ($amountPrincipal as $principal)
                {
                    $amount += $principal['monthlyPayment'];
                }

            }
            //offer add more customers


            $this->addCustomersToOffer($offer, $request);



            //add more payments


            //$this->addOfferPayments($offer, $request);


            //upload receipts


            $this->uploadOfferReceipts($offer, $request);

            $sale = Sale::create([
                'business_code' => $this->businessCode(),
                'user_id' => $offer->user_id,
                'approved_by' => $offer->approved_by,
                'is_approved' => true,
                'agent' => $offer->agent_id,
                'total_amount' => $amount,
                'balance' => (double)$amount - (double)$offer->intialPayment(),
                'title_fee' => true,

            ]);

            foreach ($offer->customers as $customer) {


                $sale->customerSales()->create([
                    'customer_id' => $customer->id,
                ]);

            }



            foreach ($offer->offerItems as $offerItem) {


               if(! SaleItem::where('plot_id' , $offerItem->plot->id)->count()){


                    $sale->saleItems()->create([

                       'business_code' => $this->businessCode(),
                       'plot_id' => $offerItem->plot->id,
                       'price' => $amount,
                       'plot_no' => $offerItem->plot->plot_no,
                       'product_id' => $offerItem->plot->product->id,

                   ]);


                   $plot = (new ListPlotsService($this))->list(['id' => $offerItem->plot->id])->first();


                   $plot->is_reserved = true;
                   $plot->can_sell = false;

                   $plot->save();
               }

            }

            $invoice = Invoice::create([
                'business_code' => $this->businessCode(),
                'user_id' => auth()->id(),
                'sale_id' => $sale->id,
                'customer_id' => $offer->customers->first()->id,
                'total_amount' => $amount,
                'total_paid' => $offer->intialPayment(),
                'due_date' => now()->addRealMonth()
            ]);

            $data['payment_type'] = "installment";



            //create installment


            Installment::create([
                'sale_id' => $sale->id,
                'months' => $months,
                'amount' =>  $amount - $offer->offerPayments->sum('amount')  / 6,
                'completion_date' => Carbon::now()->addRealMonths($months)
            ]);

            $request->request->add(['is_converted' => true]);
            $request['is_conversion'] = true;


            foreach ($offer->offerPayments as $offerPayment) {

                $payment = $sale->salePayments()->create([
                    'payment_type' => $offerPayment->payment_type,
                    'payment_method' => $offerPayment->payment_method,
                    'amount' => $offerPayment->amount,
                    'user_id' => auth()->id(),
                    'reference_code' => $offerPayment->reference_code,
                    'bank_name' => $offerPayment->bank,
                    'account_no' => $offerPayment->account_no,
                    'cheque_no' => $offerPayment->cheque_no,
                    'drawer' => $offerPayment->drawer,
                    'slip_no' => $offerPayment->slip_no,
                    'is_approved'  => true,
                    'created_at' => Carbon::parse($offerPayment->created_at),
                    'deposit_date'   => (isset($offerPayment->deposit_date) &&
                        ! is_null($offerPayment->deposit_date)) ?
                        $offerPayment->deposit_date : $offerPayment->created_at
                ]);



                $account = Account::create([
                    'user_id' => auth()->id(),
                    'amount' => $sale->salePayments->sum('amount'),
                    'source' => 'Transfer from offer to the sale',
                    'is_transfer' => true,
                    'deposit_date'   => $offerPayment->deposit_date,
                ]);


                $sale->account($account);

                $payment->savePayable($account);

            }


            /***********CREATE ACCOUNT****************/




            $offer->is_converted = true;

            $offer->save();


            DB::commit();

            return redirect(route('sale_manage'));

        } catch (\Exception $exception) {

            DB::rollBack();


            Session::flash("error", $exception->getMessage());

            return back();

        }


    }

    private function addCustomersToOffer(OfferLetter $offer, Request $data)
    {
        try {
            if (isset($data['new_customer'])) {
                for ($customerIndex = 0; $customerIndex < sizeof($data['first_name']); $customerIndex++) {
                    $customer = Customer::store([
                        'business_code' => auth()->user()->business->code,
                        'first_name' => $data['first_name'][$customerIndex],
                        'last_name' => $data['last_name'][$customerIndex],
                        'email' => $data['email'][$customerIndex],
                        'phone_number' => $data['phone_number'][$customerIndex],
                        'id_no' => $data['id_no'][$customerIndex],
                        'middle_name' => $data['middle_name'][$customerIndex],
                        'user_id' => auth()->id(),
                    ]);

                    $data['customer_id'] = $customer->id;

                    $offer->offerCustomers()->create([
                        'customer_id' => $customer->id,
                    ]);

                }
            }
        } catch (\Exception $exception) {

            throw new \Exception($exception);
        }


    }

    private function addOfferPayments(OfferLetter $offer, Request $data)
    {

        try {

            if (isset($data['check_bank_name'])) {
                $data['bank_name'] = $data['check_bank_name'];
            }
            if (isset($data['mpesa_reference_code'])) {
                $data['reference_code'] = $data['mpesa_reference_code'];
            }
            if (is_null($data['reference_code'])) {
                $data['reference_code'] = $data['payment_option'];
            }

           $payment =  $offer->offerPayments()->create([
                'payment_method' => $data['payment_option'],
                'amount' => $data['amount'],
                'user_id' => auth()->id(),
                'reference_code' => isset($data['reference_code']) ? $data['reference_code'] : $data['payment_option'],
                'bank_name' => isset($data['bank_name']) ? $data['bank_name'] : "",
                'account_no' => isset($data['account_no']) ? $data['account_no'] : "",
                'cheque_no' => isset($data['cheque_no']) ? $data['cheque_no'] : "",
                'drawer' => isset($data['drawer']) ? $data['drawer'] : "",
                'slip_no' => isset($data['slip_no']) ? $data['slip_no'] : "",
            ]);
            /***********CREATE ACCOUNT****************/


            $account = Account::create([
                'user_id' => auth()->id(),
                'amount' => $data['amount'],
                'source' => 'new Offer Deposit',
            ]);



            $payment->savePayable($account);


            $offer->account($account);

        } catch (\Exception $exception) {

            throw new \Exception($exception);

        }
    }

    private function uploadOfferReceipts(OfferLetter $offer, Request $request)
    {
        if ($request->file('receipts')) {
            foreach ($request->receipts as $index => $agreement) {


                //$path = $request->file('agreement')->store('agreements');

                try {
                    $path = $agreement->store('agreements');

                    //$sale->agreement_url= $path;


                    $offer->uploads()->create([
                        'file_url' => $path,
                        'file_name' => $agreement->getClientOriginalName(),

                    ]);


                } catch (\Exception $exception) {
                    throw new \Exception('could not upload the files . ' . $exception->getMessage());
                }


            }
        }

    }

    public function offerReceipts(Request $request)
    {

        $offer = OfferLetter::where('id', $request['offer_id'])->get();

        $output = (new GeneratePdf())->generate(new OfferReceipts(), $offer);

        return $output;

    }

    public function listSale()
    {

        $sales = (new ListSalesService($this))
            ->getSales([]);
        $saleArray = [];


        foreach ($sales as $sale) {

            if (floatval($sale->getPercentage()) < 50) {
                $saleArray[] = $sale->saleToArray();


            }

        }

        return response()
            ->json([
                'data' => $saleArray
            ]);

    }

    public function generateOfferLetter(Request $request)
    {




        $offerLetter = OfferLetter::where('id', $request['offer_id'])->first();


        $html = OfferOutput::build()->generate($offerLetter);


        return;


        return view('offer._reservation_letter')->with([
            'texts' => $html,
            'style' => OfferOutput::build()->style(),
            'offerId' => $request['offer_id']
        ]);


    }

    public function printOfferLetter(Request $request)
    {



        $html = $request['reservation'];


        if (! PrintReservation::where('offer_letter_id', $request['id'])->count())
        {
            PrintReservation::create([
                'value' => $html,
                'offer_letter_id'  => $request['id'],
                'user_id' => auth()->id(),
            ]);


        }else{
            $reservation  = PrintReservation::where('offer_letter_id', $request['id'])->first();

            $reservation->value = $html;

            $reservation->save();

        }

        $offerLetter = OfferLetter::where('id', $request['id'])->first();


        /*$html = "<p style='line-height: 1cm'><b style='text-align: center ; '>Reservation Letter</b><br/>";
        $html .= OfferOutput::build()->style();
        $html .= $request['reservation'];
        $html .= "</p>";*/


        $html = OfferOutput::build()->generate($offerLetter , $html);

        return;



    }

    public function failureResponse($message)
    {


        if (request()->ajax()) {
            return response()->json([
                'status' => "ERROR",
                'message' => $message,
            ], 500);
        }
        return redirect()->back()->with('error', $message);
    }

    public function successResponse($message)
    {
        return response()
            ->json([
                'message' => "successfully written an offer letter"
            ]);


    }

    private function downloadPdf(OfferLetter $offerLetter)
    {


        return OfferOutput::build()->generate($offerLetter);

    }
}
