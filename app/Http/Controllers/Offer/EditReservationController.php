<?php

namespace App\Http\Controllers\Offer;

use App\Customer\Customer;
use App\DB\Contract;
use App\DB\Customer\NextOfKin;
use App\DB\Offer\OfferMeta;
use App\DB\PrintReservation;
use App\DB\Sale\OfferCustomers;
use App\DB\Sale\OfferLetter;
use App\Traits\IsNextOfKin;
use App\User;
use Codex\Classes\PdfOutput\OfferOutput;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;
use Illuminate\Validation\Rule;
use Spatie\PdfToText\Pdf;

class EditReservationController extends Controller
{

    use  IsNextOfKin;

    public function index(Request $request)
    {
        $offer = OfferLetter::where('id', $request['offer_id'])->first();



        if($reservationOutPut = PrintReservation::where('offer_letter_id', $offer->id)->first()){



            $reservation = $reservationOutPut->value;
        }
        else{
            $contract  = Contract::where('name', 'reservation')->first();

            $reservation = $contract->value;
        }


        $setOutput = (new Contract())->getOfferDetails($offer);


        $reservation = str_replace('#projectName', $setOutput->projectName , $reservation);
        $reservation = str_replace('#puchaser', $setOutput->purchaser , $reservation);
        $reservation = str_replace('#propertyString', $setOutput->propertyString , $reservation);
        $reservation = str_replace('#propetrySelected', $setOutput->propertySelected , $reservation);
        $reservation = str_replace('#reservation_days', $setOutput->getMeta , $reservation);

        $reservation = str_replace('#methodOfPayment', $setOutput->methodOfPayment , $reservation);
        $reservation = str_replace('#cashOption', $setOutput->cashOption , $reservation);
        $reservation = str_replace('#installmentOption', $setOutput->installmentOption  , $reservation);
        $reservation = str_replace('reservationInWord', $setOutput->reservationInWords , $reservation);
        $reservation = str_replace('reservationInFigures', $setOutput->installmentInFigures , $reservation);
        $reservation = str_replace('#', '' , $reservation);
        $reservation = str_replace('#clientName', $setOutput->clientName , $reservation);



        return view('offer.editing')
            ->with([
                'reservation' => $reservation,
                'offer'       => $offer
            ]);

    }


    public function editDetails(Request $request)
    {



        $offer = OfferLetter::where('id', $request['offer_id'])->first();

        $agents = User::where([
            'is_active' => true
        ])->get();



        return view('offer.edit-details')->with([
            'reservation' => $offer,
            'agents'      => $agents,
            'customers'   => Customer::all(),
        ]);

    }


    public function storeKinDetails(Request $request)
    {
        try{

            $offer = OfferLetter::where('id', $request['offerId'])->first();

            $customer = $offer->customers->first();

            $rules = [
                'full_name'  => 'required',
                'phone_number'  => 'required',
                'relation' => 'required'
            ];

            $validator = $this->validateForm($request , $rules);


            if (! $validator instanceof  $request){

                return $validator;
            }

            $data = [
                'full_name'    => $request['full_name'],
                'phone_number' => $request['phone_number'],
                'relation'     => $request['relation']
            ];

            $this->nextOfKin($customer, $data);

            Session::flash("success","Successfully added next of kin");

            return back();


        }catch (\Exception $exception){


            Session::flash("error","Action could not completed, Try again Later ");

            return back();

        }

    }

    public function removeNextOfKin(Request $request)
    {

        NextOfKin::where('id', $request['id'])->forceDelete();
        Session::flash("success","Successfully deleted next of kin");

        return back();


    }


    public function updateDetails(Request $request)
    {
        $offer = OfferLetter::where(['id' => $request['offerId']])->first();



        if (! is_null($offer)) {


            $offer->payment_option = $request['payment_method'];

            $amount = floatval(str_replace(',','',$request['amount']));

            if(strtolower($offer->payment_option) == "cash option"){

                $offer->cash_option = $amount;
                $offer->total_amount = $amount;

            }

            if(strtolower($offer->payment_option) == "installment option"){

                $offer->cash_option = $amount;
                $offer->total_amount = $amount;
            }

            foreach ($offer->offerItems as $offerItem) {

                $offerItem->selling_price = $amount;

                $offerItem->save();


            }




            $offer->agent_id = $request['agent_id'];

            $offer->save();


            if (isset($request['reservation_days']))
            {
                OfferMeta::updateOrCreate([
                    'offer_id' => $offer->id,
                    'key'      => 'reservation_days'
                ], [
                    'offer_id'   => $offer->id,
                    'key'        => 'reservation_days',
                    'value'      => $request['reservation_days']
                ]);
            }


            Session::flash("success","Successfully updated reservation details");

            return back();
        }


        Session::flash("error","We could not complete the action");

        return back();

    }

    public function storeCustomers(Request $request)
    {


        $validator = $this->validateForm($request ,[
            'customer_id'  => [
                'required',
            ]
        ],[
            'customer_id.unique'  => 'Customer already in this reservation',
            'customer.required'    =>  'Please select the customer to continue'
        ]);

        if (! $validator instanceof  $request)
        {
            return $validator;
        }

        $offer = OfferLetter::where('id', $request['offer_id'])->first();



        $offer->offerCustomers()->create([
            'customer_id'  => $request['customer_id']
        ]);

        Session::flash('success', "Customer add successfully");

        return back();

    }

    public function removeCustomers(Customer $customer , OfferLetter $offer)
    {

        OfferCustomers::where([
            'customer_id'  => $customer->id,
            'offer_letter_id'     => $offer->id
        ])->delete();



        Session::flash('success', "Customer removed successfully");

        return back();

    }



}

