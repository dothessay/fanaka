<?php

namespace App\Http\Controllers\Offer;

use Codex\Contracts\ServiceCaller;
use Codex\Services\Offer\StoreOfferService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;
use Illuminate\Validation\Rule;

class StoreOfferController extends Controller implements ServiceCaller
{
    public function store(Request $request)
    {


/*
        $response = $this->validateForm($request, [
            "first_name" => [
                'required',

            ],
            "last_name" => [
                'required',

            ],

            "middle_name" => [
                'nullable',
                'array'
            ],

            "email" => [
                'nullable',
                Rule::unique('customers')->whereNotNull('email'),
                'array'
            ],
            "id_no" => [

                Rule::unique('customers')->whereNotNull('id_no'),
                'array'

            ],
            "phone_number" => [

                Rule::unique('customers')->whereNotNull('phone_number'),
                'array'
            ],
            'payment_option' => [
                'required'
            ],
            'payment_method' => [
                'required'
            ],
            'amount' => [
                'numeric'
            ]

        ]);




        if(! $response instanceof  $request)
        {
            return $response;

        }


*/


        return  (new StoreOfferService($this))->runWithDBTransaction($request->all());

    }

    public function failureResponse($message)
    {

        Session::flash("error", $message);

        return back();

        /*return response()->json([
                'status' => "ERROR",
                'message' => $message,
            ] , 500);*/

    }

    public function successResponse($message)
    {

        Session::flash("success", "successfully made a reservation");

        return back();

        /*
        return response()
            ->json([
                'message'  => "successfully written an offer letter"
            ]);*/


    }
}
