<?php

namespace App\Http\Controllers\Finance\Requisition;

use App\DB\Account\PettyCashRequisitionRequest;
use Codex\Classes\Repository\RequisitionRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class RequisitionController extends Controller
{

    private $requisition;


    public function __construct()
    {
        $this->requisition = new RequisitionRepository();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $requisitions = $this->requisition->all();

        return  view('account.requisition.browse')
            ->with([
                'requisitions'  => $requisitions
            ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {


        return  view('account.requisition.create')
            ->with([
                'requisition'  => new PettyCashRequisitionRequest()
            ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       $response =  $this->validateForm($request ,[
           'amount'  => 'required',
           'reason'  => 'required'
       ]);

       if (! $response instanceof  $request)
       {
           return  $response;
       }

        $requisition = $this->requisition->store($request);

        session()
            ->flash('disbursed');

        return redirect()->route('finance_requisition_show', ['requisition' => $requisition->id]);

    }
    public function approve(PettyCashRequisitionRequest $requisition , Request $request)
    {

        $this->requisition->approve($request , $requisition);


        session()
            ->flash('disbursed');

        return redirect()->route('finance_requisition_show', ['requisition' => $requisition->id]);
    }

    public function reject(PettyCashRequisitionRequest $requisition , Request $request)
    {

        $this->requisition->reject($request , $requisition);


        session()
            ->flash('disbursed');

        return redirect()->route('finance_requisition_show', ['requisition' => $requisition->id]);

    }

    public function disburse(PettyCashRequisitionRequest $requisition , Request $request)
    {

        $this->requisition->disburse($request , $requisition);

        session()
            ->flash('disbursed');

        return redirect()->route('finance_requisition_show', ['requisition' => $requisition->id]);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(PettyCashRequisitionRequest $requisition)
    {
        return  view('account.requisition.read')
            ->with([
                'requisition'  => $requisition
            ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(PettyCashRequisitionRequest $requisition)
    {

        return  view('account.requisition.create')
            ->with([
                'requisition'  => $requisition
            ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, PettyCashRequisitionRequest $requisition)
    {
        $this->requisition->update($request, $requisition);
        session()
            ->flash('updated');

        return redirect()->route('finance_requisition_show', ['requisition' => $requisition->id]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
