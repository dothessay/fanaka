<?php

namespace App\Http\Controllers\Finance\Expenses;

use App\DB\Exepense\PettyCash;
use App\DB\Exepense\PettyCashLog;
use App\DB\Expense\Expense;
use App\DB\Finance\ChartOfAccount;
use App\DB\Product\Product;
use App\DB\Product\ProjectExpense;
use Carbon\Carbon;
use Codex\Contracts\ServiceCaller;
use Codex\Services\Expenses\ListExpensesService;
use Faker\Factory;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;

class ExpensesController extends Controller implements ServiceCaller
{
    //

    public function index(Request $request)
    {
        $projects = Product::all();
        $expenses = collect();



        $product = new Product();

        if (isset($request['project']) )
        {
            foreach (ProjectExpense::where('project_id', $request['project'])->get() as $ProjectExpense)
            {

                $expense = [
                    'id' => $ProjectExpense->id,
                    'amount' => $ProjectExpense->amount,
                    'purpose' => $ProjectExpense->purpose,
                    'date'    => $ProjectExpense->created_at,
                    'evidence' => Storage::url($ProjectExpense->evidence)
                ];

                $expenses->push($expense);
            }

            $product = Product::where('id', $request['project'])->first();
            if (! $product)
            {
                Session::flash('error', "Project not found");
                return back();
            }
        }


        $petties = PettyCashLog::all()->map(function ($petty) {
            return $petty->pettyToArray();
        });
        $options= [];

        $options['start_date'] = isset($request['start_date']) ? $request['start_date'] : Carbon::now()->startOfMonth();
        $options['end_date'] = isset($request['end_date']) ? $request['end_date'] : Carbon::now()->endOfMonth();



        $officeExpenses =  (new ListExpensesService($this))->list($request->all(), $options);

        return view('account.finance.expenses.index')
            ->with([
                'projects'  => $projects,
                'expenses'  => $expenses,
                'product'   => $product,
                'petties'   => $petties,
                'officeExpenses' => $officeExpenses,
                'charts'  => ChartOfAccount::all()

            ]);
    }

    public function store(Request $request)
    {
        $validator = $this->validateForm($request ,[
            'amount' => 'required|numeric',
        ]);

        if (! $validator instanceof  $request)
        {
            return $validator;
        }
        $expense = new ProjectExpense();
        $expense->project_id = $request['project_id'];
        $expense->amount = $request['amount'];
        $expense->purpose = $request['purpose'];
        $expense->user_id = auth()->id();


        if ($request->hasFile('evidence'))
        {
            $path = $request->file('evidence')->store('/uploads');
            $expense->evidence  = $path;


        }
        $expense->save();

        Session::flash('success',' Saved Expensed successfully');

        return back();

    }

    public function downloadEvidence(ProjectExpense $expense)
    {
        return Storage::download($expense->evidence);

    }

    public function showPetty($id)
    {
        $petty = PettyCash::find($id);

        return view('account.finance.expenses.show_petty')->with(['petty' => $petty]);

    }

    public function showExpenses($id)
    {
        $expense = Expense::find($id);

        return view('account.finance.expenses.show_expenses')->with(['expense' => $expense]);

    }

    public function successResponse($message)
    {
        // TODO: Implement successResponse() method.
    }
}
