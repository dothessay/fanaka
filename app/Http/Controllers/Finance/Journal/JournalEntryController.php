<?php

namespace App\Http\Controllers\Finance\Journal;

use App\DB\Finance\ChartOfAccount;
use App\DB\Finance\JournalEntry;
use App\Traits\IsJournable;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;

class JournalEntryController extends Controller
{
    use IsJournable;

    public function index(Request $request)
    {
        $startDate = isset($request['startDate']) ? Carbon::parse($request['startDate']) : now()->startOfWeek();
        $endDate = isset($request['endDate']) ? Carbon::parse($request['endDate']) : now()->endOfWeek();
        $journals = JournalEntry::whereBetween('created_at', [
            $startDate , $endDate
        ])->get();

        return view('account.journal.index')
            ->with([
                'journals'  => $journals
            ]);

    }

    public function create()
    {
        return view('account.journal.create')
            ->with([
                'charts'   =>  ChartOfAccount::all(),
            ]);

    }

    public function store(Request $request)
    {
        foreach ($request->debit_account as $key => $value)
        {
            //debit account
            $this->addToJournal([
                'chart_id'  => $request['debit_account'][$key],
                'debit'  => $request['debit_amount'][$key],
                'credit'  => 0
            ]);

            $this->addToJournal([
                'chart_id'  => $request['credit_account'][$key],
                'debit'     => 0,
                'credit'    => $request['credit_amount'][$key]
            ]);
        }
        Session::flash('success','Successfully created a journal entry');

        return redirect(route('finance_journal-entry_index'));
    }
}
