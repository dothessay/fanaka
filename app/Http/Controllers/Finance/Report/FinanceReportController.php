<?php

namespace App\Http\Controllers\Finance\Report;

use App\DB\Product\Product;
use Carbon\Carbon;
use Codex\Classes\PdfOutput\DataReport;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class FinanceReportController extends Controller
{

    public function index()
    {
        return view('account.finance.reports.index');
    }

    public function analytics()
    {
        return view('account.finance.reports.analytics.index');
        
    }

    public function collections(Request $request)
    {
        $startDate = isset($request['start_date']) ? Carbon::parse($request['start_date']) : now()->startOfYear();
        $endDate = isset($request['end_date']) ? Carbon::parse($request['end_date']) : now()->endOfYear();

        $total = [];

        if (isset($request['project_id'])) {

            foreach ($request['project_id'] as $projectId) {

                $project = Product::where('id', $projectId)->first();

                $salePayments = $project->salePayments
                    ->whereBetween('created_at', [$startDate, $endDate])
                    ->groupBy(function ($p) {
                        return Carbon::parse($p->created_at)->format('Y-F');
                    });
                foreach ($salePayments as $index => $payment) {
                    $total[$project->name()][] = [
                        'months' => $index,
                        'payments' => $payment
                    ];
                }


            }
        }
        


        if (isset($request['pdf']))
        {

            $html = view('account.finance.reports.collections._collection_data')->with(['totals' => $total])->render();

            return  (new DataReport())->outPut($html , 'Project Collection Report');
        }



        return view('account.finance.reports.collections.index')
            ->with([
                'projects' => Product::all(),
                'totals'    => $total,
            ]);

    }

}
