<?php

namespace App\Http\Controllers\Finance\Chart;

use App\DB\Finance\ChartOfAccount;
use App\DB\Finance\ExpenseType;
use Codex\Classes\Handlers\Imports\ChartOfAccountImport;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;
use Illuminate\Validation\Rule;
use Maatwebsite\Excel\Facades\Excel;

class ChartOfAccountController extends Controller
{
    public function index()
    {

        return view('account.finance.charts.index')
            ->with([
                'charts'  => ChartOfAccount::all()
            ]);
    }


    public function show($id)
    {
        $chart = ChartOfAccount::find($id);

        return view('account.finance.charts.show')
            ->with([
                'chart'   => $chart
            ]);

    }


    public function edit($id)
    {
        $chart = ChartOfAccount::find($id);

        return view('account.finance.charts.create')
            ->with([
                'chart'   => $chart
            ]);

    }

    public function createChart()
    {
        return view('account.finance.charts.create');
    }


    public function getCharts()
    {
        return response()
            ->json([
                'data' => ChartOfAccount::all()
                    ->map(function ($q) { return $q->chartToArray() ; } )
            ]);

    }

    public function storeChart(Request $request)
    {
        if ( $request->hasFile('charts'))
        {
            Excel::import(new ChartOfAccountImport(),request()->file('charts'));
            Session::flash('success','successfully imported the values');
            return back();
        }

        try{
            $validator = $this->validateForm($request , [
                'name' => [
                    'required',
                    Rule::unique('chart_of_accounts','name')
                ],
                'code' => [
                    'nullable',
                    Rule::unique('chart_of_accounts','code')
                ],
                'type' => [
                    'required',
                ]
            ],[
                'name.required'     => 'kindly provide name of account type',
                'name.unique'       => 'That account type exist',
                'code.required'     => 'Kindly provide code',
                'code.unique'       => 'That code type exist',
            ]);

            if (! $validator instanceof $request)
            {
                return $validator;
            }
            ChartOfAccount::create([
                'name'            => $request->name,
                'code'            => $request->code,
                'type'            => $request->type,
            ]);

            Session::flash('success', 'Successfully created a account');

            return back();

        }catch (\Exception $exception)
        {

            Session::flash('error', 'Successfully created a account');

            return back();
        }

    }public function update(Request $request , $id)
    {

        try{
            $validator = $this->validateForm($request , [
                'name' => [
                    'required',
                    Rule::unique('chart_of_accounts','name')->ignore($id)
                ],
                'code' => [
                    'required',
                    Rule::unique('chart_of_accounts','code')->ignore($id)
                ],
                'type' => [
                    'required',
                ]
            ],[
                'name.required'     => 'kindly provide name of account type',
                'name.unique'       => 'That account type exist',
                'code.required'     => 'Kindly provide code',
                'code.unique'       => 'That code type exist',
            ]);

            if (! $validator instanceof $request)
            {
                return $validator;
            }
            $chart = ChartOfAccount::where('id', $id)->first();
            $chart->name = $request->name;
            $chart->code = $request->code;
            $chart->type = $request->type;
           $chart->save();

            Session::flash('success', 'Successfully created a account');

            return back();

        }catch (\Exception $exception)
        {

            Session::flash('error', 'Successfully created a account');

            return back();
        }

    }
    public function getType()
    {
        return response()
            ->json([
                'data' => ExpenseType::all()
                ->map(function ($q) { return $q->typeToArray() ; } )
            ]);

    }
    public function storeType(Request $request)
    {
        $validator = $this->validateAjaxForm($request , [
            'name' => [
                'required',
                Rule::unique('expense_types','name')
            ]
        ],[
            'name.required'  => 'kindly provide name of account type',
            'name.unique'  => 'That account type exist',
        ]);

        if (! $validator instanceof $request)
        {
            return $validator;
        }
        ExpenseType::create([
            'name' => $request->name
        ]);

        return response()
            ->json([
                'message' => 'Successfully created a account Type'
            ]);

    }

    public function updateType(Request $request)
    {
        $validator = $this->validateAjaxForm($request , [
            'name' => [
                'required',
                Rule::unique('expense_types','name')->ignore($request->id)
            ]
        ],[
            'name.required'  => 'kindly provide name of account type',
            'name.unique'  => 'That account type exist',
        ]);

        if (! $validator instanceof $request)
        {
            return $validator;
        }
        $type = ExpenseType::where([
            'id' => $request->id
        ])->first();

        $type->name = $request->name;
        $type->save();

        return response()
            ->json([
                'message' => 'Successfully created a account Type'
            ]);

    }

    public function destroyType(Request $request)
    {
        $type = ExpenseType::where([
            'id' => $request->id
        ])->forceDelete();

        return response()
            ->json([
                'message' => 'Successfully delete a account Type'
            ]);
    }

}
