<?php

namespace App\Http\Controllers\Account;

use App\DB\Accounting\Account;
use Carbon\Carbon;
use Codex\Classes\PdfOutput\DataReport;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;

class ListAccountController extends Controller
{
    public function index(Request $request)
    {
        $startDate = isset($request['start_date']) ? Carbon::parse($request['start_date']) : Carbon::now()->startOfDay();
        $endDate = isset($request['end_date']) ? Carbon::parse($request['end_date']) : Carbon::now()->endOfDay();


        $where = [];


        if (isset($request['source']) && !is_null($request['source'])){

            $where['source'] = $request['source'];
        }
        if (isset($request['accountable_type']) && !is_null($request['accountable_type'])){

            $where['accountable_type'] = $request['accountable_type'];
        }


        $accounts = Account::where($where)
        ->whereBetween('created_at',[
            $startDate , $endDate
        ])->get();


        if (isset($request['get-account-pdf'])){

            $html =  view('account._data')->with([
                'accounts' => $accounts
            ])->render();



            $reportName = "Report between {$startDate->format('d-F-Y')} To {$endDate->format('d-F-Y')}";


            return (new DataReport())->outPut($html ,$reportName);


        }
        return view('account.index')->with([
            'accounts' => $accounts
        ]);

    }

    public function delete(Account $account)
    {
        $account->delete();


        Session::flash('success', "Account Removed successfully");

       return back();
    }
}
