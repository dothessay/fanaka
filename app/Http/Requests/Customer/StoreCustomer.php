<?php

namespace App\Http\Requests\Customer;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class StoreCustomer extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "first_name" => [
                'required',
            ],
            "last_name" => [
                'required',
            ],

            "middle_name" => [
                'nullable',

            ],

            "email" => [
                'nullable',
                Rule::unique('customers')->whereNotNull('email')
            ],
            "id_no" => [
                'nullable',
                Rule::unique('customers')->whereNotNull('id_no')
            ],
            "phone_number" => [
                'nullable',
                Rule::unique('customers')->whereNotNull('phone_number')
            ],
        ];
    }
}
