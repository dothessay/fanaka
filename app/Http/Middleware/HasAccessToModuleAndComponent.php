<?php

namespace App\Http\Middleware;

use Carbon\Carbon;
use Closure;
use Codex\Classes\Helper;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Session;

class HasAccessToModuleAndComponent
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @param String $moduleName The module to check access for
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        $module = Helper::routeModule();

        $moduleComponent = Helper::routeModuleComponent();

        if (\session()->has('new_user'))
        {
            auth()->loginUsingId(\session('new_user'));
        }


        if (Gate::allows('access-module', $module) &&
            Gate::allows('access-module-component', Helper::componentFullName($module, $moduleComponent))
        ) {



            if (is_null(auth()->user()->checked_in_at) && now()->isToday()){

               Session::flash('error', "You must check in to continue");
                auth()->logout();
                return redirect('/');

            }


            if (now()->isToday() && Carbon::parse(auth()->user()->checked_in_at)->isToday())
            {
                return $next($request);
            }


            Session::flash('error', "You must check in to continue");
            auth()->logout();
            return redirect('/');


        }

        return redirect('/');
    }
}
