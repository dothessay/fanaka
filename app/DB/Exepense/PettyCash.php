<?php

namespace App\DB\Exepense;
use App\DB\Finance\ChartOfAccount;
use App\DB\Finance\JournalEntry;
use App\User;
use Codex\Classes\Model;

class PettyCash extends Model
{

    protected $guarded = [];

    public function path()
    {
        return route('finance_expenses_show-petty', ['petty' => $this->id]);

    }



    public function user()
    {
        return $this->belongsTo(User::class, 'user_id','id');

    }


    public function chart()
    {
        return $this->belongsTo(ChartOfAccount::class,'chart_id','id');

    }


    public function journalable()
    {
        return $this->morphMany(JournalEntry::class ,'journalable');

    }

    public function loggable()
    {
        return $this->morphMany(PettyCashLog::class ,'loggable');

    }

    public function addLoggable(PettyCashLog $cashLog)
    {
        return $this->loggable()->save($cashLog);
    }

    public function addjournalable(JournalEntry $entry)
    {
        return $this->journalable()->save($entry);
    }

    public function pettyToArray()
    {

        $balance = $this->credit - $this->debit;

        return [
            'id'  => $this->id,
            'user' => $this->user->fullName(),
            'credit'  => $this->credit,
            'debit'    => $this->debit,
            'balance'  =>  ($balance < 1 ) ? ($balance * -1) : $balance,
        ];

    }
}
