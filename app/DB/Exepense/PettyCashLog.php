<?php

namespace App\DB\Exepense;
use App\User;
use Codex\Classes\Model;

class PettyCashLog extends Model
{


    protected $guarded = [];


    public function loggable()
    {
        return $this->morphTo();

    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id','id');

    }

    public function pettyToArray()
    {

        return [
            'id'  => $this->id,
            'user' => $this->user->fullName(),
            'credit'  => $this->credit,
            'debit'    => $this->debit,
            'note'    => $this->note,
            'balance'   =>  number_format($this->balance ,2),
            'date'   => $this->created_at->format('Y-F-d H:i:s')

        ];

    }
}
