<?php

namespace App\DB\Setting;


use App\DB\Role\Business;
use App\Setting\PlotSizeSetting;
use Codex\Classes\Model;
use Illuminate\Support\Facades\DB;

class Setting extends Model
{
    protected $guarded = [];


    public function setting($name)
    {
        return isset($this->where('key',$name)->first()->value) ? $this->where('key',$name)->first()->value : "";
    }

    public static function add($requestData, $businessCode, array $data = [])
    {


        try {
            DB::beginTransaction();

                foreach ($requestData as $key => $item) {

                    self::updateOrCreate([
                        'key' => $key,
                        'business_code' => $businessCode,
                    ], [
                        'key' => $key,
                        'business_code' => $businessCode,
                        'value' => $item
                    ]);

                    if (isset($data) && sizeof($data) > 0) {


                        $business = Business::where('code', $businessCode)->first();


                        $business->name = $data['name'];
                        $business->address = $data['address'];
                        $business->slug = $data['slug'];
                        $business->logo = isset($data['image']) ? $data['image'] : $business->logo;
                        $business->save();



                        $plotSizes = explode(',', $data['plot_sizes']);

                        foreach ($plotSizes as $plotSize) {

                            PlotSizeSetting::updateOrCreate([
                                'label' => $plotSize,
                                'business_code' => $businessCode,
                            ], [

                                'business_code' => $businessCode,
                                'label' => $plotSize
                            ]);
                        }
                    }

                }
                DB::commit();


            return "successfully updated setting";
        }catch (\Exception $exception)
        {
            DB::rollback();
            return $exception->getMessage();

        }

    }


}
