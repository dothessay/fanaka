<?php

namespace App\DB\Plot;

use App\Plot\Plot;
use App\User;
use Carbon\Carbon;
use Codex\Classes\Model;

class PlotHoldingRequest extends Model
{
    protected $guarded = [];

    protected $casts = [
        'is_approved'  => 'boolean'
    ];
    public function getCustomerNameAttribute($value)
    {
        return $this->attributes['customer_name'] = ucwords(strtolower($value));
    }

    public function plot()
    {
        return $this->belongsTo(Plot::class);

    }

    public function user()
    {
        return $this->belongsTo(User::class);

    }
    public function approvedBy()
    {
        return $this->belongsTo(User::class,'approved_by','id');

    }

    public function daysRemaining()
    {
        return (Carbon::parse($this->release_at)->endOfDay()->diffInDays(now()) < 0 )
            ? "Released"
            : (Carbon::parse($this->release_at)->endOfDay()->diffInDays(now()) === 0 )
                ? "Today"
                 : Carbon::parse($this->release_at)->diffInDays(now()->startOfDay());

    }
    public function isReleased()
    {
        if ($this->plot->is_sold){

            return   "<span class='label label-danger'>Sold</span>" ;
        }

        if (Carbon::parse($this->release_at)->endOfDay()->lessThan(now()))
        {
            return  "<span class='label label-success'>Released</span>" ;

        }

        return   "<span class='label label-danger'>Not Released</span>" ;

    }

    public function status()
    {
        return $this->is_approved == true  ?  "<span class='label label-warning'>Approved</span>" : "<span class='label label-danger'>Pending</span>";
    }
}
