<?php

namespace App\DB\Plot;

use App\DB\Product\Product;
use App\Setting\PlotSizeSetting;
use Carbon\Carbon;
use Codex\Classes\Model;

class ProjectPricing extends Model
{
    protected $guarded = [];


    public function project()
    {
        return $this->belongsTo(Product::class ,'project_id','id');

    }

    public function size()
    {
        return $this->belongsTo(PlotSizeSetting::class ,'size_id','id');

    }

    public function getOfferStatus()
    {
        if ($this->payment_option == 'offer'){

            if (Carbon::parse($this->end_at)->endOfDay()->lessThan(now()))
            {
                return false;
            }
            return true;

        }
        return false;
    }

}
