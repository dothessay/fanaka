<?php

namespace App\DB\Expense;
use App\DB\Exepense\PettyCashLog;
use App\DB\Finance\ChartOfAccount;
use App\DB\Finance\JournalEntry;
use App\User;
use Codex\Classes\Model;


class Expense extends Model
{
    protected $guarded = [];

    public function path()
    {
        return route('finance_expenses_show-expense', ['expense' => $this->id]);

    }

    public function user()
    {
        return $this->belongsTo(User::class,'user_id','id');

    }

    public function chart()
    {
        return $this->belongsTo(ChartOfAccount::class,'chart_id','id');

    }

    public function loggable()
    {
        return $this->morphMany(PettyCashLog::class ,'loggable');

    }
    public function journalable()
    {
        return $this->morphMany(JournalEntry::class ,'journalable');

    }

    public function addLoggable(PettyCashLog $cashLog)
    {
        return $this->loggable()->save($cashLog);
    }

    public function addjournalable(JournalEntry $entry)
    {
        return $this->journalable()->save($entry);
    }
}
