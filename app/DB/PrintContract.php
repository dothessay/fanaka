<?php

namespace App\DB;

use App\Sale\Sale;
use Carbon\Carbon;
use Codex\Classes\Model;


class PrintContract extends Model
{
    protected $guarded = [];


    public function sale()
    {
        return $this->belongsTo(Sale::class, "sale_id", "id");

    }


    public function getAgreement()
    {
        return $this->setHeader() . '' . $this->value . ' ' . $this->setFooter();

    }

    public function setHeader()
    {
        foreach ($this->sale->saleItems as $saleItem) {

            $plotNo[] = $saleItem->plot->getPropertyDetails();
            $plotSize[] = $saleItem->plot->getPropertyDetails();
            $plotDetails = '';

        }


        $plotDetails = $plotDetails . ' ' .    collect($plotNo)->implode(',', '');;

        $logo = url('assets/img/fanaka_logo.png');
        $firstPage = "<style> .body{ 
                  font-family: serif; 
                  font-size: 12px;
                   
                  }</style>
                   <div class='body'> <p style=\"font-weight: bold; font-size: 12pt; align-content: center; text-align: center\">
                  
                     <img src='" . $logo . "' style=\"width: 100%;height: auto; max-height: 70px\"alt=\"Fanaka Real Estate\"/>
     
                    <p style='text-align: center; font-family: serif;  font-weight: bolder; font-size: 16px; '>DATED….." . Carbon::parse($this->created_at)->format('Y-F-d') . "	
                                                 <br/>
                                                 <br/>
                                                 <br/>
                                                 <br/>
                                                 <br/>
                                                 <br/>
                                                 <br/>AGREEMENT FOR SALE
                                                  <br/>
                                                 <br/>
                                                 <br/>
                                                 <br/>
                                                 <br/>
                                                 <br/>
                                                 <br/>FANAKA REAL ESTATE LTD
                                                 <br/>
                                                 <br/>
                                                 <br/>
                                                 <br/>
                                                 <br/>
                                                 (Vendor)
                                                    <br/>
                                                 <br/>
                                                 <br/>
                                                 <br/>
                                                 <br/>
                                                 <br/>
                                                   <br/>{$this->sale->getCustomerDetails()}
                                                   
                                                    <br/>
                                                 <br/>
                                                 <br/>
                                                 
                                                   <br/>(Purchaser)
                                                 <br/>
                                                 <br/>
                                                 <br/>
                                                 <br/>
                                                 <br/>
                                                 <br/>
                                                   <br/>{$plotDetails}                                           
                                                   
                         </p>";

        $firstPage .= "<p style='text-align: center ; letter-spacing: 3px'>
      <h1 style='align-content: center ; text-align: center'>
      <b>REPUBLIC OF KENYA</b></h1>
       <br/>
       <br/>
      
       </p> 
       
       <p  style='align-content: center ; text-align: center'>
        <b><u style='line-height: 1cm;'>SALE AGREEMENT</u></b></p>";


        return $firstPage;


    }

    public function setFooter()
    {

        ob_start();
        ?>
        <p style='text-align: justify ;  margin-top: 50%'>

        </p>
        <br/><br/>VENDOR<br/><br/><br/>
        Fanaka Real Estate Limited,<br/>
        Dune Plaza, Suite D1 , Ruai Town, <br/>
        P.o Box 252-00222 , Nairobi, <br/>
        Tel: +254 799 000 111, <br/>
        Email: info@fanaka.co.ke
        <br/>
        <br/>

        DIRECTOR
      <br/>
            NAME: MOSES MURIITHI KIHUNII …………………………… <br/>
            ID NO: 25876155 <br/>
            <br/>

            <?php print  $this->sale->signatories() ?><br/>


            <?php
            foreach ($this->sale->witnesses as $witness)
            {

            if ($witness->type === 'advocate') {
            $title = "<br/>SIGNED BY THE ADVOCATE<br/>";
            } else {
            $title = "<br/>SIGNED BY THE WITNESS<br/>";
            }

            print $title . "<br/>

                <b>NAME: </b> {$witness->fullname()} ………………………………. <br/>
                <b>ID NO: </b>{$witness->id_no} <br/>";

            }


            if ($this->sale->getHasNextOfKin()) {

             print "<br/>Next Of Kin<br/>" . $this->sale->getNextOfKin();
            }

        return ob_get_clean();


    }

    private function styles()
    {

    }

}
