<?php

namespace App\DB\Role;

use App\DB\Setting\Setting;
use App\Setting\PlotSizeSetting;
use App\Traits\IsPermissible;
use App\User;
use Codex\Classes\System;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Business extends Model
{
    use  IsPermissible;


    protected $guarded = [];


    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);

        $this->permissionRelation = 'business';
    }

    public function roles()
    {
        return $this->hasMany(Role::class, 'business_code', 'code');
    }

    public function users()
    {
        return $this->hasMany(User::class, 'business_code', 'code');
    }
    public function plotSizeSettings()
    {
        return $this->hasMany(PlotSizeSetting::class, 'business_code', 'code');
    }



    public static function add($business ,  $optionals = null)
    {
        $savedBusiness = DB::transaction(function () use ($business, $optionals) {

            if ($business instanceof Business) {
                $savedBusiness = Business::create($business->toArray());
            }

            if (is_array($business)) {
                $savedBusiness = Business::create($business);
            }

            $businessCode = $savedBusiness->code;


            PlotSizeSetting::create([
                'business_code' => $businessCode,
                'label' => "40x80"
            ]);

            PlotSizeSetting::create([
                'business_code' => $businessCode,
                'label' => "50x100"
            ]);



            return $savedBusiness;
        });//END Of Database Transactions

        return $savedBusiness;

    }

    public function settings()
    {
        return $this->hasMany(Setting::class,'business_code','code');

    }

    public function setting($name)
    {
        return isset($this->settings()->where('key',$name)->first()->value) ? $this->settings()->where('key',$name)->first()->value : "";
    }

    public function getNameAttribute($value)
    {
        return $this->attributes['name'] = ucwords(strtolower($value));
    }

    public function getAddressAttribute($value)
    {
        return $this->attributes['address'] = ucwords(strtolower($value));
    }


}
