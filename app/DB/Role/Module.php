<?php

namespace App\DB\Role;

use App\Traits\CanPermit;
use Illuminate\Database\Eloquent\Model;

class Module extends Model
{
    use CanPermit;


    protected $guarded = [];

    public function components()
    {
        return $this->hasMany(Component::class , 'module_id','id');

    }

}
