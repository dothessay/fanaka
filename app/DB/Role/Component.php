<?php

namespace App\DB\Role;

use App\Traits\CanPermit;
use Illuminate\Database\Eloquent\Model;

class Component extends Model
{
    use CanPermit;


    protected $guarded = [];

    public function functionalities()
    {
        return $this->hasMany(Functionality::class , 'component_id','id');

    }
    public function module()
    {
        return $this->belongsTo(Module::class , 'module_id','id');

    }

}
