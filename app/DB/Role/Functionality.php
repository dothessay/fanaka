<?php

namespace App\DB\Role;

use App\Traits\CanPermit;
use Illuminate\Database\Eloquent\Model;

class Functionality extends Model
{
    use  CanPermit;


    protected $guarded = [];



    public function component()
    {
        return $this->belongsTo(Component::class , 'component_id','id');

    }
}
