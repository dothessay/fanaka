<?php

namespace App\DB\Role;

use App\Traits\IsPermissible;
use App\User;
use Codex\Classes\Model;

class Role extends Model
{
    use IsPermissible;

    protected $guarded = [];



    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);

        $this->permissionRelation = 'role';
    }

    public function users()
    {
        return $this->hasMany(User::class, 'role_id','id');

    }



    public function path()
    {
        return route('users_role_details',['role' => $this->id]);

    }
}
