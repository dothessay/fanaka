<?php

namespace App\DB\Business;
use App\DB\Role\Role;
use App\User;
use Codex\Classes\Model;

class DepartmentAdmin extends Model
{
    protected $guarded = [];

    public function user()
    {
        return $this->belongsTo(User::class , 'user_id',"id");

    }
    public function role()
    {
        return $this->belongsTo(Role::class , 'role_id',"id");

    }
}
