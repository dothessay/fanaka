<?php

namespace App\DB;

use App\Customer\Customer;
use App\Sale\Sale;
use Codex\Classes\Model;

class Witness extends Model
{
    protected $guarded = [];

    public function sale()
    {
        return $this->belongsTo(Sale::class,'sale_id','id');

    }

    public function fullName()
    {
        return ucwords($this->name);
    }
}
