<?php

namespace App\DB\Accounting;

use App\Sale\Sale;
use App\User;
use Carbon\Carbon;
use Codex\Classes\Model;
use Codex\Scopes\AccountLog;
use Codex\Scopes\IsActiveScope;
use Illuminate\Database\Eloquent\SoftDeletes;
use phpDocumentor\Reflection\Types\Parent_;

class Account extends Model
{

    use  SoftDeletes;

    protected $guarded = [];

    protected $casts = [
        'amount' => 'double',
        'is_Active' => 'boolean',
    ];


    public $timestamps = true;

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);


        if ( ! isset($this->ccountable)){

            $this->forceDelete();



        }
    }

    public static  function boot()
    {
        parent::boot();

        $account = new self();


        if (is_null($account->accountable())) {
            self::forceDelete();

        }



        static::addGlobalScope(new  AccountLog());

        //static::addGlobalScope(new IsActiveScope());


    }

    public function accountable()
    {
        return $this->morphTo();
    }

    public function payable()
    {
        return $this->morphTo()->withoutGlobalScopes();
    }

    public function setSourceAttribute($value)
    {
        return $this->attributes['source'] = ucwords(strtolower($value));

    }
    public function getCreatedAtAttribute($value)
    {
        return $this->attributes['created_at'] = Carbon::parse($value)->format('Y-m-d H:i');

    }

    public function getSourceAttribute($value)
    {
        return  $this->attributes['source'] = ucwords(strtolower($value));

    }

    public function scopeApproved($query)
    {
        return $query;

        //return $query->where('is_active', true);


    }

    public function getAmountAttribute($value)
    {

        $accaunts = $this->accountable? $this->accountable->payments() : collect(); //->where('created_at', 'Like', "%{$this->created_at}%");

        foreach ($accaunts as $accaunt) {

            if (Carbon::parse($accaunt->created_at)->format('Y-m-d H:i') == $this->created_at){


                if ($value != $accaunt->amount){

                    return $this->attributes['amount'] = floatval($accaunt->amount);
                }
            }

        }

        return  $this->attributes['amount'] = floatval($value);

    }






    public function user()
    {
        return $this->belongsTo(User::class,'user_id','id');

    }



    public function getPaymentMethod()
    {
        //dd($this->created_at);


        return $this->payable ? $this->payable->payment_method: null;
        /*$accaunts = $this->accountable? $this->accountable->payments() : collect(); //->where('created_at', 'Like', "%{$this->created_at}%");

        foreach ($accaunts as $accaunt) {

            if (Carbon::parse($accaunt->created_at)->format('Y-m-d H:i') == $this->created_at){

                return $accaunt->payment_method;
            }

        }*/

    }

    public function getTempPaymentIdAndType()
    {
        //dd($this->created_at);

        $accaunts = $this->accountable? $this->accountable->payments() : collect(); //->where('created_at', 'Like', "%{$this->created_at}%");

        foreach ($accaunts as $accaunt) {

            if (Carbon::parse($accaunt->created_at)->format('Y-m-d H:i') == $this->created_at){

                return [
                    'id' => $accaunt->id,
                    'type' => get_class($accaunt)
                    ];
            }

        }

    }

    public function getPaymentReferenceNumbers()
    {
        //dd($this->created_at);

        return $this->payable ? $this->payable->reference_code: null;

        /*$accaunts = $this->accountable? $this->accountable->payments() : collect(); //->where('created_at', 'Like', "%{$this->created_at}%");

        foreach ($accaunts as $accaunt) {

            if (Carbon::parse($accaunt->created_at)->format('Y-m-d H:i') == $this->created_at){

                return $accaunt->reference_code;
            }

        }*/

    }

    public function accountToArray()
    {
        return [
            'id' => $this->id,
            'accountable' => [
                'customers' => @$this->accountable->customer_details,
            ]
        ];

    }


    public function getAccountType()
    {

        return ( strtolower($this->accountable_type ) == "sale" ) ? "FSL_": "OFFER_";


    }

    public function getHref()
    {
        if (strtolower($this->accountable_type )==='sale'){

            return url('sale/manage/approve-sale/'.$this->accountable_id);
        }

        return url('offer/generate/approve/'.$this->accountable_id);
    }
}
