<?php

namespace App\DB\Communication;
 use App\Customer\Customer;
 use Codex\Classes\Model;


class CommunicationMeta extends Model
{
    protected $guarded = [];


    public function communication()
    {
        return $this->belongsTo(Communication::class ,'communication_id','id');


    }

    public function customer()
    {
        return $this->belongsTo(Customer::class ,'customer_id','id');

    }
}
