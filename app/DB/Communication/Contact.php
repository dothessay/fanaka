<?php

namespace App\DB\Communication;

use App\DB\Product\Product;
use App\Traits\IsMetable;
use App\User;
use Codex\Classes\Model;

class Contact extends Model
{
    use IsMetable;

    protected $guarded = [];

    protected $casts = [
        'hasVisited' => 'boolean'
    ];

    public  $metas = [
        'status',
        'date_collected',
        'comment',
        'status',
        'projects',
        'next_follow_up',
        'last_follow_up',
    ];

    public function getPhoneAttribute($phone)
    {

        if (! is_null($phone))
        {
            $phone = str_replace('+','', $phone);

            // check if the first character is 0
            if ($phone[0] == 0)
            {
                $phone =  ltrim($phone, $phone[0]);
            }
            // if check 3 characters are 254 else add at the beginning

            if (isset($phone[0]) && isset($phone[1]) && isset($phone[2]))
            {
                $prefix = $phone[0].''.$phone[1].''.$phone[2];

                if ($prefix != 254){

                    $phone = 254..$phone;
                }

            }

            return $this->attributes['phone'] =  $phone;
        }
        return $this->attributes['phone'] = null;
    }
    public function user()
    {
        return $this->belongsTo(User::class , 'user_id','id');

    }

    public function path()
    {
        return url('/communication/manage/contacts/'. $this->id);

    }
    public function getContacts()
    {


    }

    public function getVisitedProjects()
    {
        $projects = [];

        if ($this->getMeta('projects', false))
        {
            $unserialisedProjects = unserialize($this->getMeta('projects'));

            $projects = Product::whereIn('id', $unserialisedProjects)->get()->map(function ($q) {
                return $q->name();
            })->toArray();
        }

        return implode(',', $projects);
    }


}
