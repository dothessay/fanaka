<?php

namespace App\DB\Communication;


use Codex\Classes\Model;

class Communication extends Model
{

    protected $guarded = [];


    public function metas()
    {
        return $this->hasMany(CommunicationMeta::class,'communication_id','id');

    }


    public function communicationToArray()
    {
        return [
            'id'       => $this->id,
            'message'  => $this->message,
            'short'    => str_limit($this->message,'20','....'),
            'type'     => $this->message_type,
            'metas'    => $this->metas->map(function ($meta) { return ['customer' => $meta->customer->fullName() ,'is_read' => $meta->is_read] ;}),
        ];

    }
}
