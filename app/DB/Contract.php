<?php

namespace App\DB;


use App\DB\Sale\OfferLetter;
use App\Sale\Sale;
use Codex\Classes\Helper;
use Codex\Classes\Model;


class Contract extends Model
{
    protected $guarded = [];

    public $purchaser;
    public $projectName;
    public $propertyString;
    public $propertySelected;
    public $methodOfPayment;
    public $cashOption;
    public $installmentOption;
    public $installmentInWords;
    public $reservationInWords;
    public $installmentInFigures;
    public $clientName;

    public $plotDetails;

    public $paidAmount;

    public $installmentStatement;
    public $saleAmountInWords;
    public $saleAmountInFigures;
    public $getMeta;
    public $motherTitle;
    public $project;
    public $plotNo;


    public function getOfferDetails(OfferLetter $letter)
    {
        $this->projectName = $letter->getCustomerPlots();
        $this->purchaser = $letter->getCustomerDetailsForAgreement();
        $this->propertyString = $letter->getPropertyDetails();
        $this->propertySelected = $letter->getPlotNos();
        $this->methodOfPayment = $letter->payment_option;
        $this->installmentInWords = Helper::numberToWord((double)$letter->installment_option) ;
        $this->reservationInWords = Helper::numberToWord((double)$letter->intialPayment()) ;
        $this->cashOption = number_format(floatval($letter->cash_option ) , 2) . " ". Helper::numberToWord(floatval($letter->cash_option ));
        $this->installmentOption = number_format(floatval( str_replace(',','', $letter->installment_option) ),2);
        $this->installmentInFigures = number_format((double)$letter->intialPayment(), 2);
        $this->clientName = $letter->getCustomerDetails();
        $this->getMeta = $letter->getMeta('reservation_days', fanakaConfig('reservation_days', 7));

        return $this;

    }
    public function getSaleDetails(Sale $sale)
    {
        $this->plotDetails = $sale->getPlotDetails();
        $this->purchaser = $sale->getCustomerDetailsForAgreement();
        $this->paidAmount = floatval($sale->getPaidAmount());
        $this->installmentStatement = $sale->getContractAmountInStatement();
        $this->saleAmountInWords = Helper::numberToWord(floatval($sale->getTotalSaleAmount()));
        $this->saleAmountInFigures = $sale->getTotalSaleAmount();
        $this->motherTitle =  $sale->getMotherTitle();
        $this->project =  $sale->getProject();
        $this->plotNo =  $sale->getPlotNo();


        return $this;


    }

}
