<?php

namespace App\DB\Account;


use App\User;
use Codex\Classes\Model;

class PettyCashRequisitionRequest extends Model
{
    protected $guarded = [];


    public function user()
    {
        return $this->belongsTo(User::class);

    }

    public function rejectedBy()
    {
        return $this->belongsTo(User::class ,'rejected_by','id');

    }
    public function approvedBy()
    {
        return $this->belongsTo(User::class,'approved_by','id');

    }
    public function disbursedBy()
    {
        return $this->belongsTo(User::class,'disbursed_by','id');

    }
    public function status() : string
    {
        $status = '';
        if (is_null($this->approved_at) && is_null($this->rejected_at))
        {
            $status = '<span class="label label-warning">Pending</span>';
        }
        if (!is_null($this->approved_at))
        {
            $status = '<span class="label label-primary">Approved</span>';

        }
        if (!is_null($this->rejected_at))
        {
            $status = '<span class="label label-danger">Rejected</span>';
        }
        if (!is_null($this->disbursed_at))
        {
            $status = '<span class="label label-success">Disbursed</span>';
        }
        return $status;
    }
}
