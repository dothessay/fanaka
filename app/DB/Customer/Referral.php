<?php

namespace App\DB\Customer;

use App\Customer\Customer;
use App\User;
use Carbon\Carbon;
use Codex\Classes\Model;

class Referral extends Model
{
    protected $guarded = [];





    public function customer()
    {
        return $this->belongsTo(Customer::class ,'customer_id','id');


    }


    public function agent()
    {
        return $this->belongsTo(User::class ,'agent_id','id');


    }

    public function referralToArray()
    {
        return [
            'id'            => $this->id,
            'fullName'      => $this->customer->fullName(),
            'date'          => Carbon::parse($this->created_at)->format('Y-m-d H:i:s'),
            'agent'         => $this->agent->fullName(),
            'agentUrl'      => route('users_manage_details',['user' => $this->agent->id]),
            'phone_number'  => $this->customer->phone_number,
            'is_contacted'  => $this->is_contacted,

        ];

    }
}
