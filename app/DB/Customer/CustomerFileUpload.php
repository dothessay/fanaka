<?php

namespace App\DB\Customer;


use App\Customer\Customer;
use Codex\Classes\Model;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;

class CustomerFileUpload extends Model
{
    protected $guarded = [];

    protected static function boot()
    {
        parent::boot();

    }


    protected $casts = [
        'is_seen' => 'boolean'
    ];

    public function customer()
    {
        return $this->belongsTo(Customer::class,'customer_id','id');

    }

    public function scopeNotSeen($query)
    {
        return $query->where('is_seen', false);
    }
    public function uploadsToArray()
    {
        $mimetype = File::extension($this->file_url);
        return [
            'id'       => $this->id,
            'customer' => $this->customer->fullName(),
            'type'     => $this->file_type,
            'isSeen'   => $this->is_seen,
            'url'      => url($this->file_url),
            'meme_type' => $mimetype
        ];

    }
}
