<?php

namespace App\DB\Customer;

use Codex\Classes\Model;

class NextOfKin extends Model
{
    protected $table = "next_of_kins";
    protected $guarded = [];


    public function getFullNameAttribute($value)
    {

        return $this->attributes['full_name'] = ucwords(strtolower($value));

    }
    public function getRelationAttribute($value)
    {

        return $this->attributes['relation'] = ucwords(strtolower($value));

    }

    public function fullName()
    {
        return $this->full_name;
    }
}
