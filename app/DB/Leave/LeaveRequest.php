<?php

namespace App\DB\Leave;

use App\User;
use Illuminate\Database\Eloquent\Model;

class LeaveRequest extends Model
{
    protected $guarded = [];


    public function user()
    {
        return $this->belongsTo(User::class , 'user_id','id');

    }


    public function handling()
    {
        return $this->belongsTo(User::class , 'handling_user_id','id');

    }



    public function handleClient()
    {
        return $this->belongsTo(User::class , 'handle_clients_user_id','id');

    }


    public function scopeNotApproved($query)
    {
        return $query->where('is_approved', false);
        
    }


    public function statusColor()
    {
        if ($this->status == 'pending'){

            return "badge badge-warning";

        }
        if ($this->status == 'accepted'){

            return "badge badge-success";

        }
        if ($this->status == 'rejected'){

            return "badge badge-danger";

        }

    }
}
