<?php

namespace App\DB\User;
use App\User;
use Codex\Classes\Model;

class DeactivatedAgentCustomer extends Model
{
    protected $guarded = [];


    public function activeAgent()
    {
        return $this->belongsTo(User::class , 'user_id','id');

    }

    public function agent()
    {
        return $this->belongsTo(User::class , 'agent_id','id');

    }

    public function agentCustomers()
    {

        return $this->agent->customers();

    }


}
