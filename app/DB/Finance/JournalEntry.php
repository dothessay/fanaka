<?php

namespace App\DB\Finance;
use App\User;
use Codex\Classes\Model;

class JournalEntry extends Model
{
    protected $guarded = [];


    public function journalable()
    {
        return $this->morphTo();

    }

    public function user()
    {
        return $this->belongsTo(User::class ,'user_id','id');
    }

    public function chart()
    {
        return $this->belongsTo(ChartOfAccount::class ,'chart_id','id');
    }
    public function save(array $options = [])
    {
        $this->user_id = auth()->id();

        parent::save();
    }



}
