<?php

namespace App\DB\Finance;

use Illuminate\Database\Eloquent\Model;

class ExpenseType extends \Codex\Classes\Model
{
    protected $guarded = [];

    public function typeToArray()
    {
        return [
            'id' => $this->id,
            'name'  => $this->name
        ];

    }
}
