<?php

namespace App\DB\Finance;

use App\DB\Expense\Expense;
use Illuminate\Database\Eloquent\Model;

class ChartOfAccount extends Model
{
    protected $guarded = [];


    public function expenses()
    {
       return $this->hasMany(Expense::class,'chart_id','id');
    }

    public function journals()
    {
        return $this->hasMany(JournalEntry::class,'chart_id','id');


    }
    public function path()
    {
        return url('finance/charts/'. $this->id.'/show');

    }
    public function chartToArray()
    {
        return [
            'id' => $this->id,
            'name'  => $this->name,
            'code' => $this->code,
            'type'  => [
                'id'  => $this->type->id,
                'name'  => $this->type->name
            ]
        ];

    }
}
