<?php

namespace App\DB\Product;

use Illuminate\Database\Eloquent\Model;

class ProjectExpensePurpose extends Model
{
    protected $guarded = [];
    
    public function projectexpenses()
    {
        return $this->belongsTo(ProjectExpense::class, 'expense_id');
    }
}
