<?php

namespace App\DB\Product;

use App\DB\Plot\ProjectPricing;
use App\Plot\CanSellPlot;
use App\Plot\Plot;
use App\Sale\Sale;
use App\Sale\SaleItem;
use App\Sale\SalePayment;
use App\Traits\IsMetable;
use App\User;
use Carbon\Carbon;
use Codex\Classes\Model;

class Product extends Model
{
    use IsMetable;
    protected $guarded = [];


    protected $casts = [
        'cost' => 'float'
    ];


    protected $appends = ['name'];


    public function user()
    {
        return $this->belongsTo(User::class,'user_id','id')->withTrashed();

    }

    public function plots()
    {
        return $this->hasMany(Plot::class,'product_code','code');

    }


    public function salePayments()
    {
        return $this->hasManyThrough(
            SalePayment::class,
            SaleItem::class ,
            'product_id' ,  // Foreign key on sales items table...
            'sale_id', // Foreign key ons sale payments table...
            'id', // Local key on customers table...
            'sale_id'  // Local key on customer sales  table...
        );
    }

    public function canSellPlots()
    {
        return $this->hasMany(CanSellPlot::class,'product_code','code');

    }
    public function pricing()
    {
        return $this->hasMany(ProjectPricing::class,'project_id','id');

    }

    public function setSlugAttribute($value) {

        // grab the title and slugify it
        return $this->attributes['slug'] = str_slug($this->businessCode().'_'.$this->name());
    }

    public function getTitleNoAttribute($value) {

        // grab the title and slugify it
        return $this->attributes['title_no'] = strtoupper(strtolower($value));
    }


    public function setCostAttribute($value) {

        // grab the title and slugify it
        return $this->attributes['cost'] = str_replace(',','',$value);
    }

    public function getCostAttribute($value) {

        // grab the title and slugify it
        return $this->attributes['cost'] = str_replace(',','',$value);
    }

    public function getLandLocationAttribute($value) {

        // grab the title and slugify it
        return $this->attributes['land_location'] =  ucwords(strtolower($value));
    }


    public function name()
    {
        if ($this->code === 'fanaka_malaa_phase_6')
        {
            return $this->land_location;
        }

        $phase =   $this->display_phase ? ' Phase '. $this->phase : "";
        return $this->land_location .' '. $phase;
    }


    public function sales()
    {
        $sales = collect();

        foreach ($this->plots as $plot) {

            if (isset($plot->saleItem->sale)){
                $sale = $plot->saleItem->sale;

                $sales->push($sale);
            }

        }


        return $sales;

    }

    public function getHeldPlots()
    {

        $plots = [];

        foreach ($this->plots as $plot) {


            if ($plot->isPlotOnHold())
            {
                $plots[] =  $plot;
            }

        }
        return collect($plots);
    }

    public function getAvailablePercentage()
    {
        $plots = $this->plots->count();


        $available = $this->availablePlots();

        $percentage = (100 * $available) / $plots ;

        return round(floatval($percentage ) ,-1).'%';

    }


    public function getNameAttribute()
    {
        return $this->name();
    }

    public function projectExpenses()
    {
        return $this->hasMany(ProjectExpense::class, 'project_id');
    }


    public function scopeAvailable($query)
    {

        return $query;

    }

    public function hasAvailablePlots()
    {
        if ($this->availablePlots() > 0 ){

            return true;
        }
        return false;

    }



    public function availablePlots()
    {
        $available = 0 ;

        foreach ($this->plots as $plot) {


            if ($plot->available())
            {
                $available += 1;
            }

        }

        return $available;

    }
    public function canNotPlots()
    {
        $available = 0 ;

        foreach ($this->plots as $plot) {


            if ($plot->cannotSell)
            {
                $available += 1;
            }

        }

        return $available;

    }
    public function availableForSalePlots()
    {
        $available = 0 ;

        foreach ($this->canSellPlots as $plot) {


            if ($plot->availableForSale())
            {
                $available += 1;
            }

        }

        return $available;

    }

    public function availablePlotsToArray()
    {
        $available = [] ;

        foreach ($this->plots as $plot) {


            if ($plot->available())
            {
                $available[] = $plot->plotToArray();
            }

        }

        return $available;

    }
    public function plotsToArray()
    {
        $plots = [] ;

        foreach ($this->plots as $plot) {



                $plots[] = $plot->plotToArray();


        }

        return $plots;

    }

    public function soldPlots()
    {
        $available = 0 ;

        foreach ($this->plots as $plot) {


            if (!$plot->available())
            {
                $available += 1;
            }

        }

        return $available;

    }


    public function generateMap()
    {

        $html = "<br/><br/><b>" .$this->name() ." Map </b>";

        $html .= "<table border='1' style='border-spacing: 12px' width='100px'>";
        $html .= "<thead>";
        $html .= "<tr>";


        $html .= "</thead>";

        $html .= "<tbody>";
        foreach ($this->plots->chunk(2) as $plots) {

            $html .= "<tr>";


            foreach ($plots as $plot)
            {
                $available = "red";
                $isAvailable = "Taken";
                if ($plot->available()){
                    $available = 'lightgreen';
                    $isAvailable = "Available";
                }
                $html .= "<td>".$plot->plot_no."</td>";
                $html .= "<td style='background-color: $available; '>".$isAvailable."</td>";
            }


        }
        $html .= "</tr>";
        $html .= "</tbody>";
        $html .= "</table>";
        return $html;

    }

    public function totalCollections($startDate , $endDate)
    {
        $total = [];

       $payments =  $this->plots->map(function($plot) use ($startDate, $endDate, $total) {

          if ( ! is_null($plot->saleItem)) {


             return $plot
                  ->saleItem
                  ->salePayment
                 ->whereBetween('created_at', [Carbon::parse($startDate)->startOfYear(), Carbon::parse($endDate)->endOfYear()])
                 ->groupBy(function ($payment) {
                     return Carbon::parse($payment->created_at)->format('y');
                 });

          }

       })->filter();

        foreach ($payments as $payment) {

            foreach ($payment as  $index => $item) {
                $total[$index][] = $item;
            }
            
       }


        return $total;
    }
}
