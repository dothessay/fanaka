<?php

namespace App\DB\Product;

use Illuminate\Database\Eloquent\Model;

class ProjectExpense extends Model
{
    protected $guarded = [];
    
    public function projects()
    {
        return $this->belongsTo(Product::class, 'project_id');
    }
    
    public function projectexpensepurpose()
    {
        return $this->hasMany(ProjectExpensePurpose::class, 'expense_id');
    }
}
