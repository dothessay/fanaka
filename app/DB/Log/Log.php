<?php

namespace App\DB\Log;


use App\User;
use Codex\Classes\Model;

class Log extends Model
{
    protected $guarded = [];

    public function user()
    {
        return $this->belongsTo(User::class , 'user_id','id');

    }

    public function getData()
    {

        return unserialize($this->data);
    }
}
