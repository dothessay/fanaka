<?php

namespace App\DB\Offer;


use App\DB\Sale\OfferLetter;
use Codex\Classes\Model;

class OfferUpload extends Model
{
    protected $guarded = [];


    public function offer()
    {
        return $this->belongsTo(OfferLetter::class,'offer_letter_id','id');

    }

    public function uploadsToArray()
    {


        return [
            'id'    => $this->id,
            'date'  => $this->created_at->format('Y-m-d H:i'),
            'type'  => $this->type ? : "Receipt",
            'url'   => url($this->file_url),
            'name'  => $this->file_name
        ];

    }
}
