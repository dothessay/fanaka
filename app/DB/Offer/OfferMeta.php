<?php

namespace App\DB\Offer;


use App\DB\Sale\OfferLetter;
use Codex\Classes\Model;

class OfferMeta extends Model
{
    protected $guarded = [];

    public function offer()
    {

        return $this->belongsTo(OfferLetter::class,'offer_id','id');
    }
}
