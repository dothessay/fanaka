<?php

namespace App\DB\Sale;

use App\DB\Accounting\Account;
use App\User;
use Carbon\Carbon;
use Codex\Classes\Model;


class OfferPayment extends Model
{
    protected $guarded = [];




    protected $casts = [
        'amount' => 'double'
    ];



    public function payables()
    {
        return $this->morphMany(Account::class, 'payable');

    }



    public function payable()
    {
        return $this->morphOne(Account::class, 'payable');

    }



    public function savePayable(Account $account)
    {
        return $this->payables()->save($account);
    }


    public function setAmountAttribute($value) {

        // grab the title and slugify it
        return $this->attributes['amount'] = (double) (str_replace(',','',$value));
    }

    public function getAmountAttribute($value) {

        // grab the title and slugify it
        return $this->attributes['amount'] = (double)(str_replace(',','',$value));
    }



    public function getCreatedAtAttribute($value)
    {
        return $this->attributes['created_at'] = Carbon::parse($value)->format('Y-m-d H:i');

    }


    public function getSlipNoAttribute($value)
    {
        if (strtoupper($value) === 'N/A')
        {
            $value = '';
        }

        return $this->attributes['slip_no'] = $value;
    }
    public function getReferenceCodeAttribute($value)
    {
        $value = strtolower($value);

        if ( $value == 'cash')
        {
            $value = "Cash";
        }
        if ( $value == 'bank')
        {
            $value = $this->slip_no;
        }
        if ( $value == 'cheque')
        {
            $value = $this->cheque_no;
        }




        return $this->attributes['reference_code'] = strtoupper($value);
    }


    public function user()
    {
        return $this->belongsTo(User::class, 'user_id','id');
    }


    public function offer()
    {
        return $this->belongsTo(OfferLetter::class, 'offer_id','id');
    }
}
