<?php

namespace App\DB\Sale;


use App\Sale\Sale;
use App\Sale\SalePayment;
use Carbon\Carbon;
use Codex\Classes\Model;
use Codex\Scopes\IsApproved;

class Installment extends Model
{
    protected $guarded = [];

    public function sale()
    {
        return $this->belongsTo(Sale::class,'sale_id','id');

    } public function salePayments()
    {
        return $this->hasMany(SalePayment::class,'sale_id','sale_id')->withoutGlobalScopes([IsApproved::class]);

    }

    public function getCompletionDateAttribute()
    {
        return  $this->attributes['completion_date'] = Carbon::parse($this->created_at)->addMonths($this->months);

    }


    public function getAmountAttribute($value)
    {

        if ( (int)$this->sale->getBalance() > 0) {



            $months = $this->months;


            //get initial amount

            $initialAmount = $this->sale->getInitialAmount();


            $saleAmount = $this->sale->total_amount;

            $balance = floatval($saleAmount) - floatval($initialAmount);


            if ($value === 0) {

                return $this->attributes['amount'] = 1;
            }


            if ($this->sale->hasAgreement()) {

                $initialMonths = $balance / $value;


                $balance = $this->sale->getBalance();


                $months = $this->getMonthsRemaining($initialMonths);

            }

                //dd($balance , $months .' Reamaining' , ceil($initialMonths ). " INitial");


            $installmentAmount = floatval(floatval($balance) / floatval($months <1 ? 1 : $months));


            return $this->attributes['amount'] = floatval($installmentAmount);
        }

        return $this->attributes['amount'] = floatval($value < 1 || ! isset($value) ? 1 : $value);

    }
    public function getMonthsAttribute($value)
    {

        return $this->attributes['months'] = $value ;

    }


    public function getMonthsRemaining($months)
    {



        if (! $this->sale->salePayments->count()){

            return (integer)$months;

        }


        $dateStarted = Carbon::parse($this->salePayments->last()->created_at);


        $lastPaymentMonth = Carbon::parse($this->salePayments->first()->created_at);



        $deadLine = Carbon::parse($dateStarted)->addMonths($months);


        $remainingMonths =  Carbon::parse($deadLine)->diffInMonths(Carbon::parse($lastPaymentMonth));


        return (integer)($remainingMonths);

    }


    public function installmentToArray()
    {

        return [
            'months'  => $this->months,
        ];

    }



}
