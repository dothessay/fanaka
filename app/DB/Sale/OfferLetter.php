<?php

namespace App\DB\Sale;

use App\Customer\Customer;
use App\DB\Accounting\Account;
use App\DB\Customer\NextOfKin;
use App\DB\Offer\OfferInstallment;
use App\DB\Offer\OfferMeta;
use App\DB\Offer\OfferUpload;
use App\User;
use Carbon\Carbon;
use Codex\Classes\Helper;
use Codex\Classes\Model;

class  OfferLetter extends Model
{
    protected $guarded = [];


    protected $appends = [
        'customer_details',
        'color'
    ];

    public function offerInstallment()
    {
        return $this->hasOne(OfferInstallment::class, 'offer_id', 'id');

    }

    public function getCustomerDetailsAttribute()
    {
        return $this->getCustomerDetails();
    }

    public function getTotalAmountAttribute($value)
    {
        return $this->attributes['total_amount'] = $this->offerItems->sum('selling_price');
    }

    /**
     * @return string
     */
    public function getCustomerDetails()
    {

        $customerDetails = [];


        foreach ($this->customers as $customerSale) {


            $customerDetails[] = $customerSale->fullName() . '';


        }

        return implode($customerDetails, ' , ');

    }

    public function getPaymentOptionAttribute($value)
    {
        return $this->attributes['payment_option'] = ucwords(strtolower(str_replace("_", " ", $value)));
    }


    public function offerItems()
    {
        return $this->hasMany(OfferLetterItem::class, 'offer_letter_id', 'id');

    }

    public function offerPayments()
    {
        return $this->hasMany(OfferPayment::class, 'offer_id', 'id');

    }



    public function offerCustomers()
    {
        return $this->hasMany(OfferCustomers::class, 'offer_letter_id', 'id');

    }

    public function customers()
    {
        return $this->belongsToMany(Customer::class, 'offer_customers');

    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');

    }


    public function agent()
    {
        return $this->belongsTo(User::class, 'agent_id', 'id');

    }


    public function soldBy()
    {
        return $this->belongsTo(User::class, 'agent_id', 'id');

    }


    public function approvedBy()
    {
        return $this->belongsTo(User::class, 'approved_by', 'id');

    }

    public function meta()
    {
        return $this->hasMany(OfferMeta::class, 'offer_id', 'id');

    }

    public function getMeta($key, $default = '')
    {

        $value = $this->meta->where('key', $key)->first();

        if (! is_null($value)){
            return $value->value;
        }
        return $default;

    }

    public function nextOfKin()
    {
        $nextOfKins = collect();


        foreach ($this->customers as $customer) {

            foreach ($customer->nextOfKins as $nextOfKin) {

                $nextOfKins->push($nextOfKin);

            }

        }
        return $nextOfKins;

    }







    public function getNextOfKin()
    {
        $nextOfKinArray = ["<br/><br/>Next Of Kins: "];



            foreach ($this->nextOfKin() as $nextOfKin) {

                $nextOfKinArray[] ="Name: <b>". $nextOfKin->fullName() . "</b>&nbsp;&nbsp;&nbsp;Phone Number  : &nbsp;<b>".$nextOfKin->phone_number ."</b>&nbsp;&nbsp;&nbsp;Relation :<b>" .$nextOfKin->relation."</b></br></br>";
            }




        return  implode($nextOfKinArray, ' , ');

    }


    public function account(Account $account)
    {
        return $this->accounts()->save($account);
    }

    public function accounts()
    {

        return $this->morphMany(Account::class, 'accountable');

    }


    public function uploads()
    {
        return $this->hasMany(OfferUpload::class ,'offer_letter_id','id');

    }


    public function payments()
    {
        return $this->offerPayments;

    }


    public function getNoteAttribute($value)
    {
        $note = (strtolower($value));
        $arrayNote = str_split($note);
        //unset($arrayNote[1]);
        //unset($arrayNote[0]);

        //$unsetNote  = str_replace("<"," ", $arrayNote);

        $arrayNoteToStringAgain = implode("", $arrayNote);

        $unsetNote = str_replace("<p>", " ", str_replace("</p>", "", $arrayNoteToStringAgain));

        return $this->attributes['note'] = ucfirst($unsetNote);

    }

    public function setNoteAttribute($value)
    {
        $unsetNote = str_replace("<p>", " ", str_replace("</p>", "", $value));

        return $this->attributes['note'] = ucfirst(strtolower($unsetNote));

    }

    public function customer()
    {

        $customerDetails = [];


        foreach ($this->customers as $customerSale) {


            $customerDetails[] = $customerSale->customer->contactPoint();


        }

        return implode($customerDetails, ' , ');
    }

    public function getTotalSaleAmount()
    {
        if ($this->payment_option === 'cash_option') {

            return $this->saleItems->sum('price');

        }


    }

    public function getTotalOfferAmount()
    {
        if ($this->payment_option === 'cash_option') {

            return $this->offerItems->sum('selling_price');

        }
        return $this->offerItems->sum('selling_price');


    }

    public function contactPoint()
    {

        $customerDetails = [];


        foreach ($this->customers as $customerSale) {


            $customerDetails[] = $customerSale->contactPoint();


        }

        return implode($customerDetails, ' , ');
    }

    public function intialPayment()
    {
        return @$this->offerPayments->sortByDesc('created_at')->first()->amount;

    }

    public function getPercentage()
    {
        $totalPaid = $this->offerPayments->sum('amount');


        $totalAmount = $this->total_amount;


        $percentage = ((double)$totalPaid * 100) / (double)$totalAmount;


        return $percentage;


    }

    /**
     * @return mixed
     */
    public function getPaidAmount()
    {
        return $this->offerPayments->sum('amount');

    }

    /**
     * @return string
     */
    public function getContractAmountInStatement()
    {
        if ($this->getBalance() > (double)0) {
            return "an initial amount of " . Helper::numberToWord(floatval($this->offerPayments->sum('amount')) . "  " . number_format(floatval($this->offerPayments->sum('amount'))));
        }
        return "full amount of " . Helper::numberToWord(floatval($this->offerPayments->sum('amount'))) . "  " . number_format((double)$this->offerPayments->sum('amount'));

    }

    public function getBalance()
    {
        return $this->offerItems->sum('selling_price') - $this->getPaidAmount();


    }

    public function getCustomerDetailsForAgreement()
    {

        $customerDetails = [];


        foreach ($this->customers as $customerSale) {


            $customerDetails[] = '<br/>Full Name:<i><b> ' . $customerSale->fullName() . ' </b></i> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                 ID :  &nbsp; <i><b> ' . $customerSale->getCustomerId() . '</b></i><br/>
                                 Phone number&nbsp; : <i><b> ' . $customerSale->getPhoneNumber() . '</b></i>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                 Email:<i>  <b> ' . $customerSale->email . '</b></i><br/><br/>';


        }


        return implode($customerDetails, ' , ');

    }

    public function getCustomerPlots()
    {

        $customerDetails = [];


        foreach ($this->offerItems as $offerItem) {


            $customerDetails[] = $offerItem->plot->product->name .' Plot No:'. $offerItem->plot->plot_no;



            //$customerDetails[] = $customerSale->getCustomerOfferPlots() . '';


        }

        return implode($customerDetails, ' , ');

    }


    public function getCustomerPlotNos()
    {

        $plotDetails = [];

        $plotNo = [];


        foreach ($this->offerItems as $offerItem) {


            // $plotDetails[] = $saleItem->plot->product->name() . ' ' . $saleItem->plot->product->title_no ;
            $plotNo[] = $offerItem->plot->product->name() . ' Plot  no: ' . $offerItem->plot->plot_no;


        }


        return implode($plotNo, ' , ');

    }

    public function getPlotNos()
    {

        $plotDetails = [];

        $plotNo = [];


        foreach ($this->offerItems as $offerItem) {

            $title = ! is_null($offerItem->plot->title_no) ? $offerItem->plot->title_no :"..................................................";

            // $plotDetails[] = $saleItem->plot->product->name() . ' ' . $saleItem->plot->product->title_no ;

            $plotNo[] = "plot no: <b>" .$offerItem->plot->plot_no . "</b><small><i>        Write the plot number(s) here</i></small><br/><b>" .
                $title."</b><small><i>                 Write the block numbers(s) here</i></small>";


        }


        return implode($plotNo, ' , ');

    }

    public function getPropertyDetails()
    {

        $plotDetails = [];

        $plotNo = [];


        foreach ($this->offerItems as $offerItem) {


            // $plotDetails[] = $saleItem->plot->product->name() . ' ' . $saleItem->plot->product->title_no ;
            $plotNo[] = $offerItem->plot->getPropertyDetails();


        }


        return implode($plotNo, ' , ');

    }


    public function getProjectAndPlot()
    {

        $plotDetails = [];

        $plotNo = [];


        foreach ($this->offerItems as $offerItem) {


            $plotNo[] = $offerItem->plot->product->name() . ' Plot No: ' . $offerItem->plot->plot_no ;
            //$plotNo[] = $offerItem->plot->getPropertyDetails();


        }


        return implode($plotNo, ' , ');

    }

    public function getCustomerId()
    {

        $customerDetails = [];


        foreach ($this->customers as $customerSale) {


            $customerDetails[] = $customerSale->customer->getCustomerId() . '';


        }

        return implode($customerDetails, ' , ');

    }

    public function getCustomerHref()
    {

        $customerDetails = [];

        foreach ($this->offerCustomers as $customerSale) {

            $customerDetails[] = '   <a title="Click to see customer details" target="_blank" href=' . url('customers/manage/statement/' . $customerSale->customer->id) . '?sale=' . $this->id . ' >' . $customerSale->customer->fullName() . '</a>   ';

        }

        return implode($customerDetails, ' , ');

    }

    public function getCustomerPhoneNumber()
    {

        $customerDetails = [];

        foreach ($this->offerCustomers as $offerCustomer) {


            $customerDetails[] = $offerCustomer->customer->getPhoneNumber() . '';


        }

        return implode($customerDetails, ' , ');

    }


    public function signatories()
    {
        $customerDetails = [];

        $details = "";


        foreach ($this->offerCustomers as $offerCustomer) {


            $customer = $offerCustomer->customer;



            //foreach ($offerCustomer->customer as $customer) {


               // dd($customer);

                $customerDetails[] = '<br/><br/>Name:<b>' . str_repeat("&nbsp;", 10). $customer->fullName() .str_repeat("&nbsp;", 10). ' </b> <br/> Email:<b>' . str_repeat("&nbsp;", 10). "." .str_repeat(".", 100). ' </b> <br/> Sign:'.str_repeat("&nbsp;", 9). '<b>'.str_repeat(".",100).'</b><br/>Date: '.str_repeat("&nbsp;", 10) .Carbon::now()->format('Y-m-d') ;


            //}

        }

        return implode($customerDetails, '  ');


    }

    public function GetInstallment()
    {

        if ($this->getBalance() > (double)0) {
            return $this->getBalance() / $this->offerInstallment->months;
        }

    }






    public function getProjects()
    {

        $projects = [];




        foreach ($this->offerItems as $offerItem) {

            $projects[] = $offerItem->plot->product->name();

        }
        return implode($projects, '  ');


    }


    public function offerToArray()
    {
        return [
            'customer'        => $this->getCustomerDetails(),
            'customers'       => $this->customers->map(function ($customer) { return $customer->customerToArray() ;}),
            'customerHref'    => $this->getCustomerHref(),
            'date'            => $this->created_at->format('Y-m-d H:i'),
            'paymentOption'   => $this->payment_option,
            'plots'           => $this->offerItems->map(function ($offerItem) { return $offerItem->plot->plotToArray(); }),
            'status'          => $this->is_approved ? "Is Approved" : "Pending",
            'amount'          => config('config.currency') .' '.number_format($this->offerPayments->sum('amount') , 2),
        ];
    }

    public function getColorAttribute()
    {
        $endDate =  Carbon::parse($this->created_at)->addRealDays($this->getMeta('reservation_days', 7));

        $color = "";

        if ($endDate->lessThan(now()))
        {
            $color = "background: yellow";
        }

        return $this->attributes['color']  = $color;
    }


}
