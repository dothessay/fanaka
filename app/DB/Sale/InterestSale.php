<?php

namespace App\DB\Sale;

use App\Sale\Sale;
use Carbon\Carbon;
use Codex\Classes\Model;

class InterestSale extends Model
{
    protected $guarded = [];

    public function sale()
    {
        return $this->belongsTo(Sale::class , 'sale_id','id');

    }


    public function scopeHasBalance($query)
    {
        return $query->sale->getBalance() > 0 ? true : false;

    }
}
