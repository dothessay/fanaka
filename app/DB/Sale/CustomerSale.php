<?php

namespace App\DB\Sale;

use App\Customer\Customer;
use App\Plot\Plot;
use App\Sale\Invoice;
use App\Sale\Sale;
use App\Sale\SaleItem;
use Codex\Classes\Model;

class CustomerSale extends Model
{
    protected $guarded = [];


    //protected $with = ['sale','customer'];
    public function sale()
    {
        return $this->belongsTo(Sale::class,'sale_id','id');

    }

    public function invoice()
    {
        return $this->belongsTo(Invoice::class,'sale_id','id');

    }
    public function customer()
    {
        return $this->belongsTo(Customer::class,'customer_id','id');

    }


    public function uploads()
    {
        return $this->hasMany(SaleUpload::class ,'sale_id','sale_id');

    }

    public function saleItems()
    {
        return $this->sale->saleItems;

    }
}
