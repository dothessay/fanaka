<?php

namespace App\DB\Sale;


use App\Sale\Sale;
use App\Sale\SalePayment;
use Carbon\Carbon;
use Codex\Classes\Model;
use Codex\Classes\Reports\GuessPrepaid;
use Codex\Classes\Reports\Sales\Perfomance\Sales;

class MonthlyInstallmentPlan extends Model
{

    protected $appends = [
        'payment_date'
    ];

    public function prepaid()
    {
        return $this->belongsTo(MonthlyInstallmentPrepaid::class,'month','month');
    }

    public function getPaymentDateAttribute()
    {
        return $this->attributes['payment_date'] = Carbon::parse($this->month.'-'.$this->day)->format('Y-m-d');
    }


    public function getAmountAttribute($value)
    {



        return $this->attributes['amount'] =  floatval($value);
    }

    public function getIsPaidAttribute()
    {
        $value = false;
        if (MonthlyInstallmentPrepaid::where(['sale_id' => $this->sale->id , 'month' => $this->month])->count())
        {
            $value = true;
        }
        return $this->attributes['is_paid'] =  $value;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     * @param Sale
     */
    public function sale()
    {
        return $this->belongsTo(Sale::class , 'sale_id','id');
    }

    public function balance()
    {
        return $this->sale->getBalance();
    }

    public function expectedPayment()
    {
        return '';
    }

    public function getNthDate()
    {

        $lastCharacter = substr("{$this->day}", -1);

        if ($lastCharacter === "1")
        {
            return "<sup>st</sup>";

        }
        if ($lastCharacter === "2")
        {
            return "<sup>nd</sup>";

        }
        if ($lastCharacter === "3")
        {
            return "<sup>rd</sup>";

        }
        if (floatval($lastCharacter) > 3)
        {
            return "<sup>th</sup>";

        }
    }

    public function prepaidAmount($month)
    {
        return $this->sale->getPrepaidAmount();

    }


    public function amountPaid($month , $paidAmount =0)
    {
        $amount =  (new Sales())->isMonthlyInstalmentPaid($this->sale, $month);


        $guessPrepaid =  $this->guessPrepaid();


        $months = Carbon::parse($month)->diffInMonths(Carbon::parse($this->sale->created_at)->format('Y-m-d'));


        $expected = $guessPrepaid->getRequiredPercentage() + ($this->amount * ( $months  ));


        if ($this->sale->getPaidAmount() >= $expected)
        {
            return $this->amount;
        }

        if ($paidAmount + $this->amount >  $this->sale->getPaidAmount())
        {
            return $this->sale->getPaidAmount() - $paidAmount ;

            return $expected - $paidAmount ;
            //dd($expected , $paidAmount , $this->sale->getPaidAmount() , $paidAmount + $this->amount);
        }

        return  $amount;
    }

    public function getPrepaidAmount($month, $paidAmount)
    {
        $guessPrepaid =  $this->guessPrepaid();


        $months = Carbon::parse($month)->diffInMonths(Carbon::parse($this->sale->created_at)->format('Y-m-d'));


        $expected = $guessPrepaid->getRequiredPercentage() + ($this->amount * ( $months ));

        if ( $this->sale->getPaidAmount() >= $expected)
        {
            return $expected - $paidAmount;
        }
        return 0;

    }
    /**
     * @return  GuessPrepaid
     */
    public function guessPrepaid() : GuessPrepaid
    {
       return  $this->sale->guessPrepaid();
    }
}
