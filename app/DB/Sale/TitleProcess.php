<?php

namespace App\DB\Sale;

use App\Sale\Sale;
use Codex\Classes\Model;

class TitleProcess extends Model
{
    protected $guarded = [];

    public function sale()
    {
        return $this->belongsTo(Sale::class,'sale_id','id');
    }

    public function hasIssue()
    {
        if (is_null($this->received_at) && ! is_null($this->has_issue_at))
        {
            return true;
        }
        return false;
    }
    public function goodCondition()
    {
        if (!is_null($this->received_at))
        {
            return true;
        }
        return false;
    }
    public function clientHasCollected()
    {
        if (! is_null($this->client_collect_at))
        {
            return true;
        }
        return false;
    }
    public function pending()
    {
        if (is_null($this->client_collect_at) && is_null($this->received_at) && is_null($this->has_issue_at) && is_null($this->start_processing_at))
        {
            return true;
        }
        return false;
    }
    public function processing()
    {
        if (is_null($this->client_collect_at) && is_null($this->received_at) && is_null($this->has_issue_at) && ! is_null($this->start_processing_at))
        {
            return true;
        }
        return false;
    }

    public function titleStatus()
    {


        if ($this->hasIssue())
        {
           return "<span class='label label-danger'>Bad Condition</span>";
        }

        if ($this->clientHasCollected())
        {
            return"<span class='label label-success'>Collected by The Client</span>";
        }

        if ($this->pending())
        {
            return "<span class='label label-warning'>Pending</span>";
        }
        if ($this->processing())
        {
            return "<span class='label label-inverse'>processing</span>";
        }
        if ($this->goodCondition())
        {
            return "<span class='label label-inverse'><small>Good Condition  (In the office)</small></span>";
        }


    }

    public function nextStep()
    {

        if ($this->pending())
        {
            return "<li><a href='". route('sale_transfer_upload-documents', ['titleProcess' => $this->id])."'> Upload Documents </a></li>
                    <li><a href='". route('sale_transfer_start_processing', ['titleProcess' => $this->id])."'>Start Process</a></li>";
        }
        if ($this->processing())
        {
            $html =  "<li><a href='". route('sale_transfer_start_processing', ['titleProcess' => $this->id])."'>Good Condition</a></li>";
            $html .=  "<li><a href='". route('sale_transfer_start_processing', ['titleProcess' => $this->id])."'>Bad Condition</a></li>";
            return $html;

        }
        if ($this->hasIssue())
        {
            $html =  "<li><a href='". route('sale_transfer_start_processing', ['titleProcess' => $this->id])."'>Good Condition</a></li>";
            $html .=  "<li><a href='". route('sale_transfer_start_processing', ['titleProcess' => $this->id])."'>Bad Condition</a></li>";
            return $html;

        }
        if ($this->goodCondition())
        {
            $html =  "<li><a href='". route('sale_transfer_start_processing', ['titleProcess' => $this->id])."'> Upload Title </a></li>";
            $html .=  "<li><a href='". route('sale_transfer_start_processing', ['titleProcess' => $this->id])."'> Issue </a></li>";
            return $html;

        }
    }

    public function modelToArray()
    {
        return [
            'id'  => $this->id,
            'customer' => $this->sale->customer_details,
            'balance' => $this->sale->balance,
            'project' => $this->sale->getPropertyDetails(),
            'status' => $this->titleStatus(),
            'nextStep'  => $this->nextStep()
        ];
    }
}
