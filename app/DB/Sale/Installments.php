<?php

namespace App\DB\Sale;

use Illuminate\Database\Eloquent\Model;

class Installments extends Model
{
    protected $table = "installments";
}
