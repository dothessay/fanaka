<?php

namespace App\DB\Sale;

use App\DB\Product\Product;
use App\User;
use Carbon\Carbon;
use Codex\Classes\Model;

class Appointment extends Model
{
    protected $guarded =[];


    protected $casts = [
        'is_interested' => 'boolean',
        'has_visited' => 'boolean',
    ];

    public function getDateTimeAttribute($value)
    {
        return $this->attributes['date_time'] = Carbon::parse($value);

    }

    public function getFullNameAttribute($value)
    {
        return $this->attributes['full_name'] = ucwords(strtolower($value));

    }

    public function setFullNameAttribute($value)
    {
        return $this->attributes['full_name'] = ucwords(strtolower($value));

    }

    public function user()
    {
        return $this->belongsTo(User::class,'user_id','id');
    }

    public function agent()
    {
        return $this->belongsTo(User::class,'agent_id','id');
    }

    public function getProjects()
    {
        $projectIds = unserialize($this->project_ids);


        $project = [];

        foreach ($projectIds as $projectId) {
            $project[] = Product::where('id', $projectId)->first()->name();
        }


        return implode(" and ", $project);
    }

    public function appointmentToArray()
    {
        return [
            'id'          => $this->id,
            'full_name'   => $this->full_name,
            'date_time'    =>  $this->date_time->isToday()? "<span class='badge badge-success'>Today at ".$this->date_time->format('H:i A') ."</span>": $this->date_time->format('Y-m-d H:i A'),
            'phone_number'   => $this->phone_number,
            'agent'   => $this->agent->fullName(),
            'user'    => $this->user->fullName(),
            'projects' => $this->getProjects(),
            'has_visited' =>  ($this->has_visited) ? "<span class='badge badge-success'>Yes</span>" : "<span class='badge badge-warning'>No</span>",
            'is_interested' => ($this->is_interested) ?"<span class='badge badge-success'>Yes</span>" : "<span class='badge badge-warning'>No</span>",
            'comment'  => $this->note

        ];

    }
}


