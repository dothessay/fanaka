<?php

namespace App\DB\Sale;


use App\Sale\Sale;
use Codex\Classes\Model;

class Calculator extends Model
{
    protected $guarded = [];

    public function sale()
    {
        return $this->belongsTo(Sale::class);
    }

    public function calcToArray()
    {
        return [
            'id'   => $this->id,
            'saleId'  => $this->sale->id,
            'month'  => $this->month,
            'monthlyPayment'  => $this->amount,
            'amountPaid' => $this->amount_paid
        ];
    }
}
