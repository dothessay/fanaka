<?php

namespace App\DB\Sale;

use App\Sale\Sale;
use Codex\Classes\Model;
use Illuminate\Support\Facades\Storage;

class SaleUpload extends Model
{
    protected $guarded = [];


    public function sale()
    {
        return $this->belongsTo(Sale::class,'sale_id','id');

    }

    public function uploadsToArray()
    {
        $url = file_exists($this->agreement_url) ? asset($this->agreement_url) :  asset('storage/'.$this->agreement_url);
        return [
            'id'    => $this->id,
            'date'  => $this->created_at->format('Y-m-d H:i'),
            'type'  => $this->type,
            'url'   => $url,
            'path' => $this->agreement_url,
            'name'  => $this->file_name
        ];

    }
}
