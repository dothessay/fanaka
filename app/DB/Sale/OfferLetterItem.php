<?php

namespace App\DB\Sale;

use App\Plot\Plot;
use Illuminate\Database\Eloquent\Model;

class OfferLetterItem extends Model
{
    protected $guarded = [];


    public function offer()
    {

        return $this->belongsTo(OfferLetter::class,'offer_letter_id','id');
    }

    public function plot()
    {

        return $this->belongsTo(Plot::class,'plot_id','id');
    }


    public function offerToItems()
    {
        return [
            'plots' => [
                'project' => [
                    'name' => $this->plot->project->name(),
                    'block' => $this->plot->project->title,

                    ],
                'no' => $this->plot->plot_no

            ]
        ];

    }



}
