<?php

namespace App\DB\Sale;

use App\Customer\Customer;
use Illuminate\Database\Eloquent\Model;

class OfferCustomers extends Model
{
    protected $guarded = [];


    public function offer()
    {
        return $this->belongsTo(OfferLetter::class,'offer_letter_id','id');

    }

    public function offerItems()
    {
        return $this->hasMany(OfferLetterItem::class,'offer_letter_id','offer_letter_id');

    }
    public function customer()
    {
        return $this->belongsTo(Customer::class,'customer_id','id');

    }

    public function getCustomerOfferPlots()
    {
        $plotDetails = [];

        $plotNo = [];


            foreach ($this->offerItems as $offerItem)
            {


                // $plotDetails[] = $saleItem->plot->product->name() . ' ' . $saleItem->plot->product->title_no ;
                $plotNo[] =  $offerItem->plot->title_no. ' '. $offerItem->plot->product->name() . ' Plot  no: ' . $offerItem->plot->plot_no;



        }



        return   implode($plotNo, ' , ');

    }


    public function customerOfferToArray()
    {
        return [
            'customer' => [
                'first_name' => $this->customer->first_name,
                'last_name' => $this->customer->last_name,
                'middle_name' => $this->customer->middle_name,
                'email_address' => $this->customer->email,
                'phone_number' => $this->customer->phone_number,
                'id_number' => $this->customer->id_number,
            ]
        ];


    }

}
