<?php

namespace App\DB\Sale;

use App\Sale\Sale;
use Codex\Classes\Model;

class SaleExtraNote extends Model
{
    protected $guarded = [];


    public function sale()
    {
        return $this->belongsTo(Sale::class,'sale_id','id');

    }
}
