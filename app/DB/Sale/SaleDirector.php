<?php

namespace App\DB\Sale;


use App\Customer\Customer;
use Codex\Classes\Model;
use Codex\Scopes\MyCustomers;

class SaleDirector extends Model
{
    protected $guarded =[];


    public function customer()
    {
        return $this->belongsTo(Customer::class ,'customer_id','id')->withoutGlobalScopes([MyCustomers::class]);

    }
}
