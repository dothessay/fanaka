<?php

namespace App\DB\Notification;

use App\User;
use Codex\Classes\Model;

class Notification extends Model
{
    protected $guarded =[];


    public function user()
    {
        return $this->belongsTo(User::class,'user_id','id')->withTrashed();

    }


    public function notifications()
    {
        return self::where([
            'read' => false,
            'user_id' => auth()->id()
        ])->orderBy('read','ASC')->get();
    }
}
