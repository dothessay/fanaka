<?php

namespace App\DB\Notification;


use App\User;
use Codex\Classes\Model;

class Issue extends Model
{
    protected $guarded = [];


    public function user()
    {
        return $this->belongsTo(User::class ,'user_id','id');

    }

    public function gettypeAttribute($value)
    {
        return $this->attributes['type'] = ucfirst(strtolower($value));
    }


    public function getIssueAttribute($value)
    {
        $note = (strtolower($value));
        $arrayNote = str_split($note);
        unset($arrayNote[0]);
        unset($arrayNote[1]);
        unset($arrayNote[2]);

        //$unsetNote  = str_replace("<"," ", $arrayNote);

        $arrayNoteToStringAgain = implode("",$arrayNote);

        $unsetNote  = str_replace(">"," ", str_replace("</p>","",$arrayNoteToStringAgain));

        return $this->attributes['issue'] =   ucfirst($arrayNoteToStringAgain);

    }

    public function notifications()
    {
        if(auth()->user()->can('access-module-component','communication_issue')){
            return self::where([
                'is_read' => false,
            ])->orderBy('is_read','ASC')->get();
        }
        return collect();

    }
}
