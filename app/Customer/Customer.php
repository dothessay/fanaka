<?php

namespace App\Customer;

use App\DB\Customer\CustomerFileUpload;
use App\DB\Customer\NextOfKin;
use App\DB\Customer\Referral;
use App\DB\Role\Business;
use App\DB\Sale\CustomerSale;
use App\DB\Sale\OfferCustomers;
use App\DB\Sale\OfferLetter;
use App\DB\Sale\OfferPayment;
use App\DB\Witness;
use App\Http\Requests\Customer\StoreCustomer;
use App\Sale\Invoice;
use App\Sale\Sale;
use App\Sale\SalePayment;
use App\User;
use Codex\Classes\Model;
use Codex\Scopes\IsApproved;
use Codex\Scopes\MyCustomers;
use Exception;
use Illuminate\Database\Eloquent\SoftDeletes;
use Laravel\Scout\Searchable;
use Illuminate\Validation\Rule;

class Customer extends Model
{

    use SoftDeletes, Searchable ;

    protected $appends = [
        'full_name'
    ];

    protected $guarded = [];

    protected $with = ['nextOfKins'];

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);

        static::addGlobalScope(new MyCustomers());
    }

    private  function validateRequest()
    {
        $request = request();

        return $this->validate($request , [
            'id_no'   =>  [
                'required',
                Rule::unique('customers','id_no')
            ]
        ],
            [
                'id_no.required' => 'ID no is required',
                'id_no.unique' => 'ID no is required',
            ]);
    }

    public static function store($data)
    {
        try {
            $user = request()->user();

            $customer = Customer::where('id_no', $data['id_no'])->first();
            if ( is_null($customer) )
            {
                $customer = self::create([
                    'business_code' => $user->business->code,
                    'first_name' => $data['first_name'],
                    'last_name' => $data['last_name'],
                    'email' => $data['email'],
                    'phone_number' => $data['phone_number'],
                    'id_no' => $data['id_no'],
                    'middle_name' => $data['middle_name'],
                    'user_id' => $user->id,

                ]);
            }

            return $customer;
        }
        catch (Exception $exception) {

            dd($exception);
            throw new Exception($exception->getMessage());
        }


    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id')->withTrashed();
    }

    public function business()
    {
        return $this->belongsTo(Business::class, 'business_code', 'code');
    }

    public function witness()
    {
        return $this->hasMany(Witness::class, 'customer_id', 'id');
    }

    public function nextOfKins()
    {
        return $this->hasMany(NextOfKin::class, 'customer_id', 'id');
    }

    public function sales()
    {
        return $this->belongsToMany(Sale::class, 'customer_sales');
    }

    public function salePayments()
    {
        return $this->hasManyThrough(
            SalePayment::class,
            CustomerSale::class,
            'customer_id',  // Foreign key on customer sales table...
            'sale_id', // Foreign key ons sale payments table...
            'id', // Local key on customers table...
            'sale_id'  // Local key on customer sales  table...
        );
    }
    public function invoices()
    {
        return $this->hasManyThrough(
            Invoice::class,
            CustomerSale::class,
            'customer_id',  // Foreign key on customer sales table...
            'sale_id', // Foreign key ons sale payments table...
            'id', // Local key on customers table...
            'sale_id'  // Local key on customer sales  table...
        )->where('is_paid', false);
    }

    public function offers()
    {
        return $this->belongsToMany(OfferLetter::class, 'offer_customers');
    }

    public function customerSale()
    {
        return $this->hasMany(CustomerSale::class, 'customer_id', 'id');
    }

    public function uploads()
    {
        return $this->hasMany(CustomerFileUpload::class, 'customer_id', 'id');

    }

    public function referrals()
    {
        return $this->hasMany(Referral::class, 'referrer_id', 'id');

    }

    public function saveCustomer(StoreCustomer $request)
    {

        dd($request->validated());


    }

    public function customerInvoices()
    {

        $invoices = collect();

        foreach ($this->customerSale as $customerSale) {

            // if ($customerSale->sale->getBalance() >= 1 ) {

            if ($customerSale->sale->invoices) {

                $invoices->push($customerSale->sale->invoices);
            }
            // }

        }

        return $invoices;
    }

    public function customerOffers()
    {
        return $this->hasMany(OfferCustomers::class, 'customer_id', 'id');
    }

    public function getMiddleNameAttribute($value)
    {
        if (strtoupper($value) === 'N/A' || $value === '.') {
            $value = '';
        }

        return $this->attributes['middle_name'] = ucwords(strtolower($value));
    }

    public function getFullNameAttribute()
    {

        return $this->attributes['full_name'] = $this->fullName();
    }

    public function getImageAttribute($value)
    {


        return $this->attributes['image'] = $value;
    }

    public function getPhoneNumberAttribute($value)
    {
        if (strtoupper($value) === '+254' || $value === '+254n/a' || $value === '+254+254' || $value === 'undefined' || $value === '+undefined' || $value === '254') {
            $value = '';
        }

        return $this->attributes['phone_number'] = $value;
    }

    public function getIdNoAttribute($value)
    {
        if (strtoupper($value) === 'N/A') {
            $value = '';
        }

        return $this->attributes['id_no'] = $value;
    }

    public function getEmailAttribute($value)
    {
        if (strtoupper($value) === 'N/A') {
            $value = $value;
        }

        return $this->attributes['email'] = strtolower($value);
    }

    public function contactPoint()
    {

        $email = isset($this->email) ? $this->email : " ";
        ob_start();
        ?>
        Phone Number :<?= $this->getPhoneNumber() ?> <br>
        Email: <?= $email ?>

        <?php

        return ob_get_clean();
    }

    public function getPhoneNumber()
    {
        $phoneNumber = str_split($this->phone_number);

        if (sizeof($phoneNumber) > 4) {
            return $this->phone_number;
        }
        return ".....";
    }

    public function getCustomerId()
    {

        if (isset($this->id_no)) {
            return $this->id_no;
        }
        return "....";
    }

    public function getCustomerPlots()
    {
        $plotNo = [];
        foreach ($this->customerSale as $sale) {

            foreach ($sale->sale->saleItems as $saleItem) {
                $plotNo[] = '<p>'.$saleItem->plot->product->title_no . ' ' . $saleItem->plot->product->name() . ' Plot  no: ' . $saleItem->plot_no ."</p>";
            }

        }
        return implode($plotNo, ' ');

    }

    public function getCustomerPlotNos()
    {

        $plotDetails = [];

        $plotNo = [];


        foreach ($this->customerSale as $sale) {

            foreach ($sale->sale->saleItems as $saleItem) {


                // $plotDetails[] = $saleItem->plot->product->name() . ' ' . $saleItem->plot->product->title_no ;
                $plotNo[] = $saleItem->plot->product->name() . ' Plot  no: ' . $saleItem->plot_no;

            }

        }


        return implode($plotNo, ' , ');

    }

    public function getCustomerOfferPlots()
    {

        $plotDetails = [];

        $plotNo = [];


        foreach ($this->customerOffers as $customerOffer) {

            foreach ($customerOffer->offer->offerItems as $offerItem) {


                // $plotDetails[] = $saleItem->plot->product->name() . ' ' . $saleItem->plot->product->title_no ;
                $plotNo[] = $offerItem->plot->product->title_no . ' ' . $offerItem->plot->product->name() . ' Plot  no: ' . $offerItem->plot->plot_no;

            }

        }


        return implode($plotNo, ' , ');

    }

    public function customerStatementToArray()
    {


        $toReturn = collect();

        if (collect($this->customerReservationStatementToArray())->count()) {

            if (!$this->customerReservationStatementToArray()['isConverted']) {

                $toReturn->push($this->customerReservationStatementToArray());
            }
        }
        $customerSales = CustomerSale::where('customer_id', $this->id)->get();

        $sales = [];

        $plots = [];


        $paymentDetails = [];


        $partners = [];

        $uploads = [];

        foreach ($customerSales as $customerSale) {

            $sales[] = [
                'id' => $customerSale->sale->id,
                'reference' => 'SALE/' . $customerSale->sale->id,
                'saleAmount' => $customerSale->sale->saleItems->sum('price'),
                'paidAmount' => $customerSale->sale->salePayments->sum('amount'),
                'agent' => $customerSale->sale->soldBy->fullname(),
                'balance' => $customerSale->sale->saleItems->sum('price') - $customerSale->sale->salePayments->sum('amount') //$customerSale->sale->getBalance(),
            ];

            foreach ($customerSale->sale->saleItems as $saleItem) {

                $plots[] = [
                    'landLocation' => $saleItem->plot->product->name(),
                    'price' => $saleItem->price,
                    'size' => $saleItem->plot->size->label,
                    'plotNo' => $saleItem->plot->plot_no,
                ];

            }

            $salePayments = SalePayment::withoutGlobalScope(IsApproved::class)->where([
                'sale_id' => $customerSale->sale->id
            ])->get();


            foreach ($salePayments as $payment) {

                $paymentDetails[] = [
                    'dateDeposited' => $payment->deposit_date,
                    'dateReceived' => $payment->created_at,
                    'amountPaid' => $payment->amount,
                    'method' => $payment->payment_method,
                    'referenceCode' => $payment->reference_code,
                    'receivedBy' => $payment->user->fullName(),
                ];

            }

            $partners[] = CustomerSale::where([
                'sale_id' => $customerSale->sale->id
            ])->where('customer_id', '!=', $this->id)->get();


            foreach ($customerSale->sale->saleUploads as $saleUpload) {

                $uploads[] = $saleUpload->uploadsToArray();


            }


        }


        $toReturn->push([
            'title' => "Sale Details For " . $this->fullName(),
            'sales' => $sales,
            'plots' => $plots,
            'payments' => $paymentDetails,
            'partners' => $partners,
            'uploads' => $uploads,
        ]);


        return $toReturn->toArray();


    }

    public function customerReservationStatementToArray()
    {
        $customerOffers = OfferCustomers::where('customer_id', $this->id)->get();

        $isConverted = true;
        $offers = [];

        $plots = [];


        $paymentDetails = [];


        $partners = [];

        $uploads = [];

        foreach ($customerOffers as $customerSale) {

            if (!$customerSale->offer->is_converted) {

                $isConverted = false;

            }

            $offers[] = [

                'reference' => 'OFFER/' . $customerSale->offer->id,
                'id' => $customerSale->offer->id,
                'saleAmount' => $customerSale->offer->total_amount,
                'agent' => $customerSale->offer->agent->fullName(),
                'paidAmount' => $customerSale->offer->offerPayments->sum('amount'),
                'balance' => $customerSale->offer->getBalance(),
            ];

            foreach ($customerSale->offer->offerItems as $saleItem) {

                $plots[] = [
                    'landLocation' => $saleItem->plot->product->name(),
                    'price' => $saleItem->selling_price,
                    'size' => $saleItem->plot->size->label,
                    'plotNo' => $saleItem->plot->plot_no,
                ];

            }

            $salePayments = OfferPayment::withoutGlobalScope(IsApproved::class)->where([
                'offer_id' => $customerSale->offer->id
            ])->get();


            foreach ($salePayments as $payment) {

                $paymentDetails[] = [
                    'dateDeposited' => $payment->deposit_date,
                    'dateReceived' => $payment->created_at,
                    'amountPaid' => $payment->amount,
                    'method' => $payment->payment_method,
                    'referenceCode' => $payment->reference_code,
                    'receivedBy' => $payment->user->fullName(),
                ];

            }

            $partners[] = OfferCustomers::where([
                'offer_letter_id' => $customerSale->offer->id
            ])->where('customer_id', '!=', $this->id)->get();


            foreach ($customerSale->offer->uploads as $saleUpload) {

                $uploads[] = $saleUpload->uploadsToArray();


            }


        }


        return [
            'title' => "Reservation For " . $this->fullName(),
            'sales' => $offers,
            'plots' => $plots,
            'payments' => $paymentDetails,
            'partners' => $partners,
            'uploads' => $uploads,
            'isConverted' => $isConverted
        ];


    }

    public function fullName()
    {
        $middleName = isset($this->middle_name) ? $this->middle_name : " ";

        if (strtoupper($middleName) === 'N/A') {
            $middleName = '';
        }

        return ucwords(strtolower($this->first_name . " " . $middleName . " " . $this->last_name));
    }

    public function agent()
    {
        $sales = $this->sales;

        $agents = collect();


        foreach ($sales as $sale) {

            $agents->push($sale->soldBy);

        }

        return $agents;
    }


    public function customerToArray()
    {
        return [
            'ref_no' => $this->id,
            'name' => $this->full_name,
            'first_name' => $this->first_name,
            'last_name' => $this->last_name,
            'middle_name' => $this->middle_name,
            'id' => $this->id_no,
            'phone_number' => $this->getPhoneNumber(),
            'email' => $this->email,
            'image' => $this->getImage(),
            'agents' => $this->agent()->map->userToArray()
        ];
    }

    public function customerWithSalesToArray()
    {
        return [
            'ref_no' => $this->id,
            'name' => $this->fullName(),
            'first_name' => $this->first_name,
            'last_name' => $this->last_name,
            'middle_name' => $this->middle_name,
            'id' => $this->id_no,
            'phone_number' => $this->getPhoneNumber(),
            'email' => $this->email,
            'image' => $this->getImage(),
            'sales' => $this->sales->map(function ($q) {
                return $q->saleToArray();
            }),
            'agents' => $this->agent()->map->userToArray()
        ];
    }


    public function getImage()
    {
        if (is_null($this->image)) {
            $userMail = $this->email;
            $imageWidth = '16'; //The image size
            $imageWidth = '16';
            $imgUrl = 'http://www.gravatar.com/avatar/' . md5($userMail) . 'fs=' . $imageWidth;
            return $imgUrl;
        }

        return $this->image;

    }

    public function modelToArray()
    {
        return [
            'id' => $this->id,
            'photo'   => $this->getImage(),
            'name'   => $this->fullName(),
            'email'  => $this->email,
            'phone'   => $this->phone_number,
            'id_no'   => $this->id_no
        ];
    }

    public function scopeFilter($query , array $filter)
    {
        $filter = $filter['query'];

       return $query->where('first_name','LIKE', "%{$filter}%")
           ->orWhere('last_name','LIKE', "%{$filter}%")
           ->orWhere('middle_name','LIKE', "%{$filter}%")
           ->orWhere('middle_name','LIKE', "%{$filter}%")
           ->orWhere('id_no','LIKE', "%{$filter}%")
           ->orWhere('email','LIKE', "%{$filter}%");
    }

    public function toSearchableArray()
    {
        return array_only($this->toArray(), ['id', 'first_name', 'last_name', 'middle_name' , 'id_no','email','phone_number']);
    }


}
