<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->

<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->
<head>
    @include('layouts.partials.header')
    @yield('extra_css_header')
    @yield('extra_js_header')
</head>
<body onload="startTime()">
<!-- begin #page-loader -->
<div id="page-loader" class="fade in"><span class="spinner"></span></div>
<!-- end #page-loader -->

<!-- begin #page-container -->
<div id="page-container" class="fade page-sidebar-fixed page-header-fixed">
    <!-- begin #header -->
    <div id="header" class="header navbar navbar-default navbar-fixed-top">
        <!-- begin container-fluid -->
        <div class="container-fluid">

            <!-- begin mobile sidebar expand / collapse button -->
            <div class="navbar-header">
                <a href="/" class="navbar-brand">
                    <img src=" {{ url( \Illuminate\Support\Facades\Storage::url(request()->user()->business->logo))}}"
                         style="max-height: 35px;" alt="Fanaka Real Estate"/>
                </a>
                <button type="button" class="navbar-toggle" data-click="sidebar-toggled">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
            </div>
            <!-- end mobile sidebar expand / collapse button -->

            <!-- begin navbar-collapse -->
            <div class="collapse navbar-collapse pull-left" id="top-navbar">


                <ul class="nav navbar-nav">

                    <li class="dropdown navbar-user">
                        <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown">
                            <i class="fa fa-angle-double-right"></i> Payments</a>
                        </a>
                        <ul class="dropdown-menu animated fadeInLeft">
                            <li class="arrow"></li>
                            <li> <a href="{{ route('sale_invoice') }}"> Receive Payments</a></li>
                            <li> <a href="{{ route('finance_requisition_index', [], false) }}"> Requisition</a></li>
                        </ul>
                    </li>

                    <li class="dropdown navbar-user">
                        <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown">
                            <i class="fa fa-angle-double-right"></i> Reports</a>

                        </a>
                        <ul class="dropdown-menu animated fadeInLeft">
                            <li class="arrow"></li>
                            <li><a href="{{url('/projects/reports/unpaid?agent='.auth()->id())}}">Unpaid & Defaulters</a></li>
                            <li><a href="{{url('projects/reports/available?agent='.auth()->id())}}">Availability</a></li>
                            @can('access-module-component-functionality','sale_reports_customer')
                                <li><a href="{{ route('sale_reports_customer',  ['agent' => auth()->id()])}}">By Customer</a></li>
                            @endcan
                            @can('access-module-component-functionality','sale_reports_agent')
                                <li><a href="{{ route('sale_reports_agent', ['agent' => auth()->id()])}}">By Agent</a></li>
                            @endcan
                            @can('access-module-component-functionality','sale_reports_above-50')
                                <li><a href="{{ route('sale_reports_above-50',  ['agent' => auth()->id()])}}">Above 50</a></li>
                            @endcan
                            @can('access-module-component-functionality','sale_reports_below-50')
                                <li><a href="{{ route('sale_reports_below-50',  ['agent' => auth()->id()])}}">Below 50</a></li>
                            @endcan

                            @can('access-module-component-functionality','sale_reports_scanned-agreement')
                                <li><a href="{{ route('sale_reports_scanned-agreement',  ['agent' => auth()->id()])}}">Scanned Documents</a></li>
                            @endcan

                            @can('access-module-component-functionality','sale_reports_week-installment')
                                <li><a href="{{ route('sale_reports_week-installment',  ['agent' => auth()->id()])}}">Weekly Instalments</a></li>
                            @endcan

                            @can('access-module-component-functionality','sale_reports_profit-and-loss')
                                <li><a href="{{ route('sale_reports_profit-loss',  ['agent' => auth()->id()])}}">Profit & Loss</a></li>
                            @endcan

                            @can('access-module-component-functionality','sale_reports_by-project')
                                <li><a href="{{ route('sale_reports_by-project',  ['agent' => auth()->id()])}}">Cash Sale</a></li>
                            @endcan


                        </ul>
                    </li>

                    <li><a><i class="fa fa-angle-double-right"></i> <strong><span id="innox-time"></span></strong></a></li>
                </ul>
            </div>
            <!-- end navbar-collapse -->

            <!-- begin header navigation right -->
            <ul class="nav navbar-nav navbar-right">
            {{--HIDDEN TILL SEARCH IS IMPLEMENTED--}}
            {{--<li>
                <form class="navbar-form full-width">
                    <div class="form-group">
                        <input type="text" class="form-control" placeholder="Enter keyword" />
                        <button type="submit" class="btn btn-search"><i class="fa fa-search"></i></button>
                    </div>
                </form>
            </li>--}}

            <!-- POS Button -->

            {{--  @include('layouts.partials.languages')--}}


            <!-- End of POS Button -->

                @include('layouts.partials.notification')

                @include('layouts.partials.toptab')

            </ul>
            <!-- end header navigation right -->
        </div>
        <!-- end container-fluid -->
    </div>
    <!-- end #header -->

    <!-- begin #sidebar -->
    @include('layouts.partials.sidemenu')

    <div class="sidebar-bg"></div>
    <!-- end #sidebar -->

    <!-- begin #content -->
    <div id="content" class="content">

        <!-- begin breadcrumb -->
    @yield('breadcrumbs')
    <!-- end breadcrumb -->

        <!-- begin page-header -->
        <h1 class="page-header hidden-print">@yield('title','Portal') <small>@yield('sub_title')</small></h1>

        <!-- end page-header -->

        @include('layouts.partials.error')



        <div id="app">

            <div class="panel panel-body">
                @yield('content')
            </div>

        </div>

    </div>
    <!-- end #content -->

    <!-- begin scroll to top btn -->
    <a href="javascript:;" class="btn btn-icon btn-circle btn-success btn-scroll-to-top fade" data-click="scroll-top"><i class="fa fa-angle-up"></i></a>
    <!-- end scroll to top btn -->
</div>



<!-- end page container -->
@include('layouts.partials.footer')
@include('layouts.partials.alerts')

<script src="https://unpkg.com/@ionic/core@latest/dist/ionic.js"></script>
<script src="https://cdn.jsdelivr.net/npm/vue@2.6.10/dist/vue.js"></script>
@yield('extra_js_footer')

<?php


/*print @\Codex\Classes\Helper::chatWidget() ;*/



?>

@livewireAssets
<script>
    $(document).ready(function() {
        App.init();
        $(".selectpicker").selectpicker("render");


    });

    function startTime() {
        var today = new Date();
        var h = today.getHours();
        var m = today.getMinutes();
        var s = today.getSeconds();
        m = checkTime(m);
        s = checkTime(s);
        document.getElementById('innox-time').innerHTML =
            h + ":" + m + ":" + s;
        var t = setTimeout(startTime, 500);
    }
    function checkTime(i) {
        if (i < 10) {i = "0" + i};  // add zero in front of numbers < 10
        return i;
    }



</script>

<script src="{{ mix('js/customer.js') }}" defer></script>

{{--<script type="text/javascript">
 composer require iflylabs/iflychat-php
    var iflychat_app_id="9dd03481-277f-4490-ac2c-6cfc7dab30b7";

    var iflychat_external_cdn_host="cdn.iflychat.com",iflychat_bundle=document.createElement("SCRIPT");iflychat_bundle.src="//"+iflychat_external_cdn_host+"/js/iflychat-v2.min.js?app_id="+iflychat_app_id,iflychat_bundle.async="async",document.body.appendChild(iflychat_bundle);var iflychat_popup=document.createElement("DIV");iflychat_popup.className="iflychat-popup",document.body.appendChild(iflychat_popup);
</script>--}}
</body>
</html>
