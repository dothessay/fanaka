@extends('customer::layouts.master')

@section('content')
    @livewire('customer-data-table')

@endsection
@section('extra_js')
@include('layouts.dataTables.datatable')
    <script>
        $("table.table").DataTable()
    </script>
@endsection


