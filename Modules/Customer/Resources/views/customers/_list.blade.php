<table class="table table-striped" id="data-table">
    <thead>
    <tr>
        <th>Name</th>
        <th>Id</th>
        <th>Email</th>
        <th>Sales</th>
        <th></th>
    </tr>
    </thead>
    <tbody>
    @foreach($customers as $customer)
        <tr>
            <td>{{ $customer->fullName() }}</td>
            <td>{{ $customer->id_no }}</td>
            <td>{{ $customer->email }}</td>
            <td>{{ $customer->sales->count() }}</td>
            <td> >> </td>
        </tr>
    @endforeach
    </tbody>
</table>
{!! $customers->links() !!}
