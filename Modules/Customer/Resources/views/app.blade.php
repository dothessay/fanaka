@extends('customer::layouts.master')

@section('content')
    <div class="table-responsive">
        <div class="col-md-12">
            <form>
                <div class="form-group">
                    <div class="col-md-6"></div>
                    <div class="col-md-4">
                        <input class="form-control" name="query" value="{{ old('query' , request()->query('query', '')) }}">
                    </div>
                    <input class="btn btn-success" name="search"type="submit" value="Search">
                    <input class="btn btn-danger" name="reset" type="submit" value="Reset">
                </div>
            </form>
        </div>
        @include('customer::customers._list')
    </div>
@endsection
@section('extra_js')
@include('layouts.dataTables.datatable')
    <script>
        $("table.table").DataTable()
    </script>
@endsection


